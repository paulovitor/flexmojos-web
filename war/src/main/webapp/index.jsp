<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>
	<title>ANews ${project.version}</title>
	<% 
		response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 946080000000L); // 01/Jan/2010
		response.setHeader("Last-Modified", Long.toString(new Date().getTime()));
		response.setHeader("Anti-cache", Double.toString(Math.random()));
	%>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<meta http-equiv="Expires" content="Fri, 31 Dec 2000 23:59:59 GMT"/>
	<meta http-equiv="Content-Language" content="pt-br"/>
	<meta http-equiv="Cache-Control" content="no-cache"/>
	<meta http-equiv="Pragma" content="no-cache"/>
	<meta name="Author" content="SNEWS"/>
	<meta name="Copyright" content="&copy; 2012 SoftwareNews"/>
	<meta name="Googlebot" content="noarchive"/>
	<meta name="Robots" content="none"/>

	<style type="text/css" media="screen">
		html, body {
			height: 100%;
		}

		body {
			margin: 0;
			padding: 0;
			overflow: auto;
			text-align: center;
			background-color: #ffffff;
		}

		#flashContent {
			display: none;
		}
	</style>

	<link rel="shortcut icon" type="image/ico" href="snews.ico"/>
	<link rel="stylesheet" type="text/css" href="history/history.css"/>
</head>

<body onunload="disconnectAll()" scroll="no" style="overflow:hidden;">
<div id="flashContent">
	<p>
		Para visualizar esse aplicativo tenha instalado o Adobe Flash Player 11.0 ou superior.
	</p>
	<script type="text/javascript">
		var pageHost = ((document.location.protocol == "https:") ? "https://" : "http://");
		document.write("<a href='http://www.adobe.com/go/getflashplayer'><img src='" + pageHost + "www.adobe.com/images/shared/download_buttons/get_flash_player.gif' alt='Get Adobe Flash player'/></a>");
	</script>
</div>

<noscript>
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="100%" id="ANews">
		<param name="movie" value="swf-1.0-SNAPSHOT.swf"/>
		<param name="quality" value="high"/>
        <param name="wmode" value="direct"/>
		<param name="bgcolor" value="#ffffff"/>
		<param name="allowScriptAccess" value="sameDomain"/>
		<param name="allowFullScreen" value="true"/>
		<!--[if !IE]>-->
		<object type="application/x-shockwave-flash" data="swf-1.0-SNAPSHOT.swf" width="100%" height="100%">
			<param name="quality" value="high"/>
            <param name="wmode" value="direct"/>
			<param name="bgcolor" value="#ffffff"/>
			<param name="allowScriptAccess" value="sameDomain"/>
			<param name="allowFullScreen" value="true"/>
			<!--<![endif]-->
			<!--[if gte IE 6]>-->
			<p>
				Tanto scripts como active contents n�o s�a permitidos a executar
				o Adobe Flash Palyer 11.0 ou essa vers�o n�o est� instalada.
			</p>
			<!--<![endif]-->
			<a href="http://www.adobe.com/go/getflashplayer">
				<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
						 alt="Get Adobe Flash Player"/>
			</a>
			<!--[if !IE]>-->
		</object>
		<!--<![endif]-->
	</object>
</noscript>

<script type="text/javascript" src="history/history.js"></script>
<script type="text/javascript" src="swfobject.js"></script>
<script type="text/javascript">
	var swfVersionStr = "10.2.0";
	var xiSwfUrlStr = "playerProductInstall.swf";
	var flashvars = {};
	var params = { quality: "hight", bgcolor: "#FFFFFF", allowScriptAccess: "sameDomain", allowFullScreen: "true", wmode: "direct" };
	var attributes = { id: "flexmojos-web", name: "flexmojos-web", align: "middle" };

	swfobject.embedSWF("swf-1.0-SNAPSHOT.swf?_dc=" + (new Date().getTime()), "flashContent", "100%", "100%", swfVersionStr, xiSwfUrlStr, flashvars, params, attributes);
	swfobject.createCSS("#flashContent", "display:block;text-align:left;");
</script>

<script type="text/javascript" src="js/jquery-1.11.1.js"></script>
<script type="text/javascript" src="js/alertMessage.js"></script>

<script type="text/javascript">
	$(window).focus(function () {
		$("#windowActived").attr("value", "true");
	});
	$(window).blur(function () {
		$("#windowActived").attr("value", "false");
	});

	/**
	 * Chamando no "unload" do body chamar o m�todo do SWF que
	 * desconecta do channel.
	 *
	 * @see http://livedocs.adobe.com/blazeds/1/blazeds_devguide/help.html?content=lcconnections_4.html
	 */
	function disconnectAll() {
        $("ANews").disconnectAll();
    }
</script>

<input type="hidden" id="windowActived" value="true"/>
</body>
</html>
