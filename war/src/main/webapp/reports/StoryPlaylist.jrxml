<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Story" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="539" leftMargin="28" rightMargin="28" topMargin="28" bottomMargin="14" isSummaryWithPageHeaderAndFooter="true" isFloatColumnFooter="true" whenResourceMissingType="Empty" uuid="561eef28-dca2-4171-afb6-a5fe0066c0d9">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="tv.snews.anews.util.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<import value="tv.snews.anews.infrastructure.*"/>
	<import value="tv.snews.anews.domain.*"/>
	<style name="fontStyle">
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontFamily} == FontFamily.ARIAL]]></conditionExpression>
			<style fontName="Arial"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontFamily} == FontFamily.GEORGIA]]></conditionExpression>
			<style fontName="Georgia"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontFamily} == FontFamily.TAHOMA]]></conditionExpression>
			<style fontName="Tahoma"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontFamily} == FontFamily.TIMES_NEW_ROMAN]]></conditionExpression>
			<style fontName="Times New Roman"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontFamily} == FontFamily.VERDANA]]></conditionExpression>
			<style fontName="Verdana"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontSize} == FontSize.TEN]]></conditionExpression>
			<style fontSize="10"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontSize} == FontSize.TWELVE]]></conditionExpression>
			<style fontSize="12"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontSize} == FontSize.FOURTEEN]]></conditionExpression>
			<style fontSize="14"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontSize} == FontSize.SIXTEEN]]></conditionExpression>
			<style fontSize="16"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontSize} == FontSize.EIGHTEEN]]></conditionExpression>
			<style fontSize="18"/>
		</conditionalStyle>
		<conditionalStyle>
			<conditionExpression><![CDATA[$P{fontSize} == FontSize.TWENTY]]></conditionExpression>
			<style fontSize="20"/>
		</conditionalStyle>
	</style>
	<parameter name="SUBREPORT_DIR" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[""]]></defaultValueExpression>
	</parameter>
	<parameter name="logo" class="java.io.InputStream"/>
	<parameter name="bundle" class="tv.snews.anews.util.ResourceBundle" isForPrompting="false"/>
	<parameter name="fontFamily" class="java.lang.Enum">
		<parameterDescription><![CDATA[]]></parameterDescription>
		<defaultValueExpression><![CDATA[tv.snews.anews.util.FontFamily.ARIAL]]></defaultValueExpression>
	</parameter>
	<parameter name="fontSize" class="java.lang.Enum">
		<parameterDescription><![CDATA[]]></parameterDescription>
		<defaultValueExpression><![CDATA[tv.snews.anews.util.FontSize.TWELVE]]></defaultValueExpression>
	</parameter>
	<field name="slug" class="java.lang.String"/>
	<field name="headSection.subSections" class="java.util.Set">
		<fieldDescription><![CDATA[headSection.subSections]]></fieldDescription>
	</field>
	<field name="footerSection.subSections" class="java.util.Set">
		<fieldDescription><![CDATA[footerSection.subSections]]></fieldDescription>
	</field>
	<field name="ncSection.subSections" class="java.util.Set">
		<fieldDescription><![CDATA[ncSection.subSections]]></fieldDescription>
	</field>
	<field name="vtSection" class="java.lang.Object">
		<fieldDescription><![CDATA[vtSection]]></fieldDescription>
	</field>
	<field name="page" class="java.lang.String"/>
	<field name="block.rundown.program.name" class="java.lang.String"/>
	<field name="editor.name" class="java.lang.String"/>
	<field name="date" class="java.util.Date"/>
	<field name="block.rundown.date" class="java.util.Date"/>
	<field name="editor.nickname" class="java.lang.String"/>
	<field name="storyKind.acronym" class="java.lang.String"/>
	<field name="vt" class="java.util.Date">
		<fieldDescription><![CDATA[vt]]></fieldDescription>
	</field>
	<field name="ncSection.timeImage" class="java.util.Date">
		<fieldDescription><![CDATA[ncSection.timeImage]]></fieldDescription>
	</field>
	<field name="ncSection" class="java.lang.Object">
		<fieldDescription><![CDATA[ncSection]]></fieldDescription>
	</field>
	<field name="vtSection.subSections" class="java.util.Set">
		<fieldDescription><![CDATA[vtSection.subSections]]></fieldDescription>
	</field>
	<field name="vtSection.cue" class="java.lang.String">
		<fieldDescription><![CDATA[vtSection.cue]]></fieldDescription>
	</field>
	<field name="ncSection.duration" class="java.util.Date">
		<fieldDescription><![CDATA[ncSection.duration]]></fieldDescription>
	</field>
	<field name="information" class="java.lang.String">
		<fieldDescription><![CDATA[information]]></fieldDescription>
	</field>
	<field name="story" class="tv.snews.anews.domain.Story">
		<fieldDescription><![CDATA[_THIS]]></fieldDescription>
	</field>
	<field name="headSection.duration" class="java.util.Date">
		<fieldDescription><![CDATA[headSection.duration]]></fieldDescription>
	</field>
	<field name="footerSection.duration" class="java.util.Date">
		<fieldDescription><![CDATA[footerSection.duration]]></fieldDescription>
	</field>
	<field name="tapes" class="java.lang.String">
		<fieldDescription><![CDATA[tapes]]></fieldDescription>
	</field>
	<field name="imageEditor.nickname" class="java.lang.String"/>
	<pageHeader>
		<band height="58" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement uuid="326f6544-53dd-4fb2-8ed1-5ef6ec9d5cf5" x="0" y="0" width="485" height="16" forecolor="#666666"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["(" +
new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateFormat")).format($F{block.rundown.date}) +
") " +
$F{block.rundown.program.name}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="9603ae52-205b-461f-bd72-9e57b6b8499b" x="0" y="16" width="208" height="35"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("rundown.playlist")]]></textFieldExpression>
			</textField>
			<image isUsingCache="true" onErrorType="Blank">
				<reportElement uuid="57b79f94-700a-419f-a098-5a90baf2afca" positionType="Float" x="485" y="2" width="50" height="50"/>
				<imageExpression><![CDATA[$P{logo}]]></imageExpression>
			</image>
		</band>
	</pageHeader>
	<detail>
		<band height="61" splitType="Stretch">
			<line>
				<reportElement uuid="ca742b35-5790-4c67-8214-61a2cb7635d3" positionType="Float" x="2" y="0" width="534" height="1" forecolor="#CCCCCC"/>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField isBlankWhenNull="true">
				<reportElement uuid="59c04b30-1fb8-4a30-8412-b14c91a77337" positionType="Float" mode="Opaque" x="2" y="1" width="27" height="20" backcolor="#CCCCCC"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{page}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="b79d2e56-b320-4092-a6f1-485f2daee390" x="32" y="1" width="40" height="19" isRemoveLineWhenBlank="true"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{storyKind.acronym}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="8374581e-3daf-4b34-bc7b-4e4368f58e78" x="77" y="1" width="455" height="19"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{slug} == null || $F{slug}.length() == 0 ? $P{bundle}.getMessage("defaults.noSlug") : $F{slug}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="176b26ec-fef2-4ad6-9d5f-ecf199f7d0e1" positionType="Float" x="2" y="20" width="535" height="1" forecolor="#CCCCCC"/>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<subreport>
				<reportElement uuid="12f1dd30-f9dc-4093-8098-ce6f86a71992" positionType="Float" isPrintRepeatedValues="false" x="2" y="27" width="535" height="3">
					<printWhenExpression><![CDATA[$F{story}.getNcSection() != null && $F{story}.getNcSection().getSubSections() != null && $F{story}.getNcSection().getSubSections().size() > 0]]></printWhenExpression>
				</reportElement>
				<subreportParameter name="fontSize">
					<subreportParameterExpression><![CDATA[$P{fontSize}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new JRBeanCollectionDataSource($F{story}.getNcSection().getSubSections())]]></dataSourceExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "StoryVideoPlaylist.jasper"]]></subreportExpression>
			</subreport>
			<subreport>
				<reportElement uuid="7ece4084-deec-4160-a34f-c9498a41ab2f" positionType="Float" isPrintRepeatedValues="false" x="2" y="33" width="535" height="3">
					<printWhenExpression><![CDATA[$F{story}.getVtSection() != null && $F{story}.getVtSection().getSubSections() != null && $F{story}.getVtSection().getSubSections().size() > 0]]></printWhenExpression>
				</reportElement>
				<subreportParameter name="SUBREPORT_DIR">
					<subreportParameterExpression><![CDATA[$P{SUBREPORT_DIR}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="bundle">
					<subreportParameterExpression><![CDATA[$P{bundle}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fontFamily">
					<subreportParameterExpression><![CDATA[$P{fontFamily}]]></subreportParameterExpression>
				</subreportParameter>
				<subreportParameter name="fontSize">
					<subreportParameterExpression><![CDATA[$P{fontSize}]]></subreportParameterExpression>
				</subreportParameter>
				<dataSourceExpression><![CDATA[new JRBeanCollectionDataSource($F{story}.getVtSection().getSubSections())]]></dataSourceExpression>
				<subreportExpression><![CDATA[$P{SUBREPORT_DIR} + "StoryVideoPlaylist.jasper"]]></subreportExpression>
			</subreport>
			<line>
				<reportElement uuid="cc28df00-2fc0-4b05-8e32-1ebcf055b146" positionType="Float" x="536" y="0" width="1" height="21" forecolor="#CCCCCC"/>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="6351b822-f431-418a-a776-939e741bd566" positionType="Float" isPrintRepeatedValues="false" x="2" y="40" width="534" height="19" isRemoveLineWhenBlank="true" forecolor="#FF3300">
					<printWhenExpression><![CDATA[$F{story}.hasVideos()]]></printWhenExpression>
				</reportElement>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="12" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("story.total") + " - " + new java.text.SimpleDateFormat("mm:ss").format($F{vt})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="025edb6a-f0ad-4a55-abcc-5279b7b2ecbf" x="2" y="40" width="535" height="19">
					<printWhenExpression><![CDATA[!($F{story}.hasVideos())]]></printWhenExpression>
				</reportElement>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("story.playlist.isEmpty")]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band height="4">
			<line>
				<reportElement uuid="4badb7e0-5f83-4917-aba5-ee1ff5d1b8d1" positionType="Float" x="0" y="1" width="535" height="1" forecolor="#CCCCCC"/>
				<graphicElement>
					<pen lineStyle="Solid"/>
				</graphicElement>
			</line>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="30">
			<textField>
				<reportElement uuid="092dfccf-678b-4394-b789-cc4168b1336e" isPrintRepeatedValues="false" x="380" y="1" width="110" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($V{PAGE_NUMBER}.intValue() < 10 ? "00" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.intValue() > 10 && $V{PAGE_NUMBER}.intValue() < 100 ? "0" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.toString()) + " " + $R{footer.of} +" "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="845c14e8-c579-44a6-bb94-2888598ee292" isPrintRepeatedValues="false" x="519" y="1" width="20" height="20"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($V{PAGE_NUMBER}.intValue() < 10 ? "00" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.intValue() > 10 && $V{PAGE_NUMBER}.intValue() < 100 ? "0" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.toString())]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy &apos;às&apos; HH:mm:ss" isBlankWhenNull="true">
				<reportElement uuid="c814e82b-57e2-47f1-aa18-876bd8f04696" x="0" y="6" width="317" height="15"/>
				<box leftPadding="2">
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateTimeFormat")).format(new Date())]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement uuid="3b246d9f-4513-4b84-a2ba-d876a8ea06b6" isPrintRepeatedValues="false" x="493" y="1" width="24" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("defaults.of")]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="e4149bd5-4b1b-4dbb-a244-2f99afed41fd" x="2" y="25" width="539" height="1" isPrintInFirstWholeBand="true" isPrintWhenDetailOverflows="true" forecolor="#666666"/>
				<graphicElement>
					<pen lineStyle="Solid" lineColor="#666666"/>
				</graphicElement>
			</line>
		</band>
	</pageFooter>
	<noData>
		<band height="20">
			<textField isBlankWhenNull="true">
				<reportElement uuid="54f7157e-9e12-44b9-b274-e04019d3137b" isPrintRepeatedValues="false" x="2" y="0" width="536" height="20"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("story.isEmpty")]]></textFieldExpression>
			</textField>
		</band>
	</noData>
</jasperReport>
