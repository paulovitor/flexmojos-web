<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ReportageGrid" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="567" leftMargin="14" rightMargin="14" topMargin="28" bottomMargin="14" isSummaryWithPageHeaderAndFooter="true" whenResourceMissingType="Empty" uuid="561eef28-dca2-4171-afb6-a5fe0066c0d9">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="tv.snews.anews.util.*"/>
	<import value="tv.snews.anews.infrastructure.*"/>
	<import value="tv.snews.anews.domain.*"/>
	<import value="org.apache.commons.lang.StringUtils"/>
	<parameter name="emissora" class="java.lang.String" isForPrompting="false"/>
	<parameter name="logo" class="java.io.InputStream"/>
	<parameter name="bundle" class="tv.snews.anews.util.ResourceBundle" isForPrompting="false"/>
	<queryString>
		<![CDATA[]]>
	</queryString>
	<field name="slug" class="java.lang.String"/>
	<field name="id" class="java.lang.Long"/>
	<field name="date" class="java.util.Date"/>
	<field name="state" class="tv.snews.anews.domain.DocumentState"/>
	<field name="program.id" class="java.lang.Integer"/>
	<field name="program.name" class="java.lang.String"/>
	<field name="reportage" class="tv.snews.anews.domain.Reportage">
		<fieldDescription><![CDATA[_THIS]]></fieldDescription>
	</field>
	<variable name="LINE_COUNT" class="java.lang.Integer" calculation="Count">
		<variableExpression><![CDATA[$F{id}]]></variableExpression>
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="57" splitType="Stretch">
			<textField>
				<reportElement uuid="38dabf9a-bf48-4167-9e90-2a036239dd20" x="0" y="0" width="505" height="20" forecolor="#666666"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["(" + new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateFormat")).format(new java.util.Date()) + ")"]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="3a517b18-9c9d-41c6-be27-c53a8ab1c8d3" x="0" y="52" width="567" height="1" forecolor="#666666"/>
				<graphicElement>
					<pen lineStyle="Solid" lineColor="#666666"/>
				</graphicElement>
			</line>
			<image isUsingCache="true">
				<reportElement uuid="ed3ce482-677c-478f-8efd-ddebe4984e30" positionType="Float" x="515" y="0" width="50" height="50"/>
				<imageExpression><![CDATA[$P{logo}]]></imageExpression>
			</image>
			<textField>
				<reportElement uuid="ba2d06fa-7379-4c5a-acde-4df771b814b5" x="1" y="24" width="504" height="20" forecolor="#666666"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("reportage.archive.titleStr")]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="22">
			<rectangle>
				<reportElement uuid="cb1c0c40-7f6f-45d8-bf08-0be203d24f46" x="0" y="2" width="567" height="20" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineColor="#CCCCCC"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement uuid="75ff230e-0a65-4c0a-ba4a-806673055529" x="2" y="3" width="62" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("reportage.date")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="20a83b17-359c-4410-a351-79fd9c8422c2" x="64" y="3" width="244" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("reportage.slug")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="2f6bbd80-5053-460d-a2c3-3c94b6b4f865" x="308" y="4" width="149" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("reportage.reporter")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="aaed2d94-7e8d-4184-b77c-f7bcd39e10c9" x="457" y="2" width="108" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("reportage.program")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="22" splitType="Stretch">
			<rectangle>
				<reportElement uuid="cb1c0c40-7f6f-45d8-bf08-0be203d24f46" positionType="Float" stretchType="RelativeToBandHeight" x="0" y="0" width="567" height="21" isRemoveLineWhenBlank="true" backcolor="#EEEEEE">
					<printWhenExpression><![CDATA[new Boolean($V{LINE_COUNT}.intValue() % 2 == 0)]]></printWhenExpression>
				</reportElement>
				<graphicElement>
					<pen lineColor="#EEEEEE"/>
				</graphicElement>
			</rectangle>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="6e0e75f7-d999-434e-9d91-dc0de95f0f53" x="2" y="1" width="62" height="18"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateFormat")).format($F{date})]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="8e4ee4ce-cf33-4cd9-ab8d-1f7888a19568" x="64" y="1" width="244" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{slug}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="b250f47e-9eeb-4df1-8234-8ee6c24a893e" x="308" y="1" width="149" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{reportage}.getReporter().getName()]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="b9e955be-970c-486d-b4df-e20311c5d51a" x="457" y="0" width="108" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{program.name} == null ? $P{bundle}.getMessage("defaults.globalDrawer") : $F{program.name}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="22">
			<textField evaluationTime="Report">
				<reportElement uuid="845c14e8-c579-44a6-bb94-2888598ee292" x="543" y="2" width="20" height="20"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($V{PAGE_NUMBER}.intValue() < 10 ? "00" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.intValue() > 10 && $V{PAGE_NUMBER}.intValue() < 100 ? "0" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.toString())]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="092dfccf-678b-4394-b789-cc4168b1336e" x="380" y="2" width="135" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($V{PAGE_NUMBER}.intValue() < 10 ? "00" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.intValue() > 10 && $V{PAGE_NUMBER}.intValue() < 100 ? "0" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.toString()) + " " + $R{footer.of} +" "]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy &apos;às&apos; HH:mm:ss" isBlankWhenNull="true">
				<reportElement uuid="c814e82b-57e2-47f1-aa18-876bd8f04696" x="5" y="7" width="317" height="15" isPrintInFirstWholeBand="true"/>
				<box leftPadding="2">
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateTimeFormat")).format(new Date())]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="d709c45a-5ef6-4adf-ad2b-4ee93b672935" x="517" y="2" width="24" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("defaults.of")]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<noData>
		<band height="20">
			<textField isBlankWhenNull="true">
				<reportElement uuid="6d73a8dc-9626-44c3-b674-00a309d0f3df" x="5" y="0" width="555" height="20"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.nothingToPrint")]]></textFieldExpression>
			</textField>
		</band>
	</noData>
</jasperReport>
