<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Report" pageWidth="595" pageHeight="842" whenNoDataType="NoDataSection" columnWidth="567" leftMargin="14" rightMargin="14" topMargin="28" bottomMargin="14" isSummaryWithPageHeaderAndFooter="true" whenResourceMissingType="Empty" uuid="561eef28-dca2-4171-afb6-a5fe0066c0d9">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="35"/>
	<property name="ireport.y" value="0"/>
	<import value="tv.snews.anews.util.*"/>
	<import value="tv.snews.anews.infrastructure.*"/>
	<import value="tv.snews.anews.domain.*"/>
	<import value="org.apache.commons.lang.StringUtils"/>
	<parameter name="logo" class="java.io.InputStream"/>
	<parameter name="bundle" class="tv.snews.anews.util.ResourceBundle" isForPrompting="false"/>
	<queryString>
		<![CDATA[]]>
	</queryString>
	<field name="slug" class="java.lang.String"/>
	<field name="event.name" class="java.lang.String"/>
	<field name="nature.name" class="java.lang.String"/>
	<field name="author.nickname" class="java.lang.String"/>
	<field name="content" class="java.lang.String"/>
	<field name="date" class="java.util.Date"/>
	<field name="lastChange" class="java.util.Date"/>
	<field name="origin" class="java.lang.String"/>
	<variable name="LINE_COUNT" class="java.lang.Integer" resetType="None" calculation="Count">
		<initialValueExpression><![CDATA[new Integer(0)]]></initialValueExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band height="50" splitType="Stretch">
			<textField>
				<reportElement uuid="38dabf9a-bf48-4167-9e90-2a036239dd20" x="1" y="9" width="507" height="20" forecolor="#000000"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="14" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.title")]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="3a517b18-9c9d-41c6-be27-c53a8ab1c8d3" x="0" y="41" width="567" height="1" forecolor="#666666"/>
				<graphicElement>
					<pen lineStyle="Solid" lineColor="#666666"/>
				</graphicElement>
			</line>
			<image isUsingCache="true" onErrorType="Blank">
				<reportElement uuid="ed3ce482-677c-478f-8efd-ddebe4984e30" positionType="Float" x="517" y="0" width="40" height="40"/>
				<imageExpression><![CDATA[$P{logo}]]></imageExpression>
			</image>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="48">
			<rectangle>
				<reportElement uuid="cb1c0c40-7f6f-45d8-bf08-0be203d24f46" x="0" y="25" width="567" height="20" backcolor="#CCCCCC"/>
				<graphicElement>
					<pen lineColor="#CCCCCC"/>
				</graphicElement>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement uuid="75ff230e-0a65-4c0a-ba4a-806673055529" x="3" y="26" width="77" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateFormat")).format($F{date})]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="20a83b17-359c-4410-a351-79fd9c8422c2" x="80" y="26" width="152" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[StringUtils.abbreviate($F{slug} == null ? "" : $F{slug}, 35)]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="33d15da6-01b7-4ebf-b6c3-dcb8efdb77a5" x="233" y="26" width="63" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[StringUtils.abbreviate($F{origin} == null ? "" : $F{origin}, 11)]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="2f6bbd80-5053-460d-a2c3-3c94b6b4f865" x="297" y="26" width="77" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[StringUtils.abbreviate($F{event.name} == null ? "" : $F{event.name}, 10)]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="96a06f0b-2d95-4751-bed2-e7f7038d82a6" x="375" y="26" width="76" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[StringUtils.abbreviate($F{nature.name} == null ? "" : $F{nature.name}, 10)]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="5552d799-5a92-4f0c-b301-cd3a1ed7c046" x="452" y="26" width="111" height="18"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[StringUtils.abbreviate($F{author.nickname} == null ? "" : $F{author.nickname}, 15)]]></textFieldExpression>
			</textField>
			<rectangle>
				<reportElement uuid="8ce42181-b02a-4024-a40c-1d1550041214" x="0" y="0" width="567" height="24" forecolor="#999999" backcolor="#999999"/>
			</rectangle>
			<textField isBlankWhenNull="true">
				<reportElement uuid="024952dd-3ed6-4498-8433-ef3c2e4ef0e8" x="80" y="1" width="152" height="22" forecolor="#FFFFFF"/>
				<box leftPadding="0"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.slug")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="f4a84bbe-8e96-48e5-8369-3e5d6dc4c285" x="233" y="1" width="63" height="22" forecolor="#FFFFFF"/>
				<box leftPadding="0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.origin")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="79b03c65-eabb-40c9-b783-61f801d6b4a3" x="297" y="1" width="77" height="22" forecolor="#FFFFFF"/>
				<box leftPadding="0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.event")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="5581e792-5ea5-412c-a743-19ad6b748f4a" x="375" y="1" width="77" height="22" forecolor="#FFFFFF"/>
				<box leftPadding="0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.nature")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="000ed81b-e44e-41c5-8f68-16fc569a0ebd" x="452" y="1" width="111" height="22" forecolor="#FFFFFF"/>
				<box leftPadding="0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.author")]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement uuid="accea686-2117-44dd-8b2d-fc8374640704" x="3" y="1" width="77" height="22" forecolor="#FFFFFF"/>
				<box leftPadding="0"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.date")]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band height="28">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement uuid="5ce03a20-8ebb-44d8-a93a-410950c67ff5" x="3" y="4" width="560" height="18" isPrintWhenDetailOverflows="true"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font fontName="Arial" size="12"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{content}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="26">
			<textField evaluationTime="Report">
				<reportElement uuid="845c14e8-c579-44a6-bb94-2888598ee292" x="543" y="6" width="20" height="20"/>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($V{PAGE_NUMBER}.intValue() < 10 ? "00" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.intValue() > 10 && $V{PAGE_NUMBER}.intValue() < 100 ? "0" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.toString())]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="092dfccf-678b-4394-b789-cc4168b1336e" x="380" y="6" width="135" height="20"/>
				<textElement textAlignment="Right" verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[new String($V{PAGE_NUMBER}.intValue() < 10 ? "00" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.intValue() > 10 && $V{PAGE_NUMBER}.intValue() < 100 ? "0" + $V{PAGE_NUMBER}.toString() : $V{PAGE_NUMBER}.toString()) + " " + $R{footer.of} +" "]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd/MM/yyyy &apos;às&apos; HH:mm:ss" isBlankWhenNull="true">
				<reportElement uuid="c814e82b-57e2-47f1-aa18-876bd8f04696" x="5" y="11" width="334" height="15" isPrintInFirstWholeBand="true"/>
				<box leftPadding="2">
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement verticalAlignment="Bottom">
					<font fontName="Arial" size="10"/>
				</textElement>
				<textFieldExpression><![CDATA[new java.text.SimpleDateFormat($P{bundle}.getMessage("defaults.dateTimeFormat")).format(new Date())]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="d709c45a-5ef6-4adf-ad2b-4ee93b672935" x="517" y="6" width="24" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Bottom">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("defaults.of")]]></textFieldExpression>
			</textField>
			<line>
				<reportElement uuid="bbd927e1-4759-4963-b531-9d60af953d7b" x="0" y="2" width="567" height="1" forecolor="#666666"/>
				<graphicElement>
					<pen lineStyle="Solid" lineColor="#666666"/>
				</graphicElement>
			</line>
			<break>
				<reportElement uuid="ef52fcb0-5821-4fa1-8752-01f70ddd4a6e" x="0" y="25" width="567" height="1"/>
			</break>
		</band>
	</pageFooter>
	<noData>
		<band height="20">
			<textField isBlankWhenNull="true">
				<reportElement uuid="6d73a8dc-9626-44c3-b674-00a309d0f3df" x="5" y="0" width="555" height="20"/>
				<box leftPadding="0">
					<topPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Arial" size="12" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{bundle}.getMessage("report.isEmpty")]]></textFieldExpression>
			</textField>
		</band>
	</noData>
</jasperReport>
