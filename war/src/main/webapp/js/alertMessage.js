var msgTmp = '';
var interval = '';
var titleTmp = '';
var counter = 0;

function focusMovie(movieName) {
	if (navigator.appName.indexOf("Microsoft") != -1) {
		window[movieName].focus();
	} else {
		window.document[movieName].focus();
	}
}

function delegate(that, thatMethod) {
	return function() {
		return thatMethod.call(that);
	};
}

function newMSG(msg) {
	console.debug("newMSG");
	window.msgTmp = msg;
	if (interval != '') window.clearInterval(interval);
	interval = window.setInterval(delegate(window, window.changeTitle), 1500);
}

function windowIsActived() {
	return $("#windowActived").val();
}

function changeTitle() {
	if (counter >= 10) {
		clearInterval(interval);
		counter = 0;
		window.document.title = titleTmp;
	} else if (counter % 2) {
		window.document.title = 'Nova mensagem';
	} else {
		window.document.title = window.msgTmp;
	}
	counter++;
}