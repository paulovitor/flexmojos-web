package tv.snews.anews;

/**
 * Classe utilitária que provê informações sobre a versão corrente do sistema.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public final class VersionInfo {

	/**
	 * Versão atual do sistema. Esse valor deve ser atualizado sempre que uma 
	 * nova release for lançada.
	 */
	public static final String VERSION = "1.0.0-SNAPSHOT"; // NOTE Tem como automatizar isso?
	
	/**
	 * Data na qual a versão foi lançada. Deve ser atualizada sempre que uma 
	 * nova release for lançada.
	 */
	public static final String DATE_OF_MANUFACTURE = "31/07/2012"; // NOTE Tem como automatizar isso?
	
	private VersionInfo() { }
}
