package tv.snews.anews.report;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.ScriptPlan;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;
import static tv.snews.anews.util.DateTimeUtil.sumTimes;

/**
 * @author Felipe Pinheiro
 */
public class ScriptPlanReportData {

	private final ScriptPlan scriptPlan;
	private final SortedSet<ScriptBlockReportData> blocksData = new TreeSet<>();

	private Date totalCommercial = getMidnightTime();
	private Date totalProduction = getMidnightTime();
	private Date total;

	public ScriptPlanReportData(ScriptPlan scriptPlan, boolean onlyApproved) {
		this.scriptPlan = scriptPlan;

		populate(onlyApproved);
		calculate();
	}

	private void populate(boolean onlyApproved) {
		for (ScriptBlock block : scriptPlan.getBlocks()) {
			ScriptBlockReportData blockData = new ScriptBlockReportData(block, onlyApproved);
			if (!blockData.isEmpty()) { // tem scripts a serem exibidos no relatório?
				blocksData.add(blockData);
			}
		}
	}

	private void calculate() {
		for (ScriptBlockReportData blockData : blocksData) {
			totalCommercial = sumTimes(totalCommercial, blockData.getCommercial());
			totalProduction = sumTimes(totalProduction, blockData.getTotalProduction());
		}
		total = sumTimes(totalCommercial, totalProduction);
	}

	public boolean isEmpty() {
		return blocksData.isEmpty();
	}

	//------------------------------
	//	Getters
	//------------------------------

	public ScriptPlan getScriptPlan() {
		return scriptPlan;
	}

	public SortedSet<ScriptBlockReportData> getBlocksData() {
		return blocksData;
	}

	public Date getTotalCommercial() {
		return totalCommercial;
	}

	public Date getTotalProduction() {
		return totalProduction;
	}

	public Date getTotal() {
		return total;
	}

	//------------------------------
	//	Script Plan Facade Getters
	//------------------------------

	public Date getDate() {
		return scriptPlan.getDate();
	}

	public String getSlug() {
		return scriptPlan.getSlug();
	}

	public String getChannel() {
		return scriptPlan.getChannel();
	}

	public Program getProgram() {
		return scriptPlan.getProgram();
	}

	public Date getStart() {
		return scriptPlan.getStart();
	}

	public Date getEnd() {
		return scriptPlan.getEnd();
	}

	public Date getProductionLimit() {
		return scriptPlan.getProductionLimit();
	}
}
