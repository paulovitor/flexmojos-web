package tv.snews.anews.report;

import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptBlock;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;
import static tv.snews.anews.util.DateTimeUtil.sumTimes;

/**
 * @author Felipe Pinheiro
 */
public class ScriptBlockReportData implements Comparable<ScriptBlockReportData> {

	private final ScriptBlock scriptBlock;
	private final SortedSet<Script> scripts = new TreeSet<>();

	private Date totalProduction = getMidnightTime();
	private Date total;

	public ScriptBlockReportData(ScriptBlock scriptBlock, boolean onlyApproved) {
		this.scriptBlock = scriptBlock;

		populate(onlyApproved);
		calculate();
	}

	private void populate(boolean onlyApproved) {
		for (Script script : scriptBlock.getScripts()) {
			if (!onlyApproved || script.isApproved()) {
				scripts.add(script);
			}
		}
	}

	private void calculate() {
		for (Script script : scripts) {
			totalProduction = sumTimes(totalProduction, script.getVt());
		}
		total = sumTimes(totalProduction, scriptBlock.getCommercial());
	}

	public boolean isEmpty() {
		return scripts.isEmpty();
	}

	@Override
	public int compareTo(ScriptBlockReportData other) {
		return scriptBlock.compareTo(other.scriptBlock);
	}

	//------------------------------
	//	Getters
	//------------------------------

	public ScriptBlock getScriptBlock() {
		return scriptBlock;
	}

	public SortedSet<Script> getScripts() {
		return scripts;
	}

	public Date getTotalProduction() {
		return totalProduction;
	}

	public Date getTotal() {
		return total;
	}

	//------------------------------
	//	Script Block Facade Getters
	//------------------------------

	public String getSlug() {
		return scriptBlock.getSlug();
	}

	public boolean isStandBy() {
		return scriptBlock.isStandBy();
	}

	public Date getCommercial() {
		return scriptBlock.getCommercial();
	}
}
