package tv.snews.anews.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Created by samuel on 16/07/14.
 */
public class ThumbnailGenerator {

    private ExecutorService exService = Executors.newFixedThreadPool(15);
    private BlockingDeque<ThumbnailItem> pipeline = new LinkedBlockingDeque<>();
    private ThumbnailConsumer consumer = new ThumbnailConsumer();

    public ThumbnailGenerator() {
        exService.execute(consumer);
    }

    public void generate(String filePath, String thumbnailPath, int maxWidth, int maxHeight) {
        pipeline.add(new ThumbnailItem(filePath, thumbnailPath, maxWidth, maxHeight));
    }

    class ThumbnailItem {

        private String filePath;
        private String thumbnailPath;
        private int maxWidth;
        private int maxHeight;

        public ThumbnailItem(String filePath, String thumbnailPath, int maxWidth, int maxHeight) {
            this.filePath = filePath;
            this.thumbnailPath = thumbnailPath;
            this.maxWidth = maxWidth;
            this.maxHeight = maxHeight;
        }

        public String getFilePath() {
            return filePath;
        }

        public String getThumbnailPath() {
            return thumbnailPath;
        }

        public int getMaxWidth() {
            return maxWidth;
        }

        public int getMaxHeight() {
            return maxHeight;
        }
    }

    class ThumbnailConsumer implements Runnable {

        @Override
        public void run() {
            try {
                while (true) {
                    ThumbnailItem thumbnailItem = pipeline.take();
                    generateThumbnail(thumbnailItem.getFilePath(), thumbnailItem.getThumbnailPath(), thumbnailItem.getMaxWidth(), thumbnailItem.getMaxHeight());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        private void generateThumbnail(final String filePath, final String thumbnailPath, final Integer maxWidth, final Integer maxHeight) throws IOException {
            //Gerando Thumbnail
            File thumbnail = generateNewFile(thumbnailPath);
            BufferedImage img = new BufferedImage(maxWidth, maxHeight, BufferedImage.TYPE_INT_RGB);
            img.createGraphics().drawImage(ImageIO.read(new File(filePath)).getScaledInstance(maxWidth, maxHeight, Image.SCALE_SMOOTH), 0, 0, Color.WHITE, null);
            ImageIO.write(img, "jpg", thumbnail);
            img.flush();
        }

        private File generateNewFile(String newImgPath) throws IOException {
            File newImgFile = new File(newImgPath);
            // Verifica se o diretório existe e, se não existir, tenta criá-lo
            if (!newImgFile.exists() && !newImgFile.mkdirs() && !newImgFile.canWrite()) {
                throw new IOException(ResourceBundle.INSTANCE.getMessage("storage.storageCreationError", newImgFile.getPath()));
            }
            if(!newImgFile.isFile()) {
                newImgFile.createNewFile();
            }
            return newImgFile;
        }
    }

}
