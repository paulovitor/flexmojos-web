package tv.snews.anews.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.EncryptedValueDao;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Faz a contagem do tempo em que o aplicativo já está em execução e faz o
 * controle do período de trial do software. Essa contagem será persistida no
 * banco de dados na tabela "value", para que ela seja mantida mesmo que o
 * sistema seja interrompido.
 * <p>
 * Posteriormente essa classe deve ser adaptada para um controle offline da
 * licença do software (isso para quando o controle online estiver indisponível
 * por qualquer causa).
 * </p>
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public final class UptimeCounterImpl implements Runnable, UptimeCounter {

	private static final String CHARSET = "UTF-8";

	private static final int INTERVAL = 5; // minutos
	
	private final Logger log = LoggerFactory.getLogger(UptimeCounterImpl.class);
	private final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	
	private final PasswordEncryptor encryptor = new PasswordEncryptor();
	private final EncryptedValueDao encryptedValueDao;
	
    public UptimeCounterImpl(EncryptedValueDao encryptedValueDao) {
    	this.encryptedValueDao = encryptedValueDao;
    }
	
	@Override
	@Transactional
	public void run() {
		try {
			incrementCounter();
		} catch (UnsupportedEncodingException e) {
			throw new IllegalArgumentException("Unsupported charset: " + CHARSET, e);
        }
	}
	
	private synchronized void incrementCounter() throws UnsupportedEncodingException {
		EncryptedValue value = getEncryptedValue();
		
		Integer count = incInt(parseInt(encryptor.decrypt(value, CHARSET)));
		EncryptedValue newValue = encryptor.encrypt(count.toString(), CHARSET);
		encryptedValueDao.update(value.copy(newValue));
	}
	
	@Override
    public synchronized long uptime(TimeUnit timeUnit) {
		try {
			EncryptedValue value = getEncryptedValue();
	        Integer count = parseInt(encryptor.decrypt(value, CHARSET));
	        
	        switch (timeUnit) {
	        	case DAYS:         return ((count * INTERVAL) / 60) / 24;
	        	case HOURS:        return  (count * INTERVAL) / 60;
	        	case MINUTES:      return   count * INTERVAL;
	        	case SECONDS:      return  (count * INTERVAL) * 60;
	        	case MILLISECONDS: return ((count * INTERVAL) * 60) * 1000;
        		default: throw new IllegalArgumentException("TimeUnit value not supported: " + timeUnit);
	        }
        } catch (UnsupportedEncodingException e) {
        	throw new IllegalArgumentException("Unsupported charset: " + CHARSET, e);
        }
	}
	
	private EncryptedValue getEncryptedValue() {
		EncryptedValue value = encryptedValueDao.findFirst();
		if (value == null) {
			throw new IllegalStateException("Unknow uptime counter value");
		}
		return value;
	}
	
	public void start() {
    	executor.scheduleAtFixedRate(this, INTERVAL, INTERVAL, MINUTES);
    }

    public void stop() {
    	if (executor != null && !executor.isShutdown()) {
    		executor.shutdown();
			try {
				// Espera um pouco para o executor encerrar
				if (!executor.awaitTermination(10, SECONDS)) {
					executor.shutdownNow();
					// Espera um pouco para o executor responder pelo cancelamento
					if (!executor.awaitTermination(10, SECONDS)) {
						log.error("Scheduled thread pool did not terminated.");
					}
				}
			} catch (InterruptedException e) {
				// (Re-)Cancela se a thread corrente também for interrompida
				executor.shutdownNow();
				// Preserva o status de interrompida
				Thread.currentThread().interrupt();
			}
		}
    }
    
    @Override
    protected void finalize() throws Throwable {
        if (!executor.isShutdown()) {
        	log.warn("ExecutorService still active on the finalize proccess of this object");
        	executor.shutdownNow();
        }
    }
    
    private Integer incInt(Integer value) {
    	return value++ == Integer.MAX_VALUE ? value-- : value;
    }
}
