package tv.snews.anews.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.spec.KeySpec;

/**
 * Classe utilizada para criptografar e descriptografar valores.
 * 
 * @see http://stackoverflow.com/questions/992019/java-256-bit-aes-password-based-encryption
 * 
 * @author Felipe Pinheiro
 * @since 1.2?
 */
public final class PasswordEncryptor {

	private static final Logger log = LoggerFactory.getLogger(PasswordEncryptor.class);
	
	private static final byte[] salt = {
        (byte) 0xd8, (byte) 0x73, (byte) 0x7e, (byte) 0x8c,
        (byte) 0x41, (byte) 0xc8, (byte) 0xee, (byte) 0x89
    };
	
	private static final char[] defaultPassword = { 
		35, 111, 227, 112, 105, 108, 101, 70, 33, 112, 97, 90, 36, 115, 110, 
		105, 107, 110, 101, 74, 38, 111, 104, 110, 105, 108, 117, 97, 80, 
		40, 114, 101, 122, 101, 105, 108, 69 
	};
	
	private static final int interationCount = 1024;
	private static final int keyLength = 128; // deveria ser 256
	
	private SecretKey secretKey;
	private Cipher cipher;
	
    public PasswordEncryptor() {
	    this(defaultPassword);
    }
	
	public PasswordEncryptor(String password) {
		this(password.toCharArray());
	}
	
    public PasswordEncryptor(char[] password) {
        try {
        	SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        	KeySpec spec = new PBEKeySpec(password, salt, interationCount, keyLength);
            SecretKey tmp = factory.generateSecret(spec);
            
            secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (Exception e) {
        	log.error("Failed to initialize the encryption object", e);
        	throw new RuntimeException(e);
        }
    }
	
    public final EncryptedValue encrypt(String value, String charsetName) throws UnsupportedEncodingException {
    	return encrypt(value.getBytes(charsetName));
    }
    
	public final EncryptedValue encrypt(byte[] value) {
		try {
	        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
	        return new EncryptedValue(
	        	cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV(),
	        	cipher.doFinal(value)
	        );
        } catch (Exception e) {
        	log.error("Failed to encrypt value", e);
        	throw new RuntimeException(e);
        }
	}
	
	public final String decrypt(EncryptedValue value, String charsetName) throws UnsupportedEncodingException {
		return new String(decrypt(value), charsetName);
	}
	
	public final byte[] decrypt(EncryptedValue value) {
		try {
	        cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(value.getIv()));
	        return cipher.doFinal(value.getCipherText());
        } catch (Exception e) {
        	log.error("Failed to decrypt value", e);
        	throw new RuntimeException(e);
        }
	}
}
