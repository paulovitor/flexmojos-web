package tv.snews.anews.util;

import org.xml.sax.SAXException;
import tv.snews.anews.exception.ValidatorErrorHandler;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.bind.util.JAXBSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;

public class JAXBUtil {

    /**
     * Converte o objeto em uma estrutura XML.
     * @param object objeto a ser convertido em XML.
     * @param fileName nome do arquivo XML a ser gerado.
     * @return uma string com o conteudo do XML gerado.
     */
    public static String marshalToFile(Object object, String fileName) {
        final StringWriter out = new StringWriter();
        JAXBContext context = null;
        Writer writer = null;
        try {
            context = JAXBContext.newInstance(object.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(object, new StreamResult(out));
            writer = new FileWriter(fileName);
            marshaller.marshal(object, writer);
        } catch (PropertyException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
 
        return out.toString();
    }
	
    /**
     * Realiza a conversao (unmarshal) de um arquivo XML em um objeto do seu tipo.
     * @param clazz classe referente ao objeto a ser criado a partir do XML.
     * @param fileXml nome do arquivo XML a ser convertido em objeto.
     * @return novo objeto.
     */
    public static Object unmarshalFromFile(Class<?> clazz, String fileXml) {
        try {
        	JAXBContext context = JAXBContext.newInstance(clazz);
        	Unmarshaller unmarshaller = context.createUnmarshaller();
            return unmarshaller.unmarshal(new FileInputStream(fileXml));
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Realiza a validacão do objeto com o arquivo XSD.
     * @param xsdFile nome do arquivo XSD.
     * @param object objeto a ser validado.
     */
    public static void validator(String xsdFile, Object object) {
        try {
 
            JAXBContext jc = JAXBContext.newInstance(object.getClass());
            JAXBSource source = new JAXBSource(jc, object);
 
            SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = sf.newSchema(new File(xsdFile));
 
            Validator validator = schema.newValidator();
            validator.setErrorHandler(new ValidatorErrorHandler());
            validator.validate(source);
 
            System.out.println("Successful validation!");
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
