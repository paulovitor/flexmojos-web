package tv.snews.anews.util;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Implementa um "fowarding" (Joshua Bloch) para uma instância de {@link java.util.Map},
 * criando uma composição ao invés de herdar uma implementação de Map qualquer.
 * Cada método desta classe invoca o método correspondente do  Map. Isso deixa
 * a classe sem dependências com os detalhes da implementação do Map.
 *
 * @param <K>
 * @param <V>
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FowardingMap<K, V> implements Map<K, V> {

	private final Map<K, V> map;

	public FowardingMap(Map<K, V> map) {
		this.map = map;
	}

    @Override
    public void clear() {
	    map.clear();
    }

    @Override
    public boolean containsKey(Object key) {
	    return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
	    return map.containsValue(value);
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
	    return map.entrySet();
    }

    @Override
    public V get(Object key) {
	    return map.get(key);
    }

    @Override
    public boolean isEmpty() {
	    return map.isEmpty();
    }

    @Override
    public Set<K> keySet() {
	    return map.keySet();
    }

    @Override
    public V put(K key, V value) {
	    return map.put(key, value);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
	    map.putAll(m);
    }

    @Override
    public V remove(Object key) {
	    return map.remove(key);
    }

    @Override
    public int size() {
	    return map.size();
    }

    @Override
    public Collection<V> values() {
	    return map.values();
    }
}
