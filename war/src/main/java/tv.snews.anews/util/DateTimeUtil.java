package tv.snews.anews.util;

import org.joda.time.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Eliezer Reis
 * @since 1.0.0
 */
public final class DateTimeUtil {
	
	/**
	 * TODO Javadoc explicando essa constânte.
	 */
	public static final long TIME_ZONE;

	static {
		Calendar calendar = Calendar.getInstance();
		TIME_ZONE = (calendar.get(Calendar.ZONE_OFFSET) + calendar.get(Calendar.DST_OFFSET)) * -1;
	}

	private DateTimeUtil() { }
	
	/**
	 * Faz a soma de todos os tempos informados e retorna o total.
	 * 
	 * @param times
	 * @return
	 */
	public static synchronized Date sumTimes(Date... times) {
		LocalTime sum = LocalTime.MIDNIGHT;
		for (Date timeAsDate : times) {
			sum = sum.plusMillis(new LocalTime(roundToSecond(timeAsDate)).getMillisOfDay());
		}

		DateTimeZone dateTimeZone = DateTimeZone.getDefault();
		LocalDate localDate = LocalDate.now();
		LocalDateTime localDateTime = localDate.toLocalDateTime(sum);

		if (dateTimeZone.isLocalDateTimeGap(localDateTime) && sum.getHourOfDay() == 0) {
			localDateTime = localDateTime.withDayOfMonth(localDateTime.getDayOfMonth() + 1);
		}

		return localDateTime.toDate();
	}
	
	public static Date addTimeToCurrentDate(Date time) {
		DateTimeZone dateTimeZone = DateTimeZone.getDefault();

		LocalDate localDate = LocalDate.now();
		LocalTime localTime = new LocalTime(time);
		LocalDateTime localDateTime = localDate.toLocalDateTime(localTime);

		// Se a data de hoje for o primeiro dia do horário de verão à meia-noite, tem que somar um dia porque não existe a hora "meia-noite" nesse dia.
		if (dateTimeZone.isLocalDateTimeGap(localDateTime) && localTime.getHourOfDay() == 0) {
			localDateTime = localDateTime.withDayOfMonth(localDateTime.getDayOfMonth() + 1);
		}

		return localDateTime.toDate();
	}
	
	public static Date minimizeTime(Date date) {
		DateTimeZone dateTimeZone = DateTimeZone.getDefault();

		LocalDate localDate = new LocalDate(date);
		LocalDateTime localDateTime = localDate.toLocalDateTime(LocalTime.MIDNIGHT);

		// Se a data de hoje for o primeiro dia do horário de verão, tem que somar uma hora porque não existe a hora "meia-noite" nesse dia.
		if (dateTimeZone.isLocalDateTimeGap(localDateTime)) {
			localDateTime = localDateTime.withHourOfDay(1);
		}

		return localDateTime.toDate();
	}

	public static Date minimizeTime() {
		return LocalTime.MIDNIGHT.toDateTimeToday().toDate();
	}

	// FIXME Esse nome não tem nada a ver com o que o método faz (renomear aqui e no relatório)
	public static boolean minimizedTimeToStoryReport(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		Calendar minimized = Calendar.getInstance();
		minimized.setTime(minimizeTime());
		
		return cal.get(Calendar.HOUR_OF_DAY) != minimized.get(Calendar.HOUR_OF_DAY) || 
			   cal.get(Calendar.MINUTE) != minimized.get(Calendar.MINUTE) || 
			   cal.get(Calendar.SECOND) != minimized.get(Calendar.SECOND); 
	}
	
	public static Date maximizeTime(Date date) {
		LocalDate localDate = new LocalDate(date);
		LocalTime localTime = new LocalTime(23, 59, 59, 999);
		return localDate.toDateTime(localTime).toDate();
	}
	
	public static Date millisToTime(int millis) {
		DateTimeZone dateTimeZone = DateTimeZone.getDefault();
		LocalDateTime localDateTime = LocalDate.now().toLocalDateTime(LocalTime.MIDNIGHT);

		localDateTime = localDateTime.plusMillis(millis);

		// Se a data de hoje for o primeiro dia do horário de verão, tem que somar um dia porque não existe a hora "meia-noite" nesse dia.
		if (dateTimeZone.isLocalDateTimeGap(localDateTime)) {
			localDateTime = localDateTime.withDayOfMonth(localDateTime.getDayOfMonth() + 1);
		}

		return localDateTime.toDate();
	}
	
	/**
	 * Arredondamento de segundos de uma data, arredonda os milissegundos
	 * de uma data se acima de 500 milissegundos para cima se a baixo de 500
	 * para baixo.
	 * 
	 * @param date
	 * @return Date
	 */
	public static Date roundToSecond(Date date){
		if (date == null) {
			return getMidnightTime();
		}
	    Calendar calendar = new GregorianCalendar();
	    calendar.setTime(date);
	    calendar.set(Calendar.SECOND, calendar.get(Calendar.SECOND) + calendar.get(Calendar.MILLISECOND)/500);
	    calendar.set(Calendar.MILLISECOND, 0);
	    return calendar.getTime();
	}
	
	public static Date getCurrentTime() {
		return DateTime.now().toDate();
	}
	
	public static Date getMidnightTime() {
		DateTimeZone dateTimeZone = DateTimeZone.getDefault();
		LocalDateTime localDateTime = LocalDate.now().toLocalDateTime(LocalTime.MIDNIGHT);

		// Se a data de hoje for o primeiro dia do horário de verão, tem que somar um dia porque não existe a hora "meia-noite" nesse dia.
		if (dateTimeZone.isLocalDateTimeGap(localDateTime)) {
			localDateTime = localDateTime.withDayOfMonth(localDateTime.getDayOfMonth() + 1);
		}

		return localDateTime.toDate();
	}

	/**
	 * Retorna uma string formatada no padrão definido
	 * no bundle 
	 * 		PT-BR: dd/MM/yyyy
	 * 	
	 * @param date
	 * @return
	 */
	public static String dateToDateFormat(Date date) {
		return new SimpleDateFormat(ResourceBundle.INSTANCE.getMessage("defaults.dateFormat")).format(date == null ? new Date() : date);
	}
	
	/**
	 * Retorna uma string formatada no padrão definido
	 * no bundle 
	 * 		PT-BR:  d MMM yyyy - HH:mm:ss
	 * 	
	 * @param date
	 * @return
	 */
	public static String dateToDateTimeFormat(Date date) {
		return new SimpleDateFormat(ResourceBundle.INSTANCE.getMessage("defaults.dateTimeFormat")).format(date == null ? new Date() : date);
	}
	
	/**
	 * Retorna uma string formatada no padrão definido
	 * no bundle 
	 * 		PT-BR: HH:mm:ss
	 * 
	 * @param date
	 * @return
	 */
	public static String dateToTimeFormat(Date date) {
		return new SimpleDateFormat(ResourceBundle.INSTANCE.getMessage("defaults.timeFormat")).format(date == null ? new Date() : date);
	}
	
	public static String dateToPatternFormat(Date date, String pattern) {
		return new SimpleDateFormat(pattern != null ? pattern : ResourceBundle.INSTANCE.getMessage("defaults.timeFormat")).format(date == null ? new Date() : date);
	}

	public static Date localTimeToDate(LocalTime time) {
		if (time == null) {
			return null;
		}

		DateTimeZone dateTimeZone = DateTimeZone.getDefault();

		LocalDate localDate = LocalDate.now();
		LocalDateTime localDateTime = localDate.toLocalDateTime(time);

		// Se a data de hoje for o primeiro dia do horário de verão, tem que somar um dia porque não existe a hora "meia-noite" nesse dia.
		if (dateTimeZone.isLocalDateTimeGap(localDateTime)) {
			localDateTime = localDateTime.withDayOfMonth(localDateTime.getDayOfMonth() + 1);
		}

		return localDateTime.toDate();
	}

	public static Date dateTimeToDate(DateTime dateTime) {
		return dateTime == null ? null : dateTime.toDate();
	}

	public static DateTime dateToDateTime(Date date) {
		return date == null ? null : new DateTime(date);
	}

	public static Date timeStringToDate(String time) {
		LocalTime localTime = LocalTime.parse(time);

		DateTimeZone dateTimeZone = DateTimeZone.getDefault();

		LocalDate localDate = LocalDate.now();
		LocalDateTime localDateTime = localDate.toLocalDateTime(localTime);

		// Se a data de hoje for o primeiro dia do horário de verão, tem que somar um dia porque não existe a hora "meia-noite" nesse dia.
		if (dateTimeZone.isLocalDateTimeGap(localDateTime)) {
			localDateTime = localDateTime.withDayOfMonth(localDateTime.getDayOfMonth() + 1);
		}

		return localDateTime.toDate();
	}
}
