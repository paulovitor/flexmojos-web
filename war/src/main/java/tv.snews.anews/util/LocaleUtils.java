package tv.snews.anews.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

/**
 * Classe utilitária que fornece métodos para auxiliar a manipulação de locales.
 * 
 * @author Eliezer Reis
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public final class LocaleUtils {

	private static final Logger log = LoggerFactory.getLogger(LocaleUtils.class);

	/**
	 * Construtor privado. Não deve ser usado.
	 */
	private LocaleUtils() {
		throw new AssertionError();
	}

	/**
	 * Converte uma string que representa um locale para um objeto
	 * {@link java.util.Locale} válido. A string deve conter a abreviação do idioma e do
	 * país, separados por um underscore. Exemplos:
	 *
	 * <ul>
	 * <li><code>en_US</code></li>
	 * <li><code>pt_BR</code></li>
	 * <li><code>pt_PT</code></li>
	 * </ul>
	 *
	 * <p>
	 * Caso a string do locale for inválida, será retornado o locale padrão
	 * <code>pt_BR</code>.
	 * </p>
	 *
	 * @param name String representando o locale.
	 * @return Objeto {@link java.util.Locale}.
	 */
	public static Locale getLocaleFromString(String name) {
		Locale locale = null;
		String[] tokens = name.split("_");
		if (tokens.length == 2) {
			try {
				locale = new Locale(tokens[0], tokens[1]);
			} catch (Exception e) {
				log.warn("Locale desconhecido '{}'. Será utilizado o locale padrão 'pt_BR'.", name);
				locale = new Locale("pt", "BR");
			}
		} else {
			log.warn("Locale em formato inválido '{}'. Será utilizado o locale padrão 'pt_BR'.", name);
			locale = new Locale("pt", "BR");
		}
		return locale;
	}
}
