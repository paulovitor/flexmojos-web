package tv.snews.anews.util;

import com.sun.xml.xsom.*;
import com.sun.xml.xsom.parser.XSOMParser;
import org.xml.sax.SAXException;

import java.util.*;


/**
 * Classe responsável por gerar map a partir de um schema xml dinâmicamente.
 * 
 * @author Samuel Guedes de Melo.
 *
 */
public class ParseXmlToMap {
	
	/**
	 * Método que retorna o mapa de atributos e tipos do xsd.
	 * 
	 * @param pathFileXSD
	 * @return Map<String, Class<?>>
	 */
	public static List<Map<String, String>> getMapFromXsd(String pathFileXSD) {
		List<Map<String, String>> props = new ArrayList<>();
		try {
			XSOMParser parser = new XSOMParser();
	        parser.parse(pathFileXSD);
			XSSchemaSet result = parser.getResult();
			XSSchema schema = result.getSchema(1);
			Iterator <XSElementDecl> itrElements = schema.iterateElementDecls();
			  
			while(itrElements.hasNext()) { 
				XSElementDecl xsElementDecl = (XSElementDecl) itrElements.next();
				XSComplexType xsComplexType = xsElementDecl.getType().asComplexType();
				if (xsComplexType != null) {
					XSContentType xsContentType = xsComplexType.getContentType();
					XSParticle particle = xsContentType.asParticle();
					getOptionalElements(props,  particle);
				}
			}

        } catch (SAXException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
		return props;
	}
	
	/**
	 * Metodo recursivo para iterar o ComplexType.
	 * 
	 * @param list
	 * @param xsParticle
	 */
	private static void getOptionalElements(List<Map<String, String>> list, XSParticle xsParticle) {		  
		if(xsParticle != null){	        
		   XSTerm pterm = xsParticle.getTerm();
		   if(pterm.isElementDecl()) { 
			   if(xsParticle.getMinOccurs().intValue() == 0) {
				   Map<String, String> map = new HashMap<>();
				   map.put(pterm.asElementDecl().getName(), pterm.asElementDecl().getType().getName());
				   list.add(map);
			   }
			   Map<String, String> map = new HashMap<>();
			   map.put(pterm.asElementDecl().getName(), pterm.asElementDecl().getType().getName());
			   list.add(map);
    
			   XSComplexType xsComplexType =  (pterm.asElementDecl()).getType().asComplexType();

			   if (xsComplexType != null && !(pterm.asElementDecl().getType()).toString().contains("Enumeration")) {
				   XSContentType xsContentType = xsComplexType.getContentType();
				   XSParticle xsParticleInside = xsContentType.asParticle();
				   getOptionalElements(list, xsParticleInside);
			   }
		   } else if(pterm.isModelGroup()) {  
				XSModelGroup xsModelGroup2 = pterm.asModelGroup();
				XSParticle[] xsParticleArray = xsModelGroup2.getChildren();
				for(XSParticle xsParticleTemp : xsParticleArray ){
				  getOptionalElements(list, xsParticleTemp);
				}
		   }
		}
	}
}
