package tv.snews.anews.util;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/**
 * Um {@link java.net.URLStreamHandler} que trata resources no classpath.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ClasspathUrlHandler extends URLStreamHandler {

	// ClassLoader para encontrar os resources
	private final ClassLoader classLoader;
	
    public ClasspathUrlHandler() {
    	this(Thread.currentThread().getContextClassLoader());
    }
	
    public ClasspathUrlHandler(ClassLoader classLoader) {
    	this.classLoader = classLoader;
    }
    
    @Override
    protected URLConnection openConnection(URL url) throws IOException {
    	final URL resourceUrl = classLoader.getResource(url.getPath());
        return resourceUrl.openConnection();
    }
}
