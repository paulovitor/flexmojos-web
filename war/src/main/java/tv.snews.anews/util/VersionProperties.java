package tv.snews.anews.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * Atualiza a vesão do sistema na interface de view.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public final class VersionProperties {

	private static final Logger log = LoggerFactory.getLogger(VersionProperties.class);
	
    private VersionProperties() {}
	
	/**
	 * Carrega arquivo de properties da versão.
     * @return propertiesVersion instância do properties de versão.
     */
    public static String loadVersion() {
		try {
			ClassLoader loader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = loader.getResourceAsStream("/version.properties");
			if (inputStream == null) {
				return null;
			}

			Properties properties = new Properties();
			properties.load(inputStream);
			return properties.getProperty("version");
        } catch (IOException ioe) {
	        log.error(ioe.getMessage(), ioe);
	        return null;
        }
    }
}
