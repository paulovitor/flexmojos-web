package tv.snews.anews.util;

import java.util.concurrent.TimeUnit;

/**
 * @author Felipe Pinheiro
 * @since 1.2
 */
public interface UptimeCounter {

	/**
	 * Retorna o período de tempo em que o software já está em execução desde a
	 * sua instalação.
	 * 
	 * @param timeUnit unidade de tempo em que deseja o retorno
	 * @return o período de execução do software desde a instalação
	 */
	long uptime(TimeUnit timeUnit);

}
