package tv.snews.anews.util;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public enum FontSize {

	TEN, TWELVE, FOURTEEN, SIXTEEN, EIGHTEEN, TWENTY;

	public static FontSize parse(int value) {
		switch (value) {
			case 10: return TEN;
			case 12: return TWELVE;
			case 14: return FOURTEEN;
			case 16: return SIXTEEN;
			case 18: return EIGHTEEN;
			case 20: return TWENTY;

			default:
				throw new IllegalArgumentException("Not font size not supported: " + value);
		}
	}
}
