package tv.snews.anews.util;

import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.exception.DataNotFoundException;

import java.io.File;
import java.io.IOException;

/**
 * @author Eliezer Reis
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class StorageUtil {

	private static final String TEMPORARY_DIR = "temp";
	private static final String USERS_FILES_DIR = "userfiles";
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private SettingsDao settingsDao;
	
	public static String anewsDirectory() {
		String slash = System.getProperty("file.separator");
		return ".anews" + slash;
	}
	
	public File getStorageDirectory() throws DataNotFoundException, IOException {
		Settings settings = settingsDao.loadSettings();
		if (settings == null) {
			throw new DataNotFoundException(bundle.getMessage("settings.notFound"));
		}
		
		File storage = new File(settings.getStorageLocation());
		
		// Verifica se o diretório existe e, se não existir, tenta criá-lo
		if (!storage.exists() && !storage.mkdirs()) {
			throw new IOException(bundle.getMessage("storage.storageCreationError", settings.getStorageLocation()));
		}
		
		// Verifica se tem permissão de escrita no diretório
		if (!storage.canWrite()) {
			throw new IOException(bundle.getMessage("storage.writePermissionError", settings.getStorageLocation()));
		}
		
		return storage;
	}

	public File getTemporaryDirectory() throws DataNotFoundException, IOException {
		String storagePath = getStorageDirectory().getAbsolutePath();
		String tempPath = storagePath + File.separatorChar + TEMPORARY_DIR + File.separatorChar;
		File tmpDir = new File(tempPath);
		
		// Verifica se o diretório existe e, se não existir, tenta criá-lo
		if (!tmpDir.exists() && !tmpDir.mkdirs()) {
			throw new IOException(bundle.getMessage("storage.storageCreationError", tempPath));
		}
		
		// Verifica se tem permissão de escrita no diretório
		if (!tmpDir.canWrite()) {
			throw new IOException(bundle.getMessage("storage.writePermissionError", tempPath));
		}
		
		return tmpDir;
	}
	
	public File getUsersFilesDirectory() throws DataNotFoundException, IOException {
		String storagePath = getStorageDirectory().getAbsolutePath();
		String usersPath = storagePath + File.separatorChar + USERS_FILES_DIR + File.separatorChar;
		File usersDir = new File(usersPath);
		
		// Verifica se o diretório existe e, se não existir, tenta criá-lo
		if (!usersDir.exists() && !usersDir.mkdirs()) {
			throw new IOException(bundle.getMessage("storage.storageCreationError", usersPath));
		}
		
		// Verifica se tem permissão de escrita no diretório
		if (!usersDir.canWrite()) {
			throw new IOException(bundle.getMessage("storage.writePermissionError", usersPath));
		}
		
		return usersDir;
	}
	
	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
