package tv.snews.anews.util;

import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Locale;

/**
 * @author Eliezer Reis
 * @since 1.0.0
 */
public enum ResourceBundle {
	
	INSTANCE;
	
	private ResourceBundleMessageSource bundle;
	
	private ResourceBundle() {
		bundle = new ResourceBundleMessageSource();
		bundle.setBasenames("locale/server", "locale/report");
	}
	
	public String getMessage(String code) {
		return bundle.getMessage(code, null, Locale.getDefault());
	}

	public String getMessage(String code, Object... args) {
		return bundle.getMessage(code, args, Locale.getDefault());
	}

	public String getMessage(String code, Locale locale, Object... args) {
		return bundle.getMessage(code, args, locale);
	}
	
	public void setBundle(ResourceBundleMessageSource bundle) {
		this.bundle = bundle;
	}
}
