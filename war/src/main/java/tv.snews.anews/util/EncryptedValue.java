package tv.snews.anews.util;

import tv.snews.anews.domain.AbstractEntity;

import java.math.BigInteger;

/**
 * Classe utilizada para armazenar um valor criptografado. Para descriptografar
 * este valor é necessário utilizar a classe {@link PasswordEncryptor}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2?
 */
public class EncryptedValue extends AbstractEntity<Integer> {

	private transient byte[] iv;
	private transient byte[] cipherText;
	
	private byte[] value;
	
	//----------------------------------
	//	Constructors
	//----------------------------------
	
	public EncryptedValue() {}
	
    public EncryptedValue(byte[] iv, byte[] cipherText) {
    	this.iv = iv;
    	this.cipherText = cipherText;
    	this.value = join(iv, cipherText); // junta os valores de "iv" e "cipherText"
    }
    
    //----------------------------------
  	//	Helpers
  	//----------------------------------

    /**
     * Copia os dados de outro {@link EncryptedValue} para este objeto.
     * 
     * @param other objeto com os dados a serem copiados
     * @return a referência deste objeto
     */
    public EncryptedValue copy(EncryptedValue other) {
    	this.iv = other.iv;
    	this.cipherText = other.cipherText;
    	this.value = other.value;
    	
    	return this;
    }
    
    /*
	 * Junta os dois arrays informados em um único array. Para que possam ser
	 * separados posteriormente, o length do primeiro array será armazenado nos
	 * últimas quatro posições do array retornado (int = 4 byte).
	 */
    byte[] join(byte[] a, byte[] b) {
    	// Cria um array para armazenar os valores do dois arrays mais quatro bytes extras
    	byte[] result = new byte[a.length + b.length + 4];
    	
    	// Copia os valores do array "a" para o array "result"
    	for (int i = 0; i < a.length; i++) {
    		result[i] = a[i];
    	}
    	
    	// Copia os valores do array "b" para o array "result", após os valores de "a"
    	for (int i = a.length, j = 0; i < result.length - 4; i++, j++) {
    		result[i] = b[j];
    	}
    	
    	// Converte o length de "a" em bytes (um array tamanho 4) e armazena nas 
    	// últimas posições do array de resposta
    	byte[] bytes = intToBytes(a.length);
    	for (int i = result.length - 4, j = 0; i < result.length; i++, j++) {
    		result[i] = bytes[j];
    	}
    	
    	return result;
    }

    /*
	 * Separa os valores do array informado em dois arrays distintos, sabendo-se
	 * que os últimos quatro bytes do array informado representam o lenght do
	 * primeiro array.
	 */
    byte[][] split(byte[] a) {
    	// Pega os últimos 4 bytes e converte para um int, que é o ponto do split
    	byte[] intBytes = { a[a.length - 4], a[a.length - 3], a[a.length - 2], a[a.length - 1] };
    	int split = bytesToInt(intBytes);
    	
    	byte[] firstPart = new byte[split];
    	byte[] secondPart = new byte[a.length - split - 4];
    	
    	// Preenche o array da primeira parte
    	for (int i = 0; i < firstPart.length; i++) {
    		firstPart[i] = a[i];
    	}
    	
    	// Preenche o array da segunda parte
    	for (int i = 0, j = split; i < secondPart.length; i++, j++) {
    		secondPart[i] = a[j];
    	}
    	
    	return new byte[][] { firstPart, secondPart };
    }
    
    /*
     * Converte um inteiro para um array de quatro bytes (int = 4 bytes).
     */
    byte[] intToBytes(int value) {
    	BigInteger bigInt = BigInteger.valueOf(value);
    	
    	byte[] bytes = bigInt.toByteArray();
    	byte[] fullBytes = { 0x0, 0x0, 0x0, 0x0 };
    
    	for (int i = 0, j = 4 - bytes.length; i < bytes.length; i++, j++) {
    		fullBytes[j] = bytes[i];
    	}
    	
    	return fullBytes;
    }
    
    /*
     * Converte um array de 4 bytes em um inteiro.
     */
    int bytesToInt(byte[] bytes) {
    	return new BigInteger(bytes).intValue();
    }
    
    //----------------------------------
  	//	Getters & Setters
  	//----------------------------------
    
    public byte[] getValue() {
	    return value;
    }
    
    public void setValue(byte[] value) {
	    this.value = value;
	    
	    // Separa os valores de "iv" e "cipherText"
	    byte[][] splited = split(value);
	    this.iv = splited[0];
	    this.cipherText = splited[1];
    }
    
    public byte[] getCipherText() {
	    return cipherText;
    }
    
    public byte[] getIv() {
	    return iv;
    }
    
    private static final long serialVersionUID = 5318546744178470886L;
}
