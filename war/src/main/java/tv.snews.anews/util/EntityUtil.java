package tv.snews.anews.util;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class EntityUtil {

	private EntityUtil() {}

	public static boolean validId(Short id) {
		return id != null && id > 0;
	}

	public static boolean validId(Integer id) {
		return id != null && id > 0;
	}

	public static boolean validId(Long id) {
		return id != null && id > 0;
	}

	public static boolean notValidId(Short id) {
		return !validId(id);
	}

	public static boolean notValidId(Integer id) {
		return !validId(id);
	}

	public static boolean notValidId(Long id) {
		return !validId(id);
	}
}
