package tv.snews.anews.util;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

/**
 * Classe utilitária para criptografia MD5 e RSA com chave pública e privada.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public final class CriptUtil implements PasswordEncoder {

	private static final Logger log = LoggerFactory.getLogger(CriptUtil.class);
	
	private static Cipher ecipher;
	private static Cipher dcipher;

	// 8-byte Salt
	private static byte[] salt = { (byte) 0xA9, 
		(byte) 0x9B, (byte) 0xC8, (byte) 0x32, 
		(byte) 0x56, (byte) 0x35, (byte) 0xE3, 
		(byte) 0x03 };

	// Numero de iterações.
	private static int iterationCount = 19;

	static {
		try {
			KeySpec keySpec = new PBEKeySpec("radio@TVnews".toCharArray(), salt, iterationCount);
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
			ecipher = Cipher.getInstance(key.getAlgorithm());
			dcipher = Cipher.getInstance(key.getAlgorithm());

			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);

			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
		} catch (Exception e) {
			throw new RuntimeException("Initialization exception.", e);
		}
	}

	public static String encrypt(String value) {
		try {
			byte[] utf8 = value.getBytes(Charset.forName("UTF8"));
			final byte[] data = ecipher.doFinal(utf8);
			/*
			 * Converte um array de bytes em um array de caracteres
			 * representando o valor hexadecimal de cada byte na ordem. Depois
			 * disso, converte esse array de bytes em uma string.
			 */
			return new String(Hex.encodeHex(data));
		} catch (Exception e) {
			throw new RuntimeException("Encryption failed.", e);
		}
	}

	public static String decrypt(String value) {
		try {
			/*
			 * Converte a String que representa os valores hexadecimais em um
			 * array de bytes com os mesmos valores. Depois disso,
			 * descriptografa os bytes criptografados anteriormente.
			 */
			final byte[] utf8 = dcipher.doFinal(Hex.decodeHex(value.toCharArray()));
			return new String(utf8, Charset.forName("UTF8"));
		} catch (Exception e) {
			throw new RuntimeException("Decryption failed.", e);
		}
	}

	public static String md5(String value) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");

			messageDigest.reset();
			messageDigest.update(value.getBytes(Charset.forName("UTF8")));

			final byte[] resultByte = messageDigest.digest();
			return new String(Hex.encodeHex(resultByte));
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("MD5 initialization failed.", e);
		}
	}

	//------------------------------
	//	PasswordEncoder Methods
	//------------------------------

	@Override
	public String encode(CharSequence rawPassword) {
		return encrypt((String) rawPassword);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		/*
			Constant time comparison to prevent against timing attacks.
		 */

		byte[] actual = encode(rawPassword).getBytes();
		byte[] expected = encodedPassword.getBytes();

		if (expected.length != actual.length) {
			return false;
		}

		int result = 0;
		for (int i = 0; i < expected.length; i++) {
			result |= expected[i] ^ actual[i];
		}

		return result == 0;
	}
}
