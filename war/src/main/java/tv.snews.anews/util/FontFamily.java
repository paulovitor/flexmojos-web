package tv.snews.anews.util;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public enum FontFamily {

	ARIAL, GEORGIA, TAHOMA, TIMES_NEW_ROMAN, VERDANA;

	public static FontFamily parse(String value) {
		if (value != null) {
			value = value.toUpperCase().replaceAll(" ", "_");
			return valueOf(value);
		}
		throw new IllegalArgumentException("Invalid font family: " + value);
	}
}
