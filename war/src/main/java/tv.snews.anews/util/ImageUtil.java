package tv.snews.anews.util;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Classe utilitária para gerar manipulação de imagens.
 *
 * Created by samuel on 02/07/14.
 */
public final class ImageUtil implements ResourceLoaderAware {

    private static final String NO_IMAGE_TO_LOAD = "script.msg.noImageToLoad";
    private static final Logger log = LoggerFactory.getLogger(ImageUtil.class);
    private static ResourceLoader resourceLoader;
    private static final String slash = System.getProperty("file.separator");
    private static final ThumbnailGenerator generator = new ThumbnailGenerator();
    private static ResourceBundle bundle = ResourceBundle.INSTANCE;

    public static String copyImage(final String imgPath) throws IOException {
        Resource resourceSource = resourceLoader.getResource("file:" + imgPath);
        String newImgPath = getDirectory(resourceSource.getFile().getPath()) + slash + RandomStringUtils.randomAlphanumeric(10) + getExtension(imgPath);
        copyFile(imgPath, newImgPath);
        return newImgPath;
    }

//    public static void generate(String filePath, String thumbnailPath, int maxWidth, int maxHeight) {
//        generator.generate(filePath, thumbnailPath, maxWidth, maxHeight);
//    }

	public static String generateThumbnail(String originalPath, String thumbnailFolder, int width, int height) throws IOException {
		// Thumbnail é gerado no formato PNG para preservar o alpha

		String thumbPath = thumbnailFolder + RandomStringUtils.randomAlphanumeric(10) + ".png";

		File thumbFile = new File(thumbPath);
		thumbFile.mkdirs();

		File originalFile = new File(originalPath);
		BufferedImage originalImage = ImageIO.read(originalFile);

		if (originalImage.getWidth() * originalImage.getHeight() > 4000 * 4000) {
			throw new IllegalArgumentException("Image resolution must be bellow 4000x4000.");
		}

		BufferedImage result = Scalr.resize(originalImage, Scalr.Method.SPEED, width, height);
		ImageIO.write(result, "png", new File(thumbPath));

		return thumbPath;
	}

    public static String getExtension(String imgPath) {
        if (!StringUtils.isNotEmpty(imgPath)) {
            return "";
        }
        int index = imgPath.indexOf(".");
        return index != -1 ? imgPath.substring(index) : "";
    }

    public static String getDirectory(String imgPath) {
        if (!StringUtils.isNotEmpty(imgPath)) {
            return "";
        }
        int index = imgPath.lastIndexOf("/");
        return index != -1 ? imgPath.substring(0, index) : "";
    }

    public static String removeTmpDirectory(String imgPath, String tmpDirectory) {
        if (!StringUtils.isNotEmpty(imgPath)) {
            return "";
        }
        int index = imgPath.lastIndexOf(tmpDirectory);
        return index != -1 ? imgPath.substring(0, index) + imgPath.substring(index + 5, imgPath.length()): imgPath;
    }

    public static byte[] loadImg(String imgPath) {
        byte[] img = null;
        if(verifyFilePath(imgPath)) {
            if (imgPath != null && !imgPath.trim().equals("")) {
                Resource resource = resourceLoader.getResource("file:" + imgPath);
                try {
                    img = IOUtils.toByteArray(resource.getInputStream());
                } catch (IOException e) {
                    log.warn(bundle.getMessage(NO_IMAGE_TO_LOAD), e);
                }
            }
        }
        return img;
    }

    public static void copyFile(String imgSourcePath, String imgDestinationPath) {
        InputStream inStream = null;
        OutputStream outStream = null;
        try{

            File sourceFile = new File(imgSourcePath);
            File destinationFile = new File(imgDestinationPath);

            inStream = new FileInputStream(sourceFile);
            outStream = new FileOutputStream(destinationFile); // for override file content

            byte[] buffer = new byte[1024];

            int length;
            while ((length = inStream.read(buffer)) > 0){
                outStream.write(buffer, 0, length);
            }

            if (inStream != null)inStream.close();
            if (outStream != null)outStream.close();

            System.out.println("File Copied..");
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public static boolean verifyFilePath(String imgPath) {
        File file = new File(imgPath);
        return !file.isDirectory() && file.isFile() && file.exists();
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
