package tv.snews.anews.util;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Fornece algumas funcionalidade extras para qualquer coleção java.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public final class CollectionUtil {

	/**
	 * Construtor privado. Não deve ser usado.
	 */
	private CollectionUtil() {
		throw new AssertionError();
	}
	
	/**
	 * Adquire a primeira chave encontrada a partir de uma valor.
	 * 
	 * @param map A coleção a ser percorrida.
	 * @param value O valor de onde será localizado sua chave
	 * @return A chave encontrada.
	 */
	public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
	    for (Entry<T, E> entry : map.entrySet()) {
	        if (value.equals(entry.getValue())) {
	        	return entry.getKey();
	        }
	    }
	    return null;
	}
}
