package tv.snews.anews.flex;

import tv.snews.anews.dao.GenericDao;

import java.io.Serializable;
import java.util.List;

/**
 * Classe que agrupa todos os dados do resultado paginado de uma busca (número
 * da página, tamanho da página, total de registros e os registros da página).
 * <p>
 * Esse objeto permite então retornar ao cliente todas as informações da
 * paginação em uma única chamada ao servidor.
 * </p>
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public class PageResult<T> implements Serializable {

	private int pageNumber;
	private int pageSize;
	private int total;
	private List<T> results;
	
	/**
	 * Informa todos os valores necessários com exceção do tamanho da página, o 
	 * qual será utilizado o valor padrão.
	 * 
	 * @param pageNumber número da página
	 * @param total total de registros existentes no banco de dados
	 * @param results lista de registros da página
	 */
	public PageResult(int pageNumber, int total, List<T> results) {
		this(pageNumber, GenericDao.DEFAULT_PAGE_SIZE, total, results);
	}
	
	/**
	 * Construtor que já preenche todos os dados necessários.
	 * 
	 * @param pageNumber número da página
	 * @param pageSize Quantidade de registros de uma página
	 * @param total total de registros existentes no banco de dados
	 * @param results lista de registros da página
	 */
	public PageResult(int pageNumber, int pageSize, int total, List<T> results) {
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.total = total;
		this.results = results;
	}
	
	//----------------------------------
	//	Getters & Setters
	//----------------------------------
	
	public int getPageNumber() {
		return pageNumber;
	}
	
	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getResults() {
		return results;
	}

	public void setResults(List<T> results) {
		this.results = results;
	}
	
	private static final long serialVersionUID = 4139803959855039518L;
}
