package tv.snews.anews.flex;

import org.springframework.core.convert.converter.ConverterRegistry;
import org.springframework.flex.core.io.HibernateConfigProcessor;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class CustomConfigProcessor extends HibernateConfigProcessor {

	@Override
	protected void configureConverters(ConverterRegistry registry) {
		registry.addConverter(new AmfSortedSetConverter());
		super.configureConverters(registry);
	}
}
