package tv.snews.anews.flex;

import flex.messaging.FlexContext;
import flex.messaging.FlexSession;
import flex.messaging.FlexSessionListener;
import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.infra.ClipboardAction;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.SessionMessenger;
import tv.snews.anews.util.CollectionUtil;
import tv.snews.anews.util.ResourceBundle;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * Implementação da interface {@link tv.snews.anews.infra.SessionManager} utilizando os recursos do
 * Flex para o controle de sessão.
 * 
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class FlexSessionManager implements SessionManager {

	// Keep track of each session opened by this session manager
	private final ConcurrentMap<User, FlexSession> sessions = new ConcurrentHashMap<>();

	private UserDao userDao;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private SessionMessenger sessionMessenger;

	@Override
	public User getCurrentUser() throws SessionManagerException {
		User user = null;

		FlexSession session = getSession();
		if (session == null) {
			/*
			 * A sessão será nula nas chamadas Rest do aplicativo mobile. Nesse caso
			 * obtém o usuário autenticado pelo SecurityContextHolder.
			 */
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

			if (authentication != null) {
				String email = authentication.getName();
				user = userDao.findByEmail(email);
			}
		} else if (session.isValid()) {
			/*
			 * Sessão normal do Flex. Pega o ID do usuário da sessão e carrega ele.
			 */
			Integer userId = (Integer) session.getAttribute(SESSION_KEY);
			if (userId != null && userId > 0) {
				/*
				 * Recarrega o usuário do banco de dados para recarregar
				 * quaisquer alterações no seus dados, como suas permissões.
				 */
				user = userDao.get(userId);
				user.setSessionId(session.getId());
			}
		}

		return user;
	}

	@Override
	public Long getClientTimezoneOffset() {
		return (Long) getSession().getAttribute(OFFSET_KEY);
	}

	@Override
	public String registerUserSession(User user, Long timezoneOffset) throws SessionManagerException {
		Objects.requireNonNull(user);

		// Destroi qualquer sessão antiga já registrada
		kickOut(user);

		FlexSession session = getSession();

		HttpSession httpSession = FlexContext.getHttpRequest().getSession();
		httpSession.setMaxInactiveInterval(5 * 60);

		session.setAttribute(SESSION_KEY, user.getId());
		session.setAttribute(OFFSET_KEY, timezoneOffset);
		sessions.put(user, session);

		sessionMessenger.notifyUserLoggedIn(user, session.getId());
		session.addSessionDestroyedListener(new FlexSessionListener() {

			@Override
			public void sessionCreated(FlexSession session) {
				//Não faz nada dentro de um addSession"Destroyed"Listener
			}

			@Override
			public void sessionDestroyed(FlexSession session) {
				User user = CollectionUtil.getKeyByValue(sessions, session);
				if (user != null) {
					if (session != null)
						user.setSessionId(session.getId());
					releaseUser(user);
				}
			}
		});

		return session.getId();
	}

	@Override
	public Set<User> getOnlineUsers() {
		return sessions.keySet();
	}

	@Override
	public void kickOut(User user) {
		Objects.requireNonNull(user);
		FlexSession session = sessions.get(user);

		if (session != null)
			sessionMessenger.notifyUserLoggedOut(user, session.getId()); // notifica outros clientes

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (session != null) {
			if (session.isValid()) {
				session.invalidate(); // vai disparar o sessionDestroyedListener!
			} else {
				releaseUser(user);
			}
		}
	}

	@Override
	public void leave() throws SessionManagerException {
		User user = getCurrentUser();
		if (user != null) {
			kickOut(user);
		}
	}

	@Override
	public void clipboardPush(ClipboardAction action, Long[] entities) {
		FlexSession session = getSession();

		ClipboardAction currentAction = clipboardAction();

		//If user change clipboard action, clear de clipboard before continue;
		String clipboard = "";
		if (currentAction != action) {
			clipboardFlush();
		}

		clipboard = clipboardFetch();
		//Remove repeated values
		clipboard = clipboardMerge(clipboard, entities);

		System.out.println(action + " : " + clipboard);
		session.setAttribute(CLIPBOARD_KEY, clipboard);
		session.setAttribute(CLIPBOARD_ACTION_KEY, action);

	}

	@Override
	public Long[] clipboardPull() {
		String clipboard = clipboardFetch();

		Set<Long> ids = new HashSet<>();

		for (String token : clipboard.split(",")) {
			if (!isBlank(token)) {
				ids.add(Long.parseLong(token));
			}
		}

		return ids.toArray(new Long[ids.size()]);
	}

	@Override
	public boolean containsOnClipboard(Long id) {
		String clipboard = clipboardFetch();
		List<String> ids = new ArrayList<String>(Arrays.asList(clipboard.split(",")));
		return ids.contains(id.toString());
	}

	private String clipboardMerge(String clipboard, Long[] newValues) {
		List<String> olds = new ArrayList<String>(Arrays.asList(clipboard.split(",")));

		for (Long n : newValues) {
			if (olds.contains(n.toString())) {
				olds.remove(n.toString());
			} else {
				olds.add(n.toString());
			}
		}

		return StringUtils.join(olds, ",");
	}

	@Override
	public ClipboardAction clipboardAction() {
		ClipboardAction clipboard = null;
		Object attribute = getSession().getAttribute(CLIPBOARD_ACTION_KEY);
		if (attribute != null) {
			clipboard = ClipboardAction.valueOf(attribute.toString().toUpperCase());
		}
		return clipboard;
	}

	private String clipboardFetch() {
		String clipboard = "";
		Object attribute = getSession().getAttribute(CLIPBOARD_KEY);
		if (attribute != null) {
			clipboard = attribute.toString();;
		}
		return clipboard;
	}

	@Override
	public void clipboardFlush() {
		getSession().removeAttribute(CLIPBOARD_KEY);
		getSession().removeAttribute(CLIPBOARD_ACTION_KEY);
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

	private FlexSession getSession() throws SessionManagerException {
		FlexSession session = FlexContext.getFlexSession();
		if (session == null) {
			throw new SessionManagerException(bundle.getMessage("session.creationError"));
		}
		return session;
	}

	private void releaseUser(User user) {
		sessions.remove(user);
		EntityLockerFactory.releaseAllLocksOfUser(user); // tira bloqueios
		sessionMessenger.notifyUserLoggedOut(user, user.getSessionId()); // notifica outros clientes
	}

	//--------------------------------------------------------------------------
	//	Setters & Getters
	//--------------------------------------------------------------------------
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setSessionMessenger(SessionMessenger sessionMessenger) {
		this.sessionMessenger = sessionMessenger;
	}

	public Map<User, FlexSession> getSessions() {
		return sessions;
	}

}
