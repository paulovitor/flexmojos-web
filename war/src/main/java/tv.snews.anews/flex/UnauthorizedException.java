package tv.snews.anews.flex;


/**
 * 
 * @author snews
 * @since 1.3.0
 */
public class UnauthorizedException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

	public UnauthorizedException(String cause) {
	    super(cause);
    }
    
    public UnauthorizedException(String cause, Throwable exception) {
    	super(cause, exception);
    }
}
