package tv.snews.anews.flex.serialize;

import flex.messaging.io.MessageIOConstants;
import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.AmfTrace;

import java.io.OutputStream;

/**
 * Extende a classe {@link flex.messaging.io.amf.AmfMessageSerializer} do BlazeDS para
 * que possa customizar o {@link Amf3Input} e {@link Amf3Output} que serão
 * utilizados nos channels.
 *
 * @author Felipe Pinheiro
 */
public class AmfMessageSerializer extends flex.messaging.io.amf.AmfMessageSerializer {

    @Override
    public void initialize(SerializationContext context, OutputStream out, AmfTrace trace) {
        amfOut = new Amf0Output(context);
        amfOut.setOutputStream(out);
        amfOut.setAvmPlus(version >= MessageIOConstants.AMF3);

        debugTrace = trace;
        isDebug = trace != null;
        amfOut.setDebugTrace(debugTrace);
    }
}
