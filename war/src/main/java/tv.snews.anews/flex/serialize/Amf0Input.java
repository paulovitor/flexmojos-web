package tv.snews.anews.flex.serialize;

import flex.messaging.io.SerializationContext;

import java.io.IOException;

/**
 * Extende a classe {@link flex.messaging.io.amf.Amf0Input} do BlazeDS para
 * que possa customizar o {@link Amf3Input} e {@link Amf3Output} que serão
 * utilizados nos channels.
 *
 * @author Felipe Pinheiro
 */
public class Amf0Input extends flex.messaging.io.amf.Amf0Input {

    public Amf0Input(SerializationContext context) {
        super(context);
    }

    @Override
    public Object readObject() throws ClassNotFoundException, IOException {
        if (avmPlusInput == null) {
            avmPlusInput = new Amf3Input(context);
            avmPlusInput.setDebugTrace(trace);
            avmPlusInput.setInputStream(in);
        }
        return super.readObject();
    }
}
