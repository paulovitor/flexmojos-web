package tv.snews.anews.flex.serialize;

/**
 * Extende a classe {@link flex.messaging.endpoints.AMFEndpoint} do BlazeDS para
 * que possa customizar o {@link Amf3Input} e {@link Amf3Output} que serão
 * utilizados nos channels.
 *
 * <p>Essa classe será atribuida no arquivo {@code services-config.xml}.</p>
 *
 * @author Felipe Pinheiro
 */
public class AMFEndpoint extends flex.messaging.endpoints.AMFEndpoint {

    @Override
    protected String getSerializerClassName() {
        return AmfMessageSerializer.class.getName();
    }

    @Override
    protected String getDeserializerClassName() {
        return AmfMessageDeserializer.class.getName();
    }
}
