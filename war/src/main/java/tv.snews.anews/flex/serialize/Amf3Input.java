package tv.snews.anews.flex.serialize;

import flex.messaging.FlexContext;
import flex.messaging.io.SerializationContext;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.DST_OFFSET;
import static java.util.Calendar.ZONE_OFFSET;
import static tv.snews.anews.infra.SessionManager.OFFSET_KEY;

/**
 * Extende a classe {@link flex.messaging.io.amf.Amf3Input} para que possa
 * corrigir o timezone dos objetos {@link java.util.Date} que são recebidos do cliente
 * Flex.
 *
 * <p>A correção que será efetuada necessita que o timezone do cliente esteja
 * armazenado no {@link tv.snews.anews.infra.SessionManager} na chave {@link tv.snews.anews.infra.SessionManager#OFFSET_KEY}.</p>
 *
 * @author Felipe Pinheiro
 */
public class Amf3Input extends flex.messaging.io.amf.Amf3Input {

    public Amf3Input(SerializationContext context) {
        super(context);
    }

    @Override
    protected Date readDate() throws IOException {
        Date date = super.readDate();

        if (date != null) {
            Long serverOffset = getServerTimezoneOffset(new Date());
            Long clientOffset = getClientTimezoneOffset();
            Long dateOffset = getServerTimezoneOffset(date);

            // Somente pode fazer o tratamento se o usuário já estiver logado
            if (clientOffset != null) {
                // Se os timezones forem iguais não precisa fazer nada.
                if (!clientOffset.equals(serverOffset)) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(date.getTime() - (dateOffset - clientOffset));
                    date = cal.getTime();
                }
            }
        }

        return date;
    }

    private Long getClientTimezoneOffset() {
        return (Long) FlexContext.getFlexSession().getAttribute(OFFSET_KEY);
    }

    private long getServerTimezoneOffset(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        /*
         * Em uma data dentro do horário de verão do Brasil teremos:
         *     ZONE_OFFSET = -3 horas
         *     DST_OFFSET = 1 hora
         *
         * Fora do horário de verão teremos:
         *     ZONE_OFFSET = -3 horas
         *     DST_OFFSET = 0 horas
         */
        return calendar.get(ZONE_OFFSET) + calendar.get(DST_OFFSET);
    }
}
