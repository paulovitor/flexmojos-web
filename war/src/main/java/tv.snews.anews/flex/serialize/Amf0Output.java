package tv.snews.anews.flex.serialize;

import flex.messaging.io.SerializationContext;

/**
 * Extende a classe {@link flex.messaging.io.amf.Amf0Output} do BlazeDS para
 * que possa customizar o {@link Amf3Input} e {@link Amf3Output} que serão
 * utilizados nos channels.
 *
 * @author Felipe Pinheiro
 */
public class Amf0Output extends flex.messaging.io.amf.Amf0Output {

    public Amf0Output(SerializationContext context) {
        super(context);
    }

    @Override
    protected void createAMF3Output() {
        avmPlusOutput = new Amf3Output(context);
        avmPlusOutput.setOutputStream(out);
        avmPlusOutput.setDebugTrace(trace);
    }
}
