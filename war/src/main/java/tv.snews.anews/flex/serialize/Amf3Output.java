package tv.snews.anews.flex.serialize;

import flex.messaging.FlexContext;
import flex.messaging.io.SerializationContext;
import tv.snews.anews.infra.SessionManager;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import static java.util.Calendar.DST_OFFSET;
import static java.util.Calendar.ZONE_OFFSET;
import static tv.snews.anews.infra.SessionManager.OFFSET_KEY;

/**
 * Extende a classe {@link flex.messaging.io.amf.Amf3Output} para que possa
 * corrigir o timezone dos objetos {@link java.util.Date} que serão enviados ao cliente
 * Flex.
 *
 * <p>A correção que será efetuada necessita que o timezone do cliente esteja
 * armazenado no {@link SessionManager} na chave {@link SessionManager#OFFSET_KEY}.</p>
 *
 * @author Felipe Pinheiro
 */
public class Amf3Output extends flex.messaging.io.amf.Amf3Output {

    public Amf3Output(SerializationContext context) {
        super(context);
    }

    @Override
    protected void writeAMFDate(Date date) throws IOException {
        if (date != null) {
            Long serverOffset = getServerTimezoneOffset(new Date());
            Long clientOffset = getClientTimezoneOffset();
            Long dateOffset = getServerTimezoneOffset(date);

            // Somente pode fazer o tratamento se o usuário já estiver logado
            if (clientOffset != null) {
                /*
                 * Se os timezones forem iguais não precisa fazer nada. Também
                 * ignora o caso em que o cliente não está no horário de verão e
                 * a data sim.
                 */
                if (!clientOffset.equals(serverOffset)/* && dateOffset >= clientOffset*/) {
                    Calendar cal = Calendar.getInstance();
                    cal.setTimeInMillis(date.getTime() + (dateOffset - clientOffset));
                    date = cal.getTime();
                }
            }
        }

        super.writeAMFDate(date);
    }

    private Long getClientTimezoneOffset() {
        return (Long) FlexContext.getFlexSession().getAttribute(OFFSET_KEY);
    }

    private long getServerTimezoneOffset(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        /*
         * Em uma data dentro do horário de verão do Brasil teremos:
         *     ZONE_OFFSET = -3 horas
         *     DST_OFFSET = 1 hora
         *
         * Fora do horário de verão teremos:
         *     ZONE_OFFSET = -3 horas
         *     DST_OFFSET = 0 horas
         */
        return calendar.get(ZONE_OFFSET) + calendar.get(DST_OFFSET);
    }
}
