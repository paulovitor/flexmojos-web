package tv.snews.anews.flex.serialize;

import flex.messaging.io.SerializationContext;
import flex.messaging.io.amf.AmfTrace;

import java.io.InputStream;

/**
 * Extende a classe {@link flex.messaging.io.amf.AmfMessageDeserializer} do BlazeDS para
 * que possa customizar o {@link Amf3Input} e {@link Amf3Output} que serão
 * utilizados nos channels.
 *
 * @author Felipe Pinheiro
 */
public class AmfMessageDeserializer extends flex.messaging.io.amf.AmfMessageDeserializer {

    @Override
    public void initialize(SerializationContext context, InputStream in, AmfTrace trace) {
        amfIn = new Amf0Input(context);
        amfIn.setInputStream(in);

        debugTrace = trace;
        isDebug = debugTrace != null;
        amfIn.setDebugTrace(debugTrace);
    }
}
