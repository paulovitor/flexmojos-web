package tv.snews.anews.flex;

import flex.messaging.io.ArrayCollection;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.GenericConverter;

import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class AmfSortedSetConverter implements GenericConverter {

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        return Collections.singleton(new ConvertiblePair(ArrayCollection.class, SortedSet.class));
    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        ArrayCollection collection = (ArrayCollection) source;
        return new TreeSet<Object>(collection);
    }
}
