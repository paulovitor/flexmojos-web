package tv.snews.anews.flex;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Felipe Pinheiro
 */
@XmlRootElement(name = "file")
public class UploadXmlResponse implements Serializable {

	private String fileName;
	private String extension;
	private String path;
	private long size;
	private String thumbnailPath;

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getThumbnailPath() {
		return thumbnailPath;
	}

	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}

	//------------------------------
	//	Builder Class
	//------------------------------

	public static class Builder {

		private UploadXmlResponse xmlResponse;

		public Builder() {
			xmlResponse = new UploadXmlResponse();
		}

		public Builder fileName(String value) {
			xmlResponse.setFileName(value);
			return this;
		}

		public Builder extension(String value) {
			xmlResponse.setExtension(value);
			return this;
		}

		public Builder path(String value) {
			xmlResponse.setPath(value);
			return this;
		}

		public Builder size(long value) {
			xmlResponse.setSize(value);
			return this;
		}

		public Builder thumbnailPath(String value) {
			xmlResponse.setThumbnailPath(value);
			return this;
		}

		public UploadXmlResponse build() {
			return xmlResponse;
		}
	}

	private static final long serialVersionUID = -4249994047623190124L;
}
