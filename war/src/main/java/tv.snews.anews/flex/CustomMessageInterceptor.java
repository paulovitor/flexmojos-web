package tv.snews.anews.flex;

import flex.messaging.FlexContext;
import flex.messaging.messages.Message;
import flex.messaging.messages.RemotingMessage;
import org.springframework.flex.core.MessageInterceptor;
import org.springframework.flex.core.MessageProcessingContext;
import tv.snews.anews.util.ResourceBundle;

import java.util.ArrayList;
import java.util.List;

public class CustomMessageInterceptor implements MessageInterceptor {

	private List<String> allowedRequests = new ArrayList<>();
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	public CustomMessageInterceptor() {
		allowedRequests.add("userService.loadByCredentials");
		allowedRequests.add("userService.recoveryPassword");
		allowedRequests.add("userService.changePassword");

		allowedRequests.add("loginService.getCurrentUser");
		allowedRequests.add("loginService.login");
		allowedRequests.add("loginService.leave");
		allowedRequests.add("loginService.agreeWithTerms");

		allowedRequests.add("settingsService.loadCurrentLocale");
		allowedRequests.add("settingsService.loadSettings");
		allowedRequests.add("settingsService.loadImg");
		allowedRequests.add("settingsService.loadImg");

		//Solução paleativa para as consultas entre praças.
		//Libera somente métodos de consulta.
		allowedRequests.add("storyService.storiesByFullTextSearch");
		allowedRequests.add("reportageService.reportageByFullTextSearch");
		allowedRequests.add("guidelineService.fullTextSearch");

		allowedRequests.add("programService.listAll");
		allowedRequests.add("userService.listReporters");
		allowedRequests.add("userService.listProducers");

		allowedRequests.add("guidelineService.remoteCopyRequest");
		allowedRequests.add("reportageService.remoteCopyRequest");
		allowedRequests.add("storyService.remoteCopyRequest");

		allowedRequests.add("storyService.copyRemoteStory");
		allowedRequests.add("reportageService.copyRemoteReportage");
		allowedRequests.add("guidelineService.copyRemoteGuideline");

		allowedRequests.add("guidelineService.loadById");
		allowedRequests.add("reportageService.loadById");
		allowedRequests.add("storyService.loadById");

		allowedRequests.add("rundownService.verifyRundown");
		allowedRequests.add("rundownService.loadRundown");
	}

	@Override
	public Message postProcess(MessageProcessingContext context, Message inputMessage, Message outputMessage) {
		return outputMessage;
	}

	@Override
	public Message preProcess(MessageProcessingContext context, Message inputMessage) {
		if (!(inputMessage instanceof RemotingMessage)) {
			return inputMessage;
		}

		RemotingMessage msg = (RemotingMessage) inputMessage;
		String request = msg.getDestination() + "." + msg.getOperation();
		if (FlexContext.getFlexSession() == null || FlexContext.getFlexSession().getAttribute("user_id") == null) {
			if (!allowedRequests.contains(request)) {
				throw new UnauthorizedException("tv.snews.anews.flex.UnauthorizedException : " + bundle.getMessage("session.unauthorized"));
			}
		}

		return msg;
	}
}
