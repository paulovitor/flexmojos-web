package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.AbstractMessage;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.ChatMessenger;

import java.util.Arrays;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ChatMessenger} utilizando o suporte do Flex
 * para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexChatMessenger extends AbstractFlexMessenger implements ChatMessenger {

	private static final String MEMBER_JOINED = "join";
	private static final String MEMBER_LEFT = "leave";
	private static final String MEMBER_TYPING = "typing";
	
    public FlexChatMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean sendChatMessage(AbstractMessage chatMessage) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(chatMessage);
		return sendMessage(message);
	}

	@Override
	public boolean notifyMembersJoined(Chat chat, User... members) {
		AsyncMessage asyncMessage = createAsyncMessage();
		asyncMessage.setHeader("notification", true);
		asyncMessage.setHeader("type", MEMBER_JOINED);
		asyncMessage.setHeader("chat", chat);
		asyncMessage.setHeader("users", Arrays.asList(members));
		return sendMessage(asyncMessage);
	}

	@Override
	public boolean notifyMemberLeft(Chat chat, User user) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("notification", true);
		message.setHeader("type", MEMBER_LEFT);
		message.setHeader("chat", chat);
		message.setHeader("user", user);
		return sendMessage(message);
	}

	@Override
	public boolean notifyUserIsTyping(User user, Chat chat, boolean typing) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("notification", true);
		message.setHeader("type", MEMBER_TYPING);
		message.setHeader("status", typing);
		message.setHeader("user", user);
		message.setHeader("chat", chat);
		return sendMessage(message);
	}
}
