package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.ChecklistResourceMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ChecklistResourceMessenger} utilizando o envio
 * de mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public class FlexChecklistResourceMessenger extends AbstractFlexMessenger implements ChecklistResourceMessenger {

	public FlexChecklistResourceMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(ChecklistResource checklistResource) {
		return sendMessage(createAsyncMessage(checklistResource, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(ChecklistResource checklistResource) {
		return sendMessage(createAsyncMessage(checklistResource, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(ChecklistResource checklistResource) {
		return sendMessage(createAsyncMessage(checklistResource, EntityAction.DELETED));
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(ChecklistResource checklistResource, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(checklistResource);
		message.setHeader("action", action);
		return message;
	}
		
}
