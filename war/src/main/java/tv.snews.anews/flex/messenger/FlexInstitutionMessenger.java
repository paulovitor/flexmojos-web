package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Institution;
import tv.snews.anews.messenger.InstitutionMessenger;

/**
 * Implementação da interface {@link InstitutionMessenger} utilizando o envio de 
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class FlexInstitutionMessenger extends AbstractFlexMessenger implements InstitutionMessenger {

	public FlexInstitutionMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Institution institution) {
		return sendMessage(createAsyncMessage(institution, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Institution institution) {
		return sendMessage(createAsyncMessage(institution, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Institution institution) {
		return sendMessage(createAsyncMessage(institution, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Institution institution, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(institution);
		message.setHeader("action", action);
		return message;
	}
}
