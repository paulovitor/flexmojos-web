package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.StoryTemplate;
import tv.snews.anews.messenger.StoryTemplateMessenger;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class FlexStoryTemplateMessenger extends AbstractFlexMessenger implements StoryTemplateMessenger {

    public FlexStoryTemplateMessenger(String destination) {
        super(destination);
    }

    @Override
    public boolean notifyUpdate(StoryTemplate story) {
        return sendMessage(createAsyncMessage(story, EntityAction.UPDATED));
    }

    //--------------------------------------------------------------------------
    //	Helpers
    //--------------------------------------------------------------------------

    private AsyncMessage createAsyncMessage(StoryTemplate story, EntityAction action) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(story);
        message.setHeader("action", action);
        return message;
    }
}
