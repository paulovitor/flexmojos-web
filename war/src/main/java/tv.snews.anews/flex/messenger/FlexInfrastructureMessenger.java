package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.InfrastructureMessenger;

/**
 * Implementação da interface {@link InfrastructureMessenger} utizando o envio
 * de mensgens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class FlexInfrastructureMessenger extends AbstractFlexMessenger implements InfrastructureMessenger {

	private static enum MessageType { INFO, WARN, ERROR };
	
	public FlexInfrastructureMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInfo(String infoMessage) {
		AsyncMessage message = createAsyncMessage(MessageType.INFO, infoMessage);
		return sendMessage(message);
	}
	
	@Override
	public boolean notifyInfo(String infoMessage, User user) {
		AsyncMessage message = createAsyncMessage(MessageType.INFO, infoMessage, user);
		return sendMessage(message);
	}
	
	@Override
	public boolean notifyWarn(String warnMessage) {
		AsyncMessage message = createAsyncMessage(MessageType.WARN, warnMessage);
		return sendMessage(message);
	}
	
	@Override
	public boolean notifyWarn(String warnMessage, User user) {
		AsyncMessage message = createAsyncMessage(MessageType.WARN, warnMessage, user);
		return sendMessage(message);
	}
	
	@Override
	public boolean notifyError(String errorMessage) {
		AsyncMessage message = createAsyncMessage(MessageType.ERROR, errorMessage);
		return sendMessage(message);
	}

	@Override
	public boolean notifyError(String errorMessage, User user) {
		AsyncMessage message = createAsyncMessage(MessageType.ERROR, errorMessage, user);
		return sendMessage(message);
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(MessageType type, String text) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(text);
		message.setHeader("type", type);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(MessageType type, String text, User user) {
		AsyncMessage message = createAsyncMessage(type, text);
		message.setHeader("user", user);
		return message;
	}
}
