package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Segment;
import tv.snews.anews.messenger.SegmentMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.SegmentMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class FlexSegmentMessenger extends AbstractFlexMessenger implements SegmentMessenger {

	public FlexSegmentMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Segment segment) {
		return sendMessage(createAsyncMessage(segment, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Segment segment) {
		return sendMessage(createAsyncMessage(segment, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Segment segment) {
		return sendMessage(createAsyncMessage(segment, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Segment segment, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(segment);
		message.setHeader("action", action);
		return message;
	}
}
