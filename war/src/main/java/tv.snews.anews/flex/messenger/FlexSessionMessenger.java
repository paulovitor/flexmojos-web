package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.SessionAction;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.SessionMessenger;

/**
 * Implementação da interface {@link SessionMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexSessionMessenger extends AbstractFlexMessenger implements SessionMessenger {
	
    public FlexSessionMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean notifyUserLoggedIn(User user, String sessionId) {
		AsyncMessage message = createSessionMessage(SessionAction.USER_IN, user.getId(), sessionId);
		return sendMessage(message);
	}

	@Override
	public boolean notifyUserLoggedOut(User user, String sessionId) {
		AsyncMessage message = createSessionMessage(SessionAction.USER_OUT, user.getId(), sessionId);
		return sendMessage(message);
	}
	
	private AsyncMessage createSessionMessage(SessionAction action, Integer userId, String sessionId) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(userId);
		message.setHeader("action", action);
		message.setHeader("sessionId", sessionId);
		return message;
	}
}
