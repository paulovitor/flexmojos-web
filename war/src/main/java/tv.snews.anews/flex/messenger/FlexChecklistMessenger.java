package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.ChecklistMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ChecklistMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public class FlexChecklistMessenger extends AbstractFlexMessenger implements ChecklistMessenger {

    public FlexChecklistMessenger(String destination) {
	    super(destination);
    }

    //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
    
    @Override
    public boolean notifyInsert(Checklist checklist) {
		return sendMessage(createAsyncMessage(checklist, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyUpdate(Checklist checklist) {
    	return sendMessage(createAsyncMessage(checklist, EntityAction.UPDATED));
    }
    
    @Override
    public boolean notifyDelete(Checklist checklist) {
		return sendMessage(createAsyncMessage(checklist, EntityAction.DELETED));
    }

    @Override
    public boolean notifyRestore(Checklist checklist) {
		return sendMessage(createAsyncMessage(checklist, EntityAction.RESTORED));
    }
    
	@Override
	public boolean notifyLock(Checklist checklist, User user) {
		return sendMessage(createAsyncMessage(checklist, EntityAction.LOCKED, user));
	}

	@Override
	public boolean notifyUnlock(Checklist checklist, User user) {
		return sendMessage(createAsyncMessage(checklist, EntityAction.UNLOCKED, user));
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Checklist checklist, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(checklist);
		message.setHeader("action", action);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(Checklist checklist, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(checklist, action);
		message.setHeader("user", user);
		return message;
	}
}
