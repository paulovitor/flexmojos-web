package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Program;
import tv.snews.anews.messenger.ProgramMessenger;

/**
 * Implementação da interface {@link ProgramMessenger} utilizando o envio de 
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexProgramMessenger extends AbstractFlexMessenger implements ProgramMessenger {

	public FlexProgramMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Program program) {
		return sendMessage(createAsyncMessage(program, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Program program) {
		return sendMessage(createAsyncMessage(program, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Program program) {
		return sendMessage(createAsyncMessage(program, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Program program, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(program);
		message.setHeader("action", action);
		return message;
	}
}
