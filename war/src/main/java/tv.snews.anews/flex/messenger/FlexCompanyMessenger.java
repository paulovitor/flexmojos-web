package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.Company;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.CompanyMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.CompanyMessenger} utilizando o suporte do Flex
 * para o envio de mensagens.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class FlexCompanyMessenger extends AbstractFlexMessenger implements CompanyMessenger {

	public FlexCompanyMessenger(String destination) {
		super(destination);
	}
	
	 //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInserted(Company company) {
		return sendMessage(createAsyncMessage(company, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdated(Company company) {
		return sendMessage(createAsyncMessage(company, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDeleted(Company company) {
		return sendMessage(createAsyncMessage(company, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
  	//	Helpers
  	//--------------------------------------------------------------------------
  	
  	private AsyncMessage createAsyncMessage(Company company, EntityAction action) {
  		AsyncMessage message = createAsyncMessage();
  		message.setBody(company);
  		message.setHeader("action", action);
  		return message;
  	}
}
