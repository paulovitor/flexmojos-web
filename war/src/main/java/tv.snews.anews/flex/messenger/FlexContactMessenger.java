package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.ContactMessenger;

/**
 * Implementação da interface {@link ContactMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexContactMessenger extends AbstractFlexMessenger implements ContactMessenger {

    public FlexContactMessenger(String destination) {
	    super(destination);
    }

	@Override
	public boolean notifyContactInserted(Contact contact) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(contact);
		message.setHeader("type", EntityAction.INSERTED);
		return sendMessage(message);
	}

	@Override
	public boolean notifyContactUpdated(Contact contact) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(contact);
		message.setHeader("type", EntityAction.UPDATED);
		return sendMessage(message);
	}

	@Override
	public boolean notifyContactDeleted(Contact contact) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(contact);
		message.setHeader("type", EntityAction.DELETED);
		return sendMessage(message);
	}

}
