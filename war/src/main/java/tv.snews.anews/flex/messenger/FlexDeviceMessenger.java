package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.Device;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.DeviceMessenger;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class FlexDeviceMessenger extends AbstractFlexMessenger implements DeviceMessenger {

	protected FlexDeviceMessenger(String destination) {
		super(destination);
	}

	//------------------------------
	//	Notifiers
	//------------------------------

	@Override
	public boolean notifyInsert(Device device) {
		return sendMessage(createAsyncMessage(device, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Device device) {
		return sendMessage(createAsyncMessage(device, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Device device) {
		return sendMessage(createAsyncMessage(device, EntityAction.DELETED));
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private AsyncMessage createAsyncMessage(Device device, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(device);
		message.setHeader("action", action);
		return message;
	}
}
