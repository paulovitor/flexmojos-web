package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Round;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.RoundMessenger;

/**
 * Implementação da interface {@link RoundMessenger} utilizando o envio de 
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class FlexRoundMessenger extends AbstractFlexMessenger implements RoundMessenger {

    public FlexRoundMessenger(String destination) {
	    super(destination);
    }
	
    @Override
    public boolean notifyUpdate(Round round) {
	    return sendMessage(createAsyncMessage(round, EntityAction.UPDATED));
    }

    @Override
    public boolean notifyInsert(Round round) {
    	return sendMessage(createAsyncMessage(round, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyDelete(Round round) {
    	return sendMessage(createAsyncMessage(round, EntityAction.DELETED));
    }
    
    @Override
    public boolean notifyLock(Round round, User user) {
    	return sendMessage(createAsyncMessage(round, EntityAction.LOCKED, user));
    }

    @Override
    public boolean notifyUnlock(Round round, User user) {
    	return sendMessage(createAsyncMessage(round, EntityAction.UNLOCKED, user));
    }

    //--------------------------------------------------------------------------
  	//	Helpers
  	//--------------------------------------------------------------------------
  	
  	private AsyncMessage createAsyncMessage(Round round, EntityAction action) {
  		AsyncMessage message = createAsyncMessage();
  		message.setBody(round);
  		message.setHeader("action", action);
  		return message;
  	}
  	
	private AsyncMessage createAsyncMessage(Round round, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(round, action);
		message.setHeader("user", user);
		return message;
	}

}