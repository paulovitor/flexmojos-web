package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.ReportNature;
import tv.snews.anews.messenger.ReportNatureMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ReportNatureMessenger} utilizando o envio
 * de mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class FlexReportNatureMessenger extends AbstractFlexMessenger implements ReportNatureMessenger {

	public FlexReportNatureMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(ReportNature nature) {
		return sendMessage(createAsyncMessage(nature, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(ReportNature nature) {
		return sendMessage(createAsyncMessage(nature, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(ReportNature nature) {
		return sendMessage(createAsyncMessage(nature, EntityAction.DELETED));
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(ReportNature nature, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(nature);
		message.setHeader("action", action);
		return message;
	}
		
}
