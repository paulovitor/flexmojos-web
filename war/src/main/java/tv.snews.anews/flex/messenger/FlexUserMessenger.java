package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.UserMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.UserMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexUserMessenger extends AbstractFlexMessenger implements UserMessenger {

    public FlexUserMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean notifyUsersChanged() {
		AsyncMessage message = createAsyncMessage();
		return sendMessage(message);
	}

    @Override
    public boolean notifyInsert(User user) {
    	return sendMessage(createAsyncMessage(user, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyUpdate(User user) {
    	return sendMessage(createAsyncMessage(user, EntityAction.UPDATED));
    }

    @Override
    public boolean notifyDelete(User user) {
    	return sendMessage(createAsyncMessage(user, EntityAction.DELETED));
    }
    
    //--------------------------------------------------------------------------
  	//	Helpers
  	//--------------------------------------------------------------------------
  	private AsyncMessage createAsyncMessage(User user, EntityAction action) {
  		AsyncMessage message = createAsyncMessage();
  		message.setBody(user);
  		message.setHeader("action", action);
  		return message;
  	}
}
