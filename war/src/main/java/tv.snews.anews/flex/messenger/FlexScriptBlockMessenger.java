package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.ScriptBlockMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ScriptBlockMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 *
 * @author Maxuel Ramos
 * @since 1.7
 */
public class FlexScriptBlockMessenger extends AbstractFlexMessenger implements ScriptBlockMessenger {

	public FlexScriptBlockMessenger(String destination) {
		super(destination);
	}
	

	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(ScriptBlock scriptBlock, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(scriptBlock);
		message.setHeader("action", action);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(ScriptBlock scriptBlock, EntityAction action, User currentUser) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(scriptBlock);
		message.setHeader("action", action);
		message.setHeader("user", currentUser);
		return message;
	}

    @Override
    public boolean notifyUpdate(ScriptBlock scriptBlock) {
        return sendMessage(createAsyncMessage(scriptBlock, EntityAction.UPDATED));    
    }
    
	@Override
	public boolean notifyInsert(ScriptBlock block) {
		return sendMessage(createAsyncMessage(block, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyDelete(ScriptBlock block) {
		return sendMessage(createAsyncMessage(block, EntityAction.DELETED));
	}

    @Override
    public boolean notifyMove(ScriptBlock block, User currentUser) {
		return sendMessage(createAsyncMessage(block, EntityAction.MOVED, currentUser));
    }
}
