package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.messenger.TwitterMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.TwitterMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexTwitterMessenger extends AbstractFlexMessenger implements TwitterMessenger {

	private static final String NEW_TWEETS = "newTweetsTwitterUser";
	private static final String TWITTER_USER_DELETED = "deletedTwitterUser";
	private static final String TWITTER_USER_INSERTED = "insertedTwitterUser";
	
    public FlexTwitterMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean notifyTwitterUserInserted(TwitterUser twitterUser) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("type", TWITTER_USER_INSERTED);
		message.setBody(twitterUser);
	    return sendMessage(message);
	}

	@Override
	public boolean notifyTwitterUserDeleted(TwitterUser twitterUser) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("type", TWITTER_USER_DELETED);
		message.setBody(twitterUser);
	    return sendMessage(message);
	}

	@Override
	public boolean notifyTweetsLoaded(TwitterUser twitterUser) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("type", NEW_TWEETS);
		message.setBody(twitterUser);
	    return sendMessage(message);
	}

}
