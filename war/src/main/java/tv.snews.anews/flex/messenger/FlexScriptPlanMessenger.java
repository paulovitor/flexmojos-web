package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.domain.ScriptPlanAction;
import tv.snews.anews.messenger.ScriptPlanMessenger;

/**
 * Implementação da interface {@link ScriptPlanMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 *
 * @author Maxuel Ramos
 * @since 1.7
 */
public class FlexScriptPlanMessenger extends AbstractFlexMessenger implements ScriptPlanMessenger {

	public FlexScriptPlanMessenger(String destination) {
		super(destination);
	}

	//------------------------------
	//	Notifications
	//------------------------------

	@Override
	public boolean notifyUpdateField(long planId, String field, Object value) {
		return sendMessage(createAsyncMessage(planId, EntityAction.FIELD_UPDATED, field, value));
	}

	@Override
	public boolean notifySort(ScriptPlan scriptPlan) {
		return sendMessage(createAsyncMessage(scriptPlan, ScriptPlanAction.SORTED));
	}

	@Override
	public boolean notifyLock(ScriptPlan scriptPlan) {
		return sendMessage(createAsyncMessage(scriptPlan, EntityAction.LOCKED));
	}

	@Override
	public boolean notifyUnlock(ScriptPlan scriptPlan) {
		return sendMessage(createAsyncMessage(scriptPlan, EntityAction.UNLOCKED));
	}

    @Override
    public boolean notifyDisplayResets(ScriptPlan scriptPlan) {
        return sendMessage(createAsyncMessage(scriptPlan, ScriptPlanAction.DISPLAY_RESETS));
    }

    @Override
    public boolean notifyCompleteDisplay(ScriptPlan scriptPlan) {
        return sendMessage(createAsyncMessage(scriptPlan, ScriptPlanAction.COMPLETE_DISPLAY));
    }

	//------------------------------
	//	Helpers
	//------------------------------

	private AsyncMessage createAsyncMessage(ScriptPlan scriptPlan, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(scriptPlan);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(long planId, EntityAction action, String field, Object value) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("planId", planId);
		message.setHeader("field", field);
		message.setHeader("value", value);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(ScriptPlan scriptPlan, ScriptPlanAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(scriptPlan);
		message.setHeader("action", action);
		return message;
	}
}
