package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.RundownAction;
import tv.snews.anews.messenger.RundownMessenger;

/**
 * Implementação da interface {@link RundownMessenger} utilizando o envio de 
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class FlexRundownMessenger extends AbstractFlexMessenger implements RundownMessenger {

	public FlexRundownMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Rundown rundown) {
		return sendMessage(createAsyncMessage(rundown, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdateField(long rundownId, String field, Object value) {
		return sendMessage(createAsyncMessage(rundownId, EntityAction.FIELD_UPDATED, field, value));
	}

	@Override
	public boolean notifyUpdate(Rundown rundown) {
		return sendMessage(createAsyncMessage(rundown, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Rundown rundown) {
		return sendMessage(createAsyncMessage(rundown, EntityAction.DELETED));
	}

	@Override
	public boolean notifySort(Rundown rundown) {
		return sendMessage(createAsyncMessage(rundown, RundownAction.SORTED));
	}

    @Override
    public boolean notifyDisplayResets(Rundown rundown) {
        return sendMessage(createAsyncMessage(rundown, RundownAction.DISPLAY_RESETS));
    }

    @Override
    public boolean notifyCompleteDisplay(Rundown rundown) {
        return sendMessage(createAsyncMessage(rundown, RundownAction.COMPLETE_DISPLAY));
    }

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Rundown rundown, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(rundown);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(Rundown rundown, RundownAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(rundown);
		message.setHeader("action", action);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(long rundownId, EntityAction action, String field, Object value) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("rundownId", rundownId);
		message.setHeader("field", field);
		message.setHeader("value", value);
		message.setHeader("action", action);
		return message;
	}
}
