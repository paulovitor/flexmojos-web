package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.ReportEvent;
import tv.snews.anews.messenger.ReportEventMessenger;

/**
 * Implementação da interface {@link ReportEventMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class FlexReportEventMessenger extends AbstractFlexMessenger implements ReportEventMessenger {

	public FlexReportEventMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(ReportEvent event) {
		return sendMessage(createAsyncMessage(event, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(ReportEvent event) {
		return sendMessage(createAsyncMessage(event, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(ReportEvent event) {
		return sendMessage(createAsyncMessage(event, EntityAction.DELETED));
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(ReportEvent event, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(event);
		message.setHeader("action", action);
		return message;
	}
}
