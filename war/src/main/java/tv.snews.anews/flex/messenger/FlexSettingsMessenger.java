package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.messenger.SettingsMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.SettingsMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexSettingsMessenger extends AbstractFlexMessenger implements SettingsMessenger {

    public FlexSettingsMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean notifySettingsChanged(Settings settings) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(settings);
		return sendMessage(message);
	}

}
