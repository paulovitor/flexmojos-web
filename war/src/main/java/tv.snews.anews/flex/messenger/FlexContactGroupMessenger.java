package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.ContactGroup;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.ContactGroupMessenger;

/**
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexContactGroupMessenger extends AbstractFlexMessenger implements ContactGroupMessenger {

	public FlexContactGroupMessenger(String destination) {
	    super(destination);
    }
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(ContactGroup contactGroup) {
		return sendMessage(createAsyncMessage(contactGroup, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(ContactGroup contactGroup) {
		return sendMessage(createAsyncMessage(contactGroup, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(ContactGroup contactGroup) {
		return sendMessage(createAsyncMessage(contactGroup, EntityAction.DELETED));
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(ContactGroup contactGroup, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(contactGroup);
		message.setHeader("action", action);
		return message;
	}
}
