/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.MediaCenter;
import tv.snews.anews.messenger.MediaCenterMessenger;

/**
 * Implementação da interface {@link MediaCenterMessenger} utilizando o suporte do Flex
 * para o envio de mensagens.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.5
 */
public class FlexMediaCenterMessenger extends AbstractFlexMessenger implements MediaCenterMessenger {

	public FlexMediaCenterMessenger(String destination) {
		super(destination);
	}
	
	 //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifySearchSchema(Object obj, String username) {
		return sendMessage(createAsyncMessage(obj, username, MediaCenter.SEARCH_SCHEMA));
	}

	//--------------------------------------------------------------------------
  	//	Helpers
  	//--------------------------------------------------------------------------
  	
  	private AsyncMessage createAsyncMessage(Object obj, String username, MediaCenter action) {
  		AsyncMessage message = createAsyncMessage();
  		message.setBody(obj);
		message.setHeader("username", username);
  		message.setHeader("action", action);
  		return message;
  	}
}
