package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Team;
import tv.snews.anews.messenger.TeamMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.TeamMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 *
 * @author Maxuel Ramos
 * @since 1.0.0
 */
public class FlexTeamMessenger extends AbstractFlexMessenger implements TeamMessenger {

    public FlexTeamMessenger(String destination) {
	    super(destination);
    }

    //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
    
    @Override
    public boolean notifyInsert(Team team) {
	    return sendMessage(createAsyncMessage(team, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyUpdate(Team team) {
	    return sendMessage(createAsyncMessage(team, EntityAction.UPDATED));
    }

    @Override
    public boolean notifyDelete(Team team) {
	    return sendMessage(createAsyncMessage(team, EntityAction.DELETED));
    }
	
    //--------------------------------------------------------------------------
  	//	Helpers
  	//--------------------------------------------------------------------------
  	
  	private AsyncMessage createAsyncMessage(Team team, EntityAction action) {
  		AsyncMessage message = createAsyncMessage();
  		message.setBody(team);
  		message.setHeader("action", action);
  		return message;
  	}
	
}
