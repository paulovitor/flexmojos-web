package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.ReportageMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ReportageMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexReportageMessenger extends AbstractFlexMessenger implements ReportageMessenger {

	public FlexReportageMessenger(String destination) {
		super(destination);
	}

	@Override
	public boolean notifyInsert(Reportage reportage) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Reportage reportage) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyFieldUpdate(Reportage reportage, String field, Object value) {
		AsyncMessage message = createAsyncMessage(reportage, EntityAction.FIELD_UPDATED);
		message.setHeader("field", field);
		message.setHeader("value", value);
		return sendMessage(message);
	}

	@Override
	public boolean notifyDelete(Reportage reportage) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.DELETED));
	}

	@Override
	public boolean notifyRestore(Reportage reportage) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.RESTORED));
	}

	@Override
	public boolean notifyLock(Reportage reportage) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.LOCKED));
	}

	@Override
	public boolean notifyUnlock(Reportage reportage) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.UNLOCKED));
	}

	@Override
	public boolean notifyLock(Reportage reportage, User user) {
		return sendMessage(createAsyncMessage(reportage, EntityAction.LOCKED, user));
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private AsyncMessage createAsyncMessage(Reportage reportage, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(reportage);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(Reportage reportage, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(reportage, action);
		message.setHeader("user", user);
		return message;
	}

}
