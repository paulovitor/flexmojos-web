package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.DocumentAction;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.messenger.DocumentMessenger;

/**
 * Implementação da interface
 * {@link tv.snews.anews.messenger.DocumentMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public class FlexDocumentMessenger extends AbstractFlexMessenger implements DocumentMessenger {

	public FlexDocumentMessenger(String destination) {
		super(destination);
	}

	//--------------------------------------------------------------------------
	//	Notifiers
	//--------------------------------------------------------------------------

	@Override
	public boolean notifySentToDrawer(Long[] documents, Object obj) {
		return sendMessage(createAsyncMessage(documents, obj, DocumentAction.SENT_TO_DRAWER));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

	private AsyncMessage createAsyncMessage(Long[] documentIds, Object obj, DocumentAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(documentIds);
		message.setHeader("action", action);
        if(obj instanceof Rundown)
            message.setHeader("rundownId", ((Rundown)obj).getId());
        if(obj instanceof ScriptPlan)
            message.setHeader("scriptPlanId", ((ScriptPlan)obj).getId());
		return message;
	}


}
