package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.ReportMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.ReportMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class FlexReportMessenger extends AbstractFlexMessenger implements ReportMessenger {

	public FlexReportMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Report report) {
		return sendMessage(createAsyncMessage(report, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Report report) {
		return sendMessage(createAsyncMessage(report, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Report report) {
		return sendMessage(createAsyncMessage(report, EntityAction.DELETED));
	}
	
    @Override
    public boolean notifyLock(Report report, User user) {
    	return sendMessage(createAsyncMessage(report, EntityAction.LOCKED, user));
    }

    @Override
    public boolean notifyUnlock(Report report, User user) {
    	return sendMessage(createAsyncMessage(report, EntityAction.UNLOCKED, user));
    }
    
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Report report, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(report);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(Report report, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(report, action);
		message.setHeader("user", user);
		return message;
	}
}