package tv.snews.anews.flex.messenger;

import flex.messaging.MessageBroker;
import flex.messaging.messages.AsyncMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.flex.messaging.MessageTemplate;

/**
 * Classe abstrata que define a base para envio de mensagens utilizando o
 * {@link flex.messaging.MessageBroker} do BlazeDS e o {@link org.springframework.flex.messaging.MessageTemplate} do suporte do
 * Spring para o Flex.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public abstract class AbstractFlexMessenger {

	private static final Logger log = LoggerFactory.getLogger(AbstractFlexMessenger.class);

	protected MessageTemplate messageTemplate;
	protected String destination;

	protected AbstractFlexMessenger(String destination) {
		this.destination = destination;
	}

	/**
	 * Cria uma nova {@link flex.messaging.messages.AsyncMessage} por meio do {@link org.springframework.flex.messaging.MessageTemplate}
	 * atribuido a essa classe.
	 *
	 * @return Mensagem criada.
	 */
	public AsyncMessage createAsyncMessage() {
		AsyncMessage message = messageTemplate.createMessageForDestination(destination);
		message.setDestination(destination);
		return message;
	}

	/**
	 * Faz o envio de uma {@link flex.messaging.messages.AsyncMessage} por meio do {@link flex.messaging.MessageBroker}
	 * em broadcast.
	 * 
	 * @param message Mensagem que será enviada.
	 * @return <code>true</code> caso a mensagem for enviada com sucesso.
	 */
	public boolean sendMessage(AsyncMessage message) {
		boolean delivered = false;
		try {
			MessageBroker messageBroker = messageTemplate.getMessageBroker();
			messageBroker.routeMessageToService(message, null);
			delivered = true;
		} catch (Exception e) {
			log.error("Failed do delivery message through the message broker", e);
		}
		return delivered;
	}
	
    /**
	 * @param messageTemplate 
	 * 			Template do spring-flex para envio de mensagens pelo message broker.
	 */
    public void setMessageTemplate(MessageTemplate messageTemplate) {
	    this.messageTemplate = messageTemplate;
    }

}
