package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Group;
import tv.snews.anews.messenger.GroupMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.GroupMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexGroupMessenger extends AbstractFlexMessenger implements GroupMessenger {

    public FlexGroupMessenger(String destination) {
	    super(destination);
    }

    //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
    
    @Override
    public boolean notifyInsert(Group group) {
	    return sendMessage(createAsyncMessage(group, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyUpdate(Group group) {
	    return sendMessage(createAsyncMessage(group, EntityAction.UPDATED));
    }

    @Override
    public boolean notifyDelete(Group group) {
	    return sendMessage(createAsyncMessage(group, EntityAction.DELETED));
    }
	
    //--------------------------------------------------------------------------
  	//	Helpers
  	//--------------------------------------------------------------------------
  	
  	private AsyncMessage createAsyncMessage(Group group, EntityAction action) {
  		AsyncMessage message = createAsyncMessage();
  		message.setBody(group);
  		message.setHeader("action", action);
  		return message;
  	}
	
}
