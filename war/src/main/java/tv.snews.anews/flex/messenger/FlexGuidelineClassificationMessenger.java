package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.GuidelineClassification;
import tv.snews.anews.messenger.GuidelineClassificationMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.GuidelineClassificationMessenger}
 * utilizando o envio
 * de mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public class FlexGuidelineClassificationMessenger extends AbstractFlexMessenger implements GuidelineClassificationMessenger {

	public FlexGuidelineClassificationMessenger(String destination) {
		super(destination);
	}

	//--------------------------------------------------------------------------
	//	Notifiers
	//--------------------------------------------------------------------------

	@Override
	public boolean notifyInsert(GuidelineClassification guidelineClassification) {
		return sendMessage(createAsyncMessage(guidelineClassification, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(GuidelineClassification guidelineClassification) {
		return sendMessage(createAsyncMessage(guidelineClassification, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(GuidelineClassification guidelineClassification) {
		return sendMessage(createAsyncMessage(guidelineClassification, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

	private AsyncMessage createAsyncMessage(GuidelineClassification guidelineClassification, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(guidelineClassification);
		message.setHeader("action", action);
		return message;
	}

}
