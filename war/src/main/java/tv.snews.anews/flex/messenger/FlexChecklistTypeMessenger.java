package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.ChecklistTypeMessenger;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class FlexChecklistTypeMessenger extends AbstractFlexMessenger implements ChecklistTypeMessenger {

    public FlexChecklistTypeMessenger(String destination) {
        super(destination);
    }

    //----------------------------------
    //	Notifiers
    //----------------------------------

    @Override
    public boolean notifyInsert(ChecklistType type) {
        return sendMessage(createAsyncMessage(type, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyUpdate(ChecklistType type) {
        return sendMessage(createAsyncMessage(type, EntityAction.UPDATED));
    }

    @Override
    public boolean notifyDelete(ChecklistType type) {
        return sendMessage(createAsyncMessage(type, EntityAction.DELETED));
    }

    //----------------------------------
    //	Helpers
    //----------------------------------

    private AsyncMessage createAsyncMessage(ChecklistType type, EntityAction action) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(type);
        message.setHeader("action", action);
        return message;
    }
}
