package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.News;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.NewsMessenger;

/**
 * Implementação da interface {@link FlexNewsMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class FlexNewsMessenger extends AbstractFlexMessenger implements NewsMessenger {
	
    public FlexNewsMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean notifyNewsChanged(News news, EntityAction type) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("type", type);
		message.setBody(news);
	    return sendMessage(message);
	}

    @Override
    public boolean notifyLock(News news, User user) {
	    return sendMessage(createAsyncMessage(news, EntityAction.LOCKED, user));
    }

    @Override
    public boolean notifyUnlock(News news, User user) {
    	return sendMessage(createAsyncMessage(news, EntityAction.UNLOCKED, user));
    }
    
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(News news, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(news);
		message.setHeader("action", action);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(News news, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(news, action);
		message.setHeader("user", user);
		return message;
	}
}
