package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.StoryCG;
import tv.snews.anews.domain.User;
import tv.snews.anews.infra.intelligentinterface.IntelligentInterfaceResult;
import tv.snews.anews.messenger.IntelligentInterfaceMessenger;

import java.util.List;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.IntelligentInterfaceMessenger} utilizando o suporte do
 * Flex para o envio de mensagens.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class FlexIntelligentInterfaceMessenger extends AbstractFlexMessenger implements IntelligentInterfaceMessenger {

    public FlexIntelligentInterfaceMessenger(String destination) {
	    super(destination);
    }

    //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
    
    @Override
    public boolean notifySendError(String error, User user) {
    	AsyncMessage message = createAsyncMessage();
  		message.setBody(error);
  		message.setHeader("currentUser", user);
  		message.setHeader("action", IntelligentInterfaceResult.ERROR);
  		return sendMessage(message);
    }
    
    @Override
    public boolean notifyCreditsSent(List<StoryCG> storyCGList) {
    	AsyncMessage message = createAsyncMessage();
  		message.setBody(storyCGList);
  		message.setHeader("action", IntelligentInterfaceResult.SUCCESS);
  		return sendMessage(message);
    }
}
