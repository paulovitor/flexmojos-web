package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.messenger.BlockMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.BlockMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class FlexBlockMessenger extends AbstractFlexMessenger implements BlockMessenger {

	public FlexBlockMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Block block) {
		return sendMessage(createAsyncMessage(block, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Block block) {
		return sendMessage(createAsyncMessage(block, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyDelete(Block block) {
		return sendMessage(createAsyncMessage(block, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Block block, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(block);
		message.setHeader("action", action);
		return message;
	}
}
