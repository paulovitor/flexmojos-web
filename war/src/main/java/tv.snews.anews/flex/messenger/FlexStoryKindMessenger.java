package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.StoryKind;
import tv.snews.anews.messenger.StoryKindMessenger;

/**
 * Implementação da interface {@link StoryKindMessenger} utilizando o envio de 
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public class FlexStoryKindMessenger extends AbstractFlexMessenger implements StoryKindMessenger {

	public FlexStoryKindMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyKindInserted(StoryKind kind) {
		return sendMessage(createAsyncMessage(kind, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyKindUpdated(StoryKind kind) {
		return sendMessage(createAsyncMessage(kind, EntityAction.UPDATED));
	}

	@Override
	public boolean notifyKindDeleted(StoryKind kind) {
		return sendMessage(createAsyncMessage(kind, EntityAction.DELETED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(StoryKind kind, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(kind);
		message.setHeader("action", action);
		return message;
	}
}
