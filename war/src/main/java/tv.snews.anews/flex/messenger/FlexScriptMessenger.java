package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.*;
import tv.snews.anews.messenger.ScriptMessenger;

import java.util.Collection;

/**
 * Implementação da interface
 * {@link tv.snews.anews.messenger.ScriptPlanMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring FLex.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public class FlexScriptMessenger extends AbstractFlexMessenger implements ScriptMessenger {

	public FlexScriptMessenger(String destination) {
		super(destination);
	}

	//--------------------------------------------------------------------------
	//	Notifiers
	//--------------------------------------------------------------------------

	@Override
	public boolean notifyInsert(Script script) {
		return sendMessage(createAsyncMessage(script, EntityAction.INSERTED));
	}

	@Override
	public boolean notifyUpdate(Script script) {
		return sendMessage(createAsyncMessage(script, EntityAction.UPDATED));
	}

    @Override
    public boolean notifyUpdate(Script script, long scriptPlanId) {
        return sendMessage(createAsyncMessage(script, EntityAction.UPDATED, scriptPlanId));
    }

    @Override
    public boolean notifyInsert(Script script, long scriptPlanId) {
        return sendMessage(createAsyncMessage(script, EntityAction.INSERTED, scriptPlanId));
    }

	@Override
	public boolean notifyUpdateField(long scriptId, String field, Object value) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("scriptId", scriptId);
		message.setHeader("field", field);
		message.setHeader("value", value);
		message.setHeader("action", EntityAction.FIELD_UPDATED);

		return sendMessage(message);
	}

	@Override
	public boolean notifyDelete(Script script) {
		return sendMessage(createAsyncMessage(script, EntityAction.DELETED));
	}

    @Override
    public boolean notifyDelete(Script script, long scriptPlanId) {
        return sendMessage(createAsyncMessage(script, EntityAction.DELETED, scriptPlanId));
    }

    @Override
    public boolean notifyMultipleExclusion(Long [] ids, Long scriptPlanId) {
        return sendMessage(createAsyncMessage(ids, ScriptAction.MULTIPLE_EXCLUSION, scriptPlanId));
    }

    @Override
    public boolean notifyMove(Collection<Script> scripts, long scriptPlanId) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(scripts);
        message.setHeader("action", EntityAction.MOVED);
        message.setHeader("multiple", true);
        message.setHeader("scriptPlanId", scriptPlanId);
        return sendMessage(message);
    }

    @Override
	public boolean notifyMove(Script script, User user, boolean dragNDrop) {
		AsyncMessage message = createAsyncMessage(script, EntityAction.MOVED, user);
		message.setHeader("dragNDrop", dragNDrop);
		return sendMessage(message);
	}

	@Override
	public boolean notifyMove(Collection<Script> scripts, User user) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("action", EntityAction.MOVED);
		message.setHeader("user", user);
		message.setBody(scripts);
		return sendMessage(message);
	}

    @Override
	public boolean notifyDrawerChange(Script script) {
		return sendMessage(createAsyncMessage(script, ScriptPlanAction.DRAWER));
	}

	@Override
	public boolean notifyRestore(Script script) {
		return sendMessage(createAsyncMessage(script, EntityAction.RESTORED));
	}

	@Override
	public boolean notifyLock(Script script, User user) {
		return sendMessage(createAsyncMessage(script, EntityAction.LOCKED, user));
	}

	@Override
	public boolean notifyUnlock(Script script) {
		return sendMessage(createAsyncMessage(script, EntityAction.UNLOCKED));
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

    private AsyncMessage createAsyncMessage(Script script, EntityAction action, long scriptPlanId) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(script);
        message.setHeader("action", action);
        message.setHeader("multiple", false);
        message.setHeader("scriptPlanId", scriptPlanId);
        return message;
    }

	private AsyncMessage createAsyncMessage(Script script, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(script);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(Script script, ScriptPlanAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(script);
		message.setHeader("action", action);
		return message;
	}

	private AsyncMessage createAsyncMessage(Script script, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(script, action);
		message.setHeader("user", user);
		return message;
	}

    private AsyncMessage createAsyncMessage(Long[] ids, ScriptAction action, Long scriptPlanId) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(ids);
        message.setHeader("action", action);
        message.setHeader("scriptPlanId", scriptPlanId);
        return message;
    }

}
