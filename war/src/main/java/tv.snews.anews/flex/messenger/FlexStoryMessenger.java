package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.*;
import tv.snews.anews.messenger.StoryMessenger;

import java.util.Collection;

/**
 * Implementação da interface {@link StoryMessenger} utilizando o envio de 
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class FlexStoryMessenger extends AbstractFlexMessenger implements StoryMessenger {

	public FlexStoryMessenger(String destination) {
		super(destination);
	}
	
	//--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
	
	@Override
	public boolean notifyInsert(Story story, long rundownId) {
		return sendMessage(createAsyncMessage(story, EntityAction.INSERTED, rundownId));
	}

	@Override
	public boolean notifyUpdateField(long storyId, String field, Object value, long rundownId) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("storyId", storyId);
		message.setHeader("field", field);
		message.setHeader("value", value);
		message.setHeader("action", EntityAction.FIELD_UPDATED);
		message.setHeader("rundownId", rundownId);
		return sendMessage(message);
	}

	@Override
	public boolean notifyUpdate(Story story, long rundownId) {
		return sendMessage(createAsyncMessage(story, EntityAction.UPDATED, rundownId));
	}

	@Override
	public boolean notifyDelete(Story story, long rundownId) {
		AsyncMessage message = createAsyncMessage(story, EntityAction.DELETED, rundownId);
		return sendMessage(message);
	}
	
	@Override
	public boolean notifyMove(Story story, long rundownId) {
		return sendMessage(createAsyncMessage(story, EntityAction.MOVED, rundownId));
	}

	@Override
	public boolean notifyMove(Collection<Story> stories, long rundownId) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(stories);
		message.setHeader("action", EntityAction.MOVED);
		message.setHeader("multiple", true);
		message.setHeader("rundownId", rundownId);
		return sendMessage(message);
	}
	
	@Override
	public boolean notifyDrawerChange(Story story) {
		return sendMessage(createAsyncMessage(story, RundownAction.DRAWER, -1));
	}

	@Override
	public boolean notifyLock(Story story, User user, long rundownId) {
		return sendMessage(createAsyncMessage(story, EntityAction.LOCKED, user, rundownId));
	}

	@Override
	public boolean notifyUnlock(Story story, long rundownId) {
		return sendMessage(createAsyncMessage(story, EntityAction.UNLOCKED, rundownId));
	}
	
	@Override
	public boolean notifyLockDragging(Story story, User user, long rundownId) {
		return sendMessage(createAsyncMessage(story, RundownAction.DRAG_LOCKED, user, rundownId));
	}
	
	@Override
	public boolean notifyUnlockDragging(Story story, long rundownId) {
		return sendMessage(createAsyncMessage(story, RundownAction.DRAG_UNLOCKED, rundownId));
	}

    @Override
    public boolean notifyMultipleExclusion(Long[] ids, long rundownId) {
        return sendMessage(createAsyncMessage(ids, StoryAction.MULTIPLE_EXCLUSION, rundownId));
    }

    @Override
	public boolean notifyRestore(Story story, long rundownId) {
	    return sendMessage(createAsyncMessage(story, EntityAction.RESTORED, rundownId));
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Story story, EntityAction action, long rundownId) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(story);
		message.setHeader("action", action);
		message.setHeader("multiple", false);
		message.setHeader("rundownId", rundownId);
		return message;
	}

	private AsyncMessage createAsyncMessage(Story story, RundownAction action, long rundownId) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(story);
		message.setHeader("action", action);
		message.setHeader("multiple", false);
		message.setHeader("rundownId", rundownId);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(Story story, EntityAction action, User user, long rundownId) {
		AsyncMessage message = createAsyncMessage(story, action, rundownId);
		message.setHeader("user", user);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(Story story, RundownAction action, User user, long rundownId) {
		AsyncMessage message = createAsyncMessage(story, action, rundownId);
		message.setHeader("user", user);
		return message;
	}

    private AsyncMessage createAsyncMessage(Long[] documents, StoryAction action, long rundownId) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(documents);
        message.setHeader("action", action);
        message.setHeader("rundownId", rundownId);
        return message;
    }
    

}
