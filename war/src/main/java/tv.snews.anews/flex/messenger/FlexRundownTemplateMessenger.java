package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.RundownTemplate;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.RundownTemplateMessenger;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class FlexRundownTemplateMessenger extends AbstractFlexMessenger implements RundownTemplateMessenger {

    public FlexRundownTemplateMessenger(String destination) {
        super(destination);
    }

    //----------------------------------
    //  Notifiers
    //----------------------------------

    @Override
    public boolean notifyLock(RundownTemplate template, User user) {
        return sendMessage(createAsyncMessage(template, EntityAction.LOCKED, user));
    }

    @Override
    public boolean notifyUnlock(RundownTemplate template) {
        return sendMessage(createAsyncMessage(template, EntityAction.UNLOCKED));
    }

    //----------------------------------
    //  Helpers
    //----------------------------------

    private AsyncMessage createAsyncMessage(RundownTemplate template, EntityAction action, User user) {
        AsyncMessage message = createAsyncMessage(template, action);
        message.setHeader("user", user);
        return message;
    }

    private AsyncMessage createAsyncMessage(RundownTemplate template, EntityAction action) {
        AsyncMessage message = createAsyncMessage();
        message.setBody(template);
        message.setHeader("action", action);
        return message;
    }
}
