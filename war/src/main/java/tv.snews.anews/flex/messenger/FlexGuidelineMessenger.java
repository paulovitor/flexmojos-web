package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.User;
import tv.snews.anews.messenger.GuidelineMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.GuidelineMessenger} utilizando o envio de
 * mensagens oferecido pelo BlazeDS e Spring Flex.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexGuidelineMessenger extends AbstractFlexMessenger implements GuidelineMessenger {

    public FlexGuidelineMessenger(String destination) {
	    super(destination);
    }

    //--------------------------------------------------------------------------
  	//	Notifiers
  	//--------------------------------------------------------------------------
    
    @Override
    public boolean notifyInsert(Guideline guideline) {
		return sendMessage(createAsyncMessage(guideline, EntityAction.INSERTED));
    }

    @Override
    public boolean notifyUpdate(Guideline guideline) {
		return sendMessage(createAsyncMessage(guideline, EntityAction.UPDATED));
    }
    
    @Override
    public boolean notifyDelete(Guideline guideline) {
		return sendMessage(createAsyncMessage(guideline, EntityAction.DELETED));
    }

    @Override
    public boolean notifyRestore(Guideline guideline) {
		return sendMessage(createAsyncMessage(guideline, EntityAction.RESTORED));
    }
    
	@Override
	public boolean notifyLock(Guideline guideline, User user) {
		return sendMessage(createAsyncMessage(guideline, EntityAction.LOCKED, user));
	}

	@Override
	public boolean notifyUnlock(Guideline guideline, User user) {
		return sendMessage(createAsyncMessage(guideline, EntityAction.UNLOCKED, user));
	}

    @Override
    public boolean notifyFieldUpdate(Guideline guideline, String field, Object value) {
        AsyncMessage message = createAsyncMessage(guideline, EntityAction.FIELD_UPDATED);
        message.setHeader("field", field);
        message.setHeader("value", value);
        return sendMessage(message);
    }

    //--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private AsyncMessage createAsyncMessage(Guideline guideline, EntityAction action) {
		AsyncMessage message = createAsyncMessage();
		message.setBody(guideline);
		message.setHeader("action", action);
		return message;
	}
	
	private AsyncMessage createAsyncMessage(Guideline guideline, EntityAction action, User user) {
		AsyncMessage message = createAsyncMessage(guideline, action);
		message.setHeader("user", user);
		return message;
	}
}
