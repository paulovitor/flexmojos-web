package tv.snews.anews.flex.messenger;

import flex.messaging.messages.AsyncMessage;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.messenger.RssMessenger;

/**
 * Implementação da interface {@link tv.snews.anews.messenger.RssMessenger} utilizando o suporte do Flex
 * para o envio de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexRssMessenger extends AbstractFlexMessenger implements RssMessenger {

	/**
	 * Tipos de notificações que podem ser enviadas.
	 */
	private enum NotificationType {
		FEED_INSERTED, 
		FEED_UPDATED, 
		FEED_DELETED,
		CATEGORY_INSERTED, 
		CATEGORY_UPDATED, 
		CATEGORY_DELETED
	}
	
    public FlexRssMessenger(String destination) {
	    super(destination);
    }
	
	@Override
	public boolean notifyFeedInserted(RssFeed feed) {
		AsyncMessage message = createFeedNotification(feed, NotificationType.FEED_INSERTED);
	    return sendMessage(message);
	}

	@Override
	public boolean notifyFeedUpdated(RssFeed feed) {
		AsyncMessage message = createFeedNotification(feed, NotificationType.FEED_UPDATED);
	    return sendMessage(message);
	}

	@Override
	public boolean notifyFeedDeleted(RssFeed feed) {
		AsyncMessage message = createFeedNotification(feed, NotificationType.FEED_DELETED);
	    return sendMessage(message);
	}
	
    @Override
    public boolean notifyCategoryInserted(RssCategory category) {
    	AsyncMessage message = createCategoryNotification(category, NotificationType.CATEGORY_INSERTED);
	    return sendMessage(message);
    }

    @Override
    public boolean notifyCategoryUpdated(RssCategory category) {
    	AsyncMessage message = createCategoryNotification(category, NotificationType.CATEGORY_UPDATED);
	    return sendMessage(message);
    }

    @Override
    public boolean notifyCategoryDeleted(RssCategory category) {
    	AsyncMessage message = createCategoryNotification(category, NotificationType.CATEGORY_DELETED);
	    return sendMessage(message);
    }
    
    //--------------------------------------
    //	Helpers
    //--------------------------------------
    
    private AsyncMessage createFeedNotification(RssFeed feed, NotificationType type) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("type", type);
		message.setBody(feed);
		return message;
	}
    
    private AsyncMessage createCategoryNotification(RssCategory category, NotificationType type) {
		AsyncMessage message = createAsyncMessage();
		message.setHeader("type", type);
		message.setBody(category);
		return message;
	} 
}
