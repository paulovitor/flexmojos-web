package tv.snews.anews.domain;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Representa uma categoria a qual um ou mais {@link RssFeed} podem ser
 * atribuidos.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssCategory extends AbstractEntity<Integer> {

    private static final long serialVersionUID = -6795822530296728879L;

    private String name;
    private int color;
	
    private SortedSet<RssFeed> feeds = new TreeSet<RssFeed>();
    
    /**
	 * Adiciona um {@link RssFeed} a essa categoria e define a categoria do
	 * feed. Caso o feed já pertencia a essa categoria, <code>false</code> será
	 * retornado.
	 * 
	 * @param feed {@link RssFeed} que será adicionado a essa categoria.
	 * @return <code>false</code> caso o feed já pertencia a essa categoria.
	 */
    public boolean addFeed(RssFeed feed) {
    	feed.setCategory(this);
    	return feeds.add(feed);
    }
    
    @Override
    public int hashCode() {
	    final int prime = 31;
	    int result = super.hashCode();
	    result = prime * result + ((name == null) ? 0 : name.hashCode());
	    return result;
    }

    @Override
    public boolean equals(Object obj) {
    	if (this == obj) {
    		return true;
    	}
    	if (obj instanceof RssCategory) {
    		RssCategory other = (RssCategory) obj;
    		return name == other.name || (name != null && name.equals(other.name));
    	}
	    return false;
    }

    //--------------------------------------
    //	Getters & Setters
    //--------------------------------------
    
	/**
     * @return Nome da categoria
     */
    public String getName() {
    	return name;
    }
	
    /**
     * @param name Nome da categoria
     */
    public void setName(String name) {
    	this.name = name;
    }
	
    /**
     * @return Cor da categoria
     */
    public int getColor() {
    	return color;
    }
	
    /**
     * @param color Cor da categoria
     */
    public void setColor(int color) {
    	this.color = color;
    }
    
    /**
     * @return Lista de {@link RssFeed} dessa categoria
     */
    public SortedSet<RssFeed> getFeeds() {
	    return feeds;
    }
    
    /**
     * @param feeds Lista de {@link RssFeed} dessa categoria
     */
    public void setFeeds(SortedSet<RssFeed> feeds) {
	    this.feeds = feeds;
    }
    
//    public void setFeeds(Collection<RssFeed> feeds) {
//    	this.feeds.clear();
//    	this.feeds.addAll(feeds);
//    }
}
