package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;

/**
 * Domínio que representa uma cidade.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/city")
public class City extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 7306230120041227209L;

	@Field
	private String name;

	private State state;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

}
