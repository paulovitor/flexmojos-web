package tv.snews.anews.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.annotations.Indexed;
import tv.snews.anews.infra.search.DocumentIndexingInterceptor;
import tv.snews.anews.util.DateTimeUtil;

import javax.persistence.Entity;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static tv.snews.anews.util.DateTimeUtil.*;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
@Entity
@Indexed(index = "indexes/script", interceptor = DocumentIndexingInterceptor.class)
public class Script extends Document<ScriptRevision> implements Comparable<Script> {

    public static enum Fields {
        APPROVED, EDITOR, IMAGE_EDITOR, OK, PAGE, PRESENTER, REPORTER, SLUG, SYNOPSIS, TAPE, VT, DISPLAYING;
    }

	private int order;

	private String page;
	private String tape;
	private String synopsis;
	private String mosStatus;

	private Date vt;

	private boolean ok = false;
	private boolean approved = false;
	private boolean blank = true;
	private boolean hasVideos = false;

	private User editor;
	private User imageEditor;
	private User reporter;
	private User presenter;

	private ScriptBlock block;
	private Guideline guideline;
	private Reportage reportage;
	private Script sourceScript;

    private boolean displaying = false;
    private boolean displayed = false;
    private boolean cutted = false;
    private boolean copied = false;

	private SortedSet<ScriptImage> images = new TreeSet<>();
	private SortedSet<ScriptVideo> videos = new TreeSet<>();
    private SortedSet<AbstractScriptItem> cgs = new TreeSet<>();

	private transient User editingUser;

	//------------------------------
	//	Constructors
	//------------------------------

	// Hibernate & AMF
	public Script() {}

	public Script(User author) {
		this.author = author;
		this.vt = getMidnightTime();
        this.state = DocumentState.PRODUCING;

		if (block != null && block.getScriptPlan() != null) {
			this.date = block.getScriptPlan().getDate();
			this.program = block.getScriptPlan().getProgram();
		}
	}

	public Script(Guideline guideline, User author, int order) {
		this(author);
		this.guideline = guideline;
		this.order = order;
		this.slug = guideline.getSlug();
	}

	public Script(Reportage reportage, User author, int order) {
		this(author);
		this.reportage = reportage;
		this.order = order;
		this.slug = reportage.slug;
	}

	// Construtor para criar cópia
	public Script(Script original) throws IOException {
        this.copyData(original);
		this.copyScriptData(original);

		this.sourceScript = original;
		this.mosStatus = original.getMosStatus();
		this.ok = original.isOk();
		this.approved = original.isApproved();
        this.blank = original.isBlank();
		this.reporter = original.getReporter();
		this.guideline = original.getGuideline();
		this.reportage = original.getReportage();
		this.date = original.getDate();
		this.author = original.getAuthor();
		this.guideline = original.getGuideline();

        copyImages(original);
        copyVideos(original);
        copyCGs(original);
    }

    //------------------------------
	//	Operations
	//------------------------------

    private void copyImages(Script original) throws IOException {
        for (ScriptImage image : original.getImages()) {
            addImage(new ScriptImage(image));
        }
    }

    private void copyVideos(Script original) {
        for (ScriptVideo video : original.getVideos()) {
            addVideo(new ScriptVideo(video));
        }
    }

    private void copyCGs(Script original) {
        for (AbstractScriptItem cg : original.getCgs()) {
            if (cg instanceof ScriptMosCredit) {
                addCG(new ScriptMosCredit((ScriptMosCredit) cg));
            } else if (cg instanceof ScriptCG) {
                addCG(new ScriptCG((ScriptCG) cg));
            }
        }
    }

    public void addImage(ScriptImage image) {
        image.setOrder(this.images.size());
        image.setScript(this);
        this.images.add(image);
    }

    public void addVideo(ScriptVideo video) {
        video.setOrder(this.videos.size());
        video.setScript(this);
        this.videos.add(video);
    }

    private void addCG(AbstractScriptItem cg) {
        cg.setOrder(this.cgs.size());
        cg.setScript(this);
        this.cgs.add(cg);
    }

    public void copyScriptData(Script other){
        this.slug = other.getSlug();
        this.tape = other.getTape();
        this.presenter = other.getPresenter();
        this.reporter = other.getReporter();
        this.vt = other.getVt();
        this.synopsis = other.getSynopsis();
        this.hasVideos = other.getHasVideos();
    }

	@Override
	public int compareTo(Script other) {
        int diff;

        // Trata a possibilidade de bloco nulo
        if (block == other.block) {
            diff = 0;
        } else if (block == null) {
            return 1;
        } else if (other.block == null) {
            return -1;
        } else {
            diff = block.compareTo(other.block);
        }

        // Se forem de blocos diferentes, basea-se na ordem dos blocos, senão basea-se na ordem das laudas
        return diff != 0 ? diff : order - other.order;
	}

	/**
	 * Busca pelo primeiro script ancestral que seja da gaveta.
	 *
	 * @return o script da gaveta original ou {@code null} caso não exista
	 */
	public Script findDrawerOrigin() {
		return findDrawerOrigin(sourceScript);
	}

	/*
	 * Busca recursivamente pelo script da gaveta.
	 */
	private Script findDrawerOrigin(Script script) {
		if (script == null) {
			return null;
		}
		return findDrawerOrigin(script.sourceScript);
	}

	public void updateMainFields(Script other) throws IOException {
		copyScriptData(other);
        this.videos = other.getVideos();
        this.images = other.getImages();
        this.cgs = other.getCgs();
    }

    public boolean hasContent() {
        return StringUtils.isEmpty(this.synopsis) &&
                (this.images == null || this.images.size() == 0) &&
                (this.videos == null || this.videos.size() == 0) &&
                (this.cgs == null || this.cgs.size() == 0);
    }
    
    public boolean verifyHasVideos() {
        return videos != null && videos.size() > 0;
    }

	public String reporterNickname() {
		if (reporter != null) {
			return reporter.getNickname();
		}
		if (reportage != null && reportage.getReporter() != null) {
			return reportage.getReporter().getNickname();
		}
		if (guideline != null) {
			return guideline.reportersToString();
		}
		return null;
	}

	public List<List> loadInputStreamImages() {
        List<List> imageList = new ArrayList<>();
        List<InputStream> inputStreamImages = new ArrayList<>();
        int countSize = 0;
        for(ScriptImage scriptImage : images) {
            if(inputStreamImages.size() < 4 ) {
                inputStreamImages.add(new ByteArrayInputStream(scriptImage.getThumbnailBytes()));
            } else {
                imageList.add(inputStreamImages);
                inputStreamImages = new ArrayList<>();
                inputStreamImages.add(new ByteArrayInputStream(scriptImage.getThumbnailBytes()));
            }
            countSize++;
            if(countSize == images.size()) {
                imageList.add(inputStreamImages);
            }
        }
        return imageList;
	}

	public void updateVTTime() {
		Date duration= DateTimeUtil.getMidnightTime();
		for (ScriptVideo video : this.getVideos()) {
			duration = sumTimes(duration, video.getDurationTime());
		}
	    this.vt = duration;
	}

    public String obtainReporterNickname() {
        if (guideline != null && !guideline.getReporters().isEmpty()) {
            return guideline.getReporters().iterator().next().getNickname();
        }
        if (reportage != null && reportage.getReporter() != null) {
            return reportage.getReporter().getNickname();
        }
        if (reporter != null) {
            return reporter.getNickname();
        }
        return "";
    }

    /**
     * Retorna todas os ScriptVideo do tipo informado.
     *
     * @param type tipo desejado
     * @return os ScriptVideo encontradas
     */
    public <T extends Media> List<ScriptVideo> scriptVideosOfType(Class<T> type) {
        List<ScriptVideo> results = new ArrayList<>();

        for (ScriptVideo video : videos) {
            if (video.getMedia() != null) {
                if (type.isInstance(video.getMedia())) {
                    results.add(video);
                }
            }
        }

        return results;
    }

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getTape() {
		return tape;
	}

	public void setTape(String tape) {
		this.tape = tape;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getMosStatus() {
		return mosStatus;
	}

	public void setMosStatus(String mosStatus) {
		this.mosStatus = mosStatus;
	}

	public Date getVt() {
		return vt;
	}

	public void setVt(Date vt) {
		this.vt = vt == null ? getMidnightTime() : addTimeToCurrentDate(vt);
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

    public boolean isBlank() { return blank; }

    public void setBlank(boolean blank) { this.blank = blank; }

	public User getEditor() {
		return editor;
	}

	public void setEditor(User editor) {
		this.editor = editor;
	}

    public User getImageEditor() {
        return imageEditor;
    }

    public void setImageEditor(User imageEditor) {
        this.imageEditor = imageEditor;
    }

	public User getReporter() {
		return reporter;
	}

	public void setReporter(User reporter) {
		this.reporter = reporter;
	}

	public User getPresenter() {
		return presenter;
	}

	public void setPresenter(User presenter) {
		this.presenter = presenter;
	}

	public ScriptBlock getBlock() {
		return block;
	}

	public void setBlock(ScriptBlock block) {
		this.block = block;

		if (block != null && block.getScriptPlan() != null) {
			this.date = block.getScriptPlan().getDate();
			this.program = block.getScriptPlan().getProgram();
		}
	}

	public Guideline getGuideline() {
		return guideline;
	}

	public void setGuideline(Guideline guideline) {
		this.guideline = guideline;
	}

	public Reportage getReportage() {
		return reportage;
	}

	public void setReportage(Reportage reportage) {
		this.reportage = reportage;
	}

	public Script getSourceScript() {
		return sourceScript;
	}

	public void setSourceScript(Script sourceScript) {
		this.sourceScript = sourceScript;
	}

	public SortedSet<ScriptImage> getImages() {
		return images;
	}

	public void setImages(SortedSet<ScriptImage> images) {
		this.images = images;
	}

	public SortedSet<ScriptVideo> getVideos() {
		return videos;
	}

	public void setVideos(SortedSet<ScriptVideo> videos) {
		this.videos = videos;
	}

    public SortedSet<AbstractScriptItem> getCgs() {
        return cgs;
    }

    public void setCgs(SortedSet<AbstractScriptItem> cgs) {
        this.cgs = cgs;
    }

	public User getEditingUser() {
		return editingUser;
	}

	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}
	
	public boolean getHasVideos() {
    	return hasVideos;
    }
	
	public void setHasVideos(boolean hasVideos) {
    	this.hasVideos = hasVideos;
    }

	private static final long serialVersionUID = 5267070626406120560L;

    public boolean isDisplaying() {
        return displaying;
    }

    public void setDisplaying(boolean displaying) {
        this.displaying = displaying;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public boolean isCutted() {
        return cutted;
    }

    public void setCutted(boolean cutted) {
        this.cutted = cutted;
    }

    public boolean isCopied() {
        return copied;
    }

    public void setCopied(boolean copied) {
        this.copied = copied;
    }
}
