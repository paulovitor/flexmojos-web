package tv.snews.anews.domain;

import org.apache.commons.lang.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import static org.apache.commons.lang.StringUtils.stripToEmpty;
import static tv.snews.anews.util.DateTimeUtil.dateToDateFormat;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class ChecklistRevision extends AbstractRevision<Checklist> {

    private String date;
    private String schedule;
    private String departure;
    private String arrival;

    private String slug;
    private String local;
    private String tapes;
    private String info;
    private String vehicles;

	private String producer;
	private String editors;

	private String type;

    private String resources;
    private String tasks;

    //----------------------------------
    //	Constructors
    //----------------------------------

    // Hibernate & AMF
    public ChecklistRevision() {}

    public ChecklistRevision(Checklist checklist) {
        super(checklist);
    }

    //----------------------------------
    //	Operations
    //----------------------------------

    @Override
    protected void fillRevision(Checklist checklist) {
        this.date = dateToDateFormat(checklist.getDate());
        this.schedule = formatTime(checklist.getSchedule());
        this.departure = formatTime(checklist.getDeparture());
        this.arrival = formatTime(checklist.getArrival());

        this.slug = stripToEmpty(checklist.getSlug());
        this.local = stripToEmpty(checklist.getLocal());
        this.tapes = stripToEmpty(checklist.getTapes());
        this.info = stripToEmpty(checklist.getInfo());

        this.producer = checklist.getProducer() == null ? "" : checklist.getProducer().getNickname();
        this.type = checklist.getType() == null ? "" : checklist.getType().getName();

        this.resources = joinResources(checklist.getResources());
        this.tasks = joinTasks(checklist.getTasks());
        this.vehicles = checklist.vehiclesToString();
        this.editors = checklist.editorsToString();
    }

    //----------------------------------
    //	Helpers
    //----------------------------------

    private String joinResources(Set<ChecklistResource> resources) {
        StringBuilder builder = new StringBuilder();

        for (ChecklistResource resource : resources) {
            builder.append("- ").append(resource.getName()).append('\n');
        }

        return builder.toString().trim();
    }

    private String joinTasks(Set<ChecklistTask> tasks) {
        StringBuilder builder = new StringBuilder();

        for (ChecklistTask task : tasks) {
            builder.append(task.getLabel());

            if (task.isDone()) {
                builder.append(" [OK]");
            }

            builder.append("\n");

            if (StringUtils.isNotBlank(task.getInfo())) {
                builder.append(task.getInfo());
                builder.append("\n");
            }

            builder.append("\n");
        }

        return builder.toString().trim();
    }

    private String formatTime(Date value) {
        if (value != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
            return formatter.format(value);
        }
        return "";
    }

    public ChecklistRevision createCopy(Checklist checklistCopy) {
        ChecklistRevision copy = new ChecklistRevision();

        copy.setRevisionNumber(revisionNumber);
        copy.setRevisionDate(revisionDate);
        copy.setRevisor(revisor);
        copy.setEntity(checklistCopy);

        copy.setDate(date);
        copy.setSchedule(schedule);
        copy.setDeparture(departure);
        copy.setArrival(arrival);

        copy.setSlug(slug);
        copy.setLocal(local);
        copy.setTapes(tapes);
        copy.setInfo(info);
        copy.setVehicles(vehicles);

        copy.setProducer(producer);
        copy.setEditors(editors);

        copy.setType(type);
        copy.setResources(resources);
        copy.setTasks(tasks);

        return copy;
    }

    //----------------------------------
    //	Getters & Setters
    //----------------------------------

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getLocal() {
        return local;
    }

    public String getTapes() {
        return tapes;
    }

    public void setTapes(String tapes) {
        this.tapes = tapes;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResources() {
        return resources;
    }

    public void setResources(String resources) {
        this.resources = resources;
    }

    public String getTasks() {
        return tasks;
    }

    public void setTasks(String tasks) {
        this.tasks = tasks;
    }

    public String getVehicles() {
    	return vehicles;
    }

    public void setVehicles(String vehicles) {
    	this.vehicles = vehicles;
    }
    
    public String getEditors() {
    	return editors;
    }
	
    public void setEditors(String editors) {
    	this.editors = editors;
    }
    
    private static final long serialVersionUID = 1611654227742922010L;
}
