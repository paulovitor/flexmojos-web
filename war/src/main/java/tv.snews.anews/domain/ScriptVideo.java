package tv.snews.anews.domain;

import javax.persistence.Transient;
import java.util.Date;

/**
 * @author Felipe Pinheiro
 */
public class ScriptVideo extends AbstractEntity<Long> implements Comparable<ScriptVideo> {

    private static final long serialVersionUID = -5398113112265070684L;

    private MosDeviceChannel channel;
    private String mosStatus;
    private Media media;
    private String slug = "";
    private Date durationTime;
    private boolean ready;
    private boolean disabled;

    private int order;
    private Script script;

    //------------------------------
    //	Constructors
    //------------------------------

    public ScriptVideo() {
        super();
    }

    public ScriptVideo(ScriptVideo original) {
        this.media = original.getMedia();
        this.slug = original.getSlug();
        this.durationTime = original.getDurationTime();
        this.ready = original.isReady();
        this.disabled = original.isDisabled();
        this.channel = original.getChannel();
        this.mosStatus = original.getMosStatus();
    }

    @Override
    public int compareTo(ScriptVideo other) {
        return order - other.order;
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public MosDeviceChannel getChannel() {
        return channel;
    }

    public void setChannel(MosDeviceChannel channel) {
        this.channel = channel;
    }

    public String getMosStatus() {
        return mosStatus;
    }

    public void setMosStatus(String mosStatus) {
        this.mosStatus = mosStatus;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        updateMediaData(media);
        this.media = media;
    }

    public void updateMediaData(Media media) {
		if(media != null) {
            //this.ready = media.isReady();
            this.durationTime = media.getDurationTime();
            this.slug = media.getSlug();
        }
        this.disabled = (media == null);
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Date durationTime) {
        this.durationTime = durationTime;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    @Transient
    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public MosMedia getMosMedia() {
        return media instanceof MosMedia ? (MosMedia) media : null;
    }

    public WSMedia getWSMedia() {
        return media instanceof WSMedia ? (WSMedia) media : null;
    }
}
