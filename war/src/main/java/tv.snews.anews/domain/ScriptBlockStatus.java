package tv.snews.anews.domain;

/**
 * Representa o tipo de retorno que a função que sincroniza o bloco de apoio pode
 * retornar
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */

public enum ScriptBlockStatus {
    SUCCESSFUL_SYNCHRONIZATION, SCRIPT_PLAN_NOT_FOUND;
}