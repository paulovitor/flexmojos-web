package tv.snews.anews.domain;

import java.util.Collection;

/**
 * Mensagem utilizada para enviar um arquivo para um ou mais usuários pelo
 * sistema de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FileMessage extends AbstractMessage {

	private static final long serialVersionUID = 7150941569665044865L;

	private UserFile file;

	public FileMessage() {
		// Hibernate
	}

	public FileMessage(Chat chat, UserFile file, User sender, Collection<User> recipients) {
		super(chat, sender, recipients);
		this.file = file;
	}

	public FileMessage(Chat chat, UserFile file, User sender, User... recipients) {
		super(chat, sender, recipients);
		this.file = file;
	}
	
	//----------------------------------
	//	Getters & Setters
	//----------------------------------
	
	public UserFile getFile() {
		return file;
	}
	
	public void setFile(UserFile file) {
		this.file = file;
	}
}
