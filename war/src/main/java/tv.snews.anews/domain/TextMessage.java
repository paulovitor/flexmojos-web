package tv.snews.anews.domain;

import java.util.Collection;

/**
 * Classe que representa as mensagens enviadas dentro do sistema, tanto as
 * mensagens da comunicação instantânea quanto as mensagens offline.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class TextMessage extends AbstractMessage {

	private static final long serialVersionUID = 6863108694645220339L;

	private String content;
	
	public TextMessage() {
		// Hibernate
	}

	public TextMessage(Chat chat, String content, User sender, Collection<User> recipients) {
		super(chat, sender, recipients);
		this.content = content;
	}

	public TextMessage(Chat chat, String content, User sender, User... recipients) {
		super(chat, sender, recipients);
		this.content = content;
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
