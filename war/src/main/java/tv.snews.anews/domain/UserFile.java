package tv.snews.anews.domain;

/**
 * Classe que referência um arquivo enviado por um usuário para o servidor.
 * 
 * <p>
 * NOTA: Atualmente esta classe é utilizada pelo sistema mensagens para
 * implementar o envio de arquivos, mas a idéia é que ela seja também utilizada
 * futuramente em outras áreas do sistema onde se efetua o envio de arquivos.
 * </p>
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class UserFile extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 1707874071063274375L;

	private String name;
	private String extension;
	private String path;
	private long size; //bytes
	private User owner;
	
	//----------------------------------
	//	Getters & Setters
	//----------------------------------
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getExtension() {
		return extension;
	}
	
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public long getSize() {
		return size;
	}
	
	public void setSize(long size) {
		this.size = size;
	}
	
	public User getOwner() {
		return owner;
	}
	
	public void setOwner(User owner) {
		this.owner = owner;
	}
}
