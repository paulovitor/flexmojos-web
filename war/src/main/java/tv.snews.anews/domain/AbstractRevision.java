package tv.snews.anews.domain;

import tv.snews.anews.util.StringDiffUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import static java.lang.reflect.Modifier.isFinal;
import static java.lang.reflect.Modifier.isStatic;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public abstract class AbstractRevision<T extends AbstractVersionedEntity<?, ?>> extends AbstractEntity<Long> implements Comparable<AbstractRevision<T>> {

	protected int revisionNumber;
	protected Date revisionDate;
	
	protected User revisor;
	protected T entity;
	
	//----------------------------------
    //	Constructors
    //----------------------------------
	
	protected AbstractRevision() {}
	
	/**
	 * Constroi a revisão com os dados da entidade informada.
	 * 
	 * @param entity dados atuais da entidade
	 */
	public AbstractRevision(T entity) {
		this.entity = entity;
		this.revisor = entity.getAuthor();
		this.revisionDate = new Date();
		this.revisionNumber = entity.nextRevisionNumber();
		
		// Preenche os campos específicos da revisão filha desta classe
		fillRevision(entity);
	}
	
	//----------------------------------
    //	Operations
    //----------------------------------
	
	protected abstract void fillRevision(T entity);
	
	protected boolean contentEquals(AbstractRevision<?> other) throws IllegalArgumentException, IllegalAccessException {
		if (this.getClass().equals(other.getClass())) {
			for (Field field : this.getClass().getDeclaredFields()) {

				if (field.getType().equals(String.class)) {
					field.setAccessible(true);
					
					String thisValue = correctNullOrBlank((String) field.get(this));
					String otherValue = correctNullOrBlank((String) field.get(other));

					if (!thisValue.equals(otherValue)) {
						return false;
					}
				}
			}
			
			return true;
		} else {
			throw new RuntimeException("Cannot compare different revision types.");
		}
	}
	
	/*
	 * Retorna um objeto da subclasse desta classe conténdo as marcações de diff
	 * em relação aos dados deste objeto e o objeto informado via parâmetro.
	 */
	public <R extends AbstractRevision<?>> R computeDiff(R previous) {
		@SuppressWarnings("unchecked")
        R diff = (R) createInstance();

		// Copia as propriedades desta classe para o diff
		for (Field field : getClass().getSuperclass().getDeclaredFields()) {
			if (!isStatic(field.getModifiers()) && !isFinal(field.getModifiers())) {
				try {
					field.setAccessible(true);
	                field.set(diff, field.get(this));
                } catch (IllegalArgumentException | IllegalAccessException e) {
                	throw new RuntimeException("Failed to copy info value to diff object.", e);
                }
			}
		}
		
		// Calcula o diff das propriedades da classe filha e atribui ao diff
		StringDiffUtils strDiff = new StringDiffUtils();
		for (Field field : getClass().getDeclaredFields()) {
			if (field.getType().equals(String.class)) {
				field.setAccessible(true);
				
				try {
					String thisValue = correctNullOrBlank((String) field.get(this));
					String otherValue = correctNullOrBlank((String) field.get(previous));
				
	                field.set(diff, strDiff.prettyHtmlDiff(thisValue, otherValue, true));
                } catch (IllegalArgumentException | IllegalAccessException e) {
	                throw new RuntimeException("Failed to set the diff string on the diff object.", e);
                }
			}
		}
		
		return diff;
	}
	
	@Override
	public int compareTo(AbstractRevision<T> other) {
	    return revisionNumber - other.revisionNumber;
	}
	
	@Override
	public String toString() {
	    return this.getClass().getName() + 
			"{ " +
				"revisionNumber: " + revisionNumber + ", " +
				"revisionDate: " + revisionDate + ", " +
				"revisor: " + (revisor == null ? "null" : revisor.getNickname()) + ", " +
				"entity: " + (entity == null ? "null" : entity.getId()) +
			"}";
	}

	//----------------------------------
    //	Helpers
    //----------------------------------
	
	/*
	 * Cria uma instância da subclasse dessa classe usando o construtor sem
	 * argumentos.
	 */
	private AbstractRevision<?> createInstance() {
		try {
			for (Constructor<?> constructor : getClass().getConstructors()) {
				if (constructor.getParameterTypes().length == 0) {
	                return (AbstractRevision<?>) constructor.newInstance();
				}
			}
			throw new RuntimeException("Could not find a constructor with no arguments.");
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException("Failed to create the diff object instance", e);
        }
	}
	
	private String correctNullOrBlank(String value) {
		return value == null ? "" : value;
	}
	
	//----------------------------------
    //	Getters & Setters
    //----------------------------------
	
    public int getRevisionNumber() {
	    return revisionNumber;
    }
    
    public void setRevisionNumber(int revisionNumber) {
	    this.revisionNumber = revisionNumber;
    }
	
    public Date getRevisionDate() {
	    return revisionDate;
    }
    
    public void setRevisionDate(Date revisionDate) {
	    this.revisionDate = revisionDate;
    }
    
    public User getRevisor() {
	    return revisor;
    }
    
    public void setRevisor(User revisor) {
	    this.revisor = revisor;
    }
    
    public T getEntity() {
	    return entity;
    }
    
    public void setEntity(T entity) {
	    this.entity = entity;
    }
    
    private static final long serialVersionUID = -7434339956489575823L;
}
