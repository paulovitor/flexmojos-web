package tv.snews.anews.domain;

import java.util.Objects;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class MosDeviceChannel extends AbstractEntity<Integer> implements Comparable<MosDeviceChannel> {

	private String name;
	private MosDevice device;

	public MosDeviceChannel() {}

	public MosDeviceChannel(String name, MosDevice device) {
		this.name = name;
		this.device = device;
	}

	@Override
	public int compareTo(MosDeviceChannel other) {
		if (name == other.name) return 0;
		if (name == null) return -1;
		if (other.name == null) return 1;

		return String.CASE_INSENSITIVE_ORDER.compare(name, other.name);
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}
		if (obj instanceof MosDeviceChannel) {
			MosDeviceChannel other = (MosDeviceChannel) obj;
			return Objects.equals(device, other.device)
					&& Objects.equals(name, other.name);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(device, name);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MosDevice getDevice() {
		return device;
	}

	public void setDevice(MosDevice device) {
		this.device = device;
	}

	private static final long serialVersionUID = -5909087908172845952L;
}
