package tv.snews.anews.domain;

/**
 * Classe utilizada para persistir as informações de conexão com outras praças.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class Company extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 109238703586339053L;
	private String name;
	private String host;
	private Integer port;
	private String email;
	private String password;

	//--------------------------------------------------------------------------
	//	Constructor
	//--------------------------------------------------------------------------

	public Company() {
		// Define os valores default
		this.port = 8080;
	}

	//--------------------------------------------------------------------------
	//	Getters & Setters
	//--------------------------------------------------------------------------

	/**
	 * @return o nome de identificação da praça
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name nome de identificação da praça
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return o endereço IP para conexão com da praça
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host endereço de host para conexão com da praça
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return Integer porta de conexão com da praça
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * @param port porta de conexão com da praça
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

    /**
     * @return e-mail da autenticação de conexão com da praça
     */
    public String getEmail() {
    	return email;
    }

	
    /**
     * @param email e-mail da autenticação de conexão com da praça
     */
    public void setEmail(String email) {
    	this.email = email;
    }

	/**
	 * @return senha da autenticação de conexão com da praça
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password senha da autenticação de conexão com da praça
	 */
	public void setPassword(String password) {
		this.password = password;
	}

}
