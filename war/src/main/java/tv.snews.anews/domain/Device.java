package tv.snews.anews.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class Device extends AbstractEntity<Integer> {

	protected String name;

	protected DeviceProtocol protocol;
	protected DeviceVendor vendor;

	protected Set<DeviceType> types = new HashSet<>();

	//------------------------------
	//	Operations
	//------------------------------

	public void addType(DeviceType... types) {
		for (DeviceType type : types) {
			this.types.add(type);
		}
	}

	public void removeType(DeviceType type) {
		types.remove(type);
	}

	public boolean is(DeviceProtocol protocol) {
		return this.protocol == protocol;
	}

	public boolean is(DeviceType type) {
		return types.contains(type);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DeviceProtocol getProtocol() {
		return protocol;
	}

	public void setProtocol(DeviceProtocol protocol) {
		this.protocol = protocol;
	}

	public DeviceVendor getVendor() {
		return vendor;
	}

	public void setVendor(DeviceVendor vendor) {
		this.vendor = vendor;
	}

	public Set<DeviceType> getTypes() {
		return types;
	}

	public void setTypes(Set<DeviceType> types) {
		this.types = types;
	}

	private static final long serialVersionUID = 9126746049947724245L;
}
