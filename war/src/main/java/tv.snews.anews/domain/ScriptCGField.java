package tv.snews.anews.domain;

import java.util.Objects;

public class ScriptCGField extends AbstractEntity<Long> implements Comparable<ScriptCGField>, Cloneable {

    private static final long serialVersionUID = -2024292462567682957L;

    private String name;
    private String value;
    private int number;
    private int size;

    private ScriptCG scriptCG;

    //------------------------------
    //	Constructors
    //------------------------------

    public ScriptCGField() {
        super();
    }

    public ScriptCGField(ReportageCGField reportageCGField) {
        this.name = reportageCGField.getName();
        this.value = reportageCGField.getValue();
        this.number = reportageCGField.getNumber();
        this.size = reportageCGField.getSize();
    }

    public ScriptCGField(ScriptCG scriptCG, IITemplateField field) {
        super();

        if (field != null) {
            this.scriptCG = scriptCG;
            this.name = field.getName();
            this.number = field.getNumber();
            this.size = field.getSize();
        }
    }

    public ScriptCGField(ScriptCGField original) {
        this.name = original.getName();
        this.value = original.getValue();
        this.number = original.getNumber();
        this.size = original.getSize();
    }

    @Override
    public ScriptCGField clone() {
        ScriptCGField clone = new ScriptCGField();

        clone.name = name;
        clone.value = value;
        clone.number = number;
        clone.size = size;

        return clone;
    }

    //------------------------------
    //	Operations
    //------------------------------

    @Override
    public int compareTo(ScriptCGField other) {
        return number > other.number ? 1 : number < other.number ? -1 : 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, number);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj instanceof ScriptCGField) {
            ScriptCGField other = (ScriptCGField) obj;
            return Objects.equals(id, other.id)
                    && Objects.equals(number, other.number);
        }
        return false;
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public ScriptCG getScriptCG() {
        return scriptCG;
    }

    public void setScriptCG(ScriptCG scriptCG) {
        this.scriptCG = scriptCG;
    }
}
