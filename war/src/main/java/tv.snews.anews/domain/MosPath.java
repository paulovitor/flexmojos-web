package tv.snews.anews.domain;

/**
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
public class MosPath extends MosAbstractPath {

	public MosPath() {
		super();
	}
	
	public MosPath(String techDescription, String url) {
		super(techDescription, url);
	}
	
    private static final long serialVersionUID = -2656193896499820136L;
}
