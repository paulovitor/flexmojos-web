package tv.snews.anews.domain;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public enum CommunicationVehicle {

    AGENCY, JOURNAL, RADIO, TV
}
