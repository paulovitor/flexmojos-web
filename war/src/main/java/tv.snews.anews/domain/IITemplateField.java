package tv.snews.anews.domain;

import java.util.Objects;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class IITemplateField extends AbstractEntity<Integer> implements Comparable<IITemplateField> {

	private String name;
	private int number;
	private int size;

	private IITemplate template;

	public IITemplateField() {}

	public IITemplateField(String name, int number, int size) {
		this.name = name;
		this.number = number;
		this.size = size;
	}

	@Override
	public int compareTo(IITemplateField other) {
		return Integer.compare(number, other.number);
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}
		if (obj instanceof IITemplateField) {
			IITemplateField other = (IITemplateField) obj;
			return Objects.equals(template, other.template)
					&& Objects.equals(number, other.number);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(template, number);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public IITemplate getTemplate() {
		return template;
	}

	public void setTemplate(IITemplate template) {
		this.template = template;
	}

	private static final long serialVersionUID = -8714402119736165752L;
}
