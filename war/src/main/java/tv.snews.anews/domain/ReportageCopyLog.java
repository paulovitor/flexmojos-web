package tv.snews.anews.domain;


/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 */
public class ReportageCopyLog extends GenericCopyLog implements Comparable<ReportageCopyLog> {

    private Reportage reportage;

    //----------------------------------
    //  Constructors
    //----------------------------------

    public ReportageCopyLog() {
    }

    public ReportageCopyLog(Reportage reportage, String nickname, String company) {
    	super(nickname, company);
        this.reportage = reportage;
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    @Override
    public int compareTo(ReportageCopyLog other) {
        return getDate().compareTo(other.getDate());
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public Reportage getReportage() {
        return reportage;
    }

    public void setReportage(Reportage reportage) {
        this.reportage = reportage;
    }

    private static final long serialVersionUID = -2518241456898995925L;
}
