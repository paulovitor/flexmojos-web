package tv.snews.anews.domain;

import javax.persistence.Entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe que representa a sub sessão da lauda VT.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
@Entity
//@Indexed(index = "indexes/storyvtsection")
public class StoryVTSection extends StorySection {

	private String cue;

	//------------------------------------
	//  Constructors
	//------------------------------------
	
	public StoryVTSection() {
		super();
	}
	
	public StoryVTSection(Date duration) {
		super(duration);
	}
	
	//------------------------------------
	//  Operations
	//------------------------------------

	/**
	 * Indica quais sub-sessões nunca serão armazenadas nessa sessão.
	 *
	 * Classes filhas devem sobreescrever este método caso for necessário alterar a regra.
	 */
	@Override
	protected Set<Class<?>> allowedSubSections() {
		return new HashSet<Class<?>>() {{
			add(StoryCG.class);
			add(StoryMosCredit.class);
			add(StoryMosVideo.class);
			add(StoryWSVideo.class);
		}};
	}

	@Override
	public Date calculateDuration() {
		if (countSubSectionsOfType(StoryMosVideo.class) > 0 || countSubSectionsOfType(StoryWSVideo.class) > 0) {
			return super.calculateDuration();
		}		
		return duration;
	}
	
	@Override
	protected StoryVTSection clone() throws CloneNotSupportedException {
		StoryVTSection clone = new StoryVTSection(duration);
		clone.setCue(this.cue);
		
		for (StorySubSection subSection : this.getSubSections()) {
			StorySubSection subSectionClone = subSection.clone();
			subSectionClone.setStorySection(clone);
			clone.addSubSection(subSectionClone);
        }
		return clone;
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
	public String getCue() {
		return cue;
	}

	public void setCue(String cue) {
		this.cue = cue;
	}
	
	private static final long serialVersionUID = 2288616481929772432L;
}
