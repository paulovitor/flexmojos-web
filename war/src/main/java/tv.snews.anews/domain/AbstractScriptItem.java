package tv.snews.anews.domain;

/**
 * @author Samuel Guedes de Melo
 */
public abstract class AbstractScriptItem extends AbstractEntity<Long> implements Comparable<AbstractScriptItem> {

	private static final long serialVersionUID = 4438794214731483743L;

	protected int order;
	protected boolean disabled = false;

	protected Script script;

	//------------------------------
	//	Constructors
	//------------------------------

	public AbstractScriptItem() {
	}

	//------------------------------
	//	Operations
	//------------------------------

	@Override
	public int compareTo(AbstractScriptItem other) {
		return order - other.order;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public Script getScript() {
		return script;
	}

	public void setScript(Script script) {
		this.script = script;
	}
}
