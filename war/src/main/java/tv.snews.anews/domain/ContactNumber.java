package tv.snews.anews.domain;

import java.util.Objects;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class ContactNumber extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 7842721575179156710L;

	private NumberType numberType;
	private String value;

    //----------------------------------
    //  Operations
    //----------------------------------

    public ContactNumber createCopy() {
        ContactNumber copy = new ContactNumber();
        copy.setNumberType(numberType);
        copy.setValue(value);

        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof ContactNumber) {
            ContactNumber other = (ContactNumber) obj;
            return Objects.equals(id, other.id) ||
                    (Objects.equals(value, other.value) && Objects.equals(numberType, other.numberType));
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, numberType);
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

	public NumberType getNumberType() {
		return numberType;
	}

	public void setNumberType(NumberType numberType) {
		this.numberType = numberType;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
