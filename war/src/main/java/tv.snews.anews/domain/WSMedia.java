package tv.snews.anews.domain;

import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Date;
import java.util.Objects;

@Entity
@Indexed(index = "indexes/wsmedia")
public class WSMedia extends Media {

    private int assetId;
    private String classification;
    private String city;
    private Date eventDate;
    private String observation;
    private String reporter;
    private String cameraman1;
    private String cameraman2;
    private String cameraman3;
    private String assistant;
    private String editor;
    private String videographer;
	private String streamingURL;
	private String fileName;
	private WSMediaStatus status;

    //------------------------------------
    //  Operations
    //----------------------------------

	public boolean isDifferent(WSMedia other) {
		return !(
				Objects.equals(assetId, other.assetId) &&
				Objects.equals(classification, other.classification) &&
				Objects.equals(city, other.city) &&
				Objects.equals(eventDate, other.eventDate) &&
				Objects.equals(observation, other.observation) &&
				Objects.equals(reporter, other.reporter) &&
				Objects.equals(cameraman1, other.cameraman1) &&
				Objects.equals(cameraman2, other.cameraman2) &&
				Objects.equals(cameraman3, other.cameraman3) &&
				Objects.equals(assistant, other.assistant) &&
				Objects.equals(editor, other.editor) &&
				Objects.equals(videographer, other.videographer) &&
				Objects.equals(streamingURL, other.streamingURL) &&
				Objects.equals(status, other.status) &&
				Objects.equals(slug, other.slug) &&
				Objects.equals(durationTime, other.durationTime) &&
				Objects.equals(description, other.description) &&
				Objects.equals(changed, other.changed)
		);
	}

    public void updateFields(WSMedia other){
        super.updateFields(other);
        this.assetId = other.assetId;
        this.classification = other.classification;
        this.city = other.city;
        this.eventDate = other.eventDate;
        this.observation = other.observation;
        this.reporter = other.reporter;
        this.cameraman1 = other.cameraman1;
        this.cameraman2 = other.cameraman2;
        this.cameraman3 = other.cameraman3;
        this.assistant = other.assistant;
        this.editor = other.editor;
        this.videographer = other.videographer;
        this.streamingURL = other.streamingURL;
        this.fileName = other.fileName;
        this.status = other.status;
    }

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof WSMedia) {
			WSMedia other = (WSMedia) obj;
			return Objects.equals(assetId, other.assetId);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(assetId);
	}

    //------------------------------------
    //  Getters & Setters
    //----------------------------------

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getReporter() {
        return reporter;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public String getCameraman1() {
        return cameraman1;
    }

    public void setCameraman1(String cameraman1) {
        this.cameraman1 = cameraman1;
    }

    public String getCameraman2() {
        return cameraman2;
    }

    public void setCameraman2(String cameraman2) {
        this.cameraman2 = cameraman2;
    }

    public String getCameraman3() {
        return cameraman3;
    }

    public void setCameraman3(String cameraman3) {
        this.cameraman3 = cameraman3;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getVideographer() {
        return videographer;
    }

    public void setVideographer(String videographer) {
        this.videographer = videographer;
    }

	public String getStreamingURL() {
		return streamingURL;
	}

	public void setStreamingURL(String streamingURL) {
		this.streamingURL = streamingURL;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public WSMediaStatus getStatus() {
		return status;
	}

	public void setStatus(WSMediaStatus status) {
		this.status = status;
	}

	private static final long serialVersionUID = 6521671870231403563L;
}
