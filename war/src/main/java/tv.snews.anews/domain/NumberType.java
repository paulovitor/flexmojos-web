
package tv.snews.anews.domain;

import java.util.Objects;

/**
 * TODO Você deve usar cache de segundo nível nessa classe.
 * Como essa classe só tem uma carga diretamente no banco, pode-se aplicar o
 * cache de segundo nível.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class NumberType extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 144562582210529438L;
	
	private String description;

	@Override
	public int hashCode() {
	    return Objects.hash(description);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof NumberType) {
			NumberType other = (NumberType) obj;
			return Objects.equals(description, other.description);
		}
	    return false;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
