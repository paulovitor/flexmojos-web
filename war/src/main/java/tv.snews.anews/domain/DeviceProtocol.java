package tv.snews.anews.domain;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public enum DeviceProtocol {

	/**
	 * Intelligent Interface Protocol.
	 */
	II,

	/**
	 * MOS Protocol.
	 */
	MOS,

	/**
	 * Web Services.
	 */
	WS,

	NONE;
}
