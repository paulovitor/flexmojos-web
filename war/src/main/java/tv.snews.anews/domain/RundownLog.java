package tv.snews.anews.domain;

/**
 * Classe para log das ações do Espelho
 *  
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class RundownLog extends AbstractLog {

	private RundownAction rundownAction;
    
	public RundownLog() {
	}

    public RundownLog(Rundown rundown, User author, RundownAction rundownAction) {
    	super(rundown, author);
    	this.rundownAction = rundownAction;
    }
	
	public RundownAction getRundownAction() {
	    return rundownAction;
    }

	public void setRundownAction(RundownAction rundownAction) {
	    this.rundownAction = rundownAction;
    }

	private static final long serialVersionUID = -6318675855857890298L;
}
