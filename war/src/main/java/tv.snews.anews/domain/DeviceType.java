package tv.snews.anews.domain;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public enum DeviceType {

	CG, MAM, PLAYOUT, TP
}
