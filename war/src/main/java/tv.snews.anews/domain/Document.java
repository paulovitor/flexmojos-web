package tv.snews.anews.domain;

import org.hibernate.search.annotations.Boost;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.FieldBridge;
import tv.snews.anews.infra.search.ProgramLuceneBridge;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static org.hibernate.search.annotations.Analyze.NO;
import static org.hibernate.search.annotations.Resolution.MILLISECOND;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public abstract class Document<R extends AbstractRevision<?>> extends AbstractVersionedEntity<Long, R> {

	@Field(analyze = NO)
	@DateBridge(resolution = MILLISECOND)
	protected Date date;

	protected Date changeDate = new Date();

	@Field
	@Boost(2.0f)
	protected String slug;

//	@Field(analyze = NO) || As classes filhas estão usando o DocumentIndexingInterceptor
	protected boolean excluded = false;

	@Field(analyze = NO)
	@FieldBridge(impl = ProgramLuceneBridge.class)
	protected Program program;

	protected Set<Program> programs = new HashSet<>();
	
	protected Set<Program> drawerPrograms = new HashSet<>();
	
	@Field(analyze = NO)
	protected DocumentState state;
	
	protected String statusReason;


	//------------------------------
	//	Operations
	//------------------------------

	protected void copyData(Document other) {
		this.date = other.date;
		this.changeDate = other.changeDate;
		this.slug = other.slug;
		this.excluded = other.excluded;
		this.program = other.program;
		this.state = other.state;
		this.statusReason =  other.statusReason;
		
		this.author = other.author;

		this.programs.clear();
		this.programs.addAll(other.programs);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public boolean isExcluded() {
		return excluded;
	}

	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public Set<Program> getPrograms() {
		return programs;
	}

	public void setPrograms(Set<Program> programs) {
		this.programs = programs;
	}
	
    public Set<Program> getDrawerPrograms() {
    	return drawerPrograms;
    }

    public void setDrawerPrograms(Set<Program> drawerPrograms) {
    	this.drawerPrograms = drawerPrograms;
    }
    
    public DocumentState getState() {
    	return state;
    }

    public void setState(DocumentState state) {
    	this.state = state;
    }
    
    public String getStatusReason() {
    	return statusReason;
    }

	public void setStatusReason(String statusReason) {
    	this.statusReason = statusReason;
    }
	
	private static final long serialVersionUID = 496676410880022836L;
}
