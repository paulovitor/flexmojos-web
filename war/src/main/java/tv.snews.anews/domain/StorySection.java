package tv.snews.anews.domain;

import org.joda.time.DateTime;

import java.util.*;

import static tv.snews.anews.util.DateTimeUtil.*;

/**
 * Classe que representa a sessão da lauda.
 * 
 * @author Paulo Felipe
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
//@Entity
//@Indexed(index = "indexes/storysection")
public class StorySection extends AbstractEntity<Long> implements Cloneable {

	private static final long serialVersionUID = 2288616481929772432L;

	protected Date duration;
	
//	@IndexedEmbedded
	private SortedSet<StorySubSection> subSections = new TreeSet<>();

	public StorySection() {
		this(DateTime.now().toDate()); // NOTE O tempo incial não deveria ser 00:00:00?
	}
	
	public StorySection(Date duration) {
		this.duration = duration;
	}
	
	/**
	 * Carrega os relacionamentos lazy deste objeto.
	 */
	public void loadLazies() {
		for (StorySubSection subSection : subSections) {
			subSection.loadLazies();
		}
	}

	/**
	 * Indica quais sub-seções nunca serão armazenadas nessa seção.
	 *
	 * Classes filhas devem sobreescrever este método caso for necessário alterar a regra.
	 */
	protected Set<Class<?>> allowedSubSections() {
		return new HashSet<Class<?>>() {{
			add(StoryCameraText.class);
			add(StoryCG.class);
			add(StoryMosCredit.class);
			add(StoryText.class);
		}};
	}

	public <T extends StorySubSection> int countSubSectionsOfType(Class<T> type) {
		int count = 0;

		// Caso essa seção não possa ter esse tipo, evita de iterar as coleções e carregar lazies desnecessáriamente
		if (allowedSubSections().contains(type)) {
			for (StorySubSection subSection : subSections) {
				if (type.isInstance(subSection)) {
					count++;
				}
				count += subSection.countSubSectionsOfType(type);
			}
		}

		return count;
	}

	/**
	 * Retorna todas as sub-seções do tipo informado.
	 *
	 * @param type tipo desejado
	 * @param <T>  tipo de {@link StorySubSection}
	 * @return as sub-seções encontradas
	 */
	public <T extends StorySubSection> List<T> subSectionsOfType(Class<T> type) {
		List<T> results = new ArrayList<>();

		// Caso essa seção não possa ter esse tipo, evita de iterar as coleções e carregar lazies desnecessáriamente
		if (allowedSubSections().contains(type)) {
			for (StorySubSection subSection : subSections) {
				if (type.isInstance(subSection)) {
					results.add(type.cast(subSection));
				}

				// Captura também das subSections da subSection
				results.addAll(subSection.subSectionsOfType(type));
			}
		}

		return results;
	}

	/**
	 * Remove todas as sub-seções do tipo informado.
	 *
	 * @param type tipo a ser removido
	 * @param <T>  tipo de {@link StorySubSection}
	 */
	public <T extends StorySubSection> void removeSubSectionsOfType(Class<T> type) {
		// Caso essa seção não possa ter esse tipo, evita de iterar as coleções e carregar lazies desnecessáriamente
		if (allowedSubSections().contains(type)) {
			for (Iterator<StorySubSection> it = subSections.iterator(); it.hasNext(); ) {
				StorySubSection subSection = it.next();
				if (type.isInstance(subSection)) {
					it.remove();
				}

				// Remove também das subSections da subSection
				subSection.removeSubSectionsOfType(type);
			}
		}
	}

	@Override
	protected StorySection clone() throws CloneNotSupportedException {
		StorySection clone = new StorySection();
		clone.setDuration(duration);
		for (StorySubSection subSection : subSections) {
			StorySubSection subSectionClone = subSection.clone();
			clone.addSubSection(subSectionClone);
		}
		return clone;
	}

	public Date calculateDuration() {
		duration = getMidnightTime();
		if (countSubSectionsOfType(StoryText.class) > 0 || countSubSectionsOfType(StoryCameraText.class) > 0 || countSubSectionsOfType(StoryMosVideo.class) > 0 || countSubSectionsOfType(StoryWSVideo.class) > 0) {
			for (StorySubSection subSection : subSections) {
				
				/*
				 * Por enquanto as subSections das subSections não precisam calcular 
				 * tempo, então elas não são tratadas nesse código.
				 */
				if (subSection instanceof StoryMosVideo) {
					StoryMosVideo storyMosVideo = (StoryMosVideo) subSection;
					duration = sumTimes(duration, addTimeToCurrentDate(storyMosVideo.getDurationTime()));
				}

				/*
				 * Por enquanto as subSections das subSections não precisam calcular
				 * tempo, então elas não são tratadas nesse código.
				 */
				if (subSection instanceof StoryWSVideo) {
					StoryWSVideo storyWSVideo = (StoryWSVideo) subSection;
					duration = sumTimes(duration, addTimeToCurrentDate(storyWSVideo.getDurationTime()));
				}

				if (subSection instanceof StoryText) {
					StoryText storyText = (StoryText) subSection;
					duration = sumTimes(duration, storyText.calculateReadTime());
				}
				if (subSection instanceof StoryCameraText) {
					StoryCameraText storyCameraText = (StoryCameraText) subSection;
					duration = sumTimes(duration, storyCameraText.calculateReadTime());
				}
			}
		}
		
		return duration;
	}

    /**
     * Adiciona sub-seções a esta sessão.
     *
     * @param subSection sub-sessões
     */
    public void addSubSection(StorySubSection subSection) {
        subSection.setStorySection(this);
        subSection.setOrder(this.getSubSections().size());
        subSections.add(subSection);
    }

    public boolean hasVideos() {
			return countSubSectionsOfType(StoryMosVideo.class) > 0 || countSubSectionsOfType(StoryWSVideo.class) > 0;
    }

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

    public Date getDuration() {
	    return duration;
    }

    public void setDuration(Date duration) {
		this.duration = duration == null ? getMidnightTime() : addTimeToCurrentDate(duration);
    }

	public SortedSet<StorySubSection> getSubSections() {
		return subSections;
	}

	public void setSubSections(SortedSet<StorySubSection> subSections) {
		this.subSections = subSections;
	}
}
