package tv.snews.anews.domain;

import java.util.Date;

import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * Classe que representa a sessão nc da lauda.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.2.6
 */
//@Entity
//@Indexed(index = "indexes/storytext")
public class StoryText extends StorySubSection {

	protected User presenter;
	protected Date readTime;
	
//	@Field
	protected String text;

	@Override
	protected StoryText clone() throws CloneNotSupportedException {
		StoryText clone = new StoryText();
		clone.setPresenter(this.presenter);
		clone.setReadTime(this.readTime);
		clone.setText(this.text);
		clone.setOrder(this.getOrder());

        for (StorySubSection subSection : this.getSubSections()) {
            StorySubSection subClone = subSection.clone();
            clone.addSubSection(subClone );
        }

		return clone;
	}

    /**
     * Adiciona sub-seções a esta sub-sessão.
     *
     * @param subSection sub-sessões
     */
    public void addSubSection(StorySubSection subSection) {
        subSection.setParent(this);
        subSection.setOrder(this.getSubSections().size());
        this.getSubSections().add(subSection);
    }

	public Date calculateReadTime() {
		//regex para limar tudo entre colchete ex: [off 1]
		String textToCalculate = text.replaceAll("\\[[^\\]]*\\]\\n?", "");
		readTime = presenter == null ? getMidnightTime() : presenter.calculateReadTime(textToCalculate);
		return readTime;
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
	public User getPresenter() {
		return presenter;
	}

	public void setPresenter(User presenter) {
		this.presenter = presenter;
	}

	public Date getReadTime() {
		return readTime;
	}

	public void setReadTime(Date readTime) {
		this.readTime = readTime == null ? getMidnightTime() : addTimeToCurrentDate(readTime);
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	private static final long serialVersionUID = -2323857212096957753L;
}
