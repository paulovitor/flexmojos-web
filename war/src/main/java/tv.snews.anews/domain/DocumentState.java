package tv.snews.anews.domain;

/**
 * Classe <code>enum</code> que define os possíveis estados que uma pauta pode
 * passar durante seu ciclo de produção.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public enum DocumentState {

	/**
	 * Estado inicial da pauta, onde o produtor ainda está elaborando e 
	 * coletando o mínimo de informações necessárias para que o repórter possa 
	 * cobrir a matéria. 
	 */
	PRODUCING,
	
	/**
	 * Segundo estado da pauta, que é definido a partir do momento em que o 
	 * repórter inicia a cobertura da matéria, mesmo que o produtor ainda não 
	 * tenha encerrado seu trabalho.
	 */
	COVERING, 
	
	/**
	 * Um dos estados finais da pauta que indica que o repórter encerrou a 
	 * cobertura da pauta com sucesso.
	 */
	COMPLETED, 
	
    /**
     * Um dos estados finais da pauta, que indica que a mesma foi cancelada por 
     * um motívo qualquer. A pauta pode voltar a um estado ativo posteriormente.
     */
	INTERRUPTED,
    
	/**
     * Estado que representa que uma reportagem está sob edição
     */
	EDITING;
}
