package tv.snews.anews.domain;

import java.util.*;

/**
 * Classe que representa a sub sessão da lauda.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
//@Entity
//@Indexed(index = "indexes/storysubsection")
public abstract class StorySubSection extends AbstractEntity<Long> implements Comparable<StorySubSection>, Cloneable {

	protected int order;

//	@ContainedIn
	private StorySection storySection;
	
//	@ContainedIn
	private StorySubSection parent;

//	@IndexedEmbedded(depth = 1)
	private SortedSet<StorySubSection> subSections = new TreeSet<>();

	public StorySubSection() {
		super();
	}

	StorySubSection(int order) {
		super();
		this.order = order;
	}

	/**
	 * Carrega os relacionamentos lazy deste objeto.
	 */
	public void loadLazies() {
		for (StorySubSection subSection : subSections) {
			subSection.loadLazies();
		}
	}
	
	@Override
	protected abstract StorySubSection clone() throws CloneNotSupportedException;

	@Override
	public int compareTo(StorySubSection other) {
		return order - other.order;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, order);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof StorySubSection) {
			StorySubSection other = (StorySubSection) obj;
			return Objects.equals(id, other.id)
			    && Objects.equals(order, other.order);
		}
		return false;
	}

	public <T extends StorySubSection> int countSubSectionsOfType(Class<T> type) {
		int count = 0;
		for (StorySubSection subSection : subSections) {
			if (type.isInstance(subSection)) {
				count++;
			}
		}
		return count;
	}

	/**
	 * Retorna todas as sub-seções do tipo informado.
	 * 
	 * @param type tipo desejado
	 * @return as sub-seções encontradas
	 */
	public <T extends StorySubSection> List<T> subSectionsOfType(Class<T> type) {
		List<T> results = new ArrayList<>();
		
		for (StorySubSection subSection : subSections) {
			if (type.isInstance(subSection)) {
				results.add(type.cast(subSection));
			}
		}
		
		return results;
	}

    /**
     * Remove todas as sub-seções do tipo informado.
     *
     * @param type tipo a ser removido
     * @param <T> tipo de {@link StorySubSection}
     */
    public <T extends StorySubSection> void removeSubSectionsOfType(Class<T> type) {
        for (Iterator<StorySubSection> it = subSections.iterator(); it.hasNext();) {
            StorySubSection subSection = it.next();
            if (type.isInstance(subSection)) {
                it.remove();
            }
        }
    }

    public String formatField(String field, int size){
        if(field.length() > size){
            String formattedField = field.substring(0, size-3);
            formattedField += "...";
            return formattedField;
        } else
            return field;
    }
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public StorySection getStorySection() { 
		return storySection;
	}

	public void setStorySection(StorySection storySection) {
		this.storySection = storySection;
	}

	public SortedSet<StorySubSection> getSubSections() {
		return subSections;
	}

	public void setSubSections(SortedSet<StorySubSection> subSections) {
		this.subSections = subSections;
	}

	public StorySubSection getParent() {
	    return parent;
    }

	public void setParent(StorySubSection parent) {
	    this.parent = parent;
    }

	private static final long serialVersionUID = 2288616481929772432L;
}
