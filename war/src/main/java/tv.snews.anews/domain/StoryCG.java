package tv.snews.anews.domain;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Classe que representa a sessão gc da lauda.
 *
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
public class StoryCG extends StorySubSection {

	private static final long serialVersionUID = -2323857212096957753L;

	private int code;
	private int templateCode;
	private boolean comment = false;
	private boolean disabled = false;

	private SortedSet<StoryCGField> fields = new TreeSet<>();

	public StoryCG() {
		super();
	}

	public StoryCG(ReportageCG reportageCG) {
		super();

		this.code = reportageCG.getCode();
		this.templateCode = reportageCG.getTemplateCode();
		this.comment = reportageCG.isComment();

		for (ReportageCGField reportageField : reportageCG.getFields()) {
			addField(new StoryCGField(reportageField));
		}
	}

	public void addField(StoryCGField field) {
		field.setStoryCG(this);
		fields.add(field);
	}

	@Override
	protected StoryCG clone() throws CloneNotSupportedException {
		StoryCG clone = new StoryCG();

		clone.code = code;
		clone.templateCode = templateCode;
		clone.comment = comment;
		clone.disabled = disabled;

		for (StoryCGField field : this.fields) {
			StoryCGField fieldClone = field.clone();
			fieldClone.setStoryCG(clone);
			clone.fields.add(fieldClone);
		}

		return clone;
	}

	public boolean isFilled() {
		for (StoryCGField field : fields) {
			if (isNotBlank(field.getValue())) {
				return true;
			}
		}
		return false;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(int templateCode) {
		this.templateCode = templateCode;
	}

	public boolean isComment() {
		return comment;
	}

	public void setComment(boolean comment) {
		this.comment = comment;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public SortedSet<StoryCGField> getFields() {
		return fields;
	}

	public void setFields(SortedSet<StoryCGField> fields) {
		this.fields = fields;
	}
}
