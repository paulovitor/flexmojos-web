package tv.snews.anews.domain;

import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.hibernate.search.annotations.Analyze.NO;
import static org.hibernate.search.annotations.Resolution.MILLISECOND;

/**
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
@Entity
@Indexed(index = "indexes/mosmedia")
public class MosMedia extends Media {

    private String objectId;

    @Field
    private String abstractText;

    @Field
    private String group;

    @Field
    private String type;

    private int revision;
    private boolean ready;
    private String createdBy;

    private Long duration;
    private BigDecimal timeBase;

    @Field(analyze = NO)
    @DateBridge(resolution = MILLISECOND)
    private Date created;

    private String changedBy;

    private String mosExternalMetadata;

    private MosMetadataPath metadataPath;

    private Set<MosPath> paths = new HashSet<>();
    private Set<MosProxyPath> proxyPaths = new HashSet<>();

    //------------------------------------
    //  Operations
    //----------------------------------

    public void addPath(MosPath path) {
        paths.add(path);
    }

    public void addProxyPath(MosProxyPath proxyPath) {
        proxyPaths.add(proxyPath);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof MosMedia) {
            MosMedia other = (MosMedia) obj;
            return Objects.equals(getDevice(), other.getDevice())
                    && Objects.equals(objectId, other.objectId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDevice(), objectId);
    }

    //------------------------------------
    //  Getters & Setters
    //----------------------------------

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objId) {
        this.objectId = objId;
    }

    public String getAbstractText() {
        return abstractText;
    }

    public void setAbstractText(String mosAbstract) {
        this.abstractText = mosAbstract;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String objGroup) {
        this.group = objGroup;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRevision() {
        return revision;
    }

    public void setRevision(Integer objRev) {
        this.revision = objRev == null ? -1 : objRev;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public BigDecimal getTimeBase() {
        return timeBase;
    }

    public void setTimeBase(BigDecimal timeBase) {
        this.timeBase = timeBase;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(Boolean ready) {
        this.ready = ready == null ? false : ready;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public String getMosExternalMetadata() {
        return mosExternalMetadata;
    }

    public void setMosExternalMetadata(String mosExternalMetadata) {
        this.mosExternalMetadata = mosExternalMetadata;
    }

    public Set<MosPath> getPaths() {
        return paths;
    }

    public void setPaths(Set<MosPath> mosObjPaths) {
        this.paths = mosObjPaths;
    }

    public MosMetadataPath getMetadataPath() {
        return metadataPath;
    }

    public void setMetadataPath(MosMetadataPath metadataPath) {
        this.metadataPath = metadataPath;
    }

    public Set<MosProxyPath> getProxyPaths() {
        return proxyPaths;
    }

    public void setProxyPaths(Set<MosProxyPath> mosObjProxyPaths) {
        this.proxyPaths = mosObjProxyPaths;
    }

    private static final long serialVersionUID = -2656193896499820136L;
}
