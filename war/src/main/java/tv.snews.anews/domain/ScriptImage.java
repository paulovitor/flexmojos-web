package tv.snews.anews.domain;

import tv.snews.anews.util.ImageUtil;

import java.io.IOException;
import java.util.Objects;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptImage extends AbstractEntity<Long> implements Comparable<ScriptImage> {

    // Baseado em UserFile.java

    private int order;

    private String name;
    private String extension;
    private String path;
    private long size; // bytes
    private String thumbnailPath;
    private Script script;

    //Transiente
    private byte[] thumbnailBytes;

    //------------------------------
    //	Constructors
    //------------------------------

    public ScriptImage() {

    }

    public ScriptImage(ScriptImage scriptImage) throws IOException {
        this.name = scriptImage.getName();
        this.extension = scriptImage.getExtension();
        this.path = ImageUtil.copyImage(scriptImage.getPath());
        this.size = scriptImage.getSize();
        this.thumbnailPath = ImageUtil.copyImage(scriptImage.getThumbnailPath());
    }

    @Override
    public int compareTo(ScriptImage other) {
        return order - other.order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScriptImage)) return false;
        if (!super.equals(o)) return false;

        ScriptImage that = (ScriptImage) o;

        if (size != that.size) return false;
        if (!extension.equals(that.extension)) return false;
        if (!name.equals(that.name)) return false;
        if (!path.equals(that.path)) return false;
        if (!thumbnailPath.equals(that.thumbnailPath)) return false;

        return true;
    }

    @Override
    public int hashCode() {
			return Objects.hash(name, extension, path, size, thumbnailPath);
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    public byte[] getThumbnailBytes() {
        return thumbnailBytes;
    }

    public void setThumbnailBytes(byte[] thumbnailBytes) {
        this.thumbnailBytes = thumbnailBytes;
    }

    private static final long serialVersionUID = 623656865521995526L;
}
