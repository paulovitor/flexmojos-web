package tv.snews.anews.domain;


/**
 * Enum para ações do bloco de um espelho.
 * 
 * @author Samuel Guedes de Melo
 */
public enum ScriptBlockAction {
	INSERTED, UPDATED, EXCLUDED, ORDER_CHANGED
}
