package tv.snews.anews.domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.apache.commons.lang.StringUtils.stripToEmpty;
import static tv.snews.anews.util.DateTimeUtil.dateToDateFormat;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class GuidelineRevision extends AbstractRevision<Guideline> {

	private String date;
	private String slug;
	private String proposal;
	private String referral;
	private String information;
	private String program;
	private String team;
	private String classification;
	private String producer;
	private String reporter;
	private String editor;
	private String guides;

	//----------------------------------
	//	Constructors
	//----------------------------------
	private String vehicles;

	// Hibernate & AMF
	public GuidelineRevision() {
	}

	//----------------------------------
	//	Operations
	//----------------------------------

	public GuidelineRevision(Guideline guideline) {
		super(guideline);
	}

	@Override
	protected void fillRevision(Guideline guideline) {
		this.date = dateToDateFormat(guideline.getDate());
		this.slug = stripToEmpty(guideline.getSlug());
		this.proposal = stripToEmpty(guideline.getProposal());
		this.referral = stripToEmpty(guideline.getReferral());
		this.information = stripToEmpty(guideline.getInformations());
		this.program = guideline.getProgram() == null ? "" : guideline.getProgram().getName();
		this.team = guideline.getTeam() == null ? "" : guideline.getTeam().getName();
		this.classification = guideline.getClassification() == null ? "" : guideline.getClassification().getName();
		this.producer = guideline.producersToString();
		this.reporter = guideline.reportersToString();
		this.editor = guideline.editorsToString();
		this.guides = joinGuides(guideline.getGuides());
		this.vehicles = guideline.vehiclesToString();
	}

	//----------------------------------
	//	Helpers
	//----------------------------------

	public GuidelineRevision createCopy(Guideline guidelineCopy) {
		GuidelineRevision copy = new GuidelineRevision();

		// Propriedades da classe pai
		copy.setRevisionNumber(revisionNumber);
		copy.setRevisionDate(revisionDate);
		copy.setRevisor(revisor);
		copy.setEntity(guidelineCopy);

		// Propriedades locais
		copy.setDate(date);
		copy.setSlug(slug);
		copy.setProposal(proposal);
		copy.setReferral(referral);
		copy.setInformation(information);
		copy.setProgram(program);
		copy.setClassification(classification);
		copy.setProducer(producer);
		copy.setReporter(reporter);
		copy.setEditor(editor);
		copy.setGuides(guides);
		copy.setVehicles(vehicles);
		copy.setTeam(team);

		return copy;
	}

	private String joinGuides(Set<Guide> guides) {
		StringBuilder builder = new StringBuilder();

		for (Guide guide : guides) {
			builder.append(formatTime(guide.getSchedule())).append("\n");

			for (Contact contact : guide.getContacts()) {
				builder.append(contact.getName());

				ContactNumber number = contact.getFirstNumber();
				if (number != null) {
					builder.append(" - ");
					builder.append(contact.getFirstNumber().getValue());
				}
				builder.append("\n");
			}

			if (!isBlank(guide.getAddress())) {
				builder.append(guide.getAddress());

				if (!isBlank(guide.getReference())) {
					builder.append(" (").append(guide.getReference()).append(")");
				}

				City city = guide.getCity();
				if (city != null) {
					builder.append(", ").append(city.getName()).append(" - ").append(city.getState().getName());
				}
				builder.append("\n");
			}

			builder.append(stripToEmpty(guide.getObservation())).append("\n\n");
		}

		return builder.toString().trim();
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	private String formatTime(Date value) {
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
		return value != null ? formatter.format(value) : "";
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getProposal() {
		return proposal;
	}

	public void setProposal(String proposal) {
		this.proposal = proposal;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public String getGuides() {
		return guides;
	}

	public void setGuides(String guides) {
		this.guides = guides;
	}

	public String getVehicles() {
		return vehicles;
	}

	public void setVehicles(String vehicles) {
		this.vehicles = vehicles;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	private static final long serialVersionUID = -8081635348758854012L;
}
