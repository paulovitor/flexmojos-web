package tv.snews.anews.domain;


/**
 * @author Samuel Guedes de Melo
 */
public class GuidelineCopyLog extends GenericCopyLog implements Comparable<GuidelineCopyLog> {

    private Guideline guideline;

    //----------------------------------
    //  Constructors
    //----------------------------------

    public GuidelineCopyLog() {
    }

    public GuidelineCopyLog(Guideline guideline, String nickname, String company) {
    	super(nickname, company);
        this.guideline = guideline;
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    @Override
    public int compareTo(GuidelineCopyLog other) {
        return getDate().compareTo(other.getDate());
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------
  
    public Guideline getGuideline() {
    	return guideline;
    }

    public void setGuideline(Guideline guideline) {
    	this.guideline = guideline;
    }

    private static final long serialVersionUID = -2518241456898995925L;
}
