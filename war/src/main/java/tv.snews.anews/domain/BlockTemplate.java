package tv.snews.anews.domain;

import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class BlockTemplate extends AbstractEntity<Integer> implements Comparable<BlockTemplate>, Cloneable {

    private int order;
    private boolean standBy;
    private Date commercial;

    private RundownTemplate rundown;
    private SortedSet<StoryTemplate> stories = new TreeSet<>();

    public BlockTemplate() {}

    public BlockTemplate(RundownTemplate rundown) {
        this.rundown = rundown;
        this.commercial = getMidnightTime();
        this.standBy = rundown.getBlocks().isEmpty();

        if (standBy) {
            this.order = rundown.getBlocks().size();
        } else {
            BlockTemplate standBy = rundown.getBlocks().last();
            this.order = standBy.getOrder();
            standBy.order++;
        }
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    public StoryTemplate createStory() {
        StoryTemplate story = new StoryTemplate(this);
        stories.add(story);

        return story;
    }

    public void removeStory(StoryTemplate story) {
        stories.remove(story);

        // Corrige as ordens após a remoção
        int orderCount = 0;
        for (StoryTemplate aux : stories) {
            aux.setOrder(orderCount++);
        }
    }

    @Override
    public int compareTo(BlockTemplate other) {
        // Stand By sempre vem por último

        if (standBy && other.standBy) {
            return 0;
        }
        if (standBy) {
            return 1;
        }
        if (other.standBy) {
            return -1;
        }
        return order - other.order;
    }

    @Override
    public BlockTemplate clone() throws CloneNotSupportedException {
        BlockTemplate clone = new BlockTemplate();
        clone.order = order;
        clone.standBy = standBy;
        clone.commercial = commercial;

        for (StoryTemplate story : stories) {
            StoryTemplate storyClone = story.clone();
            storyClone.setBlock(clone);
            clone.stories.add(storyClone);
        }

        return clone;
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isStandBy() {
        return standBy;
    }

    public void setStandBy(boolean standBy) {
        this.standBy = standBy;
    }

    public Date getCommercial() {
        return commercial;
    }

    public void setCommercial(Date commercial) {
        this.commercial = commercial == null ? getMidnightTime() : addTimeToCurrentDate(commercial);
    }

    public RundownTemplate getRundown() {
        return rundown;
    }

    public void setRundown(RundownTemplate rundown) {
        this.rundown = rundown;
    }

    public SortedSet<StoryTemplate> getStories() {
        return stories;
    }

    public void setStories(SortedSet<StoryTemplate> stories) {
        this.stories = stories;
    }

    private static final long serialVersionUID = -6081474996002809779L;
}
