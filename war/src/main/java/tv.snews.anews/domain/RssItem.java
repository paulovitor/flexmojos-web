package tv.snews.anews.domain;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Date;
import java.util.Objects;

/**
 * Contém as informações de uma notícia obtida a partir de um feed RSS. É
 * importante lembrar que teoricamente nenhum dos campos é obrigatório.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssItem extends AbstractEntity<Long> implements Comparable<RssItem> {

	private static final long serialVersionUID = -7377997687836271148L;

	private String title;
	private String description;
	private String content;
	private String link;
	private Date published;
	private String authors;
	private String categories;

	private RssFeed rssFeed;

	public RssItem() {
		this.title = "";
		this.published = new Date();
	}

	/**
	 * Faz a verificação baseada na classe do objeto - deve ser um
	 * {@link RssItem} - e nas propriedades {@code rssFeed} e {@code link}.
	 * 
	 * @return Boolean indicando o resultado da verificação.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RssItem) {
			RssItem other = (RssItem) obj;
			return Objects.equals(link, other.link)
				&& Objects.equals(rssFeed, other.rssFeed);
		}
		return false;
	}

	/**
	 * Faz a geração do {@code hashCode} baseada nas propriedades
	 * {@code rssFeed} (feed de origem) e {@code link}.
	 */
	@Override
	public int hashCode() {
		return Objects.hash(link, rssFeed);
	}

	/**
	 * Faz a comparação baseada na propriedade {@code published}, ou seja, a
	 * data de publicação do item. Caso possuam o mesmo valor, o critério de
	 * desempate será o valor da propriedade {@code title}.
	 * <p>
	 * A comparação pela propriedade {@code published} retorna um resultado
	 * invertido para que a ordenação seja decrescente.
	 * </p>
	 */
	@Override
	public int compareTo(RssItem other) {
		int comparison = published.compareTo(other.published);
		return comparison == 0 ? title.compareTo(other.title) : comparison * -1;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this)
		                .append("title", title)
		                .append("published", published)
		                .toString();
	}

	//----------------------------------------
	//	Getters & Setters
	//----------------------------------------

	/**
	 * @return O título da notícia.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title O título da notícia.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return A descrição da notícia.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description A descrição da notícia.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return O conteúdo da notícia.
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content O conteúdo da notícia.
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return O link da notícia.
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link O link da notícia.
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return A data de publicação da notícia.
	 */
	public Date getPublished() {
		return published;
	}

	/**
	 * Se a data de publicação for nula, o que raramente deve ocorrer, será
	 * colocada a data e hora atual.
	 * 
	 * @param published Data de publicação da notícia.
	 */
	public void setPublished(Date published) {
		this.published = published == null ? new Date() : published;
	}

	/**
	 * @return String conténdo os nomes dos autores da notícia.
	 */
	public String getAuthors() {
		return authors;
	}

	/**
	 * @param authors Os autores da notícia.
	 */
	public void setAuthors(String authors) {
		this.authors = authors;
	}

	/**
	 * @return As categorias da notícia.
	 */
	public String getCategories() {
		return categories;
	}

	/**
	 * @param categories As categorias da notícia.
	 */
	public void setCategories(String categories) {
		this.categories = categories;
	}

	/**
	 * @return O feed RSS a qual esse item pertence.
	 */
	public RssFeed getRssFeed() {
		return rssFeed;
	}

	/**
	 * @param rssFeed O feed RSS a qual esse item pertence.
	 */
	public void setRssFeed(RssFeed rssFeed) {
		this.rssFeed = rssFeed;
	}
}
