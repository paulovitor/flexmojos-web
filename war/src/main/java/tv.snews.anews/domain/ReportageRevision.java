package tv.snews.anews.domain;

import tv.snews.anews.util.ResourceBundle;

import java.util.SortedSet;

import static org.apache.commons.lang.StringUtils.isNotBlank;
import static org.apache.commons.lang.StringUtils.stripToEmpty;
import static tv.snews.anews.util.DateTimeUtil.dateToDateFormat;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class ReportageRevision extends AbstractRevision<Reportage> {

	private static final ResourceBundle bundle = ResourceBundle.INSTANCE;

	private String slug;
	private String date;
	private String program;
	private String reporter;
	private String cameraMan;
	private String sections;
	private String vehicles;
	private String tipOfHead;

	//----------------------------------
	//	Constructors
	//----------------------------------
	private String information;

	// Hibernate & AMF
	public ReportageRevision() {
	}

	//----------------------------------
	//	Operations
	//----------------------------------

	public ReportageRevision(Reportage reportage) {
		super(reportage);
	}

	public ReportageRevision createCopy(Reportage reportageCopy) {
		ReportageRevision copy = new ReportageRevision();

		// Propriedades da classe pai
		copy.setRevisionNumber(revisionNumber);
		copy.setRevisionDate(revisionDate);
		copy.setRevisor(revisor);
		copy.setEntity(reportageCopy);

		// Propriedades locais
		copy.setSlug(slug);
		copy.setDate(date);
		copy.setProgram(program);
		copy.setReporter(reporter);
		copy.setCameraMan(cameraMan);
		copy.setSections(sections);
		copy.setVehicles(vehicles);
		copy.setInformation(information);
		copy.setTipOfHead(tipOfHead);

		return copy;
	}

	@Override
	protected void fillRevision(Reportage reportage) {
		this.date = dateToDateFormat(reportage.getDate());

		this.slug = stripToEmpty(reportage.getSlug());
		this.cameraMan = stripToEmpty(reportage.getCameraman());
		this.information = stripToEmpty(reportage.getInformation());
		this.tipOfHead = stripToEmpty(reportage.getTipOfHead());

		this.vehicles = reportage.vehiclesToString();

		this.program = reportage.getProgram() == null ? bundle.getMessage("defaults.globalDrawer") : reportage.getProgram().getName();
		this.reporter = reportage.getReporter() == null ? "" : reportage.getReporter().getNickname();

		this.sections = joinSections(reportage.getSections());
	}

	//----------------------------------
	//	Helpers
	//----------------------------------

	@Override
	public String toString() {
		return
				"ReportageRevision {\n" +
						"    id: " + id + ",\n" +
						"    revisionNumber: " + revisionNumber + ",\n" +
						"    revisionDate: " + revisionDate + ",\n" +
						"    revisor: {\n" +
						"        id: " + (revisor == null ? "null" : revisor.getId()) + ",\n" +
						"        nickname: " + (revisor == null ? "null" : revisor.getNickname()) + "\n" +
						"    },\n" +
						"    entity: " + entity + ",\n" +
						"    program: " + program + ",\n" +
						"    reporter: " + reporter + ",\n" +
						"    cameraMan: " + cameraMan + ",\n" +
						"    sections: " + sections + "\n" +
						"    vehicles: " + vehicles + "\n" +
						"    information: " + information + "\n" +
						"    tipOfHead: " + tipOfHead + "\n" +
						"}";
	}

	private String joinSections(SortedSet<ReportageSection> sections) {
		int appearanceCount = 0,
				artCount = 0,
				interviewCount = 0,
				offCount = 0,
				soundUpCount = 0;

		StringBuilder builder = new StringBuilder();

		for (ReportageSection section : sections) {
			builder.append(bundleName(section.getType()));

			switch (section.getType()) {
				case APPEARANCE:
					builder.append(" ").append(++appearanceCount);
					break;
				case ART:
					builder.append(" ").append(++artCount);
					break;
				case INTERVIEW:
					builder.append(" ").append(++interviewCount);
					break;
				case OFF:
					builder.append(" ").append(++offCount);
					break;
				case SOUNDUP:
					builder.append(" ").append(++soundUpCount);
					break;
				default:
					// outros tipos não precisam de numeração
			}

			builder.append("\n");
			builder.append(stripToEmpty(section.getContent()));

			if (isNotBlank(section.getObservation())) {
				builder.append("\n");
				String obs = stripToEmpty(section.getObservation());

				switch (section.getType()) {
					case ART:
						builder.append(bundle.getMessage("reportage.sectionType.artData")).append("\n").append(obs);
						break;
					case INTERVIEW:
						builder.append("\"").append(obs).append("\"");
						break;
					default:
						builder.append(bundle.getMessage("reportage.sectionType.appearanceLocal")).append(" ").append(obs);
				}
			}

			builder.append("\n\n");
		}
		return builder.toString().trim();
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	private String bundleName(ReportageSectionType type) {
		switch (type) {
			case APPEARANCE:
				return bundle.getMessage("reportage.sectionType.appearance");
			case ART:
				return bundle.getMessage("reportage.sectionType.art");
			case CLOSURE:
				return bundle.getMessage("reportage.sectionType.closure");
			case INTERVIEW:
				return bundle.getMessage("reportage.sectionType.interview");
			case OFF:
				return bundle.getMessage("reportage.sectionType.off");
			case OPENING:
				return bundle.getMessage("reportage.sectionType.opening");
			case SOUNDUP:
				return bundle.getMessage("reportage.sectionType.soundup");
			default:
				throw new IllegalArgumentException("Unknown reportage section type: " + type);
		}
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public String getCameraMan() {
		return cameraMan;
	}

	public void setCameraMan(String cameraMan) {
		this.cameraMan = cameraMan;
	}

	public String getSections() {
		return sections;
	}

	public void setSections(String sections) {
		this.sections = sections;
	}

	public String getVehicles() {
		return vehicles;
	}

	public void setVehicles(String vehicles) {
		this.vehicles = vehicles;
	}

	public String getTipOfHead() {
		return tipOfHead;
	}

	public void setTipOfHead(String tipOfHead) {
		this.tipOfHead = tipOfHead;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	private static final long serialVersionUID = 8133720881477784371L;
}
