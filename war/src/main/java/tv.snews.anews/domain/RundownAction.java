package tv.snews.anews.domain;

/**
 * Ações exclusivas do espelho.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.0
 */
public enum RundownAction {
	SORTED, DRAWER, DRAG_LOCKED, DRAG_UNLOCKED, CREATED,
	START_CHANGED, END_CHANGED, PRODUCTION_CHANGED, DATE_CHANGED,
	DISPLAY_RESETS, COMPLETE_DISPLAY
}
