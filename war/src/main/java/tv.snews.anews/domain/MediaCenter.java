/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.domain;

/**
 * Representa as ações possíveis de acontecer sobre as entidades.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public enum MediaCenter {
	
	/**
	 * Possiveis ações que irão ocorrer em um Media Center.
	 */
	SEARCH_SCHEMA;
}
