package tv.snews.anews.domain;

/**
 * Ações exclusivas do roteiro.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public enum ScriptPlanAction {
	SORTED, DRAWER, DRAG_LOCKED, DRAG_UNLOCKED,
	CREATED, START_CHANGED, END_CHANGED, PRODUCTION_CHANGED, DATE_CHANGED, SLUG_CHANGED,
    DISPLAY_RESETS, COMPLETE_DISPLAY
}
