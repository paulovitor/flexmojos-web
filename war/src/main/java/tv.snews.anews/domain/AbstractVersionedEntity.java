package tv.snews.anews.domain;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Classe abstrata que define a base para as entidades que possuem controle de
 * revisão.
 *
 * @param <T> Tipo do ID da entitade.
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public abstract class AbstractVersionedEntity<T, R extends AbstractRevision<?>> extends AbstractEntity<T> {

	private static final long serialVersionUID = -1836267760457921007L;
	protected Integer version;
	protected User author;

	//----------------------------------
	//	Operations
	//----------------------------------
	protected SortedSet<R> revisions = new TreeSet<>();

	/**
	 * Cria uma nova revisão baseada nos dados atuais deste objeto e retorna o
	 * objeto da revisão criada. Caso nenhuma mudança tenha sido feita retorna
	 * {@code null}.
	 *
	 * @return o objeto da nova revisão ou {@code null} caso não tenha sido
	 * feita nenhuma mudança significativa
	 */
	public R createRevision() {
		try {
			@SuppressWarnings("unchecked")
			R newRevision = (R) constructRevision();
			R lastRevision = currentRevision();

			/*
			 * A regra é: se já tem alguma revisão, compara a nova revisão com a
			 * revisão anterior pra ver se mudou algo. Se teve alguma mudança ou
			 * se é a primeira revisão que está sendo criada, então a nova
			 * revisão é válida.
			 */
			if (lastRevision == null || !lastRevision.contentEquals(newRevision)) {
				revisions.add(newRevision);
				return newRevision;
			} else {
				return null;
			}
		} catch (InstantiationException | InvocationTargetException e) {
			throw new RuntimeException("Failed to construct the new revision.", e);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException("Failed to compare the new and the last revisions.", e);
		}
	}

	/**
	 * Retorna o número da revisão atual ou zero caso não tenha nenhuma revisão
	 * registrada.
	 *
	 * @return o número da versão atual ou {@code 0}
	 */
	public int currentRevisionNumber() {
		R currentRevision = currentRevision();
		return currentRevision == null ? 0 : currentRevision.getRevisionNumber();
	}

	/**
	 * Retorna o objeto da revisão atual ou {@code null} caso não tenha nenhuma
	 * registrada.
	 *
	 * @return a revisão atual ou {@code null}
	 */
	public R currentRevision() {
		return revisions.isEmpty() ? null : revisions.last();
	}

	public R getRevisionByNumber(int revisionNumber) {
		for (R revision : revisions) {
			if (revision.getRevisionNumber() == revisionNumber) {
				return revision;
			}
		}
		throw new RuntimeException("Revision with number " + revisionNumber + " not found.");
	}

	/**
	 * Retorna uma coleção conténdo todas as revisões registradas para essa
	 * entidade, marcando em cada revisão quais foram as mudanças feitas em
	 * relação a revisão anterior a ela.
	 *
	 * @return as revisões com as marcações de diff, em ordem decrescente da
	 * criação de cada uma
	 */
	public Collection<R> changesHistory() {
		List<R> changes = new ArrayList<>();
		R previous = null, current;
		for (Iterator<R> it = revisions.iterator(); it.hasNext(); ) {
			current = it.next();
			if (previous == null) {
				changes.add(current);
			} else {
				R diff = current.computeDiff(previous);
				changes.add(diff);
			}
			previous = current;
		}

		Collections.reverse(changes);
		return changes;
	}

	//----------------------------------
	//	Helpers
	//----------------------------------

	@Override
	public String toString() {
		return this.getClass().toString() + "[id=" + id + ", version=" + version + "]";
	}

	/*
	 * Faz a criação de uma instância da revisão cujo tipo for passado pelo
	 * segundo argumento genérico desta classe quando ela é extendida.
	 *
	 * Por exemplo, a classe Story extende esta classe da seguinte forma:
	 *
	 * 	... extends AbstractVersionedEntity<Long, StoryRevision>
	 *
	 * Então, para o caso de Story, este método irá criar uma instância do
	 * segundo argumento genérico: StoryRevision.
	 */
	private Object constructRevision() throws InstantiationException, InvocationTargetException, IllegalAccessException, IllegalArgumentException {
		// "this.getClass()" retorna a subclasse, e não esta classe!
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();

		Type revisionType; // tipo da revisão (pego dos argumentos da declaração da classe)

		Type[] argTypes = type.getActualTypeArguments();
		if (argTypes.length == 1) {
			// Classe herda de Document: pega o primeiro argumento genérico
			revisionType = type.getActualTypeArguments()[0];
		} else {
			// Classe herda de AbstractVersionedEntity: pega o segundo argumento genérico
			revisionType = type.getActualTypeArguments()[1];
		}

		@SuppressWarnings("unchecked")
		Class<AbstractRevision<?>> revisionClass = (Class<AbstractRevision<?>>) revisionType; // Type -> Class

		/*
		 * Procura pelo construtor de um parâmetro. Não está verificando tipo do
		 * argumento, então cuidado se for adicionar um novo construtor!
		 */
		for (Constructor<?> constructor : revisionClass.getConstructors()) {
			Class<?>[] paramTypes = constructor.getParameterTypes();
			if (paramTypes.length == 1 && paramTypes[0] == this.getClass()) {
				return constructor.newInstance(this); // instância usando o construtor
			}
		}

		throw new InstantiationException("Could not instantiate the new revision object.");
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	/**
	 * Retorna o próximo número de revisão que deve ser atribuido à próxima
	 * revisão a ser criada.
	 */
	int nextRevisionNumber() {
		R currentRevision = currentRevision();
		return currentRevision == null ? 1 : currentRevision.getRevisionNumber() + 1;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}

	public SortedSet<R> getRevisions() {
		return revisions;
	}

	public void setRevisions(SortedSet<R> revisions) {
		this.revisions = revisions;
	}
}
