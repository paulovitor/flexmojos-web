package tv.snews.anews.domain;

import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;

/**
 * Representa um relatório escrito por um usuário do sistema reportando um
 * evento relacionado a uma reportagem.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
@Entity
@Indexed(index = "indexes/reportagereport")
public class ReportageReport extends Report {

	private Reportage reportage;
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public Reportage getReportage() {
	    return reportage;
    }
    
    public void setReportage(Reportage reportage) {
	    this.reportage = reportage;
    }
    
    private static final long serialVersionUID = 6559113657008253929L;
}
