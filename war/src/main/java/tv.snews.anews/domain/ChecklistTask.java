package tv.snews.anews.domain;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Date;
import java.util.Objects;

import static org.hibernate.search.annotations.Analyze.NO;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
@Entity
@Indexed(index = "indexes/checklisttask")
public class ChecklistTask extends AbstractEntity<Long> implements Comparable<ChecklistTask> {

    @Field(analyze = NO)
    private Date date;

    @Field(analyze = NO)
    private Date time;

    @Field
    private String label;

    @Field
    private String info;

    private boolean done;
    private int index;

    @ContainedIn
    private Checklist checklist;

    //----------------------------------
    //  Constructor
    //----------------------------------

    public ChecklistTask() {
        // Usado pelo Hibernate
    }

    public ChecklistTask(String label) {
        this.label = label;
    }

    //----------------------------------
    //  Operations
    //----------------------------------
    @Override
    public int compareTo(ChecklistTask other) {
        return index - other.index;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj instanceof ChecklistTask) {
            ChecklistTask other = (ChecklistTask) obj;
            return Objects.equals(index, other.index)
                && Objects.equals(label, other.label);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(label, info, done);
    }

    public ChecklistTask createCopy(Checklist checklistCopy){
        ChecklistTask copy = new ChecklistTask();

        copy.setDate(date);
        copy.setTime(time);
        copy.setLabel(label);
        copy.setInfo(info);
        copy.setDone(done);
        copy.setIndex(index);
        copy.setChecklist(checklistCopy);
        return copy;
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Checklist getChecklist() {
        return checklist;
    }

    public void setChecklist(Checklist checklist) {
        this.checklist = checklist;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    private static final long serialVersionUID = 255408785954309006L;
}
