package tv.snews.anews.domain;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.search.annotations.DocumentId;

import java.io.Serializable;
import java.util.Objects;

/**
 * Classe abstrata que define a base para as entidades não versionadas.
 * 
 * @param <T>
 * @author Felipe Pinheiro.
 * @since 1.0.0
 */
public abstract class AbstractEntity<T> implements Serializable {

	private static final long serialVersionUID = -1836267760457921007L;
	
	@DocumentId
	protected T id;

	public AbstractEntity() {
		// Hibernate
	}

	public AbstractEntity(T identifier) {
		this.id = identifier;
	}

	public static boolean isValid(AbstractEntity<?> entity) {
		return entity != null && entity.getId() != null;
	}
	
	@SuppressWarnings("rawtypes")
	private boolean idEquals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof AbstractEntity) {
			AbstractEntity other = (AbstractEntity) obj;
			return Objects.equals(id, other.id);
		}
		return false;
	}
	
	private int idHashCode() {
		return Objects.hash(id);
	}
	
	@Override
    public boolean equals(Object other) {
        return idEquals(other);
    }
	
    @Override
    public int hashCode() {
        return idHashCode();
    }
    
    @Override
    public String toString() {
    	return new ToStringBuilder(this).append("id", id).toString();
    }
    
	public T getId() {
		return id;
	}

	public void setId(T value) {
		this.id = value;
	}
}
