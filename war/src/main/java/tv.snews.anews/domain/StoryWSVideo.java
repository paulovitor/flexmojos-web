package tv.snews.anews.domain;

import javax.persistence.Transient;
import java.util.Date;

/**
 * @author Felipe Pinheiro
 */
public class StoryWSVideo extends StorySubSection {

    private static final long serialVersionUID = -7855257840336538028L;

    private WSMedia media;
    private MosDeviceChannel channel;
    private String slug = "";
    private Date durationTime;
    private boolean disabled;

    @Override
    protected StorySubSection clone() throws CloneNotSupportedException {
        StoryWSVideo storyWSVideo = new StoryWSVideo();
        storyWSVideo.setMedia(this.media);
        storyWSVideo.setChannel(this.channel);
        storyWSVideo.setOrder(this.order);
        storyWSVideo.setSlug(this.slug);
        storyWSVideo.setDurationTime(this.durationTime);
        storyWSVideo.setDisabled(this.disabled);
        return storyWSVideo;
    }

    //------------------------------------
    //  Getters & Setters
    //------------------------------------

    public WSMedia getMedia() {
        return media;
    }

    public void setMedia(WSMedia wsMedia) {
        updateStoryWSVideo(wsMedia);
        this.media = wsMedia;
    }

    public void updateStoryWSVideo(WSMedia wsMedia){
        if (wsMedia != null) {
            this.durationTime = wsMedia.getDurationTime();
            this.slug = wsMedia.getSlug();
        }
        this.disabled = (wsMedia == null);
    }

    public MosDeviceChannel getChannel() {
        return channel;
    }

    public void setChannel(MosDeviceChannel channel) {
        this.channel = channel;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Date durationTime) {
        this.durationTime = durationTime;
    }

    @Transient
    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
