package tv.snews.anews.domain;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Classe responsável que representa o screenName do usuário no Twitter. 
 * Ex:twitter.com/(screenName)
 *
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TwitterUser extends AbstractEntity<Long> {

	private static final long serialVersionUID = -1335275467113261338L;
	
	private String screenName;
	private	SortedSet<TweetInfo> tweetsInfo = new TreeSet<>();
	private String name;
	private String profileImageUrl;
	
    /**
     * @return the name
     */
    public String getName() {
    	return name;
    }

	
    /**
     * @param name the name to set
     */
    public void setName(String name) {
    	this.name = name;
    }

	
    /**
     * @return the profileImageUrl
     */
    public String getProfileImageUrl() {
    	return profileImageUrl;
    }

	
    /**
     * @param profileImageUrl the profileImageUrl to set
     */
    public void setProfileImageUrl(String profileImageUrl) {
    	this.profileImageUrl = profileImageUrl;
    }


	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}

	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	/**
	 * @return the tweetsInfo
	 */
	public SortedSet<TweetInfo> getTweetsInfo() {
		return tweetsInfo;
	}

	/**
	 * @param tweetsInfo the tweetsInfo to set
	 */
	public void setTweetsInfo(SortedSet<TweetInfo> tweetsInfo) {
		this.tweetsInfo = tweetsInfo;
	}
	
	
}
