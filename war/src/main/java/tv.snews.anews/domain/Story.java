package tv.snews.anews.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.annotations.*;
import org.springframework.flex.core.io.AmfIgnore;
import tv.snews.anews.infra.search.DocumentIndexingInterceptor;
import tv.snews.anews.infra.search.StorySectionLuceneBridge;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.anews.util.ResourceBundle;

import javax.persistence.Entity;
import java.util.*;

import static org.apache.commons.lang.StringUtils.isBlank;
import static tv.snews.anews.util.DateTimeUtil.*;

/**
 * Classe que representa uma lauda de um espelho.
 *
 * @author Felipe Pinheiro
 * @author Paulo Felipe
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
@Entity
@Indexed(index = "indexes/story", interceptor = DocumentIndexingInterceptor.class)
public class Story extends Document<StoryRevision> implements Comparable<Story>, Cloneable {

	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
    public static enum Fields { APPROVED, DISPLAYED, DISPLAYING, EDITOR, EXPECTED, IMAGE_EDITOR, OK, PAGE, REPORTER, SLUG, TOTAL, VT}

	private int order;
	private String page;

	private Date expected;
	private Date head;
	private Date vt;
	private Date off;
	private Date total;

	private boolean ok = false;
	private boolean approved = false;
    private boolean videosAdded = false;
    private boolean vtManually = true;
    private boolean displaying = false;
    private boolean displayed = false;
    private boolean cutted = false;
    private boolean copied = false;

	@IndexedEmbedded
	private Block block;
	private User editor;
    private User imageEditor;
    private User reporter;
    private Guideline guideline;
    private Reportage reportage;
    private News news;
	
	private StoryKind storyKind;
	private Story sourceStory;
	private String mosStatus;

	@Field
	@Boost(1.0f)
	private String information;

	@Field
	private String tapes;

	@Field
	@Boost(1.5f)
	@FieldBridge(impl = StorySectionLuceneBridge.class)
	private StorySection headSection;

	@Field
	@Boost(1.0f)
	@FieldBridge(impl = StorySectionLuceneBridge.class)
	private StoryNCSection ncSection;

	// Sem indexação porque não tem texto
	private StoryVTSection vtSection;

	@Field
	@Boost(0.5f)
	@FieldBridge(impl = StorySectionLuceneBridge.class)
	private StorySection footerSection;

	@Field
	@Boost(1.5f)
	@FieldBridge(impl = StorySectionLuceneBridge.class)
	private StorySection offSection;

	private SortedSet<StoryCopyLog> copiesHistory = new TreeSet<>();

	private transient User editingUser; // não é persistido
	private transient List<? extends StorySubSection> cgSubSections = new ArrayList<>(); // não é persistido

	//Contadores utilizados para nomear as seções
	private int countOff = 0;
	private int countInterview = 0;
	private int countAppearance= 0;
	private int countArt = 0;
	private int countSoundUp = 0;
	
	public Story() {
		super();
		this.slug = "";
	}
	
	public Story(News news) {
		this.news = news;
		this.information = copyNewsData(news);
		this.slug = news.getSlug();
	}

	private String copyNewsData(News news) {
		StringBuilder builder = new StringBuilder();
		
		if (StringUtils.isNotBlank(news.getFont())) {
			builder.append("\n");
			builder.append(bundle.getMessage("reportage.fields.font").toUpperCase());
			builder.append("\n");
			builder.append(news.getFont());
		}
		
		if (StringUtils.isNotBlank(news.getAddress())) {
			builder.append("\n");
			builder.append("\n");
			builder.append(bundle.getMessage("reportage.fields.address").toUpperCase());
			builder.append("\n");
			builder.append(news.getAddress());
		}

		if (StringUtils.isNotBlank(news.getLead())) {
			builder.append("\n");
			builder.append("\n");
			builder.append(bundle.getMessage("reportage.fields.lead").toUpperCase());
			builder.append("\n");
			builder.append(news.getLead());
		}
		
		if (StringUtils.isNotBlank(news.getInformation())) {
			builder.append("\n");
			builder.append("\n");
			builder.append(bundle.getMessage("reportage.fields.information").toUpperCase());
			builder.append("\n");
			builder.append(news.getInformation());
		}

		if (news.getContacts() != null && news.getContacts().size() > 0) {
			builder.append("\n");
			builder.append("\n");
			builder.append(bundle.getMessage("reportage.fields.contacts"));
			
			builder.append("\n");
			for (Contact contact : news.getContacts()) {
				builder.append(contact.getName());
	
				ContactNumber number = contact.getFirstNumber();
				if (number != null) {
					builder.append(" - ");
					builder.append(contact.getFirstNumber().getValue());
				}
				builder.append("\n");
			}
		}
		return builder.toString();
	}
	public Story(Guideline guideline) {
		this.guideline = guideline;
		this.slug = guideline.getSlug();
	}

	public Story(Reportage reportage) {
		this.reportage = reportage;
		this.slug = reportage.getSlug();

		if (!reportage.getCgs().isEmpty()) {
			this.vtSection = new StoryVTSection();
		}

		for (ReportageCG reportageCG : reportage.getCgs()) {
			StoryCG storyCG = new StoryCG(reportageCG);
			this.vtSection.addSubSection(storyCG);
		}

		if (StringUtils.isNotBlank(reportage.getTipOfHead())) {
			this.headSection = new StorySection();

			StoryCameraText storyCameraText = new StoryCameraText();
			storyCameraText.setText(reportage.getTipOfHead());
			storyCameraText.setCamera(0);
			storyCameraText.calculateReadTime();

			this.headSection.addSubSection(storyCameraText);
		}

		if (StringUtils.isNotBlank(reportage.getInformation())) {
			setInformation(reportage.getInformation());
		}
		
		StringBuilder builder = new StringBuilder();
		for (ReportageSection section : reportage.getSections()) {
			if (StringUtils.isNotBlank(section.getContent())) {
				builder.append("\n");
				builder.append("["+ getFormattedName(section) + "]" );
				builder.append("\n");
				builder.append(section.getContent());
				if ( StringUtils.isNotBlank(section.getObservation())) {
					builder.append("\n");
					builder.append(section.getType().equals(ReportageSectionType.ART) ? bundle.getMessage("reportage.section.data") :	bundle.getMessage("reportage.section.quote"));
					builder.append(section.getObservation());
				}
				builder.append("\n");
			}
		}
		resetCounters();
		String sectionsText = builder.toString();
		if (StringUtils.isNotBlank(sectionsText)) {
			this.offSection = new StorySection();
			StoryText storyText = new StoryText();
			storyText.setText(sectionsText);
			this.offSection.addSubSection(storyText);
		}
		
	}

	private void resetCounters() {
		countAppearance = 0;
		countArt = 0;
		countInterview = 0;
		countOff = 0;
		countSoundUp = 0;
	}
	
	private String getFormattedName(ReportageSection section) {
		String name = bundle.getMessage("reportage.sectionType." + section.getType().toString().toLowerCase());
		
		switch (section.getType()){
				case APPEARANCE:
					countAppearance++;
					return  name + " "  + countAppearance;
				case ART:
					countArt++;
					return  name + " "  + countArt;					
				case INTERVIEW:
					countInterview++;
					return  name + " "  + countInterview;			
					
				case OFF:
					countOff++;
					return  name + " "  + countOff;	
				case SOUNDUP:
					countSoundUp++;
					return  name + " "  + countSoundUp;	
				default:
					return "";
		}
		
	}
	
	public Story(Block block, StoryTemplate template, User author) {
		this.block = block;
		this.author = author;
		this.page = template.getPage();
		this.slug = template.getSlug();
		this.information = template.getInformation();
		this.tapes = template.getTapes();
		this.vt = template.getVt();
		this.order = template.getOrder();
		this.storyKind = template.getKind();
		this.editor = template.getEditor();

		try {
			this.headSection = template.getHeadSection() == null ? null : template.getHeadSection().clone();
			this.offSection = template.getOffSection() == null ? null : template.getOffSection().clone();
			this.footerSection = template.getFooterSection() == null ? null : template.getFooterSection().clone();
			this.ncSection = template.getNcSection() == null ? null : template.getNcSection().clone();
			this.vtSection = template.getVtSection() == null ? null : template.getVtSection().clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e); // não deve ocorrer
		}

		// Valores padrões
		this.approved = false;
		this.videosAdded = false;
		this.vtManually = false;
		this.excluded = false;
		this.ok = false;
		this.date = new Date();
		this.expected = DateTimeUtil.getMidnightTime();
        this.state = DocumentState.PRODUCING;

		this.calculateTotal();
	}

	/**
	 * Busca pela primeira lauda ancestral que seja da gaveta.
	 *
	 * @return a lauda da gaveta original ou {@code null} caso não exista
	 */
	public Story findDrawerOrigin() {
		return findDrawerOrigin(sourceStory);
	}

	/*
	 * Busca recursivamente pela lauda ancestral da gaveta.
	 */
	private Story findDrawerOrigin(Story story) {
		if (story == null) {
			return null;
		}
		return findDrawerOrigin(story.sourceStory);
	}

	/**
	 * Carrega os relacionamentos lazy deste objeto.
	 */
	public void loadLazies() {
		if (headSection != null) {
			headSection.loadLazies();
		}
		if (ncSection != null) {
			ncSection.loadLazies();
		}
		if (vtSection != null) {
			vtSection.loadLazies();
		}
		if (footerSection != null) {
			footerSection.loadLazies();
		}
		if (offSection != null) {
			offSection.loadLazies();
		}
	}

	@Override
	public Story clone() throws CloneNotSupportedException {
		Story clone = new Story();
		clone.copyData(this);

		clone.approved = this.approved;
        clone.videosAdded = this.videosAdded;
        clone.vtManually = this.vtManually;
        clone.author = this.author;
        clone.version = this.version;
        clone.reporter = this.reporter;
        clone.editor = this.editor;
        clone.imageEditor = this.imageEditor;
        clone.guideline = this.guideline;
        clone.reportage = this.reportage;
        clone.ok = this.ok;
        clone.order = this.order;
        clone.page = this.page;
        clone.storyKind = this.storyKind;
        clone.expected = this.expected;
		clone.head = this.head;
		clone.vt = this.vt;
		clone.off = this.off;
		clone.total = this.total;
		clone.information = this.information;
		clone.tapes = this.tapes;

		for (StoryRevision revision : this.revisions) {
			clone.revisions.add(revision.clone(clone));
		}

		clone.headSection = headSection == null ? null : headSection.clone();
		clone.ncSection = ncSection == null ? null : ncSection.clone();
		clone.vtSection = vtSection == null ? null : vtSection.clone();
		clone.footerSection = footerSection == null ? null : footerSection.clone();
		clone.offSection = offSection == null ? null : offSection.clone();
        clone.setVideosAdded(this.hasVideos());

		return clone;
	}

	/**
	 * Utilizado pelo método {@link java.util.Collections#sort(java.util.List)}, em outras classes,
	 * para ordenar uma coleção de objetos {@link Story}.
	 */
	@Override
	public int compareTo(Story other) {
		int diff;

		// Trata a possibilidade de bloco nulo
		if (block == other.block) {
			diff = 0;
		} else if (block == null) {
			return 1;
		} else if (other.block == null) {
			return -1;
		} else {
			diff = block.compareTo(other.block);
		}

		// Se forem de blocos diferentes, basea-se na ordem dos blocos, senão basea-se na ordem das laudas
		return diff != 0 ? diff : order - other.order;
	}

	public <T extends StorySubSection> int countSubSectionsOfType(Class<T> type) {
		int count = 0;

		for (StorySection section : allSections()) {
			if (section != null) {
				count += section.countSubSectionsOfType(type);
			}
		}

		return count;
	}

	/**
	 * Retorna todas as sub-seções do tipo informado.
	 *
	 * @param type tipo desejado
	 * @return as sub-seções encontradas
	 */
	public <T extends StorySubSection> List<T> subSectionsOfType(Class<T> type) {
		List<T> results = new ArrayList<>();

		for (StorySection section : allSections()) {
			if (section != null) {
				results.addAll(section.subSectionsOfType(type));
			}
		}

		return results;
	}

	/**
	 * Remove todas as sub-seções do tipo informado.
	 *
	 * @param type tipo a ser removido
	 * @param <T>  tipo de {@link StorySubSection}
	 */
	public <T extends StorySubSection> void removeSubSectionsOfType(Class<T> type) {
		for (StorySection section : allSections()) {
			if (section != null) {
				section.removeSubSectionsOfType(type);
			}
		}
	}

	/**
	 * Calcula e retorna o tempo total desta lauda, atualizando o atributo que
	 * armazena o tempo total.
	 * <p>
	 * As durações das suas seções também serão calculadas e atualizadas.
	 * </p>
	 *
	 * @return o tempo da duração total dessa lauda
	 */
	public Date calculateTotal() {
		head = getMidnightTime();
		vt = vt == null ? getMidnightTime() : vt;
		off = getMidnightTime();
		Date aux = getMidnightTime();

		if (headSection != null) {
			head = sumTimes(head, headSection.calculateDuration());
		}
		if (vtSection != null) {
			aux = sumTimes(aux, vtSection.calculateDuration());
			if (!vtManually) {
				vt = aux;
			} else {
				vtSection.setDuration(vt);
			}
		}
		if (ncSection != null) {
			aux = sumTimes(aux, ncSection.calculateDuration());
			if (!vtManually) {
				vt = aux;
			} else {
				ncSection.setDuration(vt);
			}
		}
		if (footerSection != null) {
			head = sumTimes(head, footerSection.calculateDuration());
		}
		if (offSection != null) {
			off = offSection.calculateDuration();
		}
		total = sumTimes(head, vt);
		return total;
	}

	public String obtainReporterNickname() {
		if (guideline != null && !guideline.getReporters().isEmpty()) {
			return guideline.getReporters().iterator().next().getNickname();
		}
		if (reportage != null && reportage.getReporter() != null) {
			return reportage.getReporter().getNickname();
		}
		if (reporter != null) {
			return reporter.getNickname();
		}
		return "";
	}

    public boolean hasVideos() {
        return (this.vtSection != null && this.vtSection.hasVideos()) ||
                (this.ncSection != null && this.ncSection.hasVideos());
    }

	public StorySection[] allSections() {
		return new StorySection[] { headSection, ncSection, vtSection, footerSection, offSection };
	}

	public String formatField(String field, int size) {
		if (isBlank(field)) {
			return "";
		} else {
			field = field.trim();
			if (field.length() > size) {
				return field.substring(0, size - 3) + "...";
			} else {
				return field;
			}
		}
	}

	
    //------------------------------------
	//  Getters & Setters
	//------------------------------------

    public News getNews() {
    	return news;
    }
	
    public void setNews(News news) {
    	this.news = news;
    }
	
	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public Date getExpected() {
		return expected;
	}

	public void setExpected(Date expected) {
		this.expected = expected == null ? getMidnightTime() : addTimeToCurrentDate(expected);
	}

	public boolean isOk() {
		return ok;
	}

	public void setOk(boolean ok) {
		this.ok = ok;
	}

    public boolean isDisplaying() {
        return displaying;
    }

    public void setDisplaying(boolean displaying) {
        this.displaying = displaying;
    }

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }

    public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;

		if (block != null && block.getRundown() != null) {
			this.date = block.getRundown().getDate();
			this.program = block.getRundown().getProgram();
		}
	}

	public Date getTotal() {
		return total;
	}

	public void setTotal(Date total) {
		this.total = total == null ? getMidnightTime() : addTimeToCurrentDate(total);
	}

	public User getEditor() {
		return editor;
	}

	public void setEditor(User editor) {
		this.editor = editor;
	}

    public User getImageEditor() {
        return imageEditor;
    }

    public void setImageEditor(User imageEditor) {
        this.imageEditor = imageEditor;
    }

	public User getReporter() {
		return reporter;
	}

	public void setReporter(User reporter) {
		this.reporter = reporter;
	}

	public Guideline getGuideline() {
		return guideline;
	}

	public void setGuideline(Guideline guideline) {
		this.guideline = guideline;
	}

	public Reportage getReportage() {
		return reportage;
	}

	public void setReportage(Reportage reportage) {
		this.reportage = reportage;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public boolean isCutted() {
		return cutted;
	}

	public void setCutted(boolean cutted) {
		this.cutted = cutted;
	}
	
	public boolean isCopied() {
		return copied;
	}

	public void setCopied(boolean copied) {
		this.copied = copied;
	}
	
    public boolean isVideosAdded() { return videosAdded; }

    public void setVideosAdded(boolean videosAdded) { this.videosAdded = videosAdded; }

    public boolean isVtManually() {
        return vtManually;
    }

    public void setVtManually(boolean vtManually) {
        this.vtManually = vtManually;
    }

    public User getEditingUser() {
		return editingUser;
	}

	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}

	public StoryKind getStoryKind() {
		return storyKind;
	}

	public void setStoryKind(StoryKind storyKind) {
		this.storyKind = storyKind;
	}

	public StorySection getHeadSection() {
		return headSection;
	}

	public void setHeadSection(StorySection headSection) {
		this.headSection = headSection;
	}

	public StoryNCSection getNcSection() {
		return ncSection;
	}

	public void setNcSection(StoryNCSection ncSection) {
		this.ncSection = ncSection;
	}

	public StoryVTSection getVtSection() {
		return vtSection;
	}

	public void setVtSection(StoryVTSection vtSection) {
		this.vtSection = vtSection;
	}

	public String getMosStatus() {
		return mosStatus;
	}

	public void setMosStatus(String mosStatus) {
		this.mosStatus = mosStatus;
	}

	public StorySection getFooterSection() {
		return footerSection;
	}

	public void setFooterSection(StorySection footerSection) {
		this.footerSection = footerSection;
	}

	public StorySection getOffSection() {
		return offSection;
	}

	public void setOffSection(StorySection offSection) {
		this.offSection = offSection;
	}

	public SortedSet<StoryCopyLog> getCopiesHistory() {
		return copiesHistory;
	}

	public void setCopiesHistory(SortedSet<StoryCopyLog> copiesHistory) {
		this.copiesHistory = copiesHistory;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getTapes() {
		return tapes;
	}

	public void setTapes(String tapes) {
		this.tapes = tapes;
	}

	public Date getHead() {
		return head;
	}

	public void setHead(Date head) {
		this.head = head == null ? getMidnightTime() : addTimeToCurrentDate(head);
	}

	public Date getVt() {
		return vt;
	}

	public void setVt(Date vt) {
		this.vt = vt == null ? getMidnightTime() : addTimeToCurrentDate(vt);
	}

	public Date getOff() {
		return off;
	}

	public void setOff(Date off) {
		this.off = off == null ? getMidnightTime() : addTimeToCurrentDate(off);
	}

	public Story getSourceStory() {
		return sourceStory;
	}

	public void setSourceStory(Story sourceStory) {
		this.sourceStory = sourceStory;
	}

	@AmfIgnore
	public List<? extends StorySubSection> getCgSubSections() {
		return cgSubSections;
	}

	public void setCgSubSections(List<? extends StorySubSection> cgSubSections) {
		this.cgSubSections = cgSubSections;
	}

	private static final long serialVersionUID = -8124166705763116380L;
}
