package tv.snews.anews.domain;

import org.hibernate.search.annotations.*;
import org.springframework.flex.core.io.AmfIgnore;

import javax.persistence.Entity;
import java.util.Date;

import static org.hibernate.search.annotations.Analyze.NO;
import static org.hibernate.search.annotations.Resolution.MILLISECOND;

/**
 * Representa os relatórios escritos pelos usuários do sistema reportando 
 * eventos que podem ocorrer no dia a dia do jornalismo.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
@Entity
@Indexed(index = "indexes/report")
public class Report extends AbstractEntity<Long> {

	@Field @Boost(2.0f)
	private String slug;
	
	@Field
	private String content;
	
	@Field(analyze = NO)
	@DateBridge(resolution = MILLISECOND)
	private Date date;
	
	private Date lastChange;
	private User author;
	
	@IndexedEmbedded
	private ReportEvent event;
	
	@IndexedEmbedded
	private ReportNature nature;
	
	private User editingUser; // não é salvo
	
	private transient String origin;
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public String getSlug() {
	    return slug;
    }
    
    public void setSlug(String slug) {
	    this.slug = slug;
    }
    
    public String getContent() {
	    return content;
    }
    
    public void setContent(String content) {
	    this.content = content;
    }
    
    public Date getDate() {
	    return date;
    }
    
    public void setDate(Date date) {
	    this.date = date;
    }
    
    public Date getLastChange() {
	    return lastChange;
    }
    
    public void setLastChange(Date lastChange) {
	    this.lastChange = lastChange;
    }
    
    public User getAuthor() {
	    return author;
    }
    
    public void setAuthor(User author) {
	    this.author = author;
    }
    
    public ReportEvent getEvent() {
	    return event;
    }
    
    public void setEvent(ReportEvent event) {
	    this.event = event;
    }
    
    public ReportNature getNature() {
	    return nature;
    }
    
    public void setNature(ReportNature nature) {
	    this.nature = nature;
    }
	
    public User getEditingUser() {
	    return editingUser;
    }
    
    public void setEditingUser(User editingUser) {
	    this.editingUser = editingUser;
    }
    
	@AmfIgnore
    public String getOrigin() {
    	return origin;
    }
    
    public void setOrigin(String origin) {
    	this.origin = origin;
    }
    
    private static final long serialVersionUID = -6487217571340416130L;
}
