package tv.snews.anews.domain;

import org.hibernate.search.annotations.*;
import tv.snews.anews.infra.search.ContactIndexingInterceptor;
import tv.snews.anews.infra.search.EmailLuceneBridge;

import javax.persistence.Entity;

/**
 * @author Felipe Pinheiro
 * @author Paulo Felipe
 * @since 1.2.0
 */
@Entity
@Indexed(index = "indexes/agendacontact", interceptor = ContactIndexingInterceptor.class)
public class AgendaContact extends Contact {

	@Field
	@Boost(3.0f)
	private String company;

	@Field
	@FieldBridge(impl = EmailLuceneBridge.class)
	@Boost(1.0f)
	private String email;

//	@Field(analyze = NO) || Não precisa porque está usando o ContactIndexingInterceptor
	private boolean excluded;

	@Field
	@Boost(2.0f)
	private String info;

	@Field
	@Boost(1.0f)
	private String address;

	@IndexedEmbedded
	@Boost(1.0f)
	private City city;

	@IndexedEmbedded
	@Boost(3.0f)
	private ContactGroup contactGroup;

	//----------------------------------
	//  Operations
	//----------------------------------

	@Override
	public Contact createCopy() {
		// Não se cria cópias de contatos da agenda, então retorna a mesma referência
		return this;
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isExcluded() {
		return excluded;
	}

	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public ContactGroup getContactGroup() {
		return contactGroup;
	}

	public void setContactGroup(ContactGroup contactGroup) {
		this.contactGroup = contactGroup;
	}

	private static final long serialVersionUID = -2834765240548027328L;
}
