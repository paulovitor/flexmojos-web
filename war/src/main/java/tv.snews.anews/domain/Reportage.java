package tv.snews.anews.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.annotations.Boost;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import tv.snews.anews.infra.search.DocumentIndexingInterceptor;
import tv.snews.anews.util.ResourceBundle;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.*;

/**
 * Representação das reportagens que são criadas pelos repórteres. Ela sempre é
 * criada a partir de uma pauta e mantendo sempre esse vínculo. Alterações nos
 * dados da pauta de origem - como a retranca, programa e repórter - irão afetar
 * diretamente na apresentação da reportagem vinculada.
 *
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/reportage", interceptor = DocumentIndexingInterceptor.class)
public class Reportage extends Document<ReportageRevision> {

	private static final long serialVersionUID = 6614161313339504044L;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	private boolean ok;

	@Field
	private String cameraman;

	@IndexedEmbedded
	private Guideline guideline;

	@IndexedEmbedded
	private User reporter;

	@Field
	@Boost(1.0f)
	private String information;

	@Field
	@Boost(1.0f)
	private String tipOfHead;

	@IndexedEmbedded
	private Program program;

	private Set<CommunicationVehicle> vehicles = new HashSet<>();

	@IndexedEmbedded
	private SortedSet<ReportageSection> sections = new TreeSet<>(); // o SortedSet influi na lógica abaixo

    private SortedSet<ReportageCG> cgs = new TreeSet<>();

	private SortedSet<ReportageCopyLog> copiesHistory = new TreeSet<>();

	@Transient
	private User editingUser;

	@Transient
	private boolean blank;

    //--------------------------------------------------------------------------
	//	Constructors
	//--------------------------------------------------------------------------

	/**
	 * Construtor básico que normalmente não será utilizado, mas que é
	 * necessário para o funcionamento do Hibernate. Na criação de novas
	 * reportagens
	 * dê preferência ao construtor {@link Reportage#Reportage(Guideline)}
	 */
	public Reportage() {
		super();
		this.excluded = false;
	}

	/**
	 * Construtor principal para a criação de novas reportagens. Nele é
	 * atribuído o valor obrigatório da pauta a qual essa reportagem será
	 * vinculada.
	 *
	 * @param guideline Pauta que originou essa reportagem.
	 */
	public Reportage(Guideline guideline) {
		this();
		this.guideline = guideline;
	}

	//--------------------------------------------------------------------------
	//	Operations
	//--------------------------------------------------------------------------

	public Reportage createCopy(User author) throws CloneNotSupportedException {
		Reportage copy = new Reportage();
		copy.copyData(this);
		copy.setAuthor(author);

        copy.setOk(ok);
        copy.setCameraman(cameraman);
        copy.setGuideline(guideline);
        copy.setReporter(reporter);
        copy.setInformation(information);
        copy.setTipOfHead(tipOfHead);

        copy.getVehicles().addAll(vehicles);

        for (ReportageSection section : sections) {
            copy.getSections().add(section.createCopy(copy));
        }

        for (ReportageCG cg : cgs) {
					ReportageCG cgClone = cg.clone();
					cgClone.setReportage(copy);
					copy.getCgs().add(cgClone);
        }

        for (ReportageRevision revision : revisions) {
            copy.getRevisions().add(revision.createCopy(copy));
        }

        return copy;
    }

	/**
	 * Adiciona uma nova seção a essa reportagem, caso ela já não tenha sido
	 * adicionada antes. Nesse caso dela já existir, nada irá ocorrer.
	 *
	 * @param section Nova seção que será adicionada.
	 */
	public void addSection(ReportageSection section) {
		section.setReportage(this);
		section.setPosition(sections.size());
		sections.add(section);
	}

	/**
	 * Remove uma seção dessa reportagem e após isso atualiza as posições.
	 *
	 * @param section Seção a ser removida.
	 */
	public void removeSection(ReportageSection section) {
		sections.remove(section);

		// Renumera as ordens das seções
		int position = 0;
		for (ReportageSection current : sections) {
			current.setPosition(position++);
		}
	}

	/**
	 * Retorna a seção localizada na posição informada. Se a posição informada
	 * for maior que a quantidade de seções será retornado {@code null}.
	 *
	 * @param position Posição da seção desejada.
	 * @return {@code null} caso não encontre.
	 */
	public ReportageSection getSectionAt(int position) {
		int index = 0;
		for (ReportageSection section : sections) {
			if (index++ == position) {
				return section;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "Reportage[id=" + id
		    + ", excluded=" + excluded
		    + ", guideline_id=" + (guideline == null ? null : guideline.getId())
		    + ", sections_size=" + sections.size()
		    + "]";
	}

    public String vehiclesToString() {
    	SortedSet<String> vehiclesList = new TreeSet<>();
    	for (CommunicationVehicle communicationVehicle : vehicles) {
    		vehiclesList.add( bundle.getMessage("vehicle." + communicationVehicle.name().toLowerCase()));
    	}
    	return StringUtils.join(vehiclesList, ", ") + (vehiclesList.isEmpty() ? "" : ".");
    }

	/**
	 * Compara as seções desta reportagem com as seções da outra reportagem
	 * informada, verificando se possuem o mesmo contéudo nas mesmas posições.
	 *
	 * @param other outra reportagem a qual esta será comparada
	 * @return {@code true} se possuirem o mesmo conteúdo
	 */
	public boolean containsSameValues(Reportage other) {
		if (sections.size() != other.sections.size()) {
			return false;
		}

		/* Se tem a mesma quantidade de seções, compara seus conteúdos */

		Iterator<ReportageSection> thisIt = sections.iterator();
		Iterator<ReportageSection> otherIt = other.sections.iterator();

		while (thisIt.hasNext()) {
			ReportageSection thisSection = thisIt.next();
			ReportageSection otherSection = otherIt.next();

			if (!Objects.equals(thisSection.getContent(), otherSection.getContent()) ||
			    !Objects.equals(thisSection.getObservation(), otherSection.getObservation())) {
				return false;
			}
		}

		if (!Objects.equals(this.cameraman, other.getCameraman())) {
			return false;
		}

		if (!Objects.equals(this.program, other.getProgram())) {
			return false;
		}

		if (!Objects.equals(this.guideline, other.getGuideline())) {
			return false;
		}

		if (!Objects.equals(this.reporter, other.getReporter())) {
			return false;
		}

		if (!Objects.equals(this.information, other.getInformation())) {
			return false;
		}

		if (!Objects.equals(this.tipOfHead, other.getTipOfHead())) {
			return false;
		}

		if (!Objects.equals(this.slug, other.getSlug())) {
			return false;
		}

		return true;
	}

	/**
	 * Marca essa reportagem e suas seções como aprovadas.
	 *
	 * @return {@code true} se houve alteração no status
	 */
	public boolean approve() {
		if (!ok) {
			for (ReportageSection section : sections) {
				if (section.isApprovable()) {
					section.setOk(true);
				}
			}
			setOk(true);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Remove a aprovação da reportagem, se ela estivar aprovada.
	 *
	 * @return {@code true} se houve alteração no status
	 */
	public boolean disapprove() {
		if (ok) {
			setOk(false);
			return true;
		} else {
			return false;
		}
	}

	//--------------------------------------------------------------------------
	//	Getters & Setters
	//--------------------------------------------------------------------------

	/**
	 * Flag que indica se a reportagem foi aprovada ou não pelo editor.
	 *
	 * @return {@code true} se ela foi aprovada pelo editor.
	 */
	public boolean isOk() {
		return ok;
	}

	/**
	 * Permite indicar se a reportagem foi aprovada ou não pelo editor.
	 *
	 * @param ok Indicação se a reportagem foi aprovada ou não pelo editor.
	 */
	public void setOk(boolean ok) {
		this.ok = ok;
	}

	/**
	 * Retorna o cinegrafista da reportagem.
	 *
	 * @return Cinegrafista da reportagem.
	 */
	public String getCameraman() {
		return cameraman;
	}

	/**
	 * Define o cinegrafista da reportagem.
	 *
	 * @param cameraman cinegrafista da reportagem.
	 */
	public void setCameraman(String cameraman) {
		this.cameraman = cameraman;
	}

	/**
	 * Retorna a pauta que originou esta reportagem e a qual ela está vinculada.
	 *
	 * @return Pauta que originou a reportagem.
	 */
	public Guideline getGuideline() {
		return guideline;
	}

	/**
	 * Permite definir a pauta que originou esta reportagem e a qual ela será
	 * vinculada.
	 *
	 * @param guideline Pauta que originou a reportagem.
	 */
	public void setGuideline(Guideline guideline) {
		this.guideline = guideline;

		if (guideline != null) {
			if (slug == null) {
				slug = guideline.getSlug();
			}
			if (date == null) {
				date = guideline.getDate();
			}
			if (program == null) {
				program = guideline.getProgram();
			}
			if (reporter == null && !guideline.getReporters().isEmpty()) {
				reporter = guideline.getReporters().iterator().next();
			}
		}
	}

	public User getReporter() {
		return reporter;
	}

	public void setReporter(User reporter) {
		this.reporter = reporter;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

    public String getTipOfHead() {
    	return tipOfHead;
    }

    public void setTipOfHead(String tipOfHead) {
    	this.tipOfHead = tipOfHead;
    }

    public Set<CommunicationVehicle> getVehicles() {
        return vehicles;
    }

    public void setVehicles(Set<CommunicationVehicle> vehicles) {
        this.vehicles = vehicles;
    }

    /**
	 * Retorna as seções que foram criadas para essa reportagem. Por ser um
	 * {@link java.util.SortedSet}, elas se encontraram ordenadas segundo a a regra do
	 * {@link ReportageSection#compareTo(ReportageSection)}.
	 *
	 * @return the sections Sessões da reportagem.
	 */
	public SortedSet<ReportageSection> getSections() {
		return sections;
	}

	/**
	 * Define as seções dessa reportagem.
	 *
	 * @param sections Seções dessa reportagem.
	 */
	public void setSections(SortedSet<ReportageSection> sections) {
		this.sections = sections;
	}

    public SortedSet<ReportageCG> getCgs() {
        return cgs;
    }

    public void setCgs(SortedSet<ReportageCG> cgs) {
        this.cgs = cgs;
    }

    public SortedSet<ReportageCopyLog> getCopiesHistory() {
        return copiesHistory;
    }

    public void setCopiesHistory(SortedSet<ReportageCopyLog> copiesHistory) {
        this.copiesHistory = copiesHistory;
    }

    /**
	 * @return usuário que esta editando a reportagem no momento.
	 */
	public User getEditingUser() {
		return editingUser;
	}

	/**
	 * @param editingUser usuário que esta editando a reportagem no momento.
	 */
	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}

	/**
	 * @return the blank
	 */
	public boolean getBlank() {
		return blank;
	}

	/**
	 * @param blank the blank to set
	 */
	public void setBlank(boolean blank) {
		this.blank = blank;
	}

}
