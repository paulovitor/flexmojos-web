package tv.snews.anews.domain;

import java.util.Date;

import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class StoryTemplate extends AbstractEntity<Integer> implements Comparable<StoryTemplate>, Cloneable {

    public static enum Field { PAGE, SLUG, EDITOR, VT;}
    private String page;

    private String slug;
    private String information;
    private String tapes;
    private boolean videosAdded = false;

    private Date vt;

    private int order;

    private BlockTemplate block;
    private StoryKind kind;
    private User editor;

    private StorySection headSection;
    private StorySection offSection;
    private StorySection footerSection;
    private StoryNCSection ncSection;
    private StoryVTSection vtSection;

    public StoryTemplate() {}

    public StoryTemplate(BlockTemplate block) {
        this.block = block;
        this.page = block.getRundown().calculateNextPage();
        this.vt = getMidnightTime();
        this.order = block.getStories().size();
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    @Override
    public StoryTemplate clone() throws CloneNotSupportedException {
        StoryTemplate clone = new StoryTemplate();
        clone.page = page;
        clone.slug = slug;
        clone.information = information;
        clone.tapes = tapes;
        clone.videosAdded = this.videosAdded;
        clone.vt = vt;
        clone.order = order;
        clone.kind = kind;
        clone.editor = editor;

        clone.headSection = headSection == null ? null : headSection.clone();
        clone.offSection = offSection == null ? null : offSection.clone();
        clone.footerSection = footerSection == null ? null : footerSection.clone();
        clone.ncSection = ncSection == null ? null : ncSection.clone();
        clone.vtSection = vtSection == null ? null : vtSection.clone();

        return clone;
    }

    @Override
    public int compareTo(StoryTemplate other) {
        // Corrige falha na conversão do AMF
        if (block == null || other.block == null) {
            return order - other.order;
        }

        // Primeiro compara os blocos, depois as ordens das laudas
        int blockCompareResult = block.compareTo(other.block);

        if (blockCompareResult == 0) {
            return order - other.order;
        } else {
            return blockCompareResult;
        }
    }

    public boolean hasVideos() {
			return (vtSection != null && vtSection.countSubSectionsOfType(StoryMosVideo.class) > 0)
					|| (ncSection != null && ncSection.countSubSectionsOfType(StoryMosVideo.class) > 0);
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getTapes() {
        return tapes;
    }

    public void setTapes(String tapes) {
        this.tapes = tapes;
    }

    public boolean isVideosAdded() { return videosAdded; }

    public void setVideosAdded(boolean videosAdded) { this.videosAdded = videosAdded; }

    public Date getVt() {
        return vt;
    }

    public void setVt(Date vt) {
        this.vt = vt == null ? getMidnightTime() : addTimeToCurrentDate(vt);
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public BlockTemplate getBlock() {
        return block;
    }

    public void setBlock(BlockTemplate block) {
        this.block = block;
    }

    public StoryKind getKind() {
        return kind;
    }

    public void setKind(StoryKind kind) {
        this.kind = kind;
    }

    public User getEditor() {
        return editor;
    }

    public void setEditor(User editor) {
        this.editor = editor;
    }

    public StorySection getHeadSection() {
        return headSection;
    }

    public void setHeadSection(StorySection headSection) {
        this.headSection = headSection;
    }

    public StorySection getOffSection() {
        return offSection;
    }

    public void setOffSection(StorySection offSection) {
        this.offSection = offSection;
    }

    public StorySection getFooterSection() {
        return footerSection;
    }

    public void setFooterSection(StorySection footerSection) {
        this.footerSection = footerSection;
    }

    public StoryNCSection getNcSection() {
        return ncSection;
    }

    public void setNcSection(StoryNCSection ncSection) {
        this.ncSection = ncSection;
    }

    public StoryVTSection getVtSection() {
        return vtSection;
    }

    public void setVtSection(StoryVTSection vtSection) {
        this.vtSection = vtSection;
    }

    private static final long serialVersionUID = 7963589238874111802L;
}
