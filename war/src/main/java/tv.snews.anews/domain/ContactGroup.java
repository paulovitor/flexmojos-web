package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;

/**
 * Entidade que representa os grupos de contatos.
 *
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/contactgroup")
public class ContactGroup extends AbstractEntity<Integer> {

	@Field
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private static final long serialVersionUID = 2556914040088608988L;
}
