package tv.snews.anews.domain;

import org.hibernate.search.annotations.Boost;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.Indexed;
import org.springframework.flex.core.io.AmfIgnore;

import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static org.hibernate.search.annotations.Analyze.NO;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/contact")
public class Contact extends AbstractEntity<Integer> {

	@Fields({
			@Field,
			@Field(name = "nameSort", analyze = NO)
	})
	@Boost(4.0f)
	protected String name;

	@Field
	@Boost(3.0f)
	protected String profession;

	protected Set<ContactNumber> numbers = new HashSet<>();
	protected Set<Guide> guides = new HashSet<>(); // FIXME Precisa dessa coleção?
	protected Set<News> news = new HashSet<>(); // FIXME Precisa dessa coleção?

	//----------------------------------
	//  Operations
	//----------------------------------

	public Contact createCopy() {
		Contact copy = new Contact();
		copy.setName(name);
		copy.setProfession(profession);

		for (ContactNumber number : numbers) {
			copy.getNumbers().add(number.createCopy());
		}

		return copy;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof Contact) {
			Contact other = (Contact) obj;

			boolean thisIsNew = id == null || id <= 0;
			boolean otherIsNew = other.id == null || other.id <= 0;

			if (thisIsNew && otherIsNew) {
				// Nenhum dos contatos já foi cadastrado no banco
				return Objects.equals(name, other.name) && numbersAreEquals(other);
			} else {
				// Pelo menos um dos contatos já foi cadastrado no banco
				return Objects.equals(id, other.id);
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (id == null || id <= 0) {
			return Objects.hash(name, numbers);
		} else {
			return Objects.hash(id);
		}
	}

	private boolean numbersAreEquals(Contact other) {
		boolean areEquals = false;

		if (numbers.size() == other.numbers.size()) {
			for (ContactNumber number : numbers) {
				if (other.getNumbers().contains(number)) {
					areEquals = true;
				} else {
					areEquals = false;
					break;
				}
			}
		}

		return areEquals;
	}

	@AmfIgnore
	public ContactNumber getFirstNumber() {
		ContactNumber firstNumber = null;
		if (!numbers.isEmpty()) {
			firstNumber = numbers.iterator().next();
		}
		return firstNumber;
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Set<ContactNumber> getNumbers() {
		return numbers;
	}

	public void setNumbers(Set<ContactNumber> numbers) {
		this.numbers = numbers;
	}

	public Set<Guide> getGuides() {
		return guides;
	}

	public void setGuides(Set<Guide> guides) {
		this.guides = guides;
	}

	public Set<News> getNews() {
		return news;
	}

	public void setNews(Set<News> news) {
		this.news = news;
	}

	private static final long serialVersionUID = 5758423845640252956L;
}
