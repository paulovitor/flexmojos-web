package tv.snews.anews.domain;


/**
 * @author Samuel Guedes de Melo
 */
public class StoryCopyLog extends GenericCopyLog implements Comparable<StoryCopyLog> {

    private Story story;

    //----------------------------------
    //  Constructors
    //----------------------------------

    public StoryCopyLog() {
    }

    public StoryCopyLog(Story story, String nickname, String company) {
    	super(nickname, company);
        this.story = story;
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    @Override
    public int compareTo(StoryCopyLog other) {
        return getDate().compareTo(other.getDate());
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public Story getStory() {
    	return story;
    }

    public void setStory(Story story) {
    	this.story = story;
    }

    private static final long serialVersionUID = -2518241456898995925L;
}
