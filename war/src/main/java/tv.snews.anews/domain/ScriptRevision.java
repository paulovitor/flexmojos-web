package tv.snews.anews.domain;

import tv.snews.anews.util.ResourceBundle;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SortedSet;

import static org.apache.commons.lang.StringUtils.stripToEmpty;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptRevision extends AbstractRevision<Script> {

	private static final ResourceBundle bundle = ResourceBundle.INSTANCE;
	private static final long serialVersionUID = -7434181372415968023L;
	private String slug;
	private String tape;
	private String synopsis;
	private String presenter;
	private String reporter;
	private String vt;
	private String images;
	private String videos;

	//------------------------------
	//	Constructors
	//------------------------------

	// Hibernate & AMF
	public ScriptRevision() {
	}

	public ScriptRevision(Script script) {
		super(script);
	}

	//------------------------------
	//	Operations
	//------------------------------

	public ScriptRevision(ScriptRevision original) {
		this.slug = original.getSlug();
		this.tape = original.getTape();
		this.synopsis = original.getSynopsis();
		this.presenter = original.getPresenter();
		this.reporter = original.getReporter();
		this.vt = original.getVt();
		this.images = original.getImages();
		this.videos = original.getVideos();
	}

	@Override
	protected void fillRevision(Script entity) {
		this.slug = stripToEmpty(entity.getSlug());
		this.tape = stripToEmpty(entity.getTape());
		this.synopsis = stripToEmpty(entity.getSynopsis());
		this.presenter = entity.getPresenter() == null ? "" : stripToEmpty(entity.getPresenter().getNickname());
		this.reporter = entity.getReporter() == null ? "" : stripToEmpty(entity.getReporter().getNickname());
		this.vt = formatTime(entity.getVt());

		this.images = entity.getImages() != null ? stripToEmpty(fillImages(entity.getImages())) : "";
		this.videos = entity.getVideos() != null ? stripToEmpty(fillVideos(entity.getVideos())) : "";
	}
	
	private String fillImages(SortedSet<ScriptImage> images) {
		StringBuilder builder = new StringBuilder();
		for (ScriptImage image : images) {
			builder.append("\n");
			builder.append(bundle.getMessage("script.field.images")).append(": ");
			builder.append(image.getName() + "." + image.getExtension());
			builder.append("\n");
		}
		return builder.toString();
	}

	private String fillVideos(SortedSet<ScriptVideo> videos) {
		StringBuilder builder = new StringBuilder();
		for (ScriptVideo video : videos) {
			builder.append("\n");
			builder.append(bundle.getMessage("story.vt")).append(": ");
			builder.append(video.getSlug());
			builder.append("\n");
		}
		return builder.toString();
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	private String formatTime(Date value) {
		if (value != null) {
			SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
			return formatter.format(value);
		}
		return "";
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getTape() {
		return tape;
	}

	public void setTape(String tape) {
		this.tape = tape;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String getPresenter() {
		return presenter;
	}

	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public String getVt() {
		return vt;
	}

	public void setVt(String vt) {
		this.vt = vt;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getVideos() {
		return videos;
	}

	public void setVideos(String videos) {
		this.videos = videos;
	}
}
