package tv.snews.anews.domain;

import org.springframework.flex.core.io.AmfIgnore;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Transient;
import java.util.HashSet;
import java.util.Set;

/**
 * Entidade representativa das permissões de acesso ao ANews.
 * 
 * @author Samuel Gudes de Melo
 * @since 1.0.0
 */
public class Permission extends AbstractEntity<String> implements Comparable<Permission>, GrantedAuthority {

	private static final long serialVersionUID = 1L;

	private String name;

	/*
	 * Esse objeto precisa necessariamente ter esse nome (children) porque o
	 * componente tree que exibirá as sub-permissões auto identifica seus nos
	 * filhos como children.
	 */
	private Set<Permission> children = new HashSet<Permission>();

	@Transient
	private String state = "unchecked";

    //----------------------------------
    //  Constructors
    //----------------------------------

	public Permission() {
		super();
	}

	public Permission(String name, Set<Permission> children) {
		super();
		this.name = name;
		this.children = children;
	}

    //----------------------------------
    //  Operations
    //----------------------------------

    @Override
    public int compareTo(Permission other) {
        return id.compareTo(other.id);
    }

    @Override
    @AmfIgnore
    public String getAuthority() {
        return id;
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Permission> getChildren() {
		return children;
	}

	public void setChildren(Set<Permission> children) {
		this.children = children;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
