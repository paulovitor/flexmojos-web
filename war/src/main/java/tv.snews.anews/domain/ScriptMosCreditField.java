package tv.snews.anews.domain;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.7
 */
public class ScriptMosCreditField extends AbstractEntity<Long> implements Comparable<ScriptMosCreditField> {

    private String name;
    private String value;
    private int index;

    private ScriptMosCredit credit;

    // Hibernate
    public ScriptMosCreditField() {
        super();
    }

    public ScriptMosCreditField(ScriptMosCreditField original) {
        this.name = original.name;
        this.value = original.value;
    }

    @Override
    public int compareTo(ScriptMosCreditField other) {
        return index - other.index;
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public ScriptMosCredit getCredit() {
        return credit;
    }

    public void setCredit(ScriptMosCredit credit) {
        this.credit = credit;
    }

    private static final long serialVersionUID = 6390765481621789621L;
}
