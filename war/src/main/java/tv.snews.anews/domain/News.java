package tv.snews.anews.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Paulo Felipe
 * @author Samuel Guedes
 * @since 1.0.0
 */
public class News extends AbstractEntity<Long> {

	private static final long serialVersionUID = -3422146376249407471L;
	public static final int RESULTS_BY_PAGE = 50;
	
	private String font;
	private String slug;
	private String lead;
	private String information;
	private String address;
	private Date date;
	private User user;
	private Set<Contact> contacts = new HashSet<Contact>();
	
	private transient User editingUser; // não é persistido


	public boolean containsContact(Contact contact) {
		return contacts.contains(contact);
	}
	
	public String getFont() {
		return font;
	}

	public void setFont(String font) {
		this.font = font;
	}

    public String getSlug() {
    	return slug;
    }

    public void setSlug(String slug) {
    	this.slug = slug;
    }

	public String getLead() {
		return lead;
	}

	public void setLead(String lead) {
		this.lead = lead;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public User getEditingUser() {
		return editingUser;
	}

	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}
}
