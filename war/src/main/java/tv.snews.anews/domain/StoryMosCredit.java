package tv.snews.anews.domain;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class StoryMosCredit extends StoryMosObject {

	private String slug;
	private String channel;
	private boolean disabled = false;
	
	//Transient
	private boolean edited = false; 

	@Override
	protected StorySubSection clone() throws CloneNotSupportedException {
		StoryMosCredit clone = new StoryMosCredit();

		clone.mosMedia = mosMedia;
		clone.slug = slug;
		clone.channel = channel;
		clone.mosStatus = mosStatus;
		clone.order = order;
		clone.disabled = disabled;

		return clone;
	}

	public boolean isFilled() {
        if (isNotBlank(slug)) {
            return true;
        }
		return false;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isEdited() {
	    return edited;
    }

    public void setEdited(boolean edited) {
	    this.edited = edited;
    }

	private static final long serialVersionUID = 8050410313127861670L;
}
