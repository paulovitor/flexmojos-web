package tv.snews.anews.domain;

import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class RundownTemplate extends AbstractEntity<Integer> implements Cloneable {

    private String name;
    private Program program;
    private SortedSet<BlockTemplate> blocks = new TreeSet<>();

    private transient boolean editable = false;

    public RundownTemplate() {}

    public RundownTemplate(String name, Program program) {
        this.name = name;
        this.program = program;
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    public BlockTemplate createBlock() {
        BlockTemplate block = new BlockTemplate(this);
        blocks.add(block);

        return block;
    }

    public void removeBlock(BlockTemplate block) {
        blocks.remove(block);

        // Corrige as ordens após a remoção
        int orderCount = 0;
        for (BlockTemplate aux : blocks) {
            aux.setOrder(orderCount++);
        }
    }

    @Override
    public RundownTemplate clone() throws CloneNotSupportedException {
        RundownTemplate copy = new RundownTemplate();
        copy.name = name;
        copy.program = program;

        for (BlockTemplate block : blocks) {
            BlockTemplate blockCopy = block.clone();
            blockCopy.setRundown(copy);
            copy.blocks.add(blockCopy);
        }

        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj instanceof RundownTemplate) {
            RundownTemplate other = (RundownTemplate) obj;
            return Objects.equals(name, other.name)
                    && Objects.equals(program, other.program);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, program);
    }

    //----------------------------------
    //  Utilities Methods
    //----------------------------------
    
	/**
	 * Retorna a próxima página na númeração dentre as laudas já pertecentes a
	 * este espelho.
	 * 
	 * @return número da próxima p	ágina
	 */
	public String calculateNextPage() {
		int max = 0;
		for (BlockTemplate block : blocks) {
			for (StoryTemplate story : block.getStories()) {
				int aux = Integer.parseInt(story.getPage().replaceAll("[^\\d]", ""));
				max = aux > max ? aux : max;
			}
		}
		return ++max > 9 ? Integer.toString(max) : "0" + max;
	}
	
    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public SortedSet<BlockTemplate> getBlocks() {
        return blocks;
    }

    public void setBlocks(SortedSet<BlockTemplate> blocks) {
        this.blocks = blocks;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    private static final long serialVersionUID = -4020666597065435365L;
}
