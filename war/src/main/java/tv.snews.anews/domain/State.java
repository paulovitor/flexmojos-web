
package tv.snews.anews.domain;

/**
 * Domínio que representa um estado.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class State extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 7306230120041227209L;
	private String code;
	private String name;

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
