package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Objects;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
@Entity
@Indexed(index = "indexes/checklisttype")
public class ChecklistType extends AbstractEntity<Integer> implements Comparable<ChecklistType> {

    @Field
    private String name;

    //----------------------------------
    //  Operations
    //----------------------------------

    @Override
    public int compareTo(ChecklistType other) {
			if (name == other.name) return 0;
			if (name == null) return -1;
			if (other.name == null) return 1;

			return String.CASE_INSENSITIVE_ORDER.compare(name, other.name);
		}

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj instanceof ChecklistType) {
            ChecklistType other = (ChecklistType) obj;
            return Objects.equals(name, other.name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private static final long serialVersionUID = -8485479690401734088L;
}
