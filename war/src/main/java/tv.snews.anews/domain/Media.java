package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;

import javax.persistence.Entity;
import java.util.Date;

import static tv.snews.anews.util.DateTimeUtil.*;

@Entity
public abstract class Media extends AbstractEntity<Integer> {

    @Field
    protected String slug = "";

	protected Date durationTime;

    @Field
    protected String description;

	protected Date changed;

	protected Device device;

    public void updateFields(Media other){
        this.slug = other.slug;
        this.durationTime = other.durationTime;
        this.description = other.description;
        this.changed = other.changed;
        this.device = other.device;
    }

    //------------------------------------
    //  Getters & Setters
    //----------------------------------

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getDurationTime() {
        return roundToSecond(addTimeToCurrentDate(durationTime));
    }

    public void setDurationTime(Date durationTime) {
        this.durationTime = durationTime;
    }

    /**
     * Efetua o parse de data de string para date vindo do WebService.
     *
     * @param durationTime
     */
    public void setDurationTime(String durationTime) {
			if (durationTime == null) {
				this.durationTime = null;
			} else if (durationTime.length() == 0) {
				this.durationTime = getMidnightTime();
			} else {
				durationTime = durationTime.substring(0, durationTime.length() - 3);
				this.durationTime = timeStringToDate(durationTime);
			}
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getChanged() {
        return changed;
    }

    public void setChanged(Date changed) {
        this.changed = changed;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    private static final long serialVersionUID = 7463168158142765247L;
}
