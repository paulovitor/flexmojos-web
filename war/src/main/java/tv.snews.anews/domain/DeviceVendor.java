package tv.snews.anews.domain;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public enum DeviceVendor {

	AVID, CHYRON, EITV, HARRIS, MEDIA_PORTAL, ORAD, OTHER, SNEWS, VIZRT;
}
