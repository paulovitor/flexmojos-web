package tv.snews.anews.domain;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.Date;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Representa um feed RSS cadastrado pelo usuário.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssFeed extends AbstractEntity<Integer> implements Comparable<RssFeed> {

    private static final long serialVersionUID = 5421610322526671347L;
    
	private String name;
	private String url;
	private Date lastBuild;
	
	private RssCategory category;
	private SortedSet<RssItem> items = new TreeSet<RssItem>();

	public RssFeed() {
		// Mantenha para o Hibernate
	}
	
	public RssFeed(RssCategory category, String name, String url) {
		this.category = category;
		this.name = name;
		this.url = url;
	}
	
	/**
	 * Adiciona um novo item à lista de items deste feed.
	 * 
	 * @param item Novo item.
	 * @return {@code true} se o item já não estava na lista.
	 */
	public boolean addItem(RssItem item) {
		boolean result = false;
		
		item.setRssFeed(this);
		if (!containsItem(item)) {
			result = items.add(item);
		}
		return result;
	}
	
	/**
	 * Verifica se um item já está presente na lista de itens deste feed.
	 * 
	 * @param item Item procurado.
	 * @return {@code true} caso o item já exista.
	 */
	public boolean containsItem(RssItem item) {
		/*
		 * NOTE: não o utiliza o contains do TreeSet pois ele se baseia no 
		 * "compareTo" e, por algum motivo, a data retornada pelo Rome é 
		 * diferente da data persistida pelo Hibernate, afetando o resultado da
		 * comparação pela data.
		 */
		boolean contains = false;
		for (RssItem curr : items) {
			if (curr.equals(item)) {
				contains = true;
				break;
			}
		}
		return contains;
	}
	
	/**
	 * Faz a verificação baseada na classe do objeto - deve ser um
	 * {@link RssFeed} - e na propriedade {@code url} do feed.
	 * 
	 * @return Boolean indicando o resultado da verificação.
	 */
	@Override
    public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof RssFeed) {
			RssFeed other = (RssFeed) obj;
			return Objects.equals(url, other.url);
		}
		return false;
    }
	
	/**
	 * Faz a geração do {@code hashCode} baseando-se na propriedade {@code url}.
	 * 
	 * @return Código hash code gerado.
	 */
	@Override
    public int hashCode() {
		return Objects.hash(url);
    }
	
	/**
	 * Faz a comparação entre objetos {@link RssFeed} para ordenação baseando-se
	 * no nome dos mesmos.
	 * 
	 * @param other Outro feed com qual será comparado.
	 * @return {@code 1} se for maior, {@code -1} se for menor e {@code 0} se
	 *         for igual.
	 */
	@Override
	public int compareTo(RssFeed other) {
		if (name == other.name) return 0;
		if (name == null)       return -1;
		if (other.name == null) return 1;
		
		return name.compareTo(other.name);
	}
	
	/**
	 * Gera a string com base no nome e url do feed.
	 */
	@Override
	public String toString() {
	    return new ToStringBuilder(this)
	    		.append("name", name)
	    		.append("url", url)
	    		.toString();
	}
	
	//----------------------------------------
	//	Getters & Setters
	//----------------------------------------
	
	/**
	 * @return O nome do feed RSS.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name O nome do feed RSS.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return A URL do feed RSS.
	 */
	public String getUrl() {
		return url;
	}
	
	/**
	 * @param url A URL do feed RSS.
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
    /**
     * @return Data da última geração da lista de feeds.
     */
    public Date getLastBuild() {
	    return lastBuild;
    }
    
    /**
     * @param lastBuild Data da última geração da lista de feeds.
     */
    public void setLastBuild(Date lastBuild) {
	    this.lastBuild = lastBuild;
    }
    
    /**
     * @return the category
     */
    public RssCategory getCategory() {
	    return category;
    }
    
    /**
     * @param category the category to set
     */
    public void setCategory(RssCategory category) {
	    this.category = category;
    }
    
    /**
     * @return Os items já obtidos deste feed.
     */
    public SortedSet<RssItem> getItems() {
	    return items;
    }
    
    /**
     * @param items Os items já obtidos deste feed.
     */
    public void setItems(SortedSet<RssItem> items) {
	    this.items = items;
    }
}
