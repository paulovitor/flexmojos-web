package tv.snews.anews.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.annotations.*;
import org.springframework.flex.core.io.AmfIgnore;
import tv.snews.anews.infra.search.DocumentIndexingInterceptor;
import tv.snews.anews.infra.search.VehiclesLuceneBridge;
import tv.snews.anews.util.ResourceBundle;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.*;

/**
 * Domínio que representa a pauta.
 * 
 * @author Samuel Guedes de Melo
 * @author Eliezer Reis
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/guideline", interceptor = DocumentIndexingInterceptor.class)
public class Guideline extends Document<GuidelineRevision> {

	private static final long serialVersionUID = -8982819258141694933L;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@Field
	@Boost(1.0f)
	private String proposal;

	@Field
	@Boost(1.0f)
	private String referral;

	@Field
	@Boost(1.0f)
	private String informations;

	@IndexedEmbedded
	private SortedSet<User> producers = new TreeSet<>();

	@IndexedEmbedded
	private GuidelineClassification classification;

    @IndexedEmbedded
    private Team team;

	@IndexedEmbedded
	private SortedSet<User> reporters = new TreeSet<>();
	
	private Set<User> editors = new HashSet<>();

	@IndexedEmbedded
	private SortedSet<Guide> guides = new TreeSet<>();

	private News news;

	@IndexedEmbedded
	private Checklist checklist;

	@Field
	@FieldBridge(impl =  VehiclesLuceneBridge.class)
	private Set<CommunicationVehicle> vehicles = new HashSet<>();

	private Set<Reportage> reportages = new HashSet<>();

	private SortedSet<GuidelineCopyLog> copiesHistory = new TreeSet<>();

	@Transient
	private Date guideDate;

	@Transient
	private User editingUser;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public Guideline() {
		this.excluded = false;
		this.changeDate = new Date();
		this.state = DocumentState.PRODUCING;
	}

	//----------------------------------
	//  Operations
	//----------------------------------

	public Guideline createCopy(User author) {
		Guideline copy = new Guideline();
		copy.copyData(this);

		copy.setProposal(proposal);
		copy.setReferral(referral);
		copy.setInformations(informations);
		copy.setState(state);
		copy.setStatusReason(statusReason);
		copy.setNews(news);
        copy.setTeam(team);

		copy.getVehicles().addAll(vehicles);
		copy.getProducers().addAll(producers);
		copy.getReporters().addAll(reporters);
		copy.getEditors().addAll(editors);

        copy.setChecklist(checklist != null ? checklist.createCopy(author, copy) : null);

		for (Guide guide : guides) {
			copy.getGuides().add(guide.createCopy(copy));
		}

		for (GuidelineRevision revision : revisions) {
			copy.getRevisions().add(revision.createCopy(copy));
		}

		return copy;
	}

	public boolean containsContact(Contact contact) {
		for (Guide guide : guides) {
			for (Contact current : guide.getContacts()) {
				if (current.equals(contact)) {
					return true;
				}
			}
		}
		return false;
	}

	public String pendencies() {
		if (checklist == null || checklist.getTasks().isEmpty()) {
			return "";
		} else {
			int done = 0, total = checklist.getTasks().size();

			for (ChecklistTask task : checklist.getTasks()) {
				if (task.isDone()) {
					done++;
				}
			}
			return done + "/" + total;
		}
	}

	public void updateFields(Guideline newData) {
		this.copyData(newData);

		setProposal(newData.getProposal());
		setReferral(newData.getReferral());
		setInformations(newData.getInformations());
		setState(newData.getState());
		setProducers(newData.getProducers());
		setClassification(newData.getClassification());
        setTeam(newData.getTeam());
		setReporters(newData.getReporters());
		setStatusReason(newData.getStatusReason());
		setGuides(newData.getGuides());
		setNews(newData.getNews());
		setEditors(newData.getEditors());
		if(newData.getChecklist()!=null)
        	newData.getChecklist().setGuideline(this);

		setChecklist(newData.getChecklist());

		setVehicles(newData.getVehicles());
		//setReportages(newData.getReportages());
		//setCopiesHistory(newData.getCopiesHistory());

		//Esses atributos estão sendo configurado pelos metodos updateAuthorRelashionship();
		//setAuthor(newData.getAuthor());
		//setVersion(newData.getVersion());
		//setRevisions(newData.getRevisions());
	}

	public Guide firstGuide() {
		return guides.isEmpty() ? null : guides.iterator().next();
	}

	public Date firstGuideSchedule() {
		Guide first = firstGuide();
		return first != null ? first.getSchedule() : null;
	}

	public void addProducer(User producer) {
		producers.add(producer);
	}

	public void addReporter(User reporter) {
		reporters.add(reporter);
	}

	public void addGuide(Guide guide) {
		guide.setGuideline(this);
		guide.setOrderGuide(guides.size());
		guides.add(guide);
	}

	public String vehiclesToString() {
		SortedSet<String> labels = new TreeSet<>();
		for (CommunicationVehicle vehicle : vehicles) {
			labels.add(bundle.getMessage("vehicle." + vehicle.name().toLowerCase()));
		}
		String aux = StringUtils.join(labels, ", ") + (labels.isEmpty() ? "" : ".");

		// Troca a última vírgula por 'e'
		int index = aux.lastIndexOf(',');
		if (index != -1) {
			String and = bundle.getMessage("defaults.and");
			return aux.substring(0, index) + " " + and + aux.substring(index + 1, aux.length());
		} else {
			return aux;
		}
	}

	public String producersToString() {
		return joinNicknames(producers);
	}

	public String reportersToString() {
		return joinNicknames(reporters);
	}
	
	public String editorsToString() {
		return joinNicknames(editors);
	}


	private String joinNicknames(Collection<User> users) {
		SortedSet<String> nicknames = new TreeSet<>();
		for (User user : users) {
			nicknames.add(user.getNickname());
		}

		String aux = StringUtils.join(nicknames, ", ") + (nicknames.isEmpty() ? "" : ".");

		// Troca a última vírgula por 'e'
		int index = aux.lastIndexOf(',');
		if (index != -1) {
			String and = bundle.getMessage("defaults.and");
			return aux.substring(0, index) + " " + and + aux.substring(index + 1, aux.length());
		} else {
			return aux;
		}
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public String getProposal() {
		return proposal;
	}

	public void setProposal(String proposal) {
		this.proposal = proposal;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getInformations() {
		return informations;
	}

	public void setInformations(String informations) {
		this.informations = informations;
	}

	public SortedSet<User> getProducers() {
		return producers;
	}

	public void setProducers(SortedSet<User> producers) {
		this.producers = producers;
	}

	public GuidelineClassification getClassification() {
		return classification;
	}

	public void setClassification(GuidelineClassification classification) {
		this.classification = classification;
	}

	public SortedSet<User> getReporters() {
		return reporters;
	}

	public void setReporters(SortedSet<User> reporters) {
		this.reporters = reporters;
	}

	public SortedSet<Guide> getGuides() {
		return guides;
	}

	public void setGuides(SortedSet<Guide> guides) {
        this.guides = guides;
    }

	@AmfIgnore
	public Date getGuideDate() {
		return guideDate;
	}

	public void setGuideDate(Date guideDate) {
		this.guideDate = guideDate;
	}

	public User getEditingUser() {
		return editingUser;
	}

	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Checklist getChecklist() {
		return checklist;
	}

	public void setChecklist(Checklist checklist) {
		this.checklist = checklist;
	}

	public Set<CommunicationVehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(Set<CommunicationVehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public Set<Reportage> getReportages() {
		return reportages;
	}

	public void setReportages(Set<Reportage> reportages) {
		this.reportages = reportages;
	}

	public SortedSet<GuidelineCopyLog> getCopiesHistory() {
		return copiesHistory;
	}

	public void setCopiesHistory(SortedSet<GuidelineCopyLog> copiesHistory) {
		this.copiesHistory = copiesHistory;
	}

    public Set<User> getEditors() {
    	return editors;
    }

    public void setEditors(Set<User> editors) {
    	this.editors = editors;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
