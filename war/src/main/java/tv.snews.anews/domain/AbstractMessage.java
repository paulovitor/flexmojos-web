package tv.snews.anews.domain;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Define a estrutura básica de todas as mensagens do sistema de comunicação
 * instantânea, envio de arquivos e mensagens offline.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class AbstractMessage extends AbstractEntity<Long> implements Comparable<AbstractMessage> {

	private static final long serialVersionUID = -7519782496430832796L;

	// Identificadores dos tipos de mensagem
	public static final String FILE_MESSAGE = "file";
	public static final String TEXT_MESSAGE = "text";
	
	protected Date date;
	protected User sender;
	protected Chat chat;
	protected Set<MessageDestination> destinations = new HashSet<>();

	protected boolean sent; // transient

	public AbstractMessage() {
		// Hibernate
	}

	public AbstractMessage(Chat chat, User sender, Collection<User> recipients) {
		this(chat, sender, recipients.toArray(new User[recipients.size()]));
	}

	public AbstractMessage(Chat chat, User sender, User... recipients) {
		this.chat = chat;
		this.sender = sender;
		this.date = new Date();
		this.sent = false;
		
		for (User recipient : recipients) {
			addRecipient(recipient);
		}
		
		this.chat.addMessage(this);
	}

	/**
	 * Adiciona um usuário a lista de destinatários da mensagem, criando
	 * internamente um objeto {@link MessageDestination} e adicionando o mesmo a
	 * lista de destinations.
	 * 
	 * @param recipient Novo destinatário da mensagem.
	 */
	public final MessageDestination addRecipient(User recipient) {
		MessageDestination destination = getDestinationOf(recipient);
		if (destination == null) {
			destination = new MessageDestination(/*this, */recipient);
			destinations.add(destination);
		}
		return destination;
	}

	/**
	 * Retorna um objeto {@link MessageDestination} que possa como destinatário
	 * o usuário informado, ou retorna <code>null</code> caso nenhuma das
	 * destinations tenha o usuário como destinatário.
	 * 
	 * @param recipient
	 * @return
	 */
	public final MessageDestination getDestinationOf(User recipient) {
		MessageDestination userDestination = null;
		for (MessageDestination destination : destinations) {
			if (destination.getReceiver().equals(recipient)) {
				userDestination = destination;
				break;
			}
		}
		return userDestination;
	}

	public final void markMessageAsReadedTo(User recipient) {
		MessageDestination destination = getDestinationOf(recipient);
		if (destination != null) {
			destination.setReaded(true);
		}
	}

	/**
	 * A comparação entre a mensagens são baseadas na data na qual cada uma foi
	 * enviada.
	 */
	@Override
	public int compareTo(AbstractMessage other) {
		return this.date.compareTo(other.getDate());
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}
	
	public Chat getChat() {
		return chat;
	}
	
	public void setChat(Chat chat) {
		this.chat = chat;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public Set<MessageDestination> getDestinations() {
		return destinations;
	}

	public void setDestinations(Set<MessageDestination> destinations) {
		this.destinations = destinations;
	}
}
