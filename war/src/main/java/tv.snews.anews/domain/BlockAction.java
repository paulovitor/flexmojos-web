package tv.snews.anews.domain;


/**
 * Enum para ações do bloco de um espelho.
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public enum BlockAction {
	INSERTED, EXCLUDED, BREAK_CHANGED
}
