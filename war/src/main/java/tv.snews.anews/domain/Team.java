package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.HashSet;
import java.util.Set;

/**
 * Entidade que representa as equipes da pauta.
 * 
 * @author Maxuel
 * @since 1.7
 */
@Entity
@Indexed(index = "indexes/team")
public class Team extends AbstractEntity<Integer> {


    private static final long serialVersionUID = 5244391531312001212L;

    @Field
    private String name;
    private Set<User> users = new HashSet<User>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}