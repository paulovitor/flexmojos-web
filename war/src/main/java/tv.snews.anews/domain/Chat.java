package tv.snews.anews.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Representa uma conversa, ou seja, um grupo de mensagens que envolvem os
 * mesmos membros.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class Chat extends AbstractEntity<Long> {

	private static final long serialVersionUID = 5835983613791418463L;

	private Set<User> members = new HashSet<User>();
	private Set<User> leftMembers = new HashSet<User>();
	private Set<AbstractMessage> messages = new LinkedHashSet<AbstractMessage>();
	private Date lastInteraction;
	
	public Chat() {
		lastInteraction = new Date();
	}
	
	/**
	 * Retorna a lista de usuários membros do chat que não sairam da conversa.
	 * 
	 * @return Lista de usuários ativos.
	 */
	public Set<User> activeMembers() {
		Set<User> activeMembers = new HashSet<User>();
		for (User member : members) {
			if (!userLeftChat(member)) {
				activeMembers.add(member);
			}
		}
		return activeMembers;
	}
	
	/**
	 * Verifica se todos os usuários de um {@link java.util.Set} estão dentro da lista de
	 * participantes do chat, e se são exatamente os mesmos (quantidade).
	 * 
	 * @param users
	 * @return Resultado da operação
	 */
	public boolean containsAllMembers(Set<User> users) {
		boolean containsAll = true;
		if (members.size() == users.size()) {
			for (User member : users) {
				if (!containsMember(member)) {
					containsAll = false;
					break;
				}
			}
		} else {
			containsAll = false;
		}
		return containsAll;
	}
	
	/**
	 * Verifica se o usuário informado está dentro da lista de participantes da
	 * conversa.
	 * 
	 * @param user Usuário desejado.
	 * @return <code>boolean</code> indicando a existencia do usuário.
	 */
	public boolean containsMember(User user) {
		return members.contains(user);
	}
	
	/**
	 * Verifica se o usuário informado está dentro da lista de usuários que
	 * sairam da conversa.
	 * 
	 * @param user Usuário que será verificado.
	 * @return <code>boolean</code> indicando se o usuário abandonou o chat.
	 */
	public boolean userLeftChat(User user) {
		return leftMembers.contains(user);
	}
	
	/**
	 * Adiciona um ou mais usuários à lista de participantes do chat. Será
	 * retornada uma lista de usuários que foram adicionados na conversa por
	 * esse método, já que aqueles que já participavam não serão adicionados
	 * novamente.
	 * 
	 * @param newMembers Novos usuários.
	 * @return Lista de usuários que entraram na conversa.
	 */
	public Set<User> addMembers(User... newMembers) {
		Set<User> addedMembers = new HashSet<User>();
		for (User newMember : newMembers) {
			if (containsMember(newMember)) {
				if (userLeftChat(newMember)) {
					reJoinMember(newMember);
					addedMembers.add(newMember);
				}
			} else {
				members.add(newMember);
				addedMembers.add(newMember);
			}
		}
		return addedMembers;
	}
	
	/**
	 * Registra a saída de um usuário da conversa. Ele ainda estara na lista de
	 * membros do chat, mas novas mensagens não deverão adicionar ele como
	 * destinatário.
	 * <p>
	 * Esse método irá retornar um boleano indicando o resultado dessa operação;
	 * isso para evitar falhas de lógica onde o usuário já teve sua saída
	 * registrada ou até no caso em que ele não consta como um membro do chat.
	 * </p>
	 * 
	 * @param member Membro que saiu da conversa do chat.
	 * @return Indica se alguma alteração foi realmente feita.
	 */
	public boolean registerLeave(User member) {
		boolean anyChange = false;
		if (containsMember(member) && !userLeftChat(member)) {
			leftMembers.add(member);
			anyChange = true;
		}
		
		return anyChange;
	}
	
	/**
	 * Remove o usuário especificado da lista dos que abandonaram a conversa, ou
	 * seja, faz com que o usuário volte a conversa.
	 * 
	 * @param user Usuário que irá retornar à conversa.
	 */
	public void reJoinMember(User user) {
		leftMembers.remove(user);
	}
	
	public void reJoinAllMembers() {
		leftMembers.clear();
	}
	
	public void addMessage(AbstractMessage message) {
		messages.add(message);
		lastInteraction = new Date();
	}
	
	//----------------------------------
	//	Getters & Setters
	//----------------------------------
	
	public Set<User> getMembers() {
		return members;
	}
	
	public void setMembers(Set<User> members) {
		this.members = members;
	}
	
	public Set<User> getLeftMembers() {
		return leftMembers;
	}
	
	public void setLeftMembers(Set<User> leftUsers) {
		this.leftMembers = leftUsers;
	}
	
	public Set<AbstractMessage> getMessages() {
		return messages;
	}
	
	public void setMessages(Set<AbstractMessage> messages) {
		this.messages = messages;
	}
	
	public Date getLastInteraction() {
		return lastInteraction;
	}
	
	public void setLastInteraction(Date lastInteraction) {
		this.lastInteraction = lastInteraction;
	}
}
