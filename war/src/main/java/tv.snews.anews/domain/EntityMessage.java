package tv.snews.anews.domain;

import java.util.Collection;

/**
 * Classe que representa as mensagens enviadas dentro do sistema, tanto as
 * mensagens da comunicação instantânea quanto as mensagens offline.
 *
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class EntityMessage extends AbstractMessage {

	private Integer idEntity;
	private EntityType type;
	private String description;

	public EntityMessage() {
		// Hibernate
	}

	public EntityMessage(Chat chat, User sender, Collection<User> recipients) {
		super(chat, sender, recipients);
	}

	public EntityMessage(Chat chat, User sender, User... recipients) {
		super(chat, sender, recipients);
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	public Integer getIdEntity() {
		return idEntity;
	}

	public void setIdEntity(Integer idEntity) {
		this.idEntity = idEntity;
	}

	public EntityType getType() {
		return type;
	}

	public void setType(EntityType type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private static final long serialVersionUID = 659970895067742438L;
}
