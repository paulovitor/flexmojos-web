package tv.snews.anews.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Indica os tipos de entidade existentes no sistema.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public enum EntityType {

	/*
	 * IMPORTANTE: no banco esse enum está sendo salvo como um int, que é o
	 * padrão do Hibernate. Isso não tem problema mas tem que ter cuidado ao
	 * adicionar ou remover valores nesse enum.
	 * Acredito que se você adicionar um valor na primeira posição, isso vai
	 * alterar os valores dos outros que já existem, o que vai deixar de
	 * corresponder com o valor do banco.
	 */

    /**
     * Entidade que representa um tweet carregado da API do Twitter dentro
     * do sistema.
     */
    TWEETINFO(0, "TweetInfo"),

    /**
     * Entidade que representa as informações de uma notícia obtida a partir
     * de um feed RSS.
     */
    RSSITEM(1, "RssItem"),

    /**
     * Entidade que representa a notícia apurada.
     */
    NEWS(2, "News"),

    /**
     * Entidade que representa a ronda registrada.
     */
    ROUND(3, "Round"),

    /**
     * Entidade que representa o contado da agenda.
     */
    AGENDACONTACT(4, "AgendaContact"),

    /**
     * Entidade que representa a pauta.
     */
    GUIDELINE(5, "Guideline"),

    /**
     * Entidade que representa a reportagem.
     */
    REPORTAGE(6, "Reportage"),

    /**
     * Entidade que representa a lauda.
     */
    STORY(7, "Stroy"),
    
    /**
     * Entidade que representa um relatório.
     */
    REPORT(8, "Report"),

    /**
     * Entidade que representa um relatório.
     */
    DOCUMENT(9, "Document"),
    
    /**
     * Entidade que representa um relatório.
     */
    SCRIPT(10, "Script");
    

    private final int id;
	private final String name;

	EntityType(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public static List<String> getEntityTypes() {
		ArrayList<String> entityTypes = new ArrayList<String>();
		for ( int i = 0; i < EntityType.values().length; i++ ){
			entityTypes.add(EntityType.values()[i].toString());
		}
		return entityTypes;
	}

    public int getId() {
    	return id;
    }

    public String getName() {
    	return name;
    }

}
