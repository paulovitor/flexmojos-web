package tv.snews.anews.domain;

public abstract class ReportageSubSection extends AbstractEntity<Long> implements Comparable<ReportageSubSection> {

    private static final long serialVersionUID = -3345912548019733916L;

    private int order;
    private ReportageSection reportageSection;

    @Override
    public int compareTo(ReportageSubSection other) {
        return this.order - other.order;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public ReportageSection getReportageSection() {
        return reportageSection;
    }

    public void setReportageSection(ReportageSection reportageSection) {
        this.reportageSection = reportageSection;
    }

}
