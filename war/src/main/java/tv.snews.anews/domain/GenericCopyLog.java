package tv.snews.anews.domain;

import java.util.Date;

/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 */
public class GenericCopyLog extends AbstractEntity<Integer> {

    private String nickname;
    private String company;
    private Date date;

    //----------------------------------
    //  Constructors
    //----------------------------------

    public GenericCopyLog() {
    }

    public GenericCopyLog(String nickname, String company) {
        this.nickname = nickname;
        this.company = company;
        this.date = new Date();
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private static final long serialVersionUID = -2518241456898995925L;
}
