package tv.snews.anews.domain;

public enum WSMediaStatus {

    UNDEFINED(0, "Indefinido"),
    REGISTER_OK(1001, "Cadastro OK"),
    EDITION_PREPARATION(1002, "Edição em elaboração"),
    EDITION_OK(1003, "Edição OK"),
    LOW_RESOLUTION_PREPARATION(1004, "Baixa resolução elaboração"),
    LOW_RESOLUTION_OK(1005, "Baixa Resolução OK"),
    LOW_RESOLUTION_FAILED(1006, "Baixa Resolução falhou"),
    DECOUPAGE_STARTED(1007, "Decupagem iniciada"),
    DECOUPAGE_OK(1008, "Decupagem OK"),
    NEW_VERSION_REQUESTED(1009, "Nova versão requisitada"),
    NEW_VERSION_PREPARATION(1010, "Nova versão em elaboração"),
    NEW_VERSION_OK(1011, "Nova versão OK"),
    APPROVED(1012, "Aprovado"),
    CANCELED(1013, "Cancelado"),
    REVISION_PREPARATION(1014, "Revisão em elaboração"),
    ARCHIVED_VIDEOS(1015, "Vídeos arquivados");

    private final int id;
    private final String name;

    WSMediaStatus(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static WSMediaStatus getByCode(int code) {
        for (int i = 0; i < WSMediaStatus.values().length; i++) {
            if (code == WSMediaStatus.values()[i].id)
                return WSMediaStatus.values()[i];
        }
        return null;
    }
}
