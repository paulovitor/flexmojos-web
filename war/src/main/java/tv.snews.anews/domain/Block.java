package tv.snews.anews.domain;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * Domínio que representa o espelho.
 *
 * @author Paulo Felipe
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/block")
public class Block extends AbstractEntity<Long> implements Comparable<Block> {

	private static final long serialVersionUID = 2830276819407496283L;

	private int order;
	private Date commercial;
	private boolean standBy;

	@IndexedEmbedded
	private Rundown rundown;

	@ContainedIn
	private List<Story> stories = new ArrayList<>();

	public Block() {
	}

	public Block(Rundown rundown) {
		this.rundown = rundown;
	}

	public Block(Rundown rundown, BlockTemplate template, User author) {
		this(rundown);

		this.order = template.getOrder();
		this.standBy = template.isStandBy();
		this.commercial = template.getCommercial();

		for (StoryTemplate storyTemplate : template.getStories()) {
			stories.add(new Story(this, storyTemplate, author));
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof Block) {
			Block other = (Block) obj;
			return Objects.equals(rundown, other.rundown)
					&& Objects.equals(order, other.order);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(rundown, order);
	}

	/**
	 * Utilizado pelo método {@link java.util.Collections#sort(java.util.List)}, em outras classes,
	 * para ordenar uma coleção de objetos {@link Block}.
	 */
	@Override
	public int compareTo(Block other) {
		if (standBy && !other.standBy) {
			return 1;
		}
		if (!standBy && other.standBy) {
			return -1;
		}
		return this.order - other.order;
	}

	public <T extends StorySubSection> int countStoriesSubSectionsOfType(Class<T> type) {
		return countStoriesSubSectionsOfType(type, false);
	}

	public <T extends StorySubSection> int countStoriesSubSectionsOfType(Class<T> type, boolean onlyApprovedContent) {
		int count = 0;

		for (Story story : stories) {
			if (!onlyApprovedContent || story.isApproved()) {
				count += story.countSubSectionsOfType(type);
			}
		}

		return count;
	}

	public <T extends StorySubSection> List<T> storiesSubSectionsOfType(Class<T> type) {
		return storiesSubSectionsOfType(type, false);
	}

	public <T extends StorySubSection> List<T> storiesSubSectionsOfType(Class<T> type, boolean onlyApprovedContent) {
		List<T> results = new ArrayList<>();

		for (Story story : stories) {
			if (!onlyApprovedContent || story.isApproved()) {
				results.addAll(story.subSectionsOfType(type));
			}
		}

		return results;
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public Date getCommercial() {
		return commercial;
	}

	public void setCommercial(Date commercial) {
		this.commercial = commercial == null ? getMidnightTime() : addTimeToCurrentDate(commercial);
	}

	public boolean isStandBy() {
		return standBy;
	}

	public void setStandBy(boolean standBy) {
		this.standBy = standBy;
	}

	public Rundown getRundown() {
		return rundown;
	}

	public void setRundown(Rundown rundown) {
		this.rundown = rundown;
	}

	public List<Story> getStories() {
		return stories;
	}

	public void setStories(List<Story> stories) {
		this.stories = stories;
	}

}
