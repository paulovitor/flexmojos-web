package tv.snews.anews.domain;

/**
 * Enum para ações da lauda no espelho.
 *
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public enum StoryAction {

	/*
	 * NOTE A action UPDATED só é lançada quando não tem a informação de qual
	 * campo foi atualizado.
	 */
	INSERTED, UPDATED, EXCLUDED, COPIED, ARCHIVED,

	BLOCK_CHANGED, ORDER_CHANGED,

	VT_CHANGED, EDITOR_CHANGED, IMAGE_EDITOR_CHANGED, REPORTER_CHANGED, PAGE_CHANGED, SLUG_CHANGED,

	APPROVED, DISAPPROVED, OK, NOT_OK, EDITING_USER_CHANGED,

    DISPLAYING, DISPLAYED, MULTIPLE_EXCLUSION
}


