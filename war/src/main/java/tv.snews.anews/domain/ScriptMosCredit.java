package tv.snews.anews.domain;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.7
 */
public class ScriptMosCredit extends AbstractScriptItem {

	private static final long serialVersionUID = 8050410313127861670L;

	private String mosStatus;
	private MosMedia mosMedia;
	private String slug;
	private String channel;

	public ScriptMosCredit() {
		super();
	}

	public ScriptMosCredit(ScriptMosCredit original) {
		this.mosStatus = original.getMosStatus();
		this.mosMedia = original.getMosMedia();
		this.slug = original.getSlug();
		this.channel = original.getChannel();
	}

	public boolean isFilled() {
        if (isNotBlank(slug)) {
            return true;
        }
		return false;
	}

	public String getMosStatus() {
		return mosStatus;
	}

	public void setMosStatus(String mosStatus) {
		this.mosStatus = mosStatus;
	}

	public MosMedia getMosMedia() {
		return mosMedia;
	}

	public void setMosMedia(MosMedia mosMedia) {
		this.mosMedia = mosMedia;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
}
