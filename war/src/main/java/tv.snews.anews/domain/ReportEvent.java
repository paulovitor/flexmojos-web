package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Objects;

import static java.lang.String.CASE_INSENSITIVE_ORDER;

/**
 * Representa um tipo de evento dos relatórios.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
@Entity
@Indexed(index= "indexes/reportevent")
public class ReportEvent extends AbstractEntity<Integer> implements Comparable<ReportEvent> {

	@Field
	private String name;
	
	//------------------------------------
	//  Operations
	//------------------------------------

	public int compareTo(ReportEvent other) {
		return CASE_INSENSITIVE_ORDER.compare(name, other.name);
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	    	return true;
	    }
	    if (obj instanceof ReportEvent) {
	    	ReportEvent other = (ReportEvent) obj;
	    	return Objects.equals(name, other.name);
	    }
	    return false;
	}
	
	@Override
	public int hashCode() {
	    return Objects.hash(name);
	}
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public String getName() {
	    return name;
    }
    
    public void setName(String name) {
	    this.name = name;
    }
	
    private static final long serialVersionUID = 38217284525194642L;
}
