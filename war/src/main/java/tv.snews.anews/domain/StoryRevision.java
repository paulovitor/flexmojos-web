package tv.snews.anews.domain;

import tv.snews.anews.util.ResourceBundle;

import java.util.Collection;
import java.util.Iterator;

import static org.apache.commons.lang.StringUtils.stripToEmpty;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class StoryRevision extends AbstractRevision<Story> {

    private static final long serialVersionUID = -4616774087380410027L;
	private static final ResourceBundle bundle = ResourceBundle.INSTANCE;
	private String slug;
	private String storyKind;
	private String information;
	private String headSection;
	private String ncSection;
	private String vtSection;
	private String footerSection;
	private String offSection;

	//----------------------------------
    //	Constructors
    //----------------------------------
	
	// Hibernate & AMF
	public StoryRevision() {
	}
	
    public StoryRevision(Story story) {
	    super(story);
    }
    
    //----------------------------------
    //	Operations
    //----------------------------------
    
    public StoryRevision clone(Story cloneParent) {
		StoryRevision clone = new StoryRevision();
		
		clone.entity = cloneParent;
		clone.revisionNumber = this.revisionNumber;
		clone.revisionDate = this.revisionDate;
		clone.revisor = this.revisor;
		
		clone.slug = this.slug;
		clone.storyKind = this.storyKind;
		clone.information = this.information;
		clone.headSection = this.headSection;
		clone.ncSection = this.ncSection;
		clone.vtSection = this.vtSection;
		clone.footerSection = this.footerSection;
		clone.offSection = this.offSection;

		return clone;
	}
    
    @Override
    protected void fillRevision(Story story) {
	    this.slug = stripToEmpty(story.getSlug());
	    this.storyKind = stripToEmpty(story.getStoryKind() == null ? "" : story.getStoryKind().getAcronym());
	    this.information = stripToEmpty(story.getInformation());
	    this.headSection = stripToEmpty(joinSection(story.getHeadSection()));
	    this.ncSection = stripToEmpty(joinSection(story.getNcSection()));
	    this.vtSection = stripToEmpty(joinSection(story.getVtSection()));
	    this.footerSection = stripToEmpty(joinSection(story.getFooterSection()));
	    this.offSection = stripToEmpty(joinSection(story.getOffSection()));
    }
	
    //----------------------------------
    //	Helpers
    //----------------------------------
    
    private String joinSection(StorySection section) {
    	if (section == null) {
    		return "";
    	} else if (section instanceof StoryVTSection) {
    		// Tem que adicionar a "deixa"
    		StoryVTSection vtSection = (StoryVTSection) section;
            if (vtSection.getCue() != null) {
                String cue = bundle.getMessage("story.cue") + ": " + vtSection.getCue();
                return joinSubSections(section.getSubSections()) + "\n" + cue;
            }
    	}
        return joinSubSections(section.getSubSections());
    }
    
    private String joinSubSections(Collection<StorySubSection> subSections) {
    	StringBuilder builder = new StringBuilder();
    	
    	for (StorySubSection subSection : subSections) {
    		if (subSection instanceof StoryCameraText) {
    			StoryCameraText camText = (StoryCameraText) subSection;
    			builder.append("\n");
    			builder.append(joinPresenterAndText(camText.getPresenter(), camText.getCamera(), camText.getText()));
        		builder.append("\n");
        		builder.append(joinSubSections(camText.getSubSections())); // pode ter CGs
        		builder.append("\n");
//    			continue; // se StoryCameraText herdar de StoryText, precisa desse continue
    		}
    		if (subSection instanceof StoryText) {
    			builder.append("\n");
    			StoryText text = (StoryText) subSection;
    			builder.append(joinPresenterAndText(text.getPresenter(), text.getText()));
        		builder.append("\n");
    		}
    		if (subSection instanceof StoryMosVideo) {
    			builder.append("\n");
    			StoryMosVideo mosObj = (StoryMosVideo) subSection;
    			builder.append(bundle.getMessage("story.vt")).append(": ");
    			builder.append(mosObj.getSlug());
    			builder.append("\n");
    		}
    		if (subSection instanceof StoryCG) {
    			builder.append("\n");
    			
    			StoryCG cg = (StoryCG) subSection;
    				
				Iterator<StoryCGField> fieldsIt = cg.getFields().iterator();
				while (fieldsIt.hasNext()) {
					StoryCGField field = fieldsIt.next();
            			
					builder.append(field.getName()).append(": ").append(stripToEmpty(field.getValue()));
					if (fieldsIt.hasNext()) {
            				builder.append(" / ");
            			}
            		}
    			}
    		}
    	
    	return builder.toString().trim();
    }

    private String joinPresenterAndText(User presenter, String text) {
    	if (presenter == null || text == null) {
    		return "";
    	}
    	return stripToEmpty(presenter.getNickname() + ":\n" + text);
    }
    
    private String joinPresenterAndText(User presenter, int camera, String text) {
    	if (presenter == null || text == null) {
    		return "";
    	}
    	return stripToEmpty(presenter.getNickname() + " [" + camera + "]:\n" + text);
    }
    
    //----------------------------------
    //	Getters & Setters
    //----------------------------------
    
    public String getSlug() {
	    return slug;
    }
    
    public void setSlug(String slug) {
	    this.slug = slug == null ? "" : slug;
    }
    
    public String getStoryKind() {
	    return storyKind;
    }
    
    public void setStoryKind(String storyKind) {
	    this.storyKind = storyKind == null ? "" : storyKind;
    }
    
    public String getInformation() {
	    return information;
    }
    
    public void setInformation(String information) {
	    this.information = information == null ? "" : information;
    }
    
    public String getHeadSection() {
	    return headSection;
    }
    
    public void setHeadSection(String headSection) {
	    this.headSection = headSection;
    }
    
    public String getVtSection() {
	    return vtSection;
    }
    
    public void setVtSection(String vtSection) {
	    this.vtSection = vtSection;
    }
    
    public String getNcSection() {
	    return ncSection;
    }
    
    public void setNcSection(String ncSection) {
	    this.ncSection = ncSection == null ? "" : ncSection;
    }
    
    public String getFooterSection() {
	    return footerSection;
    }
    
    public void setFooterSection(String footerSection) {
	    this.footerSection = footerSection;
    }
    
    public String getOffSection() {
	    return offSection;
    }
    
    public void setOffSection(String offSection) {
	    this.offSection = offSection == null ? "" : offSection;
    }
    
}
