package tv.snews.anews.domain;

/**
 * Representa o tipo de retorno que a função que verifica as pautas pode
 * retornar
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */

public enum RundownStatus {
	EXIST, NOT_EXIST, CANT_CREATE;
}