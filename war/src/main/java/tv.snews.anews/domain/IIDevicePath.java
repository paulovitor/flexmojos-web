package tv.snews.anews.domain;

import java.util.Objects;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class IIDevicePath extends AbstractEntity<Integer> implements Comparable<IIDevicePath> {

	private String path;
	private IIDevice device;

	public IIDevicePath() {}

	public IIDevicePath(String path, IIDevice device) {
		this.path = path;
		this.device = device;
	}

	@Override
	public int compareTo(IIDevicePath other) {
		if (path == other.path) return 0;
		if (path == null) return -1;
		if (other.path == null) return 1;

		return String.CASE_INSENSITIVE_ORDER.compare(path, other.path);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IIDevicePath) {
			IIDevicePath other = (IIDevicePath) obj;
			return Objects.equals(path, other.path);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(path);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public IIDevice getDevice() {
		return device;
	}

	public void setDevice(IIDevice device) {
		this.device = device;
	}

	private static final long serialVersionUID = 2971272356734275679L;
}
