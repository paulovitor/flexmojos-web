package tv.snews.anews.domain;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class WSDevice extends Device {

	private String address;
	private String username;
	private String password;

	public WSDevice() {
		this.protocol = DeviceProtocol.WS;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private static final long serialVersionUID = -595028498497003538L;
}
