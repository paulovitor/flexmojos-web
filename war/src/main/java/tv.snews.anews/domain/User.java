package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.flex.core.io.AmfIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import tv.snews.anews.util.DateTimeUtil;

import javax.persistence.Entity;
import java.util.*;

/**
 * Classe que representa os usuários do sistema.
 * 
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/user")
public class User extends AbstractEntity<Integer> implements Comparable<User>, UserDetails {

	public static final String SUPPORT_EMAIL = "suporte@snews.tv";

	@Field
	private String name;
	
	@Field
	private String nickname;
	
	private String password;
	private String email;
	private Date birthdate;
	private String phoneNumber;
	private boolean deleted;
	private Date agreementDate;
	private Date passwordExpiration;
	private String cellNumber;
	private String nextelNumber;
	private double readTime;
    private boolean collapsedMenu;

	private Set<Group> groups = new HashSet<Group>();
    private Set<Team> teams = new HashSet<Team>();
	
	//transient
	private boolean online;
	private String sessionId;
	private String newPassword;

	/**
	 * Método que calcula e retorna o tempo que este usuário irá levar para ler
	 * o texto informado.
	 * <p>
	 * Se o texto for {@code null}, ou se o valor da constante de tempo de
	 * leitura for menor ou igual a zero, será retornado um valor de tempo
	 * {@code 00:00:00}.
	 * </p>
	 * 
	 * @param text texto cujo o tempo de leitura será calculado
	 * @return um objeto {@link java.util.Date} com o tempo de leitura calculado
	 */
	public Date calculateReadTime(String text) {
		if (text == null || readTime <= 0) {
			return DateTimeUtil.getMidnightTime();
		} else {
			text = text.trim().replace("/\n\t\r/g", "");
			return DateTimeUtil.millisToTime((int) (text.length() * readTime * 1000));
		}
	}
	
	public void addGroup(Group group) {
		if (!groups.contains(group)) {
			groups.add(group);
		}
	}

	public boolean belongsToGroup(Group group) {
		return groups.contains(group);
	}
	
	@AmfIgnore
	public boolean isSupportUser() {
		return SUPPORT_EMAIL.equals(email);
	}
	
	@Override
	public int compareTo(User other) {
		if (nickname == other.nickname) return 0;
		if (nickname == null) return -1;
		if (other.nickname == null) return 1;

		return String.CASE_INSENSITIVE_ORDER.compare(nickname, other.nickname);
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		} else if (obj instanceof User) {
			User other = (User) obj;
			return Objects.equals(nickname, other.nickname)
				&& Objects.equals(deleted, other.deleted);
		}
		return false;
	}

    //----------------------------------
    //  UserDetails Methods
    //----------------------------------

	@Override
	@AmfIgnore
	public Collection<GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new TreeSet<>();

		// Junta todas as permissões de todos os grupos do usuário em uma única coleção
		for (Group group : groups) {
			for (Permission permission : group.getPermissions()) {
				authorities.add(permission);
			}
		}

		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	@AmfIgnore
	public String getUsername() {
		return email;
	}

	@Override
	@AmfIgnore
	public boolean isAccountNonExpired() {
		return email.equals(SUPPORT_EMAIL) || passwordExpiration != null && passwordExpiration.after(new Date());
	}

	@Override
	@AmfIgnore
	public boolean isAccountNonLocked() {
		return email.equals(SUPPORT_EMAIL) || agreementDate != null;
	}

	@Override
	@AmfIgnore
	public boolean isCredentialsNonExpired() {
		return isAccountNonExpired();
	}

	@Override
	@AmfIgnore
	public boolean isEnabled() {
		return email.equals(SUPPORT_EMAIL) || !deleted;
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

    public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date value) {
		this.birthdate = value;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String value) {
		this.phoneNumber = value;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean value) {
		this.online = value;
	}

	public Set<Group> getGroups() {
		return groups;
	}

	public void setGroups(Set<Group> value) {
		this.groups = value;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String value) {
		this.sessionId = value;
	}

	public Date getAgreementDate() {
		return agreementDate;
	}

	public void setAgreementDate(Date value) {
		this.agreementDate = value;
	}

	public Date getPasswordExpiration() {
		return passwordExpiration;
	}

	public void setPasswordExpiration(Date value) {
		this.passwordExpiration = value;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String getNextelNumber() {
		return nextelNumber;
	}

	public void setNextelNumber(String nextelNumber) {
		this.nextelNumber = nextelNumber;
	}

	public double getReadTime() {
		return readTime;
	}

	public void setReadTime(double readTime) {
		this.readTime = readTime;
	}

    @AmfIgnore
    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public boolean isCollapsedMenu() {
        return collapsedMenu;
    }

    public void setCollapsedMenu(boolean collapsedMenu) {
        this.collapsedMenu = collapsedMenu;
    }

    private static final long serialVersionUID = 6761616431623828931L;
}
