package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Objects;

import static java.lang.String.CASE_INSENSITIVE_ORDER;
import static org.hibernate.search.annotations.Analyze.NO;

/**
 * Representa uma classificação da pauta.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.7
 */
@Entity
@Indexed(index = "indexes/guidelineclassifications")
public class GuidelineClassification extends AbstractEntity<Integer> implements Comparable<GuidelineClassification> {

	@Field(analyze = NO)
	private String name;
	
	//------------------------------------
	//  Operations
	//------------------------------------

	public int compareTo(GuidelineClassification other) {
		return CASE_INSENSITIVE_ORDER.compare(name, other.name);
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	    	return true;
	    }
	    if (obj instanceof GuidelineClassification) {
	    	GuidelineClassification other = (GuidelineClassification) obj;
	    	return Objects.equals(name, other.name);
	    }
	    return false;
	}
	
	@Override
	public int hashCode() {
	    return Objects.hash(name);
	}
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public String getName() {
	    return name;
    }
    
    public void setName(String name) {
	    this.name = name;
    }
	
    private static final long serialVersionUID = -7389146761450077386L;
}
