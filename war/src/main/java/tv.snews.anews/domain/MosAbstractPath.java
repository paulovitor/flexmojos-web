package tv.snews.anews.domain;

import java.util.Objects;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public abstract class MosAbstractPath extends AbstractEntity<Long> {

	protected String techDescription;
	protected String url;

	//------------------------------------
	//  Constructors
	//----------------------------------
	
	protected MosAbstractPath() {}
	
	protected MosAbstractPath(String techDescription, String url) {
		this.techDescription = techDescription;
		this.url = url;
	}
	
	//------------------------------------
	//  Operations
	//----------------------------------
	
	@Override
    public int hashCode() {
		return Objects.hash(techDescription, url);
    }

	@Override
    public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof MosAbstractPath) {
			MosAbstractPath other = (MosAbstractPath) obj;
			return Objects.equals(techDescription, other.techDescription)
				&& Objects.equals(url, other.url);
		}
		return false;
    }
	
	//------------------------------------
	//  Getters & Setters
	//----------------------------------
	
	public String getTechDescription() {
    	return techDescription;
    }
	
    public void setTechDescription(String techDescription) {
    	this.techDescription = techDescription;
    }
	
    public String getUrl() {
    	return url;
    }
	
    public void setUrl(String url) {
    	this.url = url;
    }
	
    private static final long serialVersionUID = 2383601998035770299L;

}
