package tv.snews.anews.domain;

import javax.persistence.Transient;
import java.util.Date;

public class ReportageVideo extends ReportageSubSection {

    private static final long serialVersionUID = 5245847557822054965L;

    private MosDeviceChannel channel;
    private String mosStatus;
    private Media media;
    private String slug = "";
    private Date durationTime;
    private boolean ready;
    private boolean disabled;

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public MosDeviceChannel getChannel() {
        return channel;
    }

    public void setChannel(MosDeviceChannel channel) {
        this.channel = channel;
    }

    public String getMosStatus() {
        return mosStatus;
    }

    public void setMosStatus(String mosStatus) {
        this.mosStatus = mosStatus;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        updateMediaData(media);
        this.media = media;
    }

    public void updateMediaData(Media media) {
        if (media != null) {
            this.durationTime = media.getDurationTime();
            this.slug = media.getSlug();
        }
        this.disabled = (media == null);
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Date durationTime) {
        this.durationTime = durationTime;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    @Transient
    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public MosMedia getMosMedia() {
        return media instanceof MosMedia ? (MosMedia) media : null;
    }

    public WSMedia getWSMedia() {
        return media instanceof WSMedia ? (WSMedia) media : null;
    }

}
