package tv.snews.anews.domain;

import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;

/**
 * Representa um relatório escrito por um usuário do sistema reportando um
 * evento relacionado a uma pauta.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
@Entity
@Indexed(index = "indexes/guidelinereport")
public class GuidelineReport extends Report {

	private Guideline guideline;

	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public Guideline getGuideline() {
	    return guideline;
    }
    
    public void setGuideline(Guideline guideline) {
	    this.guideline = guideline;
    }
    
    private static final long serialVersionUID = -9216819858937654322L;
}
