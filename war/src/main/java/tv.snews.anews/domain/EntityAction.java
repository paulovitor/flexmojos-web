package tv.snews.anews.domain;

/**
 * Representa as ações possíveis de acontecer sobre as entidades.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public enum EntityAction {
	
	/**
	 * Possiveis ações que uma entidade pode sofrer.
	 */
	INSERTED, DELETED, UPDATED, FIELD_UPDATED, LOCKED, UNLOCKED, RESTORED, MOVED
}
