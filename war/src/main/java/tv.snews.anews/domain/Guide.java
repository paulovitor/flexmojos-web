package tv.snews.anews.domain;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;

/**
 * Domínio que representa o roteiro.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/guide")
public class Guide extends AbstractEntity<Integer> implements Cloneable, Comparable<Guide> {

	private static final long serialVersionUID = 3208253554313372287L;

	private Date schedule;
	
	@Field
	private String address;
	
	@Field
	private String reference;
	
	@Field
	private String observation;
	
	private int orderGuide;
	
	@ContainedIn
	private Guideline guideline;
	
	private City city;
	private Set<Contact> contacts = new HashSet<>();

    //----------------------------------
    //  Operations
    //----------------------------------

    public Guide createCopy(Guideline guidelineCopy) {
        Guide copy = new Guide();
        copy.setSchedule(schedule);
        copy.setAddress(address);
        copy.setReference(reference);
        copy.setObservation(observation);
        copy.setOrderGuide(orderGuide);
        copy.setCity(city);

        copy.setGuideline(guidelineCopy);

        for (Contact contact : contacts) {
            copy.getContacts().add(contact.createCopy());
        }

        return copy;
    }

	@Override
	public Object clone() throws CloneNotSupportedException {
	    return super.clone();
	}
	
	@Override
	public int compareTo(Guide other) {
	    return orderGuide - other.orderGuide;
	}

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

	public Date getSchedule() {
		return schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule == null ? null : addTimeToCurrentDate(schedule);
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

    public int getOrderGuide() {
    	return orderGuide;
    }

    public void setOrderGuide(int orderGuide) {
    	this.orderGuide = orderGuide;
    }

	public Guideline getGuideline() {
		return guideline;
	}

	public void setGuideline(Guideline guideline) {
		this.guideline = guideline;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
}
