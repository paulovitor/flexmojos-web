package tv.snews.anews.domain;

/**
 * Enum para ações do script no roteiro.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public enum ScriptAction {
	
	/*
	 * NOTE A action UPDATED só é lançada quando não tem a informação de qual 
	 * campo foi atualizado.
	 */
	INSERTED, UPDATED, EXCLUDED, COPIED, ARCHIVED,

    BLOCK_CHANGED, ORDER_CHANGED, TAPE_CHANGED,

    VT_CHANGED, EDITOR_CHANGED, REPORTER_CHANGED, PAGE_CHANGED, SLUG_CHANGED,

    APPROVED, DISAPPROVED, OK, IMAGE_EDITOR_CHANGED, NOT_OK,

    EDITING_USER_CHANGED, MULTIPLE_EXCLUSION
}


