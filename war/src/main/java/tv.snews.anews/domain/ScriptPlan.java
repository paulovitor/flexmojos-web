package tv.snews.anews.domain;

import org.hibernate.search.annotations.*;
import tv.snews.anews.util.ResourceBundle;

import javax.persistence.Entity;
import java.util.*;

import static org.hibernate.search.annotations.Analyze.NO;
import static org.hibernate.search.annotations.Resolution.MILLISECOND;
import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
@Entity
@Indexed(index = "indexes/scriptplan")
public class ScriptPlan extends AbstractEntity<Long> {

	private String slug;
	private String channel;
	private String mosStatus;

	@Field(analyze = NO)
	@DateBridge(resolution = MILLISECOND)
	private Date date;

	private Date start;
	private Date end;
	private Date productionLimit;

	@IndexedEmbedded
	private Program program;

	@ContainedIn
	private SortedSet<ScriptBlock> blocks = new TreeSet<>();

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

    private boolean displayed = false;

	//------------------------------
	//	Constructor
	//------------------------------

	public ScriptPlan() {
		productionLimit = getMidnightTime();
	}

    public ScriptPlan(Date date, Program program, Date start, Date end, Date productionLimit){
        this.date = date;
        this.program = program;
        this.start = start;
        this.end = end;
        this.productionLimit = productionLimit;

    }

	//------------------------------
	//	Operations
	//------------------------------

	public ScriptBlock createBlock() {
		ScriptBlock block = new ScriptBlock(this);
		block.setOrder(blocks.size());
		block.setStandBy(blocks.isEmpty());

		if (block.isStandBy()) {
			block.setSlug(bundle.getMessage("scriptblock.standBy"));
		} 

		blocks.add(block);
		return block;
	}

	public void removeBlock(ScriptBlock block) {
		ListIterator<ScriptBlock> it = (new ArrayList<>(blocks)).listIterator();

		if (blocks.remove(block)) {
			while (it.hasNext()) {
				ScriptBlock curr = it.next();
				if (curr.getOrder() > block.getOrder()) {
					// Decrementa as ordens dos blocos posteriores
					curr.setOrder(curr.getOrder() - 1);
				}
			}
		} else {
			throw new IllegalArgumentException("This block doesn't belong to this script plan.");
		}
	}

	public String nextPage() {
		int max = 0;
		for (ScriptBlock block : blocks) {
			for (Script script : block.getScripts()) {
				// Tem que deixar somente números
				int aux = Integer.parseInt(script.getPage().replaceAll("[^\\d]", ""));
				max = aux > max ? aux : max;
			}
		}
		return ++max > 9 ? Integer.toString(max) : "0" + max;
	}

	public ScriptBlock standBy() {
		if (blocks.isEmpty()) {
			return null;
		}
		return blocks.first();
	}

	public SortedSet<Script> approvedScripts() {
		SortedSet<Script> result = new TreeSet<>();
		for (ScriptBlock block : blocks) {
			result.addAll(block.approvedScripts());
		}
		return result;
	}

	public Script nextApprovedScript(Script target) {
		for (Script script : approvedScripts()) {
			if (script.compareTo(target) > 0) {
				return script;
			}
		}

		return null;
	}

	public Script previousApprovedScript(Script target) {
		List<Script> aux = new ArrayList<>(approvedScripts());

		// Coloca a lista em ordem decrescente
		Collections.sort(aux);
		Collections.reverse(aux);

		for (Script script : aux) {
			if (script.compareTo(target) < 0) {
				return script;
			}
		}

		return null;
	}

	public void renumberPages() {
		int page = 0;

		for (ScriptBlock block : blocks) {
			for (Script script : block.getScripts()) {
				String newPage = (++page < 10 ? "0" : "") + page;
				if (!script.getPage().equals(newPage)) {
					script.setPage(newPage);
				}
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof ScriptPlan) {
			ScriptPlan other = (ScriptPlan) obj;
			return Objects.equals(date, other.date)
					&& Objects.equals(program, other.program);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, program);
	}

	public void updateStoriesDates() {
		for (ScriptBlock block : blocks) {
			for (Script script : block.getScripts()) {
				script.setDate(date);
			}
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getMosStatus() {
		return mosStatus;
	}

	public void setMosStatus(String mosStatus) {
		this.mosStatus = mosStatus;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start == null ? getMidnightTime() : addTimeToCurrentDate(start);
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end == null ? getMidnightTime() : addTimeToCurrentDate(end);
	}

	public Date getProductionLimit() {
		return productionLimit;
	}

	public void setProductionLimit(Date productionLimit) {
		this.productionLimit = productionLimit == null ? getMidnightTime() : addTimeToCurrentDate(productionLimit);
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;

		if (program != null) {
			if (start == null) {
				start = program.getStart();
			}
			if (end == null) {
				end = program.getEnd();
			}
		}
	}

	public SortedSet<ScriptBlock> getBlocks() {
		return blocks;
	}

	public void setBlocks(SortedSet<ScriptBlock> blocks) {
		this.blocks = blocks;
	}

	private static final long serialVersionUID = 8613097920106078657L;

    public static enum Fields {START, END, PRODUCTION, DATE, SLUG, CHANNEL}

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }
}
