package tv.snews.anews.domain;

import java.util.Objects;

/**
 * @author Eliezer Reis
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class StoryCGField extends AbstractEntity<Long> implements Comparable<StoryCGField>, Cloneable {

	private static final long serialVersionUID = 7972066299826264155L;

	private String name;
	private String value;
	private int number;
	private int size;

	private StoryCG storyCG;

	public StoryCGField() {
		super();
	}

	public StoryCGField(ReportageCGField reportageCGField) {
		this.name = reportageCGField.getName();
		this.value = reportageCGField.getValue();
		this.number = reportageCGField.getNumber();
		this.size = reportageCGField.getSize();
	}

	public StoryCGField(StoryCG storyCG, IITemplateField field) {
		super();

		if (field != null) {
			this.storyCG = storyCG;
			this.name = field.getName();
			this.number = field.getNumber();
			this.size = field.getSize();
		}
	}

	@Override
	public StoryCGField clone() throws CloneNotSupportedException {
		StoryCGField clone = new StoryCGField();

		clone.name = name;
		clone.value = value;
		clone.number = number;
		clone.size = size;

		return clone;
	}

	@Override
	public int compareTo(StoryCGField other) {
		return number > other.number ? 1 : number < other.number ? -1 : 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, number);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		} else if (obj instanceof StoryCGField) {
			StoryCGField other = (StoryCGField) obj;
			return Objects.equals(id, other.id)
					&& Objects.equals(number, other.number);
		}
		return false;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public StoryCG getStoryCG() {
		return storyCG;
	}

	public void setStoryCG(StoryCG storyCG) {
		this.storyCG = storyCG;
	}
}
