package tv.snews.anews.domain;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public abstract class StoryMosObject extends StorySubSection {

	protected MosMedia mosMedia;
	protected String mosStatus;

	public MosMedia getMosMedia() {
		return mosMedia;
	}

	public void setMosMedia(MosMedia mosMedia) {
		this.mosMedia = mosMedia;
	}

	public String getMosStatus() {
		return mosStatus;
	}

	public void setMosStatus(String mosStatus) {
		this.mosStatus = mosStatus;
	}

	private static final long serialVersionUID = -7587167493723885258L;
}
