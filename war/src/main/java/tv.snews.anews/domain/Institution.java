package tv.snews.anews.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Classe que representa as instituições da Ronda.
 *
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class Institution extends AbstractEntity<Integer> {

	private static final long serialVersionUID = -3288348964693402337L;

	private String name;
	private Segment segment;
	private Set<Contact> contacts = new HashSet<>();

    private Contact defaultContact;

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Segment getSegment() {
		return segment;
	}

	public void setSegment(Segment segment) {
		this.segment = segment;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}


    public Contact getDefaultContact() {
        return defaultContact;
    }

    public void setDefaultContact(Contact defaultContact) {
        this.defaultContact = defaultContact;
    }

}
