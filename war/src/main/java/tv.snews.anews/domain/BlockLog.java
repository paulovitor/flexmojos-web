package tv.snews.anews.domain;

/**
 * Entidade para log de ações do bloco.
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class BlockLog extends AbstractLog {

	private BlockAction action;
	
	private String blockName;

	public BlockLog() {}
	
    public BlockLog(Block block, String blockName, User author, BlockAction action) {
    	super(block.getRundown(), author);
		 this.blockName = blockName;
		 this.action = action;
    }
	
	public BlockAction getBlockAction() {
	    return action;
    }

	public void setBlockAction(BlockAction blockAction) {
	    this.action = blockAction;
    }

    public String getBlockName() {
    	return blockName;
    }
	
    public void setBlockName(String blockName) {
    	this.blockName = blockName;
    }
	
	private static final long serialVersionUID = 2694666200438047476L;
}