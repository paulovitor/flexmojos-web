package tv.snews.anews.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class IIDevice extends Device {

	private String hostname;
	private Integer port = 23;
	private Integer increment = 0;
	private boolean virtual = false;

	private SortedSet<IIDevicePath> paths = new TreeSet<>();
	private Set<IITemplate> templates = new HashSet<>();

	public IIDevice() {
		this.protocol = DeviceProtocol.II;
	}

	public void addPath(String path) {
		paths.add(new IIDevicePath(path, this));
	}

	public void removePath(IIDevicePath path) {
		if (paths.remove(path)) {
			path.setDevice(null);
		}
	}

	public void addTemplate(IITemplate template) {
		template.setDevice(this);
		templates.add(template);
	}

	public void removeTemplate(IITemplate template) {
		templates.remove(template);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getIncrement() {
		return increment;
	}

	public void setIncrement(int increment) {
		this.increment = increment;
	}

	public boolean isVirtual() {
		return virtual;
	}

	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}

	public SortedSet<IIDevicePath> getPaths() {
		return paths;
	}

	public void setPaths(SortedSet<IIDevicePath> paths) {
		this.paths = paths;
	}

	public Set<IITemplate> getTemplates() {
		return templates;
	}

	public void setTemplates(Set<IITemplate> templates) {
		this.templates = templates;
	}

	private static final long serialVersionUID = -9048985328885951283L;
}
