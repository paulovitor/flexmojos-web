package tv.snews.anews.domain;

/**
 * Entidade para log de ações do bloco.
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class StoryLog extends AbstractLog {

	private Story story;
	private StoryAction storyAction;
	
	public StoryLog () {
	}
	
    public StoryLog(Rundown rundown, Story story, User author, StoryAction storyAction) {
    	super(rundown, author);
    	this.story = story;
    	this.storyAction = storyAction;
    }
    
    public Story getStory() {
    	return story;
    }
	
    public void setStory(Story story) {
    	this.story = story;
    }

	public StoryAction getStoryAction() {
	    return storyAction;
    }

	public void setStoryAction(StoryAction storyAction) {
	    this.storyAction = storyAction;
    }

	private static final long serialVersionUID = -315029405087651649L;
}
