package tv.snews.anews.domain;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Classe que representa a sessão gc da reportagem.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
public class ReportageCG extends AbstractEntity<Long> implements Comparable<ReportageCG>, Cloneable {

	private static final long serialVersionUID = -2323857212096957753L;

	private int order;
	private int code;
	private int templateCode;
	private boolean comment = false;

	private SortedSet<ReportageCGField> fields = new TreeSet<>();
    private Reportage reportage;

    public ReportageCG() {
        super();
    }

    public ReportageCG(int order) {
        super();
        this.order = order;
    }

    @Override
    public int compareTo(ReportageCG other) {
        return order - other.order;
    }

	@Override
	protected ReportageCG clone() throws CloneNotSupportedException {
		ReportageCG clone = new ReportageCG();
		
		clone.order = order;
		clone.code = code;
		clone.templateCode = templateCode;
		clone.comment = comment;

		for (ReportageCGField field : this.fields) {
			ReportageCGField fieldClone = field.clone();
			fieldClone.setReportageCG(clone);
			clone.fields.add(fieldClone);
    }

		return clone;
    }

	/**
	 * @return verdadeiro se existe algum field preenchido dentro da lista
	 */
	public boolean isCgFilled() {
		if (fields != null) {
			for (ReportageCGField field : fields) {
				if (isNotBlank(field.getValue())) {
					return true;
				}
			}
		}
		return false;
	}

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(int templateCode) {
		this.templateCode = templateCode;
	}

	public boolean isComment() {
		return comment;
	}

	public void setComment(boolean comment) {
		this.comment = comment;
	}

	public SortedSet<ReportageCGField> getFields() {
		return fields;
	}

	public void setFields(SortedSet<ReportageCGField> fields) {
		this.fields = fields;
	}

	public Reportage getReportage() {
			return reportage;
	}

	public void setReportage(Reportage reportage) {
			this.reportage = reportage;
	}
}
