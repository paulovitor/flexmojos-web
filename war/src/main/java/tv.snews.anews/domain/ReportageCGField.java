package tv.snews.anews.domain;

/**
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
public class ReportageCGField extends AbstractEntity<Long> implements Comparable<ReportageCGField>, Cloneable {

	private static final long serialVersionUID = 7972066299826264155L;

	private String name;
	private String value;
	private int number;
	private int size;

	private ReportageCG reportageCG;

	//------------------------------
	//	Constructor
	//------------------------------

	public ReportageCGField() {
		super();
	}

	//------------------------------
	//	Operations
	//------------------------------

	@Override
	public ReportageCGField clone() throws CloneNotSupportedException {
		ReportageCGField clone = new ReportageCGField();

		clone.name = name;
		clone.value = value;
		clone.number = number;
		clone.size = size;

		return clone;
	}

	@Override
	public int compareTo(ReportageCGField other) {
		return number > other.number ? 1 : number < other.number ? -1 : 0;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public ReportageCG getReportageCG() {
		return reportageCG;
	}

	public void setReportageCG(ReportageCG reportageCG) {
		this.reportageCG = reportageCG;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
