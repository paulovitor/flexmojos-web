package tv.snews.anews.domain;


/**
 * Classe que representa o tipo da lauda de um espelho.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public class StoryKind extends AbstractEntity<Long> implements Cloneable {

	private String name;
	private String acronym;
	private Boolean defaultKind;
	
	@Override
    public StoryKind clone() throws CloneNotSupportedException {
        StoryKind clone = new StoryKind();
        clone.setName(name);
        clone.setAcronym(acronym);
        clone.setDefaultKind(defaultKind);
        return clone;
    }

    public String getName() {
    	return name;
    }

    public void setName(String name) {
    	this.name = name;
    }
	
    public String getAcronym() {
    	return acronym;
    }
	
    public void setAcronym(String acronym) {
    	this.acronym = acronym;
    }

    public Boolean getDefaultKind() {
    	return defaultKind;
    }

    public void setDefaultKind(Boolean defaultKind) {
    	this.defaultKind = defaultKind;
    }
    
    private static final long serialVersionUID = -8124166705763116380L;
}
