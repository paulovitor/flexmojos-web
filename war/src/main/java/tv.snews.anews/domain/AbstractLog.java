package tv.snews.anews.domain;

import java.util.Date;

/**
 * Classe abstrata para log de ações do sistema.
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class AbstractLog extends AbstractEntity<Long> {
	
    private static final long serialVersionUID = -3537417402417323162L;
    
	private String action; 
	private Date date;
	private User author;
	private Rundown rundown;
	
	public AbstractLog() {
	}
	
	public AbstractLog(Rundown rundown, User author) {
		this.rundown = rundown;
		this.author = author;
		this.date = new Date();
	}
	
    public String getAction() {
    	return action;
    }
	
    public void setAction(String action) {
    	this.action = action;
    }
	
    public Date getDate() {
    	return date;
    }
	
    public void setDate(Date date) {
    	this.date = date;
    }
	
    public User getAuthor() {
    	return author;
    }
	
    public void setAuthor(User author) {
    	this.author = author;
    }

	public Rundown getRundown() {
	    return rundown;
    }

	public void setRundown(Rundown rundown) {
	    this.rundown = rundown;
    }
}
