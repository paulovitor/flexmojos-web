package tv.snews.anews.domain;

import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class IITemplate extends AbstractEntity {

	private String name;

	private Integer templateCode;
	private boolean comment = false;

	private IIDevice device;

	private SortedSet<IITemplateField> fields = new TreeSet<>();

	public void addField(IITemplateField field) {
		field.setTemplate(this);
		fields.add(field);
	}

	public void removeField(IITemplateField field) {
		fields.remove(field);
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}
		if (obj instanceof IITemplate) {
			IITemplate other = (IITemplate) obj;

			if (id != null || other.id != null) {
				return Objects.equals(id, other.id);
			}

			return Objects.equals(templateCode, other.templateCode)
					&& Objects.equals(name, other.name);
		}
		return false;
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return Objects.hash(id);
		}
		return Objects.hash(templateCode, name);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(Integer templateCode) {
		this.templateCode = templateCode;
	}

	public boolean isComment() {
		return comment;
	}

	public void setComment(boolean comment) {
		this.comment = comment;
	}

	public IIDevice getDevice() {
		return device;
	}

	public void setDevice(IIDevice device) {
		this.device = device;
	}

	public SortedSet<IITemplateField> getFields() {
		return fields;
	}

	public void setFields(SortedSet<IITemplateField> fields) {
		this.fields = fields;
	}

	private static final long serialVersionUID = -7165111158296449492L;
}
