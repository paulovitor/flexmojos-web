package tv.snews.anews.domain;

import org.hibernate.search.annotations.*;
import org.joda.time.LocalDate;

import javax.persistence.Entity;
import java.util.*;

import static org.hibernate.search.annotations.Analyze.NO;
import static org.hibernate.search.annotations.Resolution.MILLISECOND;
import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * Domínio que representa o espelho.
 *
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/rundown")
public class Rundown extends AbstractEntity<Long> {

	private static final long serialVersionUID = 207710834553697420L;

	@Field(analyze = NO)
	@DateBridge(resolution = MILLISECOND)
	private Date date;

	private Date start;
	private Date end;
	private Date production;
    private boolean displayed = false;

	private String mosStatus;

	@IndexedEmbedded
	private Program program;

	@ContainedIn
	private Set<Block> blocks = new TreeSet<>();

	//----------------------------------
	//  Constructors
	//----------------------------------

	public Rundown() {
	}

	public Rundown(RundownTemplate template, User author) {
		if (template != null) {
			for (BlockTemplate blockTemplate : template.getBlocks()) {
				blocks.add(new Block(this, blockTemplate, author));
			}
		}
	}

	//----------------------------------
	//  Operations
	//----------------------------------

//	public Story previousApprovedStory(Story target) {
//		List<Story> aux = new ArrayList<>(approvedStories());
//
//		// Coloca a lista em ordem decrescente
//		Collections.sort(aux);
//		Collections.reverse(aux);
//
//		for (Story story : aux) {
//			if (story.compareTo(target) < 0) {
//				return story;
//			}
//		}
//
//		return null;
//	}

//	public SortedSet<Story> approvedStories() {
//		SortedSet<Story> stories = new TreeSet<>();
//
//		for (Block block : blocks) {
//			for (Story story : block.getStories()) {
//				if (story.isApproved()) {
//					stories.add(story);
//				}
//			}
//		}
//
//		return stories;
//	}

	/**
	 * Retorna o objeto do bloco "Stand By". Em caso incomum dele não existir,
	 * será retornado {@code null}.
	 *
	 * @return o objeto do bloco "Stand By" ou {@code null} caso não encontre
	 */
	public Block findStandBy() {
		for (Block block : blocks) {
			if (block.isStandBy()) {
				return block;
			}
		}

		return null;
	}

	/**
	 * Retorna a próxima página na númeração dentre as laudas já pertecentes a
	 * este espelho.
	 *
	 * @return número da próxima página
	 */
//	public String calculateNextPage() {
//		int max = 0;
//		for (Block block : blocks) {
//			for (Story story : block.getStories()) {
//				if (!story.isExcluded()) {
//					int aux = Integer.parseInt(story.getPage().replaceAll("[^\\d]", ""));
//					max = aux > max ? aux : max;
//				}
//			}
//		}
//		return ++max > 9 ? Integer.toString(max) : "0" + max;
//	}

	public void updateStoriesDates() {
		for (Block block : blocks) {
			for (Story story : block.getStories()) {
				story.setDate(date);
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj instanceof Rundown) {
			Rundown other = (Rundown) obj;
			return Objects.equals(date, other.date)
					&& Objects.equals(program, other.program);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, program);
	}

	public <T extends StorySubSection> int countStoriesSubSectionsOfType(Class<T> type) {
		return countStoriesSubSectionsOfType(type, false);
	}

	public <T extends StorySubSection> int countStoriesSubSectionsOfType(Class<T> type, boolean onlyApprovedContent) {
		int count = 0;

		for (Block block : blocks) {
			count += block.countStoriesSubSectionsOfType(type, onlyApprovedContent);
		}

		return count;
	}

	public <T extends StorySubSection> List<T> storiesSubSectionsOfType(Class<T> type) {
		return storiesSubSectionsOfType(type, false);
	}

	public <T extends StorySubSection> List<T> storiesSubSectionsOfType(Class<T> type, boolean onlyApprovedContent) {
		List<T> results = new ArrayList<>();

		for (Block block : blocks) {
			results.addAll(block.storiesSubSectionsOfType(type, onlyApprovedContent));
		}

		return results;
	}

	public String displayName() {
		LocalDate date = new LocalDate(this.date);
		return program.getName() + " " + date.toString();
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start == null ? getMidnightTime() : addTimeToCurrentDate(start);
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end == null ? getMidnightTime() : addTimeToCurrentDate(end);
	}

	public Date getProduction() {
		return production;
	}

	public void setProduction(Date production) {
		this.production = production == null ? getMidnightTime() : addTimeToCurrentDate(production);
	}

	public Set<Block> getBlocks() {
		return blocks;
	}

	public void setBlocks(Set<Block> blocks) {
		this.blocks = blocks;
	}

	public String getMosStatus() {
		return mosStatus;
	}

	public void setMosStatus(String mosStatus) {
		this.mosStatus = mosStatus;
	}

	public static enum Fields {START, END, PRODUCTION, DATE}

    public boolean isDisplayed() {
        return displayed;
    }

    public void setDisplayed(boolean displayed) {
        this.displayed = displayed;
    }
}
