package tv.snews.anews.domain;

import tv.snews.mos.MosRevision;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class MosDevice extends Device {

	private String mosId;
	private String hostname;

	private Integer lowerPort = 10540;
	private Integer upperPort = 10541;
	private Integer queryPort = 10542;
	private Integer currentMessageId = 0;

	private boolean activeX = false;
	private boolean tpMode = false;

	private boolean profileOneSupported = false;
	private boolean profileTwoSupported = false;
	private boolean profileThreeSupported = false;
	private boolean profileFourSupported = false;
	private boolean profileFiveSupported = false;
	private boolean profileSixSupported = false;
	private boolean profileSevenSupported = false;

	private MosRevision revision = MosRevision.V_284;
	private MosDeviceChannel defaultChannel;

	private SortedSet<MosDeviceChannel> channels = new TreeSet<>();

	public MosDevice() {
		this.protocol = DeviceProtocol.MOS;
	}

	public void addChannel(String channelName) {
		channels.add(new MosDeviceChannel(channelName, this));
	}

	public void removeChannel(MosDeviceChannel channel) {
		channels.remove(channel);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getMosId() {
		return mosId;
	}

	public void setMosId(String mosId) {
		this.mosId = mosId;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getLowerPort() {
		return lowerPort;
	}

	public void setLowerPort(int lowerPort) {
		this.lowerPort = lowerPort;
	}

	public int getUpperPort() {
		return upperPort;
	}

	public void setUpperPort(int upperPort) {
		this.upperPort = upperPort;
	}

	public int getQueryPort() {
		return queryPort;
	}

	public void setQueryPort(int queryPort) {
		this.queryPort = queryPort;
	}

	public int getCurrentMessageId() {
		return currentMessageId;
	}

	public void setCurrentMessageId(int currentMessageId) {
		this.currentMessageId = currentMessageId;
	}

	public boolean isActiveX() {
		return activeX;
	}

	public void setActiveX(boolean activeX) {
		this.activeX = activeX;
	}

	public boolean isProfileOneSupported() {
		return profileOneSupported;
	}

	public void setProfileOneSupported(boolean profileOneSupported) {
		this.profileOneSupported = profileOneSupported;
	}

	public boolean isProfileTwoSupported() {
		return profileTwoSupported;
	}

	public void setProfileTwoSupported(boolean profileTwoSupported) {
		this.profileTwoSupported = profileTwoSupported;
	}

	public boolean isProfileThreeSupported() {
		return profileThreeSupported;
	}

	public void setProfileThreeSupported(boolean profileThreeSupported) {
		this.profileThreeSupported = profileThreeSupported;
	}

	public boolean isProfileFourSupported() {
		return profileFourSupported;
	}

	public void setProfileFourSupported(boolean profileFourSupported) {
		this.profileFourSupported = profileFourSupported;
	}

	public boolean isProfileFiveSupported() {
		return profileFiveSupported;
	}

	public void setProfileFiveSupported(boolean profileFiveSupported) {
		this.profileFiveSupported = profileFiveSupported;
	}

	public boolean isProfileSixSupported() {
		return profileSixSupported;
	}

	public void setProfileSixSupported(boolean profileSixSupported) {
		this.profileSixSupported = profileSixSupported;
	}

	public boolean isProfileSevenSupported() {
		return profileSevenSupported;
	}

	public void setProfileSevenSupported(boolean profileSevenSupported) {
		this.profileSevenSupported = profileSevenSupported;
	}

	public boolean isTpMode() {
		return tpMode;
	}

	public void setTpMode(boolean tpMode) {
		this.tpMode = tpMode;
	}

	public MosRevision getRevision() {
		return revision;
	}

	public void setRevision(MosRevision revision) {
		this.revision = revision;
	}

	public MosDeviceChannel getDefaultChannel() {
		return defaultChannel;
	}

	public void setDefaultChannel(MosDeviceChannel defaultChannel) {
		this.defaultChannel = defaultChannel;
	}

	public SortedSet<MosDeviceChannel> getChannels() {
		return channels;
	}

	public void setChannels(SortedSet<MosDeviceChannel> channels) {
		this.channels = channels;
	}

	private static final long serialVersionUID = 137970852133637648L;
}
