package tv.snews.anews.domain;

import javax.persistence.Entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static tv.snews.anews.util.DateTimeUtil.*;

/**
 * Classe que representa a sub sessão da lauda NC.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
@Entity
//@Indexed(index = "indexes/storyncsection")
public class StoryNCSection extends StorySection {

	private Date timeImage = getMidnightTime();

	//------------------------------------
	//  Constructors
	//------------------------------------

	public StoryNCSection() {
		super();
	}

	public StoryNCSection(Date duration) {
		super(duration);
	}

	//------------------------------------
	//  Operations
	//------------------------------------

	/**
	 * Indica quais sub-sessões nunca serão armazenadas nessa sessão.
	 *
	 * Classes filhas devem sobreescrever este método caso for necessário alterar a regra.
	 */
	@Override
	protected Set<Class<?>> allowedSubSections() {
		return new HashSet<Class<?>>() {{
			add(StoryCG.class);
			add(StoryMosCredit.class);
			add(StoryText.class);
			add(StoryMosVideo.class);
			add(StoryWSVideo.class);
		}};
	}

	@Override
	public Date calculateDuration() {
		duration = getMidnightTime();
		timeImage = getMidnightTime();

		if (countSubSectionsOfType(StoryText.class) > 0 || countSubSectionsOfType(StoryMosVideo.class) > 0 || countSubSectionsOfType(StoryWSVideo.class) > 0) {
			for (StorySubSection subSection : getSubSections()) {
				/*
				 * Por enquanto as subSections das subSections não precisam
				 * calcular
				 * tempo, então elas não são tratadas nesse código.
				 */
				if (subSection instanceof StoryText) {
					StoryText storyText = (StoryText) subSection;
                    timeImage = sumTimes(timeImage, storyText.calculateReadTime());
				}
				if (subSection instanceof StoryMosVideo) {
					StoryMosVideo storyMosVideo = (StoryMosVideo) subSection;
                    duration = sumTimes(duration, addTimeToCurrentDate(storyMosVideo.getDurationTime()));
				}
				if (subSection instanceof StoryWSVideo) {
                    StoryWSVideo storyWSVideo = (StoryWSVideo) subSection;
                    duration = sumTimes(duration, addTimeToCurrentDate(storyWSVideo.getDurationTime()));
				}
			}
		}
		return duration;
	}

	@Override
	protected StoryNCSection clone() throws CloneNotSupportedException {
		StoryNCSection clone = new StoryNCSection();
		clone.setTimeImage(this.timeImage);
		clone.setDuration(duration);

		for (StorySubSection subSection : this.getSubSections()) {
			StorySubSection subSectionClone = subSection.clone();
			subSectionClone.setStorySection(clone);
			clone.addSubSection(subSectionClone);
		}
		return clone;
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	public Date getTimeImage() {
		return timeImage;
	}

	public void setTimeImage(Date timeImage) {
		this.timeImage = timeImage == null ? getMidnightTime() : addTimeToCurrentDate(timeImage);
	}
	
	private static final long serialVersionUID = -7020592040066334725L;
}
