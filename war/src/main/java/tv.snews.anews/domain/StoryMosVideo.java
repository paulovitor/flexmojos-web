package tv.snews.anews.domain;

import javax.persistence.Transient;
import java.util.Date;

/**
 * Classe que representa a sessão de video(obtido pelo mos) da lauda.
 *
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
public class StoryMosVideo extends StoryMosObject {

	private static final long serialVersionUID = -2323857212096957753L;

	private MosDeviceChannel channel;
    private String slug = "";
    private Date durationTime;
    private boolean ready;
    private boolean disabled;

	@Override
	protected StoryMosVideo clone() throws CloneNotSupportedException {
		StoryMosVideo storyMosVideoClone = new StoryMosVideo();
        storyMosVideoClone.setMosMedia(this.mosMedia);
        storyMosVideoClone.setSlug(this.slug);
        storyMosVideoClone.setDurationTime(this.durationTime);
        storyMosVideoClone.setReady(this.ready);
        storyMosVideoClone.setDisabled(this.disabled);
		storyMosVideoClone.setChannel(this.channel);
		storyMosVideoClone.setMosStatus(this.mosStatus);
		storyMosVideoClone.setOrder(this.getOrder());
		return storyMosVideoClone;
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

	public MosDeviceChannel getChannel() {
		return channel;
	}

	public void setChannel(MosDeviceChannel mosChannel) {
		this.channel = mosChannel;
	}

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Date getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Date durationTime) {
        this.durationTime = durationTime;
    }

    @Override
    public void setMosMedia(MosMedia mosMedia) {
        updateStoryMOSVideo(mosMedia);
        this.mosMedia = mosMedia;
    }

    public void updateStoryMOSVideo(MosMedia mosMedia){
        if(mosMedia != null) {
            this.ready = mosMedia.isReady();
            this.durationTime = mosMedia.getDurationTime();
            this.slug = mosMedia.getSlug();
        }
        this.disabled = (mosMedia == null);
        this.mosMedia = mosMedia;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    @Transient
    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
