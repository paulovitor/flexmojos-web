package tv.snews.anews.domain;

/**
 * Enum para ações dos documentos
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public enum DocumentAction {

	SENT_TO_DRAWER
}


