package tv.snews.anews.domain;

/**
 * Representa um destino de uma mensagem, contendo o destinatário e o status da
 * mensagem.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class MessageDestination extends AbstractEntity<Long> {

	private static final long serialVersionUID = -522816927673761566L;

//	private AbstractMessage message;
	private User receiver;
	private boolean readed;

	public MessageDestination() {
		// Hibernate
	}

	public MessageDestination(/*AbstractMessage message, */User receiver) {
//		this.message = message;
		this.receiver = receiver;
		this.readed = false;
	}

	/**
	 * No caso desta classe o <code>equals</code> irá verificar se a mensagem e
	 * o destinatário são iguais.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof MessageDestination) {
			MessageDestination other = (MessageDestination) obj;
			return receiver == other.receiver || (receiver != null && receiver.equals(other.receiver));
		}
		return false;
	}

	/**
	 * No caso desta classe o <code>hashCode</code> irá verificar se a mensagem
	 * e o destinatário são iguais.
	 */
	@Override
	public int hashCode() {
		int result = 1;
//		result = 31 * result + (this.message != null ? this.message.hashCode() : 0);
		result = 31 * result + (this.receiver == null ? 0 : this.receiver.hashCode());
		return result;
	}

	// ----------------------------------
	// Getters & Setters
	// ----------------------------------

//	public AbstractMessage getMessage() {
//		return message;
//	}
//
//	public void setMessage(AbstractMessage message) {
//		this.message = message;
//	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public boolean isReaded() {
		return readed;
	}

	public void setReaded(boolean readed) {
		this.readed = readed;
	}
}
