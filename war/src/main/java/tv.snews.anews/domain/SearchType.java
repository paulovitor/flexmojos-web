package tv.snews.anews.domain;



/**
 * Representa os tipos de busca.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public enum SearchType {
	
	/** Deve conter algum dos termos */
	SHOULD, 
	
	/** Deve conter todos os termos */
	MUST,
	
	/** Deve conter a frase */
	PHRASE,
	
	/** Aproximação */
	FUZZY;
	
}