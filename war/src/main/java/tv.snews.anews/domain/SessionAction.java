package tv.snews.anews.domain;

/**
 * Representa algumas ações possíveis de acontecer na sessão.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public enum SessionAction {
	USER_IN, USER_OUT;
}
