package tv.snews.anews.domain;

import org.apache.commons.lang.StringUtils;
import org.hibernate.search.annotations.DateBridge;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import tv.snews.anews.infra.search.ChecklistIndexingInterceptor;
import tv.snews.anews.util.ResourceBundle;

import javax.persistence.Entity;
import java.util.*;

import static org.hibernate.search.annotations.Analyze.NO;
import static org.hibernate.search.annotations.Resolution.MILLISECOND;
import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.EntityUtil.notValidId;
import static tv.snews.anews.util.EntityUtil.validId;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
@Entity
@Indexed(index = "indexes/checklist", interceptor = ChecklistIndexingInterceptor.class)
public class Checklist extends AbstractVersionedEntity<Long, ChecklistRevision> {

	private static final long serialVersionUID = 2110065804250190438L;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@Field(analyze = NO)
	@DateBridge(resolution = MILLISECOND)
	private Date date;      // data de execução

	@Field
	private String slug;

	@Field
	private String local;

	@Field
	private String info;

	@Field
	private String tapes;

//	@Field(analyze = NO) || Não precisa porque está usando o ChecklistIndexingInterceptor
	private boolean excluded = false;

	@IndexedEmbedded
	private User producer;

	@IndexedEmbedded
	private ChecklistType type;

	private Date schedule;  // hora de execução
	private Date departure; // hora de saída
	private Date arrival;   // hora de chegada
	private Date modified = new Date(); // data e hora de modificação/exclusão
	private boolean done = false;

	private Guideline guideline;
	private Program program;

	@IndexedEmbedded
	private SortedSet<ChecklistResource> resources = new TreeSet<>();

	@IndexedEmbedded
	private SortedSet<ChecklistTask> tasks = new TreeSet<>();

	private Set<User> editors = new HashSet<>();

	private Set<CommunicationVehicle> vehicles = new HashSet<>();

	private User editingUser; // não é persistido

	//----------------------------------
	//  Operations
	//----------------------------------

	public void add(ChecklistTask... tasksToAdd) {
		for (ChecklistTask task : tasksToAdd) {
			task.setChecklist(this);
			task.setIndex(tasks.size());
			tasks.add(task);
		}
	}

	public void add(ChecklistResource resource) {
		resources.add(resource);
	}

	public void remove(ChecklistTask task) {
        task.setChecklist(null);
		tasks.remove(task);

		// reorder
		int aux = 0;
		for (ChecklistTask current : tasks) {
			current.setIndex(aux++);
		}
	}

	public void remove(ChecklistResource resource) {
		resources.remove(resource);
	}

	public String resourcesToString() {
		SortedSet<String> names = new TreeSet<>();
		for (ChecklistResource resource : resources) {
			names.add(resource.getName());
		}
		String aux = StringUtils.join(names, ", ") + (names.isEmpty() ? "" : ".");

		// Troca a última vírgula por 'e'
		int index = aux.lastIndexOf(',');
		if (index != -1) {
			String and = bundle.getMessage("defaults.and");
			return aux.substring(0, index) + " " + and + aux.substring(index + 1, aux.length());
		} else {
			return aux;
		}
	}

	public String vehiclesToString() {
		SortedSet<String> vehiclesList = new TreeSet<>();
		for (CommunicationVehicle communicationVehicle : vehicles) {
			vehiclesList.add(bundle.getMessage("vehicle." + communicationVehicle.name().toLowerCase()));
		}
		String aux = StringUtils.join(vehiclesList, ", ") + (vehiclesList.isEmpty() ? "" : ".");

		// Troca a última vírgula por 'e'
		int index = aux.lastIndexOf(',');
		if (index != -1) {
			String and = bundle.getMessage("defaults.and");
			return aux.substring(0, index) + " " + and + aux.substring(index + 1, aux.length());
		} else {
			return aux;
		}
	}

	public String pendencies() {
		if (tasks.size() == 0) {
			return "";
		} else {
			int doneCount = 0;

			for (ChecklistTask task : tasks) {
				if (task.isDone()) {
					doneCount++;
				}
			}

			return doneCount + " / " + tasks.size();
		}
	}

	public String editorsToString() {
		return joinNicknames(editors);
	}
	
	private String joinNicknames(Collection<User> users) {
		SortedSet<String> nicknames = new TreeSet<>();
		for (User user : users) {
			nicknames.add(user.getNickname());
		}

		String aux = StringUtils.join(nicknames, ", ") + (nicknames.isEmpty() ? "" : ".");

		// Troca a última vírgula por 'e'
		int index = aux.lastIndexOf(',');
		if (index != -1) {
			String and = bundle.getMessage("defaults.and");
			return aux.substring(0, index) + " " + and + aux.substring(index + 1, aux.length());
		} else {
			return aux;
		}
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof Checklist) {
			Checklist other = (Checklist) obj;
			if (validId(id) && validId(other.id)) {
				return Objects.equals(id, other.id);
			} else {
				return Objects.equals(date, other.date)
						&& Objects.equals(slug, other.slug);
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return validId(id) ? Objects.hash(id) : Objects.hash(date, slug);
	}
	
	public void updateFields(Checklist newData) {
	 	setDate(newData.getDate());
        setSlug(newData.getSlug());
        setLocal(newData.getLocal());
        setInfo(newData.getInfo());
        setTapes(newData.getTapes());
        //setExcluded(newData.isExcluded());
        setProducer(newData.getProducer());
        setType(newData.getType());
        setSchedule(newData.getSchedule());
        setDeparture(newData.getDeparture());
        setArrival(newData.getArrival());
        setModified(new Date());
        setDone(newData.isDone());
        setGuideline(newData.getGuideline());
        
        if(newData.getGuideline() != null && newData.getGuideline().getChecklist() != null) {
        	newData.getGuideline().setChecklist(this);
        }
        
        setProgram(newData.getProgram());
        setResources(newData.getResources());
        setTasks(newData.getTasks());
        setVehicles(newData.getVehicles());
        setEditors(newData.getEditors());
        //setEditingUser(newData.getEditingUser());
	}

	public void updateFields() {
		if (guideline != null) {
			this.slug = guideline.getSlug() == null ? "" : guideline.getSlug();
			this.date = guideline.date;

			this.editors.clear();
			this.editors.addAll(guideline.getEditors());

			this.vehicles.clear();
			this.vehicles.addAll(guideline.getVehicles());

			// No momento da criação, sincronizo alguns dados extras
			if (notValidId(id)) {
				Guide firstGuide = guideline.firstGuide();
				if (firstGuide != null) {
					this.schedule = firstGuide.getSchedule();
					this.local = firstGuide.getAddress();
				}
			}
		}
	}

	public void updateFields(Guideline guideline) {
		this.guideline = guideline;
		updateFields();
	}

	public Checklist createCopy(User author, Guideline guidelineCopy) {
		Checklist copy = new Checklist();

		copy.date = date;
		copy.slug = slug;
		copy.excluded = excluded;
		copy.program = program;

		copy.author = this.author;

		copy.setSlug(slug);
		copy.setLocal(local);
		copy.setInfo(info);
		copy.setTapes(tapes);
		copy.setExcluded(excluded);
		copy.setProducer(producer);
		copy.setType(type);
		copy.setSchedule(schedule);
		copy.setDeparture(departure);
		copy.setArrival(arrival);
		copy.setModified(modified);
		copy.setDone(done);
		copy.setGuideline(guidelineCopy);
		copy.setProgram(program);
		copy.setEditingUser(editingUser);

		copy.getResources().addAll(resources);
		copy.getVehicles().addAll(vehicles);

		for (ChecklistTask task : tasks) {
			copy.getTasks().add(task.createCopy(copy));

		}

		copy.getEditors().addAll(editors);

		for (ChecklistRevision revision : revisions) {
			copy.getRevisions().add(revision.createCopy(copy));
		}

		return copy;
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule == null ? null : addTimeToCurrentDate(schedule);
	}

	public Date getDeparture() {
		return departure;
	}

	public void setDeparture(Date departure) {
		this.departure = departure == null ? null : addTimeToCurrentDate(departure);
	}

	public Date getArrival() {
		return arrival;
	}

	public void setArrival(Date arrival) {
		this.arrival = arrival == null ? null : addTimeToCurrentDate(arrival);
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getTapes() {
		return tapes;
	}

	public void setTapes(String tapes) {
		this.tapes = tapes;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public User getProducer() {
		return producer;
	}

	public void setProducer(User producer) {
		this.producer = producer;
	}

	public SortedSet<ChecklistResource> getResources() {
		return resources;
	}

	public void setResources(SortedSet<ChecklistResource> resources) {
		this.resources = resources;
	}

	public SortedSet<ChecklistTask> getTasks() {
		return tasks;
	}

	public void setTasks(SortedSet<ChecklistTask> tasks) {
		this.tasks = tasks;
	}

	public Guideline getGuideline() {
		return guideline;
	}

	public void setGuideline(Guideline guideline) {
		this.guideline = guideline;
	}

	public Program getProgram() {
		return program;
	}

	public void setProgram(Program program) {
		this.program = program;
	}

	public ChecklistType getType() {
		return type;
	}

	public void setType(ChecklistType type) {
		this.type = type;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	public boolean isExcluded() {
		return excluded;
	}

	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}

	public User getEditingUser() {
		return editingUser;
	}

	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}

	public Set<CommunicationVehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(Set<CommunicationVehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
    public Set<User> getEditors() {
    	return editors;
    }

    public void setEditors(Set<User> editors) {
    	this.editors = editors;
    }

}
