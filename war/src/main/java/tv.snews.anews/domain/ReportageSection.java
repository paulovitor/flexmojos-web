package tv.snews.anews.domain;

import org.hibernate.search.annotations.Boost;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import tv.snews.anews.util.DateTimeUtil;

import javax.persistence.Entity;
import java.util.Date;
import java.util.Objects;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Representa um trecho/seção de uma reportagem. Geralmente possui um trecho de 
 * texto, uma posição de localização e o tipo daquele trecho.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/reportagesection")
public class ReportageSection extends AbstractEntity<Long> implements Comparable<ReportageSection> {

    private static final long serialVersionUID = 8173773057009639977L;

    @Field @Boost(1.5f)
    private String content;
    
    @Field @Boost(1.5f)
    private String observation;
    
    private int position;
    private Date readTime;
    private boolean ok = false;
    
    @ContainedIn
    private Reportage reportage;
    
    private ReportageSectionType type;

    private SortedSet<ReportageSubSection> subSections = new TreeSet<>();

    //--------------------------------------------------------------------------
    //	Constructors
    //--------------------------------------------------------------------------
    
    /**
	 * Construtor básico que normalmente não será utilizado, mas que é
	 * necessário para o funcionamento do Hibernate. Na criação de novas seções
	 * dê preferência ao construtor {@link Reportage#Reportage(Guideline)}
	 */
    public ReportageSection() {
	    super();
	    calculateReadTime();
    }

    /**
	 * Construtor para a criação de seções de reportagem. Nele deve ser
	 * informado o tipo da seção.
	 *
	 * @param type Tipo da seção
	 */
    public ReportageSection(ReportageSectionType type) {
    	this();
    	this.type = type;
    }

    /**
	 * Construtor para a criação de seções de reportagem. Nele são informados a
	 * reportagem a qual a seção irá pertencer e o tipo da seção.
	 * <p>
	 * Esse construtor irá automaticamente invocar o método
	 * {@link Reportage#addSection(ReportageSection)} da reportagem informada.
	 * </p>
	 * 
	 * @param reportage Reportagem da qual a seção faz parte.
	 * @param type Tipo da seção
	 */
    public ReportageSection(Reportage reportage, ReportageSectionType type) {
    	this(type);
    	this.reportage = reportage;
    	
    	reportage.addSection(this);
    }
    
    //--------------------------------------------------------------------------
    //	Operations
    //--------------------------------------------------------------------------

    public ReportageSection createCopy(Reportage reportageCopy) {
        ReportageSection copy = new ReportageSection();
        copy.setContent(content);
        copy.setObservation(observation);
        copy.setPosition(position);
        copy.setReadTime(readTime);
        copy.setOk(ok);
        copy.setType(type);

        copy.setReportage(reportageCopy);

        return copy;
    }

    public final boolean isApprovable() {
    	if (type == null) {
    		return false;
    	}
    	
    	switch (type) {
    		case OPENING:
    		case APPEARANCE:
    		case CLOSURE:
    		case OFF:
    		case ART:
    			return true;
			default:
				return false;
    	}
    }
    
    /**
     * Informa se para este objeto deve ser calculado o tempo de leitura. Essa 
     * afirmação é baseada no tipo do mesmo.
     * 
     * @return {@code true} se deve ser calculado o tempo de leitura
     */
    public final boolean shouldCalculateReadTime() {
    	if (type == null) {
    		return false;
    	}
    	
    	switch (type) {
    		case OPENING:
    		case APPEARANCE:
    		case CLOSURE:
    		case OFF:
    			return true;
			default:
				return false;
    	}
    }
    
    /**
	 * Retorna a previsão do tempo de leitura do texto deste objeto. Se de
	 * acordo com o método {@link #shouldCalculateReadTime()} o tempo não deve
	 * ser calculado, será retornado {@code 00:00:00}.
	 * 
	 * @return a previsão do tempo de leitura ou {@code 00:00:00} se não houver
	 *         a necessidade do cálculo
	 */
    public final Date calculateReadTime() {
    	if (shouldCalculateReadTime()) {
    		readTime = reportage.getReporter().calculateReadTime(content);
    	} else {
    		readTime = DateTimeUtil.getMidnightTime();
    	}
    	return readTime;
    }
    
    /**
	 * Faz a verificação de igualdade a partir da regra definida na classe pai.
	 * No caso do resultado ser negativo, verifca se as seguintes propriedades
	 * são iguais: {@code reportage}, {@code type} e {@code position}.
	 */
    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
        	return true;
        } else if (obj instanceof ReportageSection) {
        	ReportageSection other = (ReportageSection) obj;
        	return Objects.equals(reportage, other.reportage) 
        		&& Objects.equals(type, other.type) 
        		&& Objects.equals(position, other.position);
        }
        return false;
    }
    
    /**
	 * Faz a geração do hash code a partir do cálculo efetuado pela classe pai e
	 * adiciona a regra do cálculo os valores das propriedades
	 * {@code reportage}, {@code type} e {@code position}.
	 */
    @Override
    public int hashCode() {
    	return Objects.hash(reportage, type, position);
    }
    
    /**
	 * Seguindo as regras definidas por {@link Comparable#compareTo(Object)},
	 * faz a comparação baseada no valor da propriedade {@code position}.
	 * 
	 * @return Um inteiro negativo, zero ou um inteiro positivo se esse objeto
	 *         for menor que, igual ou maior que o objeto especificado.
	 */
    @Override
    public int compareTo(ReportageSection other) {
        return position - other.position;
    }
    
    @Override
    public String toString() {
        return "ReportageSection[id=" + id 
        					+ ", type=" + type 
        					+ ", position=" + position 
        					+ ", reportage_id=" + (reportage == null ? null : reportage.getId()) 
        					+ ", content=" + content 
        					+ ", observation=" + observation 
        		+ "]";
    }

    public void addSubSection(ReportageSubSection subSection) {
        subSection.setReportageSection(this);
        subSection.setOrder(subSections.size());
        subSections.add(subSection);
    }
    
    //--------------------------------------------------------------------------
    //	Getters & Setters
    //--------------------------------------------------------------------------
    
    /**
     * Retorna o conteúdo textual dessa seção.
     * 
     * @return o conteúdo da seção
     */
    public String getContent() {
	    return content;
    }
    
    /**
     * Define o conteúdo textual dessa seção.
     * 
     * @param content conteúdo da seção
     */
    public void setContent(String content) {
		this.content = content;
    }
    
    /**
     * Retorna as observações que podem ter sido atribuidas a essa seção.
     * 
     * @return as observações da seção
     */
    public String getObservation() {
	    return observation;
    }
    
    /**
     * Define quaisquer observações sobre essa seção.
     * 
     * @param observation observações da seção
     */
    public void setObservation(String observation) {
	    this.observation = observation;
    }
    
    /**
	 * Retorna a posição em que essa seção deve permanecer dentro da ordenação
	 * das seções da reportagem.
	 * 
	 * @return a posição da seção na lista
	 */
    public int getPosition() {
	    return position;
    }
    
    /**
     * Define a posição da reportagem na ordem de listagem das seções.
     * 
     * @param position posição dessa seção
     */
    public void setPosition(int position) {
	    this.position = position;
    }
    
    /**
     * Retorna a reportagem a qual essa seção pertence.
     * 
     * @return a reportagem dona dessa seção
     */
    public Reportage getReportage() {
	    return reportage;
    }
    
    /**
     * Define a reportagem a qual essa seção pertence.
     * 
     * @param reportage Reportagem na qual essa seção está inserida.
     */
    public void setReportage(Reportage reportage) {
	    this.reportage = reportage;
    }
    
    /**
	 * Retorna o tipo dessa seção, definido pelo {@code enum}
	 * {@link ReportageSectionType}.
	 * 
	 * @return o tipo dessa seção
	 */
    public ReportageSectionType getType() {
	    return type;
    }
    
    /**
	 * Define o tipo dessa seção, definido pelo {@code enum}
	 * {@link ReportageSectionType}.
	 * 
	 * @param type tipo dessa seção
	 */
    public void setType(ReportageSectionType type) {
		this.type = type;
    }
    
    /**
     * @return o tempo médio que o repórter leva para ler o texto dessa seção
     */
    public Date getReadTime() {
	    return readTime;
    }
    
    /**
	 * @param readTime tempo médio que o repórter leva para ler o texto dessa
	 *            seção
	 */
    public void setReadTime(Date readTime) {
	    this.readTime = readTime;
    }
    
    /**
	 * @return {@code true} se este item da reportagem já foi aprovado por seu
	 *         editor
	 */
    public boolean isOk() {
	    return ok;
    }
    
    /**
     * @param ok valor boleano informado se este item da reportagem foi aprovado
     */
    public void setOk(boolean ok) {
	    this.ok = ok;
    }

    public SortedSet<ReportageSubSection> getSubSections() {
        return subSections;
    }

    public void setSubSections(SortedSet<ReportageSubSection> subSections) {
        this.subSections = subSections;
    }

}
