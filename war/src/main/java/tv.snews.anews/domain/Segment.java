
package tv.snews.anews.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Entidade que Representa o Segmento das Instituição.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class Segment extends AbstractEntity<Integer> {

	private static final long serialVersionUID = -6151472002946966091L;

	private String name;
	private Set<Institution> institutions = new HashSet<Institution>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Institution> getInstitutions() {
		return institutions;
	}

	public void setInstitutions(Set<Institution> institutions) {
		this.institutions = institutions;
	}
}
