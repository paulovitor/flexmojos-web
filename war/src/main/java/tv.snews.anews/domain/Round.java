
package tv.snews.anews.domain;

import java.util.Date;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class Round extends AbstractEntity<Long> {

	private static final long serialVersionUID = -3422146376249407471L;

	private String slug;
	private String address;
	private String information;
	private Date date;
	private Institution institution;
	private User user;
	private Contact contact;
	private transient User editingUser; // não é persistido

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}
	
	public User getEditingUser() {
		return editingUser;
	}

	public void setEditingUser(User editingUser) {
		this.editingUser = editingUser;
	}
}
