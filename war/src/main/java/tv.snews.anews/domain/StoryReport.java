package tv.snews.anews.domain;

import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;

/**
 * Representa um relatório escrito por um usuário do sistema reportando um
 * evento relacionado a uma lauda.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
@Entity
@Indexed(index = "indexes/storyreport")
public class StoryReport extends Report {

	private Story story;
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public Story getStory() {
	    return story;
    }
    
    public void setStory(Story story) {
	    this.story = story;
    }
    
    private static final long serialVersionUID = 2695287201777845000L;
}
