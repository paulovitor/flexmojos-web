package tv.snews.anews.domain;

import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;

import javax.persistence.Entity;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
@Entity
@Indexed(index = "indexes/program")
public class Program extends AbstractEntity<Integer> implements Comparable<Program> {

	private static final long serialVersionUID = 5758423845640252956L;

	@Field
	private String name;

	private Boolean disabled;
	private Date start;
	private Date end;

	private Device cgDevice;
	private Device playoutDevice;

	private IIDevicePath cgPath;

	private User defaultPresenter;

	private Set<User> presenters = new HashSet<>();
	private Set<User> editors = new HashSet<>();
	private Set<User> imageEditors = new HashSet<>();

	//--------------------------------------------------------------------------
	//	Operations
	//--------------------------------------------------------------------------

	
	/**
	 * Retorna o primeiro editor dentro da lista de editores deste programa.
	 *
	 * @return o primeiro editor ou {@code null} se não houver editores
	 */
	public User findFirstEditor() {
		if (editors.isEmpty()) {
			return null;
		} else {
			return editors.iterator().next();
		}
	}

	@Override
	public int compareTo(Program other) {
		/*
		 * Isso é usado pelo GuidelineDaoImpl para ordernar as pautas. Isso é
		 * necessário para que as pautas sejam agrupadas por programa de acordo
		 * com o horário de execução.
		 *
		 * Trata o caso de ter dois programas com o mesmo horário de execução,
		 * apesar de que isso não faz sentido nenhum.
		 */

		int result = start.compareTo(other.start);
		return result == 0 ? name.compareToIgnoreCase(other.name) : result;
	}

	//--------------------------------------------------------------------------
	//	Getters & Setters
	//--------------------------------------------------------------------------

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start == null ? getMidnightTime() : addTimeToCurrentDate(start);
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end == null ? getMidnightTime() : addTimeToCurrentDate(end);
	}

	public Set<User> getPresenters() {
		return presenters;
	}

	public void setPresenters(Set<User> presenters) {
		this.presenters = presenters;
	}

	public User getDefaultPresenter() {
		return defaultPresenter;
	}

	public void setDefaultPresenter(User defaultPresenter) {
		this.defaultPresenter = defaultPresenter;
	}

	public Set<User> getEditors() {
		return editors;
	}

	public void setEditors(Set<User> editors) {
		this.editors = editors;
	}

	public Device getCgDevice() {
		return cgDevice;
	}

	public void setCgDevice(Device cgDevice) {
		this.cgDevice = cgDevice;
	}

	public IIDevicePath getCgPath() {
		return cgPath;
	}

	public void setCgPath(IIDevicePath cgPath) {
		this.cgPath = cgPath;
	}

	public Device getPlayoutDevice() {
		return playoutDevice;
	}

	public void setPlayoutDevice(Device playoutDevice) {
		this.playoutDevice = playoutDevice;
	}

    public Set<User> getImageEditors() {
        return imageEditors;
    }

    public void setImageEditors(Set<User> imageEditors) {
        this.imageEditors = imageEditors;
    }
}
