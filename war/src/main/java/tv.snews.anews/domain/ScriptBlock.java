package tv.snews.anews.domain;

import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import tv.snews.anews.util.ResourceBundle;

import javax.persistence.Entity;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.apache.commons.collections.CollectionUtils.get;
import static tv.snews.anews.util.DateTimeUtil.addTimeToCurrentDate;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
@Entity
@Indexed(index = "indexes/scriptblock")
public class ScriptBlock extends AbstractEntity<Long> implements Comparable<ScriptBlock> {

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	private int order;
	private String slug;
	private boolean standBy = false;
	private Date commercial;

	@IndexedEmbedded
	private ScriptPlan scriptPlan;

	@ContainedIn
	private SortedSet<Script> scripts = new TreeSet<>();

    //------------------------------
    //	Constructors
    //------------------------------

	public ScriptBlock() {}

	public ScriptBlock(ScriptPlan scriptPlan) {
		this.scriptPlan = scriptPlan;
		this.commercial = getMidnightTime();
	}

    public ScriptBlock (ScriptBlock scriptBlock, ScriptPlan scriptPlan) throws IOException {
        this.order = scriptBlock.getOrder();
        this.slug = scriptBlock.getSlug();
        this.standBy = scriptBlock.isStandBy();
        this.commercial = scriptBlock.getCommercial();
        this.scriptPlan = scriptPlan;

        copyScripts(scriptBlock);
    }

	//------------------------------
	//	Operations
	//------------------------------

	public void copyScripts(ScriptBlock target) throws IOException {
		for (Script script : target.getScripts()) {
            addScript(new Script(script));
		}
	}

    public void addScript(Script script) {
        addScript(script, scripts.size());
    }

    public Script createScript(User author) {
		Script script = new Script(author);
        addScript(script);
		return script;
	}

	public Script createScript(Guideline guideline, User author, int order) {
		Script script = new Script(guideline, author, order);
		addScript(script, order);
		return script;
	}

	public Script createScript(Reportage reportage, User author, int order) {
		Script script = new Script(reportage, author, order);
		addScript(script, order);
		return script;
	}

	public void addScript(Script script, int order) {
		addScript(script, order, false);
	}

	public void addScript(Script script, int order, boolean keepPage) {
		for (int i = scripts.size() - 1; i >= order; i--) {
			Script current = (Script) get(scripts, i);
			current.setOrder(current.getOrder() + 1);
		}

		if (!keepPage) {
			script.setPage(scriptPlan.nextPage());
		}

		script.setBlock(this);
		script.setOrder(order);
		scripts.add(script);
	}

	public void removeScript(Script script) {
		Iterator<Script> it = scripts.iterator();

		while (it.hasNext()) {
			Script current = it.next();
			if (current.equals(script)) {
				it.remove();
				break;
			}
		}

		// Corrige o espaço vago nos indexes
		int oldOrder = script.getOrder();
		for (int i = oldOrder; i < scripts.size(); i++) {
			Script current = (Script) get(scripts, i);
			current.setOrder(i);
		}
	}

	public SortedSet<Script> approvedScripts() {
		SortedSet<Script> result = new TreeSet<>();

		for (Script script : scripts) {
			if (script.isApproved()) {
				result.add(script);
			}
		}
		return result;
	}

	@Override
	public int compareTo(ScriptBlock other) {
		if (standBy && !other.standBy) {
			return -1;
		}
		if (!standBy && other.standBy) {
			return 1;
		}
		return order - other.order;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = standBy ? bundle.getMessage("scriptblock.standBy") : slug;
	}

	public boolean isStandBy() {
		return standBy;
	}

	public void setStandBy(boolean standBy) {
		this.standBy = standBy;
	}

	public Date getCommercial() {
		return commercial;
	}

	public void setCommercial(Date commercial) {
		this.commercial = commercial == null ? getMidnightTime() : addTimeToCurrentDate(commercial);
	}

	public ScriptPlan getScriptPlan() {
		return scriptPlan;
	}

	public void setScriptPlan(ScriptPlan scriptPlan) {
		this.scriptPlan = scriptPlan;
	}

	public SortedSet<Script> getScripts() {
		return scripts;
	}

	public void setScripts(SortedSet<Script> scripts) {
		this.scripts = scripts;
	}

	private static final long serialVersionUID = 3286543043976388104L;
}
