package tv.snews.anews.domain;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.apache.commons.lang.StringUtils.isNotBlank;

public class ScriptCG extends AbstractScriptItem {

	private static final long serialVersionUID = 7628449542413208342L;

	private int code;
	private int templateCode;
	private boolean comment = false;

	private SortedSet<ScriptCGField> fields = new TreeSet<>();

	//------------------------------
	//	Constructors
	//------------------------------

	public ScriptCG() {
		super();
	}

	public ScriptCG(ScriptCG original) {
		this.code = original.getCode();
		this.templateCode = original.getTemplateCode();
		this.comment = original.isComment();
		copyFields(original);
	}

	private void copyFields(ScriptCG original) {
		for (ScriptCGField field : original.getFields()) {
			addField(new ScriptCGField(field));
		}
	}

	private void addField(ScriptCGField field) {
		field.setScriptCG(this);
		this.fields.add(field);
	}

	public boolean isFilled() {
		for (ScriptCGField field : fields) {
			if (isNotBlank(field.getValue())) {
				return true;
			}
		}
		return false;
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(int templateCode) {
		this.templateCode = templateCode;
	}

	public boolean isComment() {
		return comment;
	}

	public void setComment(boolean comment) {
		this.comment = comment;
	}

	public SortedSet<ScriptCGField> getFields() {
		return fields;
	}

	public void setFields(SortedSet<ScriptCGField> fields) {
		this.fields = fields;
	}
}
