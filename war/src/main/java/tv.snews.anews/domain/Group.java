package tv.snews.anews.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Entidade que representa os grupos de acesso ao ANews.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class Group extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 2556914040088608988L;

	private String name;
	private boolean showAsReporters;
	private boolean showAsProducers;
    private boolean showAsChecklistProducers;
	private boolean showAsEditors;
	
	private Set<Permission> permissions = new HashSet<Permission>();
    private Set<User> users = new HashSet<User>();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Permission> permissions) {
		this.permissions = permissions;
	}

    /**
     * @return the showAsReporters
     */
    public boolean isShowAsReporters() {
    	return showAsReporters;
    }

    /**
     * @param showAsReporters the showAsReporters to set
     */
    public void setShowAsReporters(boolean showAsReporters) {
    	this.showAsReporters = showAsReporters;
    }
	
    /**
     * @return the showAsProducers
     */
    public boolean isShowAsProducers() {
    	return showAsProducers;
    }

    /**
     * @param showAsProducers the showAsProducers to set
     */
    public void setShowAsProducers(boolean showAsProducers) {
    	this.showAsProducers = showAsProducers;
    }
	
    public boolean isShowAsEditors() {
    	return showAsEditors;
    }
	
    public void setShowAsEditors(boolean showAsEditors) {
    	this.showAsEditors = showAsEditors;
    }

    public boolean isShowAsChecklistProducers() {
        return showAsChecklistProducers;
    }

    public void setShowAsChecklistProducers(boolean showAsChecklistProducers) {
        this.showAsChecklistProducers = showAsChecklistProducers;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}