package tv.snews.anews.domain;

/**
 * Indica os tipos de seções que podem ser atribuidas em uma reportagem.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public enum ReportageSectionType {

	/*
	 * IMPORTANTE: no banco esse enum está sendo salvo como um int, que é o
	 * padrão do Hibernate. Isso não tem problema mas tem que ter cuidado ao
	 * adicionar ou remover valores nesse enum.
	 * 
	 * Acredito que se você adicionar um valor na primeira posição, isso vai
	 * alterar os valores dos outros que já existem, o que vai deixar de
	 * corresponder com o valor do banco.
	 */
	
	/**
	 * Passagem: é o momento em que o repórter aparece na matéria.
	 */
	APPEARANCE,
	
	/**
	 * Arte: trecho de arte editada.
	 */
	ART,
	
	/**
	 * Encerramento: trecho final da matéria.
	 */
	CLOSURE, 
	
	/**
	 * Sonora: é um trecho em que é feito uma entrevista.
	 */
	INTERVIEW, 
	
	/**
	 * Off: trecho de imagens com áudio gravado.
	 */
	OFF,
	
	/**
	 * Abertura: seção que inicia a matéria.
	 */
	OPENING,
	
	/**
	 * Sobe o som: seção que inicia um video aumentando o som deixando-o em evidência.
	 */
	SOUNDUP;
}
