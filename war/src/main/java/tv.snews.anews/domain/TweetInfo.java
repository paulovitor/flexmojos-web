package tv.snews.anews.domain;

import java.util.Date;
import java.util.Objects;

/**
 * Classe que representa um tweet carregado da API do Twitter
 * dentro do sistema com as informações necessárias para efetuar
 * o controle de armazenamento deste tweet na base de dados.
 *
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TweetInfo extends AbstractEntity<Long> implements Comparable<TweetInfo> {

    private static final long serialVersionUID = 5115983358930205606L;

    private String tweetId;
    private String text;
    private Date createdAt;
    private TwitterUser twitterUser;

    @Override
    public int compareTo(TweetInfo other) {
        return createdAt.compareTo(other.createdAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tweetId);
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            return true;
        } else if (obj instanceof TweetInfo) {
            TweetInfo other = (TweetInfo) obj;
            return Objects.equals(tweetId, other.tweetId);
        }
        return false;
    }

    /**
     * @return the tweetId
     */
    public String getTweetId() {
        return tweetId;
    }

    /**
     * @param tweetId the tweetId to set
     */
    public void setTweetId(String tweetId) {
        this.tweetId = tweetId;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the createdAt
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt the createdAt to set
     */
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return the twitterUser
     */
    public TwitterUser getTwitterUser() {
        return twitterUser;
    }

    /**
     * @param twitterUser the twitterUser to set
     */
    public void setTwitterUser(TwitterUser twitterUser) {
        this.twitterUser = twitterUser;
    }
}