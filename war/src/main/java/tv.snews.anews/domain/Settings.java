package tv.snews.anews.domain;

import tv.snews.anews.util.LocaleUtils;

import javax.persistence.Transient;
import java.util.Locale;

/**
 * Entidade que representa as configurações do sistema.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class Settings extends AbstractEntity<Integer> {

	private static final long serialVersionUID = 2556914040088608988L;

	private static final String DEFAULT_LOCALE = "pt_BR";
	private static final String DEFAULT_GUIDELINE_GRID_SORT = "program";

	private String name;
	private String locale;
	private int passwordExpiration;
	private int sessionExpiration;

	// Place inside server used to store objects like images, texts, documents, etc.
	private String storageLocation;
	private String storageLocationBannerLogin;
	private String storageLocationSystemLogo;
	private String storageLocationReportLogo;

	private int cleaningRound;
	private int cleaningRss;
	private int cleaningTweets;
	private int cleaningGuidelinesTrash;
	private int cleaningStoriesTrash;
    private int cleaningChecklistsTrash;

	// Configurações da limpeza das versões
	private int cleaningReportageVersions;
	private int cleaningGuidelineVersions;
	private int cleaningStoryVersions;
	
	// Configurações do editor de texto
	private boolean editorUpperCase;
	private boolean editorSpellCheck;
    private int editorFontSize;
    private String editorFontFamily;

    //Ativa o streaming do anews para visualização dos videos.
    private boolean serverStreaming;
    private boolean otherHostActive;
    private String host;
    private String rootDirectory;
    private String protocol;

    //Ordenação padrão da grid de pautas
    private String defaultGuidelineGridSort;

    // Configurações dos relatórios
    private String reportFontFamily;
    private int reportFontSize;

	// Configurações das área
	private int colorAgency;
	private int colorAgenda;
	private int colorChecklist;
	
	private int colorGuideline;
	private int colorReporter;
	private int colorRundown;
    private int colorScript;
	private int colorPersonal;
	private int colorSettings;
	private int colorEdition;
	private int colorReport;
	private int colorMediaCenter;
	private int defaultColor;
	
	// Configurações das busca avancadas
	private boolean displayIndexAlert;

    private boolean enableGuidelineTeam;
    private boolean enableRundownLoadButton;
    private boolean enableRundownImageEditor;
    private boolean enableRundownAuthor;
    private boolean enableScriptPlanImageEditor;
    private boolean enableScriptPlanAuthor;
    private boolean enableScriptPlanLoadButton;

	private boolean includeStandBy;

	@Transient
	private Locale currentLocale;

	public Settings() {
		super();
	}

	public Settings(String name) {
		this(name, DEFAULT_LOCALE);
	}

	public Settings(String name, String locale) {
		this.name = name;
		this.locale = locale;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale == null ? DEFAULT_LOCALE : locale;
		setCurrentLocale(LocaleUtils.getLocaleFromString(this.locale));
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	public Locale getCurrentLocale() {
		return currentLocale;
	}

	public void setCurrentLocale(Locale currentLocale) {
		this.currentLocale = currentLocale;
	}

    public int getSessionExpiration() {
    	return sessionExpiration;
    }

    public void setSessionExpiration(int sessionExpiration) {
    	this.sessionExpiration = sessionExpiration;
    }
	
	public int getPasswordExpiration() {
		return passwordExpiration;
	}

	public void setPasswordExpiration(int passwordExpiration) {
		this.passwordExpiration = passwordExpiration;
	}

	public int getCleaningRound() {
		return cleaningRound;
	}

	public void setCleaningRound(int cleaningRound) {
		this.cleaningRound = cleaningRound;
	}

	public int getCleaningRss() {
		return cleaningRss;
	}

	public void setCleaningRss(int cleaningRss) {
		this.cleaningRss = cleaningRss;
	}

	public int getCleaningTweets() {
		return cleaningTweets;
	}

	public void setCleaningTweets(int cleaningTweets) {
		this.cleaningTweets = cleaningTweets;
	}

	public int getCleaningGuidelineVersions() {
		return cleaningGuidelineVersions;
	}

	public void setCleaningGuidelineVersions(int cleaningGuidelineVersions) {
		this.cleaningGuidelineVersions = cleaningGuidelineVersions;
	}

	public int getCleaningGuidelinesTrash() {
		return cleaningGuidelinesTrash;
	}

	public void setCleaningGuidelinesTrash(int cleaningGuidelinesTrash) {
		this.cleaningGuidelinesTrash = cleaningGuidelinesTrash;
	}

    public int getCleaningStoriesTrash() {
	    return cleaningStoriesTrash;
    }
    
    public void setCleaningStoriesTrash(int cleaningStoriesTrash) {
	    this.cleaningStoriesTrash = cleaningStoriesTrash;
    }
	
	public boolean isEditorUpperCase() {
		return editorUpperCase;
	}

	public void setEditorUpperCase(boolean useUpperCase) {
		this.editorUpperCase = useUpperCase;
	}

	public int getEditorFontSize() {
		return editorFontSize;
	}

	public void setEditorFontSize(int fontSize) {
		this.editorFontSize = fontSize;
	}

	public String getEditorFontFamily() {
		return editorFontFamily;
	}

	public void setEditorFontFamily(String fontFamily) {
		this.editorFontFamily = fontFamily;
	}

	public boolean isEditorSpellCheck() {
		return editorSpellCheck;
	}

	public void setEditorSpellCheck(boolean editorSpellCheck) {
		this.editorSpellCheck = editorSpellCheck;
	}

	public int getColorAgency() {
		return colorAgency;
	}

	public void setColorAgency(int colorAgency) {
		this.colorAgency = colorAgency;
	}

	public int getColorAgenda() {
		return colorAgenda;
	}

	public void setColorAgenda(int colorAgenda) {
		this.colorAgenda = colorAgenda;
	}

    public int getColorChecklist() {
    	return colorChecklist;
    }

    public void setColorChecklist(int colorChecklist) {
    	this.colorChecklist = colorChecklist;
    }
    
	public int getColorGuideline() {
		return colorGuideline;
	}

	public void setColorGuideline(int colorGuideline) {
		this.colorGuideline = colorGuideline;
	}

	public int getColorReporter() {
		return colorReporter;
	}

	public void setColorReporter(int colorReporter) {
		this.colorReporter = colorReporter;
	}

	public int getColorRundown() {
		return colorRundown;
	}

	public void setColorRundown(int colorRundown) {
		this.colorRundown = colorRundown;
	}

    public int getColorScript() {
        return colorScript;
    }

    public void setColorScript(int colorScript) {
        this.colorScript = colorScript;
    }

	public int getColorPersonal() {
		return colorPersonal;
	}

	public void setColorPersonal(int colorPersonal) {
		this.colorPersonal = colorPersonal;
	}
	
    public int getColorSettings() {
    	return colorSettings;
    }
	
    public void setColorSettings(int colorSettings) {
    	this.colorSettings = colorSettings;
    }
	
    public int getCleaningReportageVersions() {
    	return cleaningReportageVersions;
    }
	
    public void setCleaningReportageVersions(int cleaningReportageVersions) {
    	this.cleaningReportageVersions = cleaningReportageVersions;
    }

    public String getStorageLocationBannerLogin() {
    	return storageLocationBannerLogin;
    }

    public void setStorageLocationBannerLogin(String storageLocationBannerLogin) {
    	this.storageLocationBannerLogin = storageLocationBannerLogin;
    }
    
    public String getStorageLocationSystemLogo() {
    	return storageLocationSystemLogo;
    }

    public void setStorageLocationSystemLogo(String storageLocationSystemLogo) {
    	this.storageLocationSystemLogo = storageLocationSystemLogo;
    }

    public String getStorageLocationReportLogo() {
    	return storageLocationReportLogo;
    }

    public void setStorageLocationReportLogo(String storageLocationReportLogo) {
    	this.storageLocationReportLogo = storageLocationReportLogo;
    }

    public int getColorEdition() {
    	return colorEdition;
    }

    public void setColorEdition(int colorEdition) {
    	this.colorEdition = colorEdition;
    }
    
    public int getColorReport() {
	    return colorReport;
    }
    
    public void setColorReport(int colorReport) {
	    this.colorReport = colorReport;
    }

    public int getColorMediaCenter() {
    	return colorMediaCenter;
    }

    public void setColorMediaCenter(int colorMediaCenter) {
    	this.colorMediaCenter = colorMediaCenter;
    }
    
    public int getCleaningStoryVersions() {
	    return cleaningStoryVersions;
    }
    
    public void setCleaningStoryVersions(int cleaningStoryVersions) {
	    this.cleaningStoryVersions = cleaningStoryVersions;
    }
    
    public int getDefaultColor() {
    	return defaultColor;
    }
	
    public void setDefaultColor(int defaultColor) {
    	this.defaultColor = defaultColor;
    }

    public int getCleaningChecklistsTrash() {
        return cleaningChecklistsTrash;
    }

    public void setCleaningChecklistsTrash(int cleaningChecklistsTrash) {
        this.cleaningChecklistsTrash = cleaningChecklistsTrash;
    }

    public String getDefaultGuidelineGridSort() {
        return defaultGuidelineGridSort;
    }

    public void setDefaultGuidelineGridSort(String defaultGuidelineGridSort) {
        this.defaultGuidelineGridSort = defaultGuidelineGridSort == null ? DEFAULT_GUIDELINE_GRID_SORT : defaultGuidelineGridSort;
    }

    public String getReportFontFamily() {
        return reportFontFamily;
    }

    public void setReportFontFamily(String reportFontFamily) {
        this.reportFontFamily = reportFontFamily;
    }

    public int getReportFontSize() {
        return reportFontSize;
    }

    public void setReportFontSize(int reportFontSize) {
        this.reportFontSize = reportFontSize;
    }
	
    public boolean isDisplayIndexAlert() {
    	return displayIndexAlert;
    }

	
    public void setDisplayIndexAlert(boolean displayIndexAlert) {
    	this.displayIndexAlert = displayIndexAlert;
    }

    public boolean isServerStreaming() {
        return serverStreaming;
    }

    public void setServerStreaming(boolean serverStreaming) {
        this.serverStreaming = serverStreaming;
    }

    public boolean isOtherHostActive() {
        return otherHostActive;
    }

    public void setOtherHostActive(boolean otherHostActive) {
        this.otherHostActive = otherHostActive;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getRootDirectory() {
        return rootDirectory;
    }

    public void setRootDirectory(String rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public boolean isEnableGuidelineTeam() {
        return enableGuidelineTeam;
    }

    public void setEnableGuidelineTeam(boolean enableGuidelineTeam) {
        this.enableGuidelineTeam = enableGuidelineTeam;
    }

	public boolean isEnableRundownLoadButton() {
		return enableRundownLoadButton;
	}

	public void setEnableRundownLoadButton(boolean enableRundownLoadButton) {
		this.enableRundownLoadButton = enableRundownLoadButton;
	}

	public boolean isEnableRundownImageEditor() {
		return enableRundownImageEditor;
	}

	public void setEnableRundownImageEditor(boolean enableRundownImageEditor) {
		this.enableRundownImageEditor = enableRundownImageEditor;
	}

	public boolean isEnableRundownAuthor() {
		return enableRundownAuthor;
	}

	public void setEnableRundownAuthor(boolean enableRundownAuthor) {
		this.enableRundownAuthor = enableRundownAuthor;
	}

    public boolean isEnableScriptPlanImageEditor() {
        return enableScriptPlanImageEditor;
    }

    public void setEnableScriptPlanImageEditor(boolean enableScriptPlanImageEditor) {
        this.enableScriptPlanImageEditor = enableScriptPlanImageEditor;
    }

    public boolean isEnableScriptPlanAuthor() {
        return enableScriptPlanAuthor;
    }

    public void setEnableScriptPlanAuthor(boolean enableScriptPlanAuthor) {
        this.enableScriptPlanAuthor = enableScriptPlanAuthor;
    }

    public boolean isEnableScriptPlanLoadButton() {
        return enableScriptPlanLoadButton;
    }

    public void setEnableScriptPlanLoadButton(boolean enableScriptPlanLoadButton) {
        this.enableScriptPlanLoadButton = enableScriptPlanLoadButton;
    }

	public boolean isIncludeStandBy() {
		return includeStandBy;
	}

	public void setIncludeStandBy(boolean includeStandBy) {
		this.includeStandBy = includeStandBy;
	}

}
