package tv.snews.anews.domain;

/**
 * Representa o tipo de retorno que a função que verifica os roteiros pode
 * retornar
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */

public enum ScriptPlanStatus {
	EXIST, NOT_EXIST, CANT_CREATE;
}