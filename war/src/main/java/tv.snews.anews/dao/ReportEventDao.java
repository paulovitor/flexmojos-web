package tv.snews.anews.dao;

import tv.snews.anews.domain.ReportEvent;

import java.util.List;

/**
 * Interface que define os métodos necessários para uma implementação do DAO 
 * responsável pelas ações relacionadas à persistência de objetos do tipo 
 * {@link tv.snews.anews.domain.ReportEvent}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportEventDao extends GenericDao<ReportEvent> {

	/**
	 * Lista todos os eventos de relatório cadastrados pelos usuários.
	 * 
	 * @return a lista de eventos cadastrados
	 */
	List<ReportEvent> findAll();
}
