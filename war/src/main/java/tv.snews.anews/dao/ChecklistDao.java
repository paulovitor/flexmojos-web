package tv.snews.anews.dao;

import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.search.ChecklistParams;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public interface ChecklistDao extends GenericDao<Checklist> {

	Checklist get(Serializable id);

	int countAllByType(ChecklistType type);

	int countAllByResource(ChecklistResource checklistResource);

	List<Checklist> findAllExcludedBefore(Date date, int maxResults);

	List<Checklist> findAllNotExcludedOfDate(Date date, boolean notDone);

	int countByParams(ChecklistParams params);

	List<Checklist> findByParams(ChecklistParams params);

	List<Checklist> findByParams(ChecklistParams params, int maxResult);

	List<Checklist> listExcludedPage(int pageNumber);

	int countExcluded();

	Checklist findByGuideline(Guideline guideline);

}
