package tv.snews.anews.dao;

import tv.snews.anews.domain.MosDeviceChannel;
import tv.snews.anews.domain.ScriptVideo;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface ScriptVideoDao extends GenericDao<ScriptVideo> {

    int countByChannel(MosDeviceChannel channel);
}
