package tv.snews.anews.dao;

import tv.snews.anews.domain.RssFeed;

import java.util.List;

/**
 * Interface que define os métodos necessários para as operações de
 * persistências de objetos RssFeed.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssFeedDao extends GenericDao<RssFeed> {

	/**
	 * Procura por um {@link RssFeed} cadastrado que possua a <code>url</code>
	 * informada.
	 * 
	 * @param url URL do feed.
	 * @return Feed encontrado ou <code>null</code> caso não encontre nenhum.
	 */
	RssFeed findByUrl(String url);
	
	/**
	 * Retorna a lista de todos os {@link RssFeed} cadastrados.
	 * 
	 * @return Feeds cadastrados.
	 */
	List<RssFeed> listAll();
}
