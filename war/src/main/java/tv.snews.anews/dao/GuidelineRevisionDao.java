package tv.snews.anews.dao;

import tv.snews.anews.domain.GuidelineRevision;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public interface GuidelineRevisionDao extends GenericDao<GuidelineRevision> {

	/**
	 * Retorna as revisões de pauta criadas anteriormente à data informada. Não
	 * será retornada uma quantidade de registros superior ao valor definido por
	 * {@code maxResults}.
	 * 
	 * @param maximumDate data limite das revisões
	 * @param maxResults quantidade máxima de registros a serem retornados
	 * @return a lista de revisões encontradas (se não existir nenhuma, uma
	 *         lista vazia será retornada)
	 */
	List<GuidelineRevision> listDateLessThan(Date date, int maxResults);
}
