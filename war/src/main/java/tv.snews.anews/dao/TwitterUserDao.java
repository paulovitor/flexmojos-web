package tv.snews.anews.dao;

import tv.snews.anews.domain.TwitterUser;

import java.util.List;

/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public interface TwitterUserDao extends GenericDao<TwitterUser> {

	/**
	 * Método responsável por retornar a lista de twitters cadastrados no sistema.
	 * @return Lista de twitters. 
	 */
	List<TwitterUser> listAll();

	/**
	 * Busca um usuário do twitter pelo ScreenName na base de dados.
	 *  
     * @param screenName
     * @return objeto TwitterUser ou null se não encontrar
     */
    TwitterUser findTwitterUserByScreenName(String screenName);
	
}
