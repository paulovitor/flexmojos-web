package tv.snews.anews.dao;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.ScriptPlan;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 */
public interface ScriptPlanDao extends GenericDao<ScriptPlan> {

	int countByDateAndProgram(Date date, Program program);

	ScriptPlan loadById(Serializable id);

	ScriptPlan loadFull(Serializable id, Serializable blockId);

	ScriptPlan findByDateAndProgram(Date date, Program program);

	List<ScriptPlan> findAllByDateGreaterOrEqualsTo(Date date);
}
