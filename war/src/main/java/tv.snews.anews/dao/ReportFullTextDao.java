package tv.snews.anews.dao;

import tv.snews.anews.domain.Report;
import tv.snews.anews.search.ReportParams;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public interface ReportFullTextDao {

	int countByParams(ReportParams params);
	
	List<Report> findByParams(ReportParams params);
}
