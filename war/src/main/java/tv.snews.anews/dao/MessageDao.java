package tv.snews.anews.dao;

import tv.snews.anews.domain.AbstractMessage;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;

import java.util.Date;
import java.util.List;

/**
 * Interface que define os métodos que devem ser implementados pelos DAOs que
 * manipulam os dados das mensagens do sistema.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface MessageDao extends GenericDao<AbstractMessage> {

	/**
	 * Retorna a lista de mensagens recebidas pelo usuário que estão marcadas
	 * como não lidas.
	 * 
	 * @param user Define o destinatário desejado.
	 * @return Lista das mensagens encontradas.
	 */
	List<AbstractMessage> listUnreadedMessages(User recipient);
	
	List<AbstractMessage> listLastMessagesFrom(Chat chat, int limit, Date firstMessageChatResult);

	List<AbstractMessage> listMessagesByChat(Chat chat);
	
	List<AbstractMessage> listMessagesOlderThan(Date date, int limit);
	
	AbstractMessage getLastMessageOfChat(Chat chat);
}
