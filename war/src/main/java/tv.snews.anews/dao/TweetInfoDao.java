package tv.snews.anews.dao;

import tv.snews.anews.domain.TweetInfo;
import tv.snews.anews.domain.TwitterUser;

import java.util.Date;
import java.util.List;


/**
 * Interface que define os métodos que devem ser implementados pelos DAOs que
 * manipulam os dados dos tweets no sistema.
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public interface TweetInfoDao extends GenericDao<TweetInfo> {
	
	/**
	 * Faz a contagem de todos os {@link TweetInfo} cadastrados.
	 * 
	 * @return Quantidade encontrada.
	 */
	int countAll();
	
	/**
     * @param twitterUser
     * @return
     */
    int countFromTwitterUser(TwitterUser twitterUser);
	
	
	/**
	 * Retorna, de forma páginada, os registros de {@link TweetInfo} persistidos
	 * do {@link TwitterUser} informado. 
	 * 
	 * @param firstResult Ponto inicial da página.
	 * @param maxResult Quantidade máxima que pode ser retornada.
	 * @return Lista de registros encontrados.
	 */
    List<TweetInfo> listPage(int firstResult, int maxResult);

    
    /**
	 * Retorna, de forma páginada, os registros de {@link TweetInfo} persistidos
	 * do {@link TwitterUser} informado. 
	 * 
	 * @param twitterUser Usuário do twitter para os tweets serem retornados.
	 * @param firstResult Ponto inicial da página.
	 * @param maxResult Quantidade máxima que pode ser retornada.
	 * @return Lista de registros encontrados.
	 */
    List<TweetInfo> listPage(TwitterUser twitterUser, int firstResult, int maxResult);

    /**
	 * Retorna o TweetInfo mais recente armazenado de um twitter User.
	 * 
     * @param twitterUser
     * @return 
     */
    TweetInfo getLastTweetInfo(TwitterUser twitterUser);

    /**
	 * Retorna uma lista de {@link TweetInfo} que possuem a data de publicação 
	 * anterior a data informada pelo parâmetro <code>minDate</code>, retornando 
	 * no máximo a quantidade definida pelo parâmetro <code>maxResult</code>.
	 * 
	 * @param minDate Data mínima de publicação.
	 * @param maxResult Quantidade máxima de registros a serem retornados.
	 * @return
	 */
    List<TweetInfo> listItemsOlderThan(Date minDate, int maxResults);

	/**
     * @param firstResult
     * @param maxResults
     * @return
     */
    List<TweetInfo> listTweets(int firstResult, int maxResults);

}