package tv.snews.anews.dao;

import tv.snews.anews.domain.*;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface DeviceDao extends GenericDao<Device> {

	List<Device> findAll();

	List<IIDevice> findAllIIDevices();

	List<MosDevice> findAllMosDevices();

	List<WSDevice> findAllWSDevices();

	List<Device> findAllByType(DeviceType type);

	List<MosDevice> findAllMosDeviceByType(DeviceType type);
}
