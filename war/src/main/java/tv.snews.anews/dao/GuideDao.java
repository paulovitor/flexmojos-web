
package tv.snews.anews.dao;

import tv.snews.anews.domain.Guide;

import java.util.List;

/**
 * Interface da persistência do roteiro.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface GuideDao extends GenericDao<Guide> {
	
	/**
	 * Método responsável por retornar a lista de roteiros cadastrados no sistema.
	 * 
	 * @return List<Guide> Lista de roteiros.
	 */
	List<Guide> listAll();

}
