package tv.snews.anews.dao;

import tv.snews.anews.domain.StoryCopyLog;

/**
 * @author Samuel Guedes de Melo
 */
public interface StoryCopyLogDao extends GenericDao<StoryCopyLog> {
}
