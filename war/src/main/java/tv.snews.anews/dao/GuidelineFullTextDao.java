package tv.snews.anews.dao;

import tv.snews.anews.domain.Guideline;
import tv.snews.anews.search.GuidelineParams;

import java.util.List;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.2.6.3
 */
public interface GuidelineFullTextDao {

    int total(GuidelineParams searchParams);

    List<Guideline> listByPage(GuidelineParams searchParams);

    List<Guideline> listTheFirstHundredByParams(GuidelineParams searchParams);

}
