package tv.snews.anews.dao;

import tv.snews.anews.domain.Settings;

/**
 * Interface de acesso aos dados e configurações da praça local.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface SettingsDao extends GenericDao<Settings> {

	/**
	 * Carrega as configurações únicas do sistema armazenadas no banco de dados.
	 * 
	 * @return as configurações salvas
	 */
	Settings loadSettings();

	/**
	 * Faz re-indexação completa dos dados do banco que podem ser indexados.
	 */
    void reindexDataBase();

    /**
     * Faz a optimização dos indexes existentes.
     */
    void optimizeIndexes();
}
