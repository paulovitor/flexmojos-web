package tv.snews.anews.dao;

import tv.snews.anews.domain.StoryKind;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa os tipos que a lauda deve
 * implementar.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.5
 */
public interface StoryKindDao extends GenericDao<StoryKind> {

	List<StoryKind> findAll();

	List<StoryKind> findAllOrderByAcronym();

    StoryKind findByNameOrAcronym(String name, String acronym);

    StoryKind getDefaultStoryKind();
}

