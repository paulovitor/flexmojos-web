package tv.snews.anews.dao;

import tv.snews.anews.domain.*;

import java.util.Date;
import java.util.List;

/**
 * Interface da persistência da pauta.
 *
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public interface GuidelineDao extends DocumentDao<Guideline> {

	int countExcluded();

	int countByProgram(Program program);

	int countByClassification(GuidelineClassification classification);

    int countByTeam(Team team);

	List<Guideline> findByDate(Date currentDay, Program program, DocumentState state);

	List<Guideline> findExcluded(int firstResult, int maxResults);

	List<Guideline> findExcludedBefore(Date minDate, int maxResults);

	List<Guideline> findByNews(News news);

	List<Guideline> findExcludedByPage(int pageNumber);

}
