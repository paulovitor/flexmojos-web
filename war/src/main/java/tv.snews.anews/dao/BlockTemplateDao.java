package tv.snews.anews.dao;

import tv.snews.anews.domain.BlockTemplate;

/**
 * @author Felipe Pinheiro
 * @since 1.6.0
 */
public interface BlockTemplateDao extends GenericDao<BlockTemplate> {
}
