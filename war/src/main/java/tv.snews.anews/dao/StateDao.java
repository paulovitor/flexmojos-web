
package tv.snews.anews.dao;

import tv.snews.anews.domain.State;

import java.util.List;

/**
 * Interface da persistência do estado.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface StateDao extends GenericDao<State> {

	/**
	 * Método responsável por retornar a lista de estados cadastrados no sistema.
	 * 
	 * @return List<State> Lista de estados.
	 */
	List<State> listAll();
}
