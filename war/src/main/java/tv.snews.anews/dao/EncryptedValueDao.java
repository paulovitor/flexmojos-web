package tv.snews.anews.dao;

import tv.snews.anews.util.EncryptedValue;

/**
 * {@code Data Access Object} para persistência e manipulação de objetos
 * {@link EncryptedValue}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public interface EncryptedValueDao extends GenericDao<EncryptedValue> {

	EncryptedValue findFirst();
}
