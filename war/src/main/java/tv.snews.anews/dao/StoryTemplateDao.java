package tv.snews.anews.dao;

import tv.snews.anews.domain.StoryTemplate;

/**
 * Interface que define os métodos de acesso e persistência aos registros das 
 * laudas do modelo de espelho.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
public interface StoryTemplateDao extends GenericDao<StoryTemplate> {
	
}
