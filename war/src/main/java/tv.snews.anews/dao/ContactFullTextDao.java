package tv.snews.anews.dao;

import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;

import java.util.List;


/**
 * 
 * @author fzapmello
 * @since 1.3.0
 */
public interface ContactFullTextDao {

	/**
	 * Retorna, de forma páginada, os registros de {@link Contact} persistidos.
	 * O registros serão ordenados pela propriedade <code>name</code>.
	 * 
	 * @param name Nome do contato 
	 * @param firstResult Ponto inicial da página.
	 * @param maxResult Quantidade máxima que pode ser retornada.
	 * @return Lista de registros encontrados.
	 */
	List<AgendaContact> findByNamePaginated(String name, int firstResult, int maxResult);
	

	
	/**
	 * Retorna o total de contatos atualmente armazenados pelo sistema filtrando pelo nome.
	 * 
     * @param name
     * @return Quantidade encontrada.
     */
    int countByName(String name);
}
