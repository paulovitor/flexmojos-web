package tv.snews.anews.dao;

import tv.snews.anews.domain.Company;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa as praças deve
 * implementar.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.3
 */
public interface CompanyDao extends GenericDao<Company> {
	List<Company> listAll();
	List<Company> listCompanyByNameHost(String name, String host);
}

