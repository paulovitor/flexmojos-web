package tv.snews.anews.dao;

import tv.snews.anews.domain.Segment;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa os segmentos deve
 * implementar.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface SegmentDao extends GenericDao<Segment> {
	List<Segment> listAll();
}
