package tv.snews.anews.dao;

import tv.snews.anews.domain.Group;
import tv.snews.anews.domain.Team;
import tv.snews.anews.domain.User;

import java.util.List;

/**
 * Estabelece um contrato de acesso aos dados do Usuário.
 *
 * @author Eliezer Reis.
 * @author Paulo Felipe.
 * @author Felipe Zap de Mello.
 * @since 1.0.0
 */
public interface UserDao extends GenericDao<User> {

	public static String NAME = "name";
	public static String NICKNAME = "nickname";

	User findByEmail(String email);

	User findByEmailAndPassword(String email, String password);

	User findByNickname(String nickname);

	List<User> listUsersByGroupAndStatus(Group group);

    List<User> listUsersByTeamAndStatus(Team team);

	List<User> listReporters();

	List<User> listProducers();

    List<User> listChecklistProducers();

	List<User> listEditors();

	List<User> listAll(String searchTerm, String sortProperty);

    List<User> listByGroupName(String searchTerm, Group group, Boolean belongsToTheGroup);

    List<User> listByTeamName(String searchTerm, Team team, Boolean belongsToTheTeam);
}
