
package tv.snews.anews.dao;

import tv.snews.anews.domain.ContactNumber;

/**
 * {@code Data Access Object} para persistência e manipulação de objetos
 * {@link ContactNumber}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public interface ContactNumberDao extends GenericDao<ContactNumber> {
	
}
