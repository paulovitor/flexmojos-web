package tv.snews.anews.dao;

import tv.snews.anews.domain.ContactGroup;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa os  grupo de contatos deve
 * implementar.
 * 
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public interface ContactGroupDao extends GenericDao<ContactGroup> {

	/**
	 * Retorna a lista de grupos de contatos cadastrados.
	 * 
	 * @return List<ContactGroup>
	 */
	List<ContactGroup> listAll();
	
	/**
	 * Procura por um registro cadastrado que possui o nome informado.
	 * 
	 * @param name nome procurado
	 * @return o registro encontrado ou {@code null} caso não encontre
	 */
	ContactGroup findByName(String name);
}
