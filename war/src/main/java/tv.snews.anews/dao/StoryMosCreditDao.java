package tv.snews.anews.dao;

import tv.snews.anews.domain.StoryMosCredit;

import java.util.List;


/**
 * 
 * @author snews
 * @since 1.3.0
 */
public interface StoryMosCreditDao extends GenericDao<StoryMosCredit> {

	public List<StoryMosCredit> getByMosMedia(Integer mosMediaId);
	
	
}
