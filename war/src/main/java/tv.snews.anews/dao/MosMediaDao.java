package tv.snews.anews.dao;

import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.search.MosMediaParams;

import java.util.List;

/**
 * Interface que define os métodos que uma implementação do DAO para os objetos 
 * {@link tv.snews.anews.domain.MosMedia} deve implementar.
 *
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
public interface MosMediaDao extends GenericDao<MosMedia> {

	/**
	 * Pesquisa pelo objId e pelo mosDevice.
	 *
     * @param objId
     * @return {@link tv.snews.anews.domain.MosMedia}
     */
    MosMedia findByObjectId(String objId, MosDevice mosDevice);
    
    /**
	 * Faz a contagem de todos os registros de mídia existentes no banco de 
	 * dados.
	 * 
	 * @param params parâmetros da busca
	 * @return a quantidade de mídias no banco de dados
	 */
	int countByParams(MosMediaParams params);
	
	/**
	 * Retorna uma página de registros de mídia existentes no banco de dados.
	 * <p>
	 * Será utilizado o tamanho de página padrão.
	 * </p>
	 * 
	 * @param params parâmetros da busca
	 * @return os registros encontrados na posição de página informada
	 */
	List<MosMedia> findByParams(MosMediaParams params);
	
	/**
	 * Retorna uma página de registros de mídia existentes no banco de dados.
	 * 
	 * @param params parâmetros da busca
	 * @param firstResult posição do primeiro registro a ser retornado
	 * @param maxResults quantidade máxima de registros que podem ser retornados
	 * @return os registros encontrados na posição de página informada
	 */
	List<MosMedia> findByParams(MosMediaParams params, int firstResult, int maxResults);

    int countByDevice(MosDevice mosDevice);

}
