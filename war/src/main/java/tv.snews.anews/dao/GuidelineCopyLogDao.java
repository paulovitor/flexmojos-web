package tv.snews.anews.dao;

import tv.snews.anews.domain.GuidelineCopyLog;

/**
 * @author Samuel Guedes de Melo
 */
public interface GuidelineCopyLogDao extends GenericDao<GuidelineCopyLog> {
}
