
package tv.snews.anews.dao;

import tv.snews.anews.domain.RssCategory;

import java.util.List;

/**
 * Interface que define os métodos necessários para o DAO que manipula objetos
 * {@link tv.snews.anews.domain.RssCategory}.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssCategoryDao extends GenericDao<RssCategory> {

	/**
	 * Procura por um registro de categoria que possua o nome informado.
	 *
	 * @param name
	 *            Nome da categoria procurada.
	 * @return Objeto {@link tv.snews.anews.domain.RssCategory} com os dados da categoria
	 *         encontrada ou <code>null</code> caso não encontre.
	 */
	RssCategory findByName(String name);

	/**
	 * Procura por um registro de categoria que possua o nome informado e que
	 * não seja a mesma categoria informada pelo segundo parâmetro.
	 *
	 * @param name
	 *            Nome da categoria procurada.
	 * @param category
	 *            Categoria a ser excluida da busca.
	 * @return Objeto {@link tv.snews.anews.domain.RssCategory} com os dados da categoria
	 *         encontrada ou <code>null</code> caso não encontre.
	 */
	RssCategory findByName(String name, RssCategory category);

	/**
	 * Lista todas as categorias cadastradas, ordenadas por ordem alfabética.
	 * 
     * @return Lista de categorias ordenadas.
     */
    List<RssCategory> list();

}
