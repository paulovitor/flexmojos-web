
package tv.snews.anews.dao;

import tv.snews.anews.domain.City;

import java.io.Serializable;
import java.util.List;

/**
 * Interface da persistência das cidades.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface CityDao extends GenericDao<City> {

	/**
	 * Método responsável por retornar a lista de cidades cadastrados no
	 * sistema.
	 * 
	 * @param idState
	 * @return List<City> Lista de cidades.
	 */
	List<City> listAll(Serializable idState);
}
