
package tv.snews.anews.dao;

import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.Rundown;

import java.io.Serializable;
import java.util.List;

/**
 * Interface da persistência do bloco do espelho.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface BlockDao extends GenericDao<Block> {
	
	List<Block> listAll(Rundown rundown);

    Block loadById(Serializable idBlock);
}
