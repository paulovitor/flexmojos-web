package tv.snews.anews.dao;

import tv.snews.anews.domain.Permission;

/**
 * Interface que define os métodos que um DAO que acessa as permissões deve
 * implementar.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface PermissionDao extends GenericDao<Permission> {
	/**
	 * Listar permissões.
	 * 
	 * @return List<Permission>
	 */
	
	Permission listAll();

}
