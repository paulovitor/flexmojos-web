package tv.snews.anews.dao;

import tv.snews.anews.domain.ChecklistType;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public interface ChecklistTypeDao extends GenericDao<ChecklistType> {

    List<ChecklistType> findAll();
}
