package tv.snews.anews.dao;

import tv.snews.anews.domain.*;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface ScriptDao extends DocumentDao<Script> {

    void update(ScriptMosCredit credit);

    void update(ScriptVideo video);

    Script nextApprovedScript(ScriptPlan scriptPlan, Script target);

    /**
     * Retorna a maior numeração de página entre os scripts de um roteiro. Os
     * scripts excluídos não serão incluídos no resultado.
     *
     * @param scriptPlan envolvido
     * @return maior página encontrada ou {@link null} caso não exista scripts no
     * roteiro informado
     */
    String getMaxPage(ScriptPlan scriptPlan);

    /**
     * Lista todas os scripts pertencentes a um bloco. Os scripts excluídos não
     * serão incluídos no resultado.
     *
     * @param block objeto do bloco dos scripts desejados
     * @return lista de scripts encontrados (se não existir nenhum, uma lista
     * vazia será retornada)
     */
    List<Script> listByBlock(ScriptBlock block);


    List<Script> listById(List<Long> idsOfScripts);

    List<Script> listById(Long[] idsOfScripts);

    /**
     * Faz a contagem de registros marcados com a flag de exclusão.
     *
     * @return a quantidade encontrada
     */
    int countExcludedScripts();

    /**
     * Retorna uma página de registros marcados como excluídos.
     *
     * @param pageNumber
     * @return lista de registros encontrados na posição informada
     */
    List<Script> listExcludedScripts(int pageNumber);

    int totalExcluded();
}
