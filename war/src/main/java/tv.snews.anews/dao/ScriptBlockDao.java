package tv.snews.anews.dao;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.ScriptBlock;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface ScriptBlockDao extends GenericDao<ScriptBlock> {

    ScriptBlock loadById(Serializable idScriptBlock, boolean fetchAll);
    ScriptBlock findLastStandByBlock(Program program, Date date);

}
