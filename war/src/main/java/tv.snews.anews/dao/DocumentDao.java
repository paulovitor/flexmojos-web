package tv.snews.anews.dao;

import tv.snews.anews.domain.Document;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.DocumentParams;

import java.io.Serializable;
import java.util.List;

/**
 * Interface que define os métodos necessários para uma implementação do DAO
 * responsável pelas ações relacionadas à persistência de objetos do tipo
 * {@link tv.snews.anews.domain.Document}.
 *
 * @author Felipe Zap de Mello
 * @since 1.6.4
 */
public interface DocumentDao<T extends Document> extends GenericDao<T> {

	Serializable save(T document, User author);

	void update(T document, User author);

	T merge(T document, User author);

	int countByParams(DocumentParams params, boolean drawerSearch);

	List<T> findByParams(DocumentParams params, boolean drawerSearch);

}
