package tv.snews.anews.dao;

import tv.snews.anews.infra.mail.MailConfig;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface MailConfigDao extends GenericDao<MailConfig> {

	/**
	 * Retorna o primeiro registro cadastrado na tabela {@code mail_config}, ou
	 * {@code null} caso não exista nenhum cadastrado.
	 * 
	 * @return o primeiro registro ou {@code null}
	 */
	MailConfig loadFirst();
}
