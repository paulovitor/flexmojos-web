package tv.snews.anews.dao;

import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;

public interface WSMediaDao extends GenericDao<WSMedia> {

    int countByDevice(WSDevice mosDevice);

}
