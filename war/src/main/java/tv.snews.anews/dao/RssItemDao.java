package tv.snews.anews.dao;

import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.domain.RssItem;

import java.util.Date;
import java.util.List;

/**
 * Interface que define os métodos que deverão ser implementados para efetuar e
 * acessar a persistência dos dados dos itens dos feeds RSS.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssItemDao extends GenericDao<RssItem> {

	/**
	 * Faz a contagem de todos os {@link RssItem} cadastrados.
	 * 
	 * @return Quantidade encontrada.
	 */
	int countAll();
	
	/**
	 * Faz a contagem de todos os {@link RssItem} cadastrados para todos os
	 * feeds da categoria informada.
	 * 
	 * @param category Categoria desejada.
	 * @return Quantidade encontrada.
	 */
	int countFromCategory(RssCategory category);
	
	/**
	 * Faz a contagem de todos os {@link RssItem} cadastrados para o feed
	 * informado.
	 * 
	 * @param feed {@link RssFeed} pai dos registros {@link RssItem} desejados.
	 * @return Quantidade encontrada.
	 */
	int countFromFeed(RssFeed feed);
	
	/**
	 * Retorna, de forma páginada, os registros de {@link RssItem} persistidos.
	 * O registros serão ordenados pela propriedade <code>published</code>, do
	 * mais novo para o mais antigo.
	 * 
	 * @param firstResult Ponto inicial da página.
	 * @param maxResult Quantidade máxima que pode ser retornada.
	 * @return Lista de registros encontrados.
	 */
	List<RssItem> listPage(int firstResult, int maxResult);
	
	/**
	 * Retorna, de forma páginada, os registros de {@link RssItem} persistidos
	 * para os feeds da categoria informada.
	 * O registros serão ordenados pela propriedade <code>published</code>, do
	 * mais novo para o mais antigo.
	 * 
	 * @param category Categoria desejada.
	 * @param firstResult Ponto inicial da página.
	 * @param maxResult Quantidade máxima que pode ser retornada.
	 * @return Lista de registros encontrados.
	 */
	List<RssItem> listPage(RssCategory category, int firstResult, int maxResult);
	
	/**
	 * Retorna, de forma páginada, os registros de {@link RssItem} persistidos
	 * do {@link RssFeed} informado. O registros serão ordenados pela
	 * propriedade <code>published</code>, do mais novo para o mais antigo.
	 * 
	 * @param feed Feed pai dos items a serem retornados.
	 * @param firstResult Ponto inicial da página.
	 * @param maxResult Quantidade máxima que pode ser retornada.
	 * @return Lista de registros encontrados.
	 */
	List<RssItem> listPage(RssFeed feed, int firstResult, int maxResult);
	
	/**
	 * Retorna uma lista de {@link RssItem} que possuem a data de publicação 
	 * anterior a data informada pelo parâmetro <code>minDate</code>, retornando 
	 * no máximo a quantidade definida pelo parâmetro <code>maxResult</code>.
	 * 
	 * @param minDate Data mínima de publicação.
	 * @param maxResult Quantidade máxima de registros a serem retornados.
	 * @return
	 */
	List<RssItem> listItemsOlderThan(Date minDate, int maxResult);
}
