
package tv.snews.anews.dao;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Rundown;

import java.util.Date;
import java.util.List;

/**
 * Interface da persistência do espelho.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface RundownDao extends GenericDao<Rundown> {
	
    Rundown findByDateAndProgram(Date date, Program program, boolean fetchAll);
	
	int count();
	
	int countRundown(Date date, Program program);

	/**
	 * Faz a contagem dos espelhos que foram criados para o programa informado.
	 * 
	 * @param program programa envolvido no processo
	 * @return a quantidade de espelhos encontrados
	 */
	int countRundownsOfProgram(Program program);
	
	/**
	 * Lista os espelhos cadastrados para o dia informado.
	 * 
	 * @param date dia desejado
	 * @return os espelhos encontradas
	 */
	List<Rundown> findAllByDateGreaterOrEqualsTo(Date date);
	
	List<Rundown> paginatedList(int firstResult, int maxResults);
}
