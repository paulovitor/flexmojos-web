package tv.snews.anews.dao;

import tv.snews.anews.domain.StoryRevision;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public interface StoryRevisionDao extends GenericDao<StoryRevision> {

	/**
	 * Retorna as revisões de lauda criadas anteriormente à data informada. Não
	 * será retornada uma quantidade de registros superior ao valor definido por
	 * {@code maxResults}.
	 * 
	 * @param maximumDate data limite das revisões
	 * @param maxResults quantidade máxima de registros a serem retornados
	 * @return a lista de revisões encontradas (se não existir nenhuma, uma
	 *         lista vazia será retornada)
	 */
	List<StoryRevision> listDateLessThan(Date date, int maxResults);
}
