package tv.snews.anews.dao;

import tv.snews.anews.domain.ReportageCopyLog;

/**
 * @author Felipe Pinheiro
 */
public interface ReportageCopyLogDao extends GenericDao<ReportageCopyLog> {
}
