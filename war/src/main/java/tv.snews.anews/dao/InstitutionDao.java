package tv.snews.anews.dao;

import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Segment;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa as instituições deve
 * implementar.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface InstitutionDao extends GenericDao<Institution> {
	List<Institution> listAll();
	
	List<Institution> listBySegment(Segment segment); 
}

