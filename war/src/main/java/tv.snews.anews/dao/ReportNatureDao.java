package tv.snews.anews.dao;

import tv.snews.anews.domain.ReportNature;

import java.util.List;

/**
 * Interface que define os métodos necessários para uma implementação do DAO 
 * responsável pelas ações relacionadas à persistência de objetos do tipo 
 * {@link ReportNature}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportNatureDao extends GenericDao<ReportNature> {

	/**
	 * Lista todos as naturezas de relatório cadastradas pelos usuários.
	 * 
	 * @return a lista de naturezas cadastradas
	 */
	List<ReportNature> findAll();
}
