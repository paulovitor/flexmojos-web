package tv.snews.anews.dao;

import tv.snews.anews.domain.MosMetadataPath;

/**
 * Interface que define o contrato do DAO para persistência de objetos
 * {@link MosMetadataPath}.
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public interface MosMetadataPathDao extends GenericDao<MosMetadataPath> {

}
