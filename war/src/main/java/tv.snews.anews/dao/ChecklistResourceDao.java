package tv.snews.anews.dao;

import tv.snews.anews.domain.ChecklistResource;

import java.util.List;

/**
 * Interface que define os métodos necessários para uma implementação do DAO 
 * responsável pelas ações relacionadas à persistência de objetos do tipo 
 * {@link tv.snews.anews.domain.ChecklistResource}.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.5
 */
public interface ChecklistResourceDao extends GenericDao<ChecklistResource> {

	/**
	 * Lista todos os recursos de produção cadastradas pelos usuários.
	 * 
	 * @return a lista de recursos de produção
	 */
	List<ChecklistResource> findAll();
}
