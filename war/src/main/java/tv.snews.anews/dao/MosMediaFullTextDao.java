package tv.snews.anews.dao;

import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.search.MosMediaParams;

import java.util.List;

/**
 * Interface que define os métodos que uma implementação do DAO para os objetos 
 * {@link tv.snews.anews.domain.MosMedia} deve implementar.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
public interface MosMediaFullTextDao {

    int countByParams(MosMediaParams params);

    List<MosMedia> findByParams(MosMediaParams params);

}
