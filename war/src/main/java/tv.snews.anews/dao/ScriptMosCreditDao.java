package tv.snews.anews.dao;

import tv.snews.anews.domain.ScriptMosCredit;

import java.util.List;


/**
 * 
 * @author snews
 * @since 1.3.0
 */
public interface ScriptMosCreditDao extends GenericDao<ScriptMosCredit>{

	public List<ScriptMosCredit> getByMosMedia(Integer mosMediaId);
	
}
