package tv.snews.anews.dao;

import tv.snews.anews.domain.Checklist;
import tv.snews.anews.search.ChecklistParams;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public interface ChecklistFullTextDao {

    int countByParams(ChecklistParams params);

    List<Checklist> findByParams(ChecklistParams params);

    List<Checklist> listTheFirstHundredByParams(ChecklistParams params);
}
