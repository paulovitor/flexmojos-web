package tv.snews.anews.dao;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownTemplate;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public interface RundownTemplateDao extends GenericDao<RundownTemplate> {

    List<RundownTemplate> findAllByProgram(Program program);

    int countByNameAndProgram(String name, Program program);
}
