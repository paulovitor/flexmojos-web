package tv.snews.anews.dao;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.Story;
import tv.snews.anews.search.StoryParams;

import java.util.Date;
import java.util.List;


/**
 * @author fzapmello
 * @since 1.2.6.3
 */
public interface StoryFullTextDao {

	/**
	 * Método utilizado para a listagem paginada dos registros da área de
	 * arquivo de laudas. Retorna a lista do registros já salvos, permitindo
	 * informar parâmetros opcionais de busca. Os parâmetros de busca que forem
	 * {@code null} serão ignorados.
	 *
	 * @return lista de registros encontrados de acordo com os parâmetros
	 * informados
	 */
	List<Story> listArchiveStories(String slug, String text, Program program, Date initialDate, Date finalDate, int pageNumber, SearchType searchType, String textIgnore);

	/**
	 * Método utilizado para calcular o total de registro da busca avançada.
	 */
	int total(String slug, String content, Program program, Date start, Date end, SearchType searchType, String textIgnore);

    List<Story> listTheFirstHundredByParams(StoryParams searchParams);
}
