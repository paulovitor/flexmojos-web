
package tv.snews.anews.dao;

import java.io.Serializable;

/**
 * Inteface generica de acesso a dados. Define os métodos básicos comuns a todos
 * os DAOs.
 * 
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 * @param <T>
 */
public interface GenericDao<T> {

	/**
	 * Tamanho padrão para as páginas das buscas.
	 */
	int DEFAULT_PAGE_SIZE = 50;

	/**
	 * Salva uma nova entidade no banco de dados. Se a entidade já existir
	 * retorna uma exceção.
	 * 
	 * @param entity
	 *            A entidade a ser persistida
	 * @return A entidade persistida
	 */
	Serializable save(T entity);

	/**
	 * Atualiza os dados da entidade no banco de dados.
	 * 
	 * @param entity
	 *            A entidade a ser atualizada
	 */
	void update(T entity);

	/**
	 * Apaga uma entidade do banco de dados.
	 * 
	 * @param entity
	 *            A entidade a ser apagada
	 */
	void delete(T entity);

	/**
	 * Retorna uma entidade através do seu identificador. Se a entidade não
	 * existir o retorno será nulo.
	 * 
	 * @param id
	 *            O identificador da entidade
	 * @return O objeto encontrado
	 */
	T get(Serializable id);

	T get(Serializable id, boolean fetchAll);

	/**
	 * Força o commit dos dados. Apos esse comando a sessão será finalizada.
	 */
	void flush();

	/**
	 * Remove um item da sessão atualmente aberta.
	 * 
	 * @param entity
	 */
	void evict(T entity);

	/**
	 * Atualiza um objeto com o estado atual dele no banco de dados, colocando-o
	 * no estado managed.
	 * 
	 * @param entity
	 */
	void refresh(T entity);
	
	/**
	 * Torna um objeto dettached em um objeto persistido porém o mantem dettached.
	 * A grande vantagem desse metodo que ele torna os objetos presentes na Entidade
	 * tracket, permitindo assim, deletar filhos orfãos - por exemplo.
	 * 
	 * @param entity
	 * @return O objeto persisto no banco com suas chaves corretamente preenchidas.
	 */
	T merge(T entity);
}
