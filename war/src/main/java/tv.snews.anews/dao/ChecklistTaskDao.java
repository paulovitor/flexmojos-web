package tv.snews.anews.dao;

import tv.snews.anews.domain.ChecklistTask;

/**
 * @author Samuel Guedes de Melo
 * @since 1.6.2
 */
public interface ChecklistTaskDao extends GenericDao<ChecklistTask> {

}
