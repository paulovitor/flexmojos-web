package tv.snews.anews.dao;

import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Round;

import java.util.Date;
import java.util.List;

/**
 * Entidade representativa de ronda.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface RoundDao extends GenericDao<Round> {
	
	/**
	 * Lista todas as rondas.
	 * @return List<Round>.
	 */
	List<Round> listAll(Date date);
	
	/**
	 * Lista todas as rondas mais velhas do que minDate.
	 * @param minDate data minima.
	 * @param maxResults limite para a busca.
	 * @return List<Round>.
	 */
	List<Round> listRoundOlderThan(Date minDate, int maxResults);
	
	/**
	 * Conta a quantidade de rondas ligadas a instituição informada.
	 * 
	 * @param institution instituição procurada
	 * @return a quantidade de rondas relacionadas à instituição
	 */
	int countByInstitution(Institution institution);
		
}
