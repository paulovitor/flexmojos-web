
package tv.snews.anews.dao;

import tv.snews.anews.domain.UserFile;

/**
 * {@code Data Access Object} para persistência e manipulação de objetos
 * {@link UserFile}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public interface UserFileDao extends GenericDao<UserFile> {
	
}
