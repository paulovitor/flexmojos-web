package tv.snews.anews.dao;

import tv.snews.anews.domain.GuidelineClassification;

import java.util.List;

/**
 * Interface que define os métodos necessários para uma implementação do DAO 
 * responsável pelas ações relacionadas à persistência de objetos do tipo 
 * {@link tv.snews.anews.domain.GuidelineClassification}.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.7
 */
public interface GuidelineClassificationDao extends GenericDao<GuidelineClassification> {

	/**
	 * Lista todos as classificações da pauta cadastradas pelos usuários.
	 * 
	 * @return a lista de classificações da pauta.
	 */
	List<GuidelineClassification> findAll();
}
