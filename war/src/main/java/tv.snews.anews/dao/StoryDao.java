package tv.snews.anews.dao;

import tv.snews.anews.domain.*;

import java.util.Date;
import java.util.List;

/**
 * Interface que define os métodos de acesso e persistência aos registros das 
 * laudas do espelhos.
 * 
 * @author Paulo Felipe
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface StoryDao extends DocumentDao<Story> {

	Story nextApprovedStory(Rundown rundown, Story target);

	Story previousApprovedStory(Rundown rundown, Story target);

	void update(StorySubSection subSection);

	/**
	 * Lista todas as laudas pertencentes a um bloco. As laudas excluídas não
	 * serão incluídas no resultado.
	 * 
	 * @param block objeto do bloco das laudas desejadas
	 * @return lista de laudas encontradas (se não existir nenhuma, uma lista
	 *         vazia será retornada)
	 */
	List<Story> listByBlock(Block block);
	
	/**
	 * Lista todas as laudas de uma {@link tv.snews.anews.domain.Rundown}. As laudas excluídas não
	 * serão incluídas no resultado.
	 *
	 * @param rundown objeto do espelho das laudas desejadas
	 * @return lista de laudas encontradas (se não existir nenhuma, uma lista
	 *         vazia será retornada)
	 */
	List<Story> listStoriesByRundown(Rundown rundown);

	List<Story> listDisplayingStories(Rundown rundown);

	/**
	 * Lista todas as laudas de uma {@link tv.snews.anews.domain.Rundown}, permitindo incluir somente
	 * as aprovadas e excluír as laudas do "Stand By".
	 * 
	 * @param rundown espelho de onde serão buscada as laudas
	 * @param onlyApproved se for {@code true} serão retornadas apenas as laudas
	 *            marcadas como aprovadas
	 * @param includeStandBy se for {@code true} as laudas do Stand By serão
	 *            incluídas no resulta
	 * @return a lista de laudas que se enquadram nos parâmetros da busca
	 */
	List<Story> listStoriesByRundown(Rundown rundown, boolean onlyApproved, boolean includeStandBy);

	/**
	 * Retorna as laudas excluídas anteriormente à data informada. Não será
	 * retornada uma quantidade de registros superior ao valor definido por
	 * {@code maxResults}.
	 * 
	 * @param maximumDate data limite do período de exclusão
	 * @param maxResults quantidade máxima de registros a serem retornados
	 * @return lista de laudas encontradas (se não existir nenhuma, uma lista
	 *         vazia será retornada)
	 */
	List<Story> listExcludedStoriesOlderThan(Date maximumDate, int maxResults);
	
	/**
	 * Retorna a maior numeração de página entre as laudas de um espelho. As
	 * laudas excluídas não serão incluídas no resultado.
	 * 
	 * @param rundown rundown envolvida
	 * @return maior página encontrada ou {@link null} caso não exista laudas no
	 *         espelho informado
	 */
	String getMaxPage(Rundown rundown);

    /**
     * Faz a contagem de registros marcados com a flag de exclusão.
     * 
     * @return a quantidade encontrada
     */
    int countExcludedStories();
    
    /**
     * Retorna uma página de registros marcados como excluídos.
     *
     * @param pageNumber
     * @return lista de registros encontrados na posição informada
     */
    List<Story> listExcludedStories(int pageNumber);

	/**
	 * Método utilizado para a listagem dos registros da área de edição de laudas.
	 * Retorna a lista do registros atribuido a um editor afim de que o mesmo possa
	 * identificar laudas pendentes para a edição na data informada como parâmetro
	 * para a busca.
	 * 
	 * @param date data da consulta
	 * @param editor editor atribuido a lauda para a filtragem dos registros
	 * @return lista de registros encontrados de acordo com os parâmetros
	 *         informados
	 */
	List<Story> listByDateAndEditor(Date date, User editor);

	/**
	 * @param guideline pauta alvo da busca
	 * @return Retorna uma lista de laudas criadas a partir de uma pauta.
	 */
	List<Story> listByGuideline(Guideline guideline);

    List<Story> listByReportage(Reportage reportage);

    int countByStoryKind(StoryKind kind);

    int totalExcluded();
    
    /**
     * Encontra todas as lauda que foram criadas a partir da lauda informada.
     *
     * @param sourceStory lauda de origem
     * @return a lista das laudas criadas a partir da lauda informada
     */
    List<Story> findBySourceStory(Story sourceStory);

    List<Story> listById(List<Long> idsOfStories);

    List<Story> listById(Long[] idsOfStories);
}
