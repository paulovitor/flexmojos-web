package tv.snews.anews.dao;

import tv.snews.anews.domain.Reportage;
import tv.snews.anews.search.ReportageParams;

import java.util.List;

/**
 * @author Felipe Zap
 * @since 1.2.6.3
 */
public interface ReportageFullTextDao {

    int totalBySearchReportage(ReportageParams searchParams);

    List<Reportage> searchReportageGeneral(ReportageParams searchParams);

    List<Reportage> listTheFirstHundredByParams(ReportageParams searchParams);
}
