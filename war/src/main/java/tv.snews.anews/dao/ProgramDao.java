package tv.snews.anews.dao;

import tv.snews.anews.domain.Device;
import tv.snews.anews.domain.IIDevicePath;
import tv.snews.anews.domain.Program;

import java.util.List;

/**
 * Estabelece um contrato de acesso aos dados do Programa.
 *
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface ProgramDao extends GenericDao<Program> {

	public static enum FetchType { CG, EDITORS, IMAGE_EDITORS, PRESENTERS };

	Program getByName(String name);

	Program get(Integer id, FetchType... types);

	int countByDevice(Device device);

	int countByPath(IIDevicePath path);

	List<Program> listAll();

	List<Program> listAllWithDisableds(FetchType... types);

	List<Program> listAllOrderByStart();
}
