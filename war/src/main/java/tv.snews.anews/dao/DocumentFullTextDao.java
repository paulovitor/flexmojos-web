
package tv.snews.anews.dao;

import tv.snews.anews.domain.Document;
import tv.snews.anews.search.DocumentParams;

import java.util.List;


/**
 * Interface da persistência dos documentos .
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public interface DocumentFullTextDao  {

	/**
     * @param params
     * @return
     */
    List<Document> listTheFirstHundredByParams(DocumentParams params);

	/**
     * @param params
     * @return
     */
    List<Document<?>> findByParams(DocumentParams params);

	/**
     * @param params
     * @return
     */
    int countByParams(DocumentParams params);
	
}
