package tv.snews.anews.dao;

import tv.snews.anews.domain.Group;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa os grupos deve
 * implementar.
 * 
 * @author Samuel Guedes de Melo.
 * @author Felipe Zap de Mello.
 * @since 1.0.0
 */
public interface GroupDao extends GenericDao<Group> {

	List<Group> listAll();

//    Group listGroup(Group group);

    Group listGroupByName(String name);
}
