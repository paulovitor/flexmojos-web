package tv.snews.anews.dao;

import tv.snews.anews.domain.ReportageRevision;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public interface ReportageRevisionDao extends GenericDao<ReportageRevision> {

	/**
	 * Retorna as revisões de reportagens criadas anteriormente à data
	 * informada. Não será retornada uma quantidade de registros superior ao
	 * valor definido por {@code maxResults}.
	 * 
	 * @param maximumDate data limite das revisões
	 * @param maxResults quantidade máxima de registros a serem retornados
	 * @return a lista de revisões encontradas (se não existir nenhuma, uma
	 *         lista vazia será retornada)
	 */
	List<ReportageRevision> listDateLessThan(Date date, int maxResults);
}
