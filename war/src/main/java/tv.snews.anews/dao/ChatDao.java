package tv.snews.anews.dao;

import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Interface que define os métodos que devem ser implementados pelos DAOs que
 * manipulam os dados dos chats do sistema de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ChatDao extends GenericDao<Chat> {

	/**
	 * Procura por um registro de chat que possua exatamente os membros
	 * informados, não aceitando registros que possuam membros adicionais. Caso
	 * não seja encontrado é retornado <code>null</code>.
	 * 
	 * @param members Membros participantes do chat.
	 * @return Objeto {@link Chat} ou <code>null</code>.
	 */
	Chat findByMembers(Set<User> members);
	
	List<Chat> listAll(int owner);
	
	/**
	 * Retorna uma lista contendo todos os chats dentro da data informada.
	 * 
	 * @param date data que será filtrada.
	 * @return Lista de chats encontrados.
	 */

	List<Chat> listByDate(Date date, int owner);
	
	/**
	 * Retorna uma lista contendo todos os chats que não possuem mensagens ou
	 * arquivos no seus registros.
	 * 
	 * @param limit Limita a quantidade de registros a serem retornados.
	 * @return Lista de chats vazios encontrados.
	 */
	List<Chat> listEmptyChats(int limit);
}
