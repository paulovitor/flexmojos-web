package tv.snews.anews.dao;

import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.ContactGroup;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa os contatos deve
 * implementar.
 *
 * @author Samuel Guedes de Melo
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface ContactDao extends GenericDao<Contact> {

	int countByName(String name);

	List<AgendaContact> findByName(String name, int firstResult, int maxResult);

	AgendaContact findMatch(AgendaContact contact);

	int countByContactGroup(ContactGroup contactGroup);

	List<AgendaContact> listExcludedContacts(int maxResult);

	List<Contact> listNotUsedContacts(int maxResult);
}
