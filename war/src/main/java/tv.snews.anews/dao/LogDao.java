package tv.snews.anews.dao;

import tv.snews.anews.domain.*;

import java.util.List;

/**
 * Interface da persistência de log do espelho. 
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public interface LogDao extends GenericDao<AbstractLog> {

	/**
	 * Retorna todos os logs armazenados no BD.
	 */
	List<AbstractLog> listAll(Rundown rundown);
	
	/**
	 * Retorna todos os logs de um espelho.
     */
	List<RundownLog> listLogsByRundownAction(Rundown rundown, List<RundownAction> actions, int pageNumber);

	List<BlockLog> listLogsByBlockAction(Rundown rundown, List<BlockAction> actions, int pageNumber);

	List<StoryLog> listLogsByStoryAction(Rundown rundown, List<StoryAction> actions, int pageNumber);

    List<StoryLog> findStoryLogsByStory(Story story);

    int totalStory(Rundown rundown, List<StoryAction> storyActions);

    int totalRundown(Rundown rundown, List<RundownAction> rundownActions);

    int totalBlock(Rundown rundown, List<BlockAction> blockActions);

    int totalAll(Rundown rundown);
}
