package tv.snews.anews.dao;

import tv.snews.anews.domain.News;
import tv.snews.anews.search.NewsParams;

import java.util.List;


/**
 * @author Paulo/Samuel
 * @since 1.0.0
 */
public interface NewsDao extends GenericDao<News> {

    List<News> listAll();

    /**
     * Lista todas as news.
     *
     * @param firstResult
     * @param fetchLimit
     * @return
     */
    List<News> listNewsOlderThan(int firstResult, int fetchLimit);

    /**
     * @return
     */
    int totalByNews();

    int totalByParams(NewsParams params);

    /**
     * @param pageNumber
     * @return
     */
    List<News> listAllPage(int pageNumber);

    List<News> findByParams(NewsParams params);
}
