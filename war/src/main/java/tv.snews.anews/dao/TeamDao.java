package tv.snews.anews.dao;

import tv.snews.anews.domain.Team;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa as equipes deve
 * implementar.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface TeamDao extends GenericDao<Team> {

	List<Team> listAll();

//    Group listGroup(Group group);

    Team listTeamByName(String name);
}
