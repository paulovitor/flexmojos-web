package tv.snews.anews.dao;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.ReportageParams;

import java.util.Date;
import java.util.List;

/**
 * Interface que define o contrato a ser seguido pelas implementações do DAO de 
 * reportagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ReportageDao extends DocumentDao<Reportage> {

    /**
	 * Carrega lista de reportagens do dia paginado.
	 * 
	 * @return lista de reportagens localizadas.
	 */
    List<Reportage> findByParams(ReportageParams params);

    int totalByParams(ReportageParams params);
	
    List<Reportage> findAllByDateAndProgram(Date currentDay, Program program);

   /**
	 * Retorna a quantidade de reportagens do reporter marcadas com a flag <code>excluded</code>
	 * com o valor <code>true</code>.
	 * 
	 * @return Quantidade encontrada.
	 */
    int countExcluded(User reporter);
	    
    /**
	 * Lista todas as reportagens do banco de dados que estão marcadas com a flag
	 * <code>excluded</code> com o valor <code>true</code>. Serão retornados
	 * apenas o registros a partir da posição indicada pela variável
	 * <code>firstResult</code> e no máximo a quantidade definida por
	 * <code>maxResults</code>.
	 * 
	 * @param firstResult Posição inicial da paginação.
	 * @param maxResults Quantidade máxima a ser retornada.
	 * @return Lista de registros encontrados na posição indicada.
	 */
    
    /**
     * @return
     */
    int totalExcluded();

    /**
     * @param reporter
     * @param pageNumber
     * @return
     */
    List<Reportage> listExcludedPage(int pageNumber);
    
	/**
	 * Lista todas as reportagens do banco de dados que estão marcadas com a
	 * flag <code>excluded</code> com valor <code>true</code>. E menores que o
	 * valor da data informada <code>date</code> informado.
	 * 
	 * @param minDate Data mínima de resultados.
	 * @param maxResults Quantidade máxima de resultados a ser retornados.
	 * @return
	 */
    List<Reportage> listReportageExcludedsOlderThan(Date minDate, int maxResults);

    /**
   	 * Retorna as versões de reportagem criadas anteriormente à data informada.
   	 * Não será retornada uma quantidade de registros superior ao valor definido 
   	 * por {@code maxResults}.
   	 * 
   	 * @param maximumDate data limite das versões
   	 * @param maxResults quantidade máxima de registros a serem retornados
   	 * @return a lista de versões encontradas (se não existir nenhuma, uma lista
   	 *         vazia será retornada)
   	 */
   	List<Reportage> listVersionsOlderThan(Date maximumDate, int maxResults);
}
