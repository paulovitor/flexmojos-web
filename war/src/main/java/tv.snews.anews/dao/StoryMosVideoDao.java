package tv.snews.anews.dao;

import tv.snews.anews.domain.MosDeviceChannel;
import tv.snews.anews.domain.StoryMosVideo;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface StoryMosVideoDao extends GenericDao<StoryMosVideo> {

    int countByChannel(MosDeviceChannel channel);
}
