package tv.snews.anews.dao;

import tv.snews.anews.domain.MosDevice;

import java.util.List;

/**
 * Interface que define os métodos que uma implementação do DAO para os objetos 
 * {@link tv.snews.anews.domain.MosDevice} deve implementar.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface MosDeviceDao extends GenericDao<MosDevice> {

	/**
	 * Busca por um dispositivo cadastrado que possua o identificador MOS
	 * informado.
	 * 
	 * @param identifier identificador MOS do dispositivo
	 * @return dispositivo encontrado ou {@code null} caso não encontre
	 */
	MosDevice findByIdentifier(String identifier);
	
	/**
	 * Lista dispositivos MOS cadastrados pelo tipo específico.
	 * 
	 * @param mosDeviceType tipo de dispositivo.
	 * @return lista de dispositivos cadastrados pelo tipo específico.
	 */
//	List<MosDevice> findAllByType(MosDeviceTypeEnum mosDeviceType);

	/**
	 * Lista todos os dispositivos MOS cadastrados.
	 * 
	 * @return lista de dispositivos cadastrados
	 */
//	List<MosDevice> list();
	
	/**
	 * Lista todos os dispositivos MOS cadastrados que suportam os profile 
	 * informado.
	 * 
	 * @param profileNumber número do profile
	 * @return a lista de dispositivos encontrados
	 */
	List<MosDevice> listByProfileSupport(int profileNumber);
}
