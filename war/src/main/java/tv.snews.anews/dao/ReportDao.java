package tv.snews.anews.dao;

import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.ReportEvent;
import tv.snews.anews.domain.ReportNature;
import tv.snews.anews.search.ReportParams;

import java.util.List;

/**
 * Interface que define os métodos necessários para uma implementação do DAO 
 * responsável pelas ações relacionadas à persistência de objetos do tipo 
 * {@link Report}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportDao extends GenericDao<Report> {

	/**
	 * Faz a contagem de todos os registros de relatórios que foram cadastrados
	 * pelo usuário no banco de dados.
	 * 
	 * @param params parâmetros da busca
	 * @return a quantidade de relatórios no banco de dados
	 */
	int countByParams(ReportParams params);
	
	/**
	 * Faz a contagem de todos os registros de relatórios que foram cadastrados 
	 * pelo usuário no banco de dados associados ao evento informado.
	 * 
	 * @param event objeto do evento desejado
	 * @return a quantidade de relatórios no banco de dados ligados ao evento
	 */
	int countAllByEvent(ReportEvent event);
	
	/**
	 * Faz a contagem de todos os registros de relatórios que foram cadastrados 
	 * pelo usuário no banco de dados associados à natureza informada.
	 * 
	 * @param nature objeto da natureza desejada
	 * @return a quantidade de relatórios no banco de dados ligados à natureza
	 */
	int countAllByNature(ReportNature nature);
	
	/**
	 * Retorna uma página de registros de relatórios cadastrados no banco de
	 * dados pelos usuários.
	 * <p>
	 * Será utilizado o tamanho de página padrão.
	 * </p>
	 * 
	 * @param params parâmetros da busca
	 * @return os registros encontrados na posição de página informada
	 */
	List<Report> findByParams(ReportParams params);
	
	/**
	 * Retorna uma página de registros de relatórios cadastrados no banco de 
	 * dados pelos usuários.
	 * 
	 * @param params parâmetros da busca
	 * @param firstResult posição do primeiro registro a ser retornado
	 * @param maxResult quantidade máxima de registros que podem ser retornados
	 * @return os registros encontrados na posição de página informada
	 */
	List<Report> findByParams(ReportParams params, int firstResult, int maxResult);
}
