package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.RoundDao;
import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Round;

import java.util.Date;
import java.util.List;

/**
 * Implementação do roundDao para acesso ao dados das rondas.
 *
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class RoundDaoImpl extends GenericDaoImpl<Round> implements RoundDao {

	@Override
	public List<Round> listAll(Date date) {
		Date startDate = minimizeTime(date);
		Date endDate = maximizeTime(date);

		DetachedCriteria criteria = DetachedCriteria.forClass(Round.class);
		criteria.add(Restrictions.between("date", startDate, endDate));

		return findByCriteria(criteria);
	}

	@Override
	public List<Round> listRoundOlderThan(Date minDate, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Round.class);
		criteria.add(Restrictions.lt("date", minDate));
		return findByCriteria(criteria, 0, maxResults);
	}

	@Override
	public int countByInstitution(Institution institution) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Round.class);
		criteria.add(Restrictions.eq("institution", institution));
		return rowCount(criteria);
	}
}
