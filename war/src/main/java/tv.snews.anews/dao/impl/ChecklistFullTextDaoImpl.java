package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.ChecklistFullTextDao;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.domain.ChecklistTask;
import tv.snews.anews.search.ChecklistParams;

import java.io.IOException;
import java.util.List;

import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class ChecklistFullTextDaoImpl extends FullTextSearchDao implements ChecklistFullTextDao {

    private static final String DATE = "date";
//    private static final String EXCLUDED = "excluded";
    private static final String LOCAL = "local";
    private static final String INFO = "info";
    private static final String SLUG = "slug";
    private static final String TEAM = "team";
    private static final String TAPES = "tapes";
    private static final String RESOURCE_NAME = "resources.name";
    private static final String TASK_LABEL = "tasks.label";
    private static final String TASK_INFO = "tasks.info";
    private static final String TASK_DATE = "tasks.date";

    private static final String PRODUCER = "producer.nickname";
    private static final String TYPE = "type.name";

    private static final String[] QUERY_PARSER_FIELDS = new String[] {
            DATE, LOCAL, INFO, SLUG, TEAM, TAPES, RESOURCE_NAME, TASK_LABEL, TASK_INFO, TASK_DATE, PRODUCER, TYPE
    };

    @Override
    public int countByParams(ChecklistParams params) {
        return countResults(luceneQuery(params), Checklist.class);
    }

    @Override
    public List<Checklist> findByParams(ChecklistParams params) {
        Query query = luceneQuery(params);

			DetachedCriteria criteria = DetachedCriteria.forClass(Checklist.class)
                .setFetchMode("resources", FetchMode.JOIN)
                .setFetchMode("tasks", FetchMode.JOIN)
                .setResultTransformer(DISTINCT_ROOT_ENTITY);

        List<Checklist> results = listResults(
						query, Checklist.class,
						firstResult(params.getPage()), DEFAULT_PAGE_SIZE,
						sort(DATE, true), criteria
				);

        Analyzer analyzer = getAnalyzer(Checklist.class);
        Highlighter highlighter = getDefaultHighlighter(Checklist.class, query);

        return highlightMatches(results, analyzer, highlighter);
    }

    @Override
    public List<Checklist> listTheFirstHundredByParams(ChecklistParams params) {
			DetachedCriteria criteria = DetachedCriteria.forClass(Checklist.class)
					.setFetchMode("resources", FetchMode.JOIN)
					.setFetchMode("tasks", FetchMode.JOIN)
					.setResultTransformer(DISTINCT_ROOT_ENTITY);

			return listResults(
					luceneQuery(params), Checklist.class,
					firstResult(params.getPage()), 100,
					sort(DATE, true), criteria
			);
    }

    //------------------------------------
    //  Helpers
    //------------------------------------

    private Query luceneQuery(ChecklistParams params) {
        LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(params.getSearchType(), params.getText(), params.getIgnoreText());

        queryBuilder.appendDatePeriod(DATE, params.getInitialDate(), params.getFinalDate());
        queryBuilder.appendFieldTerm(SLUG, params.getSlug());
//        queryBuilder.appendFieldTerm(EXCLUDED, false); || Excluídas não estão mais sendo indexadas

        if (params.getProducer() != null) {
            queryBuilder.appendFieldTerm(PRODUCER, params.getProducer().getNickname());
        }

        if (params.getType() != null) {
            queryBuilder.appendFieldTerm(TYPE, params.getType().getName());
        }

			return parseQuery(queryBuilder, Checklist.class, QUERY_PARSER_FIELDS);
    }

    private List<Checklist> highlightMatches(List<Checklist> checklists, Analyzer analyzer, Highlighter highlighter) {
        for (Checklist checklist : checklists) {
            getHibernateTemplate().evict(checklist);

            try {
                String slug = highlighter.getBestFragment(analyzer, SLUG, checklist.getSlug());
                if (slug != null) {
                    checklist.setSlug(fixWhiteSpace(slug));
                }

                if (checklist.getInfo() != null) {
                    String info = highlighter.getBestFragment(analyzer, INFO, checklist.getInfo());
                    if (info != null) {
                        checklist.setInfo(fixWhiteSpace(info));
                    }
                }

                if (checklist.getLocal() != null) {
                    String local = highlighter.getBestFragment(analyzer, LOCAL, checklist.getLocal());
                    if (local != null) {
                        checklist.setLocal(fixWhiteSpace(local));
                    }
                }

                if (checklist.getTapes() != null) {
                    String tapes = highlighter.getBestFragment(analyzer, TAPES, checklist.getTapes());
                    if (tapes != null) {
                        checklist.setTapes(tapes);
                    }
                }

                if (checklist.getProducer() != null) {
                    if (!checklist.getProducer().getNickname().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
                        String producer = highlighter.getBestFragment(analyzer, PRODUCER, checklist.getProducer().getNickname());
                        if (producer != null) {
                            getHibernateTemplate().evict(checklist.getProducer());
                            checklist.getProducer().setNickname(fixWhiteSpace(producer));
                        }
                    }
                }

                if (checklist.getType() != null) {
                    if (!checklist.getType().getName().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
                        String type = highlighter.getBestFragment(analyzer, TYPE, checklist.getType().getName());
                        if (type != null) {
                            getHibernateTemplate().evict(checklist.getType());
                            checklist.getType().setName(fixWhiteSpace(type));
                        }
                    }
                }

                for (ChecklistResource resource : checklist.getResources()) {
                    if (!resource.getName().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
                        String name = highlighter.getBestFragment(analyzer, RESOURCE_NAME, resource.getName());
                        if (name != null) {
                            getHibernateTemplate().evict(resource);
                            resource.setName(fixWhiteSpace(name));
                        }
                    }
                }

                for (ChecklistTask task : checklist.getTasks()) {
                    String name = highlighter.getBestFragment(analyzer, TASK_LABEL, task.getLabel());
                    if (name != null) {
                        getHibernateTemplate().evict(task);
                        task.setLabel(fixWhiteSpace(name));
                    }

                    if (task.getInfo() != null) {
                        String info = highlighter.getBestFragment(analyzer, TASK_INFO, task.getInfo());
                        if (info != null) {
                            getHibernateTemplate().evict(task);
                            task.setInfo(fixWhiteSpace(info));
                        }
                    }
                }
            } catch (IOException | InvalidTokenOffsetsException e) {
                throw new RuntimeException("Failed to highlight search terms.", e);
            }
        }

        return checklists;
    }
}
