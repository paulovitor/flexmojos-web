package tv.snews.anews.dao.impl;

import org.hibernate.FetchMode;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.ReportageDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.ReportageParams;
import tv.snews.anews.service.ReportageService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * Implementação da interface {@link ReportageDao} utilizando Hibernate e o
 * suporte do Spring para Hibernate.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ReportageDaoImpl extends DocumentDaoImpl<Reportage> implements ReportageDao {

	@Override
	public Reportage get(Serializable id) {
		// TODO Mover implementação para o método get(Serializable, boolean)
		// TODO Após isso utilizar fetch somente onde necessário

		DetachedCriteria criteria = forClass(Reportage.class);
		criteria.add(Restrictions.idEq(id));
		criteria.setFetchMode("guideline.checklist.tasks", JOIN);

		// Esse join é importante para o método de copiar da praça (acho que por form também)
		setFetchMode(criteria);

		return uniqueResult(criteria);
	}

	@Override
	public List<Reportage> findByParams(ReportageParams params) {
        return findByCriteria(createCriteriaWithParams(params)
                .add(eq("excluded", false))
                .addOrder(Order.desc("date")), getFirstResult(params.getPage()), ReportageService.RESULTS_BY_PAGE);
	}

    @Override
    public int totalByParams(ReportageParams params) {
        return rowCount(createCriteriaWithParams(params));
    }

	@Override
	public List<Reportage> findAllByDateAndProgram(Date day, Program program) {
		DetachedCriteria criteria = forClass(Reportage.class);
		criteria.add(eq("date", day));
		criteria.add(eq("excluded", false));

		if (program != null) {
			criteria.add(eq("program", program));
		}

		setFetchMode(criteria);

		return findByCriteria(criteria);
	}

	@Override
	public int countExcluded(User reporter) {
		DetachedCriteria criteria = forClass(Reportage.class);
		criteria.add(eq("reporter", reporter));
		criteria.add(eq("excluded", true));
		return rowCount(criteria);
	}

	@Override
	public int totalExcluded() {
		DetachedCriteria criteria = forClass(Reportage.class);
		criteria.add(eq("excluded", true));
		return rowCount(criteria);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Reportage> listExcludedPage(int pageNumber) {
		List<Long> ids = findByCriteria(
				forClass(Reportage.class)
						.add(eq("excluded", true))
						.addOrder(desc("date"))
						.setProjection(Projections.id()),
				getFirstResult(pageNumber), Long.class
		);

		if (!ids.isEmpty()) {
			DetachedCriteria criteria = forClass(Reportage.class)
					.add(in("id", ids))
					.addOrder(desc("date"));
			setFetchMode(criteria);

			return findByCriteria(criteria);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<Reportage> listReportageExcludedsOlderThan(Date minDate, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Reportage.class);
		criteria.add(Restrictions.lt("changeDate", minDate));
		criteria.add(Restrictions.eq("excluded", true));
		return findByCriteria(criteria, 0, maxResults);
	}

	@Override
	public List<Reportage> listVersionsOlderThan(Date maximumDate, int maxResults) {
		DetachedCriteria criteria = forClass(Reportage.class);
		criteria.add(Restrictions.lt("date", maximumDate));
		criteria.add(eq("excluded", false));
		return findByCriteria(criteria, 0, maxResults);
	}

	private void setFetchMode(DetachedCriteria criteria) {
        criteria.createCriteria("sections", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.setFetchMode("cgs", FetchMode.JOIN);
		criteria.setFetchMode("revisions", FetchMode.JOIN);
		criteria.setFetchMode("copiesHistory", FetchMode.JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
	}

    /**
     * Busca utilizada na listagem da área de reportagens. Atualmente, apenas os campos Retranca, data inicial e final são
     * necessários. 
     * @param params
     * @return
     */
    private DetachedCriteria createCriteriaWithParams(ReportageParams params) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Reportage.class);
        if (params != null) {
            if (params.getInitialDate() != null) {
                criteria.add(ge("date", params.getInitialDate()));
            }
            if (params.getFinalDate() != null) {
                criteria.add(le("date", params.getFinalDate()));
            }
            if (params.getSlug() != null) {
                criteria.add(Restrictions.ilike("slug", params.getSlug(), MatchMode.ANYWHERE));
            }
        }
        return criteria;
    }
}
