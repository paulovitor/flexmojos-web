package tv.snews.anews.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.DocumentDao;
import tv.snews.anews.dao.GenericDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.search.DocumentParams;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.ge;
import static org.hibernate.criterion.Restrictions.le;

/**
 * Implementa a interface {@link DocumentDao} utilizando Hibernate em sua
 * implementação.
 *
 * @author Felipe Zap de Mello
 * @since 1.6.4
 */
public class DocumentDaoImpl<T extends Document> extends GenericDaoImpl<T> implements DocumentDao<T> {

	public DocumentDaoImpl() {
		super(Document.class);
	}

	@Override
	public Serializable save(T entity) {
		// Gera revisão com os últimos dados
		entity.createRevision();
		return super.save(entity);
	}

	@Override
	public Serializable save(T document, User author) {
		document.setAuthor(author);
		document.setChangeDate(new Date());
		return save(document);
	}

	@Override
	public void update(T entity) {
		entity.createRevision();
		super.update(entity);
	}

	@Override
	public void update(T document, User author) {
		document.setAuthor(author);
		document.setChangeDate(new Date());
		update(document);
	}

	@Override
	public T merge(T entity) {
		entity.createRevision();
		return super.merge(entity);
	}

	@Override
	public T merge(T document, User author) {
		document.setAuthor(author);
		document.setChangeDate(new Date());
		return merge(document);
	}

	@Override
	public int countByParams(DocumentParams params, boolean drawerSearch) {
		DetachedCriteria criteria = createCriteriaWithParams(params, drawerSearch);
		return rowCount(criteria);
	}

	@Override
	public List<T> findByParams(DocumentParams params, boolean drawerSearch) {
		int firstResult = params.getPage() > 0 ? (params.getPage() - 1) * GenericDao.DEFAULT_PAGE_SIZE : 0;

		return findByCriteria(
				createCriteriaWithParams(params, drawerSearch).addOrder(desc("date")),
				firstResult, DEFAULT_PAGE_SIZE
		);
	}

    //------------------------------------
	//  Helpers
	//------------------------------------

	private DetachedCriteria createCriteriaWithParams(final DocumentParams params, boolean drawerSearch) {
		Class<? extends Document> klass;

		// Tipo da entidade define a classe root da criteria
		switch (params.getOrigin()) {
			case GUIDELINE:
				klass = Guideline.class;
				break;
			case REPORTAGE:
				klass = Reportage.class;
				break;
			case STORY:
				klass = Story.class;
				break;
			case SCRIPT:
				klass = Script.class;
				break;
			default:
				klass = Document.class;
		}

		DetachedCriteria criteria = forClass(klass, "doc");

        // ------------- AN-958 -------------//

        criteria.createAlias("doc.program", "program", JoinType.LEFT_OUTER_JOIN)
                .setProjection(Projections.projectionList()
                                .add(Projections.property("id"), "id")
                                .add(Projections.property("date"), "date")
                                .add(Projections.property("slug"), "slug")
                                .add(Projections.property("program"), "program")
                                .add(Projections.property("class"))
                );

		// Slug
		if (StringUtils.isNotEmpty(params.getSlug())) {
			criteria.add(Restrictions.ilike("doc.slug", params.getSlug(), MatchMode.ANYWHERE));
		}

		if (drawerSearch) {
			if (params.getProgramId() > 0) {
				criteria.createAlias("doc.drawerPrograms", "drawerPrograms");
				criteria.add(Restrictions.eq("drawerPrograms.id", params.getProgramId()));
			}
		} else {
			if (params.getProgramId() != 0) {
				if (params.getProgramId() == -1) { //GavetaGeral
					criteria.add(Restrictions.isNull("program"));
				} else {
					criteria.add(Restrictions.eq("program.id", params.getProgramId()));
				}
			}
		}

		if (params.getInitialDate() != null) {
			criteria.add(ge("date", params.getInitialDate()));
		}

		if (params.getFinalDate() != null) {
			criteria.add(le("date", params.getFinalDate()));
		}

		// O desejado nessa query é localizar todos os documentos de um programa
		// que não possuem na sua lista de programas usados o programa informado,
		// ou seja, quero localizar todos os documentos de um programa que não foram 
		// usados pelo programa.
		//
//				SELECT document.id, document.slug 
//					FROM anews.document as document LEFT JOIN anews.document_has_program ON d.id = document_has_program.document_id 
//					where document.program_id = 2 and 
//						document.drawer = true and
//						document.excluded = false and
//						document.id NOT IN 
//							(
//								select document_id 
//									FROM anews.document_has_program
//									where program_id = 2
//							);


		if (params.getProgramUsedId() != 0) {
			DetachedCriteria dcUsed = DetachedCriteria.forClass(klass, "innerDoc");
			dcUsed.createAlias("innerDoc.programs", "pr");
			dcUsed.setProjection(Projections.id());
			dcUsed.add(Restrictions.eq("pr.id", params.getProgramUsedId()));
			criteria.add(Subqueries.propertyIn("doc.id", dcUsed));
		}

		if (params.getProgramNotUsedId() != 0 || params.getHideUsed()) {
			DetachedCriteria dcNotUsed = DetachedCriteria.forClass(klass, "innerDoc");
			dcNotUsed.createAlias("innerDoc.programs", "pr");
			dcNotUsed.setProjection(Projections.id());
			dcNotUsed.add(Restrictions.eq("pr.id", params.getHideUsed() ? params.getProgramId() : params.getProgramNotUsedId()));
			criteria.add(Subqueries.propertyNotIn("doc.id", dcNotUsed));
		}

		criteria.add(Restrictions.eq("excluded", false));

		// Caso a gaveta esteja sendo exibida no roteiro, deve ocultar o tipo Lauda.
		// E caso seja no espelho, deve ser ocultado o tipo Script
		if (params.getSource() != null && params.getSource().equals("SCRIPT_PLAN"))
			criteria.add(Restrictions.ne("doc.class", Story.class));

		if (params.getSource() != null && params.getSource().equals("RUNDOWN"))
			criteria.add(Restrictions.ne("doc.class", Script.class));

		return criteria;
	}

}
