package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.ContactFullTextDao;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.SearchType;

import java.io.IOException;
import java.util.List;

import static tv.snews.anews.infra.search.EmailLuceneBridge.SPECIAL_CHARS_REGEX;

/**
 * Implementação das consultas com Hibernate Search
 * para Contact.
 *
 * @author Felipe Zap de Mello
 * @author Samuel Guedes de Melo
 * @since 1.2.6.3
 */
public class ContactFullTextDaoImpl extends FullTextSearchDao implements ContactFullTextDao {

	private static final String NAME = "name";
	private static final String NAME_SORT = "nameSort";
	private static final String PROFESSION = "profession";
	private static final String COMPANY = "company";
	private static final String GROUP = "contactGroup.name";
	private static final String INFO = "info";
	private static final String EMAIL = "email";
	private static final String ADDRESS = "address";
	private static final String CITY = "city.name";
//	private static final String EXCLUDED = "excluded";

	private static final String[] QUERY_PARSER_FIELDS = new String[]{
			NAME, PROFESSION, COMPANY, GROUP, INFO, EMAIL, ADDRESS, CITY
	};

	@Override
	public int countByName(String name) {
		Query luceneQuery = getLuceneQuery(name, SearchType.FUZZY);
		return countResults(luceneQuery, AgendaContact.class);
	}

	@Override
	public List<AgendaContact> findByNamePaginated(String name, int firstResult, int maxResult) {
		Query luceneQuery = getLuceneQuery(name, SearchType.FUZZY);

		DetachedCriteria criteria = DetachedCriteria.forClass(AgendaContact.class)
				.setFetchMode("numbers", FetchMode.JOIN);

		List<AgendaContact> contacts = listResults(
				luceneQuery, AgendaContact.class,
				firstResult, maxResult,
				sort(NAME_SORT),
				criteria
		);

		applyHighlightTerms(contacts, getDefaultHighlighter(AgendaContact.class, luceneQuery));

		return contacts;
	}

	private Query getLuceneQuery(String text, SearchType type) {
		LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(type, text);
//		queryBuilder.appendFieldTerm(EXCLUDED, false); || Excluídos não estão mais sendo indexados
		return parseQuery(queryBuilder, AgendaContact.class, QUERY_PARSER_FIELDS);
	}

	private void applyHighlightTerms(List<AgendaContact> contacts, Highlighter highlighter) {
		Analyzer analyzer = getAnalyzer(AgendaContact.class);

		for (AgendaContact contact : contacts) {
			getHibernateTemplate().evict(contact);

			try {
//				private static final String CITY = "city.name";
				String name = highlighter.getBestFragment(analyzer, NAME, contact.getName());
				if (name != null) {
					contact.setName(fixWhiteSpace(name));
				}

				if (contact.getProfession() != null) {
					String profession = highlighter.getBestFragment(analyzer, PROFESSION, contact.getProfession());
					if (profession != null) {
						contact.setProfession(profession);
					}
				}

				if (contact.getCompany() != null) {
					String company = highlighter.getBestFragment(analyzer, COMPANY, contact.getCompany());
					if (company != null) {
						contact.setCompany(company);
					}
				}

				if (contact.getInfo() != null) {
					String info = highlighter.getBestFragment(analyzer, INFO, contact.getInfo());
					if (info != null) {
						contact.setInfo(info);
					}
				}

				if (contact.getEmail() != null) {
					String email = highlighter.getBestFragment(analyzer, EMAIL, contact.getEmail());
					if (email != null) {
						contact.setEmail(email);
					} else {
						email = highlighter.getBestFragment(analyzer, EMAIL, expandEmail(contact.getEmail()));
						if (email != null) {
							contact.setEmail(collapseEmail(email));
						}
					}
				}

				if (contact.getAddress() != null) {
					String address = highlighter.getBestFragment(analyzer, ADDRESS, contact.getAddress());
					if (address != null) {
						contact.setAddress(address);
					}
				}

				if (contact.getContactGroup() != null) {
					if (!contact.getContactGroup().getName().contains(OPEN_TAG_HIGHLIGHT)) {
						String groupName = highlighter.getBestFragment(analyzer, GROUP, contact.getContactGroup().getName());
						if (groupName != null) {
							getHibernateTemplate().evict(contact.getContactGroup());
							contact.getContactGroup().setName(groupName);
						}
					}
				}

				if (contact.getCity() != null) {
					if (!contact.getCity().getName().contains(OPEN_TAG_HIGHLIGHT)) {
						String cityName = highlighter.getBestFragment(analyzer, CITY, contact.getCity().getName());
						if (cityName != null) {
							getHibernateTemplate().evict(contact.getCity());
							contact.getCity().setName(cityName);
						}
					}
				}
			} catch (IOException | InvalidTokenOffsetsException e) {
				throw new RuntimeException("Failed to highlight search terms.", e);
			}
		}
	}

	private String expandEmail(String email) {
		// Se não colocar espaço entre os caracteres especiais senão o highlight não funciona
		return email.replaceAll(SPECIAL_CHARS_REGEX, " $1 ");
	}

	private String collapseEmail(String email) {
		return email.replaceAll(SPECIAL_CHARS_REGEX, "$1");
	}

//    private void setCollectionsFetchMode(FullTextQuery fullTextQuery) {
//    	Criteria criteria = getSession()
//			.createCriteria(A.class)
//				.setFetchMode("headSection.subSections", FetchMode.JOIN)
//				.setFetchMode("headSection.subSections.subSections", FetchMode.JOIN)
//				.setFetchMode("ncSection.subSections", FetchMode.JOIN)
//				.setFetchMode("vtSection.subSections", FetchMode.JOIN)
//				.setFetchMode("footerSection.subSections", FetchMode.JOIN)
//				.setFetchMode("footerSection.subSections.subSections", FetchMode.JOIN)
//				.setFetchMode("offSection.subSections", FetchMode.JOIN);
//    	fullTextQuery.setCriteriaQuery(criteria);
//    }
}
