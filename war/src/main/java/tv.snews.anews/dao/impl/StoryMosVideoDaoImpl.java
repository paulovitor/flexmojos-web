package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.StoryMosVideoDao;
import tv.snews.anews.domain.MosDeviceChannel;
import tv.snews.anews.domain.StoryMosVideo;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Restrictions.eq;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
public class StoryMosVideoDaoImpl extends GenericDaoImpl<StoryMosVideo> implements StoryMosVideoDao {


    @Override
    public int countByChannel(MosDeviceChannel channel) {
        return rowCount(
                forClass(StoryMosVideo.class).add(eq("channel", channel))
        );
    }
}
