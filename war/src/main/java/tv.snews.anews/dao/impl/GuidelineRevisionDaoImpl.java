package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.GuidelineRevisionDao;
import tv.snews.anews.domain.GuidelineRevision;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class GuidelineRevisionDaoImpl extends GenericDaoImpl<GuidelineRevision> implements GuidelineRevisionDao {

	@Override
	public List<GuidelineRevision> listDateLessThan(Date date, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(GuidelineRevision.class);
		criteria.add(Restrictions.lt("revisionDate", date));
		return findByCriteria(criteria, 0, maxResults);
	}
}
