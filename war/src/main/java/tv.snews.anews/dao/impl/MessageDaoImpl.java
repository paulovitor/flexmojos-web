package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.MessageDao;
import tv.snews.anews.domain.AbstractMessage;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;

import java.util.Date;
import java.util.List;

/**
 * Implementação da interface {@link MessageDao} usando Hibernate.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class MessageDaoImpl extends GenericDaoImpl<AbstractMessage> implements MessageDao {

	@Override
	public List<AbstractMessage> listUnreadedMessages(User recipient) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractMessage.class);
		criteria.createCriteria("destinations")
				.add(Restrictions.eq("receiver", recipient))
				.add(Restrictions.eq("readed", false));
		criteria.addOrder(Order.asc("date"));
		return findByCriteria(criteria);
	}

	@Override
	public List<AbstractMessage> listLastMessagesFrom(Chat chat, int limit, Date firstMessageChatResult) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractMessage.class);
		criteria.add(Restrictions.eq("chat", chat));
		if (firstMessageChatResult != null) {
			criteria.add(Restrictions.lt("date", firstMessageChatResult));
		}
		criteria.addOrder(Order.desc("date"));
		return findByCriteria(criteria, 0, limit);
	}
	
	@Override
	public List<AbstractMessage> listMessagesOlderThan(Date date, int limit) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractMessage.class);
		criteria.add(Restrictions.lt("date", date));
		return findByCriteria(criteria, 0, limit);
	}
	
	@Override
	public AbstractMessage getLastMessageOfChat(Chat chat) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractMessage.class);
		criteria.add(Restrictions.eq("chat", chat));
		criteria.addOrder(Order.desc("date"));

		List<AbstractMessage> listMessage = findByCriteria(criteria, 0, 1);
		return listMessage.get(0);
	}
	
	@Override
	public List<AbstractMessage> listMessagesByChat(Chat chat) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractMessage.class);
		criteria.add(Restrictions.eq("chat", chat));
		criteria.addOrder(Order.desc("date"));
		return findByCriteria(criteria);
	}
}
