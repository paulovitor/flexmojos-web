package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.GuidelineFullTextDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.infra.search.ProgramLuceneBridge;
import tv.snews.anews.search.GuidelineParams;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;

/**
 * Implementação das consultas com Hibernate Search para Guideline.
 *
 * @author Felipe Zap de Mello
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class GuidelineFullTextDaoImpl extends FullTextSearchDao implements GuidelineFullTextDao {

	private static final String SLUG = "slug";
	private static final String PROPOSAL = "proposal";
	private static final String REFERRAL = "referral";
	private static final String INFORMATION = "informations";
	private static final String OBSERVATION = "guides.observation";
	private static final String REFERENCE = "guides.reference";
	private static final String ADDRESS = "guides.address";
	private static final String DATE = "date";
	private static final String PROGRAM = "program";
	private static final String REPORTER = "reporters.nickname";
	private static final String PRODUCER = "producers.nickname";
	private static final String CLASSIFICATION = "classification.name";
    private static final String TEAM = "team.name";
	private static final String VEHICLES = "vehicles";
//	private static final String EXCLUDED = "excluded";
	
	private static final String[] QUERY_PARSER_FIELDS = new String[] { 
		PROPOSAL, REFERRAL, INFORMATION,
		OBSERVATION, REFERENCE, ADDRESS 
	};

	@Override
	public int total(GuidelineParams searchParams) {
		Query luceneQuery = getLuceneQuery(searchParams);
		return countResults(luceneQuery, Guideline.class);
	}

	@Override
	public List<Guideline> listByPage(GuidelineParams searchParams) {
		Query luceneQuery = getLuceneQuery(searchParams);

		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class)
				.setFetchMode("guides", JOIN)
				.setFetchMode("checklist.tasks", JOIN)
				.setFetchMode("copiesHistory", JOIN)
				.setResultTransformer(DISTINCT_ROOT_ENTITY);

		List<Guideline> guidelines = listResults(
				luceneQuery, Guideline.class,
				firstResult(searchParams.getPage()), DEFAULT_PAGE_SIZE,
				sort(DATE, true),
				criteria
		);

		applyHighlightTerms(guidelines, getDefaultHighlighter(Guideline.class, luceneQuery));
		return guidelines;
	}

	@Override
	public List<Guideline> listTheFirstHundredByParams(GuidelineParams searchParams) {
		Query luceneQuery = getLuceneQuery(searchParams);

		return listResults(
				luceneQuery, Guideline.class,
				firstResult(searchParams.getPage()), 100,
				sort(DATE, true)
		);
	}

	private Query getLuceneQuery(GuidelineParams searchParams) {
		Date initialDate = searchParams.getInitialDate();
		Date finalDate = searchParams.getFinalDate();
		String slug = searchParams.getSlug();
		String text = searchParams.getText();
		String ignoreText = searchParams.getIgnoreText();
		User producer = searchParams.getProducer();
		User reporter = searchParams.getReporter();
		Program program = searchParams.getProgram();
		SearchType searchType = searchParams.getSearchType();
		GuidelineClassification classification = searchParams.getClassification();
        Team team = searchParams.getTeam();
		CommunicationVehicle vehicle = searchParams.getVehicle();

		LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(searchType, text, ignoreText);

		queryBuilder.appendDatePeriod(DATE, initialDate, finalDate);
		queryBuilder.appendFieldTerm(SLUG, slug);
//		queryBuilder.appendFieldTerm(EXCLUDED, false); || Excluídas não estão mais sendo indexadas

		if (classification != null) {
			queryBuilder.appendFieldTerm(CLASSIFICATION, classification.getName());
		}

        if (team != null) {
            queryBuilder.appendFieldTerm(TEAM, team.getName());
        }

		if (vehicle != null) {
			queryBuilder.appendFieldTerm(VEHICLES, vehicle.toString());
		}

		if (program != null && program.getId() > 0) {
			queryBuilder.appendFieldTerm(PROGRAM, program.getName());
		} else if (program != null && program.getId() == -1) {
			queryBuilder.appendFieldTerm(PROGRAM, ProgramLuceneBridge.NULL);
		}

		if (producer != null) {
			queryBuilder.appendFieldTerm(PRODUCER, producer.getNickname());
		}

		if (reporter != null) {
			queryBuilder.appendFieldTerm(REPORTER, reporter.getNickname());
		}

		return parseQuery(queryBuilder, Guideline.class, QUERY_PARSER_FIELDS);
	}

	/*
 * Aplica marcação para os termos encontrados na busca.
 */
	private void applyHighlightTerms(List<Guideline> guidelines, Highlighter highlighter) {
		Analyzer analyzer = getAnalyzer(Guideline.class);

		for (Guideline guideline : guidelines) {
			getHibernateTemplate().evict(guideline);

			try {
				String slug = highlighter.getBestFragment(analyzer, SLUG, guideline.getSlug());
				if (slug != null) {
					guideline.setSlug(fixWhiteSpace(slug));
				}

				String proposal = highlighter.getBestFragment(analyzer, PROPOSAL, guideline.getProposal());
				if (proposal != null) {
					guideline.setProposal(fixWhiteSpace(proposal));
				}

				if (guideline.getInformations() != null) {
					String informations = highlighter.getBestFragment(analyzer, INFORMATION, guideline.getInformations());
					if (informations != null) {
						guideline.setInformations(fixWhiteSpace(informations));
					}
				}

				String referral = highlighter.getBestFragment(analyzer, REFERRAL, guideline.getReferral() != null ? guideline.getReferral() : "");
				if (referral != null) {
					guideline.setReferral(fixWhiteSpace(referral));
				}

				if (guideline.getProgram() != null) {
					if (!guideline.getProgram().getName().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
						String programName = highlighter.getBestFragment(analyzer, PROGRAM, guideline.getProgram().getName());
						if (programName != null) {
							getHibernateTemplate().evict(guideline.getProgram());
							guideline.getProgram().setName(fixWhiteSpace(programName));
						}
					}
				}

				for (User producer : guideline.getProducers()) {
					if (!producer.getNickname().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
						String producerName = highlighter.getBestFragment(analyzer, PRODUCER, producer.getNickname());
						if (producerName != null) {
							getHibernateTemplate().evict(producer);
							producer.setNickname(fixWhiteSpace(producerName));
						}
					}
				}

				for (User reporter : guideline.getReporters()) {
					if (!reporter.getNickname().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
						String reporterName = highlighter.getBestFragment(analyzer, REPORTER, reporter.getNickname());
						if (reporterName != null) {
							getHibernateTemplate().evict(reporter);
							reporter.setNickname(fixWhiteSpace(reporterName));
						}
					}
				}

				for (Guide guide : guideline.getGuides()) {
					getHibernateTemplate().evict(guide);

					if (guide.getReference() != null) {
						String reference = highlighter.getBestFragment(analyzer, REFERENCE, guide.getReference());
						if (reference != null) {
							guide.setReference(fixWhiteSpace(reference));
						}
					}

					if (guide.getAddress() != null) {
						String address = highlighter.getBestFragment(analyzer, ADDRESS, guide.getAddress());
						if (address != null) {
							guide.setAddress(fixWhiteSpace(address));
						}
					}

					if (guide.getObservation() != null) {
						String observation = highlighter.getBestFragment(analyzer, OBSERVATION, guide.getObservation());
						if (observation != null) {
							guide.setObservation(fixWhiteSpace(observation));
						}
					}
				}
			} catch (IOException | InvalidTokenOffsetsException e) {
				throw new RuntimeException("Failed to highlight search terms.", e);
			}
		}
	}
}
