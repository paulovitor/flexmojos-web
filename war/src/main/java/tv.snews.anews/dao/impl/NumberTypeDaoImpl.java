package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import tv.snews.anews.dao.NumberTypeDao;
import tv.snews.anews.domain.NumberType;

import java.util.List;

/**
 * Implementação da persistência dos contatos.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public class NumberTypeDaoImpl extends GenericDaoImpl<NumberType> implements NumberTypeDao {

	@Override
	public List<NumberType> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(NumberType.class);
		criteria.addOrder(Order.asc("description").ignoreCase());
		return findByCriteria(criteria);
	}
}
