package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.TwitterUserDao;
import tv.snews.anews.domain.TwitterUser;

import java.util.List;

/**
 * Interface que define os métodos que devem ser implementados pelos DAOs que
 * manipulam os dados do twitter do usuário no sistema.
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TwitterUserDaoImpl extends GenericDaoImpl<TwitterUser> implements TwitterUserDao {
	
    @Override
	public List<TwitterUser> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(TwitterUser.class);
		criteria.addOrder(Order.asc("id"));
		return findByCriteria(criteria);
	}

    @Override
    public TwitterUser findTwitterUserByScreenName(String screenName) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(TwitterUser.class);
		criteria.add(Restrictions.eq("screenName", screenName));
		return uniqueResult(criteria);
    }
    
    @Override
    public void delete(TwitterUser twitterUser) {
    	// Correção para o problema de chegar aqui um feed com a lista de itens vazia por conta do seu lazy=true
    	getHibernateTemplate().refresh(twitterUser);
    	super.delete(twitterUser);
    }
}
