package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.StoryKindDao;
import tv.snews.anews.domain.StoryKind;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.or;

/**
 * Implementação da persistência dos tipos da lauda.
 *
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public class StoryKindDaoImpl extends GenericDaoImpl<StoryKind> implements StoryKindDao {

	@Override
	public List<StoryKind> findAll() {
		return findByCriteria(
				forClass(StoryKind.class).addOrder(asc("name").ignoreCase())
		);
	}

	@Override
	public List<StoryKind> findAllOrderByAcronym() {
		return findByCriteria(
				forClass(StoryKind.class).addOrder(asc("acronym").ignoreCase())
		);
	}

	@Override
	public StoryKind findByNameOrAcronym(String name, String acronym) {
		return uniqueResult(
				forClass(StoryKind.class).add(
						or(eq("name", name), eq("acronym", acronym))
				)
		);
	}

	@Override
	public StoryKind getDefaultStoryKind() {
		DetachedCriteria criteria = DetachedCriteria.forClass(StoryKind.class);
		criteria.add(Restrictions.eq("defaultKind", true));
		return uniqueResult(criteria);
	}
}
