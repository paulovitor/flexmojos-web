package tv.snews.anews.dao.impl;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import tv.snews.anews.dao.GenericDao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Implementação Hibernate do DAO genérico.
 * 
 * @param <T>
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class GenericDaoImpl<T> extends HibernateDaoSupport implements GenericDao<T> {
	
	private final Class<T> type;
	
	//----------------------------------
	//	Constructors
	//----------------------------------

	@SuppressWarnings("unchecked")
	public GenericDaoImpl() {
		Type type = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

		if (type instanceof ParameterizedType) {
			ParameterizedType complexType = (ParameterizedType) type;
			this.type = (Class<T>) complexType.getRawType();
		} else {
			this.type = (Class<T>) type;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GenericDaoImpl(Class type) {
		this.type = type;
	}
	
	//----------------------------------
	//	Métodos da interface
	//----------------------------------
	
	@Override
	public Serializable save(T entity) {
		return getHibernateTemplate().save(entity);
	}

	@Override
	public void update(T entity) {
		getHibernateTemplate().update(entity);
	}

	@Override
	public void delete(T entity) {
		getHibernateTemplate().delete(entity);
	}

	@Override
	public T get(Serializable id) {
		return getHibernateTemplate().get(type, id);
	}

	@Override
	public T get(Serializable id, boolean fetchAll) {
		// Sobreescrever e fazer o fetch quando necessário
		return get(id);
	}

	@Override
	public void flush() {
		getHibernateTemplate().flush();
	}

	@Override
	public void evict(T entity) {
		getHibernateTemplate().evict(entity);
	}
	
	public void refresh(T entity) {
		getHibernateTemplate().refresh(entity);
	}
	
	public T merge(T entity) {
		return getHibernateTemplate().merge(entity);
	}

	//------------------------------
	//	List Methods
	//------------------------------

	protected Session getSession() {
		return getHibernateTemplate().getSessionFactory().getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(DetachedCriteria criteria) {
		return (List<T>) getHibernateTemplate().findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(DetachedCriteria criteria, int firstResult) {
		return findByCriteria(criteria, firstResult, DEFAULT_PAGE_SIZE);
	}

	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(DetachedCriteria criteria, int firstResult, int maxResults) {
		return (List<T>) getHibernateTemplate().findByCriteria(criteria, firstResult, maxResults);
	}

	@SuppressWarnings("unchecked")
	protected <R> List<R> findByCriteria(DetachedCriteria criteria, Class<R> returnType) {
		return (List<R>) getHibernateTemplate().findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	protected <R> List<R> findByCriteria(DetachedCriteria criteria, int firstResult, Class<R> returnType) {
		return findByCriteria(criteria, firstResult, DEFAULT_PAGE_SIZE, returnType);
	}

	@SuppressWarnings("unchecked")
	protected <R> List<R> findByCriteria(DetachedCriteria criteria, int firstResult, int maxResults, Class<R> returnType) {
		return (List<R>) getHibernateTemplate().findByCriteria(criteria, firstResult, maxResults);
	}

	//----------------------------------
	//	Utilitários
	//----------------------------------

	@SuppressWarnings("unchecked")
	protected T uniqueResult(DetachedCriteria criteria) {
		List<?> results = getHibernateTemplate().findByCriteria(criteria);
		try {
			return (T) DataAccessUtils.uniqueResult(results);
		} catch (EmptyResultDataAccessException e) {
			// Quando não encontrar nada retorna null
			return null;
		}
	}

	protected int rowCount(DetachedCriteria criteria) {
		List<?> result = getHibernateTemplate().findByCriteria(
				criteria.setProjection(Projections.rowCount())
		);

		try {
			Number count = (Number) DataAccessUtils.uniqueResult(result);
			return count.intValue();
		} catch (EmptyResultDataAccessException e) {
			throw new RuntimeException(e);
		}
	}

	protected Date minimizeTime(Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	protected Date maximizeTime(Date date) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}
	
	/**
	 * Retorna o número do primeiro registro a ser buscado da página (pageNumber)
	 */
	protected int getFirstResult(int pageNumber) {
		return pageNumber > 0 ? (pageNumber - 1) * GenericDao.DEFAULT_PAGE_SIZE : 0;
    }
}
