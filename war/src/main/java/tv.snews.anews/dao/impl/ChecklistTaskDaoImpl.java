package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ChecklistTaskDao;
import tv.snews.anews.domain.ChecklistTask;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.6.2
 */
public class ChecklistTaskDaoImpl extends GenericDaoImpl<ChecklistTask> implements ChecklistTaskDao {

}
