package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import tv.snews.anews.dao.ReportFullTextDao;
import tv.snews.anews.domain.GuidelineReport;
import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.ReportageReport;
import tv.snews.anews.domain.StoryReport;
import tv.snews.anews.search.ReportParams;

import java.io.IOException;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class ReportFullTextDaoImpl extends FullTextSearchDao implements ReportFullTextDao {

	private static final String SLUG = "slug";
	private static final String CONTENT = "content";
	private static final String[] QUERY_PARSER_FIELDS = new String[]{
			CONTENT
	};
	private static final String DATE = "date";
	private static final String EVENT = "event.name";
	private static final String NATURE = "nature.name";

	@Override
	public int countByParams(ReportParams params) {
		Query query = luceneQuery(params);
		Class<?> reportClass = reportClassOf(params.getOrigin());
		return countResults(query, reportClass);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Report> findByParams(ReportParams params) {
		Query query = luceneQuery(params);
		Class<?> reportClass = reportClassOf(params.getOrigin());

		List<Report> results = (List<Report>) listResults(
				query, reportClass,
				firstResult(params.getPage()), DEFAULT_PAGE_SIZE,
				sort(DATE, true)
		);

		Analyzer analyzer = getAnalyzer(reportClass);
		Highlighter highlighter = getDefaultHighlighter(reportClass, query);

		return highlightMatchs(results, analyzer, highlighter);
	}

	//------------------------------------
	//  Helpers
	//------------------------------------

	private Query luceneQuery(ReportParams params) {
		LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(params.getSearchType(), params.getText(), params.getIgnoreText());

		queryBuilder.appendDatePeriod(DATE, params.getInitialDate(), params.getFinalDate());
		queryBuilder.appendFieldTerm(SLUG, params.getSlug());

		if (params.getEvent() != null) {
			queryBuilder.appendFieldTerm(EVENT, params.getEvent().getName());
		}
		if (params.getNature() != null) {
			queryBuilder.appendFieldTerm(NATURE, params.getNature().getName());
		}

		Class<?> reportClass = reportClassOf(params.getOrigin());
		return parseQuery(queryBuilder, reportClass, QUERY_PARSER_FIELDS);
	}

	private List<Report> highlightMatchs(List<Report> reports, Analyzer analyzer, Highlighter highlighter) {
		for (Report report : reports) {
			getHibernateTemplate().evict(report);
			try {
				String slug = highlighter.getBestFragment(analyzer, SLUG, report.getSlug());
				if (slug != null) {
					report.setSlug(fixWhiteSpace(slug));
				}

				String content = highlighter.getBestFragment(analyzer, SLUG, report.getContent());
				if (content != null) {
					report.setContent(fixWhiteSpace(content));
				}
			} catch (IOException | InvalidTokenOffsetsException e) {
				throw new RuntimeException("Failed to highlight search terms.", e);
			}
		}

		return reports;
	}

	private Class<?> reportClassOf(ReportParams.Origin origin) {
		switch (origin) {
			case GUIDELINE:
				return GuidelineReport.class;
			case REPORTAGE:
				return ReportageReport.class;
			case STORY:
				return StoryReport.class;
			default:
				return Report.class;
		}
	}
}
