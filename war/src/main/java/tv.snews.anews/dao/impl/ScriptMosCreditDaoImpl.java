package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.ScriptMosCreditDao;
import tv.snews.anews.domain.ScriptMosCredit;

import java.util.List;

import static org.hibernate.criterion.Restrictions.eq;

/**
 * 
 * @author snews
 * @since 1.3.0
 */
public class ScriptMosCreditDaoImpl extends GenericDaoImpl<ScriptMosCredit> implements ScriptMosCreditDao {

	@Override
	public List<ScriptMosCredit> getByMosMedia(Integer mosMediaId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ScriptMosCredit.class);
		criteria.createAlias("mosMedia", "mosMedia");
		criteria.add(eq("mosMedia.id", mosMediaId));
		return findByCriteria(criteria);
	}
	
}