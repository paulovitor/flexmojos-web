package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.UserFileDao;
import tv.snews.anews.domain.UserFile;

/**
 * Implementação da interface {@link tv.snews.anews.dao.UserFileDao} utilizando o suporte do
 * Spring para Hibernate.
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public class UserFileDaoImpl extends GenericDaoImpl<UserFile> implements UserFileDao {

}
