package tv.snews.anews.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.DateTools.Resolution;
import tv.snews.anews.domain.SearchType;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class LuceneQueryBuilder {

	private static final Date MINIMUM_DATE;
	private static final Date MAXIMUM_DATE;

	static {
		Calendar cal = Calendar.getInstance();

		cal.set(1950, Calendar.JANUARY, 1, 23, 59, 59);
		MINIMUM_DATE = cal.getTime();

		cal.set(2100, Calendar.JANUARY, 1, 0, 0, 0);
		MAXIMUM_DATE = cal.getTime();
	}

	private List<String> list = new ArrayList<>();

	public LuceneQueryBuilder(SearchType searchType, String text) {
		this(searchType, text, null);
	}

	public LuceneQueryBuilder(SearchType searchType, String text, String textToIgnore) {
		switch (searchType) {
			case SHOULD:
				appendShouldTerm(text);
				break;
			case PHRASE:
				appendPhraseTerm(text);
				break;
			case FUZZY:
				appendFuzzyTerm(text);
				break;
			case MUST:
				appendMustTerm(text);
				break;
		}

		if (!list.isEmpty()) {
			appendIgnoreTerm(textToIgnore);
		}
	}

	//------------------------------------
	//  Operations
	//------------------------------------

	public LuceneQueryBuilder appendFuzzyTerm(String term) {
		term = removeReservedChars(term);

		if (StringUtils.isNotBlank(term)) {
			StringBuilder queryBuilder = new StringBuilder();
			String[] words = term.split(" ");

			for (int i = 0; i < words.length; i++) {
				queryBuilder.append("+");
				queryBuilder.append("\"");
				queryBuilder.append(words[i]);
				queryBuilder.append("\"");
				queryBuilder.append("~0.5");
				queryBuilder.append(" ");
			}

			list.add(queryBuilder.toString().trim());
		}

		return this;
	}

	public LuceneQueryBuilder appendMustTerm(String term) {
		term = removeReservedChars(term);

		if (StringUtils.isNotBlank(term)) {
			StringBuilder queryBuilder = new StringBuilder();
			String[] words = term.split(" ");

			for (int i = 0; i < words.length; i++) {
				queryBuilder.append("+");
				queryBuilder.append(words[i]);
				queryBuilder.append(" ");
			}

			list.add(queryBuilder.toString().trim());
		}

		return this;
	}

	public LuceneQueryBuilder appendPhraseTerm(String term) {
		term = removeReservedChars(term);

		if (StringUtils.isNotBlank(term)) {
			list.add("\"" + term + "\"");
		}

		return this;
	}

	public LuceneQueryBuilder appendShouldTerm(String term) {
		term = removeReservedChars(term);

		if (StringUtils.isNotBlank(term)) {
			list.add("(" + term + ")");
		}

		return this;
	}

	public LuceneQueryBuilder appendIgnoreTerm(String term) {
		term = removeReservedChars(term);

		if (StringUtils.isNotBlank(term)) {
			list.add("-\"" + term + "\"");
		}

		return this;
	}

	public LuceneQueryBuilder appendFieldTerm(String field, Boolean value) {
		return appendFieldTerm(field, value.toString());
	}

	public LuceneQueryBuilder appendFieldTerm(String field, String value) {
		value = removeReservedChars(value);

		if (StringUtils.isNotBlank(value)) {
			list.add("+" + field + ":\"" + value + "\"");
		}

		return this;
	}

	public LuceneQueryBuilder appendDatePeriod(String field, Date initialDate, Date finalDate) {
		if (initialDate != null || finalDate != null) {
			if (initialDate == null) {
				initialDate = MINIMUM_DATE;
			}
			if (finalDate == null) {
				finalDate = MAXIMUM_DATE;
			}
			list.add("+" + field + ":[" + DateTools.dateToString(initialDate, Resolution.MILLISECOND) + " TO " + DateTools.dateToString(finalDate, Resolution.MILLISECOND) + "]");
		}

		return this;
	}

	@Override
	public String toString() {
		return StringUtils.join(list, " AND ");
	}

	public boolean isEmpty() {
		return list.isEmpty();
	}

	//------------------------------------
	//  Helpers
	//------------------------------------

	private String removeReservedChars(String str) {
		if (str != null) {
			return str.trim().replaceAll("[\"\\+\\-&\\|!\\(\\)\\{\\}\\[\\]\\^\\/~\\*\\?:\\\\]", "");
		}
		return str;
	}
}
