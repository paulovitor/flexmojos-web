package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.RundownTemplateDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownTemplate;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.eq;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class RundownTemplateDaoImpl extends GenericDaoImpl<RundownTemplate> implements RundownTemplateDao {

	@Override
	public List<RundownTemplate> findAllByProgram(Program program) {
		return findByCriteria(
				forClass(RundownTemplate.class)
						.add(eq("program", program))
						.addOrder(asc("name"))
		);
	}

	@Override
	public int countByNameAndProgram(String name, Program program) {
		return rowCount(
				forClass(RundownTemplate.class)
						.add(eq("name", name))
						.add(eq("program", program))
		);
	}
}
