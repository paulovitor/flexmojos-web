package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.BlockTemplateDao;
import tv.snews.anews.domain.BlockTemplate;

/**
 * @author Felipe Pinheiro
 * @since 1.6.0
 */
public class BlockTemplateDaoImpl extends GenericDaoImpl<BlockTemplate> implements BlockTemplateDao {
}
