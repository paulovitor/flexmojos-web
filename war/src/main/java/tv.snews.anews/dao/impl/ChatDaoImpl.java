package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.ChatDao;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ChatDaoImpl extends GenericDaoImpl<Chat> implements ChatDao {

	@Override
	public Chat findByMembers(Set<User> members) {
		Set<Integer> ids = new HashSet<Integer>(members.size());
		for (User member : members) {
			ids.add(member.getId());
		}
		
		DetachedCriteria criteria = DetachedCriteria.forClass(Chat.class);
		criteria.add(Restrictions.sizeEq("members", ids.size()));
		criteria.createCriteria("members").add(Restrictions.in("id", ids));

		/*
		 * A criteria acima está utilizando o "Restrictions.in" para pegar os
		 * chats que tem os membros informados, mas ela também retorna chats
		 * onde possui apena um dos membros informados. Então o código abaixo
		 * serve para eliminar os resultados indesejados.
		 * 
		 * Ainda não achei uma solução pela Criteria para evitar esse problema.
		 */
		Chat desiredChat = null;
		List<Chat> result = findByCriteria(criteria);
		for (Chat chat : result) {
			if (chat.containsAllMembers(members)) {
				desiredChat = chat;
				break;
			}
		}
		return desiredChat;
	}

	@Override
	public List<Chat> listAll(int owner) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Chat.class);
		criteria.createCriteria("members").add(Restrictions.idEq(owner));
		return findByCriteria(criteria);
	}

	@Override
	public List<Chat> listByDate(Date date, int owner) {
		Date startDate = minimizeTime(date);
		Date endDate = maximizeTime(date);

		DetachedCriteria criteria = DetachedCriteria.forClass(Chat.class);
		criteria.createCriteria("members").add(Restrictions.idEq(owner));
		criteria.add(Restrictions.between("lastInteraction", startDate, endDate));
		criteria.addOrder(Order.desc("lastInteraction"));
		
		return findByCriteria(criteria);
	}
	
	@Override
	public List<Chat> listEmptyChats(int limit) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Chat.class);
		criteria.add(Restrictions.isEmpty("messages"));
		return findByCriteria(criteria, 0, limit);
	}
}
