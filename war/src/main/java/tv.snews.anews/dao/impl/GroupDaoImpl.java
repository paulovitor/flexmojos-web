package tv.snews.anews.dao.impl;

import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.GroupDao;
import tv.snews.anews.domain.Group;

import java.util.List;

/**
 * Implementação da persistência dos grupos.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class GroupDaoImpl extends GenericDaoImpl<Group> implements GroupDao {

	@Override
	public List<Group> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Group.class);
		criteria.addOrder(Order.desc("name"));
		criteria.setFetchMode("permissions", FetchMode.JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return findByCriteria(criteria);
	}
	
	public Group listGroupByName(String name) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Group.class);
		criteria.add(Restrictions.eq("name", name));
		criteria.addOrder(Order.desc("name"));
		criteria.setFetchMode("permissions", FetchMode.JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return uniqueResult(criteria);
	}
}
