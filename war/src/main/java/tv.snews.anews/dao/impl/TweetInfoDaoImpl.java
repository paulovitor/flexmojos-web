package tv.snews.anews.dao.impl;

import org.hibernate.criterion.*;
import tv.snews.anews.dao.TweetInfoDao;
import tv.snews.anews.domain.TweetInfo;
import tv.snews.anews.domain.TwitterUser;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Zap de Mello.
 * @since 1.0.0
 */
public class TweetInfoDaoImpl extends GenericDaoImpl<TweetInfo> implements TweetInfoDao {

	@Override
	public List<TweetInfo> listPage(int firstResult, int maxResult) {
		return listPage(null, firstResult, maxResult);
	}

	@Override
	public List<TweetInfo> listPage(TwitterUser twitterUser, int firstResult, int maxResult) {
		DetachedCriteria criteria = DetachedCriteria.forClass(TweetInfo.class);
		criteria.addOrder(Order.desc("createdAt"));
		if (twitterUser != null) {
			criteria.add(Restrictions.eq("twitterUser", twitterUser));
		}
		return findByCriteria(criteria, firstResult, maxResult);
	}

	@Override
	public int countAll() {
		return countFromTwitterUser(null);
	}

	@Override
	public int countFromTwitterUser(TwitterUser twitterUser) {
		DetachedCriteria criteria = DetachedCriteria.forClass(TweetInfo.class);
		if (twitterUser != null) {
			criteria.add(Restrictions.eq("twitterUser", twitterUser));
		}
		return rowCount(criteria);
	}

	@Override
	public TweetInfo getLastTweetInfo(TwitterUser twitterUser) {
		DetachedCriteria criteria = DetachedCriteria.forClass(TweetInfo.class);
		DetachedCriteria criteria2 = DetachedCriteria.forClass(TweetInfo.class);
		ProjectionList projList = Projections.projectionList();
		projList.add(Projections.max("createdAt"));
		criteria.setProjection(projList);
		criteria2.add(Property.forName("createdAt").eq(criteria));
		criteria2.add(Restrictions.eq("twitterUser", twitterUser));
		return uniqueResult(criteria2);
	}

	@Override
	public List<TweetInfo> listItemsOlderThan(Date minDate, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(TweetInfo.class);
		//criteria.add(Restrictions.lt("createdAt", minDate));
		criteria.addOrder(Order.desc("createdAt"));
		return findByCriteria(criteria, 0, maxResults);
	}

	@Override
	public List<TweetInfo> listTweets(int firstResult, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(TweetInfo.class);
		criteria.addOrder(Order.desc("createdAt"));
		return findByCriteria(criteria, firstResult, maxResults);
	}
}
