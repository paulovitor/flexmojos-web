package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.MosMetadataPathDao;
import tv.snews.anews.domain.MosMetadataPath;

/**
 * Implementação padrão da interface {@link MosMetadataPathDao}.
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class MosMetadataPathDaoImpl extends GenericDaoImpl<MosMetadataPath> implements MosMetadataPathDao {

}
