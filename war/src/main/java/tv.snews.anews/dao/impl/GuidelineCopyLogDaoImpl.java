package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.GuidelineCopyLogDao;
import tv.snews.anews.domain.GuidelineCopyLog;

/**
 * @author Samuel Guedes de Melo
 */
public class GuidelineCopyLogDaoImpl extends GenericDaoImpl<GuidelineCopyLog> implements GuidelineCopyLogDao {
}
