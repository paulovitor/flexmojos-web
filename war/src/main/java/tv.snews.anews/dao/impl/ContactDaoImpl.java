package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.ContactDao;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.ContactGroup;

import java.util.List;

import static org.apache.commons.lang.StringUtils.isBlank;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.MatchMode.ANYWHERE;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * Implementação da persistência dos contatos.
 *
 * @author Samuel Guedes de Melo
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class ContactDaoImpl extends GenericDaoImpl<Contact> implements ContactDao {

	private DetachedCriteria searchByNameCriteria(String name) {
		DetachedCriteria criteria;

		if (isBlank(name)) {
			criteria = forClass(AgendaContact.class);
		} else {
			criteria = forClass(AgendaContact.class).add(disjunction()
							.add(ilike("name", name, ANYWHERE))
							.add(ilike("profession", name, ANYWHERE))
							.add(ilike("company", name, ANYWHERE))
							.add(ilike("email", name, ANYWHERE))
                            .add(ilike("info", name, ANYWHERE))
			);
		}
		criteria.add(eq("excluded", false));

		return criteria;
	}

	@Override
	public int countByName(String name) {
		return rowCount(searchByNameCriteria(name));
	}

	@Override
	public List<AgendaContact> findByName(String name, int firstResult, int maxResult) {
		return findByCriteria(
				searchByNameCriteria(name)
						.addOrder(asc("name").ignoreCase()),
				firstResult, maxResult, AgendaContact.class
		);
	}

	@Override
	public AgendaContact findMatch(AgendaContact contact) {
		DetachedCriteria criteria = forClass(AgendaContact.class);

		// NOTE Tem que ser tudo igual, para não perder atualizações
		criteria.add(eq("name", contact.getName()));
		criteria.add(eq("profession", contact.getProfession()));
		criteria.add(eq("company", contact.getCompany()));
		criteria.add(eq("email", contact.getEmail()));

		List<AgendaContact> matches = findByCriteria(criteria, AgendaContact.class);

		return matches.isEmpty() ? null : matches.get(0);
	}

	@Override
	public int countByContactGroup(ContactGroup contactGroup) {
		DetachedCriteria criteria = forClass(AgendaContact.class);
		if (contactGroup != null) {
			criteria.add(Restrictions.eq("contactGroup", contactGroup));
		}
		return rowCount(criteria);
	}

	@Override
	public List<AgendaContact> listExcludedContacts(int maxResult) {
		DetachedCriteria criteria = forClass(AgendaContact.class);

		criteria.add(Restrictions.eq("excluded", true));
		criteria.add(Restrictions.isEmpty("guides"));
		criteria.add(Restrictions.isEmpty("news"));

		return findByCriteria(criteria, 0, maxResult, AgendaContact.class);
	}

	@Override
	public List<Contact> listNotUsedContacts(int maxResult) {
		DetachedCriteria criteria = forClass(Contact.class);

		criteria.add(Restrictions.ne("class", AgendaContact.class));
		criteria.add(Restrictions.isEmpty("guides"));
		criteria.add(Restrictions.isEmpty("news"));

		return findByCriteria(criteria, 0, maxResult);
	}
}
