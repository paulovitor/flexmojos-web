package tv.snews.anews.dao.impl;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.domain.Device;
import tv.snews.anews.domain.IIDevicePath;
import tv.snews.anews.domain.Program;

import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.or;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class ProgramDaoImpl extends GenericDaoImpl<Program> implements ProgramDao {

	@Override
	public int countByDevice(Device device) {
		return rowCount(
				forClass(Program.class).add(or(eq("cgDevice", device), eq("playoutDevice", device)))
		);
	}

	@Override
	public int countByPath(IIDevicePath path) {
		return rowCount(
				forClass(Program.class).add(eq("cgPath", path))
		);
	}

	@Override
	public List<Program> listAll() {
		DetachedCriteria criteria = forClass(Program.class);
		criteria.add(eq("disabled", false));
		criteria.addOrder(asc("name").ignoreCase());
		criteria.addOrder(asc("start"));
		return findByCriteria(criteria);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Program> listAllOrderByStart() {
		DetachedCriteria criteria = forClass(Program.class);
		criteria.add(eq("disabled", false));
		criteria.addOrder(asc("start"));
		criteria.addOrder(asc("name").ignoreCase());
		return findByCriteria(criteria);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Program> listAllWithDisableds(FetchType... fetchTypes) {
		DetachedCriteria criteria = forClass(Program.class);
		criteria.addOrder(asc("name").ignoreCase());
        setFetchTypes(criteria, fetchTypes);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return findByCriteria(criteria);
	}

    @Override
	public Program getByName(String name) {
		DetachedCriteria criteria = forClass(Program.class);
		criteria.add(eq("name", name));
		return uniqueResult(criteria);
	}

	@Override
	public Program get(Integer programId, FetchType... fetchTypes) {
		DetachedCriteria criteria = forClass(Program.class);
		criteria.add(eq("disabled", false));
		criteria.add(eq("id", programId));
        setFetchTypes(criteria, fetchTypes);
		return uniqueResult(criteria);
	}

    private void setFetchTypes(DetachedCriteria criteria, FetchType[] fetchTypes) {
        for (FetchType fetch : fetchTypes) {
            switch (fetch) {
                case CG:
                    criteria.setFetchMode("cgDevice", JOIN);
                    break;
                case EDITORS:
                    criteria.setFetchMode("editors", JOIN);
                    break;
                case IMAGE_EDITORS:
                    criteria.setFetchMode("imageEditors", JOIN);
                    break;
                case PRESENTERS:
                    criteria.setFetchMode("presenters", JOIN);
                    break;
                default:
                    break;
            }
        }
    }
}
