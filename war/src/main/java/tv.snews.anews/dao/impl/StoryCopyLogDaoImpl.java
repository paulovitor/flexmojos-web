package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.StoryCopyLogDao;
import tv.snews.anews.domain.StoryCopyLog;

/**
 * @author Samuel Guedes de Melo
 */
public class StoryCopyLogDaoImpl extends GenericDaoImpl<StoryCopyLog> implements StoryCopyLogDao {
}
