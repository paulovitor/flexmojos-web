package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.RssItemDao;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.domain.RssItem;

import java.util.Date;
import java.util.List;

/**
 * Implementação default da interface {@link tv.snews.anews.dao.RssItemDao} utilizando o suporte a
 * Hibernate oferecido pelo Spring.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssItemDaoImpl extends GenericDaoImpl<RssItem> implements RssItemDao {

    @Override
    public int countAll() {
	    return countFrom(null, null);
    }

    @Override
    public int countFromCategory(RssCategory category) {
    	return countFrom(category, null);
    }
    
    @Override
    public int countFromFeed(RssFeed feed) {
    	return countFrom(null, feed);
    }

    @Override
    public List<RssItem> listPage(int firstResult, int maxResult) {
	    return listPage(null, null, firstResult, maxResult);
    }

    @Override
    public List<RssItem> listPage(RssCategory category, int firstResult, int maxResult) {
    	return listPage(category, null, firstResult, maxResult);
    }
    
    @Override
    public List<RssItem> listPage(RssFeed feed, int firstResult, int maxResult) {
    	return listPage(null, feed, firstResult, maxResult);
    }
    
    @Override
    public List<RssItem> listItemsOlderThan(Date minDate, int maxResults) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(RssItem.class);
    	criteria.add(Restrictions.lt("published", minDate));
        return findByCriteria(criteria, 0, maxResults);
    }
    
    //--------------------------------------
    //	Helpers
    //--------------------------------------
    
    private int countFrom(RssCategory category, RssFeed feed) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(RssItem.class);
    	if (category != null) {
    		criteria.createCriteria("rssFeed").add(Restrictions.eq("category", category));
    	}
    	if (feed != null) {
    		criteria.add(Restrictions.eq("rssFeed", feed));
    	}
	    return rowCount(criteria);
    }
    
    private List<RssItem> listPage(RssCategory category, RssFeed feed, int firstResult, int maxResult) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(RssItem.class);
    	criteria.addOrder(Order.desc("published"));
    	if (category != null) {
    		criteria.createCriteria("rssFeed").add(Restrictions.eq("category", category));
    	}
    	if (feed != null) {
    		criteria.add(Restrictions.eq("rssFeed", feed));
    	}
	    return findByCriteria(criteria, firstResult, maxResult);
    }
}
