package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.LogDao;
import tv.snews.anews.domain.*;

import java.util.List;

/**
 * Implementação da camada de persistência para log do espelho.
 *
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class LogDaoImpl extends GenericDaoImpl<AbstractLog> implements LogDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<AbstractLog> listAll(Rundown rundown) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));
		criteria.addOrder(Order.desc("date"));
		return findByCriteria(criteria);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<RundownLog> listLogsByRundownAction(Rundown rundown, List<RundownAction> actions, int pageNumber) {
		DetachedCriteria criteria = DetachedCriteria.forClass(RundownLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));
		criteria.addOrder(Order.desc("date"));

		Disjunction disjunction = Restrictions.disjunction();
		for (RundownAction rundownAction : actions) {
			disjunction.add(Restrictions.eq("rundownAction", rundownAction));
		}
		criteria.add(disjunction);

		return findByCriteria(criteria, getFirstResult(pageNumber), RundownLog.class);
	}

	@SuppressWarnings("unchecked")
	public List<BlockLog> listLogsByBlockAction(Rundown rundown, List<BlockAction> actions, int pageNumber) {
		DetachedCriteria criteria = DetachedCriteria.forClass(BlockLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));
		criteria.addOrder(Order.desc("date"));

		Disjunction disjunction = Restrictions.disjunction();
		for (BlockAction blockAction : actions) {
			disjunction.add(Restrictions.eq("blockAction", blockAction));
		}
		criteria.add(disjunction);

		return findByCriteria(criteria, getFirstResult(pageNumber), BlockLog.class);
	}

	@SuppressWarnings("unchecked")
	public List<StoryLog> listLogsByStoryAction(Rundown rundown, List<StoryAction> actions, int pageNumber) {
		DetachedCriteria criteria = DetachedCriteria.forClass(StoryLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));
		criteria.addOrder(Order.desc("date"));

		Disjunction disjunction = Restrictions.disjunction();
		for (StoryAction storyAction : actions) {
			disjunction.add(Restrictions.eq("storyAction", storyAction));
		}
		criteria.add(disjunction);

		return findByCriteria(criteria, getFirstResult(pageNumber), StoryLog.class);
	}

	@Override
	public List<StoryLog> findStoryLogsByStory(Story story) {
		DetachedCriteria criteria = DetachedCriteria.forClass(StoryLog.class);
		criteria.add(Restrictions.eq("story", story));
		return findByCriteria(criteria, StoryLog.class);
	}

	@Override
	public int totalStory(Rundown rundown, List<StoryAction> storyActions) {
		DetachedCriteria criteria = DetachedCriteria.forClass(StoryLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));

		Disjunction disjunction = Restrictions.disjunction();
		for (StoryAction storyAction : storyActions) {
			disjunction.add(Restrictions.eq("storyAction", storyAction));
		}
		criteria.add(disjunction);

		return rowCount(criteria);
	}

	@Override
	public int totalRundown(Rundown rundown, List<RundownAction> rundownActions) {
		DetachedCriteria criteria = DetachedCriteria.forClass(RundownLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));

		Disjunction disjunction = Restrictions.disjunction();
		for (RundownAction storyAction : rundownActions) {
			disjunction.add(Restrictions.eq("rundownAction", storyAction));
		}
		criteria.add(disjunction);

		return rowCount(criteria);
	}

	@Override
	public int totalBlock(Rundown rundown, List<BlockAction> blockActions) {
		DetachedCriteria criteria = DetachedCriteria.forClass(BlockLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));

		Disjunction disjunction = Restrictions.disjunction();
		for (BlockAction blockAction : blockActions) {
			disjunction.add(Restrictions.eq("blockAction", blockAction));
		}
		criteria.add(disjunction);

		return rowCount(criteria);
	}

	@Override
	public int totalAll(Rundown rundown) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AbstractLog.class);
		criteria.add(Restrictions.eq("rundown", rundown));
		return rowCount(criteria);
	}
}
