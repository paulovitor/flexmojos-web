package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.NewsDao;
import tv.snews.anews.domain.News;
import tv.snews.anews.search.NewsParams;

import java.util.List;

import static org.hibernate.criterion.Restrictions.ge;
import static org.hibernate.criterion.Restrictions.le;

/**
 * Implementação do Contrato NewsDao para acesso ao dados das notícias.
 *
 * @author Paulo Felipe
 * @author Samuel Guedes
 * @since 1.0.0
 */
public class NewsDaoImpl extends GenericDaoImpl<News> implements NewsDao {

    @Override
    public List<News> listAll() {
        return findByCriteria(createCriteriaWithParams(null).addOrder(Order.desc("date")));
    }

    @Override
    public List<News> listNewsOlderThan(int firstResult, int maxResults) {
        return findByCriteria(createCriteriaWithParams(null).addOrder(Order.desc("date")), firstResult, maxResults);
    }

    @Override
    public int totalByNews() {
        return rowCount(DetachedCriteria.forClass(News.class));
    }

    @Override
    public int totalByParams(NewsParams params) {
        return rowCount(createCriteriaWithParams(params));
    }

    @Override
    public List<News> listAllPage(int pageNumber) {
        return findByCriteria(createCriteriaWithParams(null).addOrder(Order.desc("date")), getFirstResult(pageNumber), News.RESULTS_BY_PAGE);
    }

    @Override
    public List<News> findByParams(NewsParams params) {
        return findByCriteria(createCriteriaWithParams(params).addOrder(Order.desc("date")), getFirstResult(params.getPage()), News.RESULTS_BY_PAGE);
    }

    private DetachedCriteria createCriteriaWithParams(NewsParams params) {
        DetachedCriteria criteria = DetachedCriteria.forClass(News.class);
        if (params != null) {
            if (params.getInitialDate() != null) {
                criteria.add(ge("date", params.getInitialDate()));
            }
            if (params.getFinalDate() != null) {
                criteria.add(le("date", params.getFinalDate()));
            }
            if (params.getSlug() != null) {
                criteria.add(Restrictions.ilike("slug", params.getSlug(), MatchMode.ANYWHERE));
            }
        }
        return criteria;
    }
}
