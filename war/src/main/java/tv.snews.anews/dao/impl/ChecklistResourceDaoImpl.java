package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ChecklistResourceDao;
import tv.snews.anews.domain.ChecklistResource;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;

/**
 * Implementação da interface {@link ChecklistResourceDao} que utiliza Hibernate em
 * sua implementação.
 *
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public class ChecklistResourceDaoImpl extends GenericDaoImpl<ChecklistResource> implements ChecklistResourceDao {

	@Override
	public List<ChecklistResource> findAll() {
		return findByCriteria(
				forClass(ChecklistResource.class).addOrder(asc("name"))
		);
	}
}
