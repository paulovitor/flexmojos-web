package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ChecklistTypeDao;
import tv.snews.anews.domain.ChecklistType;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class ChecklistTypeDaoImpl extends GenericDaoImpl<ChecklistType> implements ChecklistTypeDao {

    @Override
    public List<ChecklistType> findAll() {
        return findByCriteria(
            forClass(ChecklistType.class).addOrder(asc("name"))
        );
    }
}
