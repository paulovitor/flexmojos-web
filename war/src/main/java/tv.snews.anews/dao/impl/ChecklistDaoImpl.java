package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.ChecklistDao;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.search.ChecklistParams;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo.
 * @since 1.5
 */
public class ChecklistDaoImpl extends GenericDaoImpl<Checklist> implements ChecklistDao {

	@Override
	public Serializable save(Checklist entity) {
		// Gera revisão com os últimos dados
		entity.createRevision();
		return super.save(entity);
	}

	@Override
	public void update(Checklist entity) {
		// Gera revisão com os últimos dados
		entity.createRevision();
		super.update(entity);
	}

	@Override
	public Checklist get(Serializable id) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Checklist.class);
		criteria.add(Restrictions.idEq(id));

		// Esses joins são importantes para carregar coleções para relatório
		setFetchMode(criteria);
		return uniqueResult(criteria);
	}

	@Override
	public Checklist merge(Checklist entity) {
		// Gera revisão com os últimos dados
		entity.createRevision();
		return super.merge(entity);
	}


	@Override
	public Checklist findByGuideline(Guideline guideline) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Checklist.class);
		criteria.add(Restrictions.eq("guideline", guideline));
		criteria.addOrder(asc("date"));
		// Esses joins são importantes para carregar coleções para relatório
		setFetchMode(criteria);
		return uniqueResult(criteria);
	}

	public int countAllByType(ChecklistType type) {
		return rowCount(
				forClass(Checklist.class).add(eq("type", type))
		);
	}

	@Override
	public int countAllByResource(ChecklistResource checklistResource) {
		return rowCount(
				forClass(Checklist.class).createAlias("resources", "resources").add(eq("resources.id", checklistResource.getId()))
		);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Checklist> findAllExcludedBefore(Date date, int maxResults) {
		return findByCriteria(
				forClass(Checklist.class)
						.add(lt("modified", date))
						.add(eq("excluded", true))
				, 0, maxResults
		);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Checklist> findAllNotExcludedOfDate(Date date, boolean notDone) {
		// Busca os IDs das produções da data informada ou que possue uma tarefa para a data informada.
		List<Long> ids = findByCriteria(createCriteriaForAllNotExcludedOfDate(date, notDone), Long.class);

		if (!ids.isEmpty()) {
			// Busca as produções dos IDs encontrados
			DetachedCriteria criteria =
					forClass(Checklist.class)
							.add(in("id", ids))
							.addOrder(asc("schedule"))
							.addOrder(asc("slug").ignoreCase());

			setFetchMode(criteria);

			return findByCriteria(criteria);
		}
		return new ArrayList<>();
	}

    private DetachedCriteria createCriteriaForAllNotExcludedOfDate(Date date, boolean notDone) {
    	DetachedCriteria criteria = forClass(Checklist.class)
				.add(eq("excluded", false))
	    		.createAlias("tasks", "task", JoinType.LEFT_OUTER_JOIN)
	    		.add(or(eq("date", date), eq("task.date", date)))
	    		.setProjection(Projections.id());
    	if (notDone)
    		criteria.add(eq("done", !notDone));
    	return criteria;
    }

	public int countByParams(ChecklistParams params) {
		DetachedCriteria criteria = createCriteriaWithParams(params);
		return rowCount(criteria);
	}

	@Override
	public List<Checklist> findByParams(ChecklistParams params) {
		return findByParams(params, DEFAULT_PAGE_SIZE);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Checklist> findByParams(ChecklistParams params, int maxResults) {
		int firstResult = params.getPage() > 0 ? (params.getPage() - 1) * maxResults : 0;

		List<Long> ids = findByCriteria(
				createCriteriaWithParams(params)
						.addOrder(desc("date"))
						.addOrder(desc("modified"))
						.setProjection(Projections.id()),
				firstResult, maxResults, Long.class
		);

		if (!ids.isEmpty()) {
			DetachedCriteria criteria = forClass(Checklist.class).add(in("id", ids));
			criteria.addOrder(desc("date")).addOrder(desc("modified"));
			setFetchMode(criteria);

			return findByCriteria(criteria);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Checklist> listExcludedPage(int pageNumber) {
		List<Long> ids = findByCriteria(
				forClass(Checklist.class)
						.add(eq("excluded", true))
						.addOrder(desc("modified"))
						.setProjection(Projections.id()),
				getFirstResult(pageNumber), Long.class
		);

		if (!ids.isEmpty()) {
			DetachedCriteria criteria = forClass(Checklist.class).add(in("id", ids));
			criteria.add(eq("excluded", true)).addOrder(desc("modified"));
			setFetchMode(criteria);

			return findByCriteria(criteria);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public int countExcluded() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Checklist.class);
		criteria.add(Restrictions.eq("excluded", true));
		return rowCount(criteria);
	}

	//------------------------------------
	//  Helpers
	//------------------------------------

	private void setFetchMode(DetachedCriteria criteria) {
		criteria.setFetchMode("tasks", JOIN);
		criteria.setFetchMode("resources", JOIN);
		criteria.setFetchMode("revisions", JOIN);
		criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);
	}

	private DetachedCriteria createCriteriaWithParams(ChecklistParams params) {
		DetachedCriteria criteria = forClass(Checklist.class);
		criteria.add(eq("excluded", false));

		// Data inicial
		if (params.getInitialDate() != null) {
			criteria.add(ge("date", params.getInitialDate()));
		}

		// Data final
		if (params.getFinalDate() != null) {
			criteria.add(le("date", params.getFinalDate()));
		}

		// Texto
//    	if (isNotBlank(params.getText())) {
//    		criteria.add(or(
//				ilike("slug", params.getText(), MatchMode.ANYWHERE),
//    			like("content", params.getText(), MatchMode.ANYWHERE))
//			);
//    	}

		// Responsável
		if (params.getProducer() != null) {
			criteria.add(eq("producer", params.getProducer()));
		}

		// Tipo de matéria
		if (params.getType() != null) {
			criteria.add(eq("type", params.getType()));
		}

		return criteria;
	}
}
