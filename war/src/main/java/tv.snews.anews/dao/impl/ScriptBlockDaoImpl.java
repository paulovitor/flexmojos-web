package tv.snews.anews.dao.impl;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.ScriptBlockDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.ScriptBlock;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
public class ScriptBlockDaoImpl extends GenericDaoImpl<ScriptBlock> implements ScriptBlockDao {

    @Override
    public ScriptBlock loadById(Serializable idScriptBlock, boolean fetchAll) {
        if (fetchAll) {
            DetachedCriteria criteria = forClass(ScriptBlock.class);
            criteria.add(Restrictions.idEq(idScriptBlock));
            setFetchMode(criteria);
            getHibernateTemplate().setMaxResults(0);
            List<ScriptBlock> blocks = findByCriteria(criteria);
            return blocks.isEmpty() ? null : blocks.get(0);
        }
        return super.get(idScriptBlock);
    }

	@Override
	public ScriptBlock findLastStandByBlock(Program program, Date date) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ScriptBlock.class, "scriptBlock");

		criteria.createAlias("scriptBlock.scriptPlan", "scriptPlan");
		criteria.addOrder(Order.desc("scriptPlan.date"));
		criteria.add(Restrictions.le("scriptPlan.date", new Date()));
		criteria.add(Restrictions.eq("scriptPlan.program", program));
		// Recupera o bloco de apoio de algum programa anterior ao dia de hoje,
		// para evitar que seja copiado de algum programa futuro
		criteria.add(Restrictions.eq("standBy", true));
		getHibernateTemplate().setMaxResults(1);

		List<ScriptBlock> scriptBlockList = findByCriteria(criteria);
		return scriptBlockList.isEmpty() ? null : scriptBlockList.get(0);
	}

    private void setFetchMode(DetachedCriteria criteria) {
        criteria.setFetchMode("scripts", JOIN);
        criteria.createCriteria("scriptPlan", JoinType.LEFT_OUTER_JOIN).setFetchMode("blocks", JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    }
}
