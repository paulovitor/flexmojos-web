package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import tv.snews.anews.dao.SegmentDao;
import tv.snews.anews.domain.Segment;

import java.util.List;

/**
 * Implementação da persistência dos segments.
 *
 * @author Paulo Felipe de Araújo.
 * @since 1.0.0
 */
public class SegmentDaoImpl extends GenericDaoImpl<Segment> implements SegmentDao {

	@Override
	public List<Segment> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Segment.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}
}
