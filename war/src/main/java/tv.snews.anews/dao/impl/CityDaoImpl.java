package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.CityDao;
import tv.snews.anews.domain.City;

import java.io.Serializable;
import java.util.List;

/**
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class CityDaoImpl extends GenericDaoImpl<City> implements CityDao {

	@Override
	public List<City> listAll(Serializable idState) {
		DetachedCriteria criteria = DetachedCriteria.forClass(City.class);
		criteria.createAlias("state", "state", JoinType.FULL_JOIN);
		criteria.add(Restrictions.eq("state.id", idState));
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}
}
