package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.StoryRevisionDao;
import tv.snews.anews.domain.StoryRevision;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class StoryRevisionDaoImpl extends GenericDaoImpl<StoryRevision> implements StoryRevisionDao {

	@Override
	public List<StoryRevision> listDateLessThan(Date date, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(StoryRevision.class);
		criteria.add(Restrictions.lt("revisionDate", date));
		return findByCriteria(criteria, 0, maxResults);
	}

}
