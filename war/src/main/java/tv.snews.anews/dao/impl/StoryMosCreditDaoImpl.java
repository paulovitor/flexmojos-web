package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.StoryMosCreditDao;
import tv.snews.anews.domain.StoryMosCredit;

import java.util.List;

import static org.hibernate.criterion.Restrictions.eq;


/**
 * 
 * @author snews
 * @since 1.3.0
 */
public class StoryMosCreditDaoImpl extends GenericDaoImpl<StoryMosCredit> implements StoryMosCreditDao {

	@Override
	public List<StoryMosCredit> getByMosMedia(Integer mosMediaId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(StoryMosCredit.class);
		criteria.createAlias("mosMedia", "mosMedia");
		criteria.add(eq("mosMedia.id", mosMediaId));
		return findByCriteria(criteria);
	}

}
