package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ReportageCopyLogDao;
import tv.snews.anews.domain.ReportageCopyLog;

/**
 * @author Felipe Pinheiro
 */
public class ReportageCopyLogDaoImpl extends GenericDaoImpl<ReportageCopyLog> implements ReportageCopyLogDao {
}
