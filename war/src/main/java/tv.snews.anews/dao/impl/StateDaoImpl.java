package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import tv.snews.anews.dao.StateDao;
import tv.snews.anews.domain.State;

import java.util.List;

/**
 * Implementação da persistência do estado.
 *
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class StateDaoImpl extends GenericDaoImpl<State> implements StateDao {

	@Override
	public List<State> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(State.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}

}
