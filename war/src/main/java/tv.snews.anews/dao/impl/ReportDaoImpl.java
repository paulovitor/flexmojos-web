package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.GenericDao;
import tv.snews.anews.dao.ReportDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.search.ReportParams;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * Implementa a interface {@link ReportDao} utilizando Hibernate em sua
 * implementação.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportDaoImpl extends GenericDaoImpl<Report> implements ReportDao {

    @Override
    public int countByParams(ReportParams params) {
    	DetachedCriteria criteria = createCriteriaWithParams(params);
	    return rowCount(criteria);
    }

    @Override
    public int countAllByEvent(ReportEvent event) {
    	return rowCount(
					forClass(Report.class).add(eq("event", event))
			);
    }
    
    @Override
    public int countAllByNature(ReportNature nature) {
        return rowCount(
						forClass(Report.class).add(eq("nature", nature))
				);
    }
    
    @Override
    public List<Report> findByParams(ReportParams params) {
    	int firstResult = params.getPage() > 0 ? (params.getPage() - 1) * GenericDao.DEFAULT_PAGE_SIZE : 0;
        return findByParams(params, firstResult, DEFAULT_PAGE_SIZE);
    }
    
    @Override
    public List<Report> findByParams(ReportParams params, int firstResult, int maxResults) {
    	return findByCriteria(
    		createCriteriaWithParams(params)
    			.addOrder(desc("date"))
    			.addOrder(desc("lastChange")), 
    		firstResult, maxResults
		);
    }
    
    //------------------------------------
	//  Helpers
	//------------------------------------
    
    private DetachedCriteria createCriteriaWithParams(ReportParams params) {
    	DetachedCriteria criteria;
    	
    	// Tipo de origem define a classe root da criteria
    	switch (params.getOrigin()) {
    		case GUIDELINE:
    			criteria = forClass(GuidelineReport.class);
    			break;
    		case REPORTAGE:
    			criteria = forClass(ReportageReport.class);
    			break;
    		case STORY:
    			criteria = forClass(StoryReport.class);
    			break;
			default:
				criteria = forClass(Report.class);
    	}
    	
    	// Data inicial
    	if (params.getInitialDate() != null) {
    		criteria.add(ge("date", params.getInitialDate()));
    	}
    	
    	// Data final
    	if (params.getFinalDate() != null) {
    		criteria.add(le("date", params.getFinalDate()));
    	}
    	
    	// Texto
//    	if (isNotBlank(params.getText())) {
//    		criteria.add(or(
//				ilike("slug", params.getText(), MatchMode.ANYWHERE),
//    			like("content", params.getText(), MatchMode.ANYWHERE))
//			);
//    	}
    	
    	// Tipo de evento
    	if (params.getEvent() != null) {
    		criteria.add(eq("event", params.getEvent()));
    	}
    	
    	// Tipo de natureza
    	if (params.getNature() != null) {
    		criteria.add(eq("nature", params.getNature()));
    	}
    	
    	return criteria;
    }
}
