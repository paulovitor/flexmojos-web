package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.CompanyDao;
import tv.snews.anews.domain.Company;

import java.util.List;

/**
 * Implementação da persistência das praças.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.3
 */
public class CompanyDaoImpl extends GenericDaoImpl<Company> implements CompanyDao {
	
	@Override
	public List<Company> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Company.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}
	
	@Override
	public List<Company> listCompanyByNameHost(String name, String host) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Company.class);
		criteria.add(Restrictions.or(Restrictions.eq("host", host), Restrictions.eq("name", name)));
		criteria.addOrder(Order.desc("name"));
		return findByCriteria(criteria);
	}
}
