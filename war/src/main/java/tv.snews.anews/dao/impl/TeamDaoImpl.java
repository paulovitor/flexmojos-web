package tv.snews.anews.dao.impl;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.TeamDao;
import tv.snews.anews.domain.Team;

import java.util.List;

import static org.hibernate.criterion.Order.asc;

/**
 * Implementação da persistência das equipes.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public class TeamDaoImpl extends GenericDaoImpl<Team> implements TeamDao {

	@Override
	public List<Team> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Team.class);
		criteria.addOrder(Order.desc("name"));

		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return findByCriteria(criteria);
	}
	
	public Team listTeamByName(String name) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Team.class);
		criteria.add(Restrictions.eq("name", name));
		criteria.addOrder(Order.desc("name"));
        criteria.addOrder(asc("name").ignoreCase());
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
		return uniqueResult(criteria);
	}
}
