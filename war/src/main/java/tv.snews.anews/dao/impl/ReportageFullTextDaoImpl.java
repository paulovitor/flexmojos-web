package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.ReportageFullTextDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.infra.search.ProgramLuceneBridge;
import tv.snews.anews.search.ReportageParams;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;

/**
 * Implementação das consultas com Hibernate Search
 * para Guideline.
 *  
 * @author Felipe Zap de Mello
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportageFullTextDaoImpl extends FullTextSearchDao implements ReportageFullTextDao {

	private static final String SLUG = "slug";
	private static final String CAMERAMAN = "cameraman";
//	private static final String EXCLUDED = "excluded";
	private static final String CONTENT = "sections.content";
	private static final String OBSERVATION = "sections.observation";
	
	private static final String DATE = "date";
	private static final String PROGRAM = "program";
	private static final String REPORTER = "reporter.nickname";
	private static final String INFORMATION = "information";
	private static final String TIP_OF_HEAD = "tipOfHead";
	
	private static final String[] QUERY_PARSER_FIELDS = new String[] { 
		CAMERAMAN, CONTENT, OBSERVATION, INFORMATION, TIP_OF_HEAD
	};

	@Override
	public int totalBySearchReportage(ReportageParams searchParams) {
		Query luceneQuery = getLuceneQuery(searchParams);
		return countResults(luceneQuery, Reportage.class);
	}

	@Override
	@SuppressWarnings({"unchecked"})
	public List<Reportage> searchReportageGeneral(ReportageParams searchParams) {
		Query luceneQuery = getLuceneQuery(searchParams);

		DetachedCriteria criteria = DetachedCriteria.forClass(Reportage.class)
				.setFetchMode("sections", FetchMode.JOIN)
				.setFetchMode("copiesHistory", FetchMode.JOIN)
				.setResultTransformer(DISTINCT_ROOT_ENTITY);

		List<Reportage> reportages = listResults(
				luceneQuery, Reportage.class,
				firstResult(searchParams.getPage()), DEFAULT_PAGE_SIZE,
				sort(DATE, true),
				criteria
		);

		applyHighlightTerms(reportages, getDefaultHighlighter(Reportage.class, luceneQuery));

		return reportages;
	}

    @Override
    public List<Reportage> listTheFirstHundredByParams(ReportageParams searchParams) {
        Query luceneQuery = getLuceneQuery(searchParams);

        return listResults(
                luceneQuery, Reportage.class,
                firstResult(searchParams.getPage()), 100,
                sort(DATE, true)
        );
    }

	private Query getLuceneQuery(ReportageParams searchParams) {
		Date initialDate = searchParams.getInitialDate();
		Date finalDate = searchParams.getFinalDate();
		String text = searchParams.getText();
		String ignoreText = searchParams.getIgnoreText();
		User reporter = searchParams.getReporter();
		Program program = searchParams.getProgram();
		SearchType searchType = searchParams.getSearchType();

		//parser.setAllowLeadingWildcard(false);

		LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(searchType, text, ignoreText);
		queryBuilder.appendDatePeriod(DATE, initialDate, finalDate);
		queryBuilder.appendFieldTerm(SLUG, searchParams.getSlug());
//		queryBuilder.appendFieldTerm(EXCLUDED, false); || Excluídas não estão mais sendo indexadas

		if (program != null && program.getId() > 0) {
			queryBuilder.appendFieldTerm(PROGRAM, program.getName());
		} else if (program != null && program.getId() == -1) {
			queryBuilder.appendFieldTerm(PROGRAM, ProgramLuceneBridge.NULL);
		}

		if (reporter != null) {
			queryBuilder.appendFieldTerm(REPORTER, reporter.getNickname());
		}

		return parseQuery(queryBuilder, Reportage.class, QUERY_PARSER_FIELDS);
	}

	private void applyHighlightTerms(List<Reportage> reportages, Highlighter highlighter) {
    	Analyzer analyzer = getAnalyzer(Reportage.class);
    	
    	for (Reportage reportage : reportages) {
    		getHibernateTemplate().evict(reportage);
    		
			try {
				if (reportage.getProgram() != null) {
					if (!reportage.getProgram().getName().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
						String programName = highlighter.getBestFragment(analyzer, PROGRAM, reportage.getProgram().getName());
						if (programName != null) {
							reportage.getProgram().setName(fixWhiteSpace(programName));
	                    }
					}
				}
				if (!reportage.getReporter().getNickname().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
					String reporter = highlighter.getBestFragment(analyzer, REPORTER, reportage.getReporter().getNickname());
					if (reporter != null) {
						reportage.getReporter().setNickname(fixWhiteSpace(reporter));
					}
				}
				
				if (reportage.getInformation() != null) {
					String information = highlighter.getBestFragment(analyzer, INFORMATION, reportage.getInformation());
					if (information != null) {
						reportage.setInformation(fixWhiteSpace(information));
					}
				}

				if (reportage.getTipOfHead() != null) {
					String tipOfHead = highlighter.getBestFragment(analyzer, TIP_OF_HEAD, reportage.getTipOfHead());
					if (tipOfHead != null) {
						reportage.setTipOfHead(fixWhiteSpace(tipOfHead));
					}
				}
				
				String slug = highlighter.getBestFragment(analyzer, SLUG, reportage.getSlug());
				if (slug != null) {
					reportage.setSlug(fixWhiteSpace(slug));
				}
				
				if (reportage.getCameraman() != null) {
					String cameraman = highlighter.getBestFragment(analyzer, CAMERAMAN, reportage.getCameraman());
					if (cameraman != null) {
						reportage.setCameraman(fixWhiteSpace(cameraman));
					}
				}
				
				for (ReportageSection section : reportage.getSections()) {
					if (section.getContent() != null) {
						String content = highlighter.getBestFragment(analyzer, CONTENT, section.getContent());
						if (content != null) {
							section.setContent(fixWhiteSpace(content));
						}
					}
					if (section.getObservation() != null) {
						String observation = highlighter.getBestFragment(analyzer, OBSERVATION, section.getObservation());
						if (observation != null) {
							section.setObservation(fixWhiteSpace(observation));
						}
					}
                }
			} catch (IOException | InvalidTokenOffsetsException e) {
				throw new RuntimeException("Failed to highlight search terms.", e);
			}
		}
    }
}
