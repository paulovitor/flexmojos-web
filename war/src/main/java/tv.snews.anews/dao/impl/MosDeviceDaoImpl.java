package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.MosDeviceDao;
import tv.snews.anews.domain.MosDevice;

import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.eq;

/**
 * Implementação da interface {@link MosDeviceDao} utilizando o suporte do
 * Hibernate + Spring.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class MosDeviceDaoImpl extends GenericDaoImpl<MosDevice> implements MosDeviceDao {

	@Override
	public MosDevice findByIdentifier(String identifier) {
		return uniqueResult(
				forClass(MosDevice.class)
						.add(eq("mosId", identifier))
						.setFetchMode("channels", JOIN)
						.setFetchMode("types", JOIN)
						.setResultTransformer(DISTINCT_ROOT_ENTITY)
		);
	}

//    @Override
//    @SuppressWarnings("unchecked")
//    public List<MosDevice> findAllByType(MosDeviceTypeEnum mosDeviceType) {
//        return getHibernateTemplate().findByCriteria(
//                forClass(MosDevice.class)
//                        .createAlias("types", "type")
//                        .add(eq("type.description", mosDeviceType))
//                        .setFetchMode("channels", JOIN)
//                        .setFetchMode("types", JOIN)
//                        .setResultTransformer(DISTINCT_ROOT_ENTITY)
//                        .addOrder(asc("name").ignoreCase())
//        );
//    }

//    @Override
//    @SuppressWarnings("unchecked")
//    public List<MosDevice> list() {
//        return getHibernateTemplate().findByCriteria(
//                forClass(MosDevice.class)
//                        .setFetchMode("channels", JOIN)
//                        .setFetchMode("types", JOIN)
//                        .setResultTransformer(DISTINCT_ROOT_ENTITY)
//                        .addOrder(asc("name").ignoreCase())
//        );
//    }

	@Override
	@SuppressWarnings("unchecked")
	public List<MosDevice> listByProfileSupport(int profileNumber) {
		return findByCriteria(
				forClass(MosDevice.class)
						.add(eq(profilePropertyByNumber(profileNumber), true))
						.setFetchMode("channels", JOIN)
						.setFetchMode("types", JOIN)
						.setResultTransformer(DISTINCT_ROOT_ENTITY)
						.addOrder(asc("name").ignoreCase())
		);
	}

	private String profilePropertyByNumber(int profileNumber) {
		switch (profileNumber) {
			case 1:
				return "profileOneSupported";
			case 2:
				return "profileTwoSupported";
			case 3:
				return "profileThreeSupported";
			case 4:
				return "profileFourSupported";
			case 5:
				return "profileFiveSupported";
			case 6:
				return "profileSixSupported";
			case 7:
				return "profileSevenSupported";
			default:
				throw new IllegalArgumentException("Invalid MOS profile number: " + profileNumber + ". It must be greater than zero and less than 8.");
		}
	}
}
