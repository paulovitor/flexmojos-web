package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ScriptVideoDao;
import tv.snews.anews.domain.MosDeviceChannel;
import tv.snews.anews.domain.ScriptVideo;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Restrictions.eq;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
public class ScriptVideoDaoImpl extends GenericDaoImpl<ScriptVideo> implements ScriptVideoDao {


    @Override
    public int countByChannel(MosDeviceChannel channel) {
        return rowCount(
                forClass(ScriptVideo.class).add(eq("channel", channel))
        );
    }
}
