package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.GuidelineClassificationDao;
import tv.snews.anews.domain.GuidelineClassification;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;

/**
 * Implementação da interface {@link tv.snews.anews.dao.GuidelineClassificationDao} que utiliza Hibernate em
 * sua implementação.
 *
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public class GuidelineClassificationDaoImpl extends GenericDaoImpl<GuidelineClassification> implements GuidelineClassificationDao {

	@Override
	public List<GuidelineClassification> findAll() {
		return findByCriteria(
				forClass(GuidelineClassification.class).addOrder(asc("name"))
		);
	}
}
