package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.StoryFullTextDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.search.StoryParams;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;

/**
 * Implementação das consultas com Hibernate Search para Story.
 *
 * @author Felipe Zap de Mello
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class StoryFullTextDaoImpl extends FullTextSearchDao implements StoryFullTextDao {

	private static final String SLUG = "slug";
	private static final String HEAD = "headSection";
	private static final String NC = "ncSection";
	private static final String FOOTER = "footerSection";
	private static final String OFF = "offSection";
//	private static final String EXCLUDED = "excluded";

	private static final String[] QUERY_PARSER_FIELDS = new String[]{
			HEAD, NC, FOOTER, OFF
	};
	private static final String INFORMATION = "information";
	private static final String DATE = "block.rundown.date";
	private static final String PROGRAM = "block.rundown.program.name";
	
	@Override
	public int total(String slug, String content, Program program, Date start, Date end, SearchType searchType, String textIgnore) {
		Query luceneQuery = getLuceneQuery(slug, content, program, start, end, searchType, textIgnore, false);
		return countResults(luceneQuery, Story.class);
	}

	@Override
	@SuppressWarnings({"unchecked"})
	public List<Story> listArchiveStories(String slug, String text, Program program, Date initialDate, Date finalDate, int pageNumber, SearchType searchType, String textIgnore) {
		Query luceneQuery = getLuceneQuery(slug, text, program, initialDate, finalDate, searchType, textIgnore, false);

		List<Story> stories = listResults(
				luceneQuery, Story.class,
				firstResult(pageNumber), DEFAULT_PAGE_SIZE,
				sort(DATE, true),
				createFetchCriteria()
		);

		applyHighlightTerms(stories, getDefaultHighlighter(Story.class, luceneQuery));

		return stories;
	}

	private Query getLuceneQuery(String slug, String text, Program program, Date initialDate, Date finalDate, SearchType type, String textIgnore, boolean drawer) {
		LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(type, text, textIgnore);

		queryBuilder.appendFieldTerm(SLUG, slug);
		queryBuilder.appendDatePeriod(DATE, initialDate, finalDate);
//		queryBuilder.appendFieldTerm(EXCLUDED, false); || Excluídas não estão mais sendo indexadas

		if (program != null) {
			queryBuilder.appendFieldTerm(PROGRAM, program.getName());
		}

		return parseQuery(queryBuilder, Story.class, QUERY_PARSER_FIELDS);
	}

	private void applyHighlightTerms(List<Story> stories, Highlighter highlighter) {
		Analyzer analyzer = getAnalyzer(Story.class);

		for (Story story : stories) {
			getHibernateTemplate().evict(story);

			try {
				/* Retranca */
				if (story.getSlug() != null) {
					String slug = highlighter.getBestFragment(analyzer, SLUG, story.getSlug());
					if (slug != null) {
						story.setSlug(fixWhiteSpace(slug));
					}
				}
				
				/* Cabeças */
				if (story.getHeadSection() != null) {
					for (StorySubSection subSection : story.getHeadSection().getSubSections()) {
						StoryCameraText headText = (StoryCameraText) subSection;
						String text = highlighter.getBestFragment(analyzer, HEAD, headText.getText());
						if (text != null) {
							headText.setText(fixWhiteSpace(text));
						}
					}
				}

				/* NC */
				if (story.getNcSection() != null) {
					for (StorySubSection subSection : story.getNcSection().getSubSections()) {
						if (subSection instanceof StoryText) {
							StoryText ncText = (StoryText) subSection;
							String text = highlighter.getBestFragment(analyzer, NC, ncText.getText());
							if (text != null) {
								ncText.setText(fixWhiteSpace(text));
							}
						}
					}
				}
				
				/* Nota pé */
				if (story.getFooterSection() != null) {
					for (StorySubSection subSection : story.getFooterSection().getSubSections()) {
						StoryCameraText footerText = (StoryCameraText) subSection;
						String text = highlighter.getBestFragment(analyzer, FOOTER, footerText.getText());
						if (text != null) {
							footerText.setText(fixWhiteSpace(text));
						}
					}
				}
				
				/* OFF's */
				if (story.getOffSection() != null) {
					for (StorySubSection subSection : story.getOffSection().getSubSections()) {
						StoryText offText = (StoryText) subSection;
						String text = highlighter.getBestFragment(analyzer, OFF, offText.getText());
						if (text != null) {
							offText.setText(fixWhiteSpace(text));
						}
					}
				}
				
				/* Information */
				if (story.getInformation() != null) {
					String information = highlighter.getBestFragment(analyzer, INFORMATION, story.getInformation());
					if (information != null) {
						story.setInformation(fixWhiteSpace(information));
					}
				}
				/* Programa */
				if (story.getBlock() != null/* && !programHighlighted*/) {
					if (!story.getBlock().getRundown().getProgram().getName().contains(FullTextSearchDao.OPEN_TAG_HIGHLIGHT)) {
						String programName = highlighter.getBestFragment(analyzer, PROGRAM, story.getBlock().getRundown().getProgram().getName());
						if (programName != null) {
							story.getBlock().getRundown().getProgram().setName(fixWhiteSpace(programName));
						}
					}
				}
			} catch (IOException | InvalidTokenOffsetsException e) {
				throw new RuntimeException("Failed to highlight search terms.", e);
			}
		}
	}

    @Override
    public List<Story> listTheFirstHundredByParams(StoryParams searchParams) {
        String slug = searchParams.getSlug();
        String text = searchParams.getText();
        Program program = searchParams.getProgram();
        Date initialDate = searchParams.getInitialDate();
        Date finalDate = searchParams.getFinalDate();
        SearchType searchType = searchParams.getSearchType();
        String ignoreText = searchParams.getIgnoreText();
        Query luceneQuery = getLuceneQuery(slug, text, program, initialDate, finalDate, searchType, ignoreText, false);

        return listResults(
                luceneQuery, Story.class,
                firstResult(searchParams.getPage()), 100,
                sort(DATE, true)
        );
    }
	private DetachedCriteria createFetchCriteria() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Story.class);

		criteria.createCriteria("headSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("ncSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("vtSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("footerSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("offSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.setFetchMode("copiesHistory", JOIN);
		criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);

		return criteria;
	}
}
