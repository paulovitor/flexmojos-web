package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.InstitutionDao;
import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Segment;

import java.util.List;

/**
 * Implementação da persistência dos institutos.
 * 
 * @author Paulo Felipe de Araújo.
 * @since 1.0.0
 */
public class InstitutionDaoImpl extends GenericDaoImpl<Institution> implements InstitutionDao {
	
	@Override
	public List<Institution> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Institution.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}
	
	@Override
	public List<Institution> listBySegment(Segment segment) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Institution.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		criteria.add(Restrictions.eq("segment", segment));
		return findByCriteria(criteria);
	}
	
}
