package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.DeviceDao;
import tv.snews.anews.domain.*;

import java.util.Iterator;
import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class DeviceDaoImpl extends GenericDaoImpl<Device> implements DeviceDao {

	@Override
	public List<Device> findAll() {
		return findByCriteria(
				forClass(Device.class)
						.addOrder(asc("name").ignoreCase())
		);
	}

	@Override
	public List<IIDevice> findAllIIDevices() {
		return findByCriteria(
				forClass(IIDevice.class)
						.addOrder(asc("name").ignoreCase())
				, IIDevice.class
		);
	}

	@Override
	public List<MosDevice> findAllMosDevices() {
		return findByCriteria(
				forClass(MosDevice.class)
						.addOrder(asc("name").ignoreCase()), MosDevice.class
		);
	}

	@Override
	public List<WSDevice> findAllWSDevices() {
		return findByCriteria(
				forClass(WSDevice.class)
						.addOrder(asc("name").ignoreCase()), WSDevice.class
		);
	}

	@Override
	public List<Device> findAllByType(DeviceType type) {
		List<Device> result = findAll();

		// Remove manualmente os devices que não são do tipo desejado
		Iterator<Device> it = result.iterator();
		while (it.hasNext()) {
			if (!it.next().getTypes().contains(type)) {
				it.remove();
			}
		}

		return result;
	}

	@Override
	public List<MosDevice> findAllMosDeviceByType(DeviceType type) {
		DetachedCriteria criteria = DetachedCriteria.forClass(MosDevice.class);
		criteria.addOrder(asc("name").ignoreCase());

		List<MosDevice> result = findByCriteria(criteria, MosDevice.class);

		// Remove manualmente os devices que não são do tipo desejado
		Iterator<MosDevice> it = result.iterator();
		while (it.hasNext()) {
			if (!it.next().getTypes().contains(type)) {
				it.remove();
			}
		}

		return result;
	}
}
