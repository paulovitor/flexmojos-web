package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ReportEventDao;
import tv.snews.anews.domain.ReportEvent;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;

/**
 * Implementação da interface {@link tv.snews.anews.dao.ReportEventDao} que utiliza Hibernate em
 * sua implementação.
 *
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportEventDaoImpl extends GenericDaoImpl<ReportEvent> implements ReportEventDao {

	@Override
	public List<ReportEvent> findAll() {
		return findByCriteria(
				forClass(ReportEvent.class).addOrder(asc("name"))
		);
	}
}
