package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.PermissionDao;
import tv.snews.anews.domain.Permission;

/**
 * Implementação da persistência das permissões.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class PermissionDaoImpl extends GenericDaoImpl<Permission> implements PermissionDao {

	@Override
	public Permission listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Permission.class);
		criteria.add(Restrictions.idEq("0"));
		criteria.addOrder(Order.asc("id"));
		return uniqueResult(criteria);
	}
	
}
