package tv.snews.anews.dao.impl;

import org.hibernate.criterion.*;
import org.hibernate.sql.JoinType;
import org.springframework.dao.support.DataAccessUtils;
import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.domain.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * @author Felipe Pinheiro
 * @author Paulo Felipe de Araújo Júnior
 * @since 1.0.0
 */
public class StoryDaoImpl extends DocumentDaoImpl<Story> implements StoryDao {

	@Override
	public Story get(Serializable id, boolean fetchAll) {
		if (fetchAll) {
			DetachedCriteria criteria = forClass(Story.class);
			criteria.add(Restrictions.idEq(id));

			setSubSectionsFetchMode(criteria);

			return uniqueResult(criteria);
		} else {
			return super.get(id);
		}
	}

	@Override
	public void update(StorySubSection subSection) {
		getHibernateTemplate().update(subSection);
	}

	@Override
	public Story nextApprovedStory(Rundown rundown, Story target) {
		DetachedCriteria criteria = forClass(Story.class, "story_")
				.createAlias("story_.block", "block_")

				.add(eq("block_.rundown", rundown))
				.add(eq("story_.approved", true))
				.add(eq("story_.excluded", false))

				// Se a lauda for do (mesmo bloco e de ordem maior) ou se for de um bloco posterior
				.add(or(
						and(
								eq("block_.order", target.getBlock().getOrder()),
								gt("story_.order", target.getOrder())
						),
						gt("block_.order", target.getBlock().getOrder())
				))

				.addOrder(asc("block_.order"))
				.addOrder(asc("story_.order"));

		List<Story> result = findByCriteria(criteria, 0, 1);
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public Story previousApprovedStory(Rundown rundown, Story target) {
		DetachedCriteria criteria = forClass(Story.class, "story_")
				.createAlias("story_.block", "block_")

				.add(eq("block_.rundown", rundown))
				.add(eq("story_.approved", true))
				.add(eq("story_.excluded", false))

				// Se a lauda for do (mesmo bloco e de ordem menor) ou se for de um bloco anterior
				.add(or(
						and(
								eq("block_.order", target.getBlock().getOrder()),
								lt("story_.order", target.getOrder())
						),
						lt("block_.order", target.getBlock().getOrder())
				))

				.addOrder(asc("block_.order"))
				.addOrder(asc("story_.order"));

		List<Story> result = findByCriteria(criteria, 0, 1);
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public List<Story> listDisplayingStories(Rundown rundown) {
		return findByCriteria(
				forClass(Story.class)
					.add(eq("excluded", false))
					.add(eq("displaying", true))
					.createCriteria("block")
						.add(eq("rundown", rundown))
		);
	}

	@Override
	public List<Story> listByBlock(Block block) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("block", block));
		criteria.add(eq("excluded", false));
		criteria.addOrder(Order.asc("order"));
		return findByCriteria(criteria);
	}

	@Override
	public List<Story> listByGuideline(Guideline guideline) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("guideline", guideline));
		return findByCriteria(criteria);
	}

	@Override
	public List<Story> listByReportage(Reportage reportage) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("reportage", reportage));
		return findByCriteria(criteria);
	}

	@Override
	public String getMaxPage(Rundown rundown) {
		DetachedCriteria criteria = forClass(Story.class, "story");
		criteria.add(eq("story.excluded", false));

		criteria.createAlias("story.block", "block");
		criteria.add(eq("block.rundown", rundown));

		criteria.setProjection(Projections.max("story.page"));
		return (String) DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(criteria));
	}

	@Override
	public List<Story> listStoriesByRundown(Rundown rundown) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.createAlias("block", "block");
		criteria.add(eq("block.rundown", rundown));
		criteria.addOrder(Order.desc("block.id"));
		criteria.addOrder(Order.asc("id"));
		criteria.add(eq("excluded", false));
		return findByCriteria(criteria);
	}

	@Override
	public List<Story> listStoriesByRundown(Rundown rundown, boolean onlyApproved, boolean includeStandBy) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.createAlias("block", "block");
		criteria.add(eq("block.rundown", rundown));
		criteria.add(eq("excluded", false));

		if (onlyApproved) {
			criteria.add(eq("approved", true));
		}
		if (!includeStandBy) {
			criteria.add(eq("block.standBy", false));
		}

		criteria.addOrder(Order.asc("block.order"));
		criteria.addOrder(Order.asc("order"));

		return findByCriteria(criteria);
	}

	@Override
	public List<Story> listExcludedStoriesOlderThan(Date maximumDate, int maxResults) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("excluded", true));
		criteria.add(lt("changeDate", maximumDate));
		return findByCriteria(criteria, 0, maxResults);
	}

	@Override
	public int countExcludedStories() {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("excluded", true));
		return rowCount(criteria);
	}

	@Override
	public List<Story> listExcludedStories(int pageNumber) {
		List<Long> ids = findByCriteria(
				forClass(Story.class)
						.add(eq("excluded", true))
						.addOrder(desc("changeDate"))
						.setProjection(Projections.id()),
				getFirstResult(pageNumber), Long.class
		);

		if (!ids.isEmpty()) {
			DetachedCriteria criteria = forClass(Story.class)
					.add(in("id", ids))
					.addOrder(desc("changeDate"));

			setSubSectionsFetchMode(criteria);
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

			return findByCriteria(criteria);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public int countByStoryKind(StoryKind kind) {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("storyKind", kind));
		return rowCount(criteria);
	}

	@Override
	public List<Story> listByDateAndEditor(Date date, User editor) {
		DetachedCriteria criteria = forClass(Story.class);

		criteria.add(eq("excluded", false));
		criteria.add(eq("editor", editor));
		criteria.createAlias("block", "block");
		criteria.createAlias("block.rundown", "rundown");
		criteria.add(eq("rundown.date", date));
        criteria.addOrder(asc("order"));

		return findByCriteria(criteria);
	}

	@Override
	public int totalExcluded() {
		DetachedCriteria criteria = forClass(Story.class);
		criteria.add(eq("excluded", true));
		return rowCount(criteria);
	}

	@Override
	public List<Story> findBySourceStory(Story sourceStory) {
		return findByCriteria(
				forClass(Story.class).add(eq("sourceStory", sourceStory))
		);
	}

    @Override
    public List<Story> listById(List<Long> ids) {
        DetachedCriteria criteria = forClass(Story.class);
        criteria.add(in("id", ids));
        criteria.addOrder(Order.asc("order"));
        return findByCriteria(criteria);
    }

    @Override
    public List<Story> listById(Long[] idsOfStories) {
        DetachedCriteria criteria = forClass(Story.class);
        criteria.add(in("id", idsOfStories));
        return findByCriteria(criteria);
    }
	//------------------------------------
	//  Helpers
	//------------------------------------

	private void setSubSectionsFetchMode(DetachedCriteria criteria) {
		criteria.setFetchMode("revisions", JOIN);
		criteria.createCriteria("headSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("ncSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("vtSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("footerSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.createCriteria("offSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
		criteria.setFetchMode("copiesHistory", JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
	}
}
