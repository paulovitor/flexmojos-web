package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.MosMediaFullTextDao;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.search.MosMediaParams;
import tv.snews.mos.domain.MosObjectType;

import java.io.IOException;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isEmpty;
import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.Restrictions.in;

/**
 * Implementação padrão do contrato do serviço do Media Center {@link MosMediaFullTextDao}.
 * 
 * @author Samuel Guedes de Melo
 * @author Felipe Pinheiro
 * @since  1.3.0
 */
public class MosMediaFullTextDaoImpl extends FullTextSearchDao implements MosMediaFullTextDao {
	
	private static final String SLUG = "slug";
	private static final String GROUP = "group";
	private static final String ABSTRACT = "abstractText";
	private static final String DESCRIPTION = "description";
	private static final String DATE = "created";
	private static final String TYPE = "type";

	private static final String[] QUERY_PARSER_FIELDS = new String[] { 
		SLUG, GROUP, ABSTRACT, DESCRIPTION, TYPE
	};
	
	@Override
	public int countByParams(MosMediaParams params) {
		return countResults(luceneQuery(params), MosMedia.class);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<MosMedia> findByParams(MosMediaParams params) {
		Query query = luceneQuery(params);

		DetachedCriteria criteria = DetachedCriteria.forClass(MosMedia.class)
				.setFetchMode("paths", JOIN)
				.setFetchMode("proxyPaths", JOIN);

		criteria.add(in("type", new String[] { MosObjectType.VIDEO.toString(), MosObjectType.AUDIO.toString(), MosObjectType.STILL.toString() }));

		List<MosMedia> mosObjList = listResults(
				query, MosMedia.class,
				firstResult(params.getPage()), DEFAULT_PAGE_SIZE,
				sort(DATE, true),
				criteria
		);

		highlightMatches(mosObjList, getDefaultHighlighter(MosMedia.class, query));

		return mosObjList;
	}

	private Query luceneQuery(MosMediaParams params) {
		LuceneQueryBuilder queryBuilder = new LuceneQueryBuilder(
				params.getSearchType(), params.getText(), params.getIgnoreText()
		);
		queryBuilder.appendDatePeriod(DATE, params.getInitialDate(), params.getFinalDate());

		return parseQuery(queryBuilder, MosMedia.class, QUERY_PARSER_FIELDS);
	}

	private void highlightMatches(List<MosMedia> mosObjList, Highlighter highlighter) {
		Analyzer analyzer = getAnalyzer(MosMedia.class);
		
		for (MosMedia mosObj : mosObjList) {
			getHibernateTemplate().evict(mosObj);
			
			try {
				if (!isEmpty(mosObj.getSlug())) {
					String slug = highlighter.getBestFragment(analyzer, SLUG, mosObj.getSlug());
					if (slug != null) {
						mosObj.setSlug(fixWhiteSpace(slug));
					}
				}
				
				if (!isEmpty(mosObj.getAbstractText())) {
					String abstractText = highlighter.getBestFragment(analyzer, ABSTRACT, mosObj.getAbstractText());
					if (abstractText != null) {
						mosObj.setAbstractText(fixWhiteSpace(abstractText));
					}
				}
				
				if (!isEmpty(mosObj.getGroup())) {
					String group = highlighter.getBestFragment(analyzer, GROUP, mosObj.getGroup());
					if (group != null) {
						mosObj.setGroup(fixWhiteSpace(group));
					}
				}

				if (!isEmpty(mosObj.getDescription())) {
					String description = highlighter.getBestFragment(analyzer, DESCRIPTION, mosObj.getDescription());
					if (description != null) {
						mosObj.setDescription(fixWhiteSpace(description));
					}
				}
			} catch (IOException | InvalidTokenOffsetsException e) {
				throw new RuntimeException("Failed to highlight search terms.", e);
			}
		}
	}
}
