package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.GenericDao;
import tv.snews.anews.dao.MosMediaDao;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.search.MosMediaParams;
import tv.snews.mos.domain.MosObjectType;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * Implementação da interface {@link MosMediaDao} utilizando o suporte do
 * Hibernate + Spring.
 *
 * @author Samuel Guedes de Melo
 * @author Felipe Pinheiro
 * @since 1.2.6
 */
public class MosMediaDaoImpl extends GenericDaoImpl<MosMedia> implements MosMediaDao {

	private static final String DATE = "created";

	@Override
	public MosMedia findByObjectId(String objId, MosDevice mosDevice) {
		return uniqueResult(
				forClass(MosMedia.class)
						.add(eq("device", mosDevice))
						.add(eq("objectId", objId))
		);
	}

	@Override
	public int countByParams(MosMediaParams params) {
		DetachedCriteria criteria = createCriteriaWithParams(params);
		return rowCount(criteria);
	}

	@Override
	public List<MosMedia> findByParams(MosMediaParams params) {
		int firstResult = params.getPage() > 0 ? (params.getPage() - 1) * GenericDao.DEFAULT_PAGE_SIZE : 0;
		return findByParams(params, firstResult, DEFAULT_PAGE_SIZE);
	}

	@Override
	public List<MosMedia> findByParams(MosMediaParams params, int firstResult, int maxResults) {
		return findByCriteria(
				createCriteriaWithParams(params)
						.addOrder(desc(DATE)),
				firstResult, maxResults
		);
	}

    @Override
    public int countByDevice(MosDevice mosDevice) {
        return rowCount(forClass(MosMedia.class)
                        .add(eq("device", mosDevice)));
    }

    private DetachedCriteria createCriteriaWithParams(MosMediaParams params) {
		DetachedCriteria criteria = forClass(MosMedia.class);

        if (params.getMosObjectType() != null) {
            if(params.getMosObjectType().equals(MosObjectType.VIDEO) ||
                    params.getMosObjectType().equals(MosObjectType.AUDIO) ||
                    params.getMosObjectType().equals(MosObjectType.STILL)) {
                criteria.add(Restrictions.in("type", new String[]{MosObjectType.VIDEO.toString(), MosObjectType.AUDIO.toString(), MosObjectType.STILL.toString()}));
            } else {
                criteria.add(eq("type", params.getMosObjectType().toString()));
            }
        }

		// Data inicial
		if (params.getInitialDate() != null) {
			criteria.add(ge(DATE, params.getInitialDate()));
		}

		// Data final
		if (params.getFinalDate() != null) {
			criteria.add(le(DATE, params.getFinalDate()));
		}

		return criteria;
	}
}
