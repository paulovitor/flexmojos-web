package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.ReportageRevisionDao;
import tv.snews.anews.domain.ReportageRevision;

import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class ReportageRevisionDaoImpl extends GenericDaoImpl<ReportageRevision> implements ReportageRevisionDao {

	@Override
    @SuppressWarnings("unchecked")
    public List<ReportageRevision> listDateLessThan(Date date, int maxResults) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(ReportageRevision.class);
    	criteria.add(Restrictions.lt("revisionDate", date));
	    return findByCriteria(criteria, 0, maxResults);
    }
}
