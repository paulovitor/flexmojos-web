package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.RssFeedDao;
import tv.snews.anews.domain.RssFeed;

import java.util.List;

/**
 * Implementação default da interface {@link tv.snews.anews.dao.RssFeedDao} utilizando o suporte a
 * Hibernate oferecido pelo Spring.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssFeedDaoImpl extends GenericDaoImpl<RssFeed> implements RssFeedDao {

	@Override
	public RssFeed findByUrl(String url) {
		DetachedCriteria criteria = DetachedCriteria.forClass(RssFeed.class);
		criteria.add(Restrictions.eq("url", url));
		return uniqueResult(criteria);
	}

	@Override
	public List<RssFeed> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(RssFeed.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}

	@Override
	public void delete(RssFeed entity) {
		// Correção para o problema de chegar aqui um feed com a lista de itens vazia por conta do seu lazy=true
		getHibernateTemplate().refresh(entity);

		super.delete(entity);
	}
}
