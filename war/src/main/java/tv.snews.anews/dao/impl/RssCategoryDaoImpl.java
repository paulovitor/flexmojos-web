package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.RssCategoryDao;
import tv.snews.anews.domain.RssCategory;

import java.util.List;

/**
 * Implementação da interface {@link RssCategoryDao} utilizando
 * <code>Hibernate</code>.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssCategoryDaoImpl extends GenericDaoImpl<RssCategory> implements RssCategoryDao {

	@Override
	public RssCategory findByName(String name) {
		return findByName(name, null);
	}

	@Override
	public RssCategory findByName(String name, RssCategory category) {
		DetachedCriteria criteria = DetachedCriteria.forClass(RssCategory.class);
		criteria.add(Restrictions.ilike("name", name, MatchMode.EXACT));

		if (category != null) {
			criteria.add(Restrictions.not(Restrictions.idEq(category.getId())));
		}
		return uniqueResult(criteria);
	}

	@Override
	public List<RssCategory> list() {
		DetachedCriteria criteria = DetachedCriteria.forClass(RssCategory.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}

}
