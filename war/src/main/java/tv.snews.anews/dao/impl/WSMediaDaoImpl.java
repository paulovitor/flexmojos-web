package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.WSMediaDao;
import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Restrictions.eq;

public class WSMediaDaoImpl extends GenericDaoImpl<WSMedia> implements WSMediaDao {
    @Override
    public int countByDevice(WSDevice wsDevice) {
        return rowCount(forClass(WSMedia.class)
                .add(eq("device", wsDevice)));
    }
}
