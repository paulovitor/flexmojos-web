package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.domain.*;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.CriteriaSpecification.DISTINCT_ROOT_ENTITY;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Restrictions.eq;

/**
 * @author Samuel Guedes de Melo.
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class GuidelineDaoImpl extends DocumentDaoImpl<Guideline> implements GuidelineDao {

	private static final GuidelineComparator dailyComparator = new GuidelineComparator();

	@Override
	public Guideline get(Serializable id) {
		// TODO Mover implementação para o método get(Serializable, boolean)
		// TODO Após isso utilizar fetch somente onde necessário

		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		criteria.add(Restrictions.idEq(id));

		// Esses joins são importantes para o método de copiar da praça (talvez mais outras funções)
		setFetchMode(criteria);
		return uniqueResult(criteria);
	}

	@Override
	public List<Guideline> findByDate(Date currentDay, Program program, DocumentState state) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);

		// tirar isso

		if (program != null && program.getId() > 0) {
			criteria.add(Restrictions.eq("program", program));
		} else {
			if (program != null && program.getId() == -1) {
				criteria.add(Restrictions.isNull("program"));
			}
		}

		if (state != null) {
			criteria.add(Restrictions.eq("state", state));
		}

		criteria.add(Restrictions.eq("date", currentDay));
		criteria.add(Restrictions.eq("excluded", false));

		setFetchMode(criteria);

		List<Guideline> result = findByCriteria(criteria);
		Collections.sort(result, dailyComparator);

		return result;
	}

	@Override
	public int countExcluded() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		criteria.add(Restrictions.eq("excluded", true));
		return rowCount(criteria);
	}

	@Override
	public int countByProgram(Program program) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		criteria.add(Restrictions.eq("program", program));
		return rowCount(criteria);
	}

	@Override
	public List<Guideline> findExcluded(int firstResult, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		criteria.add(Restrictions.eq("excluded", true));
		criteria.addOrder(Order.desc("changeDate"));

		return findByCriteria(criteria, firstResult, maxResults);
	}

	@Override
	public List<Guideline> findExcludedByPage(int pageNumber) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		criteria.add(Restrictions.eq("excluded", true));
		criteria.addOrder(Order.desc("changeDate"));

		return findByCriteria(criteria, getFirstResult(pageNumber));
	}

	@Override
	public List<Guideline> findExcludedBefore(Date minDate, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		criteria.add(Restrictions.lt("date", minDate));
		criteria.add(Restrictions.eq("excluded", true));

		// Se tiver alguma reportagem não apaga (a task de limpeza de reportagens tem que fazer seu trabalho antes)
		criteria.add(Restrictions.isEmpty("reportages"));

		// Se tiver uma checklist não apaga (a task de limpeza de checklist tem que fazer seu trabalho antes)
		criteria.createCriteria("checklist", JoinType.LEFT_OUTER_JOIN)
				.add(Restrictions.isNull("guideline"));

		return findByCriteria(criteria, 0, maxResults);
	}

	@Override
	public List<Guideline> findByNews(News news) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);

		criteria.add(Restrictions.eq("news", news));

		return findByCriteria(criteria);
	}

	@Override
	public int countByClassification(GuidelineClassification classification) {
		return rowCount(
				forClass(Guideline.class).add(eq("classification", classification))
		);
	}

    @Override
    public int countByTeam(Team team) {
        return rowCount(
                forClass(Guideline.class).add(eq("team", team))
        );
    }

	private void setFetchMode(DetachedCriteria criteria) {
		criteria.setFetchMode("reportages", JOIN);
		criteria.setFetchMode("revisions", JOIN);
		criteria.setFetchMode("guides", JOIN);
		criteria.setFetchMode("copiesHistory", JOIN);
		criteria.setFetchMode("checklist.tasks", JOIN);
		criteria.setFetchMode("checklist.resources", JOIN);
		criteria.setFetchMode("checklist.revisions", JOIN);

		criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);
	}

	//------------------------------------
	//  Guideline Comparator
	//------------------------------------

	/**
	 * Faz a comparação entre duas pautas para saber qual é maior e qual é a
	 * menor ou se são iguais.
	 * <p/>
	 * <p>A comparação basea-se em dois critérios: o nome do programa e a hora
	 * de execução do primeiro roteiro. A hora de execução só será considerada
	 * se as pautas forem de programas diferentes.</p>
	 * <p/>
	 * <p>Se for feita a comparação entre uma hora de execução nula e uma
	 * não-nula, a hora de execução nula será considerada a maior entre as
	 * duas.</p>
	 *
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	private static class GuidelineComparator implements Comparator<Guideline> {

		@Override
		public int compare(Guideline guidelineA, Guideline guidelineB) {

			int programResult;

			if (guidelineA.getProgram() == null && guidelineB.getProgram() == null) {
				programResult = 0;
			} else if (guidelineA.getProgram() != null && guidelineB.getProgram() == null) {
				programResult = -1;
			} else if (guidelineA.getProgram() == null && guidelineB.getProgram() != null) {
				programResult = 1;
			} else {
				programResult = guidelineA.getProgram().compareTo(guidelineB.getProgram());
			}

			if (programResult != 0) {
				return programResult;
			} else {
				Date scheduleA = guidelineA.firstGuideSchedule();
				Date scheduleB = guidelineB.firstGuideSchedule();

				// Valores nulos serão considerados maiores que valores não-nulos
				if (scheduleA == null && scheduleB == null) {
					return 0; // iguais
				} else if (scheduleA == null) {
					return 1; // A é maior
				} else if (scheduleB == null) {
					return -1; // B é maior
				} else {
					return scheduleA.compareTo(scheduleB); // compara as horas de execução
				}
			}
		}
	}
}
