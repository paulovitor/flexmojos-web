
package tv.snews.anews.dao.impl;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.BlockDao;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.Rundown;

import java.io.Serializable;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;

/**
 * Implementação do rundownDao para acesso ao dados dos blocos do espelho.
 * 
 * @author Paulo Felipe de Araújo Júnior.
 * @since 1.0.0
 */
public class BlockDaoImpl extends GenericDaoImpl<Block> implements BlockDao {

	@Override
	public List<Block> listAll(Rundown rundown) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Block.class);
		criteria.add(Restrictions.eq("rundown", rundown));
		criteria.addOrder(Order.asc("order"));
		return findByCriteria(criteria);
	}

    @Override
    public Block loadById(Serializable idBlock) {
        DetachedCriteria criteria = forClass(Block.class);
        criteria.add(Restrictions.idEq(idBlock));

        setStoriesFetchMode(criteria);

        return uniqueResult(criteria);
    }

    //------------------------------------
    //  Helpers
    //------------------------------------

    private void setStoriesFetchMode(DetachedCriteria criteria) {
        criteria.setFetchMode("stories", JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
    }
}
