package tv.snews.anews.dao.impl;

import org.hibernate.CacheMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.SearchFactory;
import org.hibernate.search.batchindexing.MassIndexerProgressMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.*;

/**
 * Implementação da persistência dos dados da empresa.
 *
 * @author Eliezer Reis
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class SettingsDaoImpl extends GenericDaoImpl<Settings> implements SettingsDao {

	private static final Logger log = LoggerFactory.getLogger(SettingsDaoImpl.class);

	@Override
	public Settings loadSettings() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Settings.class);
		return uniqueResult(criteria);
	}

	@Override
	public synchronized void reindexDataBase() {
		log.info("Indexing process started...");

		// Atenção! Com a atualização do Hibernate agora não pode mais apagar a pasta antes de reindexar.
//		try {
//			String indexesPath = StorageUtil.anewsDirectory() + "indexes";
//			FileUtils.deleteDirectory(new File(indexesPath));
//
//			log.debug("Indexes folder deleted ({})", indexesPath);
//		} catch (IOException e) {
//			log.error("Failed to delete indexes directory before the re-indexing process.", e);
//		}

		try {
			/*
			 * Para mais opções de configuração e monitoração veja a documentação.
			 * 
			 * http://docs.jboss.org/hibernate/search/3.4/reference/en-US/html_single/#search-batchindex-massindexer
			 */

			Class<?>[] indexedClasses = new Class<?>[]{
					// General classes
					Program.class, User.class, /* Document.class, */

					// Address classes
					City.class,

					// Contact classes
//					ContactGroup.class, Contact.class, AgendaContact.class, // Não está sendo usado

					// Guideline classes
					Guideline.class, Guide.class, GuidelineClassification.class,

					// Reportage classes
					Reportage.class, ReportageSection.class,

					// Checklist classes
					Checklist.class, ChecklistTask.class, ChecklistType.class, ChecklistResource.class,

					//  Media classes
					MosMedia.class,

					// Rundown & Story classes
					Rundown.class, Block.class, Story.class,

					// Script classes
					ScriptPlan.class, ScriptBlock.class, Script.class,

					// Report classes
					ReportEvent.class, ReportNature.class, Report.class, GuidelineReport.class, ReportageReport.class, StoryReport.class
			};
			
			/*
			 * NOTE Existe a opção em que o Hibernate busca as classes indexadas
			 * e executa o processo para cada uma automaticamente, mas em alguns
			 * casos estava causando estouro do pool de conexões. Indexando
			 * assim, um de cada vez, não ocorre esse problema.
			 */
			for (Class<?> indexedClass : indexedClasses) {
				log.info("Indexing registers of type " + indexedClass.getSimpleName() + "...");
				
				/*
				 * NOTE Alterações no número de threads podem causar estouro do
				 * pool de conexões em alguns casos. Caso mudar teste bastante.
				 */
				getFullTextSession()
						.createIndexer(indexedClass)
						.batchSizeToLoadObjects(10)				// 10
						.threadsToLoadObjects(2)					// 2
						.cacheMode(CacheMode.NORMAL)			// defaults to CacheMode.IGNORE
						.progressMonitor(new IndexerProgressMonitor())
						.startAndWait();

//				getFullTextSession().flushToIndexes();

				log.info(indexedClass.getSimpleName() + " registers indexed successfully.");
			}
		} catch (InterruptedException e) {
			throw new RuntimeException("Failed to index the database.", e);
		}

		log.info("Indexing process finished.");
	}

	@Override
	public void optimizeIndexes() {
		log.info("Optimizing indexes...");

		getFullTextSession().flushToIndexes();

		SearchFactory searchFactory = getFullTextSession().getSearchFactory();
		searchFactory.optimize(); // pode ser especificado por classe

		log.info("Indexes optimization complete.");
	}

	//------------------------------------
	//  Helpers
	//------------------------------------

	private FullTextSession getFullTextSession() {
		return Search.getFullTextSession(getSession());
	}

	//------------------------------------
	//  Inner Classes
	//------------------------------------

	/**
	 * @see org.hibernate.search.batchindexing.MassIndexerProgressMonitor
	 */
	private static class IndexerProgressMonitor implements MassIndexerProgressMonitor {

		private long added = 0;
		private long built = 0;
		private long loaded = 0;
//    	private long totalCount = 0;

		/**
		 * @see org.hibernate.search.batchindexing.MassIndexerProgressMonitor#documentsAdded(long)
		 */
		@Override
		public void documentsAdded(long increment) {
			added += increment;
		}

		/**
		 * @see org.hibernate.search.batchindexing.MassIndexerProgressMonitor#documentsBuilt(int)
		 */
		@Override
		public void documentsBuilt(int number) {
			built += number;

			if (built % 500 == 0) {
				log.debug("{} documents already built...", built);
			}
		}

		/**
		 * @see org.hibernate.search.batchindexing.MassIndexerProgressMonitor#entitiesLoaded(int)
		 */
		@Override
		public void entitiesLoaded(int size) {
			loaded += size;
		}

		/**
		 * @see org.hibernate.search.batchindexing.MassIndexerProgressMonitor#addToTotalCount(long)
		 */
		@Override
		public void addToTotalCount(long count) {
//        	totalCount += count;
		}

		/**
		 * @see org.hibernate.search.batchindexing.MassIndexerProgressMonitor#indexingCompleted()
		 */
		@Override
		public void indexingCompleted() {
			log.info("Number of entities loaded from database: {}", loaded);
			log.info("Number of documents sent to backend: {}", added);
			log.info("Number of documents built: {}", built);
		}
	}
}
