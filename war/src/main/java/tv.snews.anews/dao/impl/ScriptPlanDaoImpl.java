package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.ScriptPlanDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.ScriptPlan;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Restrictions.eq;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptPlanDaoImpl extends GenericDaoImpl<ScriptPlan> implements ScriptPlanDao {

	@Override
	public ScriptPlan loadById(Serializable id) {
		List result = getHibernateTemplate().findByNamedParam(
				"SELECT DISTINCT plan FROM ScriptPlan plan " +
						"LEFT JOIN FETCH plan.blocks block " +
						"LEFT JOIN FETCH block.scripts script " +
						"WHERE plan.id = :planId"
				, "planId", id
		);
		return result.isEmpty() ? null : (ScriptPlan) result.get(0);
	}

	@Override
	public ScriptPlan loadFull(Serializable id, Serializable blockId) {
        List result = null;
        if(blockId != null) {
					result = getHibernateTemplate().findByNamedParam(
							"SELECT DISTINCT plan FROM ScriptPlan plan " +
									"LEFT JOIN FETCH plan.blocks block " +
									"LEFT JOIN FETCH block.scripts script " +
									"LEFT JOIN FETCH script.images image " +
									"LEFT JOIN FETCH script.videos video " +
									"LEFT JOIN FETCH script.cgs video " +
									"WHERE plan.id = :planId and block.id = :blockId"
							, new String[] { "planId", "blockId" }
							, new Object[] { id, blockId });
        } else {
					result = getHibernateTemplate().findByNamedParam(
							"SELECT DISTINCT plan FROM ScriptPlan plan " +
									"LEFT JOIN FETCH plan.blocks block " +
									"LEFT JOIN FETCH block.scripts script " +
									"LEFT JOIN FETCH script.images image " +
									"LEFT JOIN FETCH script.videos video " +
									"LEFT JOIN FETCH script.cgs video " +
									"WHERE plan.id = :planId"
							, "planId", id);
        }
		return result.isEmpty() ? null : (ScriptPlan) result.get(0);
	}

	@Override
	public int countByDateAndProgram(Date date, Program program) {
		return rowCount(
				forClass(ScriptPlan.class)
						.add(eq("date", date))
						.add(eq("program", program))
		);
	}

	@Override
	public ScriptPlan findByDateAndProgram(Date date, Program program) {
		return uniqueResult(
				forClass(ScriptPlan.class)
						.add(eq("date", date))
						.add(eq("program", program))
		);
	}

	@Override
	public List<ScriptPlan> findAllByDateGreaterOrEqualsTo(Date date) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ScriptPlan.class);
		criteria.add(Restrictions.ge("date", date));
		return findByCriteria(criteria);
	}
}
