
package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.EncryptedValueDao;
import tv.snews.anews.util.EncryptedValue;

import static org.hibernate.criterion.DetachedCriteria.forClass;

/**
 * Implementação da interface {@link EncryptedValueDao} utilizando o suporte do
 * Spring para Hibernate.
 *
 * @author Felipe Pinheiro
 * @since 1.2
 */
public class EncryptedValueDaoImpl extends GenericDaoImpl<EncryptedValue> implements EncryptedValueDao {

	@Override
	public EncryptedValue findFirst() {
		return uniqueResult(forClass(EncryptedValue.class));
	}
}
