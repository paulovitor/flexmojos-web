package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.MailConfigDao;
import tv.snews.anews.infra.mail.MailConfig;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class MailConfigDaoImpl extends GenericDaoImpl<MailConfig> implements MailConfigDao {

	@Override
	public MailConfig loadFirst() {
		DetachedCriteria criteria = DetachedCriteria.forClass(MailConfig.class);
		List<MailConfig> result = findByCriteria(criteria, 0, 1);
		return result.isEmpty() ? null : result.get(0);
	}
}
