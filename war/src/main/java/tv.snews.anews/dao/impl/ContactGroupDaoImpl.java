package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.ContactGroupDao;
import tv.snews.anews.domain.ContactGroup;

import java.util.List;

/**
 * Implementação da persistência dos grupos de contatos.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class ContactGroupDaoImpl extends GenericDaoImpl<ContactGroup> implements ContactGroupDao {

	@Override
	public List<ContactGroup> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(ContactGroup.class);
		criteria.addOrder(Order.asc("name").ignoreCase());
		return findByCriteria(criteria);
	}

	@Override
	public ContactGroup findByName(String name) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ContactGroup.class);
		criteria.add(Restrictions.eq("name", name).ignoreCase());
		return uniqueResult(criteria);
	}
}
