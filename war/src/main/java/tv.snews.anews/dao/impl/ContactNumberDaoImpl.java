package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ContactNumberDao;
import tv.snews.anews.domain.ContactNumber;

/**
 * Implementação da interface {@link tv.snews.anews.dao.ContactNumberDao} utilizando o suporte do
 * Spring para Hibernate.
 * 
 * @author Felipe Pinheiro
 * @since 1.2
 */
public class ContactNumberDaoImpl extends GenericDaoImpl<ContactNumber> implements ContactNumberDao {

}
