package tv.snews.anews.dao.impl;

import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.StoryTemplateDao;
import tv.snews.anews.domain.StoryTemplate;

import java.io.Serializable;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;

/**
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
public class StoryTemplateDaoImpl extends GenericDaoImpl<StoryTemplate> implements StoryTemplateDao {

    @Override
    public StoryTemplate get(Serializable id) {
        DetachedCriteria criteria = forClass(StoryTemplate.class);
        criteria.add(Restrictions.idEq(id));

        criteria.createCriteria("headSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
        criteria.createCriteria("ncSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
        criteria.createCriteria("vtSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
        criteria.createCriteria("footerSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
        criteria.createCriteria("offSection", JoinType.LEFT_OUTER_JOIN).setFetchMode("subSections", JOIN);
        criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

        return uniqueResult(criteria);
    }
}
