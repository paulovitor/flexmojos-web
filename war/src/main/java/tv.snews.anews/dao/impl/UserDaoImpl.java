package tv.snews.anews.dao.impl;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.sql.JoinType;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Group;
import tv.snews.anews.domain.Team;
import tv.snews.anews.domain.User;

import java.util.ArrayList;
import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.DISTINCT_ROOT_ENTITY;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.MatchMode.ANYWHERE;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * Implemenntação do Contrato UserDao para acesso ao dados do usuário.
 *
 * @author Eliezer Reis.
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

	@Override
	public User findByEmail(String email) {
		return uniqueResult(
				forClass(User.class)
						.add(eq("deleted", false))
						.add(eq("email", email))
		);
	}

	@Override
	public User findByNickname(String nickname) {
		return uniqueResult(
				forClass(User.class)
						.add(eq("deleted", false))
						.add(eq("nickname", nickname))
		);
	}

	@Override
	public User findByEmailAndPassword(String email, String password) {
		return uniqueResult(
				forClass(User.class)
						.add(eq("deleted", false))
						.add(eq("email", email))
						.add(eq("password", password))
		);
	}

	@Override
	public List<User> listUsersByGroupAndStatus(Group group) {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
		criteria.add(eq("deleted", false));
		criteria.createAlias("groups", "group", JoinType.FULL_JOIN);
		criteria.add(eq("group.id", group.getId()));
		return findByCriteria(criteria);
	}

    @Override
    public List<User> listUsersByTeamAndStatus(Team team) {
        DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
        criteria.add(eq("deleted", false));
        criteria.createAlias("teams", "team", JoinType.FULL_JOIN);
        criteria.add(eq("team.id", team.getId()));
        return findByCriteria(criteria);
    }

	//	@Override
//	@SuppressWarnings("unchecked")
//	public List<User> listPage(String searchText, int firstResult, int maxResults) {
//		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
//
//		criteria.addOrder(asc("name").ignoreCase());
//
//		if (!StringUtils.isBlank(searchText)) {
//			criteria.add(
//                or(
//                    or(
//                        ilike("name", searchText, ANYWHERE),
//                        ilike("email", searchText, ANYWHERE)),
//                    ilike("nickname", searchText, ANYWHERE)
//                )
//            );
//		}
//
//		return getHibernateTemplate().findByCriteria(criteria, firstResult, maxResults);
//	}

	@Override
	public List<User> listAll(String searchTerm, String sortProperty) {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
		criteria.add(ne("email", User.SUPPORT_EMAIL));
		criteria.add(eq("deleted", false));

		criteria.addOrder(asc(sortProperty).ignoreCase());

		if (!StringUtils.isBlank(searchTerm)) {
			criteria.add(
					or(
							or(
									ilike("name", searchTerm, ANYWHERE),
									ilike("email", searchTerm, ANYWHERE)
							),
							ilike("nickname", searchTerm, ANYWHERE)
					)
			);
		}

		return findByCriteria(criteria);
	}

	@Override
	public List<User> listReporters() {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);

		criteria.add(eq("deleted", false));
		criteria.createCriteria("groups").add(eq("showAsReporters", true));
		criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);

		criteria.addOrder(asc("nickname").ignoreCase());

		return findByCriteria(criteria);
	}

	@Override
	public List<User> listProducers() {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);

		criteria.add(eq("deleted", false));
		criteria.createCriteria("groups").add(eq("showAsProducers", true));
		criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);

		criteria.addOrder(asc("nickname").ignoreCase());

		return findByCriteria(criteria);
	}

    @Override
    @SuppressWarnings("unchecked")
    public List<User> listChecklistProducers() {
        DetachedCriteria criteria = DetachedCriteria.forClass(User.class);

        criteria.add(eq("deleted", false));
        criteria.createCriteria("groups").add(eq("showAsChecklistProducers", true));
        criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);

        criteria.addOrder(asc("nickname").ignoreCase());

        return findByCriteria(criteria);
    }

	@Override
	public List<User> listEditors() {
		DetachedCriteria criteria = DetachedCriteria.forClass(User.class);

		criteria.add(eq("deleted", false));
		criteria.createCriteria("groups").add(eq("showAsEditors", true));
		criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);

		criteria.addOrder(asc("nickname").ignoreCase());

		return findByCriteria(criteria);
	}

    @Override
    @SuppressWarnings("unchecked")
    public List<User> listByGroupName(String searchTerm, Group group, Boolean belongsToTheGroup) {
        DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
        criteria.add(ne("email", User.SUPPORT_EMAIL));
        criteria.add(eq("deleted", false));


        if (!StringUtils.isBlank(searchTerm)) {
            criteria.add(
                    or(
                            or(
                                    ilike("name", searchTerm, ANYWHERE),
                                    ilike("email", searchTerm, ANYWHERE)
                            ),
                            ilike("nickname", searchTerm, ANYWHERE)
                    )
            );
        }
        // Este método recupera os usuários que pertencem, ou não, ao grupo informado para o formulário de grupos,
        // Onde é possível editar os usuários.
        // Mas Também retorna todos os usuários para serem adicionados, caso o grupo seja novo.
        if(group != null){
            //Caso o grupo não seja nulo, é criado o alias para a tabela
            criteria.createAlias("groups", "group", JoinType.LEFT_OUTER_JOIN);
            // A flag belongsToTheGroup diz se é para trazer usuários pertencentes ao grupo ou não
            if (belongsToTheGroup){
                //Se belongsToTheGroup for verdadeiro, é para trazer apenas usuários do grupo,
                // logo a restrição é adicionada
                criteria.add(eq("group.id", group.getId()));

            // Caso deva recuperar os usuários que não sejam do grupo, é necessário recuperar os registros que:
            // não pertençam ao grupo informado ou não esteja em nenhum grupo

                // Faz essa condição pois apenas é necessário adicionar as restrições abaixo, caso o grupo tenha usuários
                // Caso não tenha, podem ser retornados todos os registros para serem adicionados ao grupo
            }else if (!belongsToTheGroup && group.getUsers().size() > 0){
                    criteria.add(or(
                                        isNull("group.id"),
                                        and(
                                                ne("group.id", group.getId()),
                                                not(in("id", getUsersIds(group)))
                                            )
                                    ));
        }
        }
        criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);
        criteria.addOrder(asc("name").ignoreCase());
        return findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> listByTeamName(String searchTerm, Team team, Boolean belongsToTheTeam) {
        DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
        criteria.add(ne("email", User.SUPPORT_EMAIL));
        criteria.add(eq("deleted", false));


        if (!StringUtils.isBlank(searchTerm)) {
            criteria.add(
                    or(
                            or(
                                    ilike("name", searchTerm, ANYWHERE),
                                    ilike("email", searchTerm, ANYWHERE)
                            ),
                            ilike("nickname", searchTerm, ANYWHERE)
                    )
            );
        }
        // Este método recupera os usuários que pertencem, ou não, à equipe informada para o formulário de equipes,
        // Onde é possível editar os usuários.
        // Mas Também retorna todos os usuários para serem adicionados, caso a equipe seja nova.
        if(team != null){
            //Caso a equipe não seja nula, é criado o alias para a tabela
            criteria.createAlias("teams", "team", JoinType.LEFT_OUTER_JOIN);
            // A flag belongsToTheTeam diz se é para trazer usuários pertencentes à equipe ou não
            if (belongsToTheTeam){
                //Se belongsToTheTeam for verdadeiro, é para trazer apenas usuários da equipe,
                // logo a restrição é adicionada
                criteria.add(eq("team.id", team.getId()));

                // Caso deva recuperar os usuários que não sejam da equipe, é necessário recuperar os registros que:
                // não pertençam à equipe informada ou não esteja em nenhuma equipe

                // Faz essa condição pois apenas é necessário adicionar as restrições abaixo, caso a equipe tenha usuários
                // Caso não tenha, podem ser retornados todos os registros para serem adicionados ao grupo
            }else if (!belongsToTheTeam && team.getUsers().size() > 0){
                criteria.add(or(
                        isNull("team.id"),
                        and(
                                ne("team.id", team.getId()),
                                not(in("id", getUsersIds(team)))
                        )
                ));
            }
        }
        criteria.setResultTransformer(DISTINCT_ROOT_ENTITY);
        criteria.addOrder(asc("name").ignoreCase());
        return findByCriteria(criteria);
    }

    private List<Integer> getUsersIds(Group group) {
        ArrayList<Integer> ids = new ArrayList<>();
        for (User user: group.getUsers()) {
            ids.add(user.getId());
        }
        return ids;
    }

    private List<Integer> getUsersIds(Team team) {
        ArrayList<Integer> ids = new ArrayList<>();
        for (User user: team.getUsers()) {
            ids.add(user.getId());
        }
        return ids;
    }
}
