
package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import tv.snews.anews.dao.GuideDao;
import tv.snews.anews.domain.Guide;
import tv.snews.anews.domain.Guideline;

import java.util.List;

/**
 * Implementação da persistência do roteiro.
 *
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class GuideDaoImpl extends GenericDaoImpl<Guide> implements GuideDao {

	@Override
	public List<Guide> listAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Guideline.class);
		return findByCriteria(criteria);
	}

}
