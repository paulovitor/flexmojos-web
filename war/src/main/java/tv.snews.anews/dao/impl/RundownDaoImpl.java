package tv.snews.anews.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.domain.*;

import java.util.Date;
import java.util.List;

/**
 * Implementação do rundownDao para acesso ao dados dos espelhos.
 *
 * @author Paulo Felipe de Araújo Júnior.
 * @since 1.0.0
 */
public class RundownDaoImpl extends GenericDaoImpl<Rundown> implements RundownDao {

	@Override
	public Rundown findByDateAndProgram(Date date, Program program, boolean fetchAll) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Rundown.class);
		criteria.add(Restrictions.eq("date", date));
		criteria.add(Restrictions.eq("program", program));

		if (fetchAll) {
//            criteria.createCriteria("blocks", CriteriaSpecification.LEFT_JOIN);
//            criteria.createCriteria("blocks.stories", CriteriaSpecification.LEFT_JOIN).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
//            criteria.createCriteria("blocks.stories.headSection", Criteria.LEFT_JOIN).setFetchMode("subSections", FetchMode.JOIN);
//            criteria.createCriteria("blocks.stories.ncSection", Criteria.LEFT_JOIN).setFetchMode("subSections", FetchMode.JOIN);
//            criteria.createCriteria("blocks.stories.vtSection", Criteria.LEFT_JOIN).setFetchMode("subSections", FetchMode.JOIN);
//            criteria.createCriteria("blocks.stories.footerSection", Criteria.LEFT_JOIN).setFetchMode("subSections", FetchMode.JOIN);
//            criteria.createCriteria("blocks.stories.offSection", Criteria.LEFT_JOIN).setFetchMode("subSections", FetchMode.JOIN);

			Rundown rundown = uniqueResult(criteria);
			if (rundown != null) {
				for (Block block : rundown.getBlocks()) {
					for (Story story : block.getStories()) {
						StorySection[] sections = {
								story.getHeadSection(), story.getNcSection(),
								story.getVtSection(), story.getOffSection(),
								story.getFooterSection()
						};
						for (StorySection section : sections) {
							if (section != null) {
								for (StorySubSection subSection : section.getSubSections()) {
									subSection.getSubSections().size();
								}
							}
						}
					}
				}
			}

			return rundown;
		} else {
			return uniqueResult(criteria);
		}
	}

	@Override
	public int count() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Rundown.class);
		return rowCount(criteria);
	}

	public int countRundown(Date date, Program program) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Rundown.class);
		criteria.add(Restrictions.eq("date", date));
		criteria.add(Restrictions.eq("program", program));

		return rowCount(criteria);
	}

	@Override
	public int countRundownsOfProgram(Program program) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Rundown.class);
		criteria.add(Restrictions.eq("program", program));
		return rowCount(criteria);
	}

	@Override
	public List<Rundown> findAllByDateGreaterOrEqualsTo(Date date) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Rundown.class);
		criteria.add(Restrictions.ge("date", date));
		return findByCriteria(criteria);
	}

	@Override
	public List<Rundown> paginatedList(int firstResult, int maxResults) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Rundown.class);
		return findByCriteria(criteria, firstResult, maxResults);
	}
}
