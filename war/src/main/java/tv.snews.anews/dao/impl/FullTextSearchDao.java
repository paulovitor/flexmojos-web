package tv.snews.anews.dao.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.highlight.*;
import org.apache.lucene.util.Version;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.SearchFactory;
import org.hibernate.search.indexes.IndexReaderAccessor;
import org.springframework.orm.hibernate4.HibernateCallback;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import tv.snews.anews.dao.GenericDao;

import java.io.IOException;
import java.util.List;

/**
 * Implementação Hibernate Search dos DAOs que
 * utilizam esse tipo de consulta.
 * 
 * @author Felipe Zap de Mello
 * @since 1.2.6.3
 */
public abstract class FullTextSearchDao extends HibernateDaoSupport {

//	private static final Logger log = LoggerFactory.getLogger(FullTextSearchDao.class);

	/**
	 * Tamanho padrão para as páginas das buscas avançadas.
	 */
	protected int DEFAULT_PAGE_SIZE = 50;
	
	public static final String OPEN_TAG_HIGHLIGHT = "|highlight|";
	public static final String CLOSE_TAG_HIGHLIGHT = " |/highlight|"; // O espaço inicial é proposital para manter compatibilidade com o Flex

	//------------------------------
	//	Session Related Methods
	//------------------------------

//	protected Criteria createCriteria(final Class<?> entity) {
//		Criteria criteria = getHibernateTemplate().executeWithNativeSession(new HibernateCallback<Criteria>() {
//			@Override
//			public Criteria doInHibernate(Session session) throws HibernateException {
//				return session.createCriteria(entity);
//			}
//		});
//
//		return criteria;
//	}

	protected int countResults(final Query luceneQuery, final Class<?>... entities) {
		Integer count = getHibernateTemplate().executeWithNativeSession(new HibernateCallback<Integer>() {
			@Override
			public Integer doInHibernate(Session session) throws HibernateException {
				FullTextSession fullTextSession = Search.getFullTextSession(session);
				FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(luceneQuery, entities);
				return fullTextQuery.getResultSize();
			}
		});

		return count;
	}

	protected <T> List<T> listResults(Query luceneQuery, Class<T> entity, int firstResult, int maxResult, Sort sort) {
		return listResults(luceneQuery, entity, firstResult, maxResult, sort, null);
	}

	@SuppressWarnings({"unchecked"})
	protected <T> List<T> listResults(final Query luceneQuery, final Class<T> entity, final int firstResult, final int maxResult, final Sort sort, final DetachedCriteria detachedCriteria) {
		List<T> results = getHibernateTemplate().executeWithNativeSession(new HibernateCallback<List<T>>() {
			@Override
			public List<T> doInHibernate(Session session) throws HibernateException {
				FullTextSession fullTextSession = Search.getFullTextSession(session);
				FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(luceneQuery, entity);

				fullTextQuery.setFirstResult(firstResult);
				fullTextQuery.setMaxResults(maxResult);
				fullTextQuery.setSort(sort);

				if (detachedCriteria != null) {
					Criteria criteria = detachedCriteria.getExecutableCriteria(session);
					fullTextQuery.setCriteriaQuery(criteria);
				}

				return fullTextQuery.list();
			}
		});

		return results;
	}

	protected Analyzer getAnalyzer(final Class<?> entityClass) {
		Analyzer analyzer = getHibernateTemplate().executeWithNativeSession(new HibernateCallback<Analyzer>() {
			@Override
			public Analyzer doInHibernate(Session session) throws HibernateException {
				FullTextSession fullTextSession = Search.getFullTextSession(session);
				return fullTextSession.getSearchFactory().getAnalyzer(entityClass);
			}
		});

		return analyzer;
	}

	@SuppressWarnings("rawtypes")
	protected Highlighter getDefaultHighlighter(final Class<?> entity, final Query luceneQuery) {
		// Cria o objeto Highlighter para marcação dos resultados com as tags HTML

		Highlighter highlighter = getHibernateTemplate().executeWithNativeSession(new HibernateCallback<Highlighter>() {
			@Override
			public Highlighter doInHibernate(Session session) throws HibernateException {
				FullTextSession fullTextSession = Search.getFullTextSession(session);

				SearchFactory factory = fullTextSession.getSearchFactory();

				IndexReaderAccessor idxAccessor = factory.getIndexReaderAccessor();
				IndexReader idxReader = idxAccessor.open(entity);

				Query rewrittenQuery;
				try {
					rewrittenQuery = luceneQuery.rewrite(idxReader);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}

				Formatter formatter = new SimpleHTMLFormatter(OPEN_TAG_HIGHLIGHT, CLOSE_TAG_HIGHLIGHT);

				Scorer scorer = new QueryScorer(rewrittenQuery);
				Highlighter highlighter = new Highlighter(formatter, scorer);
				SimpleFragmenter fragmenter = (SimpleFragmenter) highlighter.getTextFragmenter();
				fragmenter.setFragmentSize(Integer.MAX_VALUE);

				return highlighter;
			}
		});

		return highlighter;
	}

	//------------------------------
	//	Helper Methods
	//------------------------------

	protected int firstResult(int pageNumber) {
		// Retorna o número do primeiro registro a ser buscado da página
		return pageNumber > 0 ? (pageNumber - 1) * GenericDao.DEFAULT_PAGE_SIZE : 0;
	}

	protected Sort sort(String field) {
		return new Sort(new SortField(field, SortField.STRING));
	}

	protected Sort sort(String field, Boolean reverse) {
		return new Sort(new SortField(field, SortField.STRING, reverse));
	}

	protected String fixWhiteSpace(String str) {
		// Elimina o último espaço desnecessário da tag CLOSE_TAG_HIGHLIGHT
		if (str != null && str.contains(OPEN_TAG_HIGHLIGHT)) {
			int index = str.lastIndexOf(CLOSE_TAG_HIGHLIGHT);
			if (index != -1) {
				return str.substring(0, index) + CLOSE_TAG_HIGHLIGHT.trim() + str.substring(index + 13);
			}
		}
		return str;
	}

	protected Query parseQuery(LuceneQueryBuilder queryBuilder, Class<?> clazz, String... fields) {
		if (queryBuilder.isEmpty()) {
			return new MatchAllDocsQuery();
		} else {
			try {
				MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_36, fields, getAnalyzer(clazz));
				return parser.parse(queryBuilder.toString());
			} catch (ParseException e) {
				throw new RuntimeException(e);
			}
		}
	}

//	protected Query parseQuery(LuceneQueryBuilder queryBuilder, Class<?> clazz, String... fields) {
//		try {
//			MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_4_9, fields, getAnalyzer(clazz));
//			return parser.parse(queryBuilder.toString());
//		} catch (ParseException e) {
//			throw new RuntimeException(e);
//		}
//	}
//
//	protected Sort sort(String field) {
//		return new Sort(new SortField(field, SortField.Type.STRING));
//	}
//
//	protected Sort sort(String field, Boolean reverse) {
//		return new Sort(new SortField(field, SortField.Type.STRING, reverse));
//	}
}
