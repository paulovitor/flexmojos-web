package tv.snews.anews.dao.impl;

import org.hibernate.criterion.*;
import org.springframework.dao.support.DataAccessUtils;
import tv.snews.anews.dao.ScriptDao;
import tv.snews.anews.domain.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static org.hibernate.FetchMode.JOIN;
import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Restrictions.*;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptDaoImpl extends DocumentDaoImpl<Script> implements ScriptDao {

    @Override
    public Script get(Serializable idScript, boolean fetchAll) {
        if (fetchAll) {
            DetachedCriteria criteria = forClass(Script.class);
            criteria.add(Restrictions.idEq(idScript));
            setFetchMode(criteria);
            return uniqueResult(criteria);
        }
        return super.get(idScript);
    }

	@Override
	public void update(ScriptMosCredit credit) {
		getHibernateTemplate().update(credit);
	}

	@Override
	public void update(ScriptVideo video) {
		getHibernateTemplate().update(video);
	}

    @Override
    public Script nextApprovedScript(ScriptPlan scriptPlan, Script target) {
        DetachedCriteria criteria = forClass(Script.class, "script_")
                .createAlias("script_.block", "block_")

                .add(eq("block_.scriptPlan", scriptPlan))
                .add(eq("script_.approved", true))
                .add(eq("script_.excluded", false))

                        // Se o script for do (mesmo bloco e de ordem maior) ou se for de um bloco posterior
                .add(or(
                        and(
                                eq("block_.order", target.getBlock().getOrder()),
                                gt("script_.order", target.getOrder())
                        ),
                        gt("block_.order", target.getBlock().getOrder())
                ))

                .addOrder(asc("block_.order"))
                .addOrder(asc("script_.order"));

        List<Script> result = findByCriteria(criteria, 0, 1);
        return result.isEmpty() ? null : result.get(0);
    }

	@Override
	@SuppressWarnings("unchecked")
	public String getMaxPage(ScriptPlan scriptPlan) {
		DetachedCriteria criteria = forClass(Script.class, "script");
		criteria.add(eq("script.excluded", false));

		criteria.createAlias("script.block", "scriptBlock");
		criteria.add(eq("scriptBlock.scriptPlan", scriptPlan));

		criteria.setProjection(Projections.max("script.page"));
		return (String) DataAccessUtils.uniqueResult(getHibernateTemplate().findByCriteria(criteria));
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Script> listByBlock(ScriptBlock block) {
		DetachedCriteria criteria = forClass(Script.class);
		criteria.add(eq("block", block));
		criteria.add(eq("excluded", false));
		criteria.addOrder(Order.asc("order"));
		return (List<Script>) getHibernateTemplate().findByCriteria(criteria);
	}

    @Override
    public List<Script> listById(List<Long> idsOfScripts) {
        DetachedCriteria criteria = forClass(Script.class);
        criteria.add(in("id", idsOfScripts));
        return findByCriteria(criteria);
    }

    @Override
    public List<Script> listById(Long[] idsOfScripts) {
        DetachedCriteria criteria = forClass(Script.class);
        criteria.add(in("id", idsOfScripts));
        criteria.addOrder(Order.asc("order"));
        return findByCriteria(criteria);
    }

	@Override
	public int countExcludedScripts() {
		DetachedCriteria criteria = forClass(Script.class);
		criteria.add(eq("excluded", true));
		return rowCount(criteria);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Script> listExcludedScripts(int pageNumber) {
		List<Long> ids = findByCriteria(
				forClass(Script.class)
						.add(eq("excluded", true))
						.addOrder(Order.desc("changeDate"))
						.setProjection(Projections.id()),
				getFirstResult(pageNumber), Long.class
		);

		if (!ids.isEmpty()) {
			DetachedCriteria criteria = forClass(Script.class)
					.add(in("id", ids))
					.addOrder(Order.desc("changeDate"));

			//setSubSectionsFetchMode(criteria);
			criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);

			return findByCriteria(criteria);
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public int totalExcluded() {
		DetachedCriteria criteria = forClass(Script.class);
		criteria.add(eq("excluded", true));
		return rowCount(criteria);
	}

	//------------------------------------
	//  Helpers
	//------------------------------------
	private void setFetchMode(DetachedCriteria criteria) {
		criteria.setFetchMode("revisions", JOIN);
		criteria.setFetchMode("images", JOIN);
		criteria.setFetchMode("videos", JOIN);
		criteria.setFetchMode("cgs", JOIN);
		criteria.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
	}
}
