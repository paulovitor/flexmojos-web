package tv.snews.anews.dao.impl;

import tv.snews.anews.dao.ReportNatureDao;
import tv.snews.anews.domain.ReportNature;

import java.util.List;

import static org.hibernate.criterion.DetachedCriteria.forClass;
import static org.hibernate.criterion.Order.asc;

/**
 * Implementação da interface {@link tv.snews.anews.dao.ReportNatureDao} que utiliza Hibernate em
 * sua implementação.
 *
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportNatureDaoImpl extends GenericDaoImpl<ReportNature> implements ReportNatureDao {

	@Override
	public List<ReportNature> findAll() {
		return findByCriteria(
				forClass(ReportNature.class).addOrder(asc("name"))
		);
	}
}
