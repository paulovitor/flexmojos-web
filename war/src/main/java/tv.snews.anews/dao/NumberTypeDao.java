
package tv.snews.anews.dao;

import tv.snews.anews.domain.NumberType;

import java.util.List;

/**
 * Interface que define os métodos que um DAO que acessa os tipos
 * de numeros de telefone implementar.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface NumberTypeDao extends GenericDao<NumberType> {

	List<NumberType> listAll();
}
