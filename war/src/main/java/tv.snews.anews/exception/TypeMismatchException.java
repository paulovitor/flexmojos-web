package tv.snews.anews.exception;

/**
 * Exceção lança quando um tipo não é suportado em uma operação de conversão.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class TypeMismatchException extends RuntimeException {

    private static final long serialVersionUID = 4926761966119159215L;

    public TypeMismatchException() {
    	super();
    }
    
    public TypeMismatchException(String message) {
    	super(message);
    }
}
