package tv.snews.anews.exception;

/**
 * Exceção lançada quando ocorre algum erro no envio da mensagem.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class MessageDeliveryException extends Exception {

	private static final long serialVersionUID = -4792934203412842474L;

	public MessageDeliveryException(String cause) {
		super(cause);
	}
	
	public MessageDeliveryException(String cause, Throwable throwable) {
		super(cause, throwable);
	}
}
