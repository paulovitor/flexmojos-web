package tv.snews.anews.exception;


/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class InvalidEmailException extends Exception {

	private static final long serialVersionUID = -7905467764645693834L;

	public InvalidEmailException(String message) {
		super(message);
	}
}
