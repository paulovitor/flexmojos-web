package tv.snews.anews.exception;


/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class InvalidPasswordException extends Exception {

	private static final long serialVersionUID = 7495707442727301067L;

	public InvalidPasswordException(String message) {
		super(message);
	}
}
