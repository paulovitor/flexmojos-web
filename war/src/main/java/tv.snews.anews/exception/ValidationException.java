package tv.snews.anews.exception;

/**
 * Exceção genérica para casos de falha de validação dos objetos
 * <code>domain</code>.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ValidationException extends RuntimeException {

	public ValidationException(Throwable cause) {
	    super(cause);
    }
    
    public ValidationException(String validationMessage) {
    	super(validationMessage);
    }
    
    public ValidationException(String validationMessage, Throwable cause) {
	    super(validationMessage, cause);
    }
    
    private static final long serialVersionUID = -3049241398857523627L;
}
