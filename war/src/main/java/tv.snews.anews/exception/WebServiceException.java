package tv.snews.anews.exception;

public class WebServiceException extends Exception {

    private static final long serialVersionUID = -8148471406978529619L;

    public WebServiceException(String message) {
        super(message);
    }
}
