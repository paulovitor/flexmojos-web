package tv.snews.anews.exception;

/**
 * 
 * 
 * @author Eliezer Reis.
 * @since 1.0.0
 */
public class UploadException extends Exception {

	private static final long serialVersionUID = -8722211396949773040L;

	public UploadException(String cause) {
		super(cause);
	}
}
