package tv.snews.anews.exception;

/**
 * Informa possíveis problemas no controle da sessão.
 * 
 * @author Eliezer Reis.
 * @since 1.0.0
 */
public class SessionManagerException extends RuntimeException {

	private static final long serialVersionUID = -8722211396949773040L;

	public SessionManagerException(String cause) {
		super(cause);
	}
}
