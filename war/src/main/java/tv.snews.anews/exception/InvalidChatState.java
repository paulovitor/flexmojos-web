package tv.snews.anews.exception;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class InvalidChatState extends Exception {

	private static final long serialVersionUID = 7935463326067238365L;

	/**
	 * Define a mensagem que explica qual foi a falha no estado do chat que
	 * causou a exception.
	 * 
	 * @param message Mensagem da exception.
	 */
	public InvalidChatState(final String message) {
		super(message);
	}
}
