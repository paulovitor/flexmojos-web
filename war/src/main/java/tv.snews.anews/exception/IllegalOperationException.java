package tv.snews.anews.exception;

/**
 * Exceção lançada para quando uma operação não permitida é requisitada.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class IllegalOperationException extends Exception {

    private static final long serialVersionUID = 5011940430336069560L;

	public IllegalOperationException(String message) {
	    super(message);
    }
}
