package tv.snews.anews.exception;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class ValidatorErrorHandler implements ErrorHandler {

	public void warning(SAXParseException exception) throws SAXException {
        exception.printStackTrace();
    }
 
    public void error(SAXParseException exception) throws SAXException {
        exception.printStackTrace();
    }
 
    public void fatalError(SAXParseException exception) throws SAXException {
        exception.printStackTrace();
    }
}