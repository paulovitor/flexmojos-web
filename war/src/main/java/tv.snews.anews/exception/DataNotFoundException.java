package tv.snews.anews.exception;


/**
 * @author paulo
 * @since 1.0.0
 */
public class DataNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -7905467764645693834L;

	public DataNotFoundException(String message) {
		super(message);
	}
}
