package tv.snews.anews.event;

import org.springframework.context.ApplicationEvent;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.BlockAction;

/**
 * Classe de evento para o bloco
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class BlockEvent extends ApplicationEvent {

	private Block block;

    private BlockAction action;
    
    /**
     * Atributos utilizados quando se é necessário 
     * guardar o valor antigo da mudança.
     */
    private Object oldValue;
    private Object newValue;
    
    public BlockEvent(BlockAction action, Block block, Object source) {
	    super(source);
	    this.action = action;
	    this.block = block;
    }


	public Object getOldValue() {
	    return oldValue;
    }

	public void setOldValue(Object oldValue) {
	    this.oldValue = oldValue;
    }

	public Object getNewValue() {
	    return newValue;
    }

	public void setNewValue(Object newValue) {
	    this.newValue = newValue;
    }

    public Block getBlock() {
    	return block;
    }
	
    public void setBlock(Block block) {
    	this.block = block;
    }

    public BlockAction getAction() {
    	return action;
    }

    public void setAction(BlockAction blockAction) {
    	this.action = blockAction;
    }

    private static final long serialVersionUID = -630963243464678156L;
}
