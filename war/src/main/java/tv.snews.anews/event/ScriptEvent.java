package tv.snews.anews.event;

import org.springframework.context.ApplicationEvent;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptAction;
import tv.snews.anews.domain.ScriptPlan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe de evento para o script
 *
 * @author Maxuel Ramos
 * @since 1.7
 */
public class ScriptEvent extends ApplicationEvent {

    private static final long serialVersionUID = 6422729970758809842L;

    // Não pode retirar do block do script, pois o block pode ser null (exclusão) !!
    private ScriptPlan scriptPlan;

    private ScriptAction action;

    private List<Change> changes = new ArrayList<>();

    public ScriptEvent(ScriptAction action, Object source) {
        super(source);
        this.action = action;
    }

    public ScriptEvent(ScriptAction action, ScriptPlan scriptPlan, Object source) {
        super(source);
        this.action = action;
        this.scriptPlan = scriptPlan;
    }

    public ScriptEvent(ScriptAction action, Script script, Object source) {
        this(action, source);
        registerChange(script);
    }

    public ScriptEvent(ScriptAction action, Script script, ScriptPlan scriptPlan, Object source) {
        this(action, scriptPlan, source);
        registerChange(script);
    }

    //------------------------------
    //	Operations
    //------------------------------

    public boolean isEmpty() {
        return changes.isEmpty();
    }

    public void registerChange(Script target) {
        changes.add(new Change(target));

        if (target.getBlock() != null) {
            scriptPlan = target.getBlock().getScriptPlan();
        }
    }

    public void registerChange(Script target, Object oldValue, Object newValue) {
        changes.add(new Change(target, oldValue, newValue));

        if (target.getBlock() != null) {
            scriptPlan = target.getBlock().getScriptPlan();
        }
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public ScriptPlan getScriptPlan() {
        return scriptPlan;
    }

    public ScriptAction getAction() {
        return action;
    }

    public List<Change> getChanges() {
		/*
		 * Não permite alterar o Set deste evento pois o mesmo evento pode ser
		 * manipulado por classes diferentes. Se uma classe alterar o conteúdo
		 * da lista, vai afetar o resultado esperado na outra classe.
		 */
        return Collections.unmodifiableList(changes);
    }

    public List<Script> getScripts() {
        List<Script> scripts = new ArrayList<>();

        for (Change change : changes) {
            scripts.add(change.getTarget());
        }

        return scripts;
    }

    //------------------------------
    //	Inner Class
    //------------------------------

    public static class Change {

        private Script target;
        private Object oldValue;
        private Object newValue;

        public Change(Script target) {
            this.target = target;
        }

        public Change(Script target, Object oldValue, Object newValue) {
            this(target);
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public Script getTarget() {
            return target;
        }

        public Object getOldValue() {
            return oldValue;
        }

        public Object getNewValue() {
            return newValue;
        }
    }
}
