package tv.snews.anews.event;

import org.springframework.context.ApplicationEvent;
import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.ScriptBlockAction;
import tv.snews.anews.domain.ScriptPlan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe de evento para o bloco do script
 * 
 * @author Samuel Guedes de Melo
 */
public class ScriptBlockEvent extends ApplicationEvent {

    private static final long serialVersionUID = -630963243464678156L;

    // Não pode retirar do block do script, pois o block pode ser null (exclusão) !!
    private ScriptPlan scriptPlan;

    private ScriptBlockAction action;

    private List<Change> changes = new ArrayList<>();

    public ScriptBlockEvent(ScriptBlockAction action, Object source) {
        super(source);
        this.action = action;
    }

    public ScriptBlockEvent(ScriptBlockAction action, ScriptBlock scriptBlock, Object source) {
        this(action, source);
        registerChange(scriptBlock);
    }
    public ScriptBlockEvent(ScriptBlockAction action, ScriptPlan scriptPlan, Object source) {
        super(source);
        this.action = action;
        this.scriptPlan = scriptPlan;
    }

    public ScriptBlockEvent(ScriptBlockAction action, ScriptBlock scriptBlock, ScriptPlan scriptPlan, Object source) {
        this(action, scriptPlan, source);
        registerChange(scriptBlock);
    }

    //------------------------------
    //	Operations
    //------------------------------

    public boolean isEmpty() {
        return changes.isEmpty();
    }

    public void registerChange(ScriptBlock target) {
        changes.add(new Change(target));

        if (target != null) {
            scriptPlan = target.getScriptPlan();
        }
    }

    public void registerChange(ScriptBlock target, Object oldValue, Object newValue) {
        changes.add(new Change(target, oldValue, newValue));

        if (target != null) {
            scriptPlan = target.getScriptPlan();
        }
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public ScriptPlan getScriptPlan() {
        return scriptPlan;
    }

    public ScriptBlockAction getAction() {
        return action;
    }

    public List<Change> getChanges() {
		/*
		 * Não permite alterar o Set deste evento pois o mesmo evento pode ser
		 * manipulado por classes diferentes. Se uma classe alterar o conteúdo
		 * da lista, vai afetar o resultado esperado na outra classe.
		 */
        return Collections.unmodifiableList(changes);
    }

    public List<ScriptBlock> getBlocks() {
        List<ScriptBlock> blocks = new ArrayList<>();

        for (Change change : changes) {
            blocks.add(change.getTarget());
        }

        return blocks;
    }

    //------------------------------
    //	Inner Class
    //------------------------------

    public static class Change {

        private ScriptBlock target;
        private Object oldValue;
        private Object newValue;

        public Change(ScriptBlock target) {
            this.target = target;
        }

        public Change(ScriptBlock target, Object oldValue, Object newValue) {
            this(target);
            this.oldValue = oldValue;
            this.newValue = newValue;
        }

        public ScriptBlock getTarget() {
            return target;
        }

        public Object getOldValue() {
            return oldValue;
        }

        public Object getNewValue() {
            return newValue;
        }
    }

}
