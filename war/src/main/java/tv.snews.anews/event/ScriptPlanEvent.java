package tv.snews.anews.event;

import org.springframework.context.ApplicationEvent;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.domain.ScriptPlanAction;

/**
 * Classe de evento para o roteiro do sistema.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public class ScriptPlanEvent extends ApplicationEvent {

	private ScriptPlan scriptPlan;
    private ScriptPlanAction action;

    /*
     * Atributos utilizados quando se é necessário guardar o valor antigo da mudança.
     */
    private Object oldValue;
    private Object newValue;

    public ScriptPlanEvent(ScriptPlanAction action, ScriptPlan scriptPlan, Object source) {
	    super(source);
	    this.action = action;
	    this.scriptPlan = scriptPlan;
    }
    
	public ScriptPlan getScriptPlan() {
	    return scriptPlan;
    }

	public void setScriptPlan(ScriptPlan scriptPlan) {
	    this.scriptPlan = scriptPlan;
    }

	public Object getOldValue() {
	    return oldValue;
    }

	public void setOldValue(Object oldValue) {
	    this.oldValue = oldValue;
    }

	public Object getNewValue() {
	    return newValue;
    }

	public void setNewValue(Object newValue) {
	    this.newValue = newValue;
    }

	public ScriptPlanAction getAction() {
	    return action;
    }

	public void setAction(ScriptPlanAction action) {
	    this.action = action;
    }

	private static final long serialVersionUID = 5213138329680508749L;
}
