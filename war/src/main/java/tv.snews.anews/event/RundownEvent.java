package tv.snews.anews.event;

import org.springframework.context.ApplicationEvent;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.RundownAction;

/**
 * Classe de evento para o espelho do sistema.
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class RundownEvent extends ApplicationEvent {

	private Rundown rundown;
    private RundownAction action;
    
    /*
     * Atributos utilizados quando se é necessário guardar o valor antigo da mudança.
     */
    private Object oldValue;
    private Object newValue;

    public RundownEvent(RundownAction action, Rundown rundown, Object source) {
	    super(source);
	    this.action = action;
	    this.rundown = rundown;
    }
    
	public Rundown getRundown() {
	    return rundown;
    }

	public void setRundown(Rundown rundown) {
	    this.rundown = rundown;
    }

	public Object getOldValue() {
	    return oldValue;
    }

	public void setOldValue(Object oldValue) {
	    this.oldValue = oldValue;
    }

	public Object getNewValue() {
	    return newValue;
    }

	public void setNewValue(Object newValue) {
	    this.newValue = newValue;
    }

	public RundownAction getAction() {
	    return action;
    }

	public void setAction(RundownAction action) {
	    this.action = action;
    }

	private static final long serialVersionUID = 5213138329680508749L;
}
