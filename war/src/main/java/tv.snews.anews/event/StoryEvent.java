package tv.snews.anews.event;

import org.springframework.context.ApplicationEvent;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.StoryAction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe de evento para a lauda
 *
 * @author Felipe Zap de Mello
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class StoryEvent extends ApplicationEvent {

	private static final long serialVersionUID = 6422729970758809842L;

	// Não pode retirar do block da story, pois o block pode ser null (exclusão) !!
	private Rundown rundown;

	private StoryAction action;

	private List<Change> changes = new ArrayList<>();

	public StoryEvent(StoryAction action, Object source) {
		super(source);
		this.action = action;
	}

	public StoryEvent(StoryAction action, Rundown rundown, Object source) {
		super(source);
		this.action = action;
		this.rundown = rundown;
	}

	public StoryEvent(StoryAction action, Story story, Object source) {
		this(action, source);
		registerChange(story);
	}

	public StoryEvent(StoryAction action, Story story, Rundown rundown, Object source) {
		this(action, rundown, source);
		registerChange(story);
	}

	//------------------------------
	//	Operations
	//------------------------------

	public boolean isEmpty() {
		return changes.isEmpty();
	}

	public void registerChange(Story target) {
		changes.add(new Change(target));

		if (target.getBlock() != null) {
			rundown = target.getBlock().getRundown();
		}
	}

	public void registerChange(Story target, Object oldValue, Object newValue) {
		changes.add(new Change(target, oldValue, newValue));

		if (target.getBlock() != null) {
			rundown = target.getBlock().getRundown();
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Rundown getRundown() {
		return rundown;
	}

	public StoryAction getAction() {
		return action;
	}

	public List<Change> getChanges() {
		/*
		 * Não permite alterar o Set deste evento pois o mesmo evento pode ser
		 * manipulado por classes diferentes. Se uma classe alterar o conteúdo
		 * da lista, vai afetar o resultado esperado na outra classe.
		 */
		return Collections.unmodifiableList(changes);
	}

	public List<Story> getStories() {
		List<Story> stories = new ArrayList<>();

		for (Change change : changes) {
			stories.add(change.getTarget());
		}

		return stories;
	}

	//------------------------------
	//	Inner Class
	//------------------------------

	public static class Change {

		private Story target;
		private Object oldValue;
		private Object newValue;

		public Change(Story target) {
			this.target = target;
		}

		public Change(Story target, Object oldValue, Object newValue) {
			this(target);
			this.oldValue = oldValue;
			this.newValue = newValue;
		}

		public Story getTarget() {
			return target;
		}

		public Object getOldValue() {
			return oldValue;
		}

		public Object getNewValue() {
			return newValue;
		}
	}
}
