package tv.snews.anews.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.UserFileDao;
import tv.snews.anews.domain.UserFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Eliezer Reis
 * @since 1.0.0
 */
@Controller
public class DownloadController implements ResourceLoaderAware {
	
	private static final Logger log = LoggerFactory.getLogger(DownloadController.class);
	
	private ResourceLoader resourceLoader;

	@Autowired
	private UserFileDao userFileDao;

	@RequestMapping(value = "files/download.do")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) {
		int id = ServletRequestUtils.getIntParameter(request, "id", 0);

		if (id > 0) {
			UserFile file = userFileDao.get(id);
			if (file != null) {
				
				Resource resource = resourceLoader.getResource("file:" +  file.getPath());
				if (resource != null) {
					try {
						InputStream input = resource.getInputStream();
						OutputStream output = response.getOutputStream();

						response.setContentLength(resource.getInputStream().available());
						FileCopyUtils.copy(input, output);
					} catch (Exception e) {
						log.error("Copy failed.", e);
					}
				}
			}
		}
		
		return null;
	}

	//----------------------------------------
	//	Setters
	//----------------------------------------
	
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
}
