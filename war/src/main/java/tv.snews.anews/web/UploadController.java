package tv.snews.anews.web;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import tv.snews.anews.exception.UploadException;
import tv.snews.anews.flex.UploadXmlResponse;
import tv.snews.anews.util.ImageUtil;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.anews.util.StorageUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


/**
 * Faz o download dos binários do vídeo através do seu identificador.
 *
 * @author Eliezer Reis
 * @since 1.0
 */
@Controller
public class UploadController {

	private static final Logger log = LoggerFactory.getLogger(UploadController.class);

	private static final String SLASH = System.getProperty("file.separator");

	public static final String PREVIEWER = SLASH + "scriptImage" + SLASH + "previewer";
	public static final String THUMBNAIL = SLASH + "scriptImage" + SLASH + "thumbnail" + SLASH;

	private static final List<String> ALLOWED_EXTENSIONS = Arrays.asList(
			// Images
			".png", ".jpg", ".jpeg", ".gif", ".bmp",
			// Docs
			".rtf", ".txt", ".doc", ".docx", ".odt",
			// Excel
			".xls", ".xlsx", ".ods",
			// Power Point
			".pps", ".ppsx", ".odp",
			// Pdf
			".pdf",
			// Audios
			".mp3", ".wav", ".wma"
	);

	@Autowired
	private StorageUtil storageUtil;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@RequestMapping(value = "files/upload.do")
	@ResponseBody
	public UploadXmlResponse handleRequest(HttpServletRequest request, String fileName, String extension, String source, long size) throws Exception {
		if (StringUtils.isBlank(extension)) {
			throw new UploadException(bundle.getMessage("upload.extensionNotFound", extension));
		}

		if (!ALLOWED_EXTENSIONS.contains(extension.toLowerCase(Locale.getDefault()))) {
			throw new UploadException(bundle.getMessage("upload.extensionNotAllowed", extension.toLowerCase(Locale.getDefault())));
		}

		File tempDir = null;

		switch (source) {
			case "chatfile":
				tempDir = storageUtil.getTemporaryDirectory();
				break;
			case "scriptImage":
				tempDir = loadScriptImageFile();
				break;
			case "logofile":
				tempDir = loadLogoFile();
				break;
		}

		String filePath = uploadFile(request, tempDir, extension);
		String thumbnailPath = null;

		if (source.equals("scriptImage")) {
			// Thumbnail será gerado no formato
			String thumbnailFolder = storageUtil.getTemporaryDirectory().getPath() + THUMBNAIL;
			thumbnailPath = ImageUtil.generateThumbnail(filePath, thumbnailFolder, 128, 96);
		}

		// Notify the interface about informations of the file.
		return new UploadXmlResponse.Builder()
				.fileName(fileName)
				.extension(extension)
				.path(filePath)
				.size(size)
				.thumbnailPath(thumbnailPath)
				.build();
	}

	private String uploadFile(HttpServletRequest request, File tempDir, String extension) {
		// Start to process the file upload.
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(tempDir);
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Randomize the temporary name of the file.
		String filePath = tempDir.getPath() + SLASH + RandomStringUtils.randomAlphanumeric(10) + extension;

		try {
			List<FileItem> items = upload.parseRequest(request);

			for (FileItem item : items) {
				if (!item.isFormField()) {
					item.write(new File(filePath));
				}
			}

		} catch (Exception e) {
			log.error("Error on upload processing.", e);
		}
		return filePath;
	}

	private File loadLogoFile() throws Exception {
		String logoPath = System.getProperty("user.home") + SLASH + ".anews";
		File path = new File(logoPath);

		if (!path.exists()) {
			if (!path.mkdirs()) {
				throw new Exception("");
			}
		}
		return path;
	}

	private File loadScriptImageFile() throws IOException {
		String scriptImagePath = storageUtil.getTemporaryDirectory().getPath() + PREVIEWER;
		String scriptThumbnailPath = storageUtil.getTemporaryDirectory().getPath() + THUMBNAIL;

		File scriptImage = new File(scriptImagePath);
		File scriptThumbnail = new File(scriptThumbnailPath);

		// Verifica se o diretório existe e, se não existir, tenta criá-lo
		if (!scriptImage.exists() && !scriptImage.mkdirs()) {
			throw new IOException(bundle.getMessage("storage.storageCreationError", scriptImage.getPath()));
		}

		// Verifica se o diretório existe e, se não existir, tenta criá-lo
		if (!scriptThumbnail.exists() && !scriptThumbnail.mkdirs()) {
			throw new IOException(bundle.getMessage("storage.storageCreationError", scriptThumbnail.getPath()));
		}

		return scriptImage;
	}
}
