package tv.snews.anews.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.service.ProgramService;
import tv.snews.anews.web.dto.TvShowDTO;
import tv.snews.anews.web.dto.UserDTO;

import java.util.SortedSet;
import java.util.TreeSet;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Felipe Pinheiro
 */
@RestController
@RequestMapping("tvshows")
public class TvShowController {

	@Autowired
	private ProgramService programService;

	/**
	 * GET /tvshows
	 */
	@RequestMapping(method = GET)
	public SortedSet<TvShowDTO> findAll(boolean allOption, boolean generalOption) {
		SortedSet<TvShowDTO> result = new TreeSet<>();

		for (Program program : programService.listAll()) {
			result.add(new TvShowDTO(program));
		}

		if (allOption) {
			result.add(TvShowDTO.ALL);
		}
		if (generalOption) {
			result.add(TvShowDTO.GENERAL);
		}

		return result;
	}

	/**
	 * GET /tvshows/{id}
	 */
	@RequestMapping(method = GET, value = "/{id}")
	public TvShowDTO findOne(@PathVariable Integer id) {
		if (id == -2) return TvShowDTO.ALL;
		if (id == -1) return TvShowDTO.GENERAL;

		Program program = programService.loadById(id);
		return new TvShowDTO(program);
	}

	/**
	 * GET /tvshows/{id}/anchors
	 */
	@RequestMapping(method = GET, value = "/{id}/anchors")
	public SortedSet<UserDTO> findAnchors(@PathVariable Integer id) {
		Program program = programService.loadById(id);

		SortedSet<UserDTO> result = new TreeSet<>();
		for (User user : program.getPresenters()) {
			result.add(new UserDTO(user));
		}

		return result;
	}

	/**
	 * GET /tvshows/{id}/editors
	 */
	@RequestMapping(method = GET, value = "/{id}/editors")
	public SortedSet<UserDTO> findEditors(@PathVariable Integer id) {
		Program program = programService.loadById(id);

		SortedSet<UserDTO> result = new TreeSet<>();
		for (User user : program.getEditors()) {
			result.add(new UserDTO(user));
		}

		return result;
	}

	/**
	 * GET /tvshows/{id}/image_editors
	 */
	@RequestMapping(method = GET, value = "/{id}/image_editors")
	public SortedSet<UserDTO> findImageEditors(@PathVariable Integer id) {
		Program program = programService.loadById(id);

		SortedSet<UserDTO> result = new TreeSet<>();
		for (User user : program.getImageEditors()) {
			result.add(new UserDTO(user));
		}

		return result;
	}
}
