package tv.snews.anews.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.GuidelineParams;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.web.dto.AssignmentDTO;
import tv.snews.anews.web.dto.UserDTO;

import java.util.*;

import static org.apache.commons.lang.StringUtils.stripToEmpty;
import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static tv.snews.anews.web.api.AssignmentController.SearchParams.DAILY;
import static tv.snews.anews.web.api.AssignmentController.SearchParams.PERIOD;

/**
 * @author Felipe Pinheiro
 */
@RestController
@RequestMapping("assignments")
public class AssignmentController {

	@Autowired
	private GuidelineService guidelineService;

	@Autowired
	private UserService userService;

	@Autowired
	private ProgramDao programDao;

	@Autowired
	private UserDao userDao;

	/**
	 * GET /assignments
	 */
	@RequestMapping(method = GET)
	public List<AssignmentDTO> findAll(SearchParams params) {
		List<Guideline> guidelines;

		switch (params.getType()) {
			case DAILY:
				if (params.getDate() == null) {
					throw new IllegalArgumentException("A date is required for the a daily listing");
				}

				guidelines = guidelineService.listAllGuidelineOfDay(params.getDate(), params.getTvShowId(), null);
				break;

			case PERIOD:
				GuidelineParams guidelineParams = new GuidelineParams();
				guidelineParams.setInitialDate(params.getFrom());
				guidelineParams.setFinalDate(params.getTo());
				guidelineParams.setSlug(params.getSlug());
				guidelineParams.setText(params.getContent());
				guidelineParams.setProgram(parseProgram(params.getTvShowId()));
				guidelineParams.setProducer(parseProducer(params.getProducerId()));
				guidelineParams.setPage(parsePage(params.getPage()));

				guidelines = guidelineService.fullTextSearch(guidelineParams).getResults();
				break;

			default:
				throw new IllegalArgumentException("Invalid search type");
		}

		List<AssignmentDTO> assignments = new ArrayList<>();

		for (Guideline guideline : guidelines) {
			assignments.add(new AssignmentDTO(guideline));
		}

		return assignments;
	}

	/**
	 * GET /assignments/{id}
	 */
	@RequestMapping(method = GET, value = "/{id}")
	public AssignmentDTO findOne(@PathVariable Long id) {
		Guideline guideline = guidelineService.loadById(id);
		return new AssignmentDTO(guideline, true);
	}

	/**
	 * GET /assignments/producers
	 */
	@RequestMapping(method = GET, value = "/producers")
	public SortedSet<UserDTO> findProducers() {
		SortedSet<UserDTO> result = new TreeSet<>();

		List<User> producers = userService.listProducers();
		for (User producer : producers) {
			result.add(new UserDTO(producer));
		}

		return result;
	}

	/**
	 * TODO GET /assignments/classifications
	 */

	/**
	 * TODO GET /assignments/teams
	 */

	//------------------------------
	//	Helpers
	//------------------------------

	private Program parseProgram(Integer programId) {
		if (programId == null || programId == -2) {
			return null;
		}
		return programDao.get(programId);
	}

	private User parseProducer(Integer producerId) {
		if (producerId == null) {
			return null;
		}
		return userDao.get(producerId);
	}

	private int parsePage(Integer page) {
		return page == null ? 1 : page;
	}

	//------------------------------
	//	Inner Classes
	//------------------------------

	public static class SearchParams {

		public static final String DAILY = "Daily";
		public static final String PERIOD = "Period";

		private String type; // daily | period
		private String slug = "";
		private String content = "";

		@DateTimeFormat(iso = DATE)
		private Date date;

		@DateTimeFormat(iso = DATE)
		private Date from;

		@DateTimeFormat(iso = DATE)
		private Date to;

		private Integer tvShowId;
		private Integer producerId;
		private Integer page;

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getSlug() {
			return slug;
		}

		public void setSlug(String slug) {
			this.slug = stripToEmpty(slug);
		}

		public String getContent() {
			return content;
		}

		public void setContent(String content) {
			this.content = stripToEmpty(content);
		}

		public Date getDate() {
			return date;
		}

		public void setDate(Date date) {
			this.date = date;
		}

		public Date getFrom() {
			return from;
		}

		public void setFrom(Date from) {
			this.from = from;
		}

		public Date getTo() {
			return to;
		}

		public void setTo(Date to) {
			this.to = to;
		}

		public Integer getTvShowId() {
			return tvShowId;
		}

		public void setTvShowId(Integer tvShowId) {
			this.tvShowId = tvShowId;
		}

		public Integer getProducerId() {
			return producerId;
		}

		public void setProducerId(Integer producerId) {
			this.producerId = producerId;
		}

		public Integer getPage() {
			return page;
		}

		public void setPage(Integer page) {
			this.page = page;
		}
	}
}
