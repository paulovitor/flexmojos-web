package tv.snews.anews.web.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tv.snews.anews.domain.User;
import tv.snews.anews.infra.security.TokenTransfer;
import tv.snews.anews.web.dto.UserDTO;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static tv.snews.anews.infra.security.TokenUtils.createToken;

/**
 * @author Felipe Pinheiro
 */
@RestController
@RequestMapping("users")
public class UserController {

	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;

	@Autowired
	private UserDetailsService userDetailsService;

	/**
	 * POST /users/authenticate
	 */
	@RequestMapping(method = POST, value = "/authenticate")
	public TokenTransfer authenticate(String email, String password) {
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(email, password);
		Authentication authentication = authManager.authenticate(authToken);
		SecurityContextHolder.getContext().setAuthentication(authentication);

		// Reload the user as the password of authentication principal will be null
		// after the authorization and the password is needed for token generation.
		UserDetails userDetails = userDetailsService.loadUserByUsername(email);

		return new TokenTransfer(createToken(userDetails));
	}

	/**
	 * GET /users/current
	 */
	@RequestMapping(method = GET, value = "/current")
	public ResponseEntity<UserDTO> currentUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object principal = authentication.getPrincipal();

		if ("anonymousUser".equals(principal)) {
			return new ResponseEntity<>(UNAUTHORIZED);
		}

		UserDetails userDetails = (UserDetails) principal;
		User user = (User) userDetailsService.loadUserByUsername(userDetails.getUsername()); // recarrega para dados recentes

		return new ResponseEntity<>(new UserDTO(user), OK);
	}
}
