package tv.snews.anews.web.dto;

import org.springframework.format.annotation.DateTimeFormat;
import tv.snews.anews.domain.Program;

import java.io.Serializable;
import java.util.Date;

import static org.apache.commons.lang.WordUtils.capitalizeFully;
import static org.springframework.format.annotation.DateTimeFormat.ISO.TIME;

/**
 * @author Felipe Pinheiro
 */
public class TvShowDTO implements Comparable<TvShowDTO>, Serializable {

	public static final TvShowDTO ALL = new TvShowDTO(-2, "All TV Shows");
	public static final TvShowDTO GENERAL = new TvShowDTO(-1, "General Use");

	private Integer id;
	private String name;

	@DateTimeFormat(iso = TIME)
	private Date start;

	public TvShowDTO() {}

	private TvShowDTO(Integer id, String name) {
		this.id = id;
		this.name = name;
	}

	public TvShowDTO(Program program) {
		this.id = program.getId();
		this.name = capitalizeFully(program.getName()); // Converte "JORNAL NACIONAL" para "Jornal Nacional"
		this.start = program.getStart();
	}

	@Override
	public int compareTo(TvShowDTO other) {
		// Compara pelo nome, mas se houver um ID menor que zero, significa que é Gaveta Geral

		if (id > 0 && other.id > 0) {
			return name.compareTo(other.name);
		}
		return id.compareTo(other.id);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	private static final long serialVersionUID = -2491620314499153909L;
}
