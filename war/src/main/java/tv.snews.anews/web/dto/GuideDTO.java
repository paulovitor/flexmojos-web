package tv.snews.anews.web.dto;

import org.springframework.format.annotation.DateTimeFormat;
import tv.snews.anews.domain.City;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.Guide;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static java.lang.Integer.compare;
import static org.springframework.format.annotation.DateTimeFormat.ISO.TIME;

/**
 * @author Felipe Pinheiro
 */
public class GuideDTO implements Comparable<GuideDTO>, Serializable {

	private Integer id;

	@DateTimeFormat(iso = TIME)
	private Date schedule;

	private String address;
	private String reference;
	private String observation;
	private String city;
	private String state;
	private int order;
	private Set<ContactDTO> contacts = new HashSet<>();

	public GuideDTO() {}

	public GuideDTO(Guide guide) {
		this.id = guide.getId();
		this.schedule = guide.getSchedule();
		this.address = guide.getAddress();
		this.reference = guide.getReference();
		this.observation = guide.getObservation();
		this.city = parseCity(guide.getCity());
		this.state = parseState(guide.getCity());
		this.order = guide.getOrderGuide();

		for (Contact contact : guide.getContacts()) {
			contacts.add(new ContactDTO(contact));
		}
	}

	@Override
	public int compareTo(GuideDTO other) {
		if (other == null) {
			return 1;
		}
		return compare(order, other.order);
	}

	private String parseCity(City city) {
		return city == null ? null : city.getName();
	}

	private String parseState(City city) {
		return city == null ? null : city.getState().getCode();
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public Set<ContactDTO> getContacts() {
		return contacts;
	}

	public void setContacts(Set<ContactDTO> contacts) {
		this.contacts = contacts;
	}

	private static final long serialVersionUID = -5233039458985457124L;
}
