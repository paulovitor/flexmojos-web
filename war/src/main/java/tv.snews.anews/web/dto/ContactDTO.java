package tv.snews.anews.web.dto;

import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.ContactNumber;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Felipe Pinheiro
 */
public class ContactDTO implements Serializable {

	private Integer id;
	private String name;
	private String profession;
	private Set<ContactNumberDTO> numbers = new HashSet<>();

	public ContactDTO() {}

	public ContactDTO(Contact contact) {
		this.id = contact.getId();
		this.name = contact.getName();
		this.profession = contact.getProfession();

		for (ContactNumber number : contact.getNumbers()) {
			numbers.add(new ContactNumberDTO(number));
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public Set<ContactNumberDTO> getNumbers() {
		return numbers;
	}

	public void setNumbers(Set<ContactNumberDTO> numbers) {
		this.numbers = numbers;
	}

	private static final long serialVersionUID = 2617059374842539892L;
}
