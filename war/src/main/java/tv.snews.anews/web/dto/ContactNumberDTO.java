package tv.snews.anews.web.dto;

import tv.snews.anews.domain.ContactNumber;

import java.io.Serializable;

/**
 * @author Felipe Pinheiro
 */
public class ContactNumberDTO implements Serializable {

	private String value;
	private String type;

	public ContactNumberDTO() {}

	public ContactNumberDTO(ContactNumber number) {
		this.value = number.getValue();
		this.type = number.getNumberType().getDescription();
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private static final long serialVersionUID = 511559987917262391L;
}
