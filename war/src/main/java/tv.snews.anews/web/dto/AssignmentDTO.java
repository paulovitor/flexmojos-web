package tv.snews.anews.web.dto;

import org.springframework.format.annotation.DateTimeFormat;
import tv.snews.anews.domain.*;

import java.util.*;

import static org.apache.commons.lang.WordUtils.capitalizeFully;
import static org.springframework.format.annotation.DateTimeFormat.ISO.TIME;

/**
 * @author Felipe Pinheiro
 */
public class AssignmentDTO extends DocumentDTO {

	private String proposal;
	private String referral;
	private String information;

	private String classification;
	private String team;

	private String pendencies;

	@DateTimeFormat(iso = TIME)
	private Date schedule;

	private SortedSet<UserDTO> editors = new TreeSet<>();
	private SortedSet<UserDTO> producers = new TreeSet<>();
	private SortedSet<UserDTO> reporters = new TreeSet<>();

	private Set<String> vehicles = new HashSet<>();

	private SortedSet<GuideDTO> guides = new TreeSet<>();

	private SortedSet<TaskDTO> tasks = new TreeSet<>();

	public AssignmentDTO() {
		super();
	}

	public AssignmentDTO(Guideline guideline) {
		this(guideline, false);
	}

	public AssignmentDTO(Guideline guideline, boolean completeData) {
		super(guideline);

		this.proposal = guideline.getProposal();
		this.classification = parseClassification(guideline.getClassification());
		this.team = parseTeam(guideline.getTeam());
		this.pendencies = guideline.pendencies();
		this.schedule = guideline.firstGuideSchedule();

		for (User editor : guideline.getEditors()) {
			editors.add(new UserDTO(editor));
		}

		for (User producer : guideline.getProducers()) {
			producers.add(new UserDTO(producer));
		}

		for (User reporter : guideline.getReporters()) {
			reporters.add(new UserDTO(reporter));
		}

		for (CommunicationVehicle vehicle : guideline.getVehicles()) {
			vehicles.add(parseVehicle(vehicle));
		}

		if (completeData) {
			this.referral = guideline.getReferral();
			this.information = guideline.getInformations();

			for (Guide guide : guideline.getGuides()) {
				guides.add(new GuideDTO(guide));
			}

			if (guideline.getChecklist() != null) {
				for (ChecklistTask task : guideline.getChecklist().getTasks()) {
					tasks.add(new TaskDTO(task));
				}
			}
		}
	}

	private String parseClassification(GuidelineClassification classification) {
		return classification == null ? null : classification.getName();
	}

	private String parseTeam(Team team) {
		return team == null ? null : capitalizeFully(team.getName());
	}

	private String parseVehicle(CommunicationVehicle vehicle) {
		switch (vehicle) {
			case  AGENCY: return "Agency";
			case JOURNAL: return "Journal";
			case   RADIO: return "Radio";
		 	default:      return vehicle.name();
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public String getProposal() {
		return proposal;
	}

	public void setProposal(String proposal) {
		this.proposal = proposal;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getInformation() {
		return information;
	}

	public void setInformation(String information) {
		this.information = information;
	}

	public String getClassification() {
		return classification;
	}

	public void setClassification(String classification) {
		this.classification = classification;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public SortedSet<UserDTO> getProducers() {
		return producers;
	}

	public void setProducers(SortedSet<UserDTO> producers) {
		this.producers = producers;
	}

	public SortedSet<UserDTO> getReporters() {
		return reporters;
	}

	public void setReporters(SortedSet<UserDTO> reporters) {
		this.reporters = reporters;
	}

	public SortedSet<UserDTO> getEditors() {
		return editors;
	}

	public void setEditors(SortedSet<UserDTO> editors) {
		this.editors = editors;
	}

	public SortedSet<GuideDTO> getGuides() {
		return guides;
	}

	public void setGuides(SortedSet<GuideDTO> guides) {
		this.guides = guides;
	}

	public String getPendencies() {
		return pendencies;
	}

	public void setPendencies(String pendencies) {
		this.pendencies = pendencies;
	}

	public Date getSchedule() {
		return schedule;
	}

	public void setSchedule(Date schedule) {
		this.schedule = schedule;
	}

	public Set<String> getVehicles() {
		return vehicles;
	}

	public void setVehicles(Set<String> vehicles) {
		this.vehicles = vehicles;
	}

	public SortedSet<TaskDTO> getTasks() {
		return tasks;
	}

	public void setTasks(SortedSet<TaskDTO> tasks) {
		this.tasks = tasks;
	}

	private static final long serialVersionUID = -6010189961965341543L;
}
