package tv.snews.anews.web.dto;

import org.springframework.format.annotation.DateTimeFormat;
import tv.snews.anews.domain.ChecklistTask;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import static org.springframework.format.annotation.DateTimeFormat.ISO.TIME;

/**
 * @author Felipe Zap de Mello
 */
public class TaskDTO implements Comparable<TaskDTO>, Serializable {

	private Integer id;

	@DateTimeFormat(iso = TIME)
	private Date date;

	@DateTimeFormat(iso = TIME)
	private Date time;

	private String label;
	private String info;

	private boolean done;
	private int index;

	public TaskDTO(ChecklistTask checklistTask) {
		this.id = checklistTask.getId().intValue();
		this.label = checklistTask.getLabel();
		this.info = checklistTask.getInfo();
		this.done = checklistTask.isDone();
		this.index = checklistTask.getIndex();
		this.time = checklistTask.getTime();
		this.date = checklistTask.getDate();
	}

	//----------------------------------
	//  Operations
	//----------------------------------

	@Override
	public int compareTo(TaskDTO other) {
		return index - other.index;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;

		if (obj instanceof TaskDTO) {
			TaskDTO other = (TaskDTO) obj;
			return Objects.equals(index, other.index)
					&& Objects.equals(label, other.label);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(label, info, done);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	private static final long serialVersionUID = -5419981204342271807L;
}
