package tv.snews.anews.web.dto;

import tv.snews.anews.domain.Document;
import tv.snews.anews.domain.DocumentState;
import tv.snews.anews.domain.Program;

import java.io.Serializable;
import java.util.Date;

import static org.apache.commons.lang.WordUtils.capitalizeFully;

/**
 * @author Felipe Pinheiro
 */
public abstract class DocumentDTO implements Serializable {

	protected Long id;
	protected String slug;
	protected String state;
	protected Date date;
	protected TvShowDTO tvShow;

	public DocumentDTO() {}

	public DocumentDTO(Document<?> document) {
		this.id = document.getId();
		this.slug = capitalizeFully(document.getSlug());
		this.state = parseState(document.getState());
		this.date = document.getDate();
		this.tvShow = parseTvShow(document.getProgram());
	}

	private String parseState(DocumentState state) {
		return state == null ? null : state.toString();
	}

	private TvShowDTO parseTvShow(Program program) {
		return program == null ? null : new TvShowDTO(program);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TvShowDTO getTvShow() {
		return tvShow;
	}

	public void setTvShow(TvShowDTO tvShow) {
		this.tvShow = tvShow;
	}

	private static final long serialVersionUID = -9397284172353263L;
}
