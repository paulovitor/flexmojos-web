package tv.snews.anews.web.dto;

import tv.snews.anews.domain.User;

/**
 * @author Felipe Pinheiro
 */
public class UserDTO implements Comparable<UserDTO> {

	private Integer id;
	private String name;
	private String nickname;
	private String email;

	public UserDTO() {}

	public UserDTO(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.nickname = user.getNickname();
		this.email = user.getEmail();
	}

	@Override
	public int compareTo(UserDTO other) {
		return nickname.compareTo(other.nickname);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
