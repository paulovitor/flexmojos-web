package tv.snews.anews.web;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import tv.snews.anews.exception.UploadException;
import tv.snews.anews.flex.UploadXmlResponse;
import tv.snews.anews.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static tv.snews.anews.util.StorageUtil.anewsDirectory;

/**
 * Faz upload de imagens costumizadas do sistema.
 *
 * @author Eliezer Reis.
 * @author Samuel Guedes de Melo.
 * @since 1.0
 */
@Controller
public class UploadLogoController {

	private static final Logger log = LoggerFactory.getLogger(UploadLogoController.class);

	private static final List<String> ALLOWED_EXTENSIONS = Arrays.asList(
			// Somente Images
			".png", ".jpg", ".jpeg", ".gif"
	);

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@Transactional
	@ResponseBody
	@RequestMapping(value = "files/uploadLogo.do", method = POST)
	public UploadXmlResponse handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// Get some parameters from interface.
		String extension = ServletRequestUtils.getStringParameter(request, "extension");
		String source = ServletRequestUtils.getStringParameter(request, "source");
		source = source.substring(0, 1).toUpperCase() + source.substring(1);

		File tempDir = null;

		if (StringUtils.isBlank(extension)) {
			throw new UploadException(bundle.getMessage("upload.extensionNotFound", extension));
		}

		if (!ALLOWED_EXTENSIONS.contains(extension.toLowerCase(Locale.getDefault()))) {
			throw new UploadException(bundle.getMessage("upload.extensionNotAllowed", extension.toLowerCase(Locale.getDefault())));
		}

		if (source.equals("BannerLogin")) {
			String logoPath = anewsDirectory() + "img";
			File path = new File(logoPath);
			if (!path.exists()) {
				if (!path.mkdirs()) {
					throw new Exception("");
				}
			}
			tempDir = path;
		}

		if (source.equals("SystemLogo")) {
			String logoPath = anewsDirectory() + "img";
			File path = new File(logoPath);
			if (!path.exists()) {
				if (!path.mkdirs()) {
					throw new Exception("");
				}
			}
			tempDir = path;
		}

		if (source.equals("ReportLogo")) {
			String logoPath = anewsDirectory() + "img";
			File path = new File(logoPath);
			if (!path.exists()) {
				if (!path.mkdirs()) {
					throw new Exception("");
				}
			}
			tempDir = path;
		}

		long size = ServletRequestUtils.getLongParameter(request, "size", 0);

		try {
			// Start to process the file upload.
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setRepository(tempDir);

			ServletFileUpload upload = new ServletFileUpload(factory);

			List<FileItem> items = upload.parseRequest(request);
			File file = new File(tempDir.getPath() + System.getProperty("file.separator") + source + extension);

			for (FileItem item : items) {
				if (!item.isFormField()) {
					item.write(file);
				}
			}
		} catch (Exception e) {
			log.error("Error on upload processing.", e);
		}

		// Notify the interface about informations of the file.
		return new UploadXmlResponse.Builder()
				.fileName(source)
				.extension(extension)
				.path(tempDir.getPath())
				.size(size)
				.build();
	}

	private static class FileXml {

	}
}
