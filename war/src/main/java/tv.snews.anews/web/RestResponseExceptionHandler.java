package tv.snews.anews.web;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;

import static org.springframework.http.HttpStatus.*;

/**
 * @author Felipe Pinheiro
 */
@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

	public RestResponseExceptionHandler() {
		super();
	}

	//----------------------------------
	//	Bad Request - 400
	//----------------------------------

	@ExceptionHandler(value = { ConstraintViolationException.class })
	public ResponseEntity<Object> handleBadRequest(ConstraintViolationException ex, WebRequest request) {
		return handleExceptionHelper(ex, BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return handleExceptionHelper(ex, BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return handleExceptionHelper(ex, BAD_REQUEST, request);
	}

	//----------------------------------
	//	Forbidden - 401
	//----------------------------------

	@ExceptionHandler(value = { AuthenticationException.class })
	public ResponseEntity<Object> handleAuthenticationFailed(AuthenticationException ex, WebRequest request) {
		return handleExceptionHelper(ex, UNAUTHORIZED, request);
	}

	//----------------------------------
	//	Not Found - 404
	//----------------------------------

	@ExceptionHandler(value = { EntityNotFoundException.class, ResourceNotFoundException.class })
	public ResponseEntity<Object> handleNotFound(RuntimeException ex, WebRequest request) {
		return handleExceptionHelper(ex, NOT_FOUND, request);
	}

	//----------------------------------
	//	Conflict - 409
	//----------------------------------

	@ExceptionHandler(value = { InvalidDataAccessApiUsageException.class, DataIntegrityViolationException.class, DataAccessException.class })
	public ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		return handleExceptionHelper(ex, CONFLICT, request);
	}

	//----------------------------------
	//	Internal Server Error - 500
	//----------------------------------

	@ExceptionHandler(value = { NullPointerException.class, IllegalArgumentException.class, IllegalStateException.class })
	public ResponseEntity<Object> handleInternalServerError(RuntimeException ex, WebRequest request) {
		return handleExceptionHelper(ex, INTERNAL_SERVER_ERROR, request);
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private ResponseEntity<Object> handleExceptionHelper(Exception ex, HttpStatus status, WebRequest request) {
		ExceptionBody body = new ExceptionBody(ex, status);
		return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
	}

	private static class ExceptionBody {

		private int code;
		private String message;

		public ExceptionBody(Exception ex, HttpStatus status) {
			this.code = status.value();
			this.message = ex.getMessage();
		}

		public int getCode() {
			return code;
		}

		public String getMessage() {
			return message;
		}
	}
}
