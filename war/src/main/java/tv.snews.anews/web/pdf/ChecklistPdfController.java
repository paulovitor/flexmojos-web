package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.ChecklistDao;
import tv.snews.anews.dao.ChecklistFullTextDao;
import tv.snews.anews.dao.ChecklistTypeDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.ChecklistParams;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class ChecklistPdfController extends PdfController {

	@Autowired
	private ChecklistDao checklistDao;

	@Autowired
	private ChecklistFullTextDao checklistFTDao;

	@Autowired
	private ChecklistTypeDao checklistTypeDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "reports/checklist.pdf", produces = "application/pdf")
	public ModelAndView checklistHandler(String ids, String reportDir) {
		List<Checklist> data = new ArrayList<>();
		loadEntities(ids, checklistDao, data);

		String pathImageCheck = reportDir + "img/check.png";
		String pathImageUncheck = reportDir + "img/unchecked.png";

		Map<String, Object> model = createBasicModel(data);
		model.put("check", getImageStream(pathImageCheck));
		model.put("uncheck", getImageStream(pathImageUncheck));
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Checklist.jasper"), model);
	}

	@RequestMapping(value = "reports/checklistGrid.pdf", produces = "application/pdf")
	public ModelAndView checklistGridHandler(String date, String reportDir) throws ParseException {
		Date dateParsed = date.isEmpty() ? null : getDateFormat().parse(date);
		List<Checklist> data = checklistDao.findAllNotExcludedOfDate(dateParsed, false);

		Map<String, Object> model = createBasicModel(data);
		model.put("date", dateParsed);
		model.put("title", bundle.getMessage("checklist.title"));
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/ChecklistGrid.jasper"), model);
	}

	@RequestMapping(value = "reports/checklistArchiveGrid.pdf", produces = "application/pdf")
	public ModelAndView checklistArchiveGridHandler(
			String periodStart, String periodEnd, String producerId, String checklistTypeId,
			String slug, String text, String ignoreText, String searchType, String reportDir
	) throws ParseException {
		List<Checklist> data;
		ChecklistParams checklistParams = new ChecklistParams();

		Date periodStartDate = periodStart.isEmpty() ? null : getDateFormat().parse(periodStart);
		Date periodEndDate = periodEnd.isEmpty() ? null : getDateFormat().parse(periodEnd);
		User producer = producerId.isEmpty() ? null : userDao.get(new Integer(producerId));
		ChecklistType checklistType = checklistTypeId.isEmpty() ? null : checklistTypeDao.get(new Integer(checklistTypeId));

		checklistParams.setInitialDate(periodStartDate);
		checklistParams.setFinalDate(periodEndDate);
		checklistParams.setProducer(producer);
		checklistParams.setType(checklistType);
		checklistParams.setSlug(slug);
		checklistParams.setText(text);
		checklistParams.setSearchType(SearchType.valueOf(searchType));
		checklistParams.setIgnoreText(ignoreText);
		checklistParams.setPage(1);

		if (checklistParams.hasText()) {
			data = checklistFTDao.listTheFirstHundredByParams(checklistParams);
		} else {
			data = checklistDao.findByParams(checklistParams, 100);
		}

		Map<String, Object> model = createBasicModel(data);
		model.put("date", new Date());
		model.put("title", bundle.getMessage("checklist.archive.areaTitle"));
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/ChecklistGrid.jasper"), model);
	}
}
