package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.dao.StoryFullTextDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.StoryCG;
import tv.snews.anews.search.StoryParams;

import java.text.ParseException;
import java.util.*;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class StoryPdfController extends PdfController {

	@Autowired
	private StoryDao storyDao;

	@Autowired
	private StoryFullTextDao storyFTSDao;

	@Autowired
	private ProgramDao programDao;

	@RequestMapping(value = "reports/story.pdf", produces = "application/pdf")
	public ModelAndView storyHandler(String ids, String reportDir) {
		List<Story> data = new ArrayList<>();
		loadEntities(ids, storyDao, data);
		Collections.sort(data);

		for (Story story : data) {
			story.calculateTotal();
		}

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Story.jasper"), model);
	}

	@RequestMapping(value = "reports/storyPlaylist.pdf", produces = "application/pdf")
	public ModelAndView storyPlaylistHandler(String ids, String reportDir) {
		List<Story> data = new ArrayList<>();
		loadEntities(ids, storyDao, data);

		for (Story story : data) {
			story.calculateTotal();
		}

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/StoryPlaylist.jasper"), model);
	}

	@RequestMapping(value = "reports/storyOff.pdf", produces = "application/pdf")
	public ModelAndView storyOffHandler(String ids, String reportDir) {
		List<Story> data = new ArrayList<>();
		loadEntities(ids, storyDao, data);
		Collections.sort(data);

		for (Story story : data) {
			story.calculateTotal();
		}

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/StoryOff.jasper"), model);
	}

	@RequestMapping(value = "reports/storyArchiveGrid.pdf", produces = "application/pdf")
	public ModelAndView storyArchiveGridHandler(
			String programId, String periodStart, String periodEnd, String slug,
			String text, String type, String ignoreText, String reportDir
	) throws ParseException {

		StoryParams storyParams = new StoryParams();
		Program program = null;

		if (programId != null && !programId.isEmpty()) {
			if (programId.equals("-1")) {
				program = new Program();
				program.setId(-1);
			} else {
				program = programDao.get(new Integer(programId));
			}
		}

		Date periodStartDate = periodStart.isEmpty() ? null : getDateFormat().parse(periodStart);
		Date periodEndDate = periodEnd.isEmpty() ? null : getDateFormat().parse(periodEnd);

		storyParams.setInitialDate(periodStartDate);
		storyParams.setFinalDate(periodEndDate);
		storyParams.setProgram(program);
		storyParams.setSlug(slug);
		storyParams.setText(text);
		storyParams.setSearchType(SearchType.valueOf(type));
		storyParams.setIgnoreText(ignoreText);
		storyParams.setPage(1);

		List<Story> data = storyFTSDao.listTheFirstHundredByParams(storyParams);
		String pathImage = reportDir + "img/";

		Map<String, Object> model = createBasicModel(data);
		model.put("PATH_IMAGE", pathImage);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/StoryArchiveGrid.jasper"), model);
	}

	@RequestMapping(value = "reports/cgs.pdf", produces = "application/pdf")
	public ModelAndView cgsHandler(String ids, String reportDir) {
		List<Story> data = new ArrayList<>();
		loadEntities(ids, storyDao, data);

		for (Story story : data) {
			if (story.getCgSubSections().isEmpty()) {
				story.setCgSubSections(story.subSectionsOfType(StoryCG.class));
			}
		}

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Cgs.jasper"), model);
	}
}
