package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.ReportDao;
import tv.snews.anews.domain.GuidelineReport;
import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.ReportageReport;
import tv.snews.anews.domain.StoryReport;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class ReportPdfController extends PdfController {

	@Autowired
	private ReportDao reportDao;

	@RequestMapping(value = "reports/report.pdf", produces = "application/pdf")
	public ModelAndView reportHandler(String ids, String reportDir) {
		List<Report> data = new ArrayList<>();
		loadEntities(ids, reportDao, data);

		for (Report report : data) {
			report.setOrigin(bundle.getMessage("report.other")); // default value

			if (report instanceof GuidelineReport) {
				report.setOrigin(bundle.getMessage("report.guideline"));
			}
			if (report instanceof ReportageReport) {
				report.setOrigin(bundle.getMessage("report.reportage"));
			}
			if (report instanceof StoryReport) {
				report.setOrigin(bundle.getMessage("report.story"));
			}
		}

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Report.jasper"), model);
	}
}
