package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.NewsDao;
import tv.snews.anews.domain.News;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class NewsPdfController extends PdfController {

	@Autowired
	private NewsDao newsDao;

	@RequestMapping(value = "reports/news.pdf", produces = "application/pdf")
	public ModelAndView newsHandler(String ids, String reportDir) {
		List<News> data = new ArrayList<>();
		loadEntities(ids, newsDao, data);

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/News.jasper"), model);
	}
}
