package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.RoundDao;
import tv.snews.anews.domain.Round;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class RoundPdfController extends PdfController {

	@Autowired
	private RoundDao roundDao;

	@RequestMapping(value = "reports/round.pdf", produces = "application/pdf")
	public ModelAndView roundHandler(String date, String ids) throws ParseException {
		List<Round> data;
		if (date != null) {
			Date dateParsed = getDateFormat().parse(date);
			data = roundDao.listAll(dateParsed);
		} else {
			data = new ArrayList<>();
			loadEntities(ids, roundDao, data);
		}

		return new ModelAndView(pdfView("/reports/Round.jasper"), createBasicModel(data));
	}
}
