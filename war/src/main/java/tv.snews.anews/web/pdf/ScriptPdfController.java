package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.ScriptDao;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptImage;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.util.FontFamily;
import tv.snews.anews.util.FontSize;
import tv.snews.anews.util.ImageUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class ScriptPdfController extends PdfController {

	@Autowired
	private ScriptDao scriptDao;

	@RequestMapping(value = "reports/script.pdf", produces = "application/pdf")
	public ModelAndView scriptHandler(String ids, String reportDir) throws ParseException, IOException {
		List<Script> data = new ArrayList<>();
		loadEntities(ids, scriptDao, data);
		Collections.sort(data);

		for (Script current : data) {
			for (ScriptImage scriptImage : current.getImages()) {
				scriptImage.setThumbnailBytes(ImageUtil.loadImg(scriptImage.getThumbnailPath()));
			}
		}

		Map<String, Object> model = createBasicModel(data);

		String pathImage = reportDir + "img/";

		Settings settings = settingsDao.loadSettings();

		model.put("fontFamily", FontFamily.parse(settings.getReportFontFamily()));
		model.put("fontSize", FontSize.parse(settings.getReportFontSize()));

		model.put("bundle", bundle);
		model.put("data", data);
		model.put("logo", getLogoStream());
		model.put("PATH_IMAGE", pathImage);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Script.jasper"), model);
	}

	@RequestMapping(value = "reports/scriptPlaylist.pdf", produces = "application/pdf")
	public ModelAndView scriptPlaylistHandler(String ids, String reportDir) {
		List<Script> data = new ArrayList<>();
		loadEntities(ids, scriptDao, data);

		Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/ScriptPlaylist.jasper"), model);
	}
}
