package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.BlockDao;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.Story;
import tv.snews.anews.util.DateTimeUtil;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class RundownPdfController extends PdfController {

	@Autowired
	private RundownDao rundownDao;

	@Autowired
	private BlockDao blockDao;

	@Autowired
	private StoryDao storyDao;

	@RequestMapping(value = "reports/rundown.pdf", produces = "application/pdf")
	public ModelAndView rundownHandler(Long id, Boolean onlyApproved, Boolean includeStandBy) {
		Rundown rundown = rundownDao.get(id);
		Date totalCommercial = DateTimeUtil.minimizeTime(new Date());

		for (Block b : blockDao.listAll(rundown)) {
			totalCommercial = DateTimeUtil.sumTimes(totalCommercial, b.getCommercial());
		}

		List<Story> data = storyDao.listStoriesByRundown(rundown, onlyApproved, includeStandBy);
		Collections.sort(data);

		Settings settings = loadSettings();

		Map<String, Object> model = createBasicModel(data);
		model.put("totalBlockComercial", totalCommercial);
		model.put("displayImageEditor", settings.isEnableRundownImageEditor());

		return new ModelAndView(pdfView("/reports/Rundown.jasper"), model);
	}
}
