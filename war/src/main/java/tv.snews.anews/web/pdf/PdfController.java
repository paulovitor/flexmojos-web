package tv.snews.anews.web.pdf;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.servlet.view.jasperreports.JasperReportsPdfView;
import tv.snews.anews.dao.GenericDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.AbstractEntity;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.util.FontFamily;
import tv.snews.anews.util.FontSize;
import tv.snews.anews.util.ResourceBundle;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static tv.snews.anews.util.StorageUtil.anewsDirectory;

/**
 * @author Felipe Pinheiro
 */
public abstract class PdfController implements ResourceLoaderAware {

	private static final String DATA_KEY = "data";

	private static final Logger log = LoggerFactory.getLogger(PdfController.class);

	@Autowired
	protected ApplicationContext applicationContext;

	protected SettingsDao settingsDao;

	protected ResourceBundle bundle = ResourceBundle.INSTANCE;
	protected ResourceLoader resourceLoader;

	protected Map<String, Object> createBasicModel(List<?> data) {
		Map<String, Object> model = new HashMap<>();

		model.put(DATA_KEY, data);
		model.put("bundle", bundle);
		model.put("logo", getLogoStream());

		Settings settings = loadSettings();

		model.put("fontFamily", FontFamily.parse(settings.getReportFontFamily()));
		model.put("fontSize", FontSize.parse(settings.getReportFontSize()));

		return model;
	}

	protected Map<String, Object> createBasicModel(Object data) {
		List<Object> dataList = new ArrayList<>();
		dataList.add(data);
		return createBasicModel(dataList);
	}

	protected InputStream getImageStream(String pathImage) {
		if (StringUtils.isNotBlank(pathImage)) {
			try {
				Resource resource = resourceLoader.getResource(pathImage);
				return resource.getInputStream();
			} catch (IOException e) {
				log.warn("Could not load the report logo: {}.", e.getMessage());
			}
		}

		return null;
	}

	protected InputStream getLogoStream() {
		String logoPath =  anewsDirectory() + "img" + System.getProperty("file.separator");
		Settings settings = loadSettings();

		if (StringUtils.isNotBlank(settings.getStorageLocationReportLogo())) {
			try {
				Resource resource = resourceLoader.getResource("file:" + logoPath + settings.getStorageLocationReportLogo());
				return resource.getInputStream();
			} catch (IOException e) {
				log.warn("Could not load the report logo: {}.", e.getMessage());
			}
		}

		return null;
	}

	protected JasperReportsPdfView pdfView(String jasperFile) {
		JasperReportsPdfView view = new JasperReportsPdfView();
		view.setReportDataKey(DATA_KEY);
		view.setUrl(jasperFile);
		view.setApplicationContext(applicationContext);

		return view;
	}

	@SuppressWarnings("rawtypes")
	protected  <T extends AbstractEntity> void loadEntities(String ids, GenericDao<T> dao, List<T> data) {
		if (StringUtils.isNotEmpty(ids)) {
			for (String isStr : ids.split("\\|")) {
				try {
					Long id = Long.parseLong(isStr);
					T entity = dao.get(id, true);
					if (entity != null) {
						data.add(entity);
					}
				} catch (NumberFormatException e) {
					log.error("Could not parse the string (ID) to a Long.", e);
				}
			}
		}
	}

	protected Settings loadSettings() {
		return settingsDao.loadSettings();
	}

	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat(bundle.getMessage("defaults.dateFormat"));
	}

	@Autowired
	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}

	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
}
