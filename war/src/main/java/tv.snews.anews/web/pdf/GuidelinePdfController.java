package tv.snews.anews.web.pdf;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.search.GuidelineParams;

import java.text.ParseException;
import java.util.*;

import static java.util.Calendar.HOUR;
import static java.util.Calendar.MINUTE;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class GuidelinePdfController extends PdfController {

	@Autowired
	private GuidelineDao guidelineDao;

	@Autowired
	private GuidelineFullTextDao guidelineFTSDao;

	@Autowired
	private GuidelineClassificationDao classificationDao;

	@Autowired
	private ProgramDao programDao;

	@Autowired
	private TeamDao teamDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "reports/task.pdf", produces = "application/pdf")
	public ModelAndView taskHandler(String ids, String reportDir) {
		List<Guideline> data = new ArrayList<>();
		loadEntities(ids, guidelineDao, data);

		for (Guideline guideline : data) {
			Guide guide = guideline.firstGuide();

			if (guide != null) {
				Calendar guidelineCal = Calendar.getInstance();
				guidelineCal.setTime(guideline.getDate());

				Calendar guideCal = Calendar.getInstance();
				if(guide.getSchedule() != null) {
					guideCal.setTime(guide.getSchedule());
				}

				guidelineCal.set(HOUR, guideCal.get(HOUR));
				guidelineCal.set(MINUTE, guideCal.get(MINUTE));

				guideline.setGuideDate(guidelineCal.getTime());
			}
		}

		String pathImageCheck = reportDir + "img/check.png";
		String pathImageUncheck = reportDir + "img/unchecked.png";

		Map<String, Object> model = createBasicModel(data);
		model.put("check", getImageStream(pathImageCheck));
		model.put("uncheck", getImageStream(pathImageUncheck));
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Task.jasper"), model);
	}

	@RequestMapping(value = "reports/guidelineGrid.pdf", produces = "application/pdf")
	public ModelAndView guidelineGridHandler(String ids, String date, Integer programId) throws ParseException {
		Date dateParsed = getDateFormat().parse(date);

		Program program = null;

		if (programId == -1) { // Gaveta Geral
			program = new Program();
			program.setId(-1);
		} else if (programId != null) { // Programa Específico
			program = programDao.get(programId);
		}

		List<Guideline> data = new ArrayList<>();

		if (StringUtils.isNotEmpty(ids)) {
			loadEntities(ids, guidelineDao, data);
		} else {
			data = guidelineDao.findByDate(dateParsed, program, null);
		}

		Settings settings = settingsDao.loadSettings();

		Map<String, Object> model = createBasicModel(data);
		model.put("displayTeam", settings.isEnableGuidelineTeam());
		return new ModelAndView(pdfView("/reports/GuidelineGrid.jasper"), model);
	}

	@RequestMapping(value = "reports/guidelineArchiveGrid.pdf", produces = "application/pdf")
	public ModelAndView guidelineArchiveGridHandler(
			String programId, String producerId, String reporterId, String classificationText,
			String team, String periodStart, String periodEnd, String slug, String text,
			String type, String ignoreText, String vehicle, String reportDir
	) throws ParseException {

		GuidelineParams guidelineParams = new GuidelineParams();
		Program program = null;

		if (programId != null && !programId.isEmpty()) {
			if (programId.equals("-1")) {
				program = new Program();
				program.setId(-1);
			} else {
				program = programDao.get(new Integer(programId));
			}
		}

		User producer = producerId.isEmpty() ? null : userDao.get(new Integer(producerId));
		User reporter = reporterId.isEmpty() ? null : userDao.get(new Integer(reporterId));
		GuidelineClassification classification = classificationText.isEmpty() ? null : classificationDao.get(new Integer(classificationText));
		Team teamObj = team.isEmpty() ? null : teamDao.get(new Integer(team));

		Date periodStartDate = periodStart.isEmpty() ? null : getDateFormat().parse(periodStart);
		Date periodEndDate = periodEnd.isEmpty() ? null : getDateFormat().parse(periodEnd);

		guidelineParams.setInitialDate(periodStartDate);
		guidelineParams.setFinalDate(periodEndDate);
		guidelineParams.setProgram(program);
		guidelineParams.setProducer(producer);
		guidelineParams.setReporter(reporter);
		guidelineParams.setSlug(slug);
		guidelineParams.setText(text);
		guidelineParams.setSearchType(SearchType.valueOf(type));
		guidelineParams.setIgnoreText(ignoreText);
		guidelineParams.setClassification(classification);
		guidelineParams.setTeam(teamObj);
		guidelineParams.setPage(1);
		guidelineParams.setVehicle(vehicle.isEmpty() ? null : CommunicationVehicle.valueOf(vehicle));

		List<Guideline> data = guidelineFTSDao.listTheFirstHundredByParams(guidelineParams);
		String pathImage = reportDir + "img/";

		Settings settings = settingsDao.loadSettings();

		Map<String, Object> model = createBasicModel(data);
		model.put("PATH_IMAGE", pathImage);
		model.put("SUBREPORT_DIR", reportDir);
		model.put("displayTeam", settings.isEnableGuidelineTeam());

		return new ModelAndView(pdfView("/reports/GuidelineArchiveGrid.jasper"), model);
	}
}
