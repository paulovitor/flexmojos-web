package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.ScriptPlanDao;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptImage;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.report.ScriptBlockReportData;
import tv.snews.anews.report.ScriptPlanReportData;
import tv.snews.anews.util.FontFamily;
import tv.snews.anews.util.FontSize;
import tv.snews.anews.util.ImageUtil;

import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class ScriptPlanPdfController extends PdfController {

	@Autowired
	private ScriptPlanDao scriptPlanDao;

	@RequestMapping(value = "reports/scriptPlan.pdf", produces = "application/pdf")
	public ModelAndView scriptPlanHandler(Long id, boolean onlyApproved, String reportDir) {
		ScriptPlan scriptPlan = scriptPlanDao.get(id);
		ScriptPlanReportData scriptPlanReportData = new ScriptPlanReportData(scriptPlan, onlyApproved);

		Map<String, Object> model = createBasicModel(scriptPlanReportData);
		model.put("SUBREPORT_DIR", reportDir);
		model.put("displayImageEditor", loadSettings().isEnableScriptPlanImageEditor());

		return new ModelAndView(pdfView("/reports/ScriptPlan.jasper"), model);
	}

	@RequestMapping(value = "reports/scriptPlanFull.pdf", produces = "application/pdf")
	public ModelAndView scriptPlanFullHandler(Long id, Long blockId, boolean onlyApproved, String reportDir) {
		ScriptPlan scriptPlan = scriptPlanDao.loadFull(id, blockId);

		ScriptPlanReportData scriptPlanFullReportData = new ScriptPlanReportData(scriptPlan, onlyApproved);

		for(ScriptBlockReportData scriptBlockReportData : scriptPlanFullReportData.getBlocksData()) {
			for (Script script : scriptBlockReportData.getScripts()) {
				for (ScriptImage scriptImage : script.getImages()) {
					scriptImage.setThumbnailBytes(ImageUtil.loadImg(scriptImage.getThumbnailPath()));
				}
			}
		}

		Settings settings = settingsDao.loadSettings();

		Map<String, Object> model = createBasicModel(scriptPlanFullReportData);
		model.put("fontFamily", FontFamily.parse(settings.getReportFontFamily()));
		model.put("fontSize", FontSize.parse(settings.getReportFontSize()));
		model.put("bundle", bundle);
		model.put("logo", getLogoStream());
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/ScriptPlanFull.jasper"), model);
	}
}
