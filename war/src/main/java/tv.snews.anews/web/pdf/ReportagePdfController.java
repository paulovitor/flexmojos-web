package tv.snews.anews.web.pdf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.ReportageDao;
import tv.snews.anews.dao.ReportageFullTextDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.ReportageParams;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
@Controller
public class ReportagePdfController extends PdfController {

	@Autowired
	private ReportageDao reportageDao;

	@Autowired
	private ReportageFullTextDao reportageFTSDao;

	@Autowired
	private ProgramDao programDao;

	@Autowired
	private UserDao userDao;

	@RequestMapping(value = "reports/reportage.pdf", produces = "application/pdf")
	public ModelAndView reportageHandler(String ids, String reportDir) {
		List<Reportage> data = new ArrayList<>();
		loadEntities(ids, reportageDao, data);

		String pathImage = reportDir + "img/";

		Map<String, Object> model = createBasicModel(data);
		model.put("PATH_IMAGE", pathImage);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/Reportage.jasper"), model);
	}

	@RequestMapping(value = "reports/reportageArchiveGrid.pdf", produces = "application/pdf")
	public ModelAndView reportageArchiveGridHandler(
			String programId, String reporterId, String periodStart, String periodEnd,
			String slug, String text, String type, String ignoreText, String reportDir
	) throws ParseException {
		ReportageParams reportageParams = new ReportageParams();
		Program program = null;

		if (programId != null && !programId.isEmpty()) {
			if (programId.equals("-1")) {
				program = new Program();
				program.setId(-1);
			} else {
				program = programDao.get(new Integer(programId));
			}
		}

		User reporter = reporterId.isEmpty() ? null : userDao.get(new Integer(reporterId));
		Date periodStartDate = periodStart.isEmpty() ? null : getDateFormat().parse(periodStart);
		Date periodEndDate = periodEnd.isEmpty() ? null : getDateFormat().parse(periodEnd);

		reportageParams.setInitialDate(periodStartDate);
		reportageParams.setFinalDate(periodEndDate);
		reportageParams.setProgram(program);
		reportageParams.setReporter(reporter);
		reportageParams.setSlug(slug);
		reportageParams.setText(text);
		reportageParams.setSearchType(SearchType.valueOf(type));
		reportageParams.setIgnoreText(ignoreText);
		reportageParams.setPage(1);

		List<Reportage> data = reportageFTSDao.listTheFirstHundredByParams(reportageParams);
		String pathImage = reportDir + "img/";

		Map<String, Object> model = createBasicModel(data);
		model.put("PATH_IMAGE", pathImage);
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView(pdfView("/reports/ReportageArchiveGrid.jasper"), model);
	}
}
