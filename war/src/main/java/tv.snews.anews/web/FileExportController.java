package tv.snews.anews.web;

import nu.xom.Attribute;
import nu.xom.Document;
import nu.xom.Element;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.dao.WSMediaDao;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.WSMedia;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static java.lang.Integer.parseInt;
import static org.springframework.web.bind.ServletRequestUtils.getLongParameter;
import static org.springframework.web.bind.ServletRequestUtils.getStringParameter;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Felipe Pinheiro
 */
@Controller
@RequestMapping("export")
public class FileExportController {

	private RundownDao rundownDao;
	private WSMediaDao wsMediaDao;

	@Transactional
	@RequestMapping(method = GET, value = "/playlist.do")
	public ModelAndView playlistHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, IOException {
		Rundown rundown = loadRundown(req);
		List<WSMedia> medias = loadMedias(req);

		String xml = generateXML(rundown, medias);
		String xmlName = rundown.displayName().toLowerCase().replaceAll("[\\p{Space}\\p{Punct}]", "_");

		resp.setContentType("application/force-download");
		resp.setContentLength(xml.length());
		resp.setHeader("Content-Disposition", "attachment; filename=\"" + xmlName + ".xml\"");

		PrintWriter writer = new PrintWriter(resp.getOutputStream());
		writer.print(xml);
		writer.flush();

		return null;
	}

	private Rundown loadRundown(HttpServletRequest req) throws ServletRequestBindingException {
		Long rundownId = getLongParameter(req, "rundownId");
		return rundownDao.get(rundownId);
	}

	private List<WSMedia> loadMedias(HttpServletRequest req) throws ServletRequestBindingException {
		List<WSMedia> medias = new ArrayList<>();

		String mediasIds = getStringParameter(req, "mediasIds");
		StringTokenizer tokenizer = new StringTokenizer(mediasIds, "|");

		while (tokenizer.hasMoreTokens()) {
			Integer id = parseInt(tokenizer.nextToken());
			medias.add(wsMediaDao.get(id));
		}

		return medias;
	}

	private String generateXML(Rundown rundown, List<WSMedia> medias) {
//		List<StoryWSVideo> videos = rundown.storiesSubSectionsOfType(StoryWSVideo.class, true);

		/*
			Modelo do XML:

			<?xml version="1.0" encoding="UTF-8" standalone="no"?>
			<AMS>
				<PlayList DisplayName="gustavo" Loop="false" Name="2">
					<Clip ID="0_20" Index="0" Name="JORILHA05_01" PlayoutControl="Manual" VideoStandard="HD_1920x1080i_29_970" iIn="108000" iOut="109800"/>
					...
				</PlayList>
			</AMS>
	 */

		Element root = new Element("AMS");

		Element playlist = new Element("PlayList");
		playlist.addAttribute(new Attribute("DisplayName", rundown.displayName()));
		playlist.addAttribute(new Attribute("Loop", "false"));
		playlist.addAttribute(new Attribute("Name", rundown.displayName()));
		root.appendChild(playlist);

//		for (int i = 0; i < videos.size(); i++) {
//			StoryWSVideo storyVideo = videos.get(i);
//			WSMedia media = storyVideo.getMedia();

		for (int i = 0; i < medias.size(); i++) {
			WSMedia media = medias.get(i);

			Element clip = new Element("Clip");
			clip.addAttribute(new Attribute("ID", Integer.toString(media.getAssetId())));
			clip.addAttribute(new Attribute("Index", Integer.toString(i)));
			clip.addAttribute(new Attribute("Name", media.getFileName()));
			clip.addAttribute(new Attribute("PlayoutControl", "Manual"));
			// Atributos não-adicionados: VideoStandard, iIn, iOut

			playlist.appendChild(clip);
		}

		Document doc = new Document(root);
		return doc.toXML();
	}

	public void setRundownDao(RundownDao rundownDao) {
		this.rundownDao = rundownDao;
	}

	public void setWsMediaDao(WSMediaDao wsMediaDao) {
		this.wsMediaDao = wsMediaDao;
	}
}
