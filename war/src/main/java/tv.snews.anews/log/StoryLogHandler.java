package tv.snews.anews.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.StoryLog;
import tv.snews.anews.event.StoryEvent;

import java.util.Date;

import static org.apache.commons.lang.StringUtils.isEmpty;
import static tv.snews.anews.util.DateTimeUtil.dateToDateFormat;
import static tv.snews.anews.util.DateTimeUtil.dateToPatternFormat;

/**
 * Handler para registrar o log de ações da lauda no espelho
 *
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class StoryLogHandler extends GenericLogHandler implements ApplicationListener<StoryEvent> {

	private static final Logger logger = LoggerFactory.getLogger(StoryLogHandler.class);

	@Override
	public void onApplicationEvent(StoryEvent event) {

		for (StoryEvent.Change change : event.getChanges()) {
			Story story = change.getTarget();

			Object oldValue = change.getOldValue();
			Object newValue = change.getNewValue();

			String slugAndPage = getSlugAndPage(story);

			StoryLog log = new StoryLog(event.getRundown(), story, loadAuthor(), event.getAction());

			switch (event.getAction()) {
				case APPROVED:
					log.setAction(getMessage("story.log.approved", slugAndPage));
					break;
				case DISAPPROVED:
					log.setAction(getMessage("story.log.disapproved", slugAndPage));
					break;
				case COPIED:
					log.setAction(getMessage("story.log.copied", slugAndPage, oldValue, dateToDateFormat(story.getDate())));
					break;
				case EDITOR_CHANGED:
					String editor;
					if (story.getEditor() == null) {
						editor = getMessage("story.log.noEditor").toUpperCase();
					} else {
						editor = story.getEditor().getNickname().toUpperCase();
					}
					log.setAction(getMessage("story.log.editorChanged", slugAndPage, editor));
					break;
				case REPORTER_CHANGED:
					String reporter;
					if (story.getReporter() == null) {
						reporter = getMessage("story.log.noReporter").toUpperCase();
					} else {
						reporter = story.getReporter().getNickname().toUpperCase();
					}
					log.setAction(getMessage("story.log.reporterChanged", slugAndPage, reporter));
					break;
				case EXCLUDED:
					log.setAction(getMessage("story.log.excluded", slugAndPage));
					break;
				case INSERTED:
					log.setAction(getMessage("story.log.inserted", slugAndPage));
					break;
				case BLOCK_CHANGED:
					log.setAction(getMessage("story.log.movedBlock", slugAndPage, oldValue, newValue));
					break;
				case ORDER_CHANGED:
					log.setAction(getMessage("story.log.movedPosition", slugAndPage, oldValue, newValue));
					break;
				case ARCHIVED:
					log.setAction(getMessage("story.log.archived", slugAndPage));
					break;
				case SLUG_CHANGED:
					log.setAction(getMessage("story.log.slugChanged", story.getPage(), oldValue, story.getSlug()));
					break;
				case VT_CHANGED:
					log.setAction(getMessage("story.log.vtChanged", slugAndPage, dateToPatternFormat((Date) oldValue, "mm:ss"), dateToPatternFormat((Date) newValue, "mm:ss")));
					break;
				case OK:
					log.setAction(getMessage("story.log.ok", slugAndPage));
					break;
				case NOT_OK:
					log.setAction(getMessage("story.log.notOk", slugAndPage));
					break;
				case PAGE_CHANGED:
					String slug;
					if (isEmpty(story.getSlug())) {
						slug = getMessage("defaults.noSlug").toUpperCase();
					} else {
						slug = story.getSlug();
					}
					log.setAction(getMessage("story.log.pageChanged", slug, oldValue, story.getPage()));
					break;
                case EDITING_USER_CHANGED:
                    log.setAction(getMessage("story.log.userChanged", newValue, slugAndPage, oldValue));
                    break;
				default:
					logger.debug("Story event action ignored: " + event.getAction());
					return;
			}

			logDao.save(log);
		}
	}

	private String getSlugAndPage(Story story) {
		if (isEmpty(story.getSlug())) {
			return getMessage("defaults.noSlug").toUpperCase() + " " + getMessage("story.page") + " " + story.getPage();
		} else {
			return story.getSlug() + " " + getMessage("story.page") + " " + story.getPage();
		}
	}
}
