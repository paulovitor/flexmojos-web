package tv.snews.anews.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.RundownLog;
import tv.snews.anews.event.RundownEvent;
import tv.snews.anews.util.DateTimeUtil;

/**
 * Handler para registrar o log de ações do espelho
 *
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class RundownLogHandler extends GenericLogHandler implements ApplicationListener<RundownEvent> {

	private static final Logger logger = LoggerFactory.getLogger(RundownLogHandler.class);

	@Override
	public void onApplicationEvent(RundownEvent event) {
		Rundown rundown = event.getRundown();
		RundownLog log = new RundownLog(rundown, loadAuthor(), event.getAction());

		switch (event.getAction()) {
			case CREATED:
				log.setAction(getMessage("rundown.log.created", rundown.getProgram().getName()));
				break;
			case START_CHANGED:
				log.setAction(getMessage("rundown.log.startChanged", DateTimeUtil.dateToTimeFormat(rundown.getStart())));
				break;
			case END_CHANGED:
				log.setAction(getMessage("rundown.log.endChanged", DateTimeUtil.dateToTimeFormat(rundown.getEnd())));
				break;
			case PRODUCTION_CHANGED:
				log.setAction(getMessage("rundown.log.productionChanged", DateTimeUtil.dateToTimeFormat(rundown.getProduction())));
				break;
			default:
				logger.debug("Rundown event action ignored: " + event.getAction());
				return;
		}

		logDao.save(log);
	}
}
