package tv.snews.anews.log;

import tv.snews.anews.dao.LogDao;
import tv.snews.anews.domain.User;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.util.ResourceBundle;

/**
 * Classe genérica de implementação para
 * Logs do espelho.
 *
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public abstract class GenericLogHandler {

	protected LogDao logDao;
	protected SessionManager sessionManager;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	protected User loadAuthor() {
		return sessionManager.getCurrentUser();
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public void setLogDao(LogDao logDao) {
		this.logDao = logDao;
	}

	//------------------------------
	//	Bundle Methods
	//------------------------------

	public String getMessage(String code) {
		return bundle.getMessage(code);
	}

	public String getMessage(String code, Object... args) {
		return bundle.getMessage(code, args);
	}
//
//	public String getMessage(String code, Locale locale, Object... args) {
//		return bundle.getMessage(code, args, locale);
//	}
}
