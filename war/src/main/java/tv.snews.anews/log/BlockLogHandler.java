package tv.snews.anews.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.BlockLog;
import tv.snews.anews.event.BlockEvent;

import static tv.snews.anews.util.DateTimeUtil.dateToTimeFormat;

/**
 * Handler para registrar o log de ações nos blocos do espelho.
 * 
 * @author Felipe Zap de Mello
 * @since 1.3.0
 */
public class BlockLogHandler extends GenericLogHandler implements ApplicationListener<BlockEvent> {
	
	private static final Logger logger = LoggerFactory.getLogger(BlockLogHandler.class);
	
    @Override
    public void onApplicationEvent(BlockEvent event) {
    	Block block = event.getBlock();
    	BlockLog log = new BlockLog(block, blockName(block), loadAuthor(), event.getAction());
    	
    	switch (event.getAction()) {
			case BREAK_CHANGED:
				log.setAction(getMessage("block.log.breakChanged", blockName(block), dateToTimeFormat(block.getCommercial())));
				break;
			case EXCLUDED:
				log.setAction(getMessage("block.log.excluded", blockName(block)));
				break;
			case INSERTED:
				log.setAction(getMessage("block.log.inserted", blockName(block)));
				break;
			default:
				logger.debug("Block event action ignored: " + event.getAction());
				return;
		}
    	
    	logDao.save(log);
    }
    
    private String blockName(Block block) {
    	if (block.isStandBy()) {
    		return getMessage("rundown.standBy");
    	} else if (block.getOrder() < 9) {
    		return "0" + (block.getOrder() + 1);
    	} else {
    		return "" + block.getOrder() + 1;
    	}
    }
}
