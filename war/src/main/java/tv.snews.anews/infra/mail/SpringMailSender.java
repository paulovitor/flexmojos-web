package tv.snews.anews.infra.mail;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import tv.snews.anews.dao.MailConfigDao;
import tv.snews.anews.exception.MessageDeliveryException;

import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Implementação da interface {@link MailSender} utilizando o suporte do Spring
 * para envio de emails.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class SpringMailSender implements MailSender {

	private JavaMailSenderImpl sender;
	private MailConfigDao mailConfigDao;
	
	@Override
	public void send(String toEmail, String subject, String content) throws MessageDeliveryException {
		MailConfig config = getConfig();
		if (config == null) {
			throw new MessageDeliveryException("Undefined SMTP configuration.");
		} else {
			send(config.getEmail(), toEmail, subject, content);
		}
	}

	@Override
	public void send(String fromEmail, String toEmail, String subject, String content) throws MessageDeliveryException {
		JavaMailSenderImpl sender = getSender();
		
		if (sender == null) {
			throw new MessageDeliveryException("Undefined SMTP configuration.");
		} else {
			try {
				MimeMessage message = sender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(message, false, MESSAGE_ENCODING);
	
				helper.setFrom(fromEmail);
				helper.setSubject(subject);
				helper.setText(content, true);
				helper.setTo(toEmail);
	
				sender.send(message);
			} catch (Exception e) {
				throw new MessageDeliveryException("Message delivery exception.", e);
			}
		}
	}
	
	@Override
	public void resetConfiguration() {
	    sender = null;
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	private JavaMailSenderImpl getSender() {
		if (sender == null) {
			
			MailConfig config = getConfig();
			if (config != null) {
				sender = new JavaMailSenderImpl();
				sender.setHost(config.getHost());
				sender.setPort(config.getPort());
				
				Properties props = new Properties();
				if (config.isAuthenticated()) {
					props.setProperty("mail.smtp.auth", "true");
					
					if (config.getSsl() == SSL.SSL_TLS) {
						props.setProperty("mail.smtps.socketFactory.class", "tv.snews.anews.infra.mail.AlwaysTrustSSLContextFactory");
						props.setProperty("mail.smtps.socketFactory.port", config.getPort() + "");
						props.setProperty("mail.smtp.ssl.enable", "true");
						
						sender.setProtocol("smtps");
					}
					
					if (config.getSsl() == SSL.STARTTLS) {
						props.setProperty("mail.smtp.ssl.socketFactory.class", "tv.snews.anews.infra.mail.AlwaysTrustSSLContextFactory");
						props.setProperty("mail.smtp.ssl.socketFactory.port", config.getPort() + "");
						props.setProperty("mail.smtp.starttls.enable", "true");
					}
					
					sender.setUsername(config.getUsername());
					sender.setPassword(config.getPassword());
				} else {
					props.setProperty("mail.smtp.auth", "false");
				}
				
				sender.setJavaMailProperties(props);
			}
		}
		return sender;
	}
	
	private MailConfig getConfig() {
		return mailConfigDao.loadFirst();
	}
	
	//--------------------------------------------------------------------------
	//	Setters
	//--------------------------------------------------------------------------
	
    public void setMailConfigDao(MailConfigDao mailConfigDao) {
	    this.mailConfigDao = mailConfigDao;
    }
}
