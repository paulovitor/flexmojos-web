package tv.snews.anews.infra.mail;

/**
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public enum SSL {

	NONE, STARTTLS, SSL_TLS;
}
