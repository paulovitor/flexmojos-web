package tv.snews.anews.infra.mail;

import tv.snews.anews.exception.MessageDeliveryException;

/**
 * Interface o suporte para envio de emails pelo sistema.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface MailSender {

	/**
	 * Encoding utilizado nas mensagens.
	 */
	String MESSAGE_ENCODING = "UTF-8";
	
	/**
	 * Faz o email de uma mensagem de email para um email de destino, permitindo
	 * informar o título e o conteúdo da mensagem.
	 * 
	 * @param toEmail Email do destinatário da mensagem.
	 * @param subject Título da mensagem.
	 * @param content Conteúdo textual da mensagem.
	 * @throws MessageDeliveryException Falha no envio da mensagem.
	 */
	void send(String toEmail, String subject, String content) throws MessageDeliveryException;
	
	/**
	 * Faz o email de uma mensagem de email para um email de destino, permitindo
	 * informar o título e o conteúdo da mensagem.
	 * 
	 * @param fromEmail Email do remetente da mensagem.
	 * @param toEmail Email do destinatário da mensagem.
	 * @param subject Título da mensagem.
	 * @param content Conteúdo textual da mensagem.
	 * @throws MessageDeliveryException Falha no envio da mensagem.
	 */
	void send(String fromEmail, String toEmail, String subject, String content) throws MessageDeliveryException;
	
	void resetConfiguration();
}
