package tv.snews.anews.infra.mail;

import tv.snews.anews.domain.AbstractEntity;

/**
 * Armazena as configurações para o uso do SMTP para envio dos emails disparados
 * pelo sistema.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class MailConfig extends AbstractEntity<Integer> {
	
    private static final long serialVersionUID = -8468049523800810602L;

	private String email;
	private String host;
	private SSL ssl;
	private String username;
	private String password;
	private int port;
	
	//--------------------------------------------------------------------------
	//	Operations
	//--------------------------------------------------------------------------
	
	public boolean isAuthenticated() {
	    return username != null && password != null;
    }
	
	public void copy(MailConfig other) {
		this.email = other.email;
		this.host = other.host;
		this.ssl = other.ssl;
		this.username = other.username;
		this.password = other.password;
		this.port = other.port;
	}
	
	//--------------------------------------------------------------------------
	//	Getters
	//--------------------------------------------------------------------------
	
    public String getEmail() {
	    return email;
    }
    
    public void setEmail(String email) {
	    this.email = email;
    }
    
    public String getHost() {
	    return host;
    }
    
    public void setHost(String host) {
	    this.host = host;
    }
    
    public SSL getSsl() {
	    return ssl;
    }
    
    public void setSsl(SSL ssl) {
	    this.ssl = ssl;
    }
    
    public String getUsername() {
	    return username;
    }
    
    public void setUsername(String username) {
	    this.username = username;
    }
    
    public String getPassword() {
	    return password;
    }
    
    public void setPassword(String password) {
	    this.password = password;
    }
    
    public int getPort() {
	    return port;
    }
    
    public void setPort(int port) {
	    this.port = port;
    }
}
