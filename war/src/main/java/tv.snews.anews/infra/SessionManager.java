package tv.snews.anews.infra;

import tv.snews.anews.domain.User;
import tv.snews.anews.exception.SessionManagerException;

import java.util.Set;

/**
 * Interface que define os métodos para manipulação da sessão do usuário. Isso
 * ajuda a isolar a tecnologia utilizada para gerenciar a sessão.
 * 
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface SessionManager {

    public static final String SESSION_KEY = "user_id";
    public static final String OFFSET_KEY = "clientTimezoneOffset";
    public static final String CLIPBOARD_KEY = "clipboard";
    public static final String CLIPBOARD_ACTION_KEY = "clipboardAction";
     

	/**
	 * Retorna o objeto que representa o usuário que está atualmente logado, ou
	 * <code>null</code> caso não exista nenhuma sessão ativa.
	 * 
	 * @return Objeto {@link User} ou <code>null</code>.
	 * @throws tv.snews.anews.exception.SessionManagerException Exibe algum erro ao tentar acessar a
	 *             sessão.
	 */
	User getCurrentUser() throws SessionManagerException;

    Long getClientTimezoneOffset();

	/**
	 * Retorna a lista de usuários logados no sistema.
	 *
	 * @return Lista de objetos {@link User}
	 */
	Set<User> getOnlineUsers();

	/**
	 * Registra a sessão para o usuário informado.
	 *
	 * @param user O usuário que esta logando no sistema
     * @param timezoneOffset timezone offset enviado da aplicação cliente
     *               utilizado para correção de datas e tempos
	 * @return Identificador da sessão
	 * @throws tv.snews.anews.exception.SessionManagerException Erro ao acessar a sessão
	 */
	String registerUserSession(User user, Long timezoneOffset) throws SessionManagerException;

	/**
	 * Derruba o usuário da sessão.
	 *
	 * @param user O usuário a ser derrubado.
	 */
	void kickOut(User user);

	/**
	 * Destroi a sessão de um usuário que saiu do sistema.
	 *
	 * @throws tv.snews.anews.exception.SessionManagerException Erro ao acessar a sessão.
	 */
	void leave() throws SessionManagerException;
	
    void clipboardPush(ClipboardAction action, Long[] entities);
    
    void clipboardFlush();

    Long[] clipboardPull();
    
    ClipboardAction clipboardAction();
 
    boolean containsOnClipboard(Long id);
    
}
