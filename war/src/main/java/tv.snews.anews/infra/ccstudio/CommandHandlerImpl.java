package tv.snews.anews.infra.ccstudio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Story;
import tv.snews.ccstudio.CommandHandler;
import tv.snews.ccstudio.command.Error;
import tv.snews.ccstudio.command.request.PingCommand;
import tv.snews.ccstudio.command.request.RequestProgramsCommand;
import tv.snews.ccstudio.command.request.RequestRundownCommand;
import tv.snews.ccstudio.command.response.NakCommand;
import tv.snews.ccstudio.command.response.ProgramsCommand;
import tv.snews.ccstudio.command.response.ResponseCommand;
import tv.snews.ccstudio.command.response.RundownCommand;
import tv.snews.ccstudio.domain.Prog;
import tv.snews.ccstudio.domain.Register;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

/**
 * Implementação da interface {@link tv.snews.ccstudio.CommandHandler} que define as ações para
 * certos tipos de comandos recebidos do EITV CCStudio.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class CommandHandlerImpl implements CommandHandler {

	private static final Logger log = LoggerFactory.getLogger(CommandHandlerImpl.class);
	
	private ProgramDao programDao;
	private RundownDao rundownDao;

	@Override
	public Long loadRundownId(int programId, Date date) {
		Program program = programDao.get(programId);
		if (program == null) {
			log.error("Program required by CCStudio was not found.");
			return null;
		}
		
		Rundown rundown = rundownDao.findByDateAndProgram(date, program, false);
		if (rundown == null) {
			log.error("Rundown required by CCStudio was not found.");
			return null;
		}
		
		return rundown.getId();
	}
	
	@Override
	public void handleNakCommand(NakCommand nakCmd) {
		log.error("NAK received from CCStudio: {}", nakCmd.getError().toString());
		// TODO messenger!
	}

	@Override
	public void handlePingCommand(PingCommand pingCmd) {
		log.debug("Ping command received from CCStudio.");
	}

	@Override
	public ProgramsCommand handleRequestProgramsCommand(RequestProgramsCommand reqProgsCmd) {
		Collection<Prog> progs = CCStudioParser.parseProgs(programDao.listAll());
		return new ProgramsCommand(progs);
	}

	@Override
	@Transactional
	public ResponseCommand handleRequestRundownCommand(RequestRundownCommand reqRundownCmd) {
		Program program = programDao.get(reqRundownCmd.getProgramId());
		if (program == null) {
			log.error("Program required by CCStudio was not found.");
			return new NakCommand(Error.RUNDOWN_NOT_FOUND);
		}
		
		Rundown rundown = rundownDao.findByDateAndProgram(reqRundownCmd.getDate(), program, false);
		if (rundown == null) {
			log.error("Rundown required by CCStudio was not found.");
			return new NakCommand(Error.RUNDOWN_NOT_FOUND);
		}
		
		RundownCommand rundownCmd = new RundownCommand(rundown.getId(), program.getName(), rundownStart(rundown));
		
		// Transforma as Stories em Registers
		for (Block block : rundown.getBlocks()) {
//			if (!block.isStandBy()) {
				for (Story story : block.getStories()) {
					// Não vou confiar na configuração do hbm que excluí laudas da gaveta e da lixeira... vai que mexem nisso (:
					if (story.isApproved() && !story.isExcluded()) {
						Register register = CCStudioParser.parseRegister(story);
						rundownCmd.addRegister(register);
					}
				}
//			}
		}
		
		return rundownCmd;
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------
	
	/*
	 * Junta a data com a hora de início do espelho em um único objeto Date.
	 */
	private Date rundownStart(Rundown rundown) {
		Calendar date = Calendar.getInstance();
		date.setTime(rundown.getDate());
		
		Calendar time = Calendar.getInstance();
		time.setTime(rundown.getStart());
		
		date.set(Calendar.HOUR_OF_DAY, time.get(Calendar.HOUR_OF_DAY));
		date.set(Calendar.MINUTE, time.get(Calendar.MINUTE));
		date.set(Calendar.SECOND, time.get(Calendar.SECOND));
		
		return date.getTime();
	}
	
	//--------------------------------------------------------------------------
	//	Setters
	//--------------------------------------------------------------------------
	
    public void setProgramDao(ProgramDao programDao) {
	    this.programDao = programDao;
    }
    
    public void setRundownDao(RundownDao rundownDao) {
	    this.rundownDao = rundownDao;
    }
}
