package tv.snews.anews.infra.ccstudio;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.domain.*;
import tv.snews.ccstudio.domain.Prog;
import tv.snews.ccstudio.domain.Register;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Classe utilitária para converter objetos do ANEWS para objetos do CCStudio.
 *
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public final class CCStudioParser {

	private CCStudioParser() {
	}

	public static Prog parseProg(Program program) {
		return new Prog(program.getId(), program.getName());
	}

	public static Collection<Prog> parseProgs(Collection<Program> programs) {
		List<Prog> progs = new ArrayList<>();
		for (Program current : programs) {
			progs.add(new Prog(current.getId(), current.getName()));
		}
		return progs;
	}

	public static Register parseRegister(Story story) {
		StringBuilder sb = new StringBuilder();

		// Junta os textos das cabeças
		if (story.getHeadSection() != null) {
			for (StorySubSection subSection : story.getHeadSection().getSubSections()) {
				if (subSection instanceof StoryCameraText) {
					StoryCameraText text = (StoryCameraText) subSection;
					sb.append(">> ").append(text.getPresenter().getName()).append(": ");
					sb.append(text.getText()).append(" ");
				}
			}
		}

		// Adiciona o NC Vivo, se for o caso
		if (story.getNcSection() != null) {
			for (StorySubSection subSection : story.getNcSection().getSubSections()) {
				if (subSection instanceof StoryText) {
					StoryText text = (StoryText) subSection;
					if (StringUtils.isNotBlank(text.getText())) {
						sb.append(text.getText()).append(" ");
					}
				}
			}
		}

		// Adiciona também os OFFs da reportagem, se houver
		if (story.getReportage() != null) {
			Reportage reportage = story.getReportage();
			for (ReportageSection section : reportage.getSections()) {
				if (section.getType() != ReportageSectionType.ART) {
					sb.append(section.getContent()).append(" ");
				}
				if (section.getType() == ReportageSectionType.INTERVIEW) {
					sb.append(section.getObservation()).append(" ");
				}
			}
		}

		// Adiciona a nota pé, se houver
		//Nota pé
		if (story.getFooterSection() != null) {
			for (StorySubSection subSection : story.getFooterSection().getSubSections()) {
				if (subSection instanceof StoryCameraText) {
					if (StringUtils.isNotBlank(((StoryCameraText) subSection).getText())) {
						User presenter = ((StoryCameraText) subSection).getPresenter();
						sb.append(presenter.getName()).append(": ");
						sb.append(((StoryCameraText) subSection).getText()).append(" ");
					}
				}
			}
		}

		// Remove marcações de pausa, quebras de linha e comentários
		String result = sb.toString()
				.replaceAll("/", "-")             // marcações de pausa
				.replaceAll("[\\n\\t]", " ")      // quebras de linha e tabs
				.replaceAll(" +", " ")            // excesso de espaços em branco
				.replaceAll("\\[[^\\]]*\\]", "")  // comentários entre cochetes
				.replaceAll("\\{[^\\}]*\\}", ""); // comentários entre chaves

		return new Register(story.getId(), story.getPage(), story.getSlug(), result);
	}
}
