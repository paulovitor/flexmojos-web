package tv.snews.anews.infra.ccstudio;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Story;
import tv.snews.anews.event.StoryEvent;
import tv.snews.ccstudio.CCStudioInterface;
import tv.snews.ccstudio.command.request.DeleteCommand;
import tv.snews.ccstudio.command.request.InsertCommand;
import tv.snews.ccstudio.command.request.MoveCommand;
import tv.snews.ccstudio.command.request.UpdateCommand;
import tv.snews.ccstudio.domain.Register;

/**
 * Implementação de {@link org.springframework.context.ApplicationListener} que irá ouvir eventos do tipo
 * {@link tv.snews.anews.event.StoryEvent} e irá disparar as notificações necessárias para o CCStudio.
 *
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class StoryEventListener implements ApplicationListener<StoryEvent> {

	private static final Logger log = LoggerFactory.getLogger(StoryEventListener.class);

	private CCStudioInterface ccStudio;
	private StoryDao storyDao;

	@Override
	public void onApplicationEvent(StoryEvent event) {
		for (StoryEvent.Change change : event.getChanges()) {
			Story story = change.getTarget();

			switch (event.getAction()) {
				case SLUG_CHANGED:
				case VT_CHANGED:
				case UPDATED:
					if (story.isApproved()) {
						notifyReplace(story);
					}
					break;

				case ORDER_CHANGED:
					if (story.isApproved()) {
						notifyMove(story);
					}
					break;

				case APPROVED:
					notifyInsert(story);
					break;

				case ARCHIVED:
				case EXCLUDED:
					if (!story.isApproved()) break;
				case DISAPPROVED:
					notifyDelete(event.getRundown(), story);
					break;
				default:
					log.debug("Story event action ignored: " + event.getAction());
			}
		}
	}

	//------------------------------------
	//  Notifiers
	//------------------------------------

	private void notifyInsert(Story story) {
		Rundown rundown = story.getBlock().getRundown();

		Register register = CCStudioParser.parseRegister(story);
		InsertCommand insCmd = new InsertCommand(register);

		Story next = storyDao.nextApprovedStory(rundown, story);
		if (next != null) {
			insCmd.setBefore(next.getId());
		} else {
			Story previous = storyDao.previousApprovedStory(rundown, story);
			if (previous != null) {
				insCmd.setAfter(previous.getId());
			}
		}

		ccStudio.sendCommand(insCmd, rundown.getId());
	}

	private void notifyReplace(Story story) {
		Rundown rundown = story.getBlock().getRundown();

		Register register = CCStudioParser.parseRegister(story);
		UpdateCommand updateCmd = new UpdateCommand(register);

		ccStudio.sendCommand(updateCmd, rundown.getId());
	}

	private void notifyMove(Story story) {
		Rundown rundown = story.getBlock().getRundown();

		MoveCommand moveCmd = new MoveCommand(story.getId());

		Story next = storyDao.nextApprovedStory(rundown, story);
		if (next != null) {
			moveCmd.setBefore(next.getId());
		} else {
			Story previous = storyDao.previousApprovedStory(rundown, story);
			if (previous != null) {
				moveCmd.setAfter(previous.getId());
			}
		}

		ccStudio.sendCommand(moveCmd, rundown.getId());
	}

	private void notifyDelete(Rundown rundown, Story story) {
		DeleteCommand delCmd = new DeleteCommand(story.getId());
		ccStudio.sendCommand(delCmd, rundown.getId());
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

	public void setCcStudio(CCStudioInterface ccStudio) {
		this.ccStudio = ccStudio;
	}

	public void setStoryDao(StoryDao storyDao) {
		this.storyDao = storyDao;
	}
}
