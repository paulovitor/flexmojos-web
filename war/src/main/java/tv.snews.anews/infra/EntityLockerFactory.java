package tv.snews.anews.infra;

import tv.snews.anews.domain.AbstractEntity;
import tv.snews.anews.domain.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Factory responsável por gerenciar as instâncias singletons dos lockers das
 * entidades. Sendo assim, ela impede a criação de dois lockers para uma mesma
 * entidade.
 * 
 * <p>
 * Podem ser criado um {@link EntityLocker} apenas para entidades versionadas, 
 * ou seja, classes que estendem {@link tv.snews.anews.domain.AbstractVersionedEntity}.
 * </p>
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public final class EntityLockerFactory {

	private static final Map<Class<? extends AbstractEntity<?>>, EntityLocker<? extends AbstractEntity<?>>> LOCKERS = new ConcurrentHashMap<>();
	
	/*
	 * Construtor privado.
	 */
	private EntityLockerFactory() {
		// não será usado!
	}
	
	/**
	 * Cria ou obtém um {@link EntityLocker} da entidade informada. As
	 * instâncias retornadas são sempre singletons, não permitindo assim dois a
	 * criação de dois lockers para uma mesmo tipo de entidade.
	 * 
	 * @param entityClass Classe de entidade do locker desejado.
	 * @return Instância única do locker para a entidade.
	 */
	@SuppressWarnings("unchecked")
    public static <T extends AbstractEntity<?>> EntityLocker<T> getLocker(Class<T> entityClass) {
		EntityLocker<T> locker = (EntityLocker<T>) LOCKERS.get(entityClass); // not unsafe
		if (locker == null) {
			locker = new EntityLocker<T>();
			LOCKERS.put(entityClass, locker);
		}
		return locker;
	}
	
	/**
	 * Remove todos os "locks" do usuário informado para todas as entidades
	 * gerenciadas.
	 * 
	 * @param user Usuário que terá seus "locks" removidos.
	 */
	public static void releaseAllLocksOfUser(User user) {
		for (EntityLocker<? extends AbstractEntity<?>> locker : LOCKERS.values()) {
			locker.releaseLocksOfUser(user);
		}
	}
	
	/**
	 * Destroi todos os lockers criados por esta factory e retorna ao estado 
	 * inicial, ou seja, nenhum locker.
	 */
	public static void destroy() {
		for (EntityLocker<? extends AbstractEntity<?>> locker : LOCKERS.values()) {
			locker.destroy();
		}
		LOCKERS.clear();
	}
}
