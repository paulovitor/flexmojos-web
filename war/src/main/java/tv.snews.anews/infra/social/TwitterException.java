package tv.snews.anews.infra.social;


/**
 * Classe para tratar possíveis exceptions vindas da API do Twitter.
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TwitterException extends Exception {

    private static final long serialVersionUID = 8036484730765515795L;

    public TwitterException(String cause) {
		super(cause);   
	}
    
    public TwitterException(String cause, Throwable throwable) {
		super(cause, throwable);   
	}
	
}
