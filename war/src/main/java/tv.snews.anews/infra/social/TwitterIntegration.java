package tv.snews.anews.infra.social;

import tv.snews.anews.domain.TweetInfo;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.exception.ValidationException;

import java.util.SortedSet;

/**
 * Interface que estabelece um contrato para manipular dados
 * com a API do Twitter dentro do sistema.
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public interface TwitterIntegration {

	/**
	 * Retorna a lista dos 20 últimos tweets da API do Twitter.
	 * 
	 * @param twitterUser Usuário do twitter.
	 * @return Lista de tweets do usuário.
	 * @throws TwitterException Exceção da api do twitter.
	 * @throws IllegalArgumentException Caso usuário do twitter não for encontrado pelo screenName.
	 */
	SortedSet<TweetInfo> getTweetsByTwitterUser(TwitterUser twitterUser) throws IllegalArgumentException, TwitterException;
	
	/**
	 * Retorna um usuário da API do twitter.
	 *
	 * @param screenName nome do usuário no twitter. 
	 * @return 
	 * @throws IllegalArgumentException Se o screenName for nulo ou vazio.
	 * @throws TwitterException 
	 * @throws ValidationException TwitterUser não encontrado.
	 */
	TwitterUser getTwitterUser(String screenName) throws IllegalArgumentException, TwitterException, ValidationException;

	/**
	 * Retorna a lista de todos os tweets de um twitterUser.
	 * A busca retorna tweets mais recentes que o último tweet armazenado.
	 * 
     * @param twitterUser twitter user para efetuar a consulta.
     * @param lastTweetInfo último tweet armazenado na base.
     * @return
     * @throws TwitterException 
     */
    SortedSet<TweetInfo> getAllTweetFromTwitterUser(TwitterUser twitterUser, TweetInfo lastTweetInfo) throws TwitterException;
}