package tv.snews.anews.infra.social;

import org.springframework.social.ApiException;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.social.twitter.api.TimelineOperations;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.util.Assert;
import tv.snews.anews.domain.TweetInfo;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;


/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TwitterIntegrationImpl implements TwitterIntegration {

	/**
	 * Máximo de páginas dos resultados na consulta de tweets.
	 */
	public static final Integer MAX_PAGE_OF_RESULTS = 1;
	
	/**
	 * API do twitter não aceita acima de 200. 
	 * https://dev.twitter.com/docs/api/1/get/statuses/user_timeline 
	 */
	public static final Integer MAX_TWEETS_PER_RESULTS = 200;
	
	/**
	 * TweetID máximo para servir como parametro entre o tweetID MIN. 
	 * No nosso caso nunca será utilizado, por isso seu valor deve permanecer 0 
	 */
	public static final Integer MAX_TWEETID_TO_SEARCH = 0;
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private Twitter twitterSpring;
	
	@Override
	public SortedSet<TweetInfo> getTweetsByTwitterUser(TwitterUser twitterUser) throws IllegalArgumentException, TwitterException {
		Assert.notNull(twitterUser);
		
		if (twitterUser.getScreenName() == null || twitterUser.getScreenName().equals("")) {
			throw new IllegalArgumentException("twitter.msg.screenNameNotNullOrEmpty");
		}
		
		return loadTweetsInfo(twitterUser.getScreenName());
	}
	
    private SortedSet<TweetInfo> loadTweetsInfo(String screenName) throws TwitterException {
      	SortedSet<TweetInfo> returnTweets = new TreeSet<TweetInfo>();
      	
	    try {
	    	TimelineOperations operations = twitterSpring.timelineOperations();
	    	List<Tweet> tweets = operations.getUserTimeline(screenName, 80);
	    	for (Tweet tweet : tweets) {
		        returnTweets.add(copyTweetAPIToTweetInfo(tweet));
	        }
        } catch (ApiException e) {
        	throw new TwitterException(bundle.getMessage("twitter.msg.errorLoadTimelineTwitterUser") + " (" + screenName + ").", e);
        }   
	    
	    return returnTweets; 
    }

    @Override
    public SortedSet<TweetInfo> getAllTweetFromTwitterUser(TwitterUser twitterUser, TweetInfo lastTweetInfo) throws TwitterException {
    	Assert.notNull(twitterUser);
    	Assert.notNull(lastTweetInfo);
    	
		if (twitterUser.getScreenName() == null || twitterUser.getScreenName().equals("")) {
			throw new IllegalArgumentException("twitter.msg.screenNameNotNullOrEmpty");
		}
		if (lastTweetInfo == null) {
			throw new TwitterException("twitter.msg.lastTweetInfoNotNullOrEmpty");
		}
		
		SortedSet<TweetInfo> returnTweets = new TreeSet<TweetInfo>();
		try {
			TimelineOperations operations = twitterSpring.timelineOperations();
			List<Tweet> tweets = operations.getUserTimeline(twitterUser.getScreenName(), 1, 20, Long.parseLong(lastTweetInfo.getTweetId()));
			
			for (Tweet tweet : tweets) {
			    returnTweets.add(copyTweetAPIToTweetInfo(tweet));
	        }
        } catch (ApiException e) {
        	throw new TwitterException(bundle.getMessage("twitter.msg.errorLoadTimelineTwitterUser") + twitterUser.getScreenName() + ".", e);
        }

	    return returnTweets;
    }

    @Override
    public TwitterUser getTwitterUser(String screenName) throws IllegalArgumentException, ValidationException, TwitterException {
    	if (screenName == null || screenName.equals("")) {
    		throw new IllegalArgumentException("twitter.msg.screenNameNotNullOrEmpty");
		}
    	
    	TwitterProfile twitterProfile = null;
    	try {
    		twitterProfile = twitterSpring.userOperations().getUserProfile(screenName);
    	} catch (ResourceNotFoundException e) {
    		// Essa exeção ocorre quando o usuário não exite
    		throw new ValidationException(bundle.getMessage("twitter.msg.twitterUserNotFound"), e);
        } catch (ApiException e) {
        	throw new TwitterException(bundle.getMessage("twitter.msg.failedToCommunicateWithTwitter"), e);
        }
    	
    	if (twitterProfile != null) {
    		TwitterUser twitterUser = new TwitterUser();
    		twitterUser.setScreenName(twitterProfile.getScreenName());
    		twitterUser.setName(twitterProfile.getName());
    		twitterUser.setProfileImageUrl(twitterProfile.getProfileImageUrl());
    		return twitterUser;
    	}
    	
	    return null;
    }
    
    private TweetInfo copyTweetAPIToTweetInfo(Tweet tweet) {
    	TweetInfo tweetInfo = new TweetInfo();
        tweetInfo.setTweetId(String.valueOf(tweet.getId()));
        tweetInfo.setText(tweet.getText());
        tweetInfo.setCreatedAt(tweet.getCreatedAt());
        return tweetInfo;
    }
    
    //SETTERS 
	public void setTwitterSpring(Twitter twitter) {
		this.twitterSpring = twitter;
	}
}