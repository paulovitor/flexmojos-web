package tv.snews.anews.infra.rss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import tv.snews.anews.domain.RssFeed;

/**
 * Uma implementação do {@link org.springframework.integration.core.MessageSource} do Spring Integration que é
 * responsável por os objetos {@link RssFeed} de uma {@link RssFeedQueue} e
 * carregar seu conteúdo.
 *
 * <p>
 * A {@link org.springframework.messaging.Message} gerada irá conter um objeto {@link RssFetchedData} como
 * payload, que contém os dados principais que foram encontrados no arquivo do
 * feed.
 * </p>
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssReader implements MessageSource<RssFetchedData> {

	private static final Logger log = LoggerFactory.getLogger(RssReader.class);

	/**
	 * Chave para obter o ID do {@link RssFeed} a partir dos headers da
	 * {@link org.springframework.messaging.Message} que será gerada pelo método {@link #receive()}.
	 */
	public static final String FEED_KEY = "feed";
	
	private RssFeedFetcher feedFetcher;
	private RssFeedQueue feedQueue;
	
	@Override
	public Message<RssFetchedData> receive() {
		Message<RssFetchedData> result = null;
		
		RssFeed rssFeed = feedQueue.next();
		if (rssFeed == null) {
			// Já carregou todos, reinicia os status para reiniciar o 
			// carregamento do início na próxima vez que a task iniciar.
			feedQueue.resetStates();
		} else {
            try {
            	RssFetchedData fetchedData = feedFetcher.retrieveFeed(rssFeed.getUrl());
            	result = buildMessage(rssFeed, fetchedData);
            } catch (FeedRetriveException e) {
	            log.error("Failed to retrive feed content.", e);
            }
		}
		
		return result;
	}

	public Message<RssFetchedData> buildMessage(RssFeed rssFeed, RssFetchedData fetchedData) {
		return MessageBuilder.withPayload(fetchedData).setHeader(FEED_KEY, rssFeed).build();
	}
	
    /**
     * @param feedFetcher Responsável por carregar o conteúdo do feed
     */
    public void setFeedFetcher(RssFeedFetcher feedFetcher) {
	    this.feedFetcher = feedFetcher;
    }
	
    /**
     * @param feedQueue Queue de feeds do banco de dados.
     */
	public void setFeedQueue(RssFeedQueue feedQueue) {
		this.feedQueue = feedQueue;
	}
}
