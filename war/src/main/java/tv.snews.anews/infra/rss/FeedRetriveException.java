package tv.snews.anews.infra.rss;

/**
 * Exceção lançada quando ocorre uma falha ao carregar o conteúdo de um feed a
 * partir de sua URL.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FeedRetriveException extends Exception {

    private static final long serialVersionUID = 986019903401362307L;

    public FeedRetriveException(String cause) {
	    super(cause);
    }
    
    public FeedRetriveException(String cause, Throwable exception) {
    	super(cause, exception);
    }
}
