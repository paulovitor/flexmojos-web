package tv.snews.anews.infra.rss;

import org.apache.commons.lang.builder.ToStringBuilder;
import tv.snews.anews.dao.RssFeedDao;
import tv.snews.anews.domain.RssFeed;

import java.util.*;

/**
 * Controla os feeds disponíveis e uma ordem de carregamento. Irá conter uma
 * lista de objetos {@link RssFeed} que serão carregados de um datasource. Esses
 * itens irão possuir, cada um, um estado representando se é novo, aguardando
 * carregamento ou já foi carregado.
 * 
 * <p>
 * Estados:
 * <ul>
 * <li>{@code NEW}: representa um feed que acabou de ser carregado do
 * datasource. Na primeira execução todos os feeds iniciam com esse estado, após
 * isso somente feeds que forem cadastrados posteriormente.</li>
 * <li>{@code LOADED}: após ser requisitado o feed é tratado como carregado,
 * então recebe esse estado.</li>
 * <li>{@code WAINTING}: após todos os items terem sido carregados, geralmente o
 * método {@link RssFeedQueue#resetStates()} deve ser chamado para reiniciar os
 * estados. Com isso esse estado será atribuido.</li>
 * </ul>
 * </p>
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssFeedQueue {

	private final Set<RssFeedItem> feedItems = new HashSet<RssFeedItem>();

//	private RssService rssService;
	private RssFeedDao rssFeedDao;

	/**
	 * Retorna o próximo feed que está aguardando pelo carregamento. Caso todos
	 * os feeds já estiverem sido carregados, será retornado {@code null}.
	 *
	 * <p>
	 * Se desejar você pode retornar todos os feeds para o estado inicial pelo
	 * método {@link RssFeedQueue#resetStates()}.
	 * </p>
	 * 
	 * @return o próximo feed que está aguardando por carregamento ou
	 *         {@code null} caso todos já estiverem sido carregados.
	 */
	public RssFeed next() {
		// Primeiro verifica por novos feeds no banco de dados
		loadFeeds();
		
		// Procura por um feed com status NEW ou WAITING
		RssFeed result = null;
		RssFeedItem rssFeedItem = getFirstWithState(RssFeedState.NEW, RssFeedState.WAITING);
		if (rssFeedItem != null) {
			rssFeedItem.setState(RssFeedState.LOADED);
			result = rssFeedItem.getRssFeed();
		}
		
		return result;
	}
	
	/**
	 * Retorna todos os feeds para o estado de aguardo por carregamento.
	 */
	public void resetStates() {
		for (RssFeedItem item : feedItems) {
			item.setState(RssFeedState.WAITING);
		}
	}
	
	/*
	 * Faz o carregamento inicial dos feeds ou verifica a necessidade de
	 * atualizações (feeds novos ou removidos).
	 */
	protected void loadFeeds() {
		List<RssFeed> feeds = rssFeedDao.listAll();
		
		// Remove os feeds que não existem mais
		for (Iterator<RssFeedItem> it = feedItems.iterator(); it.hasNext();) {
			RssFeedItem item = it.next();
			if (!feeds.contains(item.getRssFeed())) {
				it.remove();
			}
		}
		
		// Adiciona os demais
		for (RssFeed feed : feeds) {
			addFeed(feed);
		}
	}
	
	/*
	 * Retorna o primeiro RssFeedItem encontrado com um dos states informados. A
	 * ordem dos states define a prioridade. Se nenhum tiver o state retorna
	 * null.
	 */
	private RssFeedItem getFirstWithState(RssFeedState... possibleStates) {
		for (RssFeedState state : possibleStates) {
			for (RssFeedItem feedItem : feedItems) {
				if (feedItem.getState() == state) {
					return feedItem;
				}
			}
		}
		return null;
	}
	
	protected void addFeed(RssFeed feed) {
		feedItems.add(new RssFeedItem(feed)); // coleção já evita repetidos
	}
	
	protected void addFeed(RssFeed feed, RssFeedState state) {
		feedItems.add(new RssFeedItem(feed, state)); // coleção já evita repetidos
	}
	
	//----------------------------------------
    // Getters & Setters
    //----------------------------------------
	
    /**
     * @param rssService A implementação do serviço de RSS utilizado para 
     * 		carregar os feeds disponíveis.
     */
    
    /**
     * @param feedDao the feedDao to set
     */
    public void setRssFeedDao(RssFeedDao rssFeedDao) {
	    this.rssFeedDao = rssFeedDao;
    }
	
    protected Set<RssFeedItem> getFeedItems() {
	    return feedItems;
    }
    
	//----------------------------------------
	//	Inner Enum: RssFeedState
	//----------------------------------------
	
    /**
     * Possíveis estados dos {@link RssFeed} gerenciados pela queue.
     */
	protected static enum RssFeedState {
		NEW, WAITING, LOADED;
	}
	
	//----------------------------------------
	//	Inner Class: RssFeedItem
	//----------------------------------------
	
	/**
	 * Armazena o {@link RssFeed} e o seu estado.
	 */
	protected static class RssFeedItem {
		
		private final RssFeed rssFeed;
		private RssFeedState state;
		
		public RssFeedItem(RssFeed targetRssFeed) {
			this(targetRssFeed, RssFeedState.NEW);
		}
		
		public RssFeedItem(RssFeed targetRssFeed, RssFeedState currState) {
			this.rssFeed = targetRssFeed;
			this.state = currState;
		}
		
		// Os métodos equals e hashCode irão evitar de ocorrer duplicações de feeds
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj instanceof RssFeedItem) {
				final RssFeedItem other = (RssFeedItem) obj;
				return Objects.equals(rssFeed, other.rssFeed);
			}
			return false;
		}
		
		@Override
		public int hashCode() {
		    return rssFeed.hashCode();
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
		    return new ToStringBuilder(this)
		    		.append("name", rssFeed.getName())
		    		.append("url", rssFeed.getUrl())
		    		.append("state", state)
		    		.toString();
		}
		
		//----------------------------------------
		//	Getters & Setters
		//----------------------------------------
		
		public RssFeed getRssFeed() {
			return rssFeed;
		}
		
		public RssFeedState getState() {
			return state;
		}
		
		public void setState(RssFeedState state) {
			this.state = state;
		}
	}
}
