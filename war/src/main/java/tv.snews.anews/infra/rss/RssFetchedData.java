package tv.snews.anews.infra.rss;

import tv.snews.anews.domain.RssItem;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssFetchedData {

	private String title;
	private String link;
	private String description;
	private String language;
	private Date buildDate;
	private List<RssItem> items = new ArrayList<RssItem>();
	
	public void addItem(RssItem rssItem) {
		items.add(rssItem);
	}
	
	//----------------------------------------
    //	Getters & Setters
    //----------------------------------------
	
    /**
     * @return Título contido no feed.
     */
    public String getTitle() {
    	return title;
    }
	
    /**
     * @param title Título contido no feed.
     */
    public void setTitle(String title) {
    	this.title = title == null ? "" : title.trim();
    }
	
    /**
     * @return Link contido no feed.
     */
    public String getLink() {
    	return link;
    }
	
    /**
     * @param link Link contido no feed.
     */
    public void setLink(String link) {
    	this.link = link == null ? "" : link.trim();
    }
	
    /**
     * @return Descrição contida o feed.
     */
    public String getDescription() {
    	return description;
    }
	
    /**
     * @param description Descrição contida o feed.
     */
    public void setDescription(String description) {
    	this.description = description == null ? "" : description.trim();
    }
	
    /**
     * @return Lingua do feed.
     */
    public String getLanguage() {
    	return language;
    }
	
    /**
     * @param language Lingua do feed.
     */
    public void setLanguage(String language) {
    	this.language = language == null ? "" : language.trim();
    }
	
    /**
     * @return Data da geração do arquivo do feed.
     */
    public Date getBuildDate() {
    	return buildDate;
    }
	
    /**
     * @param buildDate Data da geração do arquivo do feed.
     */
    public void setBuildDate(Date buildDate) {
    	this.buildDate = buildDate;
    }
	
    /**
     * @return Itens do feed.
     */
    public List<RssItem> getItems() {
    	return items;
    }
	
    /**
     * @param items Itens do feed.
     */
    public void setItems(List<RssItem> items) {
    	this.items = items;
    }
}
