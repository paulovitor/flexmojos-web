package tv.snews.anews.infra.rss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.RssFeedDao;
import tv.snews.anews.dao.RssItemDao;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.messenger.RssMessenger;

import java.util.Date;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssFeedMessageHandler {

	private static final Logger log = LoggerFactory.getLogger(RssFeedMessageHandler.class);
	
	private RssFeedDao rssFeedDao;
	private RssItemDao rssItemDao;
	
	private RssMessenger rssMessenger;
	
	/**
	 * A partir de um objeto {@link org.springframework.messaging.Message}, que contém um
	 * {@link RssFetchedData} como <code>payload</code>, verifica se novos itens
	 * de um feed foram obtidos e faz a persistência dos mesmos. Além disso, o
	 * valor de <code>lastBuild</code> do feed de origem é atualizado.
	 * 
	 * @param message
	 */
	@Transactional // Não da pra usar o AOP para transação nessa classe por ela ser concreta!
	public void handleMessage(Message<RssFetchedData> message) {
		RssFetchedData fetchedData = message.getPayload();
		RssFeed feed = message.getHeaders().get(RssReader.FEED_KEY, RssFeed.class);
		
//		rssFeedDao.refresh(feed);
		
		feed.setLastBuild(fetchedData.getBuildDate() == null ? new Date() : fetchedData.getBuildDate());
		rssFeedDao.update(feed);
		
		log.debug("Last build of {}: {}", feed.getName(), feed.getLastBuild());
		
		boolean newItemFetched = false;
		for (RssItem item : fetchedData.getItems()) {
			if (feed.addItem(item)) {
				rssItemDao.save(item);
				newItemFetched = true;
				
				log.debug("New item found for {}: {}", feed.getName(), item.getTitle());
			}
		}
		
		// Notificação para atualizar o cliente
		if (newItemFetched) {
			rssMessenger.notifyFeedUpdated(feed);
		}
	}
	
	//----------------------------------------
    //	Setters
    //----------------------------------------
	
    /**
     * @param rssFeedDao Implementação do RssFeedDao
     */
    public void setRssFeedDao(RssFeedDao rssFeedDao) {
	    this.rssFeedDao = rssFeedDao;
    }
    
    /**
     * @param rssItemDao Implementação do RssItemDao
     */
    public void setRssItemDao(RssItemDao rssItemDao) {
	    this.rssItemDao = rssItemDao;
    }
    
    /**
     * @param rssMessenger Implentação do mensageiro das notificações de RSS.
     */
    public void setRssMessenger(RssMessenger rssMessenger) {
	    this.rssMessenger = rssMessenger;
    }
}
