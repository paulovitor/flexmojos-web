
package tv.snews.anews.infra.rss;

/**
 * Interface que define o contrato de uma classe responsável por carregar o
 * conteúdo de um feed RSS.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssFeedFetcher {

	/**
	 * A partir da URL do feed RSS obtém o seu conteúdo e retorna na forma de um
	 * objeto {@link RssFetchedData}.
	 * 
	 * @param feedUrl
	 *            URL do feed RSS
	 * @return Objeto {@link RssFetchedData}
	 * @throws FeedRetriveException Qualquer falha ao obter o conteúdo do feed.
	 */
	RssFetchedData retrieveFeed(String feedUrl) throws FeedRetriveException;
}
