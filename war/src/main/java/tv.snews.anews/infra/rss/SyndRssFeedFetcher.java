package tv.snews.anews.infra.rss;

import com.sun.syndication.feed.synd.*;
import com.sun.syndication.fetcher.FeedFetcher;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.fetcher.FetcherListener;
import com.sun.syndication.fetcher.impl.FeedFetcherCache;
import com.sun.syndication.fetcher.impl.HashMapFeedInfoCache;
import com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.XmlReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.util.ResourceBundle;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Implementação da interface {@link RssFeedFetcher} utilizando o projeto Rome.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class SyndRssFeedFetcher implements RssFeedFetcher {

	private static final Logger log = LoggerFactory.getLogger(SyndRssFeedFetcher.class);
	private static final String CONTENTS_SEPARATOR = "<br/>";

	private final FeedFetcher feedFetcher;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	public SyndRssFeedFetcher() {
		// Encoding padrão para quando o RSS vem sem o prolog XML
		XmlReader.setDefaultEncoding("UTF-8");

		FeedFetcherCache feedInfoCache = HashMapFeedInfoCache.getInstance();
		feedFetcher = new HttpURLFeedFetcher(feedInfoCache);
	}

	@Override
	public RssFetchedData retrieveFeed(String feedUrl) throws FeedRetriveException {
		try {
			log.debug("Retrieving feeds from the URL {}...", feedUrl);

			SyndFeed syndFeed = feedFetcher.retrieveFeed(new URL(feedUrl));

			log.debug("Feed data retrieval succeed. {} items found.", syndFeed.getEntries().size());

			return parseSyndFeed(syndFeed);
		} catch (IllegalArgumentException | MalformedURLException e) {
			throw new FeedRetriveException(bundle.getMessage("rss.invalidFeed", feedUrl), e);
		} catch (IOException e) {
			// TCP error while retrieving feed
			throw new FeedRetriveException(bundle.getMessage("rss.tcpError", feedUrl), e);
		} catch (FeedException e) {
			// The feed is invalid
			throw new FeedRetriveException(bundle.getMessage("rss.invalidFeedContent", feedUrl), e);
		} catch (FetcherException e) {
			// HTTP error while retrieving feed
			throw new FeedRetriveException(bundle.getMessage("rss.httpError", feedUrl), e);
		}
	}

	//----------------------------------------
	//	Parse Methods
	//----------------------------------------

	@SuppressWarnings("unchecked")
	private RssFetchedData parseSyndFeed(SyndFeed syndFeed) throws UnsupportedEncodingException {
		log.debug("Parsing feed data...");

		RssFetchedData fetchedData = new RssFetchedData();
		fetchedData.setDescription(syndFeed.getDescription());
		fetchedData.setLanguage(syndFeed.getLanguage());
		fetchedData.setLink(syndFeed.getLink());
		fetchedData.setTitle(syndFeed.getTitle());
		fetchedData.setBuildDate(syndFeed.getPublishedDate());

		List<SyndEntry> syndEntries = syndFeed.getEntries();
		for (SyndEntry syndEntry : syndEntries) {
			fetchedData.addItem(parseSyndEntry(syndEntry));
		}

		log.debug("Feed data parsing succeed.");

		return fetchedData;
	}

	/*
	 * Converte um objeto SyndEntry para um objeto RssItem.
	 */
	private RssItem parseSyndEntry(SyndEntry syndEntry) {
		RssItem rssItem = new RssItem();

		rssItem.setTitle(correctString(syndEntry.getTitle()));
		rssItem.setDescription(correctString(syndEntry.getDescription() == null ? "" : syndEntry.getDescription().getValue()));
		rssItem.setLink(correctString(syndEntry.getLink()));
		rssItem.setPublished(syndEntry.getPublishedDate());

		pushAuthors(syndEntry, rssItem);
		pushCategories(syndEntry, rssItem);
		pushContent(syndEntry, rssItem);

		return rssItem;
	}

	/*
	 * Obtém os autores a partir da lista retornada por SyndEntry#getAuthors()
	 * ou a partir da string retornada por SyndEntry#getAuthor().
	 */
	@SuppressWarnings("unchecked")
	private void pushAuthors(SyndEntry syndEntry, RssItem rssItem) {
		StringBuilder authors = new StringBuilder();

		if (syndEntry.getAuthors() == null || syndEntry.getAuthors().isEmpty()) {
			authors.append(syndEntry.getAuthor());
		} else {
			/*
			 * NOTA: não sei se a lista de autores é composta de objetos
			 * String ou SyndPerson. A coleção não utiliza generics, a
			 * documentação do Rome não informa o tipo e nos meus testes não
			 * encontrei nenhum feed que utiliza esse recurso.
			 * 
			 * Esse é o motivo da verificação abaixo.
			 */
			Object firstItem = syndEntry.getAuthors().get(0);
			if (firstItem instanceof String) {
				log.info("HEY! syndEntry.getAuthors() retorna objetos String!");

				List<String> authorsList = (List<String>) syndEntry.getAuthors();
				for (int i = 0; i < authorsList.size(); i++) {
					if (i != 0) {
						if (i == authorsList.size() - 1) {
							authors.append(bundle.getMessage("rss.itemSeparator"));
						} else {
							authors.append(", ");
						}
					}
					authors.append(authorsList.get(i));
				}
			} else if (firstItem instanceof SyndPerson) {
				log.info("HEY! syndEntry.getAuthors() retorna objetos SyndPerson!");

				List<SyndPerson> authorsList = (List<SyndPerson>) syndEntry.getAuthors();
				for (int i = 0; i < authorsList.size(); i++) {
					if (i != 0) {
						if (i == authorsList.size() - 1) {
							authors.append(bundle.getMessage("rss.itemSeparator"));
						} else {
							authors.append(", ");
						}
					}
					authors.append(authorsList.get(i).getName());
				}
			} else {
				log.error("Tipo desconhecido retornado por syndEntry.getAuthors(): {}", firstItem.getClass());
			}
		}

		rssItem.setAuthors(authors.toString());
	}

	/*
	 * Obtém as categorias a partir da lista de objetos SyndCategory retornada
	 * pelo método SyndEntry#getCategories().
	 */
	@SuppressWarnings("unchecked")
	private void pushCategories(SyndEntry syndEntry, RssItem rssItem) {
		StringBuilder categories = new StringBuilder();

		List<SyndCategory> syndCategories = (List<SyndCategory>) syndEntry.getCategories();
		if (syndCategories != null && !syndCategories.isEmpty()) {
			for (int i = 0; i < syndCategories.size(); i++) {
				if (i != 0) {
					if (i == syndCategories.size() - 1) {
						categories.append(bundle.getMessage("rss.itemSeparator"));
					} else {
						categories.append(", ");
					}
				}
				categories.append(syndCategories.get(i).getName());
			}
		}
		rssItem.setCategories(categories.toString());
	}

	/*
	 * Obtém o conteúdo da notícia a partir do método SyndEntry#getContents().
	 * Geralmente é retornado um ou nenhum item de conteúdo. Quando mais de um
	 * item é retornado eles serão agrupados em uma única String.
	 */
	@SuppressWarnings("unchecked")
	private void pushContent(SyndEntry syndEntry, RssItem rssItem) {
		StringBuilder contents = new StringBuilder();
		List<SyndContent> syndContents = (List<SyndContent>) syndEntry.getContents();
		if (syndContents != null) {
			for (SyndContent syndContent : syndContents) {
				contents.append(syndContent.getValue()).append(CONTENTS_SEPARATOR);
			}
		}
		rssItem.setContent(contents.toString());
	}

	private String correctString(String str) {
		return str == null ? "" : str.trim();
	}

	//----------------------------------------
	//	Setters
	//----------------------------------------

	/**
	 * @param fetcherListener Listener opcional para o fetcher.
	 */
	public void setFetcherListener(FetcherListener fetcherListener) {
		if (fetcherListener != null) {
			feedFetcher.addFetcherEventListener(fetcherListener);
		}
	}

}
