package tv.snews.anews.infra.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Felipe Pinheiro
 */
public class TokenUtils {

	public static final String MAGIC_KEY = "obfuscate";

	public static String createToken(UserDetails userDetails) {
		// Expira em 8 horas
		long expires = System.currentTimeMillis() + 1000 * 60 * 60 * 8;

		return userDetails.getUsername() + ":" + expires + ":" + computeSignature(userDetails, expires);
	}

	public static String computeSignature(UserDetails userDetails, long expires) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			return new String(Hex.encode(digest.digest((userDetails.getUsername() + ":" + expires + ":" + userDetails.getPassword() + ":" + MAGIC_KEY).getBytes())));
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available");
		}
	}

	public static String getUsernameFromToken(String authToken) {
		return authToken == null ? null : authToken.split(":")[0];
	}

	public static boolean validateToken(String authToken, UserDetails userDetails) {
		String[] parts = authToken.split(":");

		if (parts.length != 3) {
			return false;
		}

		long expires = Long.parseLong(parts[1]);
		String signature = parts[2];

		return expires >= System.currentTimeMillis() && signature.equals(TokenUtils.computeSignature(userDetails, expires));
	}
}
