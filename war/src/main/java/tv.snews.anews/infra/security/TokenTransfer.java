package tv.snews.anews.infra.security;

/**
 * @author Felipe Pinheiro
 */
public class TokenTransfer {

	private final String token;

	public TokenTransfer(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}
}
