package tv.snews.anews.infra.search;

import org.hibernate.search.indexes.interceptor.EntityIndexingInterceptor;
import org.hibernate.search.indexes.interceptor.IndexingOverride;
import tv.snews.anews.domain.AgendaContact;

import static org.hibernate.search.indexes.interceptor.IndexingOverride.*;

/**
 * @author Felipe Pinheiro
 */
public class ContactIndexingInterceptor implements EntityIndexingInterceptor<AgendaContact> {

	@Override
	public IndexingOverride onAdd(AgendaContact entity) {
		return entity.isExcluded() ? SKIP : APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onUpdate(AgendaContact entity) {
		return entity.isExcluded() ? REMOVE : APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onDelete(AgendaContact entity) {
		return APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onCollectionUpdate(AgendaContact entity) {
		return onUpdate(entity);
	}
}
