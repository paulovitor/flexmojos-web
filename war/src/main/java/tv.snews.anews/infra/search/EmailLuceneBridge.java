package tv.snews.anews.infra.search;

import org.hibernate.search.bridge.StringBridge;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class EmailLuceneBridge implements StringBridge {

	public static final String SPECIAL_CHARS_REGEX = " ?([@_\\-\\.]) ?";

	@Override
	public String objectToString(Object object) {
		if (object == null) {
			return "";
		} else {
			/*
			 * Substitui os caracteres especiais do email por espaços, para indexar
			 * corretamente. Além disso concatena o resultado ao email original para
			 * que, caso o usuário busque pelo email completo, ele encontre.
			 */
			String email = (String) object;
			return email + ' ' + email.replaceAll(SPECIAL_CHARS_REGEX, " ");
		}
	}
}
