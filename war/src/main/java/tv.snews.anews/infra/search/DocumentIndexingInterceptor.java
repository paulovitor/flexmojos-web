package tv.snews.anews.infra.search;

import org.hibernate.search.indexes.interceptor.EntityIndexingInterceptor;
import org.hibernate.search.indexes.interceptor.IndexingOverride;
import tv.snews.anews.domain.AbstractRevision;
import tv.snews.anews.domain.Document;

import static org.hibernate.search.indexes.interceptor.IndexingOverride.*;

/**
 * Permite definir quando um documento deve ser indexado ou não.
 *
 * @author Felipe Pinheiro
 */
public class DocumentIndexingInterceptor implements EntityIndexingInterceptor<Document<? extends AbstractRevision<?>>> {

	@Override
	public IndexingOverride onAdd(Document<? extends AbstractRevision<?>> entity) {
		return entity.isExcluded() ? SKIP : APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onUpdate(Document<? extends AbstractRevision<?>> entity) {
		return entity.isExcluded() ? REMOVE : APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onDelete(Document<? extends AbstractRevision<?>> entity) {
		return APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onCollectionUpdate(Document<? extends AbstractRevision<?>> entity) {
		return onUpdate(entity);
	}
}
