package tv.snews.anews.infra.search;

import org.hibernate.search.indexes.interceptor.EntityIndexingInterceptor;
import org.hibernate.search.indexes.interceptor.IndexingOverride;
import tv.snews.anews.domain.Checklist;

import static org.hibernate.search.indexes.interceptor.IndexingOverride.*;

/**
 * @author Felipe Pinheiro
 */
public class ChecklistIndexingInterceptor implements EntityIndexingInterceptor<Checklist> {

	@Override
	public IndexingOverride onAdd(Checklist entity) {
		return entity.isExcluded() ? SKIP : APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onUpdate(Checklist entity) {
		return entity.isExcluded() ? REMOVE : APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onDelete(Checklist entity) {
		return APPLY_DEFAULT;
	}

	@Override
	public IndexingOverride onCollectionUpdate(Checklist entity) {
		return onUpdate(entity);
	}
}
