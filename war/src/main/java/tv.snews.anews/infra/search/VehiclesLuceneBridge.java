package tv.snews.anews.infra.search;

import org.hibernate.search.bridge.StringBridge;
import tv.snews.anews.domain.CommunicationVehicle;

import java.util.Set;

/**
 * Um {@code FieldBridge} utilizado para informar ao Hibernate Search que um 
 * objeto de um associamento deve ser indexando como uma simples {@code String} 
 * contendo todos os valores do {@link java.util.Set} de {@link CommunicationVehicle}.
 * 
 * @author Felipe Zap de Mello
 * @since 1.6
 */
public class VehiclesLuceneBridge implements StringBridge {

	public static final String NULL = "null";
	
    @SuppressWarnings("unchecked")
    @Override
    public String objectToString(Object object) {
    	if (object != null && object instanceof Set) {
    		Set<CommunicationVehicle> vehicles = (Set<CommunicationVehicle>) object;
    		StringBuilder vehiclesIndexed =  new StringBuilder();
    		for (CommunicationVehicle vehicle : vehicles) {
    			vehiclesIndexed.append(vehicle.toString());
    			vehiclesIndexed.append(" ");
    		}
    		
    		return  vehiclesIndexed.toString();
    	}
    	return NULL;
    }
}
