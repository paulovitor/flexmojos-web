package tv.snews.anews.infra.search;

import org.hibernate.search.bridge.StringBridge;
import tv.snews.anews.domain.StoryCameraText;
import tv.snews.anews.domain.StorySection;
import tv.snews.anews.domain.StorySubSection;
import tv.snews.anews.domain.StoryText;

/**
 * Implementação de {@link org.hibernate.search.bridge.StringBridge} que faz o trabalho de converter todo o
 * conteúdo textual de um {@link tv.snews.anews.domain.StorySection} para uma string única que será
 * usada na indexação do Lucene.
 * 
 * <p>Isso é necessário devido à natureza do relacionamento da lauda com suas 
 * seções, que é diferente dos outros relacionamentos de outras entidades como 
 * o da pauta para seus roteiros ou da reportagem para suas seções.</p>
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class StorySectionLuceneBridge implements StringBridge {

	private static final String BLANK = "";
	
    @Override
    public String objectToString(Object object) {
    	if (object == null) {
    		return BLANK;
    	} else {
    		final StringBuilder builder = new StringBuilder();
	    	final StorySection section = (StorySection) object;
	    	
	    	for (StorySubSection subSection : section.getSubSections()) {
	    		/*
	    		 * Está sendo indexado apenas o conteúdos dos StoryText e StoryCameraText.
	    		 */
	    		if (subSection instanceof StoryText) {
	    			builder.append(((StoryText) subSection).getText()).append(' ');
	    		}
	    		if (subSection instanceof StoryCameraText) {
	    			builder.append(((StoryCameraText) subSection).getText()).append(' ');
	    		}
	    	}
	    	
	    	return builder.toString();
    	}
    }
}
