package tv.snews.anews.infra.search;

import org.hibernate.search.bridge.StringBridge;
import tv.snews.anews.domain.Program;

/**
 * Um {@code FieldBridge} utilizado para informar ao Hibernate Search que um 
 * objeto de um associamento deve ser indexando como uma simples {@code String} 
 * indicando se o objeto Programa é nulo ou não. 
 * 
 * @author Felipe Zap de Mello
 * @since 1.6
 */
public class ProgramLuceneBridge implements StringBridge {

	/*
	FIXME Tentar trocar o uso desse bridge pela nova propriedade "indexNullAs"
	 */

	public static final String NULL = "null";

	@Override
	public String objectToString(Object object) {
		if (object instanceof Program) {
			return ((Program) object).getName();
		} else {
			return NULL;
		}
	}
}
