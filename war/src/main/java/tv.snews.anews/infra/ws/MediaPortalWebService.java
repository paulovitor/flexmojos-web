package tv.snews.anews.infra.ws;

import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.WSMediaParams;

import javax.xml.rpc.ServiceException;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;

public interface MediaPortalWebService {

    int createVideo(WSDevice device, WSMedia media) throws RemoteException, WebServiceException, ServiceException;

    int getVideoStatus(WSDevice device, int assetId) throws RemoteException, ServiceException, WebServiceException;

	String getFilename(WSDevice device, int assetId) throws ServiceException, RemoteException, WebServiceException;

    WSMedia getVideo(WSDevice device, int assetId) throws RemoteException, ServiceException, WebServiceException;


    PageResult<WSMedia> findAllByParams(WSDevice device, WSMediaParams params) throws RemoteException, ServiceException, WebServiceException, SocketTimeoutException;
}
