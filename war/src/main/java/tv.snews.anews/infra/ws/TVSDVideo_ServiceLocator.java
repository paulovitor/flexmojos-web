/**
 * TVSDVideo_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tv.snews.anews.infra.ws;

public class TVSDVideo_ServiceLocator extends org.apache.axis.client.Service implements tv.snews.anews.infra.ws.TVSDVideo_Service {

    public TVSDVideo_ServiceLocator() {
    }


    public TVSDVideo_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TVSDVideo_ServiceLocator(String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TVSDVideoPort
    private String TVSDVideoPort_address = "http://54.207.26.226:8080/mediaxpws/TVSDVideo";

    public String getTVSDVideoPortAddress() {
        return TVSDVideoPort_address;
    }

    // The WSDD service name defaults to the port name.
    private String TVSDVideoPortWSDDServiceName = "TVSDVideoPort";

    public String getTVSDVideoPortWSDDServiceName() {
        return TVSDVideoPortWSDDServiceName;
    }

    public void setTVSDVideoPortWSDDServiceName(String name) {
        TVSDVideoPortWSDDServiceName = name;
    }

    public tv.snews.anews.infra.ws.TVSDVideo_PortType getTVSDVideoPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TVSDVideoPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTVSDVideoPort(endpoint);
    }

    public tv.snews.anews.infra.ws.TVSDVideo_PortType getTVSDVideoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            tv.snews.anews.infra.ws.TVSDVideoPortBindingStub _stub = new tv.snews.anews.infra.ws.TVSDVideoPortBindingStub(portAddress, this);
            _stub.setPortName(getTVSDVideoPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTVSDVideoPortEndpointAddress(String address) {
        TVSDVideoPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (tv.snews.anews.infra.ws.TVSDVideo_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                tv.snews.anews.infra.ws.TVSDVideoPortBindingStub _stub = new tv.snews.anews.infra.ws.TVSDVideoPortBindingStub(new java.net.URL(TVSDVideoPort_address), this);
                _stub.setPortName(getTVSDVideoPortWSDDServiceName());
                return _stub;
            }
        }
        catch (Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("TVSDVideoPort".equals(inputPortName)) {
            return getTVSDVideoPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tvserradourada.webservices.mediaportal.com.br/", "TVSDVideo");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tvserradourada.webservices.mediaportal.com.br/", "TVSDVideoPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws javax.xml.rpc.ServiceException {

if ("TVSDVideoPort".equals(portName)) {
            setTVSDVideoPortEndpointAddress(address);
        }
        else
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
