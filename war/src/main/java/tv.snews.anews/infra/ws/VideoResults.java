/**
 * VideoResults.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tv.snews.anews.infra.ws;

public class VideoResults  implements java.io.Serializable {
    private int errorCode;

    private int resultCount;

    private tv.snews.anews.infra.ws.Video[] resultVideos;

    public VideoResults() {
    }

    public VideoResults(
           int errorCode,
           int resultCount,
           tv.snews.anews.infra.ws.Video[] resultVideos) {
           this.errorCode = errorCode;
           this.resultCount = resultCount;
           this.resultVideos = resultVideos;
    }


    /**
     * Gets the errorCode value for this VideoResults.
     *
     * @return errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this VideoResults.
     *
     * @param errorCode
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the resultCount value for this VideoResults.
     *
     * @return resultCount
     */
    public int getResultCount() {
        return resultCount;
    }


    /**
     * Sets the resultCount value for this VideoResults.
     *
     * @param resultCount
     */
    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }


    /**
     * Gets the resultVideos value for this VideoResults.
     *
     * @return resultVideos
     */
    public tv.snews.anews.infra.ws.Video[] getResultVideos() {
        return resultVideos;
    }


    /**
     * Sets the resultVideos value for this VideoResults.
     *
     * @param resultVideos
     */
    public void setResultVideos(tv.snews.anews.infra.ws.Video[] resultVideos) {
        this.resultVideos = resultVideos;
    }

    public tv.snews.anews.infra.ws.Video getResultVideos(int i) {
        return this.resultVideos[i];
    }

    public void setResultVideos(int i, tv.snews.anews.infra.ws.Video _value) {
        this.resultVideos[i] = _value;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof VideoResults)) return false;
        VideoResults other = (VideoResults) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            this.errorCode == other.getErrorCode() &&
            this.resultCount == other.getResultCount() &&
            ((this.resultVideos==null && other.getResultVideos()==null) ||
             (this.resultVideos!=null &&
              java.util.Arrays.equals(this.resultVideos, other.getResultVideos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getErrorCode();
        _hashCode += getResultCount();
        if (getResultVideos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResultVideos());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getResultVideos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VideoResults.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tvserradourada.webservices.mediaportal.com.br/", "videoResults"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultCount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultVideos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultVideos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://tvserradourada.webservices.mediaportal.com.br/", "video"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
