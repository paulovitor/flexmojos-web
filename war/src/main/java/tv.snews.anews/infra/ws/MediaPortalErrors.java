package tv.snews.anews.infra.ws;

import tv.snews.anews.exception.WebServiceException;

import java.util.HashMap;
import java.util.Map;

public class MediaPortalErrors {

    private static final Map<Integer, String> errors = new HashMap<>();

    static {
        setErrorsOfUser();
        setErrorsOfAssetId();
        setErrorsOfRequestId();
        setErrorsOfParams();
        setErrorsOfOperations();
        setErrorsOfStatusFluxo();
        setErrorsOfParse();
        setErrors();
        setErrorsOfVersioning();
        setErrorsOfClassifications();
    }

    private static void setErrorsOfUser() {
        errors.put(-5, "Nome de usuário inválido");
    }

    private static void setErrorsOfAssetId() {
        errors.put(-10, "AssetID inválido");
        errors.put(-12, "Nenhum vídeo associado a este AssetID");
        errors.put(-15, "Múltiplos vídeos associados a este AssetID");
    }

    private static void setErrorsOfRequestId() {
        errors.put(-30, "RequestID inválido");
        errors.put(-31, "Requisição não encontrada");
        errors.put(-35, "Fluxo inválido");
        errors.put(-36, "Fluxo não encontrado");
    }

    private static void setErrorsOfParams() {
        errors.put(-50, "Parâmetro título não pode ser vazio");
        errors.put(-51, "Parâmetro título inválido");
        errors.put(-52, "Parâmetro título inválido");
        errors.put(-53, "Parâmetro jornal inválido");
        errors.put(-54, "Parâmetro cidade inválido");
        errors.put(-55, "Parâmetro data do evento inválido");
        errors.put(-56, "Parâmetro data de edição inválido");
        errors.put(-57, "Parâmetro observação inválido");
        errors.put(-58, "Parâmetro observação inválido");
        errors.put(-59, "Parâmetro resumo inválido");
        errors.put(-60, "Parâmetro resumo inválido");
        errors.put(-61, "Parâmetro duração inválido");
        errors.put(-62, "Parâmetro repórter inválido");
        errors.put(-63, "Parâmetro cinegrafista 1 inválido");
        errors.put(-64, "Parâmetro cinegrafista 2 inválido");
        errors.put(-65, "Parâmetro cinegrafista 3 inválido");
        errors.put(-66, "Parâmetro assistente inválido");
        errors.put(-67, "Parâmetro editor inválido");
        errors.put(-68, "Parâmetro videografista inválido");
        errors.put(-69, "Parâmetro fluxo inválido");
    }

    private static void setErrorsOfOperations() {
        errors.put(-100, "Nenhum vídeo encontrado");
        errors.put(-120, "Nenhum resultado na busca de vídeo");
        errors.put(-140, "Este vídeo não possui proxy");
        errors.put(-200, "Requisição não encontrada");
    }

    private static void setErrorsOfStatusFluxo() {
        errors.put(-205, "Requisição cancelada");
        errors.put(-210, "Requisição com falha");
        errors.put(-220, "Requisição não agendada");
        errors.put(-230, "Requisição de arquivo offline");
        errors.put(-250, "Status indefinido");
        errors.put(-255, "Status indefinido");
        errors.put(-260, "Conversão com falha");
    }

    private static void setErrorsOfParse() {
        errors.put(-400, "Dado de AssetID corrompido");
        errors.put(-405, "Dado de jornal corrompido");
        errors.put(-410, "Dado de data de evento corrompido");
        errors.put(-415, "Dado de data de edição corrompido");
    }

    private static void setErrors() {
        errors.put(-500, "Erro na busca de usuário");
        errors.put(-505, "Erro no registro de usuário");
        errors.put(-506, "Erro no registro de usuário");
        errors.put(-510, "Erro na busca de AssetID");
        errors.put(-515, "Erro na busca de AssetID - dados inválidos para este MediaID");
        errors.put(-520, "Erro na contagem de assets de vídeo");
        errors.put(-550, "Erro na busca de vídeo");
        errors.put(-555, "Erro na busca de vídeo - múltiplos resultados");
        errors.put(-560, "Erro na listagem de vídeos");
        errors.put(-600, "Erro na criação de vídeo");
        errors.put(-610, "Erro na criação de asset");
        errors.put(-612, "Erro na criação de asset - AssetID inválido");
        errors.put(-615, "Erro na criação de asset - retorno inválido");
        errors.put(-620, "Erro na criação de vídeo - falha na remoção");
        errors.put(-640, "Erro na edição de vídeo");
        errors.put(-650, "Erro na edição de asset");
        errors.put(-652, "Erro na edição de asset - falha indefinida");
        errors.put(-655, "Erro na edição de asset - possível falha em permissões de usuário");
        errors.put(-680, "Erro no procedimento de apagar um vídeo");
        errors.put(-685, "Erro no procedimento de apagar um vídeo - falha");
        errors.put(-687, "Erro na remoção de asset - possível falha em permissões de usuário");
        errors.put(-690, "Erro ao apagar MediaID");
        errors.put(-700, "Erro na verificação de status de vídeo");
        errors.put(-705, "Erro na verificação de status de vídeo - retorno inválido");
        errors.put(-720, "Erro na verificação de proxy de vídeo");
        errors.put(-725, "Erro na verificação de proxy de vídeo - retorno inválido");
        errors.put(-740, "Erro na busca de localização de proxy de vídeo");
        errors.put(-745, "Erro na busca de localização de proxy de vídeo - proxy não encontrado");
        errors.put(-746, "Erro no registro de acesso a streaming no histórico");
        errors.put(-750, "Erro na busca de status do vídeo");
        errors.put(-755, "Erro na busca de status do vídeo - status indefinido");
        errors.put(-760, "Erro na busca de status do vídeo - status inválido");
        errors.put(-800, "Erro na listagem de metadados");
        errors.put(-820, "Erro na busca de palavra do dicionário controlado");
        errors.put(-825, "Erro na busca de palavra do dicionário controlado");
        errors.put(-826, "Erro na busca de palavra do dicionário controlado");
        errors.put(-840, "Erro na criação de palavra do dicionário controlado");
        errors.put(-845, "Erro na criação de palavra do dicionário controlado");
        errors.put(-846, "Erro na criação de palavra do dicionário controlado");
        errors.put(-860, "Erro na associação de palavras do dicionário controlado");
        errors.put(-880, "Erro na remoção da associação de palavras do dicionário controlado");
        errors.put(-1000, "Erro na verificação de status de requisição");
        errors.put(-1010, "Erro na busca de fluxo");
        errors.put(-1020, "Fluxo não encontrado");
        errors.put(-1100, "Erro no acionamento do fluxo");
        errors.put(-1110, "Erro no acionamento do fluxo - RequestID inválido");
        errors.put(-1115, "Erro no acionamento do fluxo - retorno inválido");
        errors.put(-1120, "Erro no procedimento de acionamento do fluxo");
        errors.put(-1200, "Erro na configuração de formatos");
    }

    private static void setErrorsOfVersioning() {
        errors.put(-2000, "Erro na verificação de existência de ficha");
        errors.put(-2005, "O vídeo já é uma subversão");
        errors.put(-2010, "Erro na clonagem de ficha");
        errors.put(-2015, "Erro na solicitação de nova versão");
        errors.put(-2020, "Erro ao salvar comentários para nova versão");
        errors.put(-2100, "Erro na busca de versão corrente");
        errors.put(-2105, "Erro na busca de versão corrente – sem resultados");
        errors.put(-2110, "Erro na busca de versão corrente");
        errors.put(-2200, "Erro na busca de subversões");
        errors.put(-2210, "Erro na listagem de subversões");
        errors.put(-2300, "Erro na busca do nome de arquivo");
        errors.put(-2305, "Nome de arquivo não encontrado");
        errors.put(-2310, "Erro na configuração de nome de arquivo de vídeo");
    }

    private static void setErrorsOfClassifications() {
        errors.put(-2400, "Erro na listagem de classificações");
        errors.put(-2410, "Erro na listagem de classificações");
    }

    public static void validateAssetId(int assetId) throws WebServiceException {
        String error = MediaPortalErrors.errors.get(assetId);
        if (error != null) {
            throw new WebServiceException(error);
        }
    }
}
