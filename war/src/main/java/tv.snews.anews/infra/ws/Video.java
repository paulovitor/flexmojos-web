/**
 * Video.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tv.snews.anews.infra.ws;

public class Video  implements java.io.Serializable {
    private int assetId;

    private String assistente;

    private String cidade;

    private String cinegrafista1;

    private String cinegrafista2;

    private String cinegrafista3;

    private String classificacao;

    private java.util.Calendar dataEdicao;

    private java.util.Calendar dataEvento;

    private String duracao;

    private String editor;

    private String observacao;

    private String reporter;

    private String resumo;

    private int status;

    private String streamingURL;

    private String titulo;

    private String videografista;

    public Video() {
    }

    public Video(
           int assetId,
           String assistente,
           String cidade,
           String cinegrafista1,
           String cinegrafista2,
           String cinegrafista3,
           String classificacao,
           java.util.Calendar dataEdicao,
           java.util.Calendar dataEvento,
           String duracao,
           String editor,
           String observacao,
           String reporter,
           String resumo,
           int status,
           String streamingURL,
           String titulo,
           String videografista) {
           this.assetId = assetId;
           this.assistente = assistente;
           this.cidade = cidade;
           this.cinegrafista1 = cinegrafista1;
           this.cinegrafista2 = cinegrafista2;
           this.cinegrafista3 = cinegrafista3;
           this.classificacao = classificacao;
           this.dataEdicao = dataEdicao;
           this.dataEvento = dataEvento;
           this.duracao = duracao;
           this.editor = editor;
           this.observacao = observacao;
           this.reporter = reporter;
           this.resumo = resumo;
           this.status = status;
           this.streamingURL = streamingURL;
           this.titulo = titulo;
           this.videografista = videografista;
    }


    /**
     * Gets the assetId value for this Video.
     *
     * @return assetId
     */
    public int getAssetId() {
        return assetId;
    }


    /**
     * Sets the assetId value for this Video.
     *
     * @param assetId
     */
    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }


    /**
     * Gets the assistente value for this Video.
     *
     * @return assistente
     */
    public String getAssistente() {
        return assistente;
    }


    /**
     * Sets the assistente value for this Video.
     *
     * @param assistente
     */
    public void setAssistente(String assistente) {
        this.assistente = assistente;
    }


    /**
     * Gets the cidade value for this Video.
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }


    /**
     * Sets the cidade value for this Video.
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }


    /**
     * Gets the cinegrafista1 value for this Video.
     *
     * @return cinegrafista1
     */
    public String getCinegrafista1() {
        return cinegrafista1;
    }


    /**
     * Sets the cinegrafista1 value for this Video.
     *
     * @param cinegrafista1
     */
    public void setCinegrafista1(String cinegrafista1) {
        this.cinegrafista1 = cinegrafista1;
    }


    /**
     * Gets the cinegrafista2 value for this Video.
     *
     * @return cinegrafista2
     */
    public String getCinegrafista2() {
        return cinegrafista2;
    }


    /**
     * Sets the cinegrafista2 value for this Video.
     *
     * @param cinegrafista2
     */
    public void setCinegrafista2(String cinegrafista2) {
        this.cinegrafista2 = cinegrafista2;
    }


    /**
     * Gets the cinegrafista3 value for this Video.
     *
     * @return cinegrafista3
     */
    public String getCinegrafista3() {
        return cinegrafista3;
    }


    /**
     * Sets the cinegrafista3 value for this Video.
     *
     * @param cinegrafista3
     */
    public void setCinegrafista3(String cinegrafista3) {
        this.cinegrafista3 = cinegrafista3;
    }


    /**
     * Gets the classificacao value for this Video.
     *
     * @return classificacao
     */
    public String getClassificacao() {
        return classificacao;
    }


    /**
     * Sets the classificacao value for this Video.
     *
     * @param classificacao
     */
    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }


    /**
     * Gets the dataEdicao value for this Video.
     *
     * @return dataEdicao
     */
    public java.util.Calendar getDataEdicao() {
        return dataEdicao;
    }


    /**
     * Sets the dataEdicao value for this Video.
     *
     * @param dataEdicao
     */
    public void setDataEdicao(java.util.Calendar dataEdicao) {
        this.dataEdicao = dataEdicao;
    }


    /**
     * Gets the dataEvento value for this Video.
     *
     * @return dataEvento
     */
    public java.util.Calendar getDataEvento() {
        return dataEvento;
    }


    /**
     * Sets the dataEvento value for this Video.
     *
     * @param dataEvento
     */
    public void setDataEvento(java.util.Calendar dataEvento) {
        this.dataEvento = dataEvento;
    }


    /**
     * Gets the duracao value for this Video.
     *
     * @return duracao
     */
    public String getDuracao() {
        return duracao;
    }


    /**
     * Sets the duracao value for this Video.
     *
     * @param duracao
     */
    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }


    /**
     * Gets the editor value for this Video.
     *
     * @return editor
     */
    public String getEditor() {
        return editor;
    }


    /**
     * Sets the editor value for this Video.
     *
     * @param editor
     */
    public void setEditor(String editor) {
        this.editor = editor;
    }


    /**
     * Gets the observacao value for this Video.
     *
     * @return observacao
     */
    public String getObservacao() {
        return observacao;
    }


    /**
     * Sets the observacao value for this Video.
     *
     * @param observacao
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }


    /**
     * Gets the reporter value for this Video.
     *
     * @return reporter
     */
    public String getReporter() {
        return reporter;
    }


    /**
     * Sets the reporter value for this Video.
     *
     * @param reporter
     */
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }


    /**
     * Gets the resumo value for this Video.
     *
     * @return resumo
     */
    public String getResumo() {
        return resumo;
    }


    /**
     * Sets the resumo value for this Video.
     *
     * @param resumo
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }


    /**
     * Gets the status value for this Video.
     *
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this Video.
     *
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * Gets the streamingURL value for this Video.
     *
     * @return streamingURL
     */
    public String getStreamingURL() {
        return streamingURL;
    }


    /**
     * Sets the streamingURL value for this Video.
     *
     * @param streamingURL
     */
    public void setStreamingURL(String streamingURL) {
        this.streamingURL = streamingURL;
    }


    /**
     * Gets the titulo value for this Video.
     *
     * @return titulo
     */
    public String getTitulo() {
        return titulo;
    }


    /**
     * Sets the titulo value for this Video.
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }


    /**
     * Gets the videografista value for this Video.
     *
     * @return videografista
     */
    public String getVideografista() {
        return videografista;
    }


    /**
     * Sets the videografista value for this Video.
     *
     * @param videografista
     */
    public void setVideografista(String videografista) {
        this.videografista = videografista;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof Video)) return false;
        Video other = (Video) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            this.assetId == other.getAssetId() &&
            ((this.assistente==null && other.getAssistente()==null) ||
             (this.assistente!=null &&
              this.assistente.equals(other.getAssistente()))) &&
            ((this.cidade==null && other.getCidade()==null) ||
             (this.cidade!=null &&
              this.cidade.equals(other.getCidade()))) &&
            ((this.cinegrafista1==null && other.getCinegrafista1()==null) ||
             (this.cinegrafista1!=null &&
              this.cinegrafista1.equals(other.getCinegrafista1()))) &&
            ((this.cinegrafista2==null && other.getCinegrafista2()==null) ||
             (this.cinegrafista2!=null &&
              this.cinegrafista2.equals(other.getCinegrafista2()))) &&
            ((this.cinegrafista3==null && other.getCinegrafista3()==null) ||
             (this.cinegrafista3!=null &&
              this.cinegrafista3.equals(other.getCinegrafista3()))) &&
            ((this.classificacao==null && other.getClassificacao()==null) ||
             (this.classificacao!=null &&
              this.classificacao.equals(other.getClassificacao()))) &&
            ((this.dataEdicao==null && other.getDataEdicao()==null) ||
             (this.dataEdicao!=null &&
              this.dataEdicao.equals(other.getDataEdicao()))) &&
            ((this.dataEvento==null && other.getDataEvento()==null) ||
             (this.dataEvento!=null &&
              this.dataEvento.equals(other.getDataEvento()))) &&
            ((this.duracao==null && other.getDuracao()==null) ||
             (this.duracao!=null &&
              this.duracao.equals(other.getDuracao()))) &&
            ((this.editor==null && other.getEditor()==null) ||
             (this.editor!=null &&
              this.editor.equals(other.getEditor()))) &&
            ((this.observacao==null && other.getObservacao()==null) ||
             (this.observacao!=null &&
              this.observacao.equals(other.getObservacao()))) &&
            ((this.reporter==null && other.getReporter()==null) ||
             (this.reporter!=null &&
              this.reporter.equals(other.getReporter()))) &&
            ((this.resumo==null && other.getResumo()==null) ||
             (this.resumo!=null &&
              this.resumo.equals(other.getResumo()))) &&
            this.status == other.getStatus() &&
            ((this.streamingURL==null && other.getStreamingURL()==null) ||
             (this.streamingURL!=null &&
              this.streamingURL.equals(other.getStreamingURL()))) &&
            ((this.titulo==null && other.getTitulo()==null) ||
             (this.titulo!=null &&
              this.titulo.equals(other.getTitulo()))) &&
            ((this.videografista==null && other.getVideografista()==null) ||
             (this.videografista!=null &&
              this.videografista.equals(other.getVideografista())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getAssetId();
        if (getAssistente() != null) {
            _hashCode += getAssistente().hashCode();
        }
        if (getCidade() != null) {
            _hashCode += getCidade().hashCode();
        }
        if (getCinegrafista1() != null) {
            _hashCode += getCinegrafista1().hashCode();
        }
        if (getCinegrafista2() != null) {
            _hashCode += getCinegrafista2().hashCode();
        }
        if (getCinegrafista3() != null) {
            _hashCode += getCinegrafista3().hashCode();
        }
        if (getClassificacao() != null) {
            _hashCode += getClassificacao().hashCode();
        }
        if (getDataEdicao() != null) {
            _hashCode += getDataEdicao().hashCode();
        }
        if (getDataEvento() != null) {
            _hashCode += getDataEvento().hashCode();
        }
        if (getDuracao() != null) {
            _hashCode += getDuracao().hashCode();
        }
        if (getEditor() != null) {
            _hashCode += getEditor().hashCode();
        }
        if (getObservacao() != null) {
            _hashCode += getObservacao().hashCode();
        }
        if (getReporter() != null) {
            _hashCode += getReporter().hashCode();
        }
        if (getResumo() != null) {
            _hashCode += getResumo().hashCode();
        }
        _hashCode += getStatus();
        if (getStreamingURL() != null) {
            _hashCode += getStreamingURL().hashCode();
        }
        if (getTitulo() != null) {
            _hashCode += getTitulo().hashCode();
        }
        if (getVideografista() != null) {
            _hashCode += getVideografista().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Video.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tvserradourada.webservices.mediaportal.com.br/", "video"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assetId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assetId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assistente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assistente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidade");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cinegrafista1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cinegrafista1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cinegrafista2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cinegrafista2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cinegrafista3");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cinegrafista3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "classificacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEdicao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEdicao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEvento");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEvento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duracao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "duracao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("editor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "editor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reporter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reporter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resumo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resumo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("streamingURL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "streamingURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videografista");
        elemField.setXmlName(new javax.xml.namespace.QName("", "videografista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
