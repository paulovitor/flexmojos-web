/**
 * TVSDVideo_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tv.snews.anews.infra.ws;

public interface TVSDVideo_PortType extends java.rmi.Remote {
    public tv.snews.anews.infra.ws.Classification[] listClassifications() throws java.rmi.RemoteException;
    public int getVideoStatus(int assetId) throws java.rmi.RemoteException;
    public int requestNewVersion(int assetId, String comments, String user) throws java.rmi.RemoteException;
    public int getRootVersion(int assetId) throws java.rmi.RemoteException;
    public int createVideo(tv.snews.anews.infra.ws.Video video, String user) throws java.rmi.RemoteException;
    public int editVideo(tv.snews.anews.infra.ws.Video video, String user) throws java.rmi.RemoteException;
    public int removeVideo(int assetId, String user) throws java.rmi.RemoteException;
    public String streamingURL(int assetId, String user) throws java.rmi.RemoteException;
    public tv.snews.anews.infra.ws.Video getVideo(int assetId, String user) throws java.rmi.RemoteException;
    public tv.snews.anews.infra.ws.VideoResults listVideos(tv.snews.anews.infra.ws.BuscaFiltros filtros, String user, int blockSize, int block) throws java.rmi.RemoteException;
    public tv.snews.anews.infra.ws.Version[] listVersions(int assetId) throws java.rmi.RemoteException;
    public String getFilename(int assetid) throws java.rmi.RemoteException;
    public boolean isRootVersion(int assetId) throws java.rmi.RemoteException;
}
