/**
 * TVSDVideo_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tv.snews.anews.infra.ws;

public interface TVSDVideo_Service extends javax.xml.rpc.Service {
    public String getTVSDVideoPortAddress();

    public tv.snews.anews.infra.ws.TVSDVideo_PortType getTVSDVideoPort() throws javax.xml.rpc.ServiceException;

    public tv.snews.anews.infra.ws.TVSDVideo_PortType getTVSDVideoPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
