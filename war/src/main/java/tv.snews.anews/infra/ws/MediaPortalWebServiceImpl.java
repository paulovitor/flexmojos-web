package tv.snews.anews.infra.ws;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.domain.WSMediaStatus;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.WSMediaParams;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.anews.util.ResourceBundle;

import javax.xml.rpc.ServiceException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.*;

public class MediaPortalWebServiceImpl implements MediaPortalWebService {

    private static final Logger log = LoggerFactory.getLogger(MediaPortalWebServiceImpl.class);
    private static final int DEFAULT_PAGE_SIZE = 50;
    private static final int TIMEOUT = 60000;
    private ResourceBundle bundle = ResourceBundle.INSTANCE;
    private TVSDVideo_Service service;
    private TVSDVideo_PortType port;

    @Override
    public int createVideo(WSDevice device, WSMedia media) throws RemoteException, WebServiceException, ServiceException {
        validateRequiredFields(device, media);
        openConnection(device);
        Video video = createVideo(media);
        int assetId = port.createVideo(video, device.getUsername());
        MediaPortalErrors.validateAssetId(assetId);
        return assetId;
    }

    @Override
    public int getVideoStatus(WSDevice device, int assetId) throws RemoteException, ServiceException, WebServiceException {
        validateRequiredFields(device, assetId);
        openConnection(device);
        return port.getVideoStatus(assetId);
    }

    @Override
    public WSMedia getVideo(WSDevice device, int assetId) throws RemoteException, ServiceException, WebServiceException {
        validateRequiredFields(device, assetId);
        openConnection(device);
        Video video = port.getVideo(assetId, device.getUsername());
        WSMedia wsMedia = null;
        try {
            MediaPortalErrors.validateAssetId(video.getAssetId());
            wsMedia = convertVideo(video);
            wsMedia.setDevice(device);
        } catch (WebServiceException wse) {
            log.debug(wse.getMessage());
        }
        return wsMedia;
    }

    @Override
    public String getFilename(WSDevice device, int assetId) throws ServiceException, RemoteException, WebServiceException {
        Objects.requireNonNull(device, bundle.getMessage("defaults.requiredFields"));

        openConnection(device);

        String fileName = port.getFilename(assetId);

        // Verifica se retornou um erro
        try {
            int asInt = Integer.parseInt(fileName);
            MediaPortalErrors.validateAssetId(asInt);
        } catch (NumberFormatException e) {
            // Não é um erro, já que não é um Inteiro
        }

        return fileName;
    }

    @Override
    public PageResult<WSMedia> findAllByParams(WSDevice device, WSMediaParams params) throws RemoteException, ServiceException, WebServiceException, SocketTimeoutException {
        validateRequiredFields(device, params);
        openConnection(device);
        List<WSMedia> videos = new ArrayList<WSMedia>();
        VideoResults result = port.listVideos(createBuscaFiltros(params), device.getUsername(), DEFAULT_PAGE_SIZE, params.getPage());
        MediaPortalErrors.validateAssetId(result.getErrorCode());
        if (result.getResultVideos() != null) {
            for (Video video : result.getResultVideos()) {
                WSMedia wsMedia = convertVideo(video);
                wsMedia.setDevice(device);
                videos.add(wsMedia);
            }
        }
        return new PageResult<>(params.getPage(), result.getResultCount(), videos);
    }

    //------------------------------------
    //  Helper Methods
    //------------------------------------

    private void validateRequiredFields(Object... objects) {
        for (Object object : objects) {
            Objects.requireNonNull(object, bundle.getMessage("defaults.requiredFields"));
        }
    }

    private void openConnection(WSDevice device) throws ServiceException {
        TVSDVideoPortBindingStub binding = (TVSDVideoPortBindingStub) getPort(device);
        binding.setUsername(device.getUsername());
        binding.setPassword(device.getPassword());
        binding.setTimeout(TIMEOUT);
    }

    private TVSDVideo_PortType getPort(WSDevice device) throws ServiceException {
        try {
            port = service.getTVSDVideoPort(new URL(device.getAddress()));
        } catch (MalformedURLException e) {
            throw new ServiceException(e);
        }
        return port;
    }

    private Video createVideo(WSMedia wsMedia) {
        Video video = new Video();
        video.setTitulo(wsMedia.getSlug());
        video.setClassificacao(wsMedia.getClassification());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(wsMedia.getEventDate());
        video.setDataEvento(calendar);

        video.setAssistente(wsMedia.getAssistant());
        video.setCidade(wsMedia.getCity());
        video.setCinegrafista1(wsMedia.getCameraman1());
        video.setCinegrafista2(wsMedia.getCameraman2());
        video.setCinegrafista3(wsMedia.getCameraman3());
        video.setDuracao(DateTimeUtil.dateToTimeFormat(wsMedia.getDurationTime()));
        video.setEditor(wsMedia.getEditor());
        video.setObservacao(wsMedia.getObservation());
        video.setReporter(wsMedia.getReporter());
        video.setResumo(wsMedia.getDescription());
        video.setVideografista(wsMedia.getVideographer());
        return video;
    }

    private WSMedia convertVideo(Video video) {
        WSMedia wsMedia = new WSMedia();
        wsMedia.setAssetId(video.getAssetId());
        wsMedia.setSlug(video.getTitulo());
        wsMedia.setClassification(video.getClassificacao());
        wsMedia.setEventDate(video.getDataEvento() != null ? video.getDataEvento().getTime() : null);

        wsMedia.setAssistant(video.getAssistente());
        wsMedia.setCity(video.getCidade());
        wsMedia.setCameraman1(video.getCinegrafista1());
        wsMedia.setCameraman2(video.getCinegrafista2());
        wsMedia.setCameraman3(video.getCinegrafista3());

        if (video.getDuracao() != null) {
            wsMedia.setDurationTime(video.getDuracao());
        }

        wsMedia.setEditor(video.getEditor());
        wsMedia.setObservation(video.getObservacao());
        wsMedia.setReporter(video.getReporter());
        wsMedia.setDescription(video.getResumo());
        wsMedia.setVideographer(video.getVideografista());
        wsMedia.setStatus(WSMediaStatus.getByCode(video.getStatus()));
        wsMedia.setStreamingURL(video.getStreamingURL());
        return wsMedia;
    }

    private BuscaFiltros createBuscaFiltros(WSMediaParams params) {
        BuscaFiltros buscaFiltros = new BuscaFiltros();
        if (StringUtils.isNotBlank(params.getTitleText())) {
            buscaFiltros.setTitulo("%" + params.getTitleText() + "%");
        }
        if (StringUtils.isNotBlank(params.getObservationText())) {
            buscaFiltros.setObservacao("%" + params.getObservationText() + "%");
        }
        if (StringUtils.isNotBlank(params.getDescriptionText())) {
            buscaFiltros.setResumo("%" + params.getDescriptionText() + "%");
        }
        if (StringUtils.isNotBlank(params.getDescriptionText())) {
            String[] descriptions = params.getDescriptionText().split(" ");
            for (int i = 0; i < descriptions.length; i++)
                descriptions[i] = "%" + descriptions[i] + "%";
            buscaFiltros.setDescricoes(descriptions);
        }

        if (StringUtils.isNotBlank(params.getClassificationText())) {
            buscaFiltros.setClassificacao(params.getClassificationText());
        }
        if (StringUtils.isNotBlank(params.getCityText())) buscaFiltros.setCidade(params.getCityText());
        if (StringUtils.isNotBlank(params.getCameramanText())) buscaFiltros.setCinegrafista(params.getCameramanText());

        if (params.getChangedDateEnd() != null) buscaFiltros.setDataEdicaoFim(getCalendar(params.getChangedDateEnd()));
        if (params.getChangedDateStart() != null) {
            buscaFiltros.setDataEdicaoInicio(getCalendar(params.getChangedDateStart()));
        }
        if (params.getEventDateEnd() != null) buscaFiltros.setDataEventoFim(getCalendar(params.getEventDateEnd()));
        if (params.getEventDateStart() != null) {
            buscaFiltros.setDataEventoInicio(getCalendar(params.getEventDateStart()));
        }

        if (StringUtils.isNotBlank(params.getEditorText())) buscaFiltros.setEditor(params.getEditorText());

        if (StringUtils.isNotBlank(params.getReporterText())) buscaFiltros.setReporter(params.getReporterText());

        if (StringUtils.isNotBlank(params.getVideographerText()))
            buscaFiltros.setVideografista(params.getVideographerText());

        return buscaFiltros;
    }

    private Calendar getCalendar(Date fieldDate) {
        Calendar calendarEditionTimeEnd = Calendar.getInstance();
        calendarEditionTimeEnd.setTime(fieldDate);
        return calendarEditionTimeEnd;
    }

    //------------------------------------
    //  Getters & Setters
    //------------------------------------

    public void setService(TVSDVideo_Service service) {
        this.service = service;
    }
}
