/**
 * BuscaFiltros.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tv.snews.anews.infra.ws;

public class BuscaFiltros  implements java.io.Serializable {
    private String assistente;

    private String cidade;

    private String cinegrafista;

    private String classificacao;

    private java.util.Calendar dataEdicaoFim;

    private java.util.Calendar dataEdicaoInicio;

    private java.util.Calendar dataEventoFim;

    private java.util.Calendar dataEventoInicio;

    private String[] descricoes;

    private String editor;

    private String observacao;

    private String reporter;

    private String resumo;

    private String titulo;

    private String videografista;

    public BuscaFiltros() {
    }

    public BuscaFiltros(
           String assistente,
           String cidade,
           String cinegrafista,
           String classificacao,
           java.util.Calendar dataEdicaoFim,
           java.util.Calendar dataEdicaoInicio,
           java.util.Calendar dataEventoFim,
           java.util.Calendar dataEventoInicio,
           String[] descricoes,
           String editor,
           String observacao,
           String reporter,
           String resumo,
           String titulo,
           String videografista) {
           this.assistente = assistente;
           this.cidade = cidade;
           this.cinegrafista = cinegrafista;
           this.classificacao = classificacao;
           this.dataEdicaoFim = dataEdicaoFim;
           this.dataEdicaoInicio = dataEdicaoInicio;
           this.dataEventoFim = dataEventoFim;
           this.dataEventoInicio = dataEventoInicio;
           this.descricoes = descricoes;
           this.editor = editor;
           this.observacao = observacao;
           this.reporter = reporter;
           this.resumo = resumo;
           this.titulo = titulo;
           this.videografista = videografista;
    }


    /**
     * Gets the assistente value for this BuscaFiltros.
     *
     * @return assistente
     */
    public String getAssistente() {
        return assistente;
    }


    /**
     * Sets the assistente value for this BuscaFiltros.
     *
     * @param assistente
     */
    public void setAssistente(String assistente) {
        this.assistente = assistente;
    }


    /**
     * Gets the cidade value for this BuscaFiltros.
     *
     * @return cidade
     */
    public String getCidade() {
        return cidade;
    }


    /**
     * Sets the cidade value for this BuscaFiltros.
     *
     * @param cidade
     */
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }


    /**
     * Gets the cinegrafista value for this BuscaFiltros.
     *
     * @return cinegrafista
     */
    public String getCinegrafista() {
        return cinegrafista;
    }


    /**
     * Sets the cinegrafista value for this BuscaFiltros.
     *
     * @param cinegrafista
     */
    public void setCinegrafista(String cinegrafista) {
        this.cinegrafista = cinegrafista;
    }


    /**
     * Gets the classificacao value for this BuscaFiltros.
     *
     * @return classificacao
     */
    public String getClassificacao() {
        return classificacao;
    }


    /**
     * Sets the classificacao value for this BuscaFiltros.
     *
     * @param classificacao
     */
    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }


    /**
     * Gets the dataEdicaoFim value for this BuscaFiltros.
     *
     * @return dataEdicaoFim
     */
    public java.util.Calendar getDataEdicaoFim() {
        return dataEdicaoFim;
    }


    /**
     * Sets the dataEdicaoFim value for this BuscaFiltros.
     *
     * @param dataEdicaoFim
     */
    public void setDataEdicaoFim(java.util.Calendar dataEdicaoFim) {
        this.dataEdicaoFim = dataEdicaoFim;
    }


    /**
     * Gets the dataEdicaoInicio value for this BuscaFiltros.
     *
     * @return dataEdicaoInicio
     */
    public java.util.Calendar getDataEdicaoInicio() {
        return dataEdicaoInicio;
    }


    /**
     * Sets the dataEdicaoInicio value for this BuscaFiltros.
     *
     * @param dataEdicaoInicio
     */
    public void setDataEdicaoInicio(java.util.Calendar dataEdicaoInicio) {
        this.dataEdicaoInicio = dataEdicaoInicio;
    }


    /**
     * Gets the dataEventoFim value for this BuscaFiltros.
     *
     * @return dataEventoFim
     */
    public java.util.Calendar getDataEventoFim() {
        return dataEventoFim;
    }


    /**
     * Sets the dataEventoFim value for this BuscaFiltros.
     *
     * @param dataEventoFim
     */
    public void setDataEventoFim(java.util.Calendar dataEventoFim) {
        this.dataEventoFim = dataEventoFim;
    }


    /**
     * Gets the dataEventoInicio value for this BuscaFiltros.
     *
     * @return dataEventoInicio
     */
    public java.util.Calendar getDataEventoInicio() {
        return dataEventoInicio;
    }


    /**
     * Sets the dataEventoInicio value for this BuscaFiltros.
     *
     * @param dataEventoInicio
     */
    public void setDataEventoInicio(java.util.Calendar dataEventoInicio) {
        this.dataEventoInicio = dataEventoInicio;
    }


    /**
     * Gets the descricoes value for this BuscaFiltros.
     *
     * @return descricoes
     */
    public String[] getDescricoes() {
        return descricoes;
    }


    /**
     * Sets the descricoes value for this BuscaFiltros.
     *
     * @param descricoes
     */
    public void setDescricoes(String[] descricoes) {
        this.descricoes = descricoes;
    }

    public String getDescricoes(int i) {
        return this.descricoes[i];
    }

    public void setDescricoes(int i, String _value) {
        this.descricoes[i] = _value;
    }


    /**
     * Gets the editor value for this BuscaFiltros.
     *
     * @return editor
     */
    public String getEditor() {
        return editor;
    }


    /**
     * Sets the editor value for this BuscaFiltros.
     *
     * @param editor
     */
    public void setEditor(String editor) {
        this.editor = editor;
    }


    /**
     * Gets the observacao value for this BuscaFiltros.
     *
     * @return observacao
     */
    public String getObservacao() {
        return observacao;
    }


    /**
     * Sets the observacao value for this BuscaFiltros.
     *
     * @param observacao
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }


    /**
     * Gets the reporter value for this BuscaFiltros.
     *
     * @return reporter
     */
    public String getReporter() {
        return reporter;
    }


    /**
     * Sets the reporter value for this BuscaFiltros.
     *
     * @param reporter
     */
    public void setReporter(String reporter) {
        this.reporter = reporter;
    }


    /**
     * Gets the resumo value for this BuscaFiltros.
     *
     * @return resumo
     */
    public String getResumo() {
        return resumo;
    }


    /**
     * Sets the resumo value for this BuscaFiltros.
     *
     * @param resumo
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }


    /**
     * Gets the titulo value for this BuscaFiltros.
     *
     * @return titulo
     */
    public String getTitulo() {
        return titulo;
    }


    /**
     * Sets the titulo value for this BuscaFiltros.
     *
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }


    /**
     * Gets the videografista value for this BuscaFiltros.
     *
     * @return videografista
     */
    public String getVideografista() {
        return videografista;
    }


    /**
     * Sets the videografista value for this BuscaFiltros.
     *
     * @param videografista
     */
    public void setVideografista(String videografista) {
        this.videografista = videografista;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof BuscaFiltros)) return false;
        BuscaFiltros other = (BuscaFiltros) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.assistente==null && other.getAssistente()==null) ||
             (this.assistente!=null &&
              this.assistente.equals(other.getAssistente()))) &&
            ((this.cidade==null && other.getCidade()==null) ||
             (this.cidade!=null &&
              this.cidade.equals(other.getCidade()))) &&
            ((this.cinegrafista==null && other.getCinegrafista()==null) ||
             (this.cinegrafista!=null &&
              this.cinegrafista.equals(other.getCinegrafista()))) &&
            ((this.classificacao==null && other.getClassificacao()==null) ||
             (this.classificacao!=null &&
              this.classificacao.equals(other.getClassificacao()))) &&
            ((this.dataEdicaoFim==null && other.getDataEdicaoFim()==null) ||
             (this.dataEdicaoFim!=null &&
              this.dataEdicaoFim.equals(other.getDataEdicaoFim()))) &&
            ((this.dataEdicaoInicio==null && other.getDataEdicaoInicio()==null) ||
             (this.dataEdicaoInicio!=null &&
              this.dataEdicaoInicio.equals(other.getDataEdicaoInicio()))) &&
            ((this.dataEventoFim==null && other.getDataEventoFim()==null) ||
             (this.dataEventoFim!=null &&
              this.dataEventoFim.equals(other.getDataEventoFim()))) &&
            ((this.dataEventoInicio==null && other.getDataEventoInicio()==null) ||
             (this.dataEventoInicio!=null &&
              this.dataEventoInicio.equals(other.getDataEventoInicio()))) &&
            ((this.descricoes==null && other.getDescricoes()==null) ||
             (this.descricoes!=null &&
              java.util.Arrays.equals(this.descricoes, other.getDescricoes()))) &&
            ((this.editor==null && other.getEditor()==null) ||
             (this.editor!=null &&
              this.editor.equals(other.getEditor()))) &&
            ((this.observacao==null && other.getObservacao()==null) ||
             (this.observacao!=null &&
              this.observacao.equals(other.getObservacao()))) &&
            ((this.reporter==null && other.getReporter()==null) ||
             (this.reporter!=null &&
              this.reporter.equals(other.getReporter()))) &&
            ((this.resumo==null && other.getResumo()==null) ||
             (this.resumo!=null &&
              this.resumo.equals(other.getResumo()))) &&
            ((this.titulo==null && other.getTitulo()==null) ||
             (this.titulo!=null &&
              this.titulo.equals(other.getTitulo()))) &&
            ((this.videografista==null && other.getVideografista()==null) ||
             (this.videografista!=null &&
              this.videografista.equals(other.getVideografista())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssistente() != null) {
            _hashCode += getAssistente().hashCode();
        }
        if (getCidade() != null) {
            _hashCode += getCidade().hashCode();
        }
        if (getCinegrafista() != null) {
            _hashCode += getCinegrafista().hashCode();
        }
        if (getClassificacao() != null) {
            _hashCode += getClassificacao().hashCode();
        }
        if (getDataEdicaoFim() != null) {
            _hashCode += getDataEdicaoFim().hashCode();
        }
        if (getDataEdicaoInicio() != null) {
            _hashCode += getDataEdicaoInicio().hashCode();
        }
        if (getDataEventoFim() != null) {
            _hashCode += getDataEventoFim().hashCode();
        }
        if (getDataEventoInicio() != null) {
            _hashCode += getDataEventoInicio().hashCode();
        }
        if (getDescricoes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDescricoes());
                 i++) {
                Object obj = java.lang.reflect.Array.get(getDescricoes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEditor() != null) {
            _hashCode += getEditor().hashCode();
        }
        if (getObservacao() != null) {
            _hashCode += getObservacao().hashCode();
        }
        if (getReporter() != null) {
            _hashCode += getReporter().hashCode();
        }
        if (getResumo() != null) {
            _hashCode += getResumo().hashCode();
        }
        if (getTitulo() != null) {
            _hashCode += getTitulo().hashCode();
        }
        if (getVideografista() != null) {
            _hashCode += getVideografista().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BuscaFiltros.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tvserradourada.webservices.mediaportal.com.br/", "buscaFiltros"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assistente");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assistente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cidade");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cidade"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cinegrafista");
        elemField.setXmlName(new javax.xml.namespace.QName("", "cinegrafista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("classificacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "classificacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEdicaoFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEdicaoFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEdicaoInicio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEdicaoInicio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEventoFim");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEventoFim"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataEventoInicio");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataEventoInicio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descricoes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "descricoes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("editor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "editor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("observacao");
        elemField.setXmlName(new javax.xml.namespace.QName("", "observacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reporter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reporter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resumo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resumo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("titulo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "titulo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videografista");
        elemField.setXmlName(new javax.xml.namespace.QName("", "videografista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
