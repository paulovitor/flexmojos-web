package tv.snews.anews.infra;

import tv.snews.anews.domain.User;

/**
 * EventListener utilizado pela classe {@link EntityLocker} para monitorar os
 * eventos de bloquear e desbloquear uma entidade para edição.
 * 
 * @param <T> tipo da entidade que o locker irá manipular
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface EntityLockerListener<T> {

	/**
	 * Handler para o evento de quando uma entidade é bloqueada para edição por
	 * um usuário.
	 * 
	 * @param entity Entidade que foi bloqueada.
	 * @param user Usuário que detem o direito de edição.
	 */
	void entityLocked(T entity, User user);
	
	/**
	 * Handler para o evento de quando uma entidade é liberada para edição.
	 * 
	 * @param entity Entidade que foi desbloqueada.
	 * @param user Usuário que detinha o direito de edição.
	 */
	void entityUnlocked(T entity, User user);
}
