package tv.snews.anews.infra;

import tv.snews.anews.domain.AbstractEntity;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Classe responsável por registrar as edições das entidades do sistema que não
 * podem sofrer edição simultânea. Toda vez que a entidade for ser editada, deve
 * ser feita por meio dessa classe a verificação se ela já não está sendo
 * editada por outro usuário. Em caso negativo ela poderá ser editada, e com
 * isso deve ser registrado o "lock" da entidade para o usuário que fará a
 * edição.
 * 
 * <p>
 * Esta classe trabalha apenas com um tipo de entidade, que é informado por meio
 * de <code>generics</code> na criação da instância. O gerênciamento de
 * múltiplos tipos de entidades é feita pela classe {@link EntityLockerFactory},
 * que utiliza esta classe como auxílio.
 * </p>
 *
 * <p>
 * Somente entidades que estendem a classe {@link tv.snews.anews.domain.AbstractVersionedEntity}, ou
 * seja, entidades versionadas.
 * </p>
 *
 * @param <T> Classe das entidades que serão gerencias pelo locker.
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class EntityLocker<T extends AbstractEntity<?>> {

	private final Map<T, User> lockedEntities = new HashMap<>();
//	private final Set<EntityLockerListener<T>> listeners = new ConcurrentHashSet<EntityLockerListener<T>>();
	private final Set<EntityLockerListener<T>> listeners = Collections.newSetFromMap(new ConcurrentHashMap<EntityLockerListener<T>, Boolean>());

	/**
	 * Registra a edição de uma entidade para um usuário. Caso a entidade já
	 * esteja sendo editada por um outro usuário, uma
	 * {@link tv.snews.anews.exception.IllegalOperationException} será lançada, mas se o usuário que
	 * estiver editando for o mesmo que pretende ser registrado a operação é
	 * ignorada, resultando em nada.
	 *
	 * @param entity Entidade que será editada
	 * @param user Usuário que efetuara a edição
	 * @throws tv.snews.anews.exception.IllegalOperationException se a entidade já estiver sendo editada
	 *             por outro usuário
	 */
	public void lock(T entity, User user) throws IllegalOperationException {
		User owner = findOwner(entity);
		if (owner == null) {
			lockedEntities.put(entity, user);
			dispatchLockEvent(entity, user);
		} else if (!owner.equals(user)) {
			throw new IllegalOperationException("The entity it is already locked by another user.");
		}
	}

	/**
	 * Remove o "lock" de uma entidade, para quando ela terminou de ser editada.
	 * No caso em que a entidade não tinha nenhum lock registrado, nada irá
	 * ocorrer.
	 *
	 * @param entity entidade que será liberada para edição
	 * @param user usuário que está solicitando a remoção do lock
	 * @throws tv.snews.anews.exception.IllegalOperationException se o usuário que solicitou a remoção da
	 *             edição não for o mesmo que criou o lock
	 */
	public void unlock(T entity, User user) throws IllegalOperationException {
		/*
		 * NOTE: Optei por não lançar exceção quando tenta dar um unlock em uma
		 * entidade que não possui lock porque acredito que não há problema
		 * nisso. Caso surge essa necessidade basta fazer como o método "lock".
		 */
		User current = lockedEntities.remove(entity);
		if (current != null) { // se não estava bloqueada irá retornar null
				dispatchUnlockEvent(entity, user);
		}
	}

	/**
	 * Verifica se uma entidade está sendo editada, retornando <code>true</code>
	 * para caso positivo.
	 *
	 * @param entity Entidade que pretende ser editada.
	 * @return <code>true</code> caso algum usuário esteja editando a entidade.
	 */
	public boolean isLocked(T entity) {
		return lockedEntities.containsKey(entity);
	}

	/**
	 * Procura pelo usuário que está editando a entidade, retornando
	 * <code>null</code> para caso não estiver sendo editada por ninguém.
	 *
	 * @param entity Entidade alvo do "lock".
	 * @return Usuário que está editando a entidade ou <code>null</code> caso
	 *         ninguém o esteja fazendo.
	 */
	public User findOwner(T entity) {
		return lockedEntities.get(entity);
	}

	/**
	 * Remove todos os locks atribuidos ao usuário informado.
	 *
	 * @param user Usuário que tera os locks removidos.
	 */
	public void releaseLocksOfUser(User user) {
		if (lockedEntities.containsValue(user)) {
			Set<T> entitiesToUnlock = new HashSet<T>();
			for (Entry<T, User> entry : lockedEntities.entrySet()) {
				if (entry.getValue().equals(user)) {
					entitiesToUnlock.add(entry.getKey());
				}
			}
			for (T entity : entitiesToUnlock) {
				lockedEntities.remove(entity);
				dispatchUnlockEvent(entity, user);
			}
		}
	}

	/**
	 * Adiciona um listener para os eventos que podem ocorrer na execução deste
	 * objeto.
	 *
	 * @param listener Listener de eventos.
	 * @see tv.snews.anews.infra.EntityLockerListener
	 */
	public void addEntityLockerListener(EntityLockerListener<T> listener) {
		listeners.add(listener);
	}

	/**
	 * Remove um listener de eventos que foi configurado para este objeto.
	 *
	 * @param listener Listener de eventos.
	 * @see tv.snews.anews.infra.EntityLockerListener
	 */
	public void removeEntityLockerListener(EntityLockerListener<T> listener) {
		listeners.remove(listener);
	}
	
	/**
	 * Remove todos os listeners de eventos atribuidos a este objeto.
	 */
	public void removeAllEntityLockerListeners() {
		listeners.clear();
	}
	
	/**
	 * Destroi este locker, ignorando todos os lockers que possivelmente possam 
	 * existir e todos os listeners criados.
	 */
	public void destroy() {
		lockedEntities.clear();
		listeners.clear();
	}
	
	//--------------------------------------------------------------------------
	//	Helper Methods
	//--------------------------------------------------------------------------
	
	/*
	 * Pecorre a lista de listeners disponíveis e dispara o evento de lock para
	 * com os valores informados.
	 */
	private void dispatchLockEvent(T entity, User user) {
		for (EntityLockerListener<T> listener : listeners) {
			listener.entityLocked(entity, user);
		}
	}
	
	/*
	 * Pecorre a lista de listeners disponíveis e dispara o evento de unlock
	 * para com os valores informados.
	 */
	private void dispatchUnlockEvent(T entity, User user) {
		for (EntityLockerListener<T> listener : listeners) {
			listener.entityUnlocked(entity, user);
		}
	}
}
