package tv.snews.anews.infra;


/**
 * 
 * @author Eliezer Reis
 * @since 1.9
 */
public enum ClipboardAction {
	COPY, CUT, PAST
}
