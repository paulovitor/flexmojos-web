package tv.snews.anews.infra.intelligentinterface;

import tv.snews.anews.domain.IIDevice;
import tv.snews.intelligentinterface.exception.TelnetException;

import java.io.IOException;
import java.util.Set;

/**
 * Define os métodos necessários para o envio de GCs.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface IntelligentInterface {

    /**
     * Faz o envio de uma lista de comandos para um dispositivo GC.
     *
     * @param device
     * @param commands
     * @throws java.io.IOException
     * @throws tv.snews.intelligentinterface.exception.TelnetException
     */
    void sendCredits(IIDevice device, Set<String> commands) throws IOException, TelnetException;
}
