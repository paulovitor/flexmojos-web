package tv.snews.anews.infra.intelligentinterface;

import tv.snews.anews.domain.ScriptCG;
import tv.snews.anews.domain.ScriptCGField;
import tv.snews.anews.domain.StoryCG;
import tv.snews.anews.domain.StoryCGField;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class TelnetCommand {

	/**
	 * Comando para criar template baseado em modelo existente.
	 **/
	public static final String WRITE_MESSAGE = "W";

	/**
	 * Comando para criar selecionar o diretório de mensagens.
	 **/
	public static final String SELECT_MESSAGE_DIRECTORY = "M";

	/**
	 * Utilizado para finalizar comandos.
	 **/
	public static final String CARRIAGE_RETURN_AND_LINE_FEED = "\\";

	/**
	 * Separador.
	 */
	public static final String SEPARATOR = "\\";

	/**
	 * Gera comando para criar um Credit.
	 * 
	 * @param storyCG
	 */
	public String getWrite(StoryCG storyCG) {
		StringBuffer writeCommand = new StringBuffer();
		writeCommand.append(WRITE_MESSAGE);
		writeCommand.append(SEPARATOR);
		writeCommand.append(storyCG.getCode());
		writeCommand.append(SEPARATOR);
		writeCommand.append(storyCG.getTemplateCode());
		writeCommand.append(SEPARATOR);
		for (StoryCGField cgField : storyCG.getFields()) {
			writeCommand.append((cgField.getValue() == null ? "" : cgField.getValue()));
			writeCommand.append(SEPARATOR);
		}
		writeCommand.append(CARRIAGE_RETURN_AND_LINE_FEED);
		return writeCommand.toString();
	}

    public String getWrite(ScriptCG scriptCG) {
        StringBuffer writeCommand = new StringBuffer();
        writeCommand.append(WRITE_MESSAGE);
        writeCommand.append(SEPARATOR);
        writeCommand.append(scriptCG.getCode());
        writeCommand.append(SEPARATOR);
        writeCommand.append(scriptCG.getTemplateCode());
        writeCommand.append(SEPARATOR);
        for (ScriptCGField cgField : scriptCG.getFields()) {
            writeCommand.append((cgField.getValue() == null ? "" : cgField.getValue()));
            writeCommand.append(SEPARATOR);
        }
        writeCommand.append(CARRIAGE_RETURN_AND_LINE_FEED);
        return writeCommand.toString();
    }

	/**
	 * Gera comando para selecionar o diretório de mensagens.
	 * 
	 * @param pathMessageDirectory
	 * @return
	 */
	public String selectMessageDirectory(String pathMessageDirectory) {
		StringBuffer writeCommand = new StringBuffer();
		writeCommand.append(SELECT_MESSAGE_DIRECTORY);
		writeCommand.append(SEPARATOR);
		writeCommand.append(pathMessageDirectory);
		writeCommand.append(SEPARATOR);
		writeCommand.append(CARRIAGE_RETURN_AND_LINE_FEED);
		return writeCommand.toString();
	}
}
