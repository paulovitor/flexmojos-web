package tv.snews.anews.infra.intelligentinterface;

import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.domain.IIDevice;
import tv.snews.intelligentinterface.Provider;
import tv.snews.intelligentinterface.TelnetClient;
import tv.snews.intelligentinterface.TelnetClientFactory;
import tv.snews.intelligentinterface.exception.TelnetException;

import java.io.IOException;
import java.util.Set;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class IntelligentInterfaceImpl implements IntelligentInterface {

    private TelnetClientFactory telnetClientFactory;

    @Override
    @Transactional
    public void sendCredits(IIDevice device, Set<String> commands) throws IOException, TelnetException {
        Provider provider = getProvider(device);
        if (provider != null) {
            TelnetClient telnetClient = telnetClientFactory.openClient(device.getHostname(), device.getPort(), provider);
            for (String command : commands) {
                telnetClient.addCommand(command);
            }
        }
    }

    private Provider getProvider(IIDevice device) {
        if (device.getVendor().toString().equals(Provider.CHYRON.toString()))
            return Provider.CHYRON;
        else if (device.getVendor().toString().equals(Provider.VIZRT.toString()))
            return Provider.VIZRT;
        return null;
    }

    //--------------------------------------------------------------------------
    //	Setters
    //--------------------------------------------------------------------------

    public void setTelnetClientFactory(TelnetClientFactory telnetClientFactory) {
        this.telnetClientFactory = telnetClientFactory;
    }

}
