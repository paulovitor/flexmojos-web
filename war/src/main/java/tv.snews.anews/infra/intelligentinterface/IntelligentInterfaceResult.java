package tv.snews.anews.infra.intelligentinterface;

/**
 * Possíveis tipos de notificações do Intelligent Interface.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.0
 */
public enum IntelligentInterfaceResult {

	SUCCESS, ERROR
}
