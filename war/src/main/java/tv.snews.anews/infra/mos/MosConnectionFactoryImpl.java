package tv.snews.anews.infra.mos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.domain.MosDevice;
import tv.snews.mos.SpecificRules;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.domain.Device;
import tv.snews.mos.message.InvalidMessageException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static tv.snews.anews.infra.mos.AnewsMosParser.asDevice;

/**
 * Fábrica de conexões MOS que além de criar as conexões também gerencia as
 * mesmas.
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class MosConnectionFactoryImpl implements MosConnectionFactory {

	private static final Logger log = LoggerFactory.getLogger(MosConnectionFactoryImpl.class);
	
	private final Map<MosDevice, MosConnection> connections = new HashMap<>();
	private final SpecificRules specificRules = new AnewsSpecificRules();

	private MosSettings mosSettings;
	
	@Override
	public MosConnection getConnection(MosDevice mosDevice) {
		MosConnection conn = connections.get(mosDevice);
		
		if (conn == null) {
			log.debug("Creating a new connection to device {}.", mosDevice);
			
			Device local = mosSettings.createLocalDevice();
			Device remote = asDevice(mosDevice);
			String host = mosDevice.getHostname();
			
			conn = new MosConnection(local, remote, host);
			conn.setSpecificRules(specificRules);

			connections.put(mosDevice, conn);
			
			try {
				conn.requestInfo();
			} catch (InvalidMessageException e) {
				log.warn("Remote device did not responded to the message reqMachInfo.", e);
			}
		}
		
		return conn;
	}
	
	@Override
	public void resetConnection(MosDevice mosDevice) {
		/*
		 * Tem que ignorar o objeto antigo completamente para caso tenha mudado 
		 * alguma porta, host ou identificador do dispositivo MOS.
		 * 
		 * A nova conexão com os dados corretos será criada quando chamar o 
		 * getConnection() com os dados novos do dispositivo.
		 */
		closeConnection(mosDevice);
	}
	
	@Override
	public void closeConnection(MosDevice mosDevice) {
		log.info("Closing connections to device {}...", mosDevice);
		
		MosConnection conn = connections.remove(mosDevice);
		if (conn != null) {
			conn.closeActiveConnections();
			log.info("Connections closed.");
		} else {
			log.info("There's no connections to close.");
		}
	}
	
	@Override
	public void close() {
		log.debug("Closing connections factory...");
		
		Iterator<MosConnection> it = connections.values().iterator();
	    while (it.hasNext()) {
	    	MosConnection conn = it.next();
	    	try {
	    		log.debug("Closing a active connection.");
	            conn.close();
            } catch (IOException e) {
	            log.warn("IOException while closing connection.", e);
            }
	    	it.remove();
	    }
	    
	    log.info("Connections factory closed.");
	}
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public void setMosSettings(MosSettings mosSettings) {
	    this.mosSettings = mosSettings;
    }
}
