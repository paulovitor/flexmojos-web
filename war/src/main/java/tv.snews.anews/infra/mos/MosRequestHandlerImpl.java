package tv.snews.anews.infra.mos;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.commons.io.ExecutorUtils;
import tv.snews.mos.MosRevision;
import tv.snews.mos.ResourceNotFoundException;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.domain.*;
import tv.snews.mos.server.handler.MosRequestHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.util.Calendar.DATE;
import static tv.snews.anews.infra.mos.AnewsMosParser.*;
import static tv.snews.anews.infra.mos.MosHelper.filterMosItems;
import static tv.snews.anews.infra.mos.RundownMosParser.*;
import static tv.snews.anews.infra.mos.ScriptPlanMosParser.scriptPlanId;

/**
 * Implementação padrão de {@link tv.snews.mos.server.handler.MosRequestHandler}.
 *
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class MosRequestHandlerImpl implements MosRequestHandler {

	private static final Logger log = LoggerFactory.getLogger(MosRequestHandlerImpl.class);
	private static final int MINIMUM_DELAY = 2000; // 4 segundos

	private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

	private DeviceInfo localDeviceInfo;

	private MosConnectionFactory mosConnFactory;

	private MosDeviceDao mosDeviceDao;
	private MosMetadataPathDao mosMetadataPathDao;
	private MosMediaDao mosMediaDao;
	private RundownDao rundownDao;
	private StoryDao storyDao;
	private ScriptPlanDao scriptPlanDao;
	private ScriptDao scriptDao;

	//------------------------------------
	//  Operations
	//------------------------------------

	public void destroy() {
		ExecutorUtils.shutdownAndAwaitTermination(executor);
	}

	//------------------------------------
	//  Profile 0
	//------------------------------------

	@Override
	public MosRevision resolveDeviceMosRevision(String deviceId) throws ResourceNotFoundException {
		MosDevice mosDevice = mosDeviceDao.findByIdentifier(deviceId);
		if (mosDevice == null) {
			throw new ResourceNotFoundException("Server not found or not registered.");
		}
		return mosDevice.getRevision();
	}

	@Override
	public DeviceInfo handleDeviceInfoRequest() {
		return localDeviceInfo;
	}

	//------------------------------------
	//  Profile 1
	//------------------------------------

	@Override
	@Transactional
	public void onActionPerformed(String mosId, MosObject fromMos, MosStatus action) {
		MosDevice mosDevice = mosDeviceDao.findByIdentifier(mosId);

		if (mosDevice == null) {
			throw new IllegalArgumentException("Unknown MOS device with ID " + mosId);
		}

		MosMedia fromDb = mosMediaDao.findByObjectId(fromMos.getId(), mosDevice);

		if (fromDb == null) {
			if (action == MosStatus.DELETED) {
				log.warn("Failed to delete mosObj because it was not found (already deleted?) [objID=" + fromMos.getId() + "]");
			} else {
				save(fromMos, mosDevice);
			}
		} else {
			if (action == MosStatus.DELETED) {
				mosMediaDao.delete(fromDb);
			} else {
				update(fromMos, fromDb);
			}
		}
	}

	private void save(MosObject fromMos, MosDevice mosDevice) {
		MosMedia toDb = asMosMedia(fromMos, mosDevice);
		mosMediaDao.save(toDb);
	}

	private void update(MosObject fromMos, MosMedia fromDb) {
        if(StringUtils.isNotEmpty(fromMos.getAbstractText())) {
            fromDb.setAbstractText(fromMos.getAbstractText());
        }

		if (fromMos.getChangeDate() != null) {
			fromDb.setChanged(fromMos.getChangeDate().toDate());
		}

		if (fromMos.getCreationDate() != null) {
			fromDb.setCreated(fromMos.getCreationDate().toDate());
		}

        if(StringUtils.isNotEmpty(fromMos.getChanger())) {
            fromDb.setChangedBy(fromMos.getChanger());
        }

        if(StringUtils.isNotEmpty(fromMos.getCreator())) {
            fromDb.setCreatedBy(fromMos.getCreator());
        }

        if(StringUtils.isNotEmpty(fromMos.getDescription())) {
            fromDb.setDescription(fromMos.getDescription());
        }

		if (fromMos.getDurationTime() != null) {
			fromDb.setDurationTime(fromMos.getDurationTime().toDateTimeToday().toDate());
		}

        if (fromMos.getDuration() != null) {
            fromDb.setDuration(fromMos.getDuration());
        }

        if (fromMos.getTimeBase() != null) {
            fromDb.setTimeBase(fromMos.getTimeBase());
        }

        if(StringUtils.isNotEmpty(fromMos.getGroup())) {
            fromDb.setGroup(fromMos.getGroup());
        }

        if(StringUtils.isNotEmpty(fromMos.getSlug())) {
            fromDb.setSlug(fromMos.getSlug());
        }

        fromDb.setReady(fromMos.getReady());
        fromDb.setRevision(fromMos.getRevision());
		fromDb.setType(fromMos.getType() == null ? "" : fromMos.getType().toString());

		MosObjectPath[] aux = {}; // informa o tipo a ser retornado ao método toArray()

		Set<MosPath> mosPaths = asMosPaths(fromMos.getPaths().toArray(aux));
        if(mosPaths.size() > 0) {
            fromDb.getPaths().retainAll(mosPaths);  // Remove os que não existem mais
            fromDb.getPaths().addAll(mosPaths);    // Adiciona os novos
        }
		Set<MosProxyPath> mosProxies = asMosProxyPaths(fromMos.getProxyPaths().toArray(aux));
        if(mosProxies.size() > 0) {
            fromDb.getProxyPaths().retainAll(mosProxies);  // Remove os que não existem mais
            fromDb.getProxyPaths().addAll(mosProxies);    // Adiciona os novos
        }

		MosMetadataPath newMetadata = asMosMetadataPath(fromMos.getMetadataPath());
		MosMetadataPath dbMetadata = fromDb.getMetadataPath();

		if (dbMetadata == null) {
			fromDb.setMetadataPath(newMetadata);
		} else if (!dbMetadata.equals(newMetadata)) {
			fromDb.setMetadataPath(newMetadata);
			mosMetadataPathDao.delete(dbMetadata); // cascade delete não funciona, apaga na mão
		}

		mosMediaDao.update(fromDb);
	}

	//------------------------------------
	//  Profile 2
	//------------------------------------

	@Override
	@Transactional
	public RunningOrder handleRunningOrderRequest(String mosId, String runningOrderId) throws ResourceNotFoundException {
		MosDevice mosDevice = mosDeviceDao.findByIdentifier(mosId);

		try {
			RunningOrder ro;

			// Verifica se está requisitando um Roteiro ou um Espelho
			if (isRundownId(runningOrderId)) {
				ro = asRunningOrder(loadRundown(runningOrderId), mosDevice);
			} else {
				ro = ScriptPlanMosParser.asRunningOrder(loadScriptPlan(runningOrderId), mosDevice);
			}

			// Retira os MosItems que não devem ser enviados
			sendStoryBodies(mosId, filterMosItems(mosDevice, ro));

			return ro;
		} catch (NumberFormatException e) {
			log.warn("Invalid rundown ID: " + runningOrderId, e);
			return null;
		}
	}

	private void sendStoryBodies(String mosId, final RunningOrder ro) {
		MosDevice mosDevice = mosDeviceDao.findByIdentifier(mosId);

		if (mosDevice == null) {
			log.error("Could not find a MOS device with mosID " + mosId);
		} else if (mosDevice.isProfileFourSupported()) {
			final MosConnection conn = mosConnFactory.getConnection(mosDevice);

			// Não é preciso filtrar os MosItems para quem suporta o profile 4 ou 5

			/*
			 * Envia as mensagens após um delay para evitar que as mensagens 
			 * roStorySend cheguem antes da mensagem roList.
			 */
			executor.schedule(new Runnable() {
				@Override
				public void run() {
					for (MosStory story : ro.getStories()) {
						conn.sendStoryBody(ro.getId(), story);
					}
				}
			}, MINIMUM_DELAY + ro.getStories().size() * 50, TimeUnit.MILLISECONDS);
		}
	}

	@Override
	@Transactional
	public void onStatusChanged(String runningOrderId, MosStatus status) throws ResourceNotFoundException {
		// TODO Adicionar um notificador da mudança de status caso for exibir na interface

		try {
			if (isRundownId(runningOrderId)) {
				Rundown rundown = loadRundown(runningOrderId);
				rundown.setMosStatus(status.toString());
				rundownDao.update(rundown);
			} else {
				ScriptPlan plan = loadScriptPlan(runningOrderId);
				plan.setMosStatus(status.toString());
				scriptPlanDao.update(plan);
			}
		} catch (NumberFormatException e) {
			log.error("Invalid rundown/scriptPlan ID: " + runningOrderId, e);
		}
	}

	@Override
	@Transactional
	public void onStatusChanged(String runningOrderId, String storyId, MosStatus status) throws ResourceNotFoundException {
		// TODO Adicionar um notificador da mudança de status caso for exibir na interface

		try {
			Long id = new Long(storyId);

			if (isRundownId(runningOrderId)) {
				Story story = storyDao.get(id);
				story.setMosStatus(status.toString());
				storyDao.update(story);
			} else {
				Script script = scriptDao.get(id);
				script.setMosStatus(status.toString());
				scriptDao.update(script);
			}
		} catch (NumberFormatException e) {
			log.error("Invalid story/script ID: " + storyId, e);
		}
	}

	@Override
	@Transactional
	public void onStatusChanged(String runningOrderId, String storyId, String itemId, MosStatus status) throws ResourceNotFoundException {
		// TODO Adicionar um notificador da mudança de status caso for exibir na interface

		try {
			boolean isCredit = isCreditId(itemId);

			if (isRundownId(runningOrderId)) {
				Story story = storyDao.get(new Long(storyId));
				if (isCredit) {
					updateStoryCreditStatus(story, creditId(itemId), status);
				} else {
					updateStoryVideoStatus(story, videoId(itemId), status);
				}
			} else {
				Script script = scriptDao.get(new Long(storyId));
				if (isCredit) {
					updateScriptCreditStatus(script, creditId(itemId), status);
				} else {
					updateScriptVideoStatus(script, videoId(itemId), status);
				}
			}
		} catch (NumberFormatException e) {
			log.error("Invalid story/script ID: " + storyId, e);
		}
	}

	private void updateStoryCreditStatus(Story story, Long creditId, MosStatus status) {
		for (StoryMosCredit credit : story.subSectionsOfType(StoryMosCredit.class)) {
			if (credit.getId().equals(creditId)) {
				credit.setMosStatus(status.toString());
				storyDao.update(credit);
				break;
			}
		}
	}

	private void updateStoryVideoStatus(Story story, Long videoId, MosStatus status) {
		for (StoryMosVideo video : story.subSectionsOfType(StoryMosVideo.class)) {
			if (video.getId().equals(videoId)) {
				video.setMosStatus(status.toString());
				storyDao.update(video);
				break;
			}
		}
	}

	private void updateScriptCreditStatus(Script script, Long creditId, MosStatus status) {
		for (AbstractScriptItem item : script.getCgs()) {
			if (item instanceof ScriptMosCredit && item.getId().equals(creditId)) {
				ScriptMosCredit credit = (ScriptMosCredit) item;
				credit.setMosStatus(status.toString());
				scriptDao.update(credit);
				break;
			}
		}
	}

	private void updateScriptVideoStatus(Script script, Long videoId, MosStatus status) {
		for (ScriptVideo video : script.getVideos()) {
			if (video.getId().equals(videoId)) {
				video.setMosStatus(status.toString());
				scriptDao.update(video);
				break;
			}
		}
	}

	//------------------------------------
	//  Profile 3
	//------------------------------------

	@Override
	@Transactional
	public void handleItemReplace(String runningOrderId, String storyId, MosStoryItem storyItem) throws ResourceNotFoundException {
		// TODO Atualizar os dados o item no espelho/roteiro
		throw new NotImplementedException();
	}

	//------------------------------------
	//  Profile 4
	//------------------------------------

	@Override
	@Transactional
	public List<RunningOrderMetadata> handleAllRunningOrdersRequest() {
		Calendar cal = Calendar.getInstance();
		cal.set(DATE, cal.get(DATE) - 5);

		List<Rundown> rundowns = rundownDao.findAllByDateGreaterOrEqualsTo(cal.getTime());
		List<ScriptPlan> plans = scriptPlanDao.findAllByDateGreaterOrEqualsTo(cal.getTime());

		List<RunningOrderMetadata> rosMetadata = new ArrayList<>();

		for (Rundown rundown : rundowns) {
			rosMetadata.add(asRunningOrderMetadata(rundown));
		}

		for (ScriptPlan plan : plans) {
			rosMetadata.add(ScriptPlanMosParser.asRunningOrderMetadata(plan));
		}

		return rosMetadata;
	}

	//------------------------------------
	//  Profile 7
	//------------------------------------

	@Override
	@Transactional
	public boolean handleStoryActionRequest(MosStory story, NcsOperation action) {
		// Não está suportando o profile 7
		return false;
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private Rundown loadRundown(String runningOrderId) {
		return rundownDao.get(rundownId(runningOrderId));
	}

	private ScriptPlan loadScriptPlan(String runningOrderId) {
		return scriptPlanDao.get(scriptPlanId(runningOrderId));
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

	public void setMosConnFactory(MosConnectionFactory mosConnFactory) {
		this.mosConnFactory = mosConnFactory;
	}

	public void setLocalDeviceInfo(DeviceInfo localDeviceInfo) {
		this.localDeviceInfo = localDeviceInfo;
	}

	public void setMosDeviceDao(MosDeviceDao mosDeviceDao) {
		this.mosDeviceDao = mosDeviceDao;
	}

	public void setMosMetadataPathDao(MosMetadataPathDao mosMetadataPathDao) {
		this.mosMetadataPathDao = mosMetadataPathDao;
	}

	public void setMosMediaDao(MosMediaDao mosMediaDao) {
		this.mosMediaDao = mosMediaDao;
	}

	public void setRundownDao(RundownDao rundownDao) {
		this.rundownDao = rundownDao;
	}

	public void setStoryDao(StoryDao storyDao) {
		this.storyDao = storyDao;
	}

	public void setScriptPlanDao(ScriptPlanDao scriptPlanDao) {
		this.scriptPlanDao = scriptPlanDao;
	}

	public void setScriptDao(ScriptDao scriptDao) {
		this.scriptDao = scriptDao;
	}
}
