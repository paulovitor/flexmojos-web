package tv.snews.anews.infra.mos;

import org.joda.time.DateTime;
import tv.snews.anews.util.VersionProperties;
import tv.snews.mos.MosRevision;
import tv.snews.mos.Profile;
import tv.snews.mos.domain.Device;
import tv.snews.mos.domain.DeviceInfo;
import tv.snews.mos.domain.DeviceType;

/**
 * 
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class MosSettings {

	public Device createLocalDevice() {
		return new Device(
			DeviceType.NCS, "anews.snews.tv", MosRevision.V_284, 
			Profile.ZERO, Profile.ONE, Profile.TWO, Profile.THREE, Profile.FOUR
		);
	}
	
	public DeviceInfo createLocalDeviceInfo() {
		DeviceInfo deviceInfo = new DeviceInfo(
			DeviceType.NCS, MosRevision.V_284, 
			Profile.ZERO, Profile.ONE, Profile.TWO, Profile.THREE, Profile.FOUR
		);
		
		deviceInfo.setManufacturer("SNEWS");
		deviceInfo.setSoftwareRevision(VersionProperties.loadVersion());
		deviceInfo.setMachineId("anews.snews.tv");
		deviceInfo.setTime(DateTime.now());
		
		return deviceInfo;
	}
}
