package tv.snews.anews.infra.mos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.dao.MosDeviceDao;
import tv.snews.anews.dao.ScriptDao;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.event.ScriptEvent;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.domain.MosStory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static tv.snews.anews.infra.mos.MosHelper.filterMosItems;
import static tv.snews.anews.infra.mos.ScriptPlanMosParser.asMosStory;
import static tv.snews.anews.infra.mos.ScriptPlanMosParser.runningOrderId;

/**
 * @author Felipe Pinheiro
 */
public class ScriptEventListener implements ApplicationListener<ScriptEvent> {

    private static final Logger log = LoggerFactory.getLogger(StoryEventListener.class);

    private MosConnectionFactory mosConnFactory;
    private MosDeviceDao mosDeviceDao;
    private ScriptDao scriptDao;

    @Override
    public void onApplicationEvent(ScriptEvent event) {
        ScriptPlan scriptPlan = event.getScriptPlan();

        try {
            Collection<Script> scripts = event.getScripts();

            switch (event.getAction()) {
                case BLOCK_CHANGED:
                case PAGE_CHANGED:
                case SLUG_CHANGED:
                case VT_CHANGED:
                case UPDATED:
                    for (Script script : onlyApproved(scripts)) {
                        notifyReplace(scriptPlan, script);
                    }
                    break;

                case ORDER_CHANGED:
                    notifyMove(scriptPlan, onlyApproved(scripts));
                    break;

                case APPROVED:
                    for (Script script : event.getScripts()) {
                        notifyInsert(scriptPlan, script);
                    }
                    break;

                case ARCHIVED:
                case EXCLUDED:
                    scripts = onlyApproved(scripts);
                case DISAPPROVED:
                    notifyDelete(scriptPlan, scripts);
                    break;

                default:
                    log.debug("Script event action ignored: " + event.getAction());
            }
        } catch (Exception e) {
        /*
         * Qualquer exceção que possa vir a ocorrer na comunicação com os
         * dispositivos MOS não deve interromper a execução do fluxo do sistema.
         */
            log.error("Failed to communicate with the MOS device.", e);
        }
    }

    //------------------------------------
    //  Notifier Methods
    //------------------------------------

    private void notifyInsert(ScriptPlan scriptPlan, Script script) {
        String roId = mosRoIdOf(scriptPlan);
        String nextId = nextMosScriptId(script);

        for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
            MosConnection conn = mosConnFactory.getConnection(mosDevice);

            MosStory mosStory = asMosStory(script, mosDevice.isTpMode());
            MosStory filtered = filterMosItems(mosDevice, mosStory);

            conn.notifyStoriesInsert(roId, nextId, filtered);
        }
    }

    private void notifyReplace(ScriptPlan scriptPlan, Script script) {
        String roId = mosRoIdOf(scriptPlan);
        String scriptId = mosScriptIdOf(script);

        for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
            MosConnection conn = mosConnFactory.getConnection(mosDevice);

            MosStory mosStory = asMosStory(script, mosDevice.isTpMode());
            MosStory filtered = filterMosItems(mosDevice, mosStory);

            conn.notifyStoriesReplace(roId, scriptId, filtered);
        }
    }

    private void notifyMove(ScriptPlan scriptPlan, Collection<Script> scripts) {
        if (!scripts.isEmpty()) {
            String roId = mosRoIdOf(scriptPlan);

            List<String> aux = new ArrayList<>();
            for (Script script : scripts) {
                aux.add(mosScriptIdOf(script));
            }

            String[] scriptIds = aux.toArray(new String[aux.size()]);
            String nextScriptId = nextMosScriptId(scripts);

            for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
                MosConnection conn = mosConnFactory.getConnection(mosDevice);
                conn.notifyStoryMove(roId, nextScriptId, scriptIds);
            }
        }
    }

    private void notifyDelete(ScriptPlan scriptPlan, Collection<Script> scripts) {
        if (!scripts.isEmpty()) {
            String roId = mosRoIdOf(scriptPlan);

            List<String> aux = new ArrayList<>();
            for (Script script : scripts) {
                aux.add(mosScriptIdOf(script));
            }

            String[] scriptIds = aux.toArray(new String[aux.size()]);

            for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
                MosConnection conn = mosConnFactory.getConnection(mosDevice);
                conn.notifyStoryDelete(roId, scriptIds);
            }
        }
    }

    //------------------------------
    //	Helpers
    //------------------------------
    private String mosRoIdOf(ScriptPlan scriptPlan) {
        return runningOrderId(scriptPlan);
    }

    private String mosScriptIdOf(Script script) {
        return script.getId().toString();
    }

    private String nextMosScriptId(Script script) {
        ScriptPlan plan = script.getBlock().getScriptPlan();

        Script next = plan.nextApprovedScript(script);
        return next == null ? null : next.getId().toString();
    }

    private String nextMosScriptId(Collection<Script> scripts) {
        ScriptPlan scriptPlan = null;
        Script next = null;

        for (Script script : scripts) {
            if (scriptPlan == null) {
                scriptPlan = script.getBlock().getScriptPlan();
            }

            next = scriptDao.nextApprovedScript(scriptPlan, script);
            if (next == null || !scripts.contains(next)) {
                break;
            }
        }

        return next == null ? null : next.getId().toString();
    }

    private Collection<Script> onlyApproved(Collection<Script> scripts) {
        // Cria uma cópia pois a coleção do evento não permite alterações
        scripts = new ArrayList<>(scripts);

        Iterator<Script> iterator = scripts.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().isApproved()) {
                iterator.remove();
            }
        }

        return scripts;
    }

    //------------------------------------
    //  Getters & Setters
    //------------------------------------

    public void setMosConnFactory(MosConnectionFactory mosConnFactory) {
        this.mosConnFactory = mosConnFactory;
    }

    public void setMosDeviceDao(MosDeviceDao mosDeviceDao) {
        this.mosDeviceDao = mosDeviceDao;
    }

    public void setScriptDao(ScriptDao scriptDao) {
        this.scriptDao = scriptDao;
    }
}
