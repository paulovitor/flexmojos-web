package tv.snews.anews.infra.mos;

import tv.snews.mos.SpecificRules;
import tv.snews.mos.domain.Device;

/**
 * @author Felipe Pinheiro
 * @since 1.6.8
 */
public class AnewsSpecificRules implements SpecificRules {

	@Override
	public boolean needStoryText(Device mosDevice) {
		return !mosDevice.getId().toLowerCase().contains("tpnews");
	}
}
