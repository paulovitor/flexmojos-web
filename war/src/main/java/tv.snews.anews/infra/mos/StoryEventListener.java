package tv.snews.anews.infra.mos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.dao.MosDeviceDao;
import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Story;
import tv.snews.anews.event.StoryEvent;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.domain.MosStory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static tv.snews.anews.infra.mos.MosHelper.filterMosItems;
import static tv.snews.anews.infra.mos.RundownMosParser.asMosStory;
import static tv.snews.anews.infra.mos.RundownMosParser.runningOrderId;

/**
 * Implementação de {@link org.springframework.context.ApplicationListener} que monitora eventos do tipo
 * {@link tv.snews.anews.event.StoryEvent} para que as mundanças feitas nas laudas sejam
 * notificadas para os dispositivos MOS.
 *
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class StoryEventListener implements ApplicationListener<StoryEvent> {

	private static final Logger log = LoggerFactory.getLogger(StoryEventListener.class);

	private MosConnectionFactory mosConnFactory;
	private MosDeviceDao mosDeviceDao;
	private StoryDao storyDao;

	@Override
	public void onApplicationEvent(StoryEvent event) {
		Rundown rundown = event.getRundown();

		try {
			Collection<Story> stories = event.getStories();

			switch (event.getAction()) {
				case BLOCK_CHANGED:
				case PAGE_CHANGED:
				case SLUG_CHANGED:
				case VT_CHANGED:
				case UPDATED:
					for (Story story : onlyApproved(stories)) {
						notifyReplace(rundown, story);
					}
					break;

				case ORDER_CHANGED:
					notifyMove(rundown, onlyApproved(stories));
					break;

				case APPROVED:
					for (Story story : event.getStories()) {
						notifyInsert(rundown, story);
					}
					break;

				case ARCHIVED:
				case EXCLUDED:
					stories = onlyApproved(stories);
				case DISAPPROVED:
					notifyDelete(rundown, stories);
					break;

				default:
					log.debug("Story event action ignored: " + event.getAction());
			}
		} catch (Exception e) {
			/*
			 * Qualquer exceção que possa vir a ocorrer na comunicação com os
			 * dispositivos MOS não deve interromper a execução do fluxo do sistema.
			 */
			log.error("Failed to communicate with the MOS device.", e);
		}
	}

	//------------------------------------
	//  Notifier Methods
	//------------------------------------

	private void notifyInsert(Rundown rundown, Story story) {
		String roId = mosRoIdOf(rundown);
		String nextId = nextMosStoryId(story);

		for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
			MosConnection conn = mosConnFactory.getConnection(mosDevice);

			MosStory mosStory = asMosStory(story, mosDevice.isTpMode());
			MosStory filtered = filterMosItems(mosDevice, mosStory);

			conn.notifyStoriesInsert(roId, nextId, filtered);
		}
	}

	private void notifyReplace(Rundown rundown, Story story) {
		String roId = mosRoIdOf(rundown);
		String storyId = mosStoryIdOf(story);

		for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
			MosConnection conn = mosConnFactory.getConnection(mosDevice);

			MosStory mosStory = asMosStory(story, mosDevice.isTpMode());
			MosStory filtered = filterMosItems(mosDevice, mosStory);

			conn.notifyStoriesReplace(roId, storyId, filtered);
		}
	}

	private void notifyMove(Rundown rundown, Collection<Story> stories) {
		if (!stories.isEmpty()) {
			String roId = mosRoIdOf(rundown);

			List<String> aux = new ArrayList<>();
			for (Story story : stories) {
				aux.add(mosStoryIdOf(story));
			}

			String[] storyIds = aux.toArray(new String[aux.size()]);
			String nextStoryId = nextMosStoryId(stories);

			for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
				MosConnection conn = mosConnFactory.getConnection(mosDevice);
				conn.notifyStoryMove(roId, nextStoryId, storyIds);
			}
		}
	}

	private void notifyDelete(Rundown rundown, Collection<Story> stories) {
		if (!stories.isEmpty()) {
			String roId = mosRoIdOf(rundown);

			List<String> aux = new ArrayList<>();
			for (Story story : stories) {
				aux.add(mosStoryIdOf(story));
			}

			String[] storyIds = aux.toArray(new String[aux.size()]);

			for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
				MosConnection conn = mosConnFactory.getConnection(mosDevice);
				conn.notifyStoryDelete(roId, storyIds);
			}
		}
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private String mosRoIdOf(Rundown rundown) {
		return runningOrderId(rundown);
	}

	private String mosStoryIdOf(Story story) {
		return story.getId().toString();
	}

	private String nextMosStoryId(Story story) {
		Rundown rundown = story.getBlock().getRundown();

		Story next = storyDao.nextApprovedStory(rundown, story);
		return next == null ? null : next.getId().toString();
	}

	private String nextMosStoryId(Collection<Story> stories) {
		Rundown rundown = null;
		Story next = null;

		for (Story story : stories) {
			if (rundown == null) {
				rundown = story.getBlock().getRundown();
			}

			next = storyDao.nextApprovedStory(rundown, story);
			if (next == null || !stories.contains(next)) {
				break;
			}
		}

		return next == null ? null : next.getId().toString();
	}

	private Collection<Story> onlyApproved(Collection<Story> stories) {
		// Cria uma cópia pois a coleção do evento não permite alterações
		stories = new ArrayList<>(stories);

		Iterator<Story> iterator = stories.iterator();
		while (iterator.hasNext()) {
			if (!iterator.next().isApproved()) {
				iterator.remove();
			}
		}

		return stories;
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

	public void setMosConnFactory(MosConnectionFactory mosConnFactory) {
		this.mosConnFactory = mosConnFactory;
	}

	public void setMosDeviceDao(MosDeviceDao mosDeviceDao) {
		this.mosDeviceDao = mosDeviceDao;
	}

	public void setStoryDao(StoryDao storyDao) {
		this.storyDao = storyDao;
	}
}
