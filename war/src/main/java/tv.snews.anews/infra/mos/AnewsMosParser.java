package tv.snews.anews.infra.mos;

import tv.snews.anews.domain.*;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.mos.Profile;
import tv.snews.mos.domain.Device;
import tv.snews.mos.domain.DeviceType;
import tv.snews.mos.domain.*;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

/**
 * Classe utilitária para fazer conversões entre os objetos do ANews e do MOS.
 *
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class AnewsMosParser {

	static final String RUNDOWN_PREFIX = "R_";
	static final String SCRIPT_PLAN_PREFIX = "S_";
	static final String CREDIT_PREFIX = "C_";
	static final String VIDEO_PREFIX = "V_";

	static final SimpleDateFormat timeFormatter = new SimpleDateFormat("mm:ss");

	//------------------------------------
	//  
	//	General Domain Objects
	//
	//------------------------------------

	/**
	 * Converte um objeto {@link tv.snews.anews.domain.MosDevice} do Anews para um objeto
	 * {@link tv.snews.mos.domain.Device} do MOS.
	 *
	 * @param mosDevice objeto com os dados armazenados pelo Anews
	 * @return o objeto correspondente do MOS
	 */
	public static Device asDevice(MosDevice mosDevice) {
		Set<Profile> profiles = new HashSet<>();
		profiles.add(Profile.ZERO);

		if (mosDevice.isProfileOneSupported()) {
			profiles.add(Profile.ONE);
		}
		if (mosDevice.isProfileTwoSupported()) {
			profiles.add(Profile.TWO);
		}
		if (mosDevice.isProfileThreeSupported()) {
			profiles.add(Profile.THREE);
		}
		if (mosDevice.isProfileFourSupported()) {
			profiles.add(Profile.FOUR);
		}
		if (mosDevice.isProfileFiveSupported()) {
			profiles.add(Profile.FIVE);
		}
		if (mosDevice.isProfileSixSupported()) {
			profiles.add(Profile.SIX);
		}
		if (mosDevice.isProfileSevenSupported()) {
			profiles.add(Profile.SEVEN);
		}

		return new Device(
				DeviceType.MOS, mosDevice.getMosId(),
				mosDevice.getRevision(),
				mosDevice.getLowerPort(), mosDevice.getUpperPort(), mosDevice.getQueryPort(),
				profiles.toArray(new Profile[]{})
		);
	}

	//------------------------------------
	//
	//  MosObject Related Objects
	//
	//------------------------------------

	/**
	 * Converte uma instância da classe {@link tv.snews.mos.domain.MosObject} do MOS para o objeto
	 * correspondente da classe {@link tv.snews.anews.domain.MosMedia} do Anews.
	 *
	 * @param mosObject dados enviados pelo dispositivo MOS
	 * @param mosDevice device do Anews que corresponde ao dispositivo MOS
	 * @return a representação correspondente da classe {@link tv.snews.anews.domain.MosMedia}
	 */
	public static MosMedia asMosMedia(MosObject mosObject, MosDevice mosDevice) {
		MosMedia mosMedia = new MosMedia();
		mosMedia.setAbstractText(mosObject.getAbstractText());
		mosMedia.setChanged(DateTimeUtil.dateTimeToDate(mosObject.getChangeDate()));
		mosMedia.setChangedBy(mosObject.getChanger());
		mosMedia.setCreated(DateTimeUtil.dateTimeToDate(mosObject.getCreationDate()));
		mosMedia.setCreatedBy(mosObject.getCreator());
		mosMedia.setDescription(mosObject.getDescription());

		if (mosObject.getDurationTime() != null) {
			mosMedia.setDurationTime(DateTimeUtil.localTimeToDate(mosObject.getDurationTime()));
			mosMedia.setDuration(mosObject.getDuration());
			mosMedia.setTimeBase(mosObject.getTimeBase());
		}

		mosMedia.setGroup(mosObject.getGroup());
		mosMedia.setDevice(mosDevice);
		mosMedia.setObjectId(mosObject.getId());
		mosMedia.setReady(mosObject.getReady());
		mosMedia.setRevision(mosObject.getRevision());
		mosMedia.setSlug(mosObject.getSlug());
		mosMedia.setType(mosObject.getType() == null ? "" : mosObject.getType().toString());

		for (MosObjectPath path : mosObject.getPaths()) {
			mosMedia.addPath(asMosPath(path));
		}

		for (MosObjectPath path : mosObject.getProxyPaths()) {
			mosMedia.addProxyPath(asMosProxyPath(path));
		}

		MosObjectPath path = mosObject.getMetadataPath();
		if (path != null) {
			mosMedia.setMetadataPath(asMosMetadataPath(path));
		}

		return mosMedia;
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.MosMedia} do Anews para o objeto
	 * correspondente da classe {@link tv.snews.mos.domain.MosObjectMetadata} do MOS.
	 *
	 * @param mosMedia modelo que representa o Objeto de Mídia.
	 * @return a representação correspondente da classe {@link tv.snews.mos.domain.MosObjectMetadata}
	 */
	public static MosObjectMetadata asMosObjectMetadata(MosMedia mosMedia) {
		MosObjectMetadata mosObjectMetadata = new MosObjectMetadata();
		mosObjectMetadata.setAbstractText(mosMedia.getAbstractText());
		mosObjectMetadata.setChangeDate(DateTimeUtil.dateToDateTime(mosMedia.getChanged()));
		mosObjectMetadata.setChanger(mosMedia.getChangedBy());
		mosObjectMetadata.setCreator(mosMedia.getCreatedBy());
		mosObjectMetadata.setDescription(mosMedia.getDescription());
		mosObjectMetadata.setDuration(mosMedia.getDuration());
		mosObjectMetadata.setTimeBase(mosMedia.getTimeBase());
		mosObjectMetadata.setGroup(mosMedia.getGroup());
		mosObjectMetadata.setType(MosObjectType.valueOf(mosMedia.getType() == null ? MosObjectType.VIDEO.toString() : mosMedia.getType()));
		mosObjectMetadata.setSlug(mosMedia.getSlug());

		return mosObjectMetadata;
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.MosPath} do Anews para o objeto
	 * correspondente da classe {@link tv.snews.mos.domain.MosObjectPath} do MOS.
	 *
	 * @param mosPath objeto de mídia.
	 */
	public static MosObjectPath asMosObjectPath(MosPath mosPath) {
		return new MosObjectPath(mosPath.getTechDescription(), mosPath.getUrl());
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.MosPath} do Anews para o objeto
	 * correspondente da classe {@link tv.snews.mos.domain.MosObjectPath} do MOS.
	 *
	 * @param mosProxyPath objeto de mídia.
	 */
	public static MosObjectPath asMosObjectPath(MosProxyPath mosProxyPath) {
		return new MosObjectPath(mosProxyPath.getTechDescription(), mosProxyPath.getUrl());
	}

	/**
	 * Converte uma instância da classe {@link MosMetadataPath} do Anews para o objeto
	 * correspondente da classe {@link tv.snews.mos.domain.MosObjectPath} do MOS.
	 *
	 * @param mosMetadataPath objeto de mídia.
	 */
	public static MosObjectPath asMosObjectPath(MosMetadataPath mosMetadataPath) {
		return new MosObjectPath(mosMetadataPath.getTechDescription(), mosMetadataPath.getUrl());
	}

	/**
	 * Converte um objeto {@link tv.snews.mos.domain.MosObjectPath} para um objeto {@link tv.snews.anews.domain.MosPath}.
	 *
	 * @param path instância de {@link tv.snews.mos.domain.MosObjectPath}
	 * @return a representação correspondente de {@link tv.snews.anews.domain.MosPath}
	 */
	public static MosPath asMosPath(MosObjectPath path) {
		if (path == null) {
			return null;
		}
		return new MosPath(path.getTechDescription(), path.getPath());
	}

	/**
	 * Converte um conjunto de objetos {@link tv.snews.mos.domain.MosObjectPath} em um {@link java.util.Set}
	 * de objetos {@link tv.snews.anews.domain.MosPath}.
	 *
	 * @param paths um ou mais paths do dispositivo MOS
	 * @return os objetos convertidos
	 */
	public static Set<MosPath> asMosPaths(MosObjectPath... paths) {
		Set<MosPath> mosPaths = new HashSet<>();
		for (MosObjectPath path : paths) {
			mosPaths.add(asMosPath(path));
		}
		return mosPaths;
	}

	/**
	 * Converte um objeto {@link tv.snews.mos.domain.MosObjectPath} para um objeto
	 * {@link tv.snews.anews.domain.MosProxyPath}.
	 *
	 * @param path instância de {@link tv.snews.mos.domain.MosObjectPath}
	 * @return a representação correspondente de {@link tv.snews.anews.domain.MosProxyPath}
	 */
	public static MosProxyPath asMosProxyPath(MosObjectPath path) {
		if (path == null) {
			return null;
		}
		return new MosProxyPath(path.getTechDescription(), path.getPath());
	}

	/**
	 * Converte um conjunto de objetos {@link tv.snews.mos.domain.MosObjectPath} em um {@link java.util.Set}
	 * de objetos {@link tv.snews.anews.domain.MosPath}.
	 *
	 * @param proxyPaths um ou mais paths do dispositivo MOS
	 * @return os objetos convertidos
	 */
	public static Set<MosProxyPath> asMosProxyPaths(MosObjectPath... proxyPaths) {
		Set<MosProxyPath> mosProxyPaths = new HashSet<>();
		for (MosObjectPath path : proxyPaths) {
			mosProxyPaths.add(asMosProxyPath(path));
		}
		return mosProxyPaths;
	}

	/**
	 * Converte um objeto {@link tv.snews.mos.domain.MosObjectPath} para um objeto
	 * {@link MosMetadataPath}.
	 *
	 * @param path instância de {@link tv.snews.mos.domain.MosObjectPath}
	 * @return a representação correspondente de {@link MosMetadataPath}
	 */
	public static MosMetadataPath asMosMetadataPath(MosObjectPath path) {
		if (path == null) {
			return null;
		}
		return new MosMetadataPath(path.getTechDescription(), path.getPath());
	}

	public static boolean isCreditId(String mosStoryItemId) {
		return mosStoryItemId.startsWith(CREDIT_PREFIX);
	}

	public static Long creditId(String mosStoryItemId) {
		return new Long(mosStoryItemId.replaceFirst(CREDIT_PREFIX, ""));
	}

	public static Long videoId(String mosStoryItemId) {
		return new Long(mosStoryItemId.replaceFirst(VIDEO_PREFIX, ""));
	}
}
