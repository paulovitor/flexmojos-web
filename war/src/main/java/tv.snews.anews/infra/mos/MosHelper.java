package tv.snews.anews.infra.mos;

import tv.snews.anews.domain.MosDevice;
import tv.snews.mos.domain.MosStory;
import tv.snews.mos.domain.RunningOrder;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public final class MosHelper {

	private MosHelper() {}

	public static RunningOrder filterMosItems(MosDevice device, RunningOrder ro) {
		if (filterIsNeeded(device)) {
			RunningOrder copy = new RunningOrder(ro);
			copy.filterByMosId(device.getMosId());
			return copy;
		} else {
			return ro;
		}
	}

	public static MosStory filterMosItems(MosDevice device, MosStory story) {
		if (filterIsNeeded(device)) {
			MosStory copy = new MosStory(story);
			copy.filterByMosId(device.getMosId());
			return copy;
		} else {
			return story;
		}
	}

	private static boolean filterIsNeeded(MosDevice device) {
		return !device.isProfileFourSupported() && !device.isProfileFiveSupported();
	}
}
