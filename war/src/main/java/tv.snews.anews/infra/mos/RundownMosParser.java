package tv.snews.anews.infra.mos;

import nu.xom.ParsingException;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import tv.snews.anews.domain.*;
import tv.snews.mos.domain.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isNotBlank;
import static tv.snews.anews.infra.mos.AnewsMosParser.*;
import static tv.snews.anews.infra.mos.MosHelper.filterMosItems;

/**
 * Faz a conversão entre objetos do espelho para objetos da API MOS.
 *
 * @author Felipe Pinheiro
 */
public class RundownMosParser {

	public static String runningOrderId(Rundown rundown) {
		return RUNDOWN_PREFIX + rundown.getId().toString();
	}

	public static String mosStoryItemId(StoryMosObject storyMosObj) {
		if (storyMosObj instanceof StoryMosCredit) {
			return CREDIT_PREFIX + storyMosObj.getId().toString();
		}
		if (storyMosObj instanceof StoryMosVideo) {
			return VIDEO_PREFIX + storyMosObj.getId().toString();
		}
		throw new IllegalArgumentException("There is no implementation to parse an ID from this type. " + storyMosObj);
	}

	public static boolean isRundownId(String runningOrderId) {
		// Se não tiver o SCRIPT_PLAN_PREFIX e nem o RUNDOWN_PREFIX será considerado como Rundown (para caso de dados antigos anteriores a essa mudança)
		return !runningOrderId.startsWith(SCRIPT_PLAN_PREFIX);
	}

	public static Long rundownId(String runningOrderId) {
		return new Long(runningOrderId.replaceFirst(RUNDOWN_PREFIX, ""));
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.Rundown} do Anews para uma
	 * instância de {@link tv.snews.mos.domain.RunningOrder} do MOS.
	 *
	 * @param rundown dados obtidos do Anews
	 * @return o objeto correspondente do MOS
	 */
	public static RunningOrder asRunningOrder(Rundown rundown, MosDevice mosDevice) {
		if (rundown == null) {
			return null;
		}

		RunningOrder runningOrder = new RunningOrder(runningOrderId(rundown));
		parseData(rundown, runningOrder);

		List<Block> blocks = new ArrayList<>(rundown.getBlocks());
		Collections.sort(blocks);

		for (Block block : blocks) {
			for (Story story : block.getStories()) {
				if (story.isApproved()) {
					MosStory mosStory = asMosStory(story, mosDevice.isTpMode());
					MosStory filtered = filterMosItems(mosDevice, mosStory);
					runningOrder.addStory(filtered);
				}
			}
		}

		return runningOrder;
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.Rundown} do Anews para uma
	 * instância de {@link tv.snews.mos.domain.RunningOrderMetadata} do MOS.
	 *
	 * @param rundown dados obtidos do Anews
	 * @return o objeto correspondente do MOS
	 */
	public static RunningOrderMetadata asRunningOrderMetadata(Rundown rundown) {
		if (rundown == null) {
			return null;
		}

		RunningOrderMetadata roMetadata = new RunningOrderMetadata(runningOrderId(rundown));
		parseData(rundown, roMetadata);

		return roMetadata;
	}

	/**
	 * Converte uma instância da classe {@link Story} do Anews para uma
	 * instância de {@link tv.snews.mos.domain.MosStory} do MOS.
	 *
	 * @param story dados obtidos do Anews
	 * @return o objeto correspondente do MOS
	 */
	public static MosStory asMosStory(Story story, boolean tpMode) {
		MosStory mosStory = new MosStory(story.getId().toString(), tpMode);
		mosStory.setNumber(story.getPage());
		mosStory.setSlug(story.getSlug());

		// Garante a ordem das cabeças
		if (story.getHeadSection() != null) {
			for (StorySubSection section : story.getHeadSection().getSubSections()) {
				if (section instanceof StoryCameraText) {
					StoryCameraText camText = (StoryCameraText) section;

					// Junta o número da câmera com o texto
					String aux = "";
					if (camText.getCamera() > 0) {
						aux += "[CAM " + camText.getCamera() + "]\n";
					}
					aux += camText.getText().trim();

					User presenter = camText.getPresenter();
					MosStoryText text = new MosStoryText(presenter.getNickname(), new BigDecimal(presenter.getReadTime()), aux);
					mosStory.addText(text);

					// MOS Objects
					for (StoryMosObject storyMosObj : camText.subSectionsOfType(StoryMosObject.class)) {
                        if (storyMosObj.getMosMedia() == null) {
                            continue;
                        }
						if (storyMosObj instanceof StoryMosCredit && ((StoryMosCredit) storyMosObj).isDisabled()) {
							continue;
						}
						mosStory.addItem(asMosStoryItem(storyMosObj));
					}
				}
			}
		}

		// Roda VT
		if (story.getVtSection() != null) {
			Date vtTime = story.getVtSection().getDuration();
			String vtStr = vtTime == null ? null : timeFormatter.format(vtTime);
			if (vtStr != null && !vtStr.equals("00:00")) {
				mosStory.addText(new MosStoryText("[RODA VT " + vtStr + "]"));

				// Deixa (pode ser nula se apenas informou o tempo pelo espelho)
				if (isNotBlank(story.getVtSection().getCue())) {
					mosStory.addText(new MosStoryText("[DEIXA " + story.getVtSection().getCue() + "]"));
				}
			}
			// MOS Objects
			for (StoryMosObject storyMosObj : story.getVtSection().subSectionsOfType(StoryMosObject.class)) {
                if (storyMosObj.getMosMedia() == null) {
                    continue;
                }
				if (storyMosObj instanceof StoryMosCredit && ((StoryMosCredit) storyMosObj).isDisabled()) {
					continue;
				}
				mosStory.addItem(asMosStoryItem(storyMosObj));
			}
		}

		// Texto do NC somente para NC
		if (story.getNcSection() != null) {
			mosStory.addText(new MosStoryText("[ENTRA IMAGEM]"));
			for (StorySubSection section : story.getNcSection().getSubSections()) {
				if (section instanceof StoryText) {
					StoryText storyText = (StoryText) section;

					User presenter = storyText.getPresenter();
					MosStoryText text = new MosStoryText(presenter.getNickname(), new BigDecimal(presenter.getReadTime()), storyText.getText().trim());
					mosStory.addText(text);
				}
				// MOS Objects
				if (section instanceof StoryMosObject) {
                    if (((StoryMosObject) section).getMosMedia() == null) {
                        continue;
                    }
					if (section instanceof StoryMosCredit && ((StoryMosCredit) section).isDisabled()) {
						continue;
					}
					mosStory.addItem(asMosStoryItem((StoryMosObject) section));
				}
			}
			mosStory.addText(new MosStoryText("[SAI IMAGEM]"));
		}

		// Nota pé
		if (story.getFooterSection() != null) {
			for (StorySubSection section : story.getFooterSection().getSubSections()) {
				if (section instanceof StoryCameraText) {
					StoryCameraText camText = (StoryCameraText) section;

					// Junta o número da câmera com o texto
					String aux = "";
					if (camText.getCamera() > 0) {
						aux += "[CAM " + camText.getCamera() + "]\n";
					}
					aux += camText.getText().trim();

					User presenter = camText.getPresenter();
					MosStoryText text = new MosStoryText(presenter.getNickname(), new BigDecimal(presenter.getReadTime()), aux);
					mosStory.addText(text);

					// MOS Objects
					for (StoryMosObject storyMosObj : camText.subSectionsOfType(StoryMosObject.class)) {
                        if (storyMosObj.getMosMedia() == null) {
                            continue;
                        }
						if (storyMosObj instanceof StoryMosCredit && ((StoryMosCredit) storyMosObj).isDisabled()) {
							continue;
						}
						mosStory.addItem(asMosStoryItem(storyMosObj));
					}
				}
			}
		}

		// Metadata

		Block block = story.getBlock();

		MosExternalMetadata metadata = new MosExternalMetadata();
		metadata.setSchema("");
		metadata.setScope(MosScope.PLAYLIST);
		metadata.addPayloadValue("block", block.isStandBy() ? "Stand By" : Integer.toString(block.getOrder()));
		metadata.addPayloadValue("type", story.getStoryKind() == null ? "" : story.getStoryKind().getAcronym());

		mosStory.addMosExternalMetadata(metadata);

		return mosStory;
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.StoryMosObject} do Anews para o objeto
	 * correspondente da classe {@link tv.snews.mos.domain.MosStoryItem} do MOS.
	 *
	 * @param storyMosObj objeto de mídia.
	 */
	public static MosStoryItem asMosStoryItem(StoryMosObject storyMosObj) {
		MosMedia mosMedia = storyMosObj.getMosMedia();

		MosStoryItem mosStoryItem = new MosStoryItem(mosStoryItemId(storyMosObj));
		mosStoryItem.setSlug(mosMedia.getSlug());
		mosStoryItem.setObjectId(mosMedia.getObjectId());
		mosStoryItem.setMosId(((MosDevice) mosMedia.getDevice()).getMosId());
		mosStoryItem.setAbstractText(mosMedia.getAbstractText());
		mosStoryItem.setDuration(mosMedia.getDuration());

		if (storyMosObj instanceof StoryMosVideo) {
			MosDeviceChannel channel = ((StoryMosVideo) storyMosObj).getChannel();
			mosStoryItem.setChannel(channel == null ? "" : channel.getName());
		}
		if (storyMosObj instanceof StoryMosCredit) {
			String channel = ((StoryMosCredit) storyMosObj).getChannel();
			mosStoryItem.setChannel(channel);
		}

		if (isNotBlank(mosMedia.getMosExternalMetadata())) {
			try {
				MosExternalMetadata mosExtMetadata = new MosExternalMetadata(mosMedia.getMosExternalMetadata());
				mosStoryItem.addMosExternalMetadata(mosExtMetadata);
			} catch (ParsingException e) {
				throw new RuntimeException("Failed to parse the mosExternalMetadata string.", e);
			}
		}

		// Campos não informados (são necessários?)
//		mosStoryItem.start
//		mosStoryItem.userTimingDuration;
//		mosStoryItem.trigger;
//		mosStoryItem.macroIn;
//		mosStoryItem.macroOut;

		// Paths
		for (MosPath mosPath : mosMedia.getPaths()) {
			mosStoryItem.addPath(asMosObjectPath(mosPath));
		}
		for (MosProxyPath mosProxyPath : mosMedia.getProxyPaths()) {
			mosStoryItem.addProxyPath(asMosObjectPath(mosProxyPath));
		}
		if (mosMedia.getMetadataPath() != null) {
			mosStoryItem.setMetadataPath(asMosObjectPath(mosMedia.getMetadataPath()));
		}
		return mosStoryItem;
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private static void parseData(Rundown from, RunningOrderMetadata to) {
		// Retranca fica sendo o nome do programa junto com a data do espelho
		to.setSlug(from.displayName());

		LocalDate date = new LocalDate(from.getDate());
		LocalTime startTime = new LocalTime(from.getStart());
		LocalTime endTime = new LocalTime(from.getEnd());

		// Junta a data do espelho com a hora de início
		to.setStart(date.toLocalDateTime(startTime));

		// Duração é a diferença entre a hora de início e término
		LocalTime diff = endTime.minusHours(startTime.getHourOfDay())
				.minusMinutes(startTime.getMinuteOfHour())
				.minusSeconds(startTime.getSecondOfMinute())
				.minusMillis(startTime.getMillisOfSecond());
		to.setDuration(diff);
	}
}
