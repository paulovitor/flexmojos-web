package tv.snews.anews.infra.mos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.dao.MosDeviceDao;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.event.ScriptPlanEvent;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.domain.RunningOrderMetadata;

import static tv.snews.anews.infra.mos.ScriptPlanMosParser.asRunningOrderMetadata;
import static tv.snews.mos.domain.NcsOperation.INSERT;
import static tv.snews.mos.domain.NcsOperation.REPLACE;

/**
 * @author Felipe Pinheiro
 */
public class ScriptPlanEventListener implements ApplicationListener<ScriptPlanEvent> {

	private static final Logger log = LoggerFactory.getLogger(ScriptPlanEventListener.class);

	private MosConnectionFactory mosConnFactory;
	private MosDeviceDao mosDeviceDao;

	@Override
	public void onApplicationEvent(ScriptPlanEvent event) {
		try {
			switch (event.getAction()) {
				case CREATED:
					notifyInsert(event.getScriptPlan());
					break;

				case START_CHANGED:
				case END_CHANGED:
				case DATE_CHANGED:
				case SLUG_CHANGED:
					notifyReplaceMetadata(event.getScriptPlan());
					break;

				default:
					log.debug("ScriptPlan event action ignored: " + event.getAction());
			}
		} catch (Exception e) {
			/*
			 * Qualquer exceção que possa vir a ocorrer na comunicação com os
			 * dispositivos MOS não deve interromper a execução do fluxo do sistema.
			 */
			log.error("Failed to communicate with the MOS device.", e);
		}
	}

	//------------------------------
	//	Notifiers
	//------------------------------

	private void notifyInsert(ScriptPlan plan) {
		RunningOrderMetadata roMetadata = asRunningOrderMetadata(plan);

		for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
			MosConnection conn = mosConnFactory.getConnection(mosDevice);
			conn.notifyAction(roMetadata, INSERT);
		}
	}

	private void notifyReplaceMetadata(ScriptPlan plan) {
		RunningOrderMetadata roMetadata = asRunningOrderMetadata(plan);

		for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
			MosConnection conn = mosConnFactory.getConnection(mosDevice);
			conn.notifyAction(roMetadata, REPLACE);
		}
	}

	//------------------------------
	//	Setters
	//------------------------------

	public void setMosConnFactory(MosConnectionFactory mosConnFactory) {
		this.mosConnFactory = mosConnFactory;
	}

	public void setMosDeviceDao(MosDeviceDao mosDeviceDao) {
		this.mosDeviceDao = mosDeviceDao;
	}
}
