package tv.snews.anews.infra.mos;

import tv.snews.anews.domain.MosDevice;
import tv.snews.mos.client.MosConnection;

import java.io.Closeable;

/**
 * Define a interface da fábrica de conexões com dispositivos MOS.
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public interface MosConnectionFactory extends Closeable {

	/**
	 * Cria ou retorna uma conexão já existente com o dispositivo MOS informado.
	 * 
	 * @param mosDevice objeto com os dados para a conexão
	 * @return a conexão criada agora ou anteriormente
	 */
	MosConnection getConnection(MosDevice mosDevice);
	
	/**
	 * Redefine o objeto de conexão com o dispositivo informado. Caso ainda não 
	 * tenha sido criada uma conexão com o dispositivo, nada será feito.
	 * 
	 * @param mosDevice dispositivo remoto
	 */
	void resetConnection(MosDevice mosDevice);
	
	/**
	 * Encerra a conexão com o dispositivo informado, caso exista alguma ativa, 
	 * e remove o registro da conexão desta fábrica de conexões.
	 * 
	 * @param mosDevice dispositivo remoto
	 */
	void closeConnection(MosDevice mosDevice);
}
