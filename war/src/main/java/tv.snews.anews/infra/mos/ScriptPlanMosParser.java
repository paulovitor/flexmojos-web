package tv.snews.anews.infra.mos;

import nu.xom.ParsingException;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import tv.snews.anews.domain.*;
import tv.snews.mos.domain.*;

import java.math.BigDecimal;

import static org.apache.commons.lang.StringUtils.isNotBlank;
import static tv.snews.anews.infra.mos.AnewsMosParser.*;
import static tv.snews.anews.infra.mos.MosHelper.filterMosItems;

/**
 * Faz a conversão entre objetos do roteiro e objetos da API MOS.
 *
 * @author Felipe Pinheiro
 */
public class ScriptPlanMosParser {

	public static String runningOrderId(ScriptPlan plan) {
		return SCRIPT_PLAN_PREFIX + plan.getId().toString();
	}

	public static String mosStoryItemId(ScriptMosCredit credit) {
		return CREDIT_PREFIX + credit.getId().toString();
	}

	public static String mosStoryItemId(ScriptVideo video) {
		return VIDEO_PREFIX + video.getId().toString();
	}

	public static Long scriptPlanId(String runningOrderId) {
		return new Long(runningOrderId.replaceFirst(SCRIPT_PLAN_PREFIX, ""));
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.ScriptPlan} do Anews para uma
	 * instância de {@link tv.snews.mos.domain.RunningOrder} do MOS.
	 *
	 * @param plan dados obtidos do ANEWS
	 * @return o objeto correspondente do MOS
	 */
	public static RunningOrder asRunningOrder(ScriptPlan plan, MosDevice mosDevice) {
		if (plan == null) {
			return null;
		}

		RunningOrder runningOrder = new RunningOrder(runningOrderId(plan));
		parseData(plan, runningOrder);

		for (ScriptBlock block : plan.getBlocks()) {
			for (Script script : block.getScripts()) {
				if (script.isApproved()) {
					MosStory mosStory = asMosStory(script, mosDevice.isTpMode());
					MosStory filtered = filterMosItems(mosDevice, mosStory);
					runningOrder.addStory(filtered);
				}
			}
		}

		return runningOrder;
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.ScriptPlan} do ANEWS para uma
	 * instância de {@link tv.snews.mos.domain.RunningOrderMetadata} do MOS.
	 *
	 * @param plan dados obtidos do ANEWS
	 * @return o objeto correspondente do MOS
	 */
	public static RunningOrderMetadata asRunningOrderMetadata(ScriptPlan plan) {
		if (plan == null) {
			return null;
		}

		RunningOrderMetadata roMetadata = new RunningOrderMetadata(runningOrderId(plan));
		parseData(plan, roMetadata);

		return roMetadata;
	}

	/**
	 * Converte uma instância da classe {@link tv.snews.anews.domain.Script} do ANEWS para uma instância
	 * de {@link tv.snews.mos.domain.MosStory} do MOS.
	 *
	 * @param script dados obtidos do ANEWS
	 * @return o objeto correspondente do MOS
	 */
	public static MosStory asMosStory(Script script, boolean tpMode) {
		MosStory mosStory = new MosStory(script.getId().toString(), tpMode);
		mosStory.setNumber(script.getPage());
		mosStory.setSlug(script.getSlug());

		if (isNotBlank(script.getSynopsis())) {
			String text = script.getSynopsis().trim();

			if (script.getPresenter() == null) {
				mosStory.addText(new MosStoryText(text));
			} else {
				String presenter = script.getPresenter().getNickname();
				BigDecimal readTime = new BigDecimal(script.getPresenter().getReadTime());
				mosStory.addText(new MosStoryText(presenter, readTime, text));
			}
		}

		for (ScriptVideo video : script.getVideos()) {
			if (video.getMosMedia() == null) {
                continue;
            }
			mosStory.addItem(asMosStoryItem(video));
		}

		for (AbstractScriptItem item : script.getCgs()) {
			if (item instanceof ScriptMosCredit) {
				ScriptMosCredit credit = (ScriptMosCredit) item;
                if (credit.getMosMedia() == null) {
                    continue;
                }
				if (!credit.isDisabled()) {
					mosStory.addItem(asMosStoryItem(credit));
				}
			}
		}

		ScriptBlock block = script.getBlock();

		MosExternalMetadata metadata = new MosExternalMetadata();
		metadata.setSchema("");
		metadata.setScope(MosScope.STORY);
		metadata.addPayloadValue("block", block.isStandBy() ? "Stand By" : block.getSlug());

		mosStory.addMosExternalMetadata(metadata);

		return mosStory;
	}

	private static MosStoryItem asMosStoryItem(ScriptMosCredit scriptCredit) {
		MosStoryItem mosStoryItem = new MosStoryItem(mosStoryItemId(scriptCredit));
		parseData(scriptCredit.getMosMedia(), mosStoryItem);

		String channel = scriptCredit.getChannel();
		mosStoryItem.setChannel(channel);

		return mosStoryItem;
	}

	public static MosStoryItem asMosStoryItem(ScriptVideo scriptVideo) {
		MosStoryItem mosStoryItem = new MosStoryItem(mosStoryItemId(scriptVideo));
		parseData(scriptVideo.getMosMedia(), mosStoryItem);

		MosDeviceChannel channel = scriptVideo.getChannel();
		mosStoryItem.setChannel(channel == null ? "" : channel.getName());

		return mosStoryItem;
	}

	private static void parseData(ScriptPlan from, RunningOrderMetadata to) {
		LocalDate date = new LocalDate(from.getDate());

		// Usar a retranca do ScriptPlan quebra a separação da data e programa no TP
//		if (isNotBlank(from.getSlug())) {
//			to.setSlug(from.getSlug());
//		} else {
			// Se não informou o tema utiliza o nome do programa com a data
			to.setSlug(from.getProgram().getName() + " " + date.toString());
//		}

		LocalTime startTime = new LocalTime(from.getStart());
		LocalTime endTime = new LocalTime(from.getEnd());

		// Junta a data do roteiro com a hora de início
		to.setStart(date.toLocalDateTime(startTime));

		// Duração é a diferença entre a hora de início e término
		LocalTime diff = endTime.minusHours(startTime.getHourOfDay())
				.minusMinutes(startTime.getMinuteOfHour())
				.minusSeconds(startTime.getSecondOfMinute())
				.minusMillis(startTime.getMillisOfSecond());
		to.setDuration(diff);
	}

	private static void parseData(MosMedia from, MosStoryItem to) {
		to.setSlug(from.getSlug());
		to.setObjectId(from.getObjectId());
		to.setMosId(((MosDevice)from.getDevice()).getMosId());
		to.setAbstractText(from.getAbstractText());
		to.setDuration(from.getDuration());

		if (isNotBlank(from.getMosExternalMetadata())) {
			try {
				MosExternalMetadata mosExtMetadata = new MosExternalMetadata(from.getMosExternalMetadata());
				to.addMosExternalMetadata(mosExtMetadata);
			} catch (ParsingException e) {
				throw new RuntimeException("Failed to parse the mosExternalMetadata string.", e);
			}
		}

		// Paths
		for (MosPath mosPath : from.getPaths()) {
			to.addPath(asMosObjectPath(mosPath));
		}
		for (MosProxyPath mosProxyPath : from.getProxyPaths()) {
			to.addProxyPath(asMosObjectPath(mosProxyPath));
		}
		if (from.getMetadataPath() != null) {
			to.setMetadataPath(asMosObjectPath(from.getMetadataPath()));
		}
	}
}
