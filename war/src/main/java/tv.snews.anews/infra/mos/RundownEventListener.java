package tv.snews.anews.infra.mos;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import tv.snews.anews.dao.MosDeviceDao;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.event.RundownEvent;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.domain.RunningOrderMetadata;

import static tv.snews.anews.infra.mos.RundownMosParser.asRunningOrderMetadata;
import static tv.snews.mos.domain.NcsOperation.INSERT;
import static tv.snews.mos.domain.NcsOperation.REPLACE;

/**
 * Implementação de {@link org.springframework.context.ApplicationListener} que monitora eventos do tipo
 * {@link RundownEvent} para que as mundanças feitas no espelho sejam
 * notificadas para os dispositivos MOS.
 *
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class RundownEventListener implements ApplicationListener<RundownEvent> {

	private static final Logger log = LoggerFactory.getLogger(RundownEventListener.class);

	private MosConnectionFactory mosConnFactory;
	private MosDeviceDao mosDeviceDao;

	@Override
	public void onApplicationEvent(RundownEvent event) {
		try {
			switch (event.getAction()) {
				case CREATED:
					notifyInsert(event.getRundown());
					break;

				case START_CHANGED:
				case END_CHANGED:
				case DATE_CHANGED:
					notifyReplaceMetadata(event.getRundown());
					break;

				default:
					log.debug("Rundown event action ignored: " + event.getAction());
			}
		} catch (Exception e) {
				/*
    		 * Qualquer exceção que possa vir a ocorrer na comunicação com os 
    		 * dispositivos MOS não deve interromper a execução do fluxo do sistema.
    		 */
			log.error("Failed to communicate with the MOS device.", e);
		}
	}

	private void notifyInsert(Rundown rundown) {
		RunningOrderMetadata roMetadata = asRunningOrderMetadata(rundown);

		for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
			MosConnection conn = mosConnFactory.getConnection(mosDevice);
			conn.notifyAction(roMetadata, INSERT);
		}
	}

	private void notifyReplaceMetadata(Rundown rundown) {
		RunningOrderMetadata roMetadata = asRunningOrderMetadata(rundown);

		for (MosDevice mosDevice : mosDeviceDao.listByProfileSupport(2)) {
			MosConnection conn = mosConnFactory.getConnection(mosDevice);
			conn.notifyAction(roMetadata, REPLACE);
		}
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

	public void setMosConnFactory(MosConnectionFactory mosConnFactory) {
		this.mosConnFactory = mosConnFactory;
	}

	public void setMosDeviceDao(MosDeviceDao mosDeviceDao) {
		this.mosDeviceDao = mosDeviceDao;
	}
}
