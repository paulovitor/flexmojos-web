package tv.snews.anews.infra.tvcam;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import tv.snews.anews.domain.CommunicationVehicle;
import tv.snews.anews.domain.Guide;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.User;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Felipe Pinheiro
 */
public class TvCamGuidelineDao {

	private JdbcTemplate jdbc;

	/*
	 * Lista de pautas com mais de uma produção.
	 */
	public List<Guideline> guidelinesToCorrect() {
		return jdbc.query(SEL_GUIDELINES, guidelineRowMapper);
	}

	private List<Guide> guidelineGuides(Guideline guideline) {
		return jdbc.query(SEL_GUIDES, guideRowMapper, guideline.getId());
	}

	private List<CommunicationVehicle> guidelineVehicles(Guideline guideline) {
		return jdbc.query(SEL_VEHICLES, vehicleRowMapper, guideline.getId());
	}

	//------------------------------
	//	Row Mappers
	//------------------------------

	private final RowMapper<Guideline> guidelineRowMapper = new RowMapper<Guideline>() {
		@Override
		public Guideline mapRow(ResultSet rs, int rowNum) throws SQLException {
			Guideline guideline = new Guideline();
			guideline.setId(rs.getLong("id"));
			guideline.setDate(rs.getDate("date"));
			guideline.setSlug(rs.getString("slug"));
			guideline.setExcluded(rs.getBoolean("excluded"));

			User author = new User();
			author.setId(rs.getInt("author_id"));
			guideline.setAuthor(author);

			guideline.getGuides().addAll(guidelineGuides(guideline));
			guideline.getVehicles().addAll(guidelineVehicles(guideline));

			return guideline;
		}
	};

	private final RowMapper<Guide> guideRowMapper = new RowMapper<Guide>() {
		@Override
		public Guide mapRow(ResultSet rs, int rowNum) throws SQLException {
			Guide guide = new Guide();
			guide.setSchedule(rs.getTime("schedule"));
			return guide;
		}
	};

	private final RowMapper<CommunicationVehicle> vehicleRowMapper = new RowMapper<CommunicationVehicle>() {
		@Override
		public CommunicationVehicle mapRow(ResultSet rs, int rowNum) throws SQLException {
			return CommunicationVehicle.valueOf(rs.getString("vehicle"));
		}
	};

	//------------------------------
	//	SQLs
	//------------------------------

	private final String SEL_GUIDELINES =
			"SELECT id, date, slug, excluded, author_id FROM anews.guideline AS g WHERE 1 < (" +
					"SELECT count(*) FROM anews.checklist WHERE guideline_id IS NOT NULL AND g.id = guideline_id" +
			")";

	private final String SEL_GUIDES = "SELECT schedule FROM anews.guide WHERE guideline_id = ? ORDER BY orderguide LIMIT 1";
	private final String SEL_VEHICLES = "SELECT vehicle FROM anews.guideline_has_vehicle WHERE guideline_id = ?";

	//------------------------------
	//	Setters
	//------------------------------

	public void setDataSource(DataSource dataSource) {
		jdbc = new JdbcTemplate(dataSource);
	}
}
