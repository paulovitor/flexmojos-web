package tv.snews.anews.infra.tvcam;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistTask;
import tv.snews.anews.domain.Guideline;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 */
public class TvCamCorrection {

	private static final Logger log = LoggerFactory.getLogger(TvCamCorrection.class);

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

	private TvCamGenericDao genericDao;
	private TvCamGuidelineDao guidelineDao;
	private TvCamChecklistDao checklistDao;

	public void correct() {
		String version = genericDao.currentVersion();

		// Somente executa quando está na versão 1.6.3
		if ("1.6.3".equals(version)) {

			log.info("CORRIGINDO PAUTAS COM MAIS DE UMA PRODUCAO");
			log.info(" ");
			log.info("Buscando por pautas com mais de uma producao...");

			for (Guideline guideline : guidelineDao.guidelinesToCorrect()) {
				log.info("Unificando pauta (ID {})...", guideline.getId());

				Checklist singleChecklist = checklistFor(guideline);

				List<Checklist> checklists = checklistDao.guidelineChecklists(guideline);

				log.info("\t{} producoes a unificar...", checklists.size());

				for (Checklist checklist : checklists) {
					log.info("\ttransformando producao (ID {}) em tarefa...", checklist.getId());

					singleChecklist.add(taskFor(checklist));

					// Apaga a checklist antiga
					checklistDao.delete(checklist);

					log.info("\tproducao (ID {}) apagada.", checklist.getId());
				}

				// Salva a nova checklist
				checklistDao.save(singleChecklist);

				log.info("\tproducao unificada salva.");
				log.info(" ");
			}
		}
	}

	//------------------------------
	//	Helpers
	//------------------------------

	/*
	 * Transforma uma pauta em produção.
	 */
	private Checklist checklistFor(Guideline guideline) {
		Checklist result = new Checklist();

		result.setGuideline(guideline);
		result.setDate(guideline.getDate());
		result.setSlug(guideline.getSlug());
		result.setExcluded(guideline.isExcluded());
		result.setAuthor(guideline.getAuthor());

		if (!guideline.getGuides().isEmpty()) {
			result.setSchedule(guideline.firstGuideSchedule());
		}

		result.getVehicles().addAll(guideline.getVehicles());

		result.setModified(new Date());
		result.setDone(false);
		result.setVersion(0);

		// TODO resources, type?

		return result;
	}

	/*
	 * Transforma uma produção em tarefa.
	 */
	private ChecklistTask taskFor(Checklist checklist) {
		ChecklistTask result = new ChecklistTask();
		result.setDate(checklist.getDate());
		result.setDone(checklist.isDone());
		result.setTime(checklist.getSchedule());
		result.setLabel(checklist.getSlug());

		// Caso tenha tarefas na produção, transforma elas em texto
		StringBuilder builder = new StringBuilder();
		for (ChecklistTask task : checklist.getTasks()) {
			builder.append(toString(task));
			builder.append("\n\n");
		}
		result.setInfo(builder.toString().trim());

		return result;
	}

	/*
	 * Transforma uma tarefa em texto.
	 */
	private String toString(ChecklistTask task) {
		StringBuilder builder = new StringBuilder();

		if (task.isDone()) {
			builder.append("[FEITO] ");
		}

		if (task.getDate() != null || task.getTime() != null) {
			builder.append('(');

			if (task.getDate() != null) {
				builder.append(' ').append(dateFormat.format(task.getDate()));
			}
			if (task.getTime() != null) {
				builder.append(' ').append(timeFormat.format(task.getTime()));
			}

			builder.append(" ) ");
		}

		builder.append(task.getLabel());

		if (StringUtils.isNotBlank(task.getInfo())) {
			builder.append("\n");
			builder.append(task.getInfo().trim());
		}

		return builder.toString();
	}

	//------------------------------
	//	Setters
	//------------------------------

	public void setGenericDao(TvCamGenericDao genericDao) {
		this.genericDao = genericDao;
	}

	public void setGuidelineDao(TvCamGuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public void setChecklistDao(TvCamChecklistDao checklistDao) {
		this.checklistDao = checklistDao;
	}
}
