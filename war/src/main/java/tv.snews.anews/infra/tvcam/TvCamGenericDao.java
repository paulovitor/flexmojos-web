package tv.snews.anews.infra.tvcam;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Felipe Pinheiro
 */
public class TvCamGenericDao {

	private JdbcTemplate jdbc;

	/*
	 * Versão atual registrada pelo flyway.
	 */
	public String currentVersion() {
		return jdbc.queryForObject(SEL_VERSION, versionRowMapper);
	}

	//------------------------------
	//	Row Mappers
	//------------------------------
	
	private final RowMapper<String> versionRowMapper = new RowMapper<String>() {
		@Override
		public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			return rs.getString("version");
		}
	};
	
	//------------------------------
	//	SQLs
	//------------------------------

	private final String SEL_VERSION = "SELECT version FROM anews.schema_version WHERE current_version IS true";

	//------------------------------
	//	Setters
	//------------------------------

	public void setDataSource(DataSource dataSource) {
		jdbc = new JdbcTemplate(dataSource);
	}
}
