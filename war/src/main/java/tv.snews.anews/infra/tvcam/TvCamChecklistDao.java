package tv.snews.anews.infra.tvcam;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistTask;
import tv.snews.anews.domain.CommunicationVehicle;
import tv.snews.anews.domain.Guideline;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Felipe Pinheiro
 */
public class TvCamChecklistDao {

	private JdbcTemplate jdbc;

	private SimpleJdbcInsert insertChecklist;
	private SimpleJdbcInsert insertTask;
	private SimpleJdbcInsert insertVehicle;

	/*
	 * Lista de produções de uma pauta.
	 */
	public List<Checklist> guidelineChecklists(Guideline guideline) {
		return jdbc.query(SEL_CHECKLISTS, checklistRowMapper, guideline.getId());
	}

	/*
	 * Lista de tarefas de uma produção.
	 */
	private List<ChecklistTask> checklistTasks(Checklist checklist) {
		return jdbc.query(SEL_CHECKLIST_TASKS, taskRowMapper, checklist.getId());
	}

	/*
	 * Salva uma produção e suas tarefas.
	 */
	public void save(Checklist checklist) {
		Map<String, Object> values = new HashMap<>();
		values.put("date", checklist.getDate());
		values.put("schedule", checklist.getSchedule());
		values.put("slug", checklist.getSlug());
		values.put("excluded", checklist.isExcluded());
		values.put("author_id", checklist.getAuthor().getId());
		values.put("done", checklist.isDone());
		values.put("version", checklist.getVersion());
		values.put("modified", checklist.getModified());
		values.put("guideline_id", checklist.getGuideline().getId());

		Number id = insertChecklist.executeAndReturnKey(values);
		checklist.setId(id.longValue());

		for (CommunicationVehicle vehicle : checklist.getVehicles()) {
			save(vehicle, checklist);
		}

		for (ChecklistTask task : checklist.getTasks()) {
			save(task);
		}
	}

	/*
	 * Salva um veículo da produção.
	 */
	private void save(CommunicationVehicle vehicle, Checklist checklist) {
		Map<String, Object> values = new HashMap<>();
		values.put("vehicle", vehicle.toString());
		values.put("checklist_id", checklist.getId());

		insertVehicle.execute(values);
	}

	/*
	 * Salva uma tarefa da produção.
	 */
	private void save(ChecklistTask task) {
		Map<String, Object> values = new HashMap<>();
		values.put("date", task.getDate());
		values.put("done", task.isDone());
		values.put("time", task.getTime());
		values.put("label", task.getLabel());
		values.put("info", task.getInfo());
		values.put("index", task.getIndex());
		values.put("checklist_id", task.getChecklist().getId());

		Number id = insertTask.executeAndReturnKey(values);
		task.setId(id.longValue());
	}

	/*
	 * Apaga os dados de uma produção e todos os dados ligados a ela.
	 */
	public void delete(Checklist checklist) {
		for (String sql : DELETE_CHECKLIST) {
			jdbc.update(sql, checklist.getId());
		}
	}

	//------------------------------
	//	Row Mappers
	//------------------------------

	private final RowMapper<Checklist> checklistRowMapper = new RowMapper<Checklist>() {
		@Override
		public Checklist mapRow(ResultSet rs, int rowNum) throws SQLException {
			Checklist checklist = new Checklist();
			checklist.setId(rs.getLong("id"));
			checklist.setDate(rs.getDate("date"));
			checklist.setDone(rs.getBoolean("done"));
			checklist.setSchedule(rs.getTime("schedule"));
			checklist.setSlug(rs.getString("slug"));

			List<ChecklistTask> tasks = checklistTasks(checklist);
			checklist.add(tasks.toArray(new ChecklistTask[] {}));

			return checklist;
		}
	};

	private final RowMapper<ChecklistTask> taskRowMapper = new RowMapper<ChecklistTask>() {
		@Override
		public ChecklistTask mapRow(ResultSet rs, int rowNum) throws SQLException {
			ChecklistTask task = new ChecklistTask();
			task.setId(rs.getLong("id"));
			task.setLabel(rs.getString("label"));
			task.setDone(rs.getBoolean("done"));
			task.setInfo(rs.getString("info"));
			task.setDate(rs.getDate("date"));
			task.setTime(rs.getTime("time"));
			return task;
		}
	};

	//------------------------------
	//	SQLs
	//------------------------------

	private final String SEL_CHECKLISTS      = "SELECT id, date, done, schedule, slug FROM anews.checklist WHERE guideline_id = ?";
	private final String SEL_CHECKLIST_TASKS = "SELECT * FROM anews.checklist_task WHERE checklist_id = ? ORDER BY index";

	private final String[] DELETE_CHECKLIST = {
			"DELETE FROM anews.checklist_has_resource WHERE checklist_id = ?",
			"DELETE FROM anews.checklist_revision WHERE entity_id = ?",
			"DELETE FROM anews.checklist_task WHERE checklist_id = ?",
			"DELETE FROM anews.checklist_has_vehicle WHERE checklist_id = ?",
			"DELETE FROM anews.checklist WHERE id = ?"
	};

	//------------------------------
	//	Setters
	//------------------------------

	public void setDataSource(DataSource dataSource) {
		jdbc = new JdbcTemplate(dataSource);

		insertChecklist = new SimpleJdbcInsert(dataSource)
				.withSchemaName("anews")
				.withTableName("checklist")
				.usingGeneratedKeyColumns("id");

		insertTask = new SimpleJdbcInsert(dataSource)
				.withSchemaName("anews")
				.withTableName("checklist_task")
				.usingGeneratedKeyColumns("id");

		insertVehicle = new SimpleJdbcInsert(dataSource)
				.withSchemaName("anews")
				.withTableName("checklist_has_vehicle");
	}
}
