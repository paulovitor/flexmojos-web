package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.infra.social.TwitterException;
import tv.snews.anews.service.TwitterService;

/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class RefreshTweetsTask implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(RefreshTweetsTask.class);

	private TwitterService twitterService;

	@Override
	public void run() {
		log.debug("Sincronizando tweets...");	
		
		try {
			twitterService.refreshTweetsFromTwitterUsers();
		} catch (TwitterException e) {
			throw new RuntimeException(e);
		}
		
		log.debug("Tweets sincronizados com sucesso!");
	}
	
    public void setTwitterService(TwitterService twitterService) {
    	this.twitterService = twitterService;
    }	
}