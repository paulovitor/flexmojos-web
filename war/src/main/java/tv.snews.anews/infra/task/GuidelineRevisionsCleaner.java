package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.GuidelineRevisionDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.GuidelineRevision;
import tv.snews.anews.domain.Settings;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável pela a limpeza de versões antigas das pautas.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class GuidelineRevisionsCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(GuidelineRevisionsCleaner.class);
	
	private static final int DEFAULT_DAYS = 3;
	private static final int FETCH_LIMIT = 50; //quantidade de pautas que serão excluídas por vez.
	
	private SettingsDao settingsDao;
	private GuidelineRevisionDao guidelineRevisionDao;
	
	private int days;

	public GuidelineRevisionsCleaner() {
		this.days = DEFAULT_DAYS;
	}

	@Override
	@Transactional
	public void run() {
		log.info("Guidelines revisions cleaning started.");
		
		findAndDeleteOldRevisions();
		
		log.info("Guidelines revisions cleaning finished.");
	}
	
	private void findAndDeleteOldRevisions() {
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningGuidelineVersions() > 0) {
			this.days = settings.getCleaningGuidelineVersions();
		}
		final Date minDate = generateMinDate(days);
		log.debug("Guidelines revision date limit: {} ({} days)", minDate, days);
		
		List<GuidelineRevision> oldRevisions;
		while (!(oldRevisions = listOldRevisions(minDate)).isEmpty()) {
			for (GuidelineRevision revision : oldRevisions) {
				guidelineRevisionDao.delete(revision);
				log.debug("Guideline revision with ID {} was deleted.", revision.getId());
			}
		}
	}
	
	/*
	 * Calcula a data mínima a partir do intervalo de dias informado.
	 */
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}

	private List<GuidelineRevision> listOldRevisions(Date minDate) {
		List<GuidelineRevision> oldRevisions = guidelineRevisionDao.listDateLessThan(minDate, FETCH_LIMIT);
		log.debug("Found {} old revisions to delete.", oldRevisions.size());
		return oldRevisions;
	}
	
	//----------------------------------
	//	Setters
	//----------------------------------
	
    public void setGuidelineRevisionDao(GuidelineRevisionDao guidelineRevisionDao) {
	    this.guidelineRevisionDao = guidelineRevisionDao;
    }

	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
