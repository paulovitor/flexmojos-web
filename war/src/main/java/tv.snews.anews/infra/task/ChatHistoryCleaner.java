package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.ChatDao;
import tv.snews.anews.dao.MessageDao;
import tv.snews.anews.domain.AbstractMessage;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.FileMessage;
import tv.snews.anews.service.UserFileService;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável por apagar a mensagens antigas do histórico do chat. Após
 * isso, procura por registros de chat que não possuem mensagens e os apaga.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ChatHistoryCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ChatHistoryCleaner.class);
	
	private static final int DEFAULT_DAYS = 60;
	private static final int FETCH_LIMIT = 50;
	
	private ChatDao chatDao;
	private MessageDao messageDao;
	private UserFileService userFileService;

	@Override
	@Transactional
	public void run() {
		log.info("Chat history cleaning started.");
		
		deleteOldMessages();
		deleteEmptyChats();
		
		log.info("Chat history cleaning finished.");
	}
	
	private void deleteOldMessages() {
		log.debug("Deleting old chat messages & files...");
		
		final Date minDate = generateMinDate(DEFAULT_DAYS);
		log.debug("Messages date limit: {}", minDate);
		
		List<AbstractMessage> oldMessages;
		while (!(oldMessages = messageDao.listMessagesOlderThan(minDate, FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} old messages to delete.", oldMessages.size());
			
			for (AbstractMessage message : oldMessages) {
				messageDao.delete(message);
				log.debug("Message with ID {} was deleted.", message.getId());
				
				// Se for uma mensagem de arquivo tem que apagar também o arquivo
				if (message instanceof FileMessage) {
					FileMessage fileMessage = (FileMessage) message;
					try {
						userFileService.deleteUserFile(fileMessage.getFile());
					} catch (IOException e) {
						throw new RuntimeException("Failed to delete the message user file.", e);
					}
				}
			}
		}
	}
	
	/*
	 * Procura por registros de chat que não possuem mensagens e os apaga.
	 */
	private void deleteEmptyChats() {
		log.debug("Deleting chats with no messages...");
		
		List<Chat> emptyChats = chatDao.listEmptyChats(FETCH_LIMIT);
		while (!emptyChats.isEmpty()) {
			log.debug("Found {} empty chats to delete.", emptyChats.size());
			
			for (Chat chat : emptyChats) {
				chatDao.delete(chat);
				log.debug("Chat with ID {} was deleted.", chat.getId());
			}
			
			emptyChats = chatDao.listEmptyChats(FETCH_LIMIT);
		}
	}
	
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}
	
	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setChatDao(ChatDao chatDao) {
		this.chatDao = chatDao;
	}
	
	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}
	
	public void setUserFileService(UserFileService userFileService) {
		this.userFileService = userFileService;
	}
}
