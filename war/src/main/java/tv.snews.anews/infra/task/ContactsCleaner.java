package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.ContactDao;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;

import java.util.List;

/**
 * Task responsável por fazer a limpeza dos registros de contatos e 
 * relacionamentos vinculados.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ContactsCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ContactsCleaner.class);
	private static final int FETCH_LIMIT = 50; // quantidade de contatos que serão deletados por vez
	
	private ContactDao contactDao;

	@Override
	public void run() {
		log.info("Contacts cleaning started.");
		
		clearExcludedContacts();
		clearContactsWithoutRelationship();
		
		log.info("Contacts cleaning finished.");
	}
	
	@Transactional
	private void clearExcludedContacts() {
		List<AgendaContact> contacts;
		while (!(contacts = contactDao.listExcludedContacts(FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} deleteable contacts.", contacts.size());
			
			for (AgendaContact contact : contacts) {
				contactDao.delete(contact);
			}
		}
	}
	
	@Transactional
	private void clearContactsWithoutRelationship() {
		List<Contact> contacts;
		while (!(contacts = contactDao.listNotUsedContacts(FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} deleteable contacts.", contacts.size());
			
			for (Contact contact : contacts) {
				contactDao.delete(contact);
			}
		}
	}
	
	//----------------------------------
	//	Setters
	//----------------------------------
	
	/**
     * @param contactDao implementação de {@link tv.snews.anews.dao.ContactDao}
     */
    public void setContactDao(ContactDao contactDao) {
	    this.contactDao = contactDao;
    }
}
