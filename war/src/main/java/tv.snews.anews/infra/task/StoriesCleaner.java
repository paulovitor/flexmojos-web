package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.LogDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.StoryLog;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável por fazer a limpeza das laudas que foram excluídas.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class StoriesCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(StoriesCleaner.class);

	private static final int DEFAULT_DAYS = 2;
	private static final int FETCH_LIMIT = 50; //quantidade de laudas que serão excluídas por vez.
	
	private SettingsDao settingsDao;

	private StoryDao storyDao;
    private LogDao logDao;
	
	private int days;

	public StoriesCleaner() {
		this.days = DEFAULT_DAYS;
	}

	@Override
	@Transactional
	public void run() {
		log.info("Stories trash cleaning started.");
		findAndDeleteOldStories();
		log.info("Stories trash cleaning finished.");
	}

	private void findAndDeleteOldStories() {
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningStoriesTrash() > 0) {
			this.days = settings.getCleaningStoriesTrash();
		}
		final Date minDate = generateMinDate(days);
		log.debug("Stories date limit: {}", minDate);

		// Lista das guidelineas que serão deletadas. 		
		List<Story> oldStories;
		while (!(oldStories = storyDao.listExcludedStoriesOlderThan(minDate, FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} old stories to delete.", oldStories.size());

			for (Story story : oldStories) {

                // Remove a referência de possíveis cópias antes de apagar
                for (Story copy : storyDao.findBySourceStory(story)) {
                    copy.setSourceStory(null);
                }

                // Apaga logs
                for (StoryLog log : logDao.findStoryLogsByStory(story)) {
                    logDao.delete(log);
                }

                storyDao.delete(story);
				log.debug("Story with ID {} was deleted.", story.getId());
			}
		}
	}

	/*
	 * Calcula a data mínima a partir do intervalo de dias informado.
	 */
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}

	//----------------------------------
	//	Setters
	//----------------------------------
	
    /**
     * @param storyDao implementação de {@link StoryDao}
     */
    public void setStoryDao(StoryDao storyDao) {
	    this.storyDao = storyDao;
    }

	/**
	 * @param settingsDao implementação de {@link SettingsDao}
	 */
	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}

    public void setLogDao(LogDao logDao) {
        this.logDao = logDao;
    }
}
