
package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.RssItemDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.domain.Settings;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável por apagar os registros de {@link RssItem} salvos com base
 * em um período limite informado pelo método construtor.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssItemsCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(RssItemsCleaner.class);

	private static final int DEFAULT_DAYS = 2;
	private static final int FETCH_LIMIT = 50;

	private int days;
	private RssItemDao rssItemDao;
	private SettingsDao settingsDao;

	public RssItemsCleaner() {
		this.days = DEFAULT_DAYS;
	}

	/**
	 * Enquanto houver {@link RssItem} antigos no banco, baseando-se no limite
	 * definido no construtor, obtém uma lista deles com a quantidade definida
	 * pela constante interna {@code FETCH_LIMIT} e os apaga.
	 */
	@Override
	@Transactional
	public void run() {
		if (days == 0) {
			throw new IllegalArgumentException();
		}

		log.info("RSS items cleaner started.");
		
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningRss() > 0) {
			this.days = settings.getCleaningRss();
		}
		
		final Date minDate = generateMinDate(days);
		log.debug("Items date limit: {}", minDate);

		List<RssItem> oldItems;
		while (!(oldItems = rssItemDao.listItemsOlderThan(minDate, FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} old messages to delete.", oldItems.size());

			for (RssItem item : oldItems) {
				rssItemDao.delete(item);
				log.debug("RssItem with ID {} was deleted.", item.getId());
			}
		}

		log.info("RSS items cleaning finished.");
	}

	/*
	 * Calcula a data mínima a partir do intervalo de dias informado.
	 */
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}

	//---------------------------------------
	//	Setters
	//---------------------------------------

	public void setRssItemDao(RssItemDao rssItemDao) {
		this.rssItemDao = rssItemDao;
	}

	/**
	 * @param settingsDao
	 *            the settingsDao to set
	 */
	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
