package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.exception.DataNotFoundException;
import tv.snews.anews.util.StorageUtil;
import tv.snews.anews.web.UploadController;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Faz a limpeza dos arquivos temporários que não estão mais sendo usados.
 * 
 * @author Eliezer Reis 
 * @since 1.0.0
 */
public class TemporaryFilesCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(TemporaryFilesCleaner.class);
	private static final int FILE_AGE_LIMIT = 3 * 60 * 60 * 100; // 3 horas
	private StorageUtil storageUtil;

	@Override
	public void run() {
		log.debug("Cleaning old temporary files...");
		try {
            File tmpDir = storageUtil.getTemporaryDirectory();
            log.debug("Cleaning files from {}", tmpDir.getAbsolutePath());
            deleteFiles(tmpDir);
            File tmpDirScriptImagePreviewer = new File(storageUtil.getTemporaryDirectory().getPath() + UploadController.PREVIEWER);
            log.debug("Cleaning files from {}", tmpDirScriptImagePreviewer.getAbsolutePath());
            deleteFiles(tmpDirScriptImagePreviewer);
            File tmpDirScriptImageThumbnail = new File(storageUtil.getTemporaryDirectory().getPath() + UploadController.THUMBNAIL);
            log.debug("Cleaning files from {}", tmpDirScriptImageThumbnail.getAbsolutePath());
            deleteFiles(tmpDirScriptImageThumbnail);

        } catch (DataNotFoundException e) {
			log.error("Settings not found on database", e);
		} catch (IOException e) {
			log.error("IOException", e);
		}
		
		log.debug("Old temporary files cleaning completed.");
	}

    private void deleteFiles(File tmpDir) {
        if(tmpDir != null && tmpDir.listFiles() != null) {
            final long now = new Date().getTime();
            for (File tmpFile : tmpDir.listFiles()) {
                if (now - tmpFile.lastModified() > FILE_AGE_LIMIT) {
                    if (tmpFile.delete()) {
                        log.debug("\"{}\" was deleted.", tmpFile.getName());
                    } else {
                        log.error("Temporary file \"{}\" wasn't deleted.", tmpFile.getName());
                    }
                }
            }
        }
    }

    //----------------------------------
	//	Setters
	//----------------------------------
	
	public void setStorageUtil(StorageUtil storageUtil) {
		this.storageUtil = storageUtil;
	}
}
