
package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.ContactDao;
import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.*;

import java.util.*;

/**
 * Task responsável por fazer a limpeza das pautas que foram excluídas.
 * 
 * @author Paulo Felipe de Araújo
 * @since 1.0.0
 */
public class GuidelinesCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(GuidelinesCleaner.class);

	private static final int DEFAULT_DAYS = 3;
	private static final int FETCH_LIMIT = 50; //quantidade de pautas que serao deletadas por vez.
	private int days;

	private ContactDao contactDao;
	private GuidelineDao guidelineDao;
	private SettingsDao settingsDao;

	public GuidelinesCleaner() {
		this.days = DEFAULT_DAYS;
	}

	@Override
	@Transactional
	public void run() {
		log.info("Guideline trash cleaning started.");
		deleteOldGuidelines();
		log.info("Guideline trash cleaning finished.");
	}

	private void deleteOldGuidelines() {
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningGuidelinesTrash() > 0) {
			this.days = settings.getCleaningGuidelinesTrash();
		}
		final Date minDate = generateMinDate(days);
		log.debug("Guidelines date limit: {}", minDate);

		// Lista das guidelineas que serão deletadas. 		
		List<Guideline> oldGuidelines;
		while (!(oldGuidelines = guidelineDao.findExcludedBefore(minDate, FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} old guidelines to delete.", oldGuidelines.size());

			for (Guideline guideline : oldGuidelines) {
				// Apaga os possíveis contatos exclusivos dos roteiros da pauta
				for (Guide guide : guideline.getGuides()) {
					for (Iterator<Contact> it = guide.getContacts().iterator(); it.hasNext(); ) {
						Contact contact = it.next();
						if (!(contact instanceof AgendaContact)) {
							it.remove();
							contactDao.delete(contact);
						}
					}
				}
				
				guidelineDao.delete(guideline);
				log.debug("Guideline with ID {} was deleted.", guideline.getId());
			}
		}
	}

	/*
	 * Calcula a data mínima a partir do intervalo de dias informado.
	 */
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}

	//----------------------------------
	//	Setters
	//----------------------------------
	
    public void setContactDao(ContactDao contactDao) {
	    this.contactDao = contactDao;
    }
	
	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
