package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.ReportageRevisionDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.ReportageRevision;
import tv.snews.anews.domain.Settings;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável pela a limpeza de versões antigas das reportagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportageRevisionsCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(ReportageRevisionsCleaner.class);
	
	private static final int DEFAULT_DAYS = 3;
	private static final int FETCH_LIMIT = 50; // quantidade de reportagens que serão excluídas por vez.
	
	private SettingsDao settingsDao;
	private ReportageRevisionDao reportageRevisionDao;
	
	private int days;

	public ReportageRevisionsCleaner() {
		this.days = DEFAULT_DAYS;
	}
	
	@Override
	@Transactional
	public void run() {
		log.info("Reportage revisions cleaning started.");
		findAndDeleteOldRevisions();
		log.info("Reportage revisions cleaning finished.");
	}
	
	private void findAndDeleteOldRevisions() {
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningReportageVersions() > 0) {
			this.days = settings.getCleaningReportageVersions();
		}
		final Date minDate = generateMinDate(days);
		log.debug("Reportages revision date limit: {} ({} days)", minDate, days);
		
		List<ReportageRevision> oldRevisions;
		while (!(oldRevisions = listOldRevisions(minDate)).isEmpty()) {
			for (ReportageRevision revision : oldRevisions) {
				reportageRevisionDao.delete(revision);
				log.debug("Reportage revision with ID {} was deleted.", revision.getId());
			}
		}
	}
	
	/*
	 * Calcula a data mínima a partir do intervalo de dias informado.
	 */
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}

	private List<ReportageRevision> listOldRevisions(Date minDate) {
		List<ReportageRevision> oldRevisions = reportageRevisionDao.listDateLessThan(minDate, FETCH_LIMIT);
		log.debug("Found {} old revisions to delete.", oldRevisions.size());
		return oldRevisions;
	}
	
	//----------------------------------
	//	Setters
	//----------------------------------
	
    public void setReportageRevisionDao(ReportageRevisionDao reportageRevisionDao) {
	    this.reportageRevisionDao = reportageRevisionDao;
    }

	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
