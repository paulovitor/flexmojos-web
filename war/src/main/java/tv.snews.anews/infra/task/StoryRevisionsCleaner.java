package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.dao.StoryRevisionDao;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.StoryRevision;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável pela a limpeza de versões antigas das laudas.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class StoryRevisionsCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(StoryRevisionsCleaner.class);
	
	private static final int DEFAULT_DAYS = 3;
	private static final int FETCH_LIMIT = 50; //quantidade de revisões laudas que serão excluídas por vez.
	
	private SettingsDao settingsDao;
	private StoryRevisionDao storyRevisionDao;
	
	private int days;

	public StoryRevisionsCleaner() {
		this.days = DEFAULT_DAYS;
	}
	
	@Override
	@Transactional
	public void run() {
		log.info("Stories revisions cleaning started.");
		findAndDeleteOldRevisions();
		log.info("Stories revisions cleaning finished.");
	}
	
	private void findAndDeleteOldRevisions() {
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningStoryVersions() > 0) {
			this.days = settings.getCleaningStoryVersions();
		}
		final Date minDate = generateMinDate(days);
		log.debug("Stories revisions date limit: {}", minDate);
		
		List<StoryRevision> oldRevisions;
		while (!(oldRevisions = listOldRevisions(minDate)).isEmpty()) {
			for (StoryRevision revision : oldRevisions) {
				storyRevisionDao.delete(revision);
				log.debug("Story revision with ID {} was deleted.", revision.getId());
			}
		}
	}
	
	/*
	 * Calcula a data mínima a partir do intervalo de dias informado.
	 */
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}

	private List<StoryRevision> listOldRevisions(Date minDate) {
		List<StoryRevision> oldRevisions = storyRevisionDao.listDateLessThan(minDate, FETCH_LIMIT);
		log.debug("Found {} old versions to delete.", oldRevisions.size());
		return oldRevisions;
	}
	
	//----------------------------------
	//	Setters
	//----------------------------------
	
    /**
     * @param storyRevisionDao implementação de {@link StoryRevisionDao}
     */
    public void setStoryRevisionDao(StoryRevisionDao storyRevisionDao) {
	    this.storyRevisionDao = storyRevisionDao;
    }

	/**
	 * @param settingsDao implementação de {@link SettingsDao}
	 */
	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
