package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.ChecklistDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.Settings;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class ChecklistsCleaner implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(GuidelinesCleaner.class);

    private static final int DEFAULT_DAYS = 3;
    private static final int FETCH_LIMIT = 50;
    private int days;

    private ChecklistDao checklistDao;
    private SettingsDao settingsDao;

    public ChecklistsCleaner() {
        this.days = DEFAULT_DAYS;
    }

    @Override
    public void run() {
        log.info("Checklist trash cleaning started");

        Settings settings = settingsDao.loadSettings();
        if (settings != null && settings.getCleaningChecklistsTrash() > 0) {
            this.days = settings.getCleaningChecklistsTrash();
        }

        final Date limit = generateMinDate(days);
        log.debug("Checklist date limit: {}", limit);

        // Lista das checklists que serão excluídas
        List<Checklist> checklists;
        while (!(checklists = checklistDao.findAllExcludedBefore(limit, FETCH_LIMIT)).isEmpty()) {
            log.debug("{} checklists will be deleted permanently.", checklists.size());

            for (Checklist checklist : checklists) {
                checklistDao.delete(checklist);
                log.debug("Checklist with ID {} was deleted forever.", checklist.getId());
            }
        }

        log.info("Checklist trash cleaned");
    }

    //----------------------------------
    //	Helpers
    //----------------------------------

    private Date generateMinDate(int interval) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
        return calendar.getTime();
    }

    //----------------------------------
    //	Setters
    //----------------------------------

    public void setChecklistDao(ChecklistDao checklistDao) {
        this.checklistDao = checklistDao;
    }

    public void setSettingsDao(SettingsDao settingsDao) {
        this.settingsDao = settingsDao;
    }
}
