package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.infra.social.TwitterException;
import tv.snews.anews.infra.social.TwitterIntegration;
import tv.snews.anews.service.TwitterService;

import java.util.List;

/**
 * Task que atualiza (se necessário) os dados pessoais de cada TwitterUser 
 * cadastrado no sistema.
 *  
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class UpdateTwitterUsersData implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(UpdateTwitterUsersData.class);

	private TwitterService twitterService;
	private TwitterIntegration twitter;

	private int totalUpdated = 0;
	
	@Override
	@Transactional
	public void run() {
		totalUpdated = 0;
		log.debug("Initializing update twitter users data");
		List<TwitterUser> twitterUsers = twitterService.listAllTwitterUser();
		
		try {
			for (TwitterUser twitterUser : twitterUsers) {
				TwitterUser twitterUserFromAPI = twitter.getTwitterUser(twitterUser.getScreenName());
				if (twitterUserFromAPI != null) {
					synchronizeTwitterUser(twitterUser , twitterUserFromAPI);
				}
	        }
		} catch (TwitterException | ValidationException e) {
			throw new RuntimeException(e);
        }
		
		log.debug("Total TwitterUsers  updated: " + totalUpdated);
		log.debug("Update completed successfully");
	}
	
	/**
	 * Método que sincroniza os dados do twitterUser da API com o já
	 * cadastrado no sistema.
	 * 
	 * @param twitterUser usuário já cadastrado no sistema.
	 * @param twitterUserFromAPI usuário com dados atualizados da API do twitter.
	 * @throws TwitterException 
	 */
	private void synchronizeTwitterUser(TwitterUser	twitterUser, TwitterUser twitterUserFromAPI) throws TwitterException {
		boolean toUpdate = false;
		
		// Veririfca se houve mudanças para então atualizar na base
		if (!twitterUser.getName().equals(twitterUserFromAPI.getName())) {
			twitterUser.setName(twitterUserFromAPI.getName());
			toUpdate = true;
		}
		
		if (!twitterUser.getProfileImageUrl().equals(twitterUserFromAPI.getProfileImageUrl())) {
			twitterUser.setProfileImageUrl(twitterUserFromAPI.getProfileImageUrl());
			toUpdate = true;
		}
		
		if (toUpdate) {
			twitterService.save(twitterUser);
			totalUpdated++;
		}
	}
	
    public void setTwitterService(TwitterService twitterService) {
    	this.twitterService = twitterService;
    }	
    
    public void setTwitter(TwitterIntegration twitter) {
    	this.twitter = twitter;
    }
}
