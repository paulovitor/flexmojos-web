package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.RoundDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.Round;
import tv.snews.anews.domain.Settings;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Task responsável por apagar a rondas antigas.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class RoundsCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(RoundsCleaner.class);
	
	private static final int DEFAULT_DAYS = 2;
	private static final int FETCH_LIMIT = 50;
	
	private RoundDao roundDao;
	
	private SettingsDao settingsDao;

	private int days;
	
	public RoundsCleaner() {
		this.days = DEFAULT_DAYS; 
	}
	
	@Override
	@Transactional
	public void run() {
    	if (days == 0) {
    		throw new IllegalArgumentException();
    	}
    	
		log.info("Round cleaning started.");	
		deleteOldRounds();
		log.info("Round cleaning finished.");
	}
	
	private void deleteOldRounds() {
		log.debug("Deleting old rounds...");
		
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningRound() > 0) {
			this.days = settings.getCleaningRound();
		}
		
		final Date minDate = generateMinDate(days);
		log.debug("Round date limit: {}", minDate);
		
		List<Round> oldRounds;
		while (!(oldRounds = roundDao.listRoundOlderThan(minDate, FETCH_LIMIT)).isEmpty()) {
			log.debug("Found {} old round to delete.", oldRounds.size());
			
			for (Round round : oldRounds) {
				roundDao.delete(round);
				log.debug("Round with ID {} was deleted.", round.getId());
			}
		}
	}
	
	private Date generateMinDate(int interval) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
		return calendar.getTime();
	}
	
	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setDays(int days) {
		this.days = days;
	}
	
    public void setRoundDao(RoundDao roundDao) {
    	this.roundDao = roundDao;
    }
    
    public void setSettingsDao(SettingsDao settingsDao) {
    	this.settingsDao = settingsDao;
    }
}
