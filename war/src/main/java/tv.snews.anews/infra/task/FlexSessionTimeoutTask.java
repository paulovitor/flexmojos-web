package tv.snews.anews.infra.task;

import flex.messaging.FlexSession;
import flex.messaging.client.FlexClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.User;
import tv.snews.anews.flex.FlexSessionManager;
import tv.snews.anews.infra.SessionManager;

import java.util.Map;
import java.util.Map.Entry;

/**
 * Task executada para verificar se uma sessão expirou e então invalida-la.
 * 
 * @author Felipe Zap de Mello
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class FlexSessionTimeoutTask implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(FlexSessionTimeoutTask.class);

	private int SESSION_TIMEOUT = 60_000;
	
	private FlexSessionManager sessionManager;
	private SettingsDao settingsDao;

	@Override
	public void run() {
        SESSION_TIMEOUT = settingsDao.loadSettings().getSessionExpiration() * 60_000;

        final long now = System.currentTimeMillis();
        
		Map<User, FlexSession> sessions = sessionManager.getSessions();
		
		log.info("--------------------");
		log.info("SESSIONS_COUNT  : {}", sessions.size());
		log.info("SESSION_TIMEOUT : {}", SESSION_TIMEOUT);
		
		for (Entry<User, FlexSession> entry : sessions.entrySet()) {
			FlexSession session = entry.getValue();
			
			if (session.isValid()) {
//				boolean sessionHasActivedClients = false;

				log.info("--------------------");
				log.info("CLIENTS_COUNT : {} ", session.getFlexClients().size());
				
				for (FlexClient client : session.getFlexClients()) {
					long idleTime = now - client.getLastUse();

					log.info("--------------------");
					log.info("-> CLIENT_ID            : {}", client.getId());
					log.info("-> CLIENT_IDLE          : {}", idleTime);
					log.info("-> CLIENT_TIMEOUTPERIOD : {}", client.getTimeoutPeriod());
					
//					if (idleTime < SESSION_TIMEOUT) {
//						sessionHasActivedClients = true;
//					} else {
//						client.invalidate();
//						log.warn("Client {} invalidated. Last activity at {}.", client.getId(), new Date(client.getLastUse()));
//					}
				}
				
//				if (!sessionHasActivedClients) {
//					log.info("Found one expired session. Kicking out the user..."); 
//					sessionManager.kickOut(entry.getKey());
//				}
			}
		}
	}
	
    public void setSessionManager(SessionManager sessionManager) {
    	this.sessionManager = (FlexSessionManager) sessionManager;
    }
    
    /**
     * @param settingsDao the settingsDao to set
     */
    public void setSettingsDao(SettingsDao settingsDao) {
    	this.settingsDao = settingsDao;
    }

}