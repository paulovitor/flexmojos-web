package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.SettingsDao;

/**
 * De tempos em tempos, o index do Lucene precisa ser otimizado. O processo é 
 * basicamente uma desfragmentação. Até a otimização ser executada o Lucene irá 
 * apenas marcar documentos como excluídos, nenhuma exclusão física é feita. 
 * Durante o processo de otimização as exclusões serão aplicadas, o que também 
 * afeta o número de arquivos do diretório do Lucene.
 * 
 * <p>Otimizar o index do Lucene acelera as buscas mas não tem efeito na 
 * performance de atualizações do index. Durante as otimização, buscas podem 
 * ser feitas, mas provavelmente serão mais lentas. Todas as atualizações do 
 * index serão suspendidas. É recomendado que a otimização seja feita:</p>
 * 
 * <ul>
 * 	<li>quando o sistema está ocioso ou quando as buscas são menos frequentes</li>
 * 	<li>após muitas modificações no index</li>
 * </ul>
 * 
 * <p>Mais informações podem ser obtidas na documentação oficial do Hibernate Search.</p>
 * 
 * @see <a href="http://docs.jboss.org/hibernate/search/3.4/reference/en-US/html_single/#search-optimize">Documentação: Index Optimization</a>
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class LuceneIndexOptimizer implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(LuceneIndexOptimizer.class);

	private SettingsDao settingsDao;
	
    @Override
    public void run() {
    	log.info("Lucene indexes optimization proccess started.");
    	
    	// Existe a otimização automática, mas ela pode ser executada em um horário impróprio
    	settingsDao.optimizeIndexes();
    	
    	log.info("Lucene indexes optimization proccess finished.");
    }
    
    //------------------------------------
	//  Setters
	//------------------------------------
    
    public void setSettingsDao(SettingsDao settingsDao) {
	    this.settingsDao = settingsDao;
    }
}
