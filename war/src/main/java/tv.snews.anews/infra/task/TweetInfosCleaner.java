package tv.snews.anews.infra.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.dao.TweetInfoDao;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.TweetInfo;

import java.util.List;

/**
 * Task responsável por apagar os registros de {@link tv.snews.anews.domain.TweetInfo} salvos com base
 * em um período limite informado pelo método construtor.
 *
 * @author Felipe Pinheiro
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TweetInfosCleaner implements Runnable {

	private static final Logger log = LoggerFactory.getLogger(TweetInfosCleaner.class);

	private static final int DEFAULT_DAYS = 2;
	private static final int FETCH_LIMIT = 50;
	private static final int MINIMUN_OF_REGISTERS = 50;

	private int days;
	private TweetInfoDao tweetInfoDao;
	private SettingsDao settingsDao;

    public TweetInfosCleaner() {
	    this.days = DEFAULT_DAYS;
    }

    /**
     * Enquanto houver {@link tv.snews.anews.domain.TweetInfo} antigos no banco, baseando-se no limite
     * definido no construtor, obtém uma lista deles com a quantidade definida 
     * pela constante interna {@code FETCH_LIMIT} e os apaga.
     */
    @Override
    @Transactional
    public void run() {
		if (days == 0) {
			throw new IllegalArgumentException();
		}

		log.info("Tweets from Twitter Users cleaner started.");
		Settings settings = settingsDao.loadSettings();
		if (settings != null && settings.getCleaningTweets() > 0) {
			this.days = settings.getCleaningTweets();
		}
		
//    	final Date minDate = generateMinDate(days);
//    	log.debug("Tweets date limit: {}", minDate);
    	
    	List<TweetInfo> oldTweets;
    	while (!(oldTweets = tweetInfoDao.listTweets(MINIMUN_OF_REGISTERS, FETCH_LIMIT)).isEmpty()) {
    		log.debug("Found {} old tweets to delete", oldTweets.size());
    		
			for (TweetInfo tweet : oldTweets) {
    			tweetInfoDao.delete(tweet);
    			log.debug("TweetInfo with ID {} was deleted", tweet.getId());
    		}
    		
    	}
    	
    	log.info("Tweets cleaning finished.");
    }
    
    /*
     * Calcula a data mínima a partir do intervalo de dias informado.
     */
//    private Date generateMinDate(int interval) {
//		GregorianCalendar calendar = new GregorianCalendar();
//		calendar.add(Calendar.DAY_OF_MONTH, interval * -1);
//		return calendar.getTime();
//	}
    
    //---------------------------------------
    //	Setters
    //---------------------------------------
    
    public void setTweetInfoDao(TweetInfoDao tweetInfoDao) {
	    this.tweetInfoDao = tweetInfoDao;
    }
    
	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}
}
