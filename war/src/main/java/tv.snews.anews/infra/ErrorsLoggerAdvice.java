package tv.snews.anews.infra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Força a exibição dos erros lançados no log.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ErrorsLoggerAdvice {

	private static final Logger log = LoggerFactory.getLogger(ErrorsLoggerAdvice.class);

	public void logException(Throwable exception) {
		log.error("[ Uncaught exception ]", exception);
	}
}
