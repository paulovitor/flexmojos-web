package tv.snews.anews.service;

import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.ClipboardAction;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Fornece os serviços para manipulação dos laudas.
 * 
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface StoryService {

	String OK = "OK";
	String RUNDOWN_NOT_FOUND = "RUNDOWN_NOT_FOUND";
	String RUNDOWN_DISPLAYED = "RUNDOWN_DISPLAYED";

	/**
	 * Retorna os dados da lauda que possui o identificador informado.
	 * 
	 * @param id identificador único da lauda
	 * @return objeto com os dados da lauda ou {@code null} se nenhum registro
	 *         for encontrado
	 */
	Story loadById(Long id);

	void createStory(long blockId, int index) throws ValidationException, SessionManagerException;

	void createStoryFromGuideline(long blockId, int index, long guidelineId) throws ValidationException, SessionManagerException;

	void createStoryFromReportage(long blockId, int index, long reportageId) throws ValidationException, SessionManagerException;

	void moveStory(long blockIdDestination, int storyOrder, long storyId) throws ValidationException;

	void moveStories(Long[] storiesIds, Long blockId, int dropIndex);

	void removeStory(long storyId) throws ValidationException, SessionManagerException;

	void remove(Story story) throws ValidationException, SessionManagerException;

	/**
	 * Atualiza um determinado campo da lauda.
	 * 
	 * @param storyId Id da lauda que será atualizada
	 * @param field O campo da lauda que será atualizada
	 * @param object O conteúdo do campo que será atualizado
	 */
	void updateField(long storyId, String field, Object object) throws ValidationException;

	/**
	 * Retorna lista de laudas de um espelho.
	 * 
	 * @param rundownId espelho para buscar suas laudas.
	 * @return List<Story> lista de laudas.
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	List<Story> listStoriesByRundown(Long rundownId) throws ValidationException;

	/**
	 * Faz a atualização dos dados de uma lauda já cadastrada.
	 * ATENÇÃO, o unico objeto que condiz com o banco (os ids por exemplo) e o
	 * retornado
	 * pelo metodo.
	 *
	 * @param story
	 * @return objeto lauda atualizado.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	Story merge(Story story) throws SessionManagerException;

	/**
	 * Restaura uma lauda que foi excluída para o espelho do dia e programa
	 * informados.
	 *
	 * @param storyId lauda a ser restaurada
	 * @param date data destino da restauração
	 * @param programId programa destino da restauração
	 * @return código do resultado da restauração
	 * @throws tv.snews.anews.exception.SessionManagerException se falhar ao obter os dados do usuário
	 *             logado
	 */
	String restoreExcludedStory(Long storyId, Date date, int programId) throws SessionManagerException;

	/**
	 * Faz a contagem de todas as laudas marcadas como excluídas.
	 *
	 * @return a quantidade de laudas excluídas
	 */
	int countExcludedStories();

	PageResult<Story> listExcludedStories(int pageNumber);

	/**
	 * Salva uma lauda já existente.
	 *
	 * @param story
	 * @return Objeto da lauda com seus respectivos objetos populados em sua
	 *         composição.
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	void save(Story story) throws ValidationException;

	void copyTo(Long[] storiesIds, Date date, Integer programId, boolean move) throws CloneNotSupportedException;

	Long[] sendStoriesToClipboard(ClipboardAction action, Long[] ids);

	Long[] pastStoriesFromClipboard(Long blockId, int dropIndex) throws CloneNotSupportedException;

	String copyRemoteStory(Story remoteStory, Date destinyDate, Program destinyProgram);

	Story remoteCopyRequest(Long reportageId, String userNickname, String company);

	String copyDrawerStoryToBlock(Long storyId, Long blockId, int index) throws CloneNotSupportedException;

	/**
	 * Busca por todas as laudas não excluídas e que não estão na gaveta que
	 * pertencem ao espelho do dia e programa informados.
	 *
	 * @param date data do espelho
	 * @param program programa do espelho
	 * @return a lista de laudas do espelho informado
	 */
	List<Story> listByDateAndProgram(Date date, Program program);

	List<Story> listStoriesByDateEditorOrderProgram(Date date, User editor);

	/**
	 * Carrega a lista de revisões da lauda, em ordem decrescente de criação.
	 * Serão marcadas as mudanças feitas em cada revisão em relação a revisão
	 * anterior.
	 *
	 * @param storyId identificador da lauda
	 * @return a lista de mudanças feitas
	 */
	Collection<StoryRevision> loadChangesHistory(Long storyId);

	/**
	 * Carrega uma revisão especifica da lauda, a partir do {@code id} da lauda
	 * e do número da revisão.
	 * <p>
	 * Caso o valor de {@code showDiff} seja {@code true}, serão marcadas mas
	 * mudanças feitas em relação a revisão atual e a revisão solicitada.
	 * </p>
	 *
	 * @param storyId identificador da lauda
	 * @param revisionNumber número da revisão
	 * @param showDiff flag para marcação das mudanças
	 * @return a revisão encontrada
	 */
	StoryRevision loadRevision(Long storyId, int revisionNumber, boolean showDiff);

	/**
	 * Método utilizado para a listagem paginada dos registros da área de
	 * arquivo de laudas. Retorna a lista do registros já salvos, permitindo
	 * informar
	 * parâmetros opcionais de busca. Os parâmetros de busca que forem
	 * {@code null} serão ignorados.
	 *
	 * @return lista de registros encontrados de acordo com os parâmetros
	 *         informados
	 */
	PageResult<Story> storiesByFullTextSearch(String slug, String text, Program program, Date initialDate, Date finalDate, int pageNumber,
                                              SearchType searchType, String textIgnore);

	void lockEdition(Long storyId, Integer userId) throws IllegalOperationException;

    void assumeEdition(Long storyId, Integer userId, Integer droppedUserId) throws IllegalOperationException;

	User isLockedForEdition(Long storyId);

	void unlockEdition(Long storyId) throws IllegalOperationException;

	void lockDragging(Long storyId, Integer userId) throws IllegalOperationException;

	void unlockDragging(Long storyId) throws SessionManagerException;

	/**
	 * Retorna uma lista de Videos da lauda.
	 */
	List<StorySubSection> loadVideosByStoryId(Long id);

	/**
	 * Remove um videos da lauda.
	 */
	void removeVideoByStoryMosObjById(Long idStory, StoryMosVideo stMosObj);

	/**
	 * Envia uma lauda para a gaveta.
	 *
	 * @param story
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	void sendToDrawer(Story story) throws ValidationException;

	void sendReportageToDrawer(Long reportageId, Integer programId) throws ValidationException;

	void createStoryMosVideo(Long id, StoryMosVideo storyMosVideo) throws ValidationException, IOException, IllegalOperationException;

	void createStoryWSVideo(Long id, StoryWSVideo storyWSVideo) throws RemoteException, ServiceException, WebServiceException;

    void loadAndUpdateStoryWSVideo(long storyId, StoryWSVideo storyWSVideo) throws RemoteException, ServiceException, WebServiceException;

    void removeStories(Long[] idsOfStories, Long rundownId);

    void clearClipboard();

    void createStoryFromNews(long blockId, int index, long newsId) throws ValidationException, SessionManagerException;

}
