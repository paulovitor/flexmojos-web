
package tv.snews.anews.service;

import tv.snews.anews.domain.City;

import java.io.Serializable;
import java.util.List;

/**
 * Interface do serviço de manipulação de cidades.
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface CityService {

	/**
	 * Lista cidades pelo estado.
	 * 
	 * @param idState
	 * @return List<City>
	 */
	List<City> listAll(Serializable idState);

	/**
	 * Consulta cidade pelo identificador.
	 * 
	 * @param idCity
	 * @return City
	 */
	City get(Serializable idCity);
	
	/**
	 * Exclui a cidade.
	 * 
	 * @param City
	 */
	void remove(City city);

	/**
	 * Salva a cidade.
	 * 
	 * @param City
	 */
	void save(City city);
	
}
