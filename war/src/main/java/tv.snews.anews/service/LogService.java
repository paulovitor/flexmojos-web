package tv.snews.anews.service;

import tv.snews.anews.domain.AbstractLog;
import tv.snews.anews.domain.BlockLog;
import tv.snews.anews.domain.RundownLog;
import tv.snews.anews.domain.StoryLog;
import tv.snews.anews.flex.PageResult;

import java.util.List;

/**
 * Interface que define o contrado do serviço para manipulação dos logs
 * do espelho.
 *
 * @author Felipe Zap de Mello
 * @since 1.2.6
 */
public interface LogService {

    PageResult<StoryLog> listLogsByStoryAction(Long rundownId, List<String> actions, int pageNumber);

    PageResult<BlockLog> listLogsByBlockAction(Long rundownId, List<String> actions, int pageNumber);

    PageResult<RundownLog> listLogsByRundownAction(Long rundownId, List<String> actions, int pageNumber);
    
    PageResult<AbstractLog> listAll(Long rundownId, int pageNumber);
}
