package tv.snews.anews.service;

import tv.snews.anews.domain.UserFile;
import tv.snews.anews.exception.DataNotFoundException;
import tv.snews.anews.exception.SessionManagerException;

import java.io.IOException;

/**
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface UserFileService {

	UserFile saveUserFile(UserFile file) throws IOException, SessionManagerException, DataNotFoundException;
	
	void deleteUserFile(UserFile file) throws IOException;
}
