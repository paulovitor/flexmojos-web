
package tv.snews.anews.service;

import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Segment;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Fornece os serviços para manipulação dos institutos.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface InstitutionService {

	/**
	 * Faz o cadastro de um novo instituto informado pelo usuário. Se já existir
	 * um
	 * instituto com o mesmo Nome nao grava.
	 * 
	 * @param segment
	 */
	void save(Institution institution);

	/**
	 * Deleta o instituto.
	 * 
	 * @param institution
	 */
	void delete(Integer id) throws IllegalOperationException;

	/**
	 * Retorna a lista de todos os @{link Institution} cadastrados em ordem
	 * alfabética.
	 * 
	 * @return Lista de institutos cadastrados.
	 */
	List<Institution> listAll();
	
	/**
	 * Retorna a lista de @{link Institution} por id do segmento.
	 * 
	 * 
	 * @return Lista de institutos cadastrados por segmento.
	 */
	List<Institution> listBySegment(Segment segment);

}
