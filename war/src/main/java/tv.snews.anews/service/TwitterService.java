package tv.snews.anews.service;

import tv.snews.anews.domain.TweetInfo;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.infra.social.TwitterException;

import java.util.List;


/**
 * Estabele um contrato para manipulação da API do Twitter no sistema.
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public interface TwitterService {
	
	/**
	 * Obtem a lista de twitterUser.
	 * Não é responsabilidade deste método carregar os tweets de cada twitteruser.
	 *
	 * @return lista de TwitterUser
	 */
	List<TwitterUser> listAllTwitterUser();

	/**
	 * Atualiza a lista de tweets de todos os twitterUsers armazenados na base de dados.
	 * A verificação para atualizar os tweets é baseado nos últimos carregados da API do Twitter, 
	 * comparando-os com os já carregados anteriormente, caso existam.
	 * Se houver alteração nos tweets, esse método também é responsáel por avisar o cliente sobre 
	 * as mudanças ocorridas.
	 * 
	 * @throws TwitterException Se houver alguma falha de comunicação com a API do Twitter 
	 */
	void refreshTweetsFromTwitterUsers() throws TwitterException;

	/**
	 * Verifica se existe o usuário na API do twitter pelo screenName.
	 * Se o usuário existir, carrega os dados dele.
	 *  
     * @param screenName
     * 		ex: 'fzapmello' 
     * 		(twitter.com/(screenName)
     * @return
	 * @throws TwitterException Possíveis exception ao buscar recursos da API
	 * @throws tv.snews.anews.exception.ValidationException
     */
    TwitterUser checkTwitterUser(String screenName) throws TwitterException, ValidationException;
    
    /**
	 * Salva ou atualiza um twitter user.
	 *  
     * @param twitterUser
     * @return
     * @throws TwitterException 
     */
    void save(TwitterUser twitterUser) throws TwitterException;
    
    /**
	 * Remove um twitterUser e seus tweets.
	 *  
     * @param id
     * @return
     */
    void delete(TwitterUser twitterUser);
    
    /**
	 * Busca um usuário do twitter pelo ScreenName.
	 *  
     * @param screenName
     * @return objeto TwitterUser ou null se não encontrar
     */
    TwitterUser findTwitterUserByScreenName(String screenName);
    
    
    /**
     * Permite carregar de forma páginada os tweets de todos os twitter users
	 * cadastrados.
	 * 
	 * @param start Posição do primeiro registro a ser retornado.
	 * @param count Quantidade de registros a serem retornados.
	 * @return Lista de registros encontrados.
	 */
	List<TweetInfo> listTweetsInfoFromAllTweetUsers(int start, int count);
	
	/**
	 * Carrega de forma paginada os tweets de um usuário específico.
	 *  
     * @param twitterUser
     * @param start Posição do primeiro registro a ser retornado.
	 * @param count Quantidade de registros a serem retornados.
	 * @return Lista de registros encontrados.
     */
    List<TweetInfo> listTweetsInfoFromTwitterUser(TwitterUser twitterUser, int start, int count);
    
	/**
	 * Retorna o total de tweets atualmente armazenados pelo sistema, de todos os
	 * twitter users.
	 * 
	 * @return Quantidade encontrada.
	 */
	int countTweetInfoFromAllTwitterUsers();

	/**
	 * Retorna o total de tweets atualmente armazenados pelo sistema, de um
	 * twitter user.
	 * 
     * @param twitterUser
     * @return
     */
    int countTweetsFromTwitterUser(TwitterUser twitterUser);

	/**
	 * Retorna o tweetinfo pelo identificador
	 * 
     * @param idEntity
     * @return TweetInfo
     */
    TweetInfo findTweetInfoById(Long idEntity);

}