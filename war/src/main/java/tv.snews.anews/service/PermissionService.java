package tv.snews.anews.service;

import tv.snews.anews.domain.Permission;

/**
 * Fornece os serviços para manipulação das permissões.
 * 
 * @author Samuel Guedes de Melo.
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface PermissionService {
	Permission listAll();
}
