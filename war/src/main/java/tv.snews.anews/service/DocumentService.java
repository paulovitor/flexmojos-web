package tv.snews.anews.service;

import tv.snews.anews.domain.Document;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.DocumentParams;


/**
 * Fornece os serviços para manipulação dos laudas.
 * 
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface DocumentService {
	
    PageResult<Document<?>> fullTextSearch(DocumentParams searchParams, boolean drawerSearch);

    PageResult<Document<?>> findByParams(DocumentParams params, boolean drawerSearch);

    public String archiveGuidelineToDrawer(Long guidelineId, Integer programId, boolean toAdd);

    public String archiveReportageToDrawer(Long reportageId, Integer programId, boolean toAdd);

    public String archiveScriptToDrawer(Long scriptId, Integer programId, boolean toAdd);

    public String archiveStoryToDrawer(Long storyId, Integer programId, boolean toAdd);

    /**
     *
     * @param idsOfStories
     * @param rundownId
     * @param toAdd flag que serve para identificar se é para colocar ou remover um documento da gaveta de algum programa
     */
    public void archiveStoriesToDrawer(Long[] idsOfStories, Long rundownId, boolean toAdd);

    public void archiveScriptsToDrawer(Long[] idsOfScripts, Long scriptPlanId, boolean toAdd);
}
