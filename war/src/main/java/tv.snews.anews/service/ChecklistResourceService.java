package tv.snews.anews.service;

import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Define o contrato do serviço que manipula objetos {@link tv.snews.anews.domain.ChecklistResource}.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public interface ChecklistResourceService {

	/**
	 * Busca e retorna a lista de todos os recursos cadastradas 
	 * pelos usuários do sistema.
	 * 
	 * @return as recursos cadastrados
	 */
	List<ChecklistResource> listAll();
	
	/**
	 * Cadastra ou atualiza os dados de um recurso informados pelo usuário.
	 * 
	 * @param checklistResource dados do recurso
	 */
	void save(ChecklistResource checklistResource);
	
	/**
	 * Apaga no banco de dados os dados de um recurso cadastrado.
	 * 
	 * @param id identificador do registro do recurso
	 * @throws IllegalOperationException se o recurso estiver em uso por alguma lista de verificação.
	 */
	void remove(Integer id) throws IllegalOperationException;
	
	/**
	 * Verifica se um recurso está associado a alguma lista de verificação.
	 * 
	 * @param id identificador do registro do recurso
	 * @return {@code true} se o recurso estiver associado a alguma lista de verificação
	 */
	boolean isInUse(Integer id);
}
