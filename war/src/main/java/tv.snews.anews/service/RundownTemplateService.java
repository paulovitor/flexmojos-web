package tv.snews.anews.service;

import tv.snews.anews.domain.BlockTemplate;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownTemplate;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public interface RundownTemplateService {

    RundownTemplate load(Integer rundownId);

    List<RundownTemplate> findAllByProgram(Program program);

    boolean nameAvailable(String name, Program program);

    RundownTemplate createTemplate(String name, Program program);

    RundownTemplate createCopy(Integer originalId, String copyName, Program program);

    void updateTemplate(RundownTemplate template);

    void removeTemplate(RundownTemplate template);

    BlockTemplate createBlock(Integer rundownId);

    BlockTemplate createBlock(RundownTemplate rundownTemplate);

    void updateBlock(BlockTemplate blockTemplate);

    void removeBlock(Integer blockId);

    void lockEdition(RundownTemplate template) throws IllegalOperationException;

    void unlockAllEditions();
}
