package tv.snews.anews.service;

import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;

import java.util.List;

/**
 * Serviço responsável pelas operações relacionadas à classe de domínio.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssCategoryService {

	/**
	 * Faz o cadastro de uma nova categoria no banco de dados.
	 * 
	 * @param category Dados da nova categoria.
	 * @return ID gerado para o registro da categoria.
	 * @throws ValidationException Caso a categoria possua dados inválidos.
	 */
	Integer insert(RssCategory category) throws ValidationException;
	
	/**
	 * Faz a atualização dos dados de uma categoria existente no banco de dados.
	 * 
	 * @param category Novos dados da categoria.
	 * @throws ValidationException 
	 */
	void update(RssCategory category) throws ValidationException;
	
	/**
	 * Faz a exclusão de uma categoria que não possua feeds.
	 * 
	 * @param category Categoria que será apagada.
	 * @throws IllegalOperationException Caso a categoria contenha feeds.
	 */
	void remove(RssCategory category) throws IllegalOperationException;
	
	/**
	 * Retorna a lista de todas as categorias cadastradas.
	 * 
	 * @return Lista de categorias.
	 */
	List<RssCategory> listAll();
}
