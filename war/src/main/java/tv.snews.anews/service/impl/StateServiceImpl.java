package tv.snews.anews.service.impl;

import tv.snews.anews.dao.StateDao;
import tv.snews.anews.domain.State;
import tv.snews.anews.service.StateService;

import java.io.Serializable;
import java.util.List;

/**
 * Implementação do Serviço de manipulação dos estados.
 *
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class StateServiceImpl implements StateService {

	private StateDao stateDao;

	@Override
	public List<State> listAll() {
		return stateDao.listAll();
	}

    @Override
    public State get(Serializable idState) {
	    return stateDao.get(idState);
    }	
	
	@Override
	public void save(State state) {
		if (state != null && state.getId() != null && state.getId()  > 0) {
			stateDao.update(state);
		} else {
			stateDao.save(state);
		}
	}

	@Override
	public void remove(State state) {
		if (state != null && state.getId() != null && state.getId()  > 0) {
			stateDao.delete(state);
		}
	}

	//----------------------------------
	//	Setters
	//----------------------------------

    public void setStateDao(StateDao stateDao) {
    	this.stateDao = stateDao;
    }
}
