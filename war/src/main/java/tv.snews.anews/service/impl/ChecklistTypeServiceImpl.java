package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ChecklistDao;
import tv.snews.anews.dao.ChecklistTypeDao;
import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.ChecklistTypeMessenger;
import tv.snews.anews.service.ChecklistTypeService;

import java.util.List;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class ChecklistTypeServiceImpl implements ChecklistTypeService {

	private ChecklistDao checklistDao;
	private ChecklistTypeDao checklistTypeDao;
	private ChecklistTypeMessenger checklistTypeMessenger;
	
	@Override
	public List<ChecklistType> listAll() {
		return checklistTypeDao.findAll();
	}

	@Override
	public void save(ChecklistType type) {
		if (notValidId(type.getId())) {
			checklistTypeDao.save(type);
			checklistTypeMessenger.notifyInsert(type);
		} else {
			checklistTypeDao.update(type);
			checklistTypeMessenger.notifyUpdate(type);
		}
	}

	@Override
	public void remove(Integer id) throws IllegalOperationException {
		if (isInUse(id)) {
			throw new IllegalOperationException("Cannot delete a Checklist type that's in use.");
		}
		ChecklistType type = checklistTypeDao.get(id);
		checklistTypeDao.delete(type);
		checklistTypeMessenger.notifyDelete(type);
	}

	@Override
	public boolean isInUse(Integer id) {
		ChecklistType type = checklistTypeDao.get(id);
		return checklistDao.countAllByType(type) != 0;
	}
	
	//------------------------------------
	//  Setters
	//------------------------------------

    public void setChecklistDao(ChecklistDao ChecklistDao) {
	    this.checklistDao = ChecklistDao;
    }
	
    public void setChecklistTypeDao(ChecklistTypeDao ChecklistTypeDao) {
	    this.checklistTypeDao = ChecklistTypeDao;
    }
    
    public void setChecklistTypeMessenger(ChecklistTypeMessenger ChecklistTypeMessenger) {
	    this.checklistTypeMessenger = ChecklistTypeMessenger;
    }
}
