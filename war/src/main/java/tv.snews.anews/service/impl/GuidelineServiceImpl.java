package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.infra.mail.MailSender;
import tv.snews.anews.messenger.GuidelineMessenger;
import tv.snews.anews.messenger.InfrastructureMessenger;
import tv.snews.anews.search.GuidelineParams;
import tv.snews.anews.service.ChecklistService;
import tv.snews.anews.service.ContactGroupService;
import tv.snews.anews.service.ContactService;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.anews.util.StringDiffUtils;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * Implementação do serviço de manipulação de pauta.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class GuidelineServiceImpl implements GuidelineService {

	private static final Logger log = LoggerFactory.getLogger(GuidelineServiceImpl.class);

	private final EntityLocker<Guideline> locker = EntityLockerFactory.getLocker(Guideline.class);
	private final EntityLocker<Checklist> checklistLocker = EntityLockerFactory.getLocker(Checklist.class);

	private InfrastructureMessenger infrastructureMessenger;
	private GuidelineMessenger guidelineMessenger;

	private MailSender mailSender;
	private SessionManager sessionManager;

	private ContactService contactService;
    private ChecklistService checklistService;
	private ContactGroupService contactGroupService;

	private GuidelineDao guidelineDao;
	private GuidelineFullTextDao guidelineFTSDao;
	private GuidelineCopyLogDao guidelineCopyLogDao;

	private ContactDao contactDao;
	private GuideDao guideDao;
	private SettingsDao settingsDao;
	private StoryDao storyDao;
	private ProgramDao programDao;
	private UserDao userDao;
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	private final StringDiffUtils diffUtils = new StringDiffUtils();

	@Override
	public Guideline loadById(Long guidelineId) {
		return markLock(guidelineDao.get(guidelineId));
	}

	@Override
	public Guideline save(Guideline guideline) throws IllegalOperationException {
		// Salva novos contatos dos roteiros
		for (Guide guide : guideline.getGuides()) {
			for (Contact contact : guide.getContacts()) {
				if (notValidId(contact.getId())) {
					contactService.save(contact);
				}
			}
		}

		if (notValidId(guideline.getId())) {
			guidelineDao.save(guideline, sessionManager.getCurrentUser());

			// Criando a partir de uma notícia não abre o form, então não deve bloquear
			if (guideline.getNews() == null) {
 				locker.lock(guideline, guideline.getAuthor());
				guideline.setEditingUser(guideline.getAuthor());
			}

			if (guideline.getChecklist() != null) {
				checklistService.save(guideline.getChecklist(), false);
			}

			guidelineMessenger.notifyInsert(guideline);
		} else {
			Guideline dbGuideline = guidelineDao.get(guideline.getId());
			guidelineDao.evict(dbGuideline);

			correctContactsOnGuideline(dbGuideline, guideline);

			dbGuideline.updateFields(guideline);

			dbGuideline = guidelineDao.merge(dbGuideline, sessionManager.getCurrentUser());
			guidelineMessenger.notifyUpdate(dbGuideline);

			if (guideline.getChecklist() != null) {
				checklistService.save(guideline.getChecklist(), false);
			}
		}

		return guideline;
	}

	@Override
	public void registerProgram(Guideline guideline, Program program) {
        guideline.getPrograms().add(program);
        guidelineDao.update(guideline);
        guidelineMessenger.notifyFieldUpdate(guideline, "programs", guideline.getPrograms());
    }

	@Override
	public void unregisterProgram(Guideline guideline, Program program) {
        guideline.getPrograms().remove(program);
        guidelineDao.update(guideline);
        guidelineMessenger.notifyFieldUpdate(guideline, "programs", guideline.getPrograms());
    }

	@Override
	public void updateState(Long guidelineId, DocumentState state, String reason) {
		Guideline guideline = guidelineDao.get(guidelineId);
		guideline.setState(state);

		if (reason != null) {
			guideline.setStatusReason(reason);
		}

		guidelineDao.update(guideline);
		guidelineMessenger.notifyUpdate(guideline);
	}

	@Override
	public void copy(Date date, Integer programId, Long guidelineId) throws ValidationException, SessionManagerException, IllegalOperationException {
		Guideline guideline = guidelineDao.get(guidelineId);
		guidelineDao.evict(guideline);

        User currentUser = sessionManager.getCurrentUser();

        Guideline copy = guideline.createCopy(currentUser);
        copy.setDate(date);
        if(copy.getChecklist() != null) copy.getChecklist().setDate(date);

        if (programId == -1) {
        	copy.setProgram(null);
        } else {
        	copy.setProgram(programDao.get(programId));	
        }

		save(copy);

        // O save faz o lock, então retira o lock
        locker.unlock(copy, currentUser);
	}

    @Override
    public void copyRemoteGuideline(Guideline remoteGuideline, Date destinyDate, Program destinyProgram, User producer) throws IllegalOperationException {
        User currentUser = sessionManager.getCurrentUser();

        Guideline copy = remoteGuideline.createCopy(currentUser);
        // Não setar o autor no metodo acima, pois, esse metodo e o mesmo usado para copias locais
        // onde o autor deve permanecer o mesmo
        copy.setAuthor(currentUser);
        copy.setDate(destinyDate);
        copy.setChangeDate(new Date());
        copy.setProgram(destinyProgram);

        // Limpara os producers e adiciona o selecionado
        copy.getProducers().clear();
        copy.addProducer(producer);

        copy.getEditors().clear();

        // Limpa propriedades incompatíveis
        copy.getReporters().clear();
        copy.setNews(null);
        copy.getRevisions().clear();
        copy.getDrawerPrograms().clear();
        copy.getPrograms().clear();

        copy.setTeam(null);
        copy.setState(DocumentState.PRODUCING);

        if(copy.getChecklist() != null){
            copy.getChecklist().getRevisions().clear();
            copy.getChecklist().getResources().clear();
            copy.getChecklist().setType(null);
            copy.getChecklist().setProducer(null);
            copy.getChecklist().setAuthor(currentUser);
        }

        for (Guide guide : copy.getGuides()) {

            // Substitui contatos da agenda duplicados na cópia
            Set<AgendaContact> replacements = new HashSet<>();

            for (Iterator<Contact> it = guide.getContacts().iterator(); it.hasNext();) {
                Contact contact = it.next();

                // AgendaContact.createCopy() não cria uma cópia, apenas retorna a
                // mesma instância. Por isso o código abaixo cria uma cópia de verdade.
                if (contact instanceof AgendaContact) {
                    AgendaContact agendaContact = (AgendaContact) contact;

                    AgendaContact match = contactDao.findMatch(agendaContact);
                    if (match != null) {
                        it.remove();
                        replacements.add(match);
                        continue;
                    }

                    AgendaContact agendaCopy = new AgendaContact();

                    agendaCopy.setName(agendaContact.getName());
                    agendaCopy.setProfession(agendaContact.getProfession());
                    agendaCopy.setAddress(agendaContact.getAddress());
                    agendaCopy.setCompany(agendaContact.getCompany());
                    agendaCopy.setEmail(agendaContact.getEmail());
                    agendaCopy.setExcluded(agendaContact.isExcluded());
                    agendaCopy.setCity(agendaContact.getCity());

                    // Substitui o grupo do contato por um idêntico da base local
                    if (agendaContact.getContactGroup() != null) {
                        ContactGroup localGroup = contactGroupService.findOrCreateByName(agendaContact.getContactGroup().getName());
                        agendaCopy.setContactGroup(localGroup);
                    }

                    for (ContactNumber number : agendaContact.getNumbers()) {
                        agendaCopy.getNumbers().add(number.createCopy());
                    }

                    // Remove a armazena a cópia para ser adicionada depois
                    it.remove();
                    replacements.add(agendaCopy);
                }
            }

            guide.getContacts().addAll(replacements);
        }

        save(copy);

        // O save faz o lock, então retira o lock
        locker.unlock(copy, currentUser);
    }
    
    @Override
    public Guideline remoteCopyRequest(Long guidelineId, String nickname, String company) {
    	Guideline guideline = guidelineDao.get(guidelineId);

    	GuidelineCopyLog copyLog = new GuidelineCopyLog(guideline, nickname, company);
        guideline.getCopiesHistory().add(copyLog);

        guidelineCopyLogDao.save(copyLog);
        guidelineMessenger.notifyUpdate(guideline);

        return guideline;
    }

    @Override
	public void remove(Long guidelineId) throws ValidationException, SessionManagerException {
		Objects.requireNonNull(guidelineId, bundle.getMessage("guideline.nullGuideline"));
		Guideline guideline = guidelineDao.get(guidelineId);

		if (existStoryForGuideline(guideline)) {
			String errorMessage = bundle.getMessage("guideline.existStory");
			log.error(errorMessage);
			throw new ValidationException(errorMessage);
		}

		guideline.setExcluded(true);

		guidelineDao.update(guideline, sessionManager.getCurrentUser());
		guidelineMessenger.notifyDelete(guideline);
	}

	//Esta classe não deve ser serializada. Usa-se o guideline.
	@Override
	public List<Guideline> listAllGuidelineOfDay(Date currentDay, Integer programId, DocumentState state) throws ValidationException {
		Objects.requireNonNull(currentDay, bundle.getMessage("defaults.invalidDate", currentDay));

		Program program = null; // Se continuar null indica "todos os programas"

		if (programId != null) {
			if (programId > 0) {
				program = programDao.get(programId);
			} else if (programId == -1) { //GAVETA GERAL
				program = new Program();
				program.setId(programId);
			}
		}

		List<Guideline> guidelines = guidelineDao.findByDate(currentDay, program, state);
		return markLocks(guidelines);
	}

	@Override
	public PageResult<Guideline> fullTextSearch(GuidelineParams searchParams) {
		int total = guidelineFTSDao.total(searchParams);
		List<Guideline> guidelines = guidelineFTSDao.listByPage(searchParams);
		return new PageResult<>(searchParams.getPage(), total, guidelines);
	}

	@Override
	public User isLockedForEdition(Long guidelineId) {
		Guideline guideline = guidelineDao.get(guidelineId);	
		return locker.findOwner(guideline);
	}

	@Override
	public void lockEdition(Long guidelineId, Integer userId) throws IllegalOperationException {
		Guideline guideline = guidelineDao.get(guidelineId);
		User user = userDao.get(userId);
		locker.lock(guideline, user);
	}

	@Override
	public void unlockEdition(Long guidelineId) throws SessionManagerException, IllegalOperationException {
		Guideline guideline = guidelineDao.get(guidelineId);
		locker.unlock(guideline, sessionManager.getCurrentUser());
	}

	@Override
	public void restoreExcludedGuideline(Long guidelineId, Integer programToCopyId, Date guidelineToCopyDate) throws SessionManagerException {
		Guideline guideline = guidelineDao.get(guidelineId);

		if (programToCopyId == -1) {
			guideline.setProgram(null);
		} else {
			guideline.setProgram(programDao.get(programToCopyId));
		}

		guideline.setDate(guidelineToCopyDate);

		guideline.setExcluded(false);

		guidelineDao.update(guideline, sessionManager.getCurrentUser());
		guidelineMessenger.notifyRestore(guideline);
	}

	@Override
	public int countExcludedGuidelines() {
		return guidelineDao.countExcluded();
	}

	@Override
	public List<Guideline> listExcludedGuidelines(int firstResult, int maxResults) {
		return correctGuidelines(guidelineDao.findExcluded(firstResult, maxResults));
	}

	@Override
	public PageResult<Guideline> listExcludedGuidelinesPage(int pageNumber) {
		List<Guideline> guidelines = correctGuidelines(guidelineDao.findExcludedByPage(pageNumber));
		return new PageResult<>(pageNumber, guidelineDao.countExcluded(), guidelines);
	}

	@Override
	public Collection<GuidelineRevision> loadChangesHistory(Long id) {
		return guidelineDao.get(id).changesHistory();
	}

	@Override
	public GuidelineRevision loadRevision(Long guidelineId, int revisionNumber, boolean showDiff) {
		Guideline guideline = guidelineDao.get(guidelineId);
		GuidelineRevision current = guideline.currentRevision();
		GuidelineRevision revision = guideline.getRevisionByNumber(revisionNumber);
		
		if (showDiff && current.getRevisionNumber() != revisionNumber) {
			return current.computeDiff(revision);
		} else {
			return revision;
		}
	}
	
	@Override
	public Guideline compareDiffBetweenGuidelines(Guideline guideline1, Guideline guideline2) {
		guideline2.setSlug(diffUtils.prettyHtmlDiff(guideline1.getSlug(), guideline2.getSlug(), settingsDao.loadSettings().isEditorUpperCase()));
		guideline2.setProposal(diffUtils.prettyHtmlDiff(guideline1.getProposal(), guideline2.getProposal(), settingsDao.loadSettings().isEditorUpperCase()));
		if (guideline2.getInformations() != null && guideline1.getInformations() != null) {
			guideline2.setInformations(diffUtils.prettyHtmlDiff(guideline1.getInformations(), guideline2.getInformations(), settingsDao.loadSettings()
			                .isEditorUpperCase()));
		}
		if (guideline2.getReferral() != null && guideline1.getReferral() != null) {
			guideline2.setReferral(diffUtils.prettyHtmlDiff(guideline1.getReferral(), guideline2.getReferral(), settingsDao.loadSettings().isEditorUpperCase()));
		}
		return guideline2;
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

	private boolean existStoryForGuideline(Guideline guideline) {
		return storyDao.listByGuideline(guideline).size() > 0;
	}

	/**
	 * Verifica se os campos obrigatório da pauta foram preenchidos. Verifica
	 * também os campos dos roteiros, se existirem. Campos obrigatórios da
	 * pauta: retranca, autor, proposta, data de modificação, produtor, repórter
	 * e programa. Campos obrigatórios dos roteiros: horário de execução
	 */
	private void validateFields(Guideline guideline) throws ValidationException {
		StringBuilder validationErrors = new StringBuilder();

		if (StringUtils.isBlank(guideline.getSlug())) {
			validationErrors.append(bundle.getMessage("guideline.slugRequired")).append("\n");
		}
		if (StringUtils.isBlank(guideline.getProposal())) {
			validationErrors.append(bundle.getMessage("guideline.proposalRequired")).append("\n");
		}
		if (guideline.getChangeDate() == null) {
			validationErrors.append(bundle.getMessage("guideline.modifiedRequired")).append("\n");
		}
		if (!AbstractEntity.isValid(guideline.getAuthor())) {
			validationErrors.append(bundle.getMessage("guideline.authorRequired")).append("\n");
		}
		for (Guide guide : guideline.getGuides()) {
			if (guide.getSchedule() == null) {
				validationErrors.append(bundle.getMessage("guideline.guideScheduleRequired")).append("\n");
			}
		}

		String validationMessage = validationErrors.toString().trim();

		if (validationMessage.length() != 0) {
			log.error(validationMessage);
			throw new ValidationException(validationMessage);
		}
	}

	@Override
	public void sendGuidelineMail(Long guidelineId) throws ValidationException, SessionManagerException {
		Guideline guideline = guidelineDao.get(guidelineId);

		Objects.requireNonNull(guideline, bundle.getMessage("guideline.nullGuideline"));

        if (guideline.getReporters().isEmpty()) {
            throw new NullPointerException(bundle.getMessage("guideline.noReporterToSend"));
        }

		String title = DateFormat.getInstance().format(guideline.getDate()) + " - " + (guideline.getProgram() == null ? bundle.getMessage("defaults.globalDrawer") :  guideline.getProgram().getName()) + "<br/>";

		StringBuilder content = new StringBuilder();

		//Html presentation for Title
		content.append("<font size='3' color='#B2B2B2' style='float:left;'>");
		content.append(title);
		content.append("</font>");

		//Html for producer
        if (!guideline.getProducers().isEmpty()) {
		content.append("<font size='3' color='#B2B2B2' style='float:right;'>");
		content.append(bundle.getMessage("guideline.producedBy"));
		content.append(" ");
            content.append(guideline.producersToString());
		content.append("</font>");
        }

		SimpleDateFormat a = new SimpleDateFormat(bundle.getMessage("defaults.dateTimeFormat"));

		//Html data versão.
		content.append("<font size='3' color='#B2B2B2' style='float:left;'>");
		content.append(bundle.getMessage("guideline.revision"));
		content.append(" ");
		content.append(a.format(guideline.getChangeDate()));
		content.append("</font>");

		//Html for slug
		content.append("<div style='clear:both;'>&nbsp;</div>");
		content.append("<hr color='black'>");
		content.append("<font size='6'>");
		content.append(guideline.getSlug());
		content.append("</font>");
		content.append("<br/>");

		//Html for reporter
		content.append("<font size='2' color='#B2B2B2'>");
		content.append(bundle.getMessage("guideline.reporters"));
		content.append(" ");
		content.append(guideline.reportersToString());
		content.append("</font>");
		content.append("<br/>");

		//Html for proposal
		content.append("<hr color='black'>");
		content.append("<b>");
		content.append("<font size='2' color='#B2B2B2'>");
		content.append("(");
		content.append(bundle.getMessage("guideline.proposal"));
		content.append(") ");
		content.append("</font>");
		content.append("<font size='2'  align='justify'>");
		content.append(guideline.getProposal());
		content.append("</font>");
		content.append("</b>");
		content.append("<br/>");

		//Html for referral
		String encaminhamento = guideline.getReferral() == null ? "" : guideline.getReferral();
		content.append("<hr color='black'>");
		content.append("<b>");
		content.append("<font size='2' color='#B2B2B2'>");
		content.append("(");
		content.append(bundle.getMessage("guideline.referral"));
		content.append(") ");
		content.append("</font>");
		content.append("<font size='2' align='justify'>");
		content.append(encaminhamento);
		content.append("</font>");
		content.append("</b>");
		content.append("<br/>");

		//Html for informations
		String informations = guideline.getInformations() == null ? "" : guideline.getInformations();
		content.append("<hr color='black'>");
		content.append("<b>");
		content.append("<font size='2' color='#B2B2B2'>");
		content.append("(");
		content.append(bundle.getMessage("guideline.informations"));
		content.append(") ");
		content.append("</font>");
		content.append("<font size='2' align='justify'>");
		content.append(informations);
		content.append("</font>");
		content.append("</b>");
		content.append("</div>");
		content.append(getMessageBundleGuideWithContacts(guideline));
		content.append("</div>");

		try {
            for (User reporter : guideline.getReporters()) {
			mailSender.send(
                        reporter.getEmail(),
			    bundle.getMessage("guideline.mailSubject"),
			    content.toString()
			                );
            }
		} catch (MessageDeliveryException e) {
			log.warn("Could not send the email with the guideline data.", e);
			infrastructureMessenger.notifyError(bundle.getMessage("guideline.mailFailed"), sessionManager.getCurrentUser());
		}
	}
	
	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

	private Guideline markLock(Guideline guideline) {
		guideline.setEditingUser(locker.findOwner(guideline));

		Checklist checklist = guideline.getChecklist();
		if (checklist != null) {
			checklist.setEditingUser(checklistLocker.findOwner(checklist));
		}

		return guideline;
	}

	private List<Guideline> markLocks(List<Guideline> guidelines) {
		for (Guideline guideline : guidelines) {
			markLock(guideline);
		}
		return guidelines;
	}

	/**
	 * Retorna uma lista de guideline com slug de news se a guideline foi criada
	 * pela uma news.
	 * 
	 * @param guidelineList de guideline a ser verificada.
	 * @return List<Guideline>.
	 */
	private List<Guideline> correctGuidelines(List<Guideline> guidelineList) {
		for (Guideline guideline : guidelineList) {
			guideline.getGuides().size();
		}
		return guidelineList;
	}

	/**
	 * Corrige os contatos dos roteiros da pauta. Faz o controle dos contatos internos dos roteiros
	 * quando alterar, deleta ou modifica os roteiros.
	 *
	 * @param current
	 * @param newData
	 */
	private void correctContactsOnGuideline(Guideline current, Guideline newData) {
		for (Guide currentGuide : current.getGuides()) {
			if (newData.getGuides().contains(currentGuide)) {
				// Verifica se algum contato específico desta pauta foi apagado
				for (Iterator<Contact> it = currentGuide.getContacts().iterator(); it.hasNext(); ) {
					Contact oldContact = it.next();
					if (!(oldContact instanceof AgendaContact)) {
						if (!newData.containsContact(oldContact)) {
							it.remove();
							contactService.delete(oldContact);
						}
					}
				}
			} else {
				// Apaga os contatos especificos do guide, da agenda não!
				for (Iterator<Contact> it = currentGuide.getContacts().iterator(); it.hasNext(); ) {
					Contact oldContact = it.next();
					if (!(oldContact instanceof AgendaContact)) {
						it.remove();
						contactService.delete(oldContact);
					}
				}
				guideDao.delete(currentGuide);
			}
		}
	}

	/*
	 * Método que retorna os valores de roteiro com os contatos para o envio do
	 * email.
	 */
	private String getMessageBundleGuideWithContacts(Guideline guideline) {
		Format formatter = new SimpleDateFormat("hh:mm", Locale.getDefault());

		StringBuilder guides = new StringBuilder();

		int i = 0;
		for (Guide guide : guideline.getGuides()) {
			StringBuilder markers = new StringBuilder();

			int j = 0;
			for (Contact contact : guide.getContacts()) {
				ContactNumber firstNumber = contact.getFirstNumber();
				markers.append("<font size='2' color='#B2B2B2'>");
				markers.append("(");
				markers.append(bundle.getMessage("guideline.markers"));
				markers.append(")");
				markers.append("</font>");
				markers.append("<br/>");
				markers.append("<b>");
				markers.append("<font size='2'>");
				markers.append(String.valueOf(++j));
				markers.append(" - ");
				markers.append(contact.getName());
				markers.append(" (");
				markers.append(firstNumber == null ? "" : contact.getFirstNumber().getValue());
				markers.append(")");

				if (contact instanceof AgendaContact) {
					AgendaContact agendaContact = (AgendaContact) contact;
					markers.append(" - ");
					markers.append(agendaContact.getEmail() == null ? "" : agendaContact.getEmail());
				}

				markers.append("</font>");
				markers.append("</b>");
				markers.append("<br/>");
			}

			guides.append("<hr  color='#B2B2B2'>");
			guides.append("<font size='2'  color='#B2B2B2'>");
			guides.append(bundle.getMessage("guideline.guide"));
			guides.append(" ");
			guides.append(String.valueOf(++i));
			guides.append(" (");
			guides.append(formatter.format(guide.getSchedule()));
			guides.append(") ");
			guides.append("</font>");
			guides.append("<hr color='#B2B2B2'>");
			guides.append(" ");
			guides.append(markers.toString());
			guides.append("<b>");
			guides.append("<font size='2'>");

			if (guide.getAddress() != null) {
				guides.append("<font size='2' color='#B2B2B2'>");
				guides.append(" (");
				guides.append(bundle.getMessage("guideline.address"));
				guides.append(") ");
				guides.append("</font>");
				guides.append(guide.getAddress());
				if (guide.getReference() != null) {
					guides.append(" - ");
					guides.append(guide.getReference());
				}
				guides.append("<br/>");
			}

			if (guide.getCity() != null) {
				guides.append("<font size='2' color='#B2B2B2'>");
				guides.append(" (");
				guides.append(bundle.getMessage("guideline.city"));
				guides.append(") ");
				guides.append("</font>");
				guides.append(guide.getCity().getName()).append(" - ").append(guide.getCity().getState().getName());
				guides.append("<br/>");
			}

			if (guide.getObservation() != null) {
				guides.append("<font size='2' color='#B2B2B2'>");
				guides.append("(");
				guides.append(bundle.getMessage("guideline.observation"));
				guides.append(") ");
				guides.append("</font>");
				guides.append(guide.getObservation());
				guides.append("<br/>");
			}

			guides.append("</font>");
			guides.append("</b>");

		}

		return guides.toString();
	}

	//--------------------------------------------------------------------------
	//	Setters
	//--------------------------------------------------------------------------

	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public void setGuideDao(GuideDao guideDao) {
		this.guideDao = guideDao;
	}

	public void setGuidelineMessenger(GuidelineMessenger value) {
		this.guidelineMessenger = value;

		locker.removeAllEntityLockerListeners();
		locker.addEntityLockerListener(new EntityLockerListener<Guideline>() {

			@Override
			public void entityLocked(Guideline guideline, User user) {
				guidelineMessenger.notifyLock(guideline, user);
			}

			@Override
			public void entityUnlocked(Guideline guideline, User user) {
				guidelineMessenger.notifyUnlock(guideline, user);
			}
		});
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setInfrastructureMessenger(InfrastructureMessenger infrastructureMessenger) {
		this.infrastructureMessenger = infrastructureMessenger;
	}

	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}

	public void setStoryDao(StoryDao storyDao) {
		this.storyDao = storyDao;
	}

	public void setProgramDao(ProgramDao programDao) {
		this.programDao = programDao;
	}

	public void setContactService(ContactService contactService) {
		this.contactService = contactService;
	}

    public void setChecklistService(ChecklistService checklistService) {
        this.checklistService = checklistService;
    }

	public void setContactDao(ContactDao contactDao) {
		this.contactDao = contactDao;
	}

	public void setContactGroupService(ContactGroupService contactGroupService) {
		this.contactGroupService = contactGroupService;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setGuidelineFTSDao(GuidelineFullTextDao guidelineFTSDao) {
		this.guidelineFTSDao = guidelineFTSDao;
	}

	public void setGuidelineCopyLogDao(GuidelineCopyLogDao guidelineCopyLogDao) {
		this.guidelineCopyLogDao = guidelineCopyLogDao;
	}
}
