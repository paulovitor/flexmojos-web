package tv.snews.anews.service.impl;

import tv.snews.anews.dao.LogDao;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.LogService;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementação padrão da interface RundownLog 
 *
 * @author Felipe Zap de Mello
 * @since 1.2.6
 */
public class LogServiceImpl implements LogService {

	private RundownDao rundownDao;
	private LogDao logDao;
	
	@Override
    public PageResult<RundownLog> listLogsByRundownAction(Long rundownId, List<String> actions, int pageNumber) {
    	List<RundownAction> rundownActions = new ArrayList<RundownAction>();
    	for (String action : actions) {
    		rundownActions.add(RundownAction.valueOf(action));
        }
    	Rundown rundown = rundownDao.get(rundownId);
    	int total = logDao.totalRundown(rundown, rundownActions);
	    List<RundownLog> logs = logDao.listLogsByRundownAction(rundown, rundownActions, pageNumber);
	    return new PageResult<>(pageNumber, total, logs);
    }	

	@Override
    public PageResult<BlockLog> listLogsByBlockAction(Long rundownId, List<String> actions, int pageNumber) {
    	List<BlockAction> blockActions = new ArrayList<BlockAction>();
    	for (String action : actions) {
    		blockActions.add(BlockAction.valueOf(action));
        }
    	Rundown rundown = rundownDao.get(rundownId);
    	int total = logDao.totalBlock(rundown, blockActions);
	    List<BlockLog> logs = logDao.listLogsByBlockAction(rundown, blockActions, pageNumber);
	    return new PageResult<>(pageNumber, total, logs);
    }	
	
    @Override
    public PageResult<StoryLog> listLogsByStoryAction(Long rundownId, List<String> actions, int pageNumber) {
    	List<StoryAction> storyActions = new ArrayList<StoryAction>();
    	for (String action : actions) {
    		storyActions.add(StoryAction.valueOf(action));
        }
    	Rundown rundown = rundownDao.get(rundownId);
    	int total = logDao.totalStory(rundown, storyActions);
	    List<StoryLog> logs = logDao.listLogsByStoryAction(rundown, storyActions, pageNumber);
	    return new PageResult<>(pageNumber, total, logs);
    }	
    
    @Override
    public PageResult<AbstractLog> listAll(Long rundownId, int pageNumber) {
    	Rundown rundown = rundownDao.get(rundownId);
    	int total = logDao.totalAll(rundown);
	    return new PageResult<AbstractLog>(pageNumber, total, logDao.listAll(rundown));
    }
    
    public void setRundownDao(RundownDao rundownDao) {
    	this.rundownDao = rundownDao;
    }
	
	public void setLogDao(LogDao logDao) {
	    this.logDao = logDao;
    }
}
