package tv.snews.anews.service.impl;

import tv.snews.anews.dao.GuidelineClassificationDao;
import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.domain.GuidelineClassification;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.GuidelineClassificationMessenger;
import tv.snews.anews.service.GuidelineClassificationService;

import java.util.List;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * Implementação padrão da interface do serviço {@link GuidelineClassificationService}.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public class GuidelineClassificationServiceImpl implements GuidelineClassificationService {

	private GuidelineDao guidelineDao;
	private GuidelineClassificationDao guidelineClassificationDao;
	private GuidelineClassificationMessenger guidelineClassificationMessenger;

	@Override
	public List<GuidelineClassification> listAll() {
		return guidelineClassificationDao.findAll();
	}

	@Override
	public void save(GuidelineClassification classification) {
		if (notValidId(classification.getId())) {
			guidelineClassificationDao.save(classification);
			guidelineClassificationMessenger.notifyInsert(classification);
		} else {
			guidelineClassificationDao.update(classification);
			guidelineClassificationMessenger.notifyUpdate(classification);
		}
	}

	@Override
	public void remove(Integer id) throws IllegalOperationException {
		GuidelineClassification classification = guidelineClassificationDao.get(id);

		if (isInUse(classification)) {
			throw new IllegalOperationException("Cannot delete a report nature that's in use.");
		}

		guidelineClassificationDao.delete(classification);
		guidelineClassificationMessenger.notifyDelete(classification);
	}

	@Override
	public boolean isInUse(Integer id) {
		GuidelineClassification classification = guidelineClassificationDao.get(id);
		return isInUse(classification);
	}

	@Override
	public boolean isInUse(GuidelineClassification classification) {
		return guidelineDao.countByClassification(classification) > 0;
	}

	//------------------------------------
	//  Setters
	//------------------------------------
	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public void setGuidelineClassificationDao(GuidelineClassificationDao guidelineClassificationDao) {
		this.guidelineClassificationDao = guidelineClassificationDao;
	}

	public void setGuidelineClassificationMessenger(GuidelineClassificationMessenger guidelineClassificationMessenger) {
		this.guidelineClassificationMessenger = guidelineClassificationMessenger;
	}

}
