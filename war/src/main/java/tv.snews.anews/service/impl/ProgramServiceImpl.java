package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.messenger.ProgramMessenger;
import tv.snews.anews.messenger.SettingsMessenger;
import tv.snews.anews.service.ProgramService;
import tv.snews.anews.util.ResourceBundle;

import java.util.*;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class ProgramServiceImpl implements ProgramService {

	private static final Logger log = LoggerFactory.getLogger(ProgramServiceImpl.class);
	
	private GuidelineDao guidelineDao;
	private ProgramDao programDao;
	private RundownDao rundownDao;
	private SettingsDao settingsDao;
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private ProgramMessenger programMessenger;
	private SettingsMessenger settingsMessenger;


	@Override
    public Program loadById(Integer idProgram) {
        return programDao.get(idProgram);
    }

	@Override
	public void save(Program program) throws ValidationException {		
		if (program.getId() > 0) {
			Program oldProgram = programDao.get(program.getId());
			programDao.evict(oldProgram);
			if (!oldProgram.getName().equals(program.getName())) {
				Settings settings =	settingsDao.loadSettings();
				settings.setDisplayIndexAlert(true);
				settingsDao.update(settings);
        		settingsMessenger.notifySettingsChanged(settings);
			}
			
			programDao.update(program);
			programMessenger.notifyUpdate(program);
		} else {
			if(programDao.getByName(program.getName()) != null) {
				throw new ValidationException(bundle.getMessage("program.msg.alreadyExistWithThatName"));
			}
			programDao.save(program);
			programMessenger.notifyInsert(program);
		}
	}

	@Override
	public void delete(Integer id) throws IllegalOperationException {
		Objects.requireNonNull(id, bundle.getMessage("program.msg.canNotBeNullIdOfProgram"));
		
		Program program = programDao.get(id);
		Objects.requireNonNull(id, bundle.getMessage("program.msg.canNotBeFoundProgramIdInformed"));
		
		if (guidelineDao.countByProgram(program) != 0 || rundownDao.countRundownsOfProgram(program) != 0) {
			String errorMessage = bundle.getMessage("program.msg.canNotBeDeleted");
			log.error(errorMessage);
			throw new IllegalOperationException(errorMessage);
		} else {
			programDao.delete(program);
			programMessenger.notifyDelete(program);
		}
	}

	@Override
	public List<Program> listAll() {
		return programDao.listAll();
	}
	
	@Override
	public List<Program> listAllOrderByStart() {
		return programDao.listAllOrderByStart();
	}

    @Override
	public List<Program> listAllWithDisableds(ProgramDao.FetchType... fetchTypes) {
		return programDao.listAllWithDisableds(fetchTypes);
	}

	@Override
	public Set<User> listPresenters(Integer programId) {
		Program program = programDao.get(programId, ProgramDao.FetchType.PRESENTERS);
		return program != null ? program.getPresenters() : null;
	}

	@Override
	public Set<User> listEditors(Integer programId) {
		Program program = programDao.get(programId, ProgramDao.FetchType.EDITORS);
		return program != null ? program.getEditors() : null;
	}

    @Override
    public SortedSet<User> listEditorsOfPrograms() {
        SortedSet<User> allEditors = new TreeSet<>();
        for (Program program : listAll()) {
            allEditors.addAll(programDao.get(program.getId(), ProgramDao.FetchType.EDITORS).getEditors());
        }
        return allEditors;
    }

    @Override
    public Set<User> listImageEditors(Integer programId) {
        Program program = programDao.get(programId, ProgramDao.FetchType.IMAGE_EDITORS);
        return program != null ? program.getImageEditors() : null;
    }

	//----------------------------------
	//	Setters
	//----------------------------------

	public void setProgramDao(ProgramDao programDao) {
		this.programDao = programDao;
	}

    public void setGuidelineDao(GuidelineDao guidelineDao) {
	    this.guidelineDao = guidelineDao;
    }

    public void setRundownDao(RundownDao rundownDao) {
	    this.rundownDao = rundownDao;
    }
    
	public void setProgramMessenger(ProgramMessenger programMessenger) {
		this.programMessenger = programMessenger;
	}
	
	public void setSettingsDao(SettingsDao settingsDao) {
	    this.settingsDao = settingsDao;
	}
	
    public void setSettingsMessenger(SettingsMessenger settingsMessenger) {
    	this.settingsMessenger = settingsMessenger;
    }
}
