package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ReportDao;
import tv.snews.anews.dao.ReportEventDao;
import tv.snews.anews.domain.ReportEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.ReportEventMessenger;
import tv.snews.anews.service.ReportEventService;

import java.util.List;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * Implementação padrão da interface do serviço {@link tv.snews.anews.service.ReportEventService}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportEventServiceImpl implements ReportEventService {

	private ReportDao reportDao;
	private ReportEventDao reportEventDao;
	private ReportEventMessenger reportEventMessenger;
	
	@Override
	public List<ReportEvent> listAll() {
		return reportEventDao.findAll();
	}

	@Override
	public void save(ReportEvent event) {
		if (notValidId(event.getId())) {
			reportEventDao.save(event);
			reportEventMessenger.notifyInsert(event);
		} else {
			reportEventDao.update(event);
			reportEventMessenger.notifyUpdate(event);
		}
	}

	@Override
	public void remove(Integer id) throws IllegalOperationException {
		if (isInUse(id)) {
			throw new IllegalOperationException("Cannot delete a report event that's in use.");
		}
		ReportEvent event = reportEventDao.get(id);
		reportEventDao.delete(event);
		reportEventMessenger.notifyDelete(event);
	}

	@Override
	public boolean isInUse(Integer id) {
		ReportEvent event = reportEventDao.get(id);
		return reportDao.countAllByEvent(event) != 0;
	}
	
	//------------------------------------
	//  Setters
	//------------------------------------

    public void setReportDao(ReportDao reportDao) {
	    this.reportDao = reportDao;
    }
	
    public void setReportEventDao(ReportEventDao reportEventDao) {
	    this.reportEventDao = reportEventDao;
    }
    
    public void setReportEventMessenger(ReportEventMessenger reportEventMessenger) {
	    this.reportEventMessenger = reportEventMessenger;
    }
}
