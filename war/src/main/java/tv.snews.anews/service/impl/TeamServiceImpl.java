package tv.snews.anews.service.impl;

import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.dao.TeamDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Team;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.TeamMessenger;
import tv.snews.anews.messenger.UserMessenger;
import tv.snews.anews.service.TeamService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TeamServiceImpl implements TeamService {
	private UserDao userDao;
	private TeamDao teamDao;
	private TeamMessenger teamMessenger;
    private UserMessenger userMessenger;
    private GuidelineDao guidelineDao;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	@Override
	public List<Team> listAll() {
		return teamDao.listAll();
	}
	
	@Override
	public void save(Team team) {
		if (team.getId() > 0) {
            teamDao.update(team);
            teamMessenger.notifyUpdate(team);
		} else {
            teamDao.save(team);
            teamMessenger.notifyInsert(team);
		}
        userMessenger.notifyUsersChanged();
	}

	@Override
	public void delete(Integer id) throws IllegalOperationException {
        Team team = teamDao.get(id);
		if (team != null) {
			if (checkTeamHasUsers(team)) {
				throw new IllegalOperationException(bundle.getMessage("guideline.team.teamHasUsersError"));
			} else {
				teamDao.delete(team);
                teamMessenger.notifyDelete(team);
			}
		}
	}

	public Team listTeamByName(String name) {
		return teamDao.listTeamByName(name);
	}

	/**
	 * Verifica se existe usuários vinculados ao grupo.
	 * 
     * @param team
     * @return Se existe usuários naquele grupo.
     */
    private boolean checkTeamHasUsers(Team team) {
    	return !userDao.listUsersByTeamAndStatus(team).isEmpty();
    }

    @Override
    public boolean isInUse(Integer id) {
        Team team = teamDao.get(id);
        return guidelineDao.countByTeam(team) > 0;
    }

	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setTeamDao(TeamDao teamDao) {
		this.teamDao = teamDao;
	}
    
	/**
     * @param userDao the teamDao to set
     */
    public void setUserDao(UserDao userDao) {
	    this.userDao = userDao;
    }
    
    public void setTeamMessenger(TeamMessenger teamMessenger) {
	    this.teamMessenger = teamMessenger;
    }

    public void setUserMessenger(UserMessenger userMessenger) {
        this.userMessenger = userMessenger;
    }

    public void setGuidelineDao(GuidelineDao guidelineDao) {
        this.guidelineDao = guidelineDao;
    }
}
