package tv.snews.anews.service.impl;

import tv.snews.anews.dao.DeviceDao;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.ScriptVideoDao;
import tv.snews.anews.dao.StoryMosVideoDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.infra.mos.MosConnectionFactory;
import tv.snews.anews.messenger.DeviceMessenger;
import tv.snews.anews.service.DeviceService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

import static tv.snews.anews.util.EntityUtil.validId;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class DeviceServiceImpl implements DeviceService {

	private DeviceDao deviceDao;
	private DeviceMessenger deviceMessenger;

	private ProgramDao programDao;

    private StoryMosVideoDao storyMosVideoDao;
    private ScriptVideoDao scriptVideoDao;

	private MosConnectionFactory mosConnFactory;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

    private final String CHANNELS_IN_USE = "CHANNELS_IN_USE";
    private final String CHANNELS_NOT_IN_USE = "CHANNELS_NOT_IN_USE";

	@Override
	public <T extends Device> T findById(Integer id) {
		return (T) deviceDao.get(id);
	}

	@Override
	public List<? extends Device> findAll() {
		return deviceDao.findAll();
	}

	@Override
	public List<? extends Device> findAllByProtocol(DeviceProtocol protocol) {
		switch (protocol) {
			case II:
				return deviceDao.findAllIIDevices();
			case MOS:
				return deviceDao.findAllMosDevices();
			case WS:
				return deviceDao.findAllWSDevices();
			default:
				throw new RuntimeException("Unknown device protocol: " + protocol);
		}
	}

	@Override
	public List<? extends Device> findAllByType(DeviceType type) {
		return deviceDao.findAllByType(type);
	}

	@Override
	public List<? extends MosDevice> findAllMosDeviceByType(DeviceType type) {
		return deviceDao.findAllMosDeviceByType(type);
	}

    @Override
    public String checkChannelsInUse(Device device){
        if (device.getId() != 0 && device instanceof MosDevice) {
            MosDevice oldDevice = (MosDevice) deviceDao.get(device.getId());

            // Precorre a lista dos canais antigos
            for(MosDeviceChannel oldChannel : oldDevice.getChannels()){

                Boolean isOnList = false;
                // Percorre a nova lista de canais
                for(MosDeviceChannel newChannel : ((MosDevice) device).getChannels()){
                     if (oldChannel.getId().equals(newChannel.getId())){
                         isOnList = true;
                     }
                }
                // Se o canal não está na lista, ele foi deletado
                if(!isOnList){
                    if(scriptVideoDao.countByChannel(oldChannel) > 0 ||
                            storyMosVideoDao.countByChannel(oldChannel) > 0){
                        return CHANNELS_IN_USE;
                    }
                }
            }
        }
        return CHANNELS_NOT_IN_USE;
    }

	@Override
	public <T extends Device> T save(T device) throws IllegalOperationException {
		if (validId(device.getId())) {

			device = (T) deviceDao.merge(device);
			deviceMessenger.notifyUpdate(device);

			if (device instanceof MosDevice) {
				// Força a re-criação da conexão
				mosConnFactory.resetConnection((MosDevice) device);
			}
		} else {
			deviceDao.save(device);
			deviceMessenger.notifyInsert(device);
		}
		return device;
	}

	@Override
	public <T extends Device> void remove(T device) throws IllegalOperationException {
		if (programDao.countByDevice(device) != 0) {
			throw new IllegalOperationException(bundle.getMessage("device.msg.cannotDelete"));
		}

		deviceDao.delete(device);
		deviceMessenger.notifyDelete(device);

		if (device instanceof MosDevice) {
			// Fecha a conexão
			mosConnFactory.closeConnection((MosDevice) device);
		}
	}

	@Override
	public boolean checkPathInUse(IIDevicePath path) {
		return programDao.countByPath(path) != 0;
	}

	//------------------------------
	//	Setters
	//------------------------------

	public void setDeviceDao(DeviceDao deviceDao) {
		this.deviceDao = deviceDao;
	}

	public void setDeviceMessenger(DeviceMessenger deviceMessenger) {
		this.deviceMessenger = deviceMessenger;
	}

	public void setProgramDao(ProgramDao programDao) {
		this.programDao = programDao;
	}

	public void setMosConnFactory(MosConnectionFactory mosConnFactory) {
		this.mosConnFactory = mosConnFactory;
	}


    public void setStoryMosVideoDao(StoryMosVideoDao storyMosVideoDao) {
        this.storyMosVideoDao = storyMosVideoDao;
    }

    public void setScriptVideoDao(ScriptVideoDao scriptVideoDao) {
        this.scriptVideoDao = scriptVideoDao;
    }
}
