package tv.snews.anews.service.impl;

import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.messenger.DocumentMessenger;
import tv.snews.anews.search.DocumentParams;
import tv.snews.anews.service.DocumentService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.service.StoryService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementação do serviço de manipulação de storys.
 *
 * @author Felipe Zap
 * @since 1.7
 */
public class DocumentServiceImpl implements DocumentService {

	private DocumentDao documentDao;

    private ScriptDao scriptDao;
    private ScriptPlanDao scriptPlanDao;
    private ReportageDao reportageDao;
    private GuidelineDao guidelineDao;
    private StoryDao storyDao;
    private StoryService storyService;
    private ScriptService scriptService;

    private ProgramDao programDao;
    private RundownDao rundownDao;
    private DocumentMessenger documentMessenger;
    private final String SUCCESS = "SUCCESS";
    private final String ALREADY_INCLUDED = "ALREADY_INCLUDED";

	@Override
	public PageResult<Document<?>> fullTextSearch(DocumentParams searchParams, boolean drawerSearch) {
		int total = documentDao.countByParams(searchParams, drawerSearch);
		List<Document<?>> documents = documentDao.findByParams(searchParams, drawerSearch);
		return new PageResult<>(searchParams.getPage(), total, documents);
	}

	@Override
	public PageResult<Document<?>> findByParams(DocumentParams params, boolean drawerSearch) {
		int total = documentDao.countByParams(params, drawerSearch);
		List results = documentDao.findByParams(params, drawerSearch);

		//TODO verificar se é necessário lockar as entidades.
		//markLocks(results);

        List<Document<?>> documents = new ArrayList<>();
        for (Object object : results) {
            documents.add(createDocumentInstance(object, drawerSearch));
        }

		return new PageResult<>(params.getPage(), total, documents);
	}

    private Document<?> createDocumentInstance(Object result, boolean drawerSearch) {
        Document document;

        Object[] object = (Object[]) result;
        Integer klass = object[4] != null ? (Integer) object[4] : null;

        switch (klass) {
            case 1:
                document = new Guideline();
                break;
            case 2:
                document = new Reportage();
                break;
            case 3:
                document = new Story();
                break;
            case 4:
                document = new Script();
                break;
            default:
                document = null;
        }

        if (document != null) {
            document.setId(object[0] != null ? (Long) object[0] : null);
            document.setDate(object[1] != null ? (Date) object[1] : null);
            document.setSlug(object[2] != null ? (String) object[2] : null);
            document.setProgram(object[3] != null ? (Program) object[3] : null);
            documentDao.refresh(document);
            if (drawerSearch)
                document.getDrawerPrograms().size();
        }

        return document;
    }

    /**
     * O fluxo de envio para a gaveta depende do tipo de documento.
     *      Reportagens: Automaticamente, são listadas na aba "Materias". A função "Enviar p/ gaveta" a envia para a
     *                   gaveta do programa e adiciona o programa na sua lista drawerPrograms
     *      Pautas: Tem o mesmo comportamento que as reportagens.
     *      Laudas: Ao ser criada, uma lauda não vai para a aba matérias. A unica forma de isso acontecer é pela função "Enviar p/ a gaveta",
     *              que define true no campo drawer a envia para a gaveta do programa e pode ser visivel na area de materias.
     *      Scripts: tem o mesmo comportamento das laudas.
     *
     *      Logo, o fluxo para os dois primeiros é ser enviada para a gaveta do programa. Para os segundos, precisa-se primeiro tratar se
     *      o documento vem de um espelho, se sim, faz as operações definidas no service de cada um, logo em seguida, adiciona o programa
     *      à sua lista.
     * @param document instância de alguma subclasse de document
     * @param program programa
     * @param toAdd indicativo que diz se é para adicionar ou remover o documento da gaveta do programa
     * @return
     */
    private String setDrawer(Document document, Program program, boolean toAdd){
        if (toAdd) {
            if(document.getDrawerPrograms().contains(program))
                return ALREADY_INCLUDED;
        	document.getDrawerPrograms().add(program);
        } else {
        	document.getDrawerPrograms().remove(program);
        }
        
        save(document);
        return SUCCESS;
    }

    @Override
    public String archiveGuidelineToDrawer(Long guidelineId, Integer programId, boolean toAdd) {
        Program program = programDao.get(programId);
        return setDrawer(guidelineDao.get(guidelineId), program, toAdd);
    }

    @Override
    public String archiveReportageToDrawer(Long reportageId, Integer programId, boolean toAdd) {
        Program program = programDao.get(programId);
        return setDrawer(reportageDao.get(reportageId), program, toAdd);
    }

    @Override
    public String archiveScriptToDrawer(Long scriptId, Integer programId, boolean toAdd) {
        if (toAdd) scriptService.sendToDrawer(scriptId);
        Program program = programDao.get(programId);
        return setDrawer(scriptDao.get(scriptId), program, toAdd);
    }

    @Override
    public String archiveStoryToDrawer(Long storyId, Integer programId, boolean toAdd) {
        Story story = storyDao.get(storyId);
        Program program = programDao.get(programId);
        return setDrawer(story, program, toAdd);
    }

    @Override
    public void archiveStoriesToDrawer(Long[] idsOfStories, Long rundownId, boolean toAdd) {
        Rundown rundown = rundownDao.get(rundownId);
        List<Story> stories = storyDao.listById(idsOfStories);
        Long[] ids = new Long[stories.size()];
        int index = 0;
        for(Story current : stories){
            if (toAdd)
                storyService.sendToDrawer(current);
            setDrawer(current, rundown.getProgram(), toAdd);
            ids[index] = current.getId();
            index++;
        }
        documentMessenger.notifySentToDrawer(ids, rundown);
    }

    @Override
    public void archiveScriptsToDrawer(Long[] idsOfScripts, Long scriptPlanId, boolean toAdd) {
        ScriptPlan scriptPlan = scriptPlanDao.get(scriptPlanId);
        List<Script> scripts = scriptDao.listById(idsOfScripts);
        Long[] ids = new Long[scripts.size()];
        int index = 0;
        for(Script current : scripts){
            if (toAdd)
                scriptService.sendToDrawer(current.getId());
            setDrawer(current, scriptPlan.getProgram(), toAdd);
            ids[index] = current.getId();
            index++;
        }
        documentMessenger.notifySentToDrawer(ids, scriptPlan);
    }

    private void save(Document document) {
    	if (document instanceof Guideline) {
    		guidelineDao.save((Guideline) document);
    	} else if (document instanceof Story) {
    		storyDao.save((Story) document);
    	} else if (document instanceof Script) {
    		scriptDao.save((Script) document);
    	} else if (document instanceof Reportage) {
    		reportageDao.save((Reportage) document);
    	}
    }

    public void setDocumentDao(DocumentDao documentDao) {
		this.documentDao = documentDao;
	}

    public void setScriptDao(ScriptDao scriptDao) {
        this.scriptDao = scriptDao;
    }

    public void setScriptPlanDao(ScriptPlanDao scriptPlanDao) {
        this.scriptPlanDao = scriptPlanDao;
    }

    public void setReportageDao(ReportageDao reportageDao) {
        this.reportageDao = reportageDao;
    }

    public void setGuidelineDao(GuidelineDao guidelineDao) {
        this.guidelineDao = guidelineDao;
    }

    public void setStoryDao(StoryDao storyDao) {
        this.storyDao = storyDao;
    }

    public void setProgramDao(ProgramDao programDao) {
        this.programDao = programDao;
    }

    public void setDocumentMessenger(DocumentMessenger documentMessenger) {
        this.documentMessenger = documentMessenger;
    }

    public void setStoryService(StoryService storyService) {
        this.storyService = storyService;
    }

    public void setScriptService(ScriptService scriptService) {
        this.scriptService = scriptService;
    }

    public void setRundownDao(RundownDao rundownDao) {
        this.rundownDao = rundownDao;
    }
}
