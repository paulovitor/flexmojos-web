package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.dao.BlockTemplateDao;
import tv.snews.anews.dao.RundownTemplateDao;
import tv.snews.anews.domain.BlockTemplate;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownTemplate;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.RundownTemplateMessenger;
import tv.snews.anews.service.RundownTemplateService;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class RundownTemplateServiceImpl implements RundownTemplateService {

    private final EntityLocker<RundownTemplate> locker;

    private RundownTemplateDao rundownTemplateDao;
    private BlockTemplateDao blockTemplateDao;

    private RundownTemplateMessenger rundownTemplateMessenger;
    private SessionManager sessionManager;

    public RundownTemplateServiceImpl() {
        locker = EntityLockerFactory.getLocker(RundownTemplate.class);
        locker.addEntityLockerListener(new LockerListener());
    }

    @Override
    public RundownTemplate load(Integer rundownId) {
        return rundownTemplateDao.get(rundownId);
    }

    @Override
    public List<RundownTemplate> findAllByProgram(Program program) {
        List<RundownTemplate> templates = rundownTemplateDao.findAllByProgram(program);

        // Registra os templates em edição
        for (RundownTemplate template : templates) {
            template.setEditable(!locker.isLocked(template));
        }

        return templates;
    }

    @Override
    public boolean nameAvailable(String name, Program program) {
        return rundownTemplateDao.countByNameAndProgram(name, program) == 0;
    }

    @Override
    public RundownTemplate createTemplate(String name, Program program) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Invalid rundown template name: '" + name + "'.");
        }

        if (!nameAvailable(name, program)) {
            throw new IllegalArgumentException("There is already a rundown template with this name: '" + name + "'.");
        }

        RundownTemplate template = new RundownTemplate(name, program);
        save(template);

        // Cria o bloco Stand By
        createBlock(template);

        return template;
    }

    @Override
    public RundownTemplate createCopy(Integer originalId, String copyName, Program program) {
        RundownTemplate original = rundownTemplateDao.get(originalId);
        try {
            RundownTemplate copy = original.clone();
            copy.setName(copyName);
            copy.setProgram(program);
            return save(copy);
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateTemplate(RundownTemplate template) {
        rundownTemplateDao.update(template);
    }

    @Override
    public void removeTemplate(RundownTemplate template) {
        rundownTemplateDao.delete(template);
    }

    @Override
    public synchronized BlockTemplate createBlock(Integer rundownId) {
        return createBlock(rundownTemplateDao.get(rundownId));
    }

    @Override
    public synchronized BlockTemplate createBlock(RundownTemplate rundown) {
        BlockTemplate block = rundown.createBlock();
        block.setId((Integer) blockTemplateDao.save(block));

        return block;
    }

    @Override
    public void updateBlock(BlockTemplate blockTemplate) {
        blockTemplateDao.update(blockTemplate);
    }

    @Override
    public void removeBlock(Integer blockId) {
        BlockTemplate block = blockTemplateDao.get(blockId);
        RundownTemplate rundown = block.getRundown();
        rundown.removeBlock(block);
    }

    @Override
    public void lockEdition(RundownTemplate template) throws IllegalOperationException {
        // Pode editar apenas um template por vez
        unlockAllEditions();

        User user = sessionManager.getCurrentUser();
        locker.lock(template, user);
    }

    @Override
    public void unlockAllEditions() {
        User user = sessionManager.getCurrentUser();
        locker.releaseLocksOfUser(user);
    }

    //----------------------------------
    //  Helpers
    //----------------------------------

    private RundownTemplate save(RundownTemplate rundown) {
        Integer id = (Integer) rundownTemplateDao.save(rundown);
        rundown.setId(id);
        return rundown;
    }

    //----------------------------------
    //  Setters
    //----------------------------------

    public void setRundownTemplateDao(RundownTemplateDao rundownTemplateDao) {
        this.rundownTemplateDao = rundownTemplateDao;
    }

    public void setBlockTemplateDao(BlockTemplateDao blockTemplateDao) {
        this.blockTemplateDao = blockTemplateDao;
    }

    public void setRundownTemplateMessenger(RundownTemplateMessenger rundownTemplateMessenger) {
        this.rundownTemplateMessenger = rundownTemplateMessenger;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    //----------------------------------
    //  Inner Classes
    //----------------------------------

    private class LockerListener implements EntityLockerListener<RundownTemplate> {

        @Override
        public void entityLocked(RundownTemplate entity, User user) {
            rundownTemplateMessenger.notifyLock(entity, user);
        }

        @Override
        public void entityUnlocked(RundownTemplate entity, User user) {
            rundownTemplateMessenger.notifyUnlock(entity);
        }
    }
}
