package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;
import tv.snews.anews.dao.UserFileDao;
import tv.snews.anews.domain.UserFile;
import tv.snews.anews.exception.DataNotFoundException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.service.UserFileService;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.anews.util.StorageUtil;

import java.io.File;
import java.io.IOException;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class UserFileServiceImpl implements UserFileService {

	private static final Logger log = LoggerFactory.getLogger(UserFileServiceImpl.class);
	
	private UserFileDao userFileDao;
	private StorageUtil storageUtil;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private SessionManager sessionManager;

	@Override
	public UserFile saveUserFile(UserFile file) throws IOException, SessionManagerException, DataNotFoundException {
		File usersDir = storageUtil.getUsersFilesDirectory();
		File tempFile = new File(file.getPath());

		// Move file to new directory
		if (tempFile.renameTo(new File(usersDir, tempFile.getName()))) {
			file.setPath(usersDir.getAbsolutePath() + "/" + tempFile.getName());
		} else {
			tempFile.delete();
			throw new IOException(bundle.getMessage("storage.moveToFolderError"));
		}

		file.setOwner(sessionManager.getCurrentUser());
		Integer fileId = (Integer) userFileDao.save(file);
		file.setId(fileId);
		
		return file;
	}

	@Override
	public void deleteUserFile(UserFile file) throws IOException {
		Assert.notNull(file);
		
		File physicalFile = new File(file.getPath());
		if (physicalFile.exists()) {
			if (!physicalFile.delete()) {
				// Falha ao apagar o arquivo
				throw new IOException(bundle.getMessage("storage.fileNotFound"));
			}
		} else {
			// O arquivo não existe, mas deveria existir. Apenas emite um WARN.
			log.warn(bundle.getMessage("storage.deleteError"));
		}
		
		userFileDao.delete(file);
	}
	
	//----------------------------------
	//  Setters
	//----------------------------------

	public void setUserFileDao(UserFileDao userFileDao) {
		this.userFileDao = userFileDao;
	}

	public void setStorageUtil(StorageUtil storageUtil) {
		this.storageUtil = storageUtil;
	}
	
	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
}
