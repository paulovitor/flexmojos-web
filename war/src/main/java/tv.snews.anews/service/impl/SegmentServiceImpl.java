
package tv.snews.anews.service.impl;

import tv.snews.anews.dao.RoundDao;
import tv.snews.anews.dao.SegmentDao;
import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Segment;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.SegmentMessenger;
import tv.snews.anews.service.SegmentService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class SegmentServiceImpl implements SegmentService {

	private SegmentDao segmentDao;
	private SegmentMessenger segmentMessenger;

	private RoundDao roundDao;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	@Override
	public void save(Segment segment) {
		if (segment.getId() > 0) {
			segmentDao.update(segment);
			segmentMessenger.notifyUpdate(segment);
		} else {
			segmentDao.save(segment);
			segmentMessenger.notifyInsert(segment);
		}
	}

	@Override
	public void delete(Integer id) throws IllegalOperationException {
		Segment segment = segmentDao.get(id);
		// Verifica se alguma instituição está em uso
		for (Institution institution : segment.getInstitutions()) {
			if (roundDao.countByInstitution(institution) == 0) {
				throw new IllegalOperationException(bundle.getMessage("institution.msg.canNotBeDeleted"));
			}
		}
		
		segmentDao.delete(segment);
		segmentMessenger.notifyDelete(segment);
	}

	@Override
	public List<Segment> listAll() {
		return segmentDao.listAll();
	}

	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setSegmentDao(SegmentDao segmentDao) {
		this.segmentDao = segmentDao;
	}
	
    public void setSegmentMessenger(SegmentMessenger segmentMessenger) {
    	this.segmentMessenger = segmentMessenger;
    }
    
    public void setRoundDao(RoundDao roundDao) {
	    this.roundDao = roundDao;
    }
}
