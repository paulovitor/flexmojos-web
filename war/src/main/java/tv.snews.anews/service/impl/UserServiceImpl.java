package tv.snews.anews.service.impl;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import tv.snews.anews.dao.GroupDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.dao.TeamDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Group;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.Team;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.*;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.infra.mail.MailSender;
import tv.snews.anews.messenger.InfrastructureMessenger;
import tv.snews.anews.messenger.SettingsMessenger;
import tv.snews.anews.messenger.UserMessenger;
import tv.snews.anews.service.UserService;
import tv.snews.anews.util.CriptUtil;
import tv.snews.anews.util.ResourceBundle;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import static tv.snews.anews.util.CriptUtil.encrypt;

/**
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class UserServiceImpl implements UserService {

	private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

	private UserDao userDao;
    private GroupDao groupDao;
    private TeamDao teamDao;
	private SettingsDao settingsDao;

	private MailSender mailSender;
	private SessionManager sessionManager;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	private InfrastructureMessenger infrastructureMessenger;
	private UserMessenger userMessenger;
	private SettingsMessenger settingsMessenger;

	@Override
	public User loadByCredentials(String email, String password) {
		return userDao.findByEmailAndPassword(email, encrypt(password));
	}

	@Override
	public void recoveryPassword(String email) throws DataNotFoundException, InvalidEmailException, SessionManagerException {
		if (email == null) {
			throw new InvalidEmailException(bundle.getMessage("user.invalidEmail"));
		}

		User user = userDao.findByEmail(email);
		if (user == null) {
			throw new DataNotFoundException(bundle.getMessage("user.notFound"));
		}

		try {
			mailSender.send(
					user.getEmail(),
					bundle.getMessage("user.mailSubject"),
					bundle.getMessage("user.mailMessage", user.getEmail(), CriptUtil.decrypt(user.getPassword()))
			);
		} catch (MessageDeliveryException e) {
			log.warn("Could not send the email with the user data.", e);
			infrastructureMessenger.notifyWarn(bundle.getMessage("user.mailFailed"), sessionManager.getCurrentUser());
		}
	}

	@Override
	public User changePassword(String email, String oldPassword, String newPassword) throws DataNotFoundException, InvalidPasswordException {
		User user = userDao.findByEmailAndPassword(email, encrypt(oldPassword));
		if (user == null) {
			throw new DataNotFoundException(bundle.getMessage("user.notFound"));
		}
		if (user.getPassword().equals(encrypt(newPassword))) {
			throw new InvalidPasswordException(bundle.getMessage("user.passwordNotEqualPrevious"));
		}

		Settings settings = settingsDao.loadSettings();
		Calendar passwordExpiration = new GregorianCalendar();
		passwordExpiration.add(Calendar.MONTH, settings.getPasswordExpiration());

		user.setPassword(encrypt(newPassword));
		user.setPasswordExpiration(passwordExpiration.getTime());
		userDao.update(user);

		return user;
	}

	@Override
	public List<User> listUsersByGroupAndStatus(Group group) throws SessionManagerException {
		List<User> users = userDao.listUsersByGroupAndStatus(group);
		users.remove(sessionManager.getCurrentUser());

		for (User user : users) {
			if (sessionManager.getOnlineUsers().contains(user)) {
				user.setOnline(true);
			}
		}
		return users;
	}

	@Override
	public List<User> listAll() {
		return userDao.listAll(null, UserDao.NICKNAME);
	}

	@Override
	public List<User> listAll(String searchTerm) {
		List<User> users = userDao.listAll(searchTerm, UserDao.NAME);

		for (User user : users) {
			if (sessionManager.getOnlineUsers().contains(user)) {
				user.setOnline(true);
			}
		}


		return users;
	}

	@Override
	public List<User> listReporters() {
		return userDao.listReporters();
	}

	@Override
	public List<User> listProducers() {
		return userDao.listProducers();
	}

    @Override
    public List<User> listChecklistProducers() {
        return userDao.listChecklistProducers();
    }

	@Override
	public List<User> listEditors() {
		return userDao.listEditors();
	}

	@Override
	public String save(User user) throws InvalidEmailException, SessionManagerException {
		if (StringUtils.isBlank(user.getEmail())) {
			throw new InvalidEmailException(bundle.getMessage("user.invalidEmail"));
		}

		User userByNickname = userDao.findByNickname(user.getNickname());
		User userByEmail = userDao.findByEmail(user.getEmail());

		if (user.getId() == null) {
			if (userByNickname != null) {
				return "INVALID_NICKNAME";
			}
			if (userByEmail != null) {
				return "INVALID_EMAIL";
			}
		} else {
			if (userByNickname != null && !Objects.equals(user.getId(), userByNickname.getId())) {
				return "INVALID_NICKNAME";
			}
			if (userByEmail != null && !Objects.equals(user.getId(), userByEmail.getId())) {
				return "INVALID_EMAIL";
			}
		}

		// Configure the expiration if not configured by administrator.
		if (user.getPasswordExpiration() == null) {
			Settings settings = settingsDao.loadSettings();
			Calendar passwordExpiration = new GregorianCalendar();
			passwordExpiration.add(Calendar.MONTH, settings.getPasswordExpiration());

			user.setPasswordExpiration(passwordExpiration.getTime());
		}

		//Trimming (leading and trailing) the content of password.
		user.setNewPassword(StringUtils.trim(user.getNewPassword()));

		//Cripting the new password informed.
		if (user.getNewPassword() != null && !user.getNewPassword().equals("")) {
			user.setPassword(encrypt(user.getNewPassword()));
		}

		if (user.getId() > 0) {
			User old = userDao.get(user.getId()); //Before save, get the old data to compare.
			userDao.evict(old); //Detach object from session.

			if (!old.getNickname().equals(user.getNickname())) {
				Settings settings = settingsDao.loadSettings();
				settings.setDisplayIndexAlert(true); //Mostra mensagem que a base precisa ser re-indexada
				settingsDao.update(settings);
				settingsMessenger.notifySettingsChanged(settings);
			}

			userDao.update(user); //Attach again, but merge new data.
			userMessenger.notifyUpdate(user);
			userDao.flush(); //Force save, if everything works fine send the e-mail.

			// If some login data change, send e-mail notifing it.
			if (!old.getPassword().equals(user.getPassword()) || !old.getEmail().equals(user.getEmail())) {
				try {
					mailSender.send(
							user.getEmail(),
							bundle.getMessage("user.mailChangeLoginSubject"),
							bundle.getMessage("user.mailChangeLoginMessage", user.getEmail(), CriptUtil.decrypt(user.getPassword()))
					);
				} catch (MessageDeliveryException e) {
					log.warn("Could not send the email with the user data.", e);
					infrastructureMessenger.notifyWarn(bundle.getMessage("user.mailFailed"), sessionManager.getCurrentUser());
				}
			}
		} else {
			// If adding the user, check your password. If blank, generate one randomily.
			if (StringUtils.isBlank(user.getNewPassword())) {
				user.setNewPassword(RandomStringUtils.randomAlphanumeric(10));
				user.setPassword(encrypt(user.getNewPassword()));
			}

			int userId = (Integer) userDao.save(user);
			userMessenger.notifyInsert(user);
			userDao.flush(); //Force save, if everything works fine send the e-mail.
			if (userId > 0) {
				try {
					mailSender.send(
							user.getEmail(),
							bundle.getMessage("user.mailNewUserSubject"),
							bundle.getMessage("user.mailNewUserMessage", user.getEmail(), CriptUtil.decrypt(user.getPassword()))
					);
				} catch (MessageDeliveryException e) {
					log.warn("Could not send the email with the user data.", e);
					infrastructureMessenger.notifyWarn(bundle.getMessage("user.mailFailed"), sessionManager.getCurrentUser());
				}
			}
		}
		userMessenger.notifyUsersChanged();

		return "";
	}

	@Override
	public String saveData(User user, String password, String newPassword, String confirmation) throws InvalidEmailException, SessionManagerException {
		String result = "";

		//If user type your password, is because he has intantion to change your password.
		if (StringUtils.isNotBlank(password)) {
			if (StringUtils.isNotBlank(newPassword) && StringUtils.isNotBlank(confirmation)) {
				if (newPassword.equals(confirmation)) {
					User old = userDao.get(user.getId()); // Get actual data
					userDao.evict(old); // Detach from session;

					if (CriptUtil.decrypt(old.getPassword()).equals(password)) {
						user.setPassword(encrypt(newPassword));
					} else {
						result = "PASSWORD_NOT_MATCH";
					}
				} else {
					result = "PASSWORD_AND_CONFIRMATION_NOT_MATCH";
				}
			} else {
				result = "NEW_PASSWORD_INVALID";
			}
		}
		if (result.equals("")) result = save(user);

		userMessenger.notifyUsersChanged();

		return result;
	}

	@Override
	public void delete(User user) {
		if (user != null) {
			user.setDeleted(true);
			userDao.update(user);

			// if deleted user is online, should be kicked out
			disconnectUser(user);
			userMessenger.notifyDelete(user);
			userMessenger.notifyUsersChanged();
		}
	}

	@Override
	public void disconnectUser(User user) {
		sessionManager.kickOut(user);
	}

	@Override
	public void requestUserToAgreeWithTerms(int userId) {
		User user = userDao.get(userId);
		if (user != null) {
			user.setAgreementDate(null);
			userDao.save(user);
		}
	}

	@Override
	public void releaseLocksFromUser(User user) {
		EntityLockerFactory.releaseAllLocksOfUser(user);
	}

    @Override
    public List<User> listByGroupName(String searchTerm, int groupId, Boolean belongsToTheGroup) {
        Group group = groupDao.get(groupId);
        List<User> users = userDao.listByGroupName(searchTerm, group, belongsToTheGroup);
        return users;
    }

    @Override
    public List<User> listByTeamName(String searchTerm, int teamId, Boolean belongsToTheTeam) {
        Team team = teamDao.get(teamId);
        List<User> users = userDao.listByTeamName(searchTerm, team, belongsToTheTeam);
        return users;
    }

    @Override
    public void collectMenu(int userId, boolean value) {
        User user = userDao.get(userId);
        user.setCollapsedMenu(value);
        userDao.update(user);
    }

	//------------------------------
	//	UserDetailsService Methods
	//------------------------------

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByEmail(username);
		if (user != null) {
			user.getAuthorities().size(); // Carregar as authorities fora da sessão da error
		}
		return user;
	}

	//----------------------------------------
	//	Setters
	//----------------------------------------

	public void setInfrastructureMessenger(InfrastructureMessenger infrastructureMessenger) {
		this.infrastructureMessenger = infrastructureMessenger;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public void setUserMessenger(UserMessenger userMessenger) {
		this.userMessenger = userMessenger;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setSettingsMessenger(SettingsMessenger settingsMessenger) {
		this.settingsMessenger = settingsMessenger;
	}

    public void setGroupDao(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public void setTeamDao(TeamDao teamDao) {
        this.teamDao = teamDao;
    }
}
