package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.infra.ccstudio.CCStudioParser;
import tv.snews.anews.infra.mail.MailSender;
import tv.snews.anews.messenger.InfrastructureMessenger;
import tv.snews.anews.messenger.ReportageMessenger;
import tv.snews.anews.search.ReportageParams;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.ccstudio.CCStudioInterface;
import tv.snews.ccstudio.command.request.UpdateCommand;
import tv.snews.ccstudio.domain.Register;

import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Implementação padrão da interface {@link tv.snews.anews.service.ReportageService}.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ReportageServiceImpl implements ReportageService {

	private static final Logger log = LoggerFactory.getLogger(ReportageServiceImpl.class);

	private final EntityLocker<Reportage> locker;

	private ReportageDao reportageDao;
    private ReportageCopyLogDao reportageCopyLogDao;
	private ReportageFullTextDao reportageFTSDao;

	private StoryDao storyDao;
    private ProgramDao programDao;

	private InfrastructureMessenger infrastructureMessenger;
	private ReportageMessenger reportageMessenger;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private SessionManager sessionManager;
	private MailSender mailSender;

	private CCStudioInterface ccStudio;

	public ReportageServiceImpl() {
		locker = EntityLockerFactory.getLocker(Reportage.class);
	}

	@Override
	public Reportage save(Reportage reportage) throws IllegalArgumentException, SessionManagerException, IllegalOperationException {
		Objects.requireNonNull(reportage, bundle.getMessage("reportage.msg.nullReportage"));
		if (reportage.getId() != null && reportage.getId() > 0) {
			return update(reportage);
		}
		insert(reportage);
		return reportage;
	}

	private void insert(Reportage reportage) throws IllegalOperationException {
		User currentUser = sessionManager.getCurrentUser();
		reportage.setId((Long) reportageDao.save(reportage, currentUser));
		sendToCCStudio(reportage);

		markEditor(reportage);
		reportageMessenger.notifyInsert(reportage);

        locker.lock(reportage, currentUser);
	}

    private Reportage update(Reportage reportage) {
        Reportage current = reportageDao.get(reportage.getId());

        // Como faz o merge é no objeto "reportage", tem que carregar as revisões
        reportage.setCopiesHistory(current.getCopiesHistory());
        reportage.setRevisions(current.getRevisions());
        reportage.setVersion(current.getVersion());
        reportage.setDrawerPrograms(current.getDrawerPrograms());

        reportageDao.evict(current);

        //Verifica se os dados são diferente para enviar infos para o CCStudio.
        if (!reportage.containsSameValues(current)) {
            sendToCCStudio(reportage);
        }

        reportage = reportageDao.merge(reportage, sessionManager.getCurrentUser());

        reportageMessenger.notifyUpdate(reportage);
        return reportage;
    }

    private void sendToCCStudio(Reportage reportage) {
        // Se for preciso, notifica o CCStudio da mudança dos campos da reportagem
        List<Story> stories = storyDao.listByReportage(reportage);
        for (Story story : stories) {
            if (story.isApproved() && !story.isExcluded()) {
                long rundownId = story.getBlock().getRundown().getId();
                Register register = CCStudioParser.parseRegister(story);
                UpdateCommand updateCmd = new UpdateCommand(register);
                ccStudio.sendCommand(updateCmd, rundownId);
            }
        }
    }

    @Override
	public Reportage loadById(Long reportageId) {
		Reportage reportage = reportageDao.get(reportageId);
		Objects.requireNonNull(reportage, bundle.getMessage("reportage.msg.reportageNotFoundById") + reportageId);
		return reportage;
	}

    @Override
    public Reportage remoteCopyRequest(Long reportageId, String nickname, String company) {
        Reportage reportage = reportageDao.get(reportageId);

        ReportageCopyLog copyLog = new ReportageCopyLog(reportage, nickname, company);
        reportage.getCopiesHistory().add(copyLog);

        reportageCopyLogDao.save(copyLog);
        reportageMessenger.notifyUpdate(reportage);

        return reportage;
    }

    @Override
	public void remove(Long reportageId) throws ValidationException {
		Reportage reportage = reportageDao.get(reportageId);
		
		if (reportage == null) {
			throw new ValidationException(bundle.getMessage("reportage.msg.nullReportage"));
		}
		
		reportage.setExcluded(true);

		reportageDao.update(reportage, sessionManager.getCurrentUser());
		reportageMessenger.notifyDelete(reportage);
	}

	@Override
	public void changeOk(Long id, boolean status) {
		Reportage reportage = reportageDao.get(id);
		boolean changed = status ? reportage.approve() : reportage.disapprove();
		if (changed) {
			reportageDao.merge(reportage);
			reportageMessenger.notifyFieldUpdate(reportage, "ok", status);
		}
	}

	@Override
	public PageResult<Reportage> listAll(ReportageParams params) {
        int total = reportageDao.totalByParams(params);
        List<Reportage> reportages = reportageDao.findByParams(params);
        return new PageResult<Reportage>(params.getPage(), ReportageService.RESULTS_BY_PAGE, total, reportages);
	}

	@Override
	public List<Reportage> listAllReportageOfDay(Date date, Program program) throws ValidationException {
		if (date == null) {
			throw new ValidationException(bundle.getMessage("defaults.invalidDate", date));
		}
		return markEditor(reportageDao.findAllByDateAndProgram(date, program));
	}

	@Override
	public PageResult<Reportage> listExcludedReportagesPage(int pageNumber) throws SessionManagerException {
		List<Reportage> reportages = markEditor(reportageDao.listExcludedPage(pageNumber));
		return new PageResult<>(pageNumber, reportageDao.totalExcluded(), reportages);
	}

	@Override
	public PageResult<Reportage> reportageByFullTextSearch(ReportageParams searchParams) {
		int total = reportageFTSDao.totalBySearchReportage(searchParams);
		List<Reportage> reportages = markEditor(reportageFTSDao.searchReportageGeneral(searchParams));
		return new PageResult<>(searchParams.getPage(), total, reportages);
	}

	@Override
	public Collection<ReportageRevision> loadChangesHistory(Long reportageId) {
		return reportageDao.get(reportageId).changesHistory();
	}

	@Override
	public ReportageRevision loadRevision(Long reportageId, int revisionNumber, boolean showDiff) {
		Reportage reportage = reportageDao.get(reportageId);
		ReportageRevision current = reportage.currentRevision();
		ReportageRevision revision = reportage.getRevisionByNumber(revisionNumber);

		if (showDiff && current.getRevisionNumber() != revisionNumber) {
			return current.computeDiff(revision);
		} else {
			return revision;
		}
	}

	@Override
	public void restoreExcluded(Long reportageId, Date date, int destinyProgramId) {
		Reportage reportage = reportageDao.get(reportageId);

        if (destinyProgramId == -1) {
            reportage.setProgram(null);
        } else {
            reportage.setProgram(programDao.get(destinyProgramId));
        }

        reportage.setDate(date);
		reportage.setExcluded(false);
		
		reportageDao.update(reportage, sessionManager.getCurrentUser());
		reportageMessenger.notifyRestore(reportage);
	}

	@Override
	public User isLockedForEdition(Long reportageId) {
		return locker.findOwner(reportageDao.get(reportageId));
	}

	@Override
	public void lockEdition(Long reportageId, User user) throws IllegalOperationException {
		Reportage reportage = reportageDao.get(reportageId);
		locker.lock(reportage, user);
	}

	@Override
	public void unlockEdition(Long reportageId) throws IllegalOperationException {
		Reportage reportage = reportageDao.get(reportageId);
		locker.unlock(reportage, sessionManager.getCurrentUser());
	}

	@Override
	public void sendReportageMail(Long reportageId) throws ValidationException, SessionManagerException {
		Reportage reportage = reportageDao.get(reportageId);

		Objects.requireNonNull(reportage, bundle.getMessage("reportage.msg.nullReportage"));
		Objects.requireNonNull(reportage, bundle.getMessage("reportage.msg.invalidReporter"));

		String title = DateFormat.getInstance().format(reportage.getDate()) + " - " + reportage.getGuideline().getProgram().getName()
		    + "<br/>";

		StringBuilder content = new StringBuilder();

		//Html presentation for Title
		content.append("<font size='3' color='#B2B2B2' style='float:left;'>");
		content.append(title);
		content.append("</font>");

		//Html for producer
        if (reportage.getGuideline() != null && !reportage.getGuideline().getProducers().isEmpty()) {
            content.append("<font size='3' color='#B2B2B2' style='float:right;'>");
            content.append(bundle.getMessage("guideline.producedBy"));
            content.append(" ");
            content.append(reportage.getGuideline().producersToString());
            content.append("</font>");
        }

		//Cinegrafista
		if (!StringUtils.isEmpty(reportage.getCameraman())) {
			content.append("<font size='3' color='#B2B2B2' style='float:right;'>");
			content.append(bundle.getMessage("reportage.cameramanStr"));
			content.append(": ");
			content.append(reportage.getCameraman());
			content.append("</font>");
		}

		//Html for slug
		content.append("<div style='clear:both;'>&nbsp;</div>");
		content.append("<hr color='black'>");
		content.append("<font size='6'>");
		content.append(reportage.getSlug());
		content.append("</font>");
		content.append("<br/>");

		for (ReportageSection rc : reportage.getSections()) {
			content.append("<hr color='black'>");
			content.append("<b>");
			content.append("<font size='2' color='#B2B2B2'>");
			content.append("(");
			content.append(typeSection(rc.getType()));
			content.append(") ");
			content.append("</font>");
			content.append("<font size='2'  align='justify'>");
			content.append(rc.getContent());
			content.append("</font>");
			content.append("</b>");
			content.append("<br/>");
			if (rc.getObservation() != null) {
				content.append("<b>");
				content.append("<font size='2' color='#B2B2B2'>");
				content.append("(");
				content.append(bundle.getMessage("reportage.quoteUpperCase"));
				content.append(") ");
				content.append("<font size='2' color='black'>");
				content.append("\"").append(rc.getObservation()).append("\"");
			}
		}

		try {
			mailSender.send(
			    reportage.getReporter().getEmail(),
			    bundle.getMessage("reportage.mailSubject"),
			    content.toString()
            );
		} catch (MessageDeliveryException e) {
			log.warn("Could not send the email with the reportage data.", e);
			infrastructureMessenger.notifyError(bundle.getMessage("reportage.mailFailed"), sessionManager.getCurrentUser());
		}
	}

    @Override
    public void copyReportage(Long reportageId, Date date, Program program) throws IllegalOperationException, CloneNotSupportedException {
        User currentUser = sessionManager.getCurrentUser();

        Reportage original = reportageDao.get(reportageId);
        Reportage copy = original.createCopy(currentUser);
        copy.setDate(date);
        copy.setProgram(program);

        copy = save(copy);

        // save cria um lock, então retira o lock criado
        locker.unlock(copy, currentUser);
    }

    @Override
    public void copyRemoteReportage(Reportage remoteReportage, Date destinyDate, Program destinyProgram, User reporter) throws IllegalOperationException, CloneNotSupportedException {
        User currentUser = sessionManager.getCurrentUser();

        Reportage copy = remoteReportage.createCopy(currentUser);
        copy.setDate(destinyDate);
        copy.setChangeDate(new Date());
        copy.setProgram(destinyProgram);
        copy.setReporter(reporter);
        copy.getRevisions().clear();

        copy.setCameraman(null);
        copy.getPrograms().clear();
        copy.getDrawerPrograms().clear();
        copy.setState(DocumentState.PRODUCING);

        for (ReportageSection section : copy.getSections()){
            section.setOk(false);
        }
        copy.setOk(false);
//        copy.getCopiesHistory().clear(); // não é copiado

        /*
         * Retira o vínculo com a pauta para não ter que criar uma cópia da pauta
         * também. Se for preciso ver a pauta que originou a reportagem o usuário
         * pode consultar diretamente na praça de origem.
         */
        copy.setGuideline(null);

        copy = save(copy);

        // save cria um lock, então retira o lock criado
        locker.unlock(copy, currentUser);
    }
    
    //--------------------------------------------------------------------------
	//	Helper Methods
	//--------------------------------------------------------------------------

	private Reportage markEditor(Reportage reportage) {
		reportage.setEditingUser(locker.findOwner(reportage));
		return reportage;
	}

	private List<Reportage> markEditor(List<Reportage> reportages) {
		for (Reportage reportage : reportages) {
			markEditor(reportage);
		}
		return reportages;
	}

	private String typeSection(ReportageSectionType rst) {
		String toReturn = "";
		switch (rst) {
			case APPEARANCE:
				toReturn = bundle.getMessage("reportage.sectionType.appearanceUpperCase");
				break;
			case ART:
				toReturn = bundle.getMessage("reportage.sectionType.artUpperCase");
				break;
			case CLOSURE:
				toReturn = bundle.getMessage("reportage.sectionType.closureUpperCase");
				break;
			case INTERVIEW:
				toReturn = bundle.getMessage("reportage.sectionType.interviewUpperCase");
				break;
			case OFF:
				toReturn = bundle.getMessage("reportage.sectionType.offUpperCase");
				break;
			case OPENING:
				toReturn = bundle.getMessage("reportage.sectionType.openingUpperCase");
				break;
			case SOUNDUP:
				toReturn = bundle.getMessage("reportage.sectionType.soundUpUpperCase");
				break;
		}
		return toReturn;
	}
	
	@Override
	public void updateState(Long reportageId, DocumentState state, String reason) {
		Reportage reportage = reportageDao.get(reportageId);
		reportage.setState(state);
		
		if (reason != null) {
			reportage.setStatusReason(reason);
		}

        reportageDao.update(reportage, sessionManager.getCurrentUser());
		reportageMessenger.notifyUpdate(reportage);
	}

    @Override
    public void registerProgram(Reportage reportage, Program program) {
        reportage.getPrograms().add(program);
        reportageDao.update(reportage);
        reportageMessenger.notifyFieldUpdate(reportage, "programs", reportage.getPrograms());
    }

    @Override
    public void unregisterProgram(Reportage reportage, Program program) {
        reportage.getPrograms().remove(program);
        reportageDao.update(reportage);
        reportageMessenger.notifyFieldUpdate(reportage, "programs", reportage.getPrograms());
    }

	//--------------------------------------------------------------------------
	//	Getters & Setters
	//--------------------------------------------------------------------------

	public void setInfrastructureMessenger(InfrastructureMessenger infrastructureMessenger) {
		this.infrastructureMessenger = infrastructureMessenger;
	}

	public void setReportageDao(ReportageDao reportageDao) {
		this.reportageDao = reportageDao;
	}

    public void setReportageCopyLogDao(ReportageCopyLogDao reportageCopyLogDao) {
        this.reportageCopyLogDao = reportageCopyLogDao;
    }

    public void setReportageMessenger(ReportageMessenger value) {
		this.reportageMessenger = value;

		locker.removeAllEntityLockerListeners();
		locker.addEntityLockerListener(new EntityLockerListener<Reportage>() {

			@Override
			public void entityLocked(Reportage reportage, User user) {
				reportageMessenger.notifyLock(reportage, user);
			}

			@Override
			public void entityUnlocked(Reportage reportage, User user) {
				reportageMessenger.notifyUnlock(reportage);
			}
		});
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public void setStoryDao(StoryDao storyDao) {
		this.storyDao = storyDao;
	}

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setCcStudio(CCStudioInterface ccStudio) {
		this.ccStudio = ccStudio;
	}

	public void setReportageFTSDao(ReportageFullTextDao reportageFTSDao) {
		this.reportageFTSDao = reportageFTSDao;
	}

    public void setProgramDao(ProgramDao programDao) {
        this.programDao = programDao;
    }
}
