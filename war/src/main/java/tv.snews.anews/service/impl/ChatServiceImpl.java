package tv.snews.anews.service.impl;

import org.springframework.util.Assert;
import tv.snews.anews.dao.ChatDao;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.messenger.ChatMessenger;
import tv.snews.anews.service.ChatService;
import tv.snews.anews.util.ResourceBundle;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Classe do serviço que implementa a interface {@link tv.snews.anews.service.ChatService}. Responsável
 * pelas ações diretamente ligadas a classe de domínio {@link Chat}.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class ChatServiceImpl implements ChatService {

	private ChatDao chatDao;
	private ChatMessenger chatMessenger;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	@Override
	public Chat loadChat(Long chatId) {
		Assert.notNull(chatId);
		return chatDao.get(chatId);
	}
	
	@Override
	public Chat createChat(Set<User> members) {
		Assert.notNull(members);
		if (members.size() < 2) {
			throw new IllegalArgumentException(bundle.getMessage("chat.invalidMembersSize"));
		}
		
		Chat chat = findChat(members);
		if (chat == null) {
			chat = new Chat();
			chat.setMembers(members);
			Long chatId = (Long) chatDao.save(chat);
			chat.setId(chatId);
		} else {
			// No caso de um chat encerrado que está sendo reaberto, limpa o 
			// registro de quem havia saido da conversa
			chat.reJoinAllMembers();
			chatDao.update(chat);
		}
		return chat;
	}
	
	@Override
	public Chat findChat(Set<User> members) {
		Assert.notNull(members);
		if (members.size() < 2) {
			throw new IllegalArgumentException(bundle.getMessage("chat.invalidMembersSize"));
		}
		return chatDao.findByMembers(members);
	}

	@Override
	public void addMembers(Chat chat, User... newMembers) throws MessageDeliveryException {
		chatDao.refresh(chat);
		Set<User> addedMembers = chat.addMembers(newMembers);
		
		if (!addedMembers.isEmpty()) {
			chatDao.update(chat);
			boolean sent = chatMessenger.notifyMembersJoined(chat, addedMembers.toArray(new User[addedMembers.size()]));
			if (!sent) {
				throw new MessageDeliveryException(bundle.getMessage("chat.newMemberNotificationFailed"));
			}
		}
	}
	
	@Override
	public void registerLeave(Chat chat, User user) throws MessageDeliveryException {
		chatDao.refresh(chat);
		if (chat.registerLeave(user)) {
			chatDao.update(chat);
			boolean sent = chatMessenger.notifyMemberLeft(chat, user);
			if (!sent) {
				throw new MessageDeliveryException(bundle.getMessage("chat.memberLeftNotificationFailed"));
			}
		}
	}
	
	@Override
	public List<Chat> listAll(int owner) {
		return chatDao.listAll(owner);
	}
	
	@Override
	public List<Chat> listByDate(Date date, int owner) {
		return chatDao.listByDate(date, owner);
	}

	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setChatDao(ChatDao chatDao) {
		this.chatDao = chatDao;
	}
	
    public void setChatMessenger(ChatMessenger chatMessenger) {
	    this.chatMessenger = chatMessenger;
    }
}
