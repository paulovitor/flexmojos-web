package tv.snews.anews.service.impl;

import tv.snews.anews.dao.PermissionDao;
import tv.snews.anews.domain.Permission;
import tv.snews.anews.service.PermissionService;

/**
 *  Implementação dos serviços de acesso as permissões.
 *  
 * @author Samuel Guedes de Melo.
 * @since 0.01
 */
public class PermissionServiceImpl implements PermissionService {
	
	private PermissionDao permissionDao;
	
	/**
	 * Lista todas as permissões.
	 * @return List<Permission>
	 */
	@Override
	public Permission listAll() {
		//TODO: Incluir cache de segundo nível (ecache) uma vez que esses não serão modificado sem reboot do servidor.
		return permissionDao.listAll();
	}
	
	/**
	 * Atrubui a instância de permissionDao.
	 */
	public void setPermissionDao(PermissionDao permissionDao) {
		this.permissionDao = permissionDao;
	}
}
