package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import tv.snews.anews.dao.ChatDao;
import tv.snews.anews.dao.MessageDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.InvalidChatState;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.ChatMessenger;
import tv.snews.anews.service.MessageService;
import tv.snews.anews.util.ResourceBundle;

import java.util.*;

/**
 * Serviço que implementa o envio de mensagens e arquivos pelo sistema de
 * comunicação.
 * 
 * @author Felipe Pinheiro
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class MessageServiceImpl implements MessageService {

	private static final int EPOCH_YEAR = 1970;

	private ChatDao chatDao;
	private ChatMessenger chatMessenger;
	private MessageDao messageDao;
	private SessionManager sessionManager;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	/**
	 * Epoch Year.
	 */
	public static final long DT_1970;

	static {
		Calendar cal = new GregorianCalendar(EPOCH_YEAR, 0, 1, 0, 0, 0);
		cal.set(Calendar.MILLISECOND, 0);
		DT_1970 = cal.getTimeInMillis();
	}

	@Override
	public TextMessage sendMessage(String content, Chat chat) throws MessageDeliveryException, SessionManagerException, InvalidChatState {
		Assert.notNull(chat);
		Assert.notNull(chat.getId());
		if (chat.getMembers().size() < 2) {
			throw new IllegalArgumentException(bundle.getMessage("message.invalidMembersSize"));
		}

		User sender = sessionManager.getCurrentUser();
		Set<User> recipients = extractRecipients(chat, sender);

		TextMessage message = new TextMessage(chat, content, sender, recipients);
		messageDao.save(message);

		boolean sent = chatMessenger.sendChatMessage(message);
		if (sent) {
			message.setSent(sent);
			return message;
		} else {
			throw new MessageDeliveryException(bundle.getMessage("message.notSent"));
		}
	}

	@Override
	public FileMessage sendFile(UserFile file, Chat chat) throws MessageDeliveryException, SessionManagerException, InvalidChatState {
		Assert.notNull(file);
		Assert.notNull(chat);
		Assert.notNull(chat.getId());

		if (chat.getMembers().size() < 2) {
			throw new IllegalArgumentException(bundle.getMessage("message.invalidMembersSize"));
		}

		User sender = sessionManager.getCurrentUser();
		Set<User> recipients = extractRecipients(chat, sender);

		FileMessage message = new FileMessage(chat, file, sender, recipients);
		messageDao.save(message);

		boolean sent = chatMessenger.sendChatMessage(message);
		if (sent) {
			message.setSent(sent);
			return message;
		} else {
			throw new MessageDeliveryException(bundle.getMessage("message.notSent"));
		}
	}

	@Override
	public EntityMessage sendEntityMessage(Object obj, Chat chat) throws MessageDeliveryException, SessionManagerException, InvalidChatState {
		Assert.notNull(obj);
		Assert.notNull(chat.getId());

		if (chat.getMembers().size() < 2) {
			throw new IllegalArgumentException(bundle.getMessage("message.invalidMembersSize"));
		}

		EntityType entityType = EntityType.valueOf(obj.getClass().getSimpleName().toUpperCase());
		User sender = sessionManager.getCurrentUser();
		Set<User> recipients = extractRecipients(chat, sender);

		EntityMessage message = new EntityMessage(chat, sender, recipients);
		message.setType(entityType);

		switch (entityType) {
			case TWEETINFO:
				TweetInfo tweet = (TweetInfo) obj;
				message.setIdEntity(tweet.getId().intValue());
				message.setDescription("@" + tweet.getTwitterUser().getScreenName());
				break;

			case RSSITEM:
				RssItem rssItem = (RssItem) obj;
				message.setIdEntity(rssItem.getId().intValue());
				message.setDescription(rssItem.getRssFeed().getName() + " - " + rssItem.getTitle());
				break;

			case NEWS:
				News news = (News) obj;
				message.setIdEntity(news.getId().intValue());
				message.setDescription(news.getSlug());
				break;

			case ROUND:
				Round round = (Round) obj;
				message.setIdEntity(round.getId().intValue());
				message.setDescription(round.getSlug());
				break;

			case AGENDACONTACT:
				AgendaContact contact = (AgendaContact) obj;
				message.setIdEntity(contact.getId());
				message.setDescription(contact.getName());
				break;

			case GUIDELINE:
				Guideline guideline = (Guideline) obj;
				message.setIdEntity(guideline.getId().intValue());
				message.setDescription(guideline.getSlug());
				break;

			case REPORTAGE:
				Reportage reportage = (Reportage) obj;
				message.setIdEntity(reportage.getId().intValue());
				message.setDescription(reportage.getSlug());
				break;

			case STORY:
				Story story = (Story) obj;
				message.setIdEntity(story.getId().intValue());
				message.setDescription(StringUtils.isBlank(story.getSlug()) ? bundle.getMessage("defaults.noSlug") : story.getSlug());
				break;

			case REPORT:
				Report report = (Report) obj;
				message.setIdEntity(report.getId().intValue());
				message.setDescription(report.getSlug());
				break;
			case SCRIPT: 
				Script script = (Script) obj;
				message.setIdEntity(script.getId().intValue());
				message.setDescription(StringUtils.isBlank(script.getSlug()) ? bundle.getMessage("defaults.noSlug") : script.getSlug());
				break;

			default:
				throw new RuntimeException("Type not found");
		}

		messageDao.save(message);

		boolean sent = chatMessenger.sendChatMessage(message);
		if (sent) {
			message.setSent(sent);
			return message;
		} else {
			throw new MessageDeliveryException(bundle.getMessage("message.notSent"));
		}
	}

	@Override
	public void markAsReaded(AbstractMessage message) throws SessionManagerException {
		Assert.notNull(message);
		Assert.notNull(message.getId());

		message.markMessageAsReadedTo(sessionManager.getCurrentUser());
		messageDao.update(message);
	}

	@Override
	public void markAsReadedList(List<AbstractMessage> messages) throws SessionManagerException {
		for (AbstractMessage message : messages) {
			message = messageDao.get(message.getId());
			message.markMessageAsReadedTo(sessionManager.getCurrentUser());
			messageDao.update(message);
		}
	}

	@Override
	public List<AbstractMessage> loadOfflineMessages() throws SessionManagerException {
		return messageDao.listUnreadedMessages(sessionManager.getCurrentUser());
	}

	@Override
	public AbstractMessage loadMessageById(long messageId) {
		return messageDao.get(messageId);
	}

	@Override
	public List<AbstractMessage> loadLastMessagesWith(User user, int limit, Date firstMessageChatResult) throws SessionManagerException {
		Assert.notNull(user);

		if (firstMessageChatResult != null && firstMessageChatResult.getTime() < DT_1970) {
			throw new IllegalArgumentException();
		}

		Chat chat = resolveChat(sessionManager.getCurrentUser(), user);
		List<AbstractMessage> messages;
		if (chat == null) {
			// O chat não é encontrado quando não existe nenhuma conversa entre os usuários no histórico de mensagens
			messages = new ArrayList<AbstractMessage>();
		} else {
			messages = messageDao.listLastMessagesFrom(chat, limit, firstMessageChatResult);

			// Como o DAO retorna as mensagens ordenadas pela ordem decrescente da data, é necessário inverter a lista
			Collections.reverse(messages);
		}

		return messages;
	}

	@Override
	public List<AbstractMessage> loadMessagesByChat(Chat chat) throws SessionManagerException {
		List<AbstractMessage> messages = messageDao.listMessagesByChat(chat);

		return messages;
	}

	@Override
	public AbstractMessage getLastMessageOfChat(Chat chat) throws SessionManagerException {
		return messageDao.getLastMessageOfChat(chat);
	}

	@Override
	public void notifyUserIsTyping(boolean typing, Chat chat) throws SessionManagerException {
		Assert.notNull(chat);

		User sender = sessionManager.getCurrentUser();
		chatMessenger.notifyUserIsTyping(sender, chat, typing);
	}

	//----------------------------------
	//	Helpers
	//----------------------------------

	/*
	 * Obtém a lista de usuários ativos do chat (já retirando os que sairam da
	 * conversa) e remove o sender, já que ele não irá se tornar um destinatário
	 * da mensagem.
	 */
	private Set<User> extractRecipients(Chat chat, User sender) throws InvalidChatState {
		Set<User> result = chat.activeMembers();
		result.remove(sender);
		if (result.isEmpty()) {
			throw new InvalidChatState(bundle.getMessage("message.recipientsExtractError"));
		}
		return result;
	}

	private Chat resolveChat(User sender, User... recipients) {
		Set<User> members = new HashSet<>();
		Collections.addAll(members, recipients);
		members.add(sender);

		return chatDao.findByMembers(members);
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	public void setChatDao(ChatDao chatDao) {
		this.chatDao = chatDao;
	}

	public void setMessageDao(MessageDao messageDao) {
		this.messageDao = messageDao;
	}

	public void setChatMessenger(ChatMessenger chatMessenger) {
		this.chatMessenger = chatMessenger;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}
}
