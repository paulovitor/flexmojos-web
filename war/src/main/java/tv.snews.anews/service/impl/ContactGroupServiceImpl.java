package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ContactDao;
import tv.snews.anews.dao.ContactGroupDao;
import tv.snews.anews.domain.ContactGroup;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.messenger.ContactGroupMessenger;
import tv.snews.anews.service.ContactGroupService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;
import java.util.Objects;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * Implementação do serviço de manipulação do grupo de contatos.
 * 
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class ContactGroupServiceImpl implements ContactGroupService {

	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	private ContactDao contactDao;
	private ContactGroupDao contactGroupDao;
	private ContactGroupMessenger contactGroupMessenger;

	@Override
	public List<ContactGroup> listAll() {
		return contactGroupDao.listAll();
	}

	@Override
	public void save(ContactGroup group) throws ValidationException {
		Objects.requireNonNull(group, bundle.getMessage("agenda.group.isNull"));
		Objects.requireNonNull(group.getName(), bundle.getMessage("agenda.group.nameIsNull"));
		
		ContactGroup current = contactGroupDao.findByName(group.getName());
		if (notValidId(group.getId())) {
			if (current == null) {
				// Tudo ok, pode salvar
				contactGroupDao.save(group);
				contactGroupMessenger.notifyInsert(group);
			} else {
				// Nome duplicado
				throw new ValidationException(bundle.getMessage("agenda.group.duplicatedName"));
			}
		} else {
			if (current == null || current.getId() == group.getId()) {
				// Teve mudança no nome: busca o registro do banco por causa do lazy de contacts
				contactGroupDao.update(group);
				contactGroupMessenger.notifyUpdate(group);
			} else {
				// Nome duplicado
				throw new ValidationException(bundle.getMessage("agenda.group.duplicatedName"));
			}
		}
	}

	@Override
	public void deleteById(Integer id) throws IllegalOperationException {
		ContactGroup group = contactGroupDao.get(id);
		Objects.requireNonNull(group, bundle.getMessage("agenda.group.isNull"));

		if (contactDao.countByContactGroup(group) > 0) {
			throw new IllegalOperationException(bundle.getMessage("agenda.group.cannotDelete"));
		}
		
		contactGroupDao.delete(group);
		contactGroupMessenger.notifyDelete(group);
	}

    @Override
    public ContactGroup findOrCreateByName(String name) {
        ContactGroup group = contactGroupDao.findByName(name);
        if (group == null) {
            group = new ContactGroup();
            group.setName(name);
            save(group);
        }
        return group;
    }

    //----------------------------------
	//	Setters
	//----------------------------------
	
    public void setContactDao(ContactDao contactDao) {
	    this.contactDao = contactDao;
    }
	
    public void setContactGroupDao(ContactGroupDao contactGroupDao) {
    	this.contactGroupDao = contactGroupDao;
    }

    public void setContactGroupMessenger(ContactGroupMessenger contactGroupMessenger) {
	    this.contactGroupMessenger = contactGroupMessenger;
    }
}
