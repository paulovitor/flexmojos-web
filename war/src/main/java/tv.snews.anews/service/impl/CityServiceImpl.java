package tv.snews.anews.service.impl;

import tv.snews.anews.dao.CityDao;
import tv.snews.anews.domain.City;
import tv.snews.anews.service.CityService;

import java.io.Serializable;
import java.util.List;

/**
 * Implementação do Serviço de manipulação de cidades.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public class CityServiceImpl implements CityService {

	private CityDao cityDao;

	@Override
	public List<City> listAll(Serializable idState) {
		return cityDao.listAll(idState);
	}

	@Override
	public City get(Serializable idCity) {
		return cityDao.get(idCity);
	}

	@Override
	public void save(City city) {
		if (city != null && city.getId() != null && city.getId() > 0) {
			cityDao.update(city);
		} else {
			cityDao.save(city);
		}
	}

	@Override
	public void remove(City city) {
		if (city != null && city.getId() != null && city.getId() > 0) {
			cityDao.delete(city);
		}
	}

	//----------------------------------
	//	Setters
	//----------------------------------

	public void setCityDao(CityDao cityDao) {
		this.cityDao = cityDao;
	}
}
