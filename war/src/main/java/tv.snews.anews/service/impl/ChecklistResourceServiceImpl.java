package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ChecklistDao;
import tv.snews.anews.dao.ChecklistResourceDao;
import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.ChecklistResourceMessenger;
import tv.snews.anews.service.ChecklistResourceService;
import tv.snews.anews.util.EntityUtil;

import java.util.List;

/**
 * Implementação padrão da interface do serviço {@link ChecklistResourceService}.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public class ChecklistResourceServiceImpl implements ChecklistResourceService {

	private ChecklistDao checklistDao;
	private ChecklistResourceDao checklistResourceDao;
	private ChecklistResourceMessenger checklistResourceMessenger;
	
	@Override
	public List<ChecklistResource> listAll() {
		return checklistResourceDao.findAll();
	}

	@Override
	public void save(ChecklistResource checklistResource) {
		if (EntityUtil.notValidId(checklistResource.getId())) {
			checklistResourceDao.save(checklistResource);
			checklistResourceMessenger.notifyInsert(checklistResource);
		} else {
			checklistResourceDao.update(checklistResource);
			checklistResourceMessenger.notifyUpdate(checklistResource);
		}
	}

	@Override
	public void remove(Integer id) throws IllegalOperationException {
		if (isInUse(id)) {
			throw new IllegalOperationException("Cannot delete a report nature that's in use.");
		}
		ChecklistResource checklistResource = checklistResourceDao.get(id);
		checklistResourceDao.delete(checklistResource);
		checklistResourceMessenger.notifyDelete(checklistResource);
	}

	@Override
	public boolean isInUse(Integer id) {
		ChecklistResource checklistResource = checklistResourceDao.get(id);
	    return checklistDao.countAllByResource(checklistResource) != 0;
	}
	
	//------------------------------------
	//  Setters
	//------------------------------------
	
    public void setChecklistDao(ChecklistDao checklistDao) {
	    this.checklistDao = checklistDao;
    }
	
    public void setChecklistResourceDao(ChecklistResourceDao checklistResourceDao) {
	    this.checklistResourceDao = checklistResourceDao;
    }
    
    public void setChecklistResourceMessenger(ChecklistResourceMessenger checklistResourceMessenger) {
	    this.checklistResourceMessenger = checklistResourceMessenger;
    }
}
