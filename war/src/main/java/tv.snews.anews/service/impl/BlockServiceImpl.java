package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import tv.snews.anews.dao.BlockDao;
import tv.snews.anews.dao.RundownDao;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.BlockAction;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Story;
import tv.snews.anews.event.BlockEvent;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.messenger.BlockMessenger;
import tv.snews.anews.service.BlockService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.anews.util.ResourceBundle;

import java.util.Date;
import java.util.List;

/**
 * Implementação do serviço de manipulação de blocks.
 * 
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class BlockServiceImpl implements BlockService, ApplicationEventPublisherAware {

	private static final String DEFAULTS_REQUIRED_FIELDS = "defaults.requiredFields";
	private static final Logger log = LoggerFactory.getLogger(BlockServiceImpl.class);
	
	private BlockDao blockDao;
	private RundownDao rundownDao;
	
	private StoryService storyService;
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private BlockMessenger blockMessenger;
	
	/** Despacha eventos **/ 
	private ApplicationEventPublisher publisher;
	
	@Override
	public synchronized void createBlock(long rundowId) throws ValidationException {
		Rundown rundown = rundownDao.get(rundowId);
		if (rundown == null) {
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}

		Block block = new Block();
		Block standBy = rundown.findStandBy();

        // FIXME Realmente existe a possibilidade de não ter um Stand By?
		if (standBy == null) {
			block.setStandBy(true);
			block.setOrder(0);
		} else {
			standBy.setOrder(rundown.getBlocks().size());
			save(standBy);
			blockDao.flush();
			
			block.setStandBy(false);
			block.setOrder(rundown.getBlocks().size() - 1);
		}
		
		block.setRundown(rundown);
		block.setCommercial(DateTimeUtil.getMidnightTime());
		
		save(block);
		dispatchEvent(new BlockEvent(BlockAction.INSERTED, block, this));
	}

	@Override
	public synchronized void removeBlock(long blockId) throws ValidationException {
		Block block = blockDao.get(blockId);

		if (block == null) {
			throw new ValidationException(bundle.getMessage("defaults.msg.argumentsFail"));
		} else {
			//Remove as laudas do Block
			removeStoriesOfBlock(block.getId());

			//Remove o block
			blockDao.delete(block);
			blockDao.flush();
			
			//Ordena os Blocos que ficaram
			sortBlocks(block.getRundown().getId());
			
			blockMessenger.notifyDelete(block);
			dispatchEvent(new BlockEvent(BlockAction.EXCLUDED, block, this));
		}
	}

	@Override
	public void updateBlockTime(long blockId, Date blockTime) throws ValidationException {
		Block block = blockDao.get(blockId);

		if (block == null) {
			String errorMessage = bundle.getMessage("defaults.msg.argumentsFail");
			log.error(errorMessage);
			throw new ValidationException(errorMessage);
		} else {
			block.setCommercial(blockTime);
			save(block);
			dispatchEvent(new BlockEvent(BlockAction.BREAK_CHANGED, block, this));
		}
	}

	private void removeStoriesOfBlock(long blockId) {
		Block block = blockDao.get(blockId);
		
		for (Story story : block.getStories()) {
			storyService.remove(story);
//			story.setExcluded(true);
//			story.setBlock(null);
//			storyDao.update(story);
		}
	}

	private void sortBlocks(long rundownId) {
		Rundown rundown = rundownDao.get(rundownId);
		List<Block> blocks = blockDao.listAll(rundown);

		for (int i = 0; i < blocks.size(); i++) {
			Block block = blocks.get(i);

			if (block.getOrder() != i) {
				block.setOrder(i);
				blockDao.save(block);
			}
		}
	}

	private void save(Block block) throws ValidationException {
		validateFields(block);
		if (block.getId() != null && block.getId() > 0) {
			blockDao.update(block);
			blockMessenger.notifyUpdate(block);
		} else {
			blockDao.save(block);
			blockMessenger.notifyInsert(block);
		}
	}

	/**
	 * Valida campos obrigatórios dos blocks.
	 */

	private void validateFields(Block block) throws ValidationException {
		if (block == null || block.getRundown() == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	private void dispatchEvent(BlockEvent blockEvent) {
		publisher.publishEvent(blockEvent);
	}

	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setBlockDao(BlockDao blockDao) {
		this.blockDao = blockDao;
	}

    public void setStoryService(StoryService storyService) {
	    this.storyService = storyService;
    }
    
	public void setRundownDao(RundownDao rundownDao) {
		this.rundownDao = rundownDao;
	}

	public void setBlockMessenger(BlockMessenger blockMessenger) {
		this.blockMessenger = blockMessenger;
	}
	
    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
	    this.publisher = publisher;
    }
}
