package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ReportDao;
import tv.snews.anews.dao.ReportNatureDao;
import tv.snews.anews.domain.ReportNature;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.ReportNatureMessenger;
import tv.snews.anews.service.ReportNatureService;

import java.util.List;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * Implementação padrão da interface do serviço {@link ReportNatureService}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportNatureServiceImpl implements ReportNatureService {

	private ReportDao reportDao;
	private ReportNatureDao reportNatureDao;
	private ReportNatureMessenger reportNatureMessenger;
	
	@Override
	public List<ReportNature> listAll() {
		return reportNatureDao.findAll();
	}

	@Override
	public void save(ReportNature nature) {
		if (notValidId(nature.getId())) {
			reportNatureDao.save(nature);
			reportNatureMessenger.notifyInsert(nature);
		} else {
			reportNatureDao.update(nature);
			reportNatureMessenger.notifyUpdate(nature);
		}
	}

	@Override
	public void remove(Integer id) throws IllegalOperationException {
		if (isInUse(id)) {
			throw new IllegalOperationException("Cannot delete a report nature that's in use.");
		}
		ReportNature nature = reportNatureDao.get(id);
		reportNatureDao.delete(nature);
		reportNatureMessenger.notifyDelete(nature);
	}

	@Override
	public boolean isInUse(Integer id) {
		ReportNature nature = reportNatureDao.get(id);
	    return reportDao.countAllByNature(nature) != 0;
	}
	
	//------------------------------------
	//  Setters
	//------------------------------------
	
    public void setReportDao(ReportDao reportDao) {
	    this.reportDao = reportDao;
    }
	
    public void setReportNatureDao(ReportNatureDao reportNatureDao) {
	    this.reportNatureDao = reportNatureDao;
    }
    
    public void setReportNatureMessenger(ReportNatureMessenger reportNatureMessenger) {
	    this.reportNatureMessenger = reportNatureMessenger;
    }
}
