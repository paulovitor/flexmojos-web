package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.RundownEvent;
import tv.snews.anews.event.StoryEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.infra.ClipboardAction;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.infra.intelligentinterface.IntelligentInterface;
import tv.snews.anews.infra.intelligentinterface.TelnetCommand;
import tv.snews.anews.infra.ws.MediaPortalWebService;
import tv.snews.anews.messenger.IntelligentInterfaceMessenger;
import tv.snews.anews.messenger.RundownMessenger;
import tv.snews.anews.messenger.StoryMessenger;
import tv.snews.anews.service.RundownService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.intelligentinterface.TelnetListener;
import tv.snews.intelligentinterface.exception.TelnetException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.lang.StringUtils.isNotBlank;
import static tv.snews.anews.domain.RundownAction.*;
import static tv.snews.anews.domain.StoryAction.PAGE_CHANGED;

/**
 * Implementação padrão da interface {@link RundownService}.
 *
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */

public class RundownServiceImpl implements RundownService, TelnetListener, ApplicationEventPublisherAware {

    private static final Logger log = LoggerFactory.getLogger(RundownServiceImpl.class);

    private final TelnetCommand telnetCommand;
    private final EntityLocker<Story> locker;

    private StoryDao storyDao;
    private StoryService storyService;
    private BlockDao blockDao;
    private RundownDao rundownDao;
    private ProgramDao programDao;
    private RundownTemplateDao rundownTemplateDao;
	private WSMediaDao wsMediaDao;

    private RundownMessenger rundownMessenger;

    private StoryMessenger storyMessenger;
    private SessionManager sessionManager;
    private IntelligentInterfaceMessenger intelligentInterfaceMessenger;
    private IntelligentInterface intelligentInterface;

    private ResourceBundle bundle = ResourceBundle.INSTANCE;
    private User intelligentInterfaceUser;
    private ApplicationEventPublisher publisher;

	private MediaPortalWebService mediaPortalWebService;

    public RundownServiceImpl() {
        locker = EntityLockerFactory.getLocker(Story.class);
        telnetCommand = new TelnetCommand();
    }

	@Override
	public Rundown loadRundown(Date date, Integer programId, boolean fetchAll) throws ValidationException {
		System.out.println("LOADING " + new Date());
		Program program = programDao.get(programId);

		if (date == null || program == null) {
			String errorMessage = ""; // bundle.getMessage("defaults.msg.requiredFields");
			log.error(errorMessage);
			throw new ValidationException(errorMessage);
		}

		Rundown rundown = rundownDao.findByDateAndProgram(date, program, fetchAll);

		if (rundown != null) {
			for (Block block : rundown.getBlocks()) {
				for (Story story : block.getStories()) {
					story.setEditingUser(locker.findOwner(story));

					if(sessionManager.containsOnClipboard(story.getId())) {
						ClipboardAction action = sessionManager.clipboardAction();
						story.setCutted(action == ClipboardAction.CUT);
						story.setCopied(action == ClipboardAction.COPY);
					}
				}
			}
		}

		System.out.println("   DONE " + new Date());

		return rundown;
	}

    @Override
    public synchronized Rundown createRundown(Date date, Integer programId, Integer templateId) throws ValidationException {
        Program program = programDao.get(programId);
        RundownTemplate template = templateId != null && templateId > 0 ? rundownTemplateDao.get(templateId) : null;

        if (date == null || program == null) {
            String errorMessage = bundle.getMessage("defaults.msg.argumentsFail");
            throw new ValidationException(errorMessage);
        } else {
            if (verifyRundown(date, program.getId()) == RundownStatus.NOT_EXIST) {
                User author = sessionManager.getCurrentUser();

                Rundown rundown = new Rundown(template, author);
                rundown.setDate(date);
                rundown.setProgram(program);
                rundown.setStart(program.getStart());
                rundown.setEnd(program.getEnd());
                rundown.setProduction(null); // = 00:00:00
                rundown.setId((Long) rundownDao.save(rundown)); // necessário para não quebrar testes

                Block standBy = rundown.findStandBy();
                if (standBy == null) {
                    standBy = new Block();
                    standBy.setRundown(rundown);
                    standBy.setOrder(0);
                    standBy.setCommercial(DateTimeUtil.getMidnightTime());
                    standBy.setStandBy(true);
                    blockDao.save(standBy);

                    rundown.getBlocks().add(standBy);
                }

                // Dispara evento para log & notificação MOS
                dispatchEvent(new RundownEvent(RundownAction.CREATED, rundown, this));

                return rundown;
            } else {
                String errorMessage = bundle.getMessage("defaults.msg.saveFail");
                throw new ValidationException(errorMessage);
            }
        }
    }

    @Override
    public synchronized RundownStatus verifyRundown(Date date, Integer programId) throws ValidationException {
        Program program = programDao.get(programId);

        if (date == null || program == null) {
            String errorMessage = bundle.getMessage("defaults.msg.argumentsFail");
            log.error(errorMessage);
            throw new ValidationException(errorMessage);
        }

        int countRoundow = rundownDao.countRundown(date, program);
        if (countRoundow == 0) { //não achou
            Date today = DateTimeUtil.minimizeTime(new Date());

            if (date.equals(today) || date.after(today)) {
                // Se a data for maior ou igual a data de hoje, pode criar
                return RundownStatus.NOT_EXIST;
            } else {
                // Data antiga, não pode criar
                return RundownStatus.CANT_CREATE;
            }
        } else {
            return RundownStatus.EXIST;
        }
    }

    @Override
    public Date currentTime() {
        return DateTimeUtil.getCurrentTime();
    }

	@Override
	public void resetDisplayOfStories(Long rundownId) throws IllegalOperationException {
		Rundown rundown = rundownDao.get(rundownId);
		rundown.setDisplayed(false);

		for (Block block : rundown.getBlocks()) {
			for (Story story : block.getStories()) {
				if (story.isDisplayed() || story.isDisplaying()) {
					story.setDisplaying(false);
					story.setDisplayed(false);
					storyDao.save(story);
				}
			}
		}

		rundownDao.save(rundown);
		rundownDao.flush();

		rundownMessenger.notifyDisplayResets(rundown);
	}

	@Override
	public void completeStoriesDisplay(Long rundownId) throws IllegalOperationException {
		Rundown rundown = rundownDao.get(rundownId);
		rundown.setDisplayed(true);

		for (Block block : rundown.getBlocks()) {
			for (Story story : block.getStories()) {
				story.setEditingUser(null);
				if (story.isDisplaying()) {
					story.setDisplaying(false);
					story.setDisplayed(true);
					storyDao.save(story);
					break;
				}
			}
		}
		
		rundownDao.save(rundown);
		rundownDao.flush();

		rundownMessenger.notifyCompleteDisplay(rundown);
	}

    @Override
    public void updateField(Long rundownId, String field, Object object) throws ValidationException {
        Rundown rundown = rundownDao.get(rundownId);

        RundownEvent rundownEvent = null;
        switch (Rundown.Fields.valueOf(field.toUpperCase())) {
            case START:
                rundownEvent = new RundownEvent(START_CHANGED, rundown, this);
                rundown.setStart((Date) object);
                break;
            case END:
                rundownEvent = new RundownEvent(END_CHANGED, rundown, this);
                rundown.setEnd((Date) object);
                break;
            case PRODUCTION:
                rundownEvent = new RundownEvent(PRODUCTION_CHANGED, rundown, this);
                rundown.setProduction((Date) object);
                break;
            case DATE:
                rundownEvent = new RundownEvent(DATE_CHANGED, rundown, this);
                rundown.setDate((Date) object);
                rundown.updateStoriesDates();
                break;
        }

			rundownDao.save(rundown);
			rundownDao.flush();

        // Dispara evento para log & notificação MOS
        if (rundownEvent != null) {
            dispatchEvent(rundownEvent);
        }

        rundownMessenger.notifyUpdateField(rundownId, field, object);
    }

    @Override
    public synchronized void sortStories(Long rundownId) {
        Rundown rundown = rundownDao.get(rundownId);
        int page = 0;

        for (Block block : rundown.getBlocks()) {
            for (Story story : block.getStories()) {
                String newPage = ++page < 10 ? "0" + page : "" + page;
                if (!story.getPage().equals(newPage)) {
                    StoryEvent storyEvent = new StoryEvent(PAGE_CHANGED, this);
                    storyEvent.registerChange(story, story.getPage(), newPage);

                    story.setPage(newPage);

                    storyDao.save(story);
                    storyDao.flush();

                    // Dispara evento para log & MOS
                    dispatchEvent(storyEvent);
                }
            }
        }
        rundownMessenger.notifySort(rundown);
    }

    @Override
    public String generateTeleprompterText(Long rundownId) {
        Rundown rundown = rundownDao.get(rundownId);
        StringBuilder builder = new StringBuilder();

        SimpleDateFormat timeFormatter = new SimpleDateFormat("mm:ss");

        String br = "<br/>";

        String pageStart = "<pageMarkup>--- ";
        String pageEnd = " ---</pageMarkup>";

        String commentStart = "<commentMarkup>[";
        String commentEnd = "]</commentMarkup>";

        String textStart = "<textMarkup>";
        String textEnd = "</textMarkup>";

        for (Block block : rundown.getBlocks()) {
            for (Story story : block.getStories()) {
                builder.append(pageStart).append("PÁGINA").append(" ").append(story.getPage()).append(pageEnd).append(br);

                // Head Section
                if (story.getHeadSection() != null) {
                    for (StorySubSection subSection : story.getHeadSection().getSubSections()) {
                        if (subSection instanceof StoryCameraText) {
                            StoryCameraText camText = (StoryCameraText) subSection;

                            String presenter = camText.getPresenter().getNickname().toUpperCase();
                            String text = camText.getText().toUpperCase();

                            builder.append(commentStart).append(presenter).append(commentEnd);

                            if (camText.getCamera() > 0) {
                                builder.append(" ").append(commentStart).append("CAM").append(" ").append(camText.getCamera()).append(commentEnd);
                            }
                            builder.append(br);

                            builder.append(textStart).append(text).append(textEnd).append(br);
                        }
                    }
                }

                // NC Section
                if (story.getNcSection() != null) {
                    builder.append(commentStart).append("ENTRA IMAGEM").append(commentEnd).append(br);
                    for (StorySubSection subSection : story.getNcSection().getSubSections()) {
                        if (subSection instanceof StoryText) {
                            StoryText storyText = (StoryText) subSection;

                            String presenter = storyText.getPresenter().getNickname().toUpperCase();
                            String text = storyText.getText().toUpperCase();

                            builder.append(commentStart).append(presenter).append(commentEnd).append(br);
                            builder.append(textStart).append(text).append(textEnd).append(br);
                        }
                    }
                    builder.append(commentStart).append("SAI IMAGEM").append(commentEnd).append(br);
                }

                if (story.getVtSection() != null) {
                    Date vtTime = story.getVtSection().getDuration();
                    String vtStr = vtTime == null ? null : timeFormatter.format(vtTime);
                    if (vtStr != null && !vtStr.equals("00:00")) {
                        builder.append(commentStart).append("RODA VT").append(" ").append(vtStr).append(commentEnd).append(br);

                        // Deixa (pode ser nula se apenas informou o tempo pelo espelho)
                        if (isNotBlank(story.getVtSection().getCue())) {
                            builder.append(commentStart).append("DEIXA").append(" ").append(story.getVtSection().getCue()).append(commentEnd).append(br);
                        }
                    }
                }

                // Footer Section
                if (story.getFooterSection() != null) {
                    for (StorySubSection subSection : story.getFooterSection().getSubSections()) {
                        if (subSection instanceof StoryCameraText) {
                            StoryCameraText camText = (StoryCameraText) subSection;

                            String presenter = camText.getPresenter().getNickname().toUpperCase();
                            String text = camText.getText().toUpperCase();

                            builder.append(commentStart).append(presenter).append(commentEnd);

                            if (camText.getCamera() > 0) {
                                builder.append(" ").append(commentStart).append("CAM").append(" ").append(camText.getCamera()).append(commentEnd);
                            }
                            builder.append(br);

                            builder.append(textStart).append(text).append(textEnd).append(br);
                        }
                    }
                }

                builder.append(br);
            }
        }

        return builder.toString();
    }

    @Override
    public void synchronizeStatusVideosWS(Long rundownId) throws RemoteException, ServiceException, WebServiceException {
        Rundown rundown = rundownDao.get(rundownId);
        for (Block block : rundown.getBlocks()) {
        	for (Story story : block.getStories()) {
        		if (story.getVtSection() != null) {
        			for (StoryWSVideo vtWSVideo : story.getVtSection().subSectionsOfType(StoryWSVideo.class)) {
        				if (vtWSVideo.getMedia() != null) {
                            storyService.loadAndUpdateStoryWSVideo(story.getId(), vtWSVideo);
        				}
        			}
        		}

        		if (story.getNcSection() != null) {
        			for (StoryWSVideo ncWSVideo : story.getNcSection().subSectionsOfType(StoryWSVideo.class)) {
        				if (ncWSVideo.getMedia() != null) {
                            storyService.loadAndUpdateStoryWSVideo(story.getId(), ncWSVideo);
        				}
        			}
        		}
        	}
        }
    }


    @Override
    public void sendCredits(Long rundownId) throws SessionManagerException, IOException, TelnetException {
        Rundown rundown = rundownDao.get(rundownId);
        Program program = rundown.getProgram();

        if (program.getCgDevice() instanceof IIDevice) {
            IIDevice device = (IIDevice) program.getCgDevice();
            if (!device.isVirtual()) {
                sendCreditsToIIDevice(rundown, device);
            }
        }
    }

    private void sendCreditsToIIDevice(Rundown rundown, IIDevice device) throws IOException, TelnetException {
        Set<String> commands = new HashSet<>();
        intelligentInterfaceUser = sessionManager.getCurrentUser();

        commands.add(telnetCommand.selectMessageDirectory(rundown.getProgram().getCgPath().getPath()));

        int code = device.getIncrement();
        for (Block block : rundown.getBlocks()) {
            for (Story story : block.getStories()) {
                if (story.isApproved()) {
                    code = updateStoryCGs(commands, code, story, rundown.getId());
                }
            }
        }

        if (commands.size() > 1)
            intelligentInterface.sendCredits(device, commands);
    }

    private int updateStoryCGs(Set<String> commands, int code, Story story, long rundownId) {
        boolean updateStoryCGs = false;
        for (StoryCG cg : story.subSectionsOfType(StoryCG.class)) {
            if (!cg.isComment() && !cg.isDisabled()) {
                cg.setCode(code++);
                commands.add(telnetCommand.getWrite(cg));
                updateStoryCGs = true;
            }
        }
        if (updateStoryCGs) {
            storyDao.update(story);
            storyMessenger.notifyUpdate(story, rundownId);
        }
        return code;
    }

    @Override
    public void onResponseReceived(String command, String response) {
        log.debug(bundle.getMessage("cg.command") + ": " + command + bundle.getMessage("cg.response") + ": " + response);
    }

    @Override
    public void onExceptionReceived(TelnetException telnetException) {
        log.debug(telnetException.getMessage());
        intelligentInterfaceMessenger.notifySendError(telnetException.getMessage(), intelligentInterfaceUser);
    }

    @Override
	public List<Integer> loadAssetsIds(Long rundownId) throws RemoteException, ServiceException, WebServiceException {
		Rundown rundown = rundownDao.get(rundownId);
		List<StoryWSVideo> videos = rundown.storiesSubSectionsOfType(StoryWSVideo.class, true);

		List<Integer> assetIds = new ArrayList<>();
		for (StoryWSVideo video : videos) {
			WSMedia media = video.getMedia();
			WSDevice device = (WSDevice) media.getDevice();

			if (media != null) {
				assetIds.add(video.getMedia().getId());

				// Atualiza o fileName da mídia
				String fileName = mediaPortalWebService.getFilename(device, media.getAssetId());
				media.setFileName(fileName);
				wsMediaDao.update(media);
			}
		}

		return assetIds;
	}

	@Override
    public int totalRundowns() {
        return rundownDao.count();
    }

    @Override
    public List<Rundown> listRundowns(Integer firstResult, Integer maxResults) {
        return rundownDao.paginatedList(firstResult, maxResults);
    }

    private void dispatchEvent(RundownEvent rundownEvent) {
        publisher.publishEvent(rundownEvent);
    }

    private void dispatchEvent(StoryEvent storyEvent) {
        publisher.publishEvent(storyEvent);
    }

    //--------------------------------------------------------------------------
    //	Getters & Setters
    //--------------------------------------------------------------------------

    public void setStoryDao(StoryDao storyDao) {
        this.storyDao = storyDao;
    }

    public void setStoryService(StoryService storyService) {
        this.storyService = storyService;
    }

    public void setBlockDao(BlockDao blockDao) {
        this.blockDao = blockDao;
    }

    public void setRundownDao(RundownDao rundownDao) {
        this.rundownDao = rundownDao;
    }

    public void setProgramDao(ProgramDao programDao) {
        this.programDao = programDao;
    }

    public void setRundownTemplateDao(RundownTemplateDao rundownTemplateDao) {
        this.rundownTemplateDao = rundownTemplateDao;
    }

	public void setWsMediaDao(WSMediaDao wsMediaDao) {
		this.wsMediaDao = wsMediaDao;
	}

    public void setRundownMessenger(RundownMessenger rundownMessenger) {
        this.rundownMessenger = rundownMessenger;
    }

    public void setStoryMessenger(StoryMessenger storyMessenger) {
        this.storyMessenger = storyMessenger;
    }

    public void setSessionManager(SessionManager value) {
        this.sessionManager = value;
    }

    public void setIntelligentInterface(IntelligentInterface intelligentInterface) {
        this.intelligentInterface = intelligentInterface;
    }

    public void setIntelligentInterfaceMessenger(IntelligentInterfaceMessenger intelligentInterfaceMessenger) {
        this.intelligentInterfaceMessenger = intelligentInterfaceMessenger;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public void setMediaPortalWebService(MediaPortalWebService mediaPortalWebService) {
    	this.mediaPortalWebService = mediaPortalWebService;
    }
}
