package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tv.snews.anews.dao.ContactDao;
import tv.snews.anews.dao.ContactNumberDao;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.ContactNumber;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.messenger.ContactMessenger;
import tv.snews.anews.service.ContactService;
import tv.snews.anews.util.ResourceBundle;

import java.io.Serializable;
import java.util.List;

/**
 * @author Samuel Guedes
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class ContactServiceImpl implements ContactService {

	private static final String DEFAULTS_REQUIRED_FIELDS = "defaults.requiredFields";
	private static final Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	private ContactDao contactDao;
	private ContactNumberDao contactNumberDao;

	private ContactMessenger contactMessenger;

	@Override
	public int countAll() {
		return contactDao.countByName(null);
	}

	@Override
	public List<AgendaContact> findAll(int firstResult, int maxResult) {
		return contactDao.findByName(null, firstResult, maxResult);
	}

	@Override
	public int countByName(String name) {
		return contactDao.countByName(name);
	}

	@Override
	public List<AgendaContact> findByName(String name, int firstResult, int maxResult) {
		return contactDao.findByName(name, firstResult, maxResult);
	}

	@Override
	public Contact save(Contact contact) throws ValidationException {
		if (contact == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}

		validateFields(contact);

		if (contact.getId() != null && contact.getId() > 0) {
			/*
			 * Precisa-se deletar os números que não existem mais no contato
			 * (filhos orfão) manualmente, porque em mapeamentos unidirecionais
			 * não existe relação de pai/filho e consequentemente não é possível
			 * utilizar a cascata "delete-orphan" para matar todos os filhos
			 * orfãos. Contact e ContactNumber possuem uma relação de
			 * agregração.
			 */
			Contact parent = contactDao.get(contact.getId());
			contactDao.evict(parent); // REMOVE DA SESSÃO.

			parent.setName(contact.getName());
			parent.setProfession(contact.getProfession());
			
			//copia os campos a mais de agenda contact
			if (parent instanceof AgendaContact && contact instanceof AgendaContact) {
				AgendaContact oldContact = (AgendaContact) parent;
				AgendaContact newContact =  (AgendaContact) contact;
				oldContact.setAddress(newContact.getAddress());
				oldContact.setCity(newContact.getCity());
				oldContact.setContactGroup(newContact.getContactGroup());
				oldContact.setEmail(newContact.getEmail());
				oldContact.setExcluded(newContact.isExcluded());
				oldContact.setInfo(newContact.getInfo());
				oldContact.setCompany(newContact.getCompany());
				parent = (Contact) oldContact;
			}
			
			for (ContactNumber child : parent.getNumbers()) {  //Percorre lista que esta no banco.

				// Se a lista atual não contém o objeto que existia na lista anterior // deleta o número.
				if (!contact.getNumbers().contains(child)) {
					contactNumberDao.delete(child);
				}
			}
			
			parent.setNumbers(contact.getNumbers());
			//parent.setGuides(contact.getGuides());
			parent = contactDao.merge(parent);

			contactMessenger.notifyContactUpdated(parent);
		} else {
			contact.setId((Integer) contactDao.save(contact));
			contactMessenger.notifyContactInserted(contact);
		}

		return contact;
	}

	@Override
	public AgendaContact save(AgendaContact contact) throws ValidationException {
		return (AgendaContact) save((Contact) contact);
	}

	@Override
	public void delete(Contact contact) {
		Contact dbContact = contactDao.get(contact.getId());
		if (dbContact instanceof AgendaContact) {
			AgendaContact agendaContact = (AgendaContact) dbContact;
			agendaContact.setExcluded(true);
			contactDao.update(agendaContact);
		} else if (dbContact != null) {
			contactDao.delete(dbContact);
		}
		contactMessenger.notifyContactDeleted(contact);
	}

	@Override
	public Contact get(Serializable idContact) {
		return contactDao.get(idContact);
	}

	/**
	 * Valida campos obrigatórios do contato.
	 *
	 * @param contact
	 * @throws ValidationException
	 */
	private void validateFields(Contact contact) throws ValidationException {
		if (contact == null || contact.getName() == null || contact.getName().equals("")) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	@Override
	public AgendaContact findAgendaContactById(Integer idEntity) {
		return (AgendaContact) contactDao.get(idEntity);
	}

	//----------------------------------
	//	Setters
	//----------------------------------

	public void setContactDao(ContactDao contactDao) {
		this.contactDao = contactDao;
	}

	public void setContactNumberDao(ContactNumberDao contactNumberDao) {
		this.contactNumberDao = contactNumberDao;
	}

	public void setContactMessenger(ContactMessenger contactMessenger) {
		this.contactMessenger = contactMessenger;
	}
}
