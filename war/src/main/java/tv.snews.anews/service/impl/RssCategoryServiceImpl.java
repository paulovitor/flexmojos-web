package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.dao.RssCategoryDao;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.messenger.RssMessenger;
import tv.snews.anews.service.RssCategoryService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

/**
 * Implementação da interface {@link tv.snews.anews.service.RssCategoryService}.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssCategoryServiceImpl implements RssCategoryService {

	private RssCategoryDao rssCategoryDao;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	private RssMessenger rssMessenger;

	@Override
	public Integer insert(RssCategory category) throws ValidationException {
		validate(category);
		rssMessenger.notifyCategoryInserted(category);

		return (Integer) rssCategoryDao.save(category);
	}

	@Override
	public void update(RssCategory category) throws ValidationException {
		if (category.getId() == null) {
			throw new IllegalArgumentException(bundle.getMessage("rss.category.upgradeIdError"));
		}
		validate(category);
		rssCategoryDao.update(category);

		rssMessenger.notifyCategoryUpdated(category);
	}

	@Override
	public void remove(RssCategory category) throws IllegalOperationException {
		if (category.getId() == null) {
			throw new IllegalArgumentException(bundle.getMessage("rss.category.deleteIdError"));
		}
		if (!category.getFeeds().isEmpty()) {
			throw new IllegalOperationException(bundle.getMessage("rss.category.notEmptyCategory"));
		}
		rssCategoryDao.delete(category);

		rssMessenger.notifyCategoryDeleted(category);
	}

	@Override
	public List<RssCategory> listAll() {
		return rssCategoryDao.list();
	}

	//----------------------------------
	//	Helpers
	//----------------------------------

	private void validate(RssCategory category) throws ValidationException {
		// Nome não pode ser null ou vazio
		if (StringUtils.isBlank(category.getName())) {
			throw new ValidationException(bundle.getMessage("rss.category.nullOrEmptyName"));
		}
		// Nome deve ser único
		if (category.getId() == null) {
			if (rssCategoryDao.findByName(category.getName()) != null) {
				throw new ValidationException(bundle.getMessage("rss.category.uniqueNameError"));
			}
		} else {
			if (rssCategoryDao.findByName(category.getName(), category) != null) {
				throw new ValidationException(bundle.getMessage("rss.category.uniqueNameError"));
			}
		}
	}

	//----------------------------------
	//	Setters
	//----------------------------------

    /**
     * @param rssDao Implementação do {@link tv.snews.anews.dao.RssCategoryDao}
     */
    public void setRssCategoryDao(RssCategoryDao rssCategoryDao) {
	    this.rssCategoryDao = rssCategoryDao;
    }
    
    /**
     * @param rssMessenger Implementação do mensageiro de notificações RSS.
     */
    public void setRssMessenger(RssMessenger rssMessenger) {
	    this.rssMessenger = rssMessenger;
    }
}
