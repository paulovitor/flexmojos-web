package tv.snews.anews.service.impl;

import tv.snews.anews.dao.StoryDao;
import tv.snews.anews.dao.StoryKindDao;
import tv.snews.anews.domain.StoryKind;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.StoryKindMessenger;
import tv.snews.anews.service.StoryKindService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

/**
 * @author Samuel Guedes de Melo
 * @since 1.2.5
 */
public class StoryKindServiceImpl implements StoryKindService {

	private StoryKindDao storyKindDao;
	private StoryDao storyDao;
	private StoryKindMessenger storyKindMessenger;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@Override
	public void save(StoryKind storyKind) {
		//Seta false ao antigo StoryKind padrão.
		StoryKind oldDefault = storyKindDao.getDefaultStoryKind();
		if (oldDefault != null) {
			storyKindDao.evict(oldDefault);
			if (!storyKind.getId().equals(oldDefault.getId()) && storyKind.getDefaultKind()) {
				oldDefault.setDefaultKind(false);
				storyKindDao.update(oldDefault);	
			}
		}
		if (storyKind.getId() != null && storyKind.getId()  > 0) {
			storyKindDao.update(storyKind);
			storyKindMessenger.notifyKindUpdated(storyKind);
		} else {
			storyKindDao.save(storyKind);
			storyKindMessenger.notifyKindInserted(storyKind);
		}
	}

	@Override
	public void delete(Long storyKindId) throws IllegalOperationException {
		StoryKind storyKind = storyKindDao.get(storyKindId);
		if (storyKind != null) {
			storyKindDao.delete(storyKind);
			storyKindMessenger.notifyKindDeleted(storyKind);
		} else {
			throw new IllegalOperationException(bundle.getMessage("institution.msg.canNotBeDeleted"));
		}
	}

	@Override
	public List<StoryKind> listAll() {
		return storyKindDao.findAll();
	}

	@Override
	public List<StoryKind> listAllOrderAcronym() {
		return storyKindDao.findAllOrderByAcronym();
	}
	
	@Override
	public Boolean verifyDeleteKind(Long id) {
        StoryKind kind = storyKindDao.get(id);
        return storyDao.countByStoryKind(kind) > 0;
	}

    @Override
    public StoryKind findOrCreateReplacement(StoryKind kind) throws CloneNotSupportedException {
        StoryKind replacement = storyKindDao.findByNameOrAcronym(kind.getName(), kind.getAcronym());
        if (replacement == null) {
            replacement = kind.clone();
            // Necessário para não dar Nullpointer na hora de salvar
            if(replacement.getId() == null)
                replacement.setId(0L);
            save(replacement);
        }
        return replacement;
    }

    //----------------------------------
	//	Setters
	//----------------------------------
    public void setStoryKindDao(StoryKindDao storyKindDao) {
    	this.storyKindDao = storyKindDao;
    }
    
    public void setStoryKindMessenger(StoryKindMessenger storyKindMessenger) {
    	this.storyKindMessenger = storyKindMessenger;
    }

    public void setStoryDao(StoryDao storyDao) {
    	this.storyDao = storyDao;
    }

}
