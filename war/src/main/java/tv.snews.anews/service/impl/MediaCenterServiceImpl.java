/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.service.impl;

import tv.snews.anews.dao.DeviceDao;
import tv.snews.anews.dao.MosMediaDao;
import tv.snews.anews.dao.MosMediaFullTextDao;
import tv.snews.anews.dao.WSMediaDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.infra.mos.MosConnectionFactory;
import tv.snews.anews.infra.ws.MediaPortalWebService;
import tv.snews.anews.search.MosMediaParams;
import tv.snews.anews.search.WSMediaParams;
import tv.snews.anews.service.MediaCenterService;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.mos.client.CommunicationException;
import tv.snews.mos.client.MosConnection;
import tv.snews.mos.client.UnexpectedMessageException;
import tv.snews.mos.domain.MosObject;
import tv.snews.mos.domain.MosObjectType;
import tv.snews.mos.domain.SearchParams;
import tv.snews.mos.domain.SearchResult;

import javax.xml.rpc.ServiceException;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import static tv.snews.anews.infra.mos.AnewsMosParser.asMosMedia;
import static tv.snews.anews.infra.mos.AnewsMosParser.asMosObjectMetadata;

/**
 * Implementação padrão do contrato do serviço do Media Center
 * {@link MediaCenterService}.
 *
 * @author Samuel Guedes de Melo
 * @author Felipe Pinheiro
 * @since 1.2.5
 */
public class MediaCenterServiceImpl implements MediaCenterService {

    /*
     * Pode ser informando na mensagem mosReqAll. Quando informado cada objeto
     * do dispositivo MOS será enviado em uma mensagem mosObj com esse intervalo
     * entre cada mensagem.
     */
    private static int INTERVAL = 1; // em segundos
    private static String OBJECT_DUPLICATED = "mediaCenter.msg.mediaSlugAlreadyUsed";
    private static String MESSAGE_NOT_SUPPORTED_ON_PORT = "mediaCenter.msg.msgNotSupportedOnPort";
    private static String DEVICE_OK = "DEVICE_OK";
    private static String DEVICE_IN_USE = "DEVICE_IN_USE";

    private MosConnectionFactory mosConnFactory;
    private ResourceBundle bundle = ResourceBundle.INSTANCE;

    private SessionManager sessionManager;
    private MosMediaFullTextDao mosMediaFullTextDao;
    private MosMediaDao mosMediaDao;
    private MediaPortalWebService mediaPortalWebService;
    private WSMediaDao wsMediaDao;
    private DeviceDao deviceDao;

    @Override
    public MosMedia mosObjCreate(MosMedia mosMedia) throws UnexpectedMessageException, IllegalOperationException {
        try {
            MosConnection conn = getConn((MosDevice) mosMedia.getDevice());
            String objID = conn.requestCreateObject(asMosObjectMetadata(mosMedia));
            MosMedia searchMosMediaOnServer = mosMediaDao.findByObjectId(objID, (MosDevice) mosMedia.getDevice());
            if (searchMosMediaOnServer == null) {
                mosMedia.setObjectId(objID);
                mosMediaDao.save(mosMedia);
                return mosMedia;
            } else {
                throw new IllegalOperationException(bundle.getMessage(OBJECT_DUPLICATED));
            }
        } catch (CommunicationException ex) {
            throw new UnexpectedMessageException(bundle.getMessage(MESSAGE_NOT_SUPPORTED_ON_PORT));
        }
    }

    @Override
    public Boolean canCreateMosObj(String objID, MosDevice mosDevice) {
        return mosMediaDao.findByObjectId(objID, mosDevice) == null;
    }

    @Override
    public WSMedia createWSVideo(WSMedia wsMedia) throws RemoteException, WebServiceException, ServiceException {
        int assetId = mediaPortalWebService.createVideo((WSDevice) wsMedia.getDevice(), wsMedia);
        wsMedia.setAssetId(assetId);
        wsMediaDao.save(wsMedia);
        return wsMedia;
    }

    @Override
    public WSMedia loadWSMedia(WSMedia wsMedia) throws RemoteException, ServiceException, WebServiceException {
        return mediaPortalWebService.getVideo((WSDevice) wsMedia.getDevice(), wsMedia.getAssetId());
    }

    @Override
    public String checkUseOfDevice(Integer deviceId) {
        Device device = deviceDao.get(deviceId);
        String result = DEVICE_OK;
        if (device instanceof MosDevice) {
            if (mosMediaDao.countByDevice((MosDevice) device) > 0) result = DEVICE_IN_USE;
        } else if (device instanceof WSDevice) {
            if (wsMediaDao.countByDevice((WSDevice) device) > 0) result = DEVICE_IN_USE;
        }
        return result;
    }

    @Override
    public List<MosMedia> loadById(List<Integer> ids) {
        List<MosMedia> mosMediaList = new ArrayList<>();
        for (Integer mosMediaId : ids) {
            mosMediaList.add(mosMediaDao.get(mosMediaId));
        }
        return mosMediaList;
    }

    @Override
    public List<MosMedia> mosReqAll(MosDevice mosDevice, Boolean interval) {
        MosConnection conn = getConn(mosDevice);

        List<MosMedia> medias = new ArrayList<>();

        if (interval) {
            conn.requestAllObjects(INTERVAL);
        } else {
            List<MosObject> mosObjs = conn.requestAllObjects();

            for (MosObject mosObj : mosObjs) {
                medias.add(asMosMedia(mosObj, mosDevice));
            }
        }

        return medias;
    }

    @Override
    public String mosReqSearchableSchema(MosDevice mosDevice) throws SessionManagerException {
        MosConnection conn = getConn(mosDevice);
        User user = sessionManager.getCurrentUser();
        return conn.requestSearchSchema(user.getNickname());
    }

    @Override
    public void mosReqObjList(MosDevice mosDevice, String schema) throws SessionManagerException {
        MosConnection conn = getConn(mosDevice);
        User user = sessionManager.getCurrentUser();

        SearchParams params = new SearchParams(schema, user.getNickname());

        // TODO Setar parâmetros de busca e paginação

        SearchResult result = conn.queryObjects(params);

        // TODO Retornar o resultado
    }

    @Override
    public PageResult<MosMedia> findByParams(MosMediaParams params) {
        List<MosMedia> results;
        int total;
        params.setMosObjectType(MosObjectType.VIDEO);

		/*
         * NOTE Essas buscas foram feitas sem levar em consideração o MAM
		 * selecionado na interface.
		 * Atualmente não deve acontecer de ter mais de um MAM, então não mudei
		 * esse comportamento. Mas se for necessário basta adicionar mais um
		 * parametro no MosMediaParams.
		 */

        // Só usa o Lucene se tiver texto nos parâmetros da busca
        if (params.hasText()) {
            total = mosMediaFullTextDao.countByParams(params);
            results = mosMediaFullTextDao.findByParams(params);
        } else {
            total = mosMediaDao.countByParams(params);
            results = mosMediaDao.findByParams(params);
        }

        return new PageResult<>(params.getPage(), total, results);
    }

    @Override
    public PageResult<WSMedia> findByWSParams(WSDevice wsDevice, WSMediaParams params) throws RemoteException, ServiceException, WebServiceException, SocketTimeoutException {
        return mediaPortalWebService.findAllByParams(wsDevice, params);
    }

    @Override
    public void deleteWSMedia(WSMedia wsMedia) {
        WSMedia media = wsMediaDao.get(wsMedia.getId());
        if (media != null) {
            wsMediaDao.delete(media);
        }
    }

    //------------------------------------
    //  Helper Methods
    //------------------------------------

    private MosConnection getConn(MosDevice mosDevice) {
        return mosConnFactory.getConnection(mosDevice);
    }


    //------------------------------------
    //  Getters & Setters
    //------------------------------------

    public void setMosConnFactory(MosConnectionFactory mosConnectionFactory) {
        this.mosConnFactory = mosConnectionFactory;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public void setMosMediaFullTextDao(MosMediaFullTextDao mosMediaFullTextDao) {
        this.mosMediaFullTextDao = mosMediaFullTextDao;
    }

    public void setMosMediaDao(MosMediaDao mosMediaDao) {
        this.mosMediaDao = mosMediaDao;
    }

    public void setMediaPortalWebService(MediaPortalWebService mediaPortalWebService) {
        this.mediaPortalWebService = mediaPortalWebService;
    }

    public void setWsMediaDao(WSMediaDao wsMediaDao) {
        this.wsMediaDao = wsMediaDao;
    }

    public void setDeviceDao(DeviceDao deviceDao) {
        this.deviceDao = deviceDao;
    }
}
