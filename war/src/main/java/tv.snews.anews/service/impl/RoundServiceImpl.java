
package tv.snews.anews.service.impl;

import tv.snews.anews.dao.RoundDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Round;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.RoundMessenger;
import tv.snews.anews.service.RoundService;
import tv.snews.anews.util.ResourceBundle;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public class RoundServiceImpl implements RoundService {

	private RoundDao roundDao;
	private SessionManager sessionManager;
	private RoundMessenger roundMessenger;
	
	private final EntityLocker<Round> lockerRound = EntityLockerFactory.getLocker(Round.class);
	private UserDao userDao;
    private ResourceBundle bundle = ResourceBundle.INSTANCE;

	
	@Override
	public List<Round> listAll(Date date) {
		return markLocks(roundDao.listAll(date));
	}

    @Override
    public Round get(Serializable idRound) {
    	Round round = roundDao.get(idRound);
    	if (round != null) {
    		round.setEditingUser(lockerRound.findOwner(round));
    	}
        return round;
    }	
	
	@Override
	public void save(Round round) throws SessionManagerException {
		if (round.getId() != null && round.getId()  > 0) {
			roundDao.update(round);
			roundMessenger.notifyUpdate(round);
		} else {
			round.setDate(new Date());
			round.setUser(sessionManager.getCurrentUser());
			roundDao.save(round);
			roundMessenger.notifyInsert(round);
		}
	}
	
	@Override
	public void remove(Long id) {
		Round round = roundDao.get(id);
		if (round != null) {
			roundDao.delete(round);
			roundMessenger.notifyDelete(round);
		}
	}
	
    @Override
	public void lockEdition(Long roundId, Integer userId) throws IllegalOperationException {
		Round round = roundDao.get(roundId);
		User user = userDao.get(userId);

		try {
			lockerRound.lock(round, user);
		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(bundle.getMessage("news.msg.newsLock"));
		}
	}
    
    @Override
	public void unlockEdition(Long roundId) throws IllegalOperationException {
		Round round = roundDao.get(roundId);
		lockerRound.unlock(round, sessionManager.getCurrentUser());
	}
    
    @Override
	public User isLockedForEdition(Long roundId) {
		Round round = roundDao.get(roundId);	
		return lockerRound.findOwner(round);
	}
    
    
	private List<Round> markLocks(List<Round> listRound) {
		for (Round round : listRound) {
			markLock(round);
		}
		return listRound;
	}
	 	
	private Round markLock(Round round) {
		round.setEditingUser(lockerRound.findOwner(round));
		return round;
	}
    

	//----------------------------------
	//	Setters
	//----------------------------------
	
    public void setUserDao(UserDao userDao) {
    	this.userDao = userDao;
    }
    
	public void setRoundDao(RoundDao roundDao) {
		this.roundDao = roundDao;
	}
	
    public void setSessionManager(SessionManager sessionManager) {
    	this.sessionManager = sessionManager;
    }

    public void setRoundMessenger(RoundMessenger value) {
    	this.roundMessenger = value;
    	
    	lockerRound.removeAllEntityLockerListeners();
		lockerRound.addEntityLockerListener(new EntityLockerListener<Round>() {

			@Override
			public void entityLocked(Round round, User user) {
				roundMessenger.notifyLock(round, user);
			}

			@Override
			public void entityUnlocked(Round round, User user) {
				roundMessenger.notifyUnlock(round, user);
			}
		});
    }
}
