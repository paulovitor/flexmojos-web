package tv.snews.anews.service.impl;

import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.LoginException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.service.LoginService;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.anews.util.UptimeCounter;

import java.util.Date;

/**
 * Implementa o contrato que prestar serviço de acesso ao sistema.
 * 
 * @author Eliezer Reis
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public final class LoginServiceImpl implements LoginService {

	private UserDao userDao;
	private SessionManager sessionManager;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	private UptimeCounter uptimeCounter;
	
	@Override
	public User getCurrentUser() throws SessionManagerException {
        User user = sessionManager.getCurrentUser();
        if (user != null) {
            user = userDao.get(user.getId());
            user.getAuthorities(); // carrega grupos e permissões
        }
		return user;
	}
	
	@Override
	public User login(User user, Long timezoneOffset) throws SessionManagerException, LoginException {
 		// Descomente para ativar o período de trial
//		if (uptimeCounter.uptime(TimeUnit.DAYS) > 30) {
//			throw new LoginException(bundle.getMessage("login.trialExpired"));
//		}

        user = userDao.get(user.getId());
        user.getAuthorities(); // carrega grupos e permissões

        String sessionId = sessionManager.registerUserSession(user, timezoneOffset);
        user.setSessionId(sessionId);

		return user;
	}

	@Override
	public User agreeWithTerms(int userId) {
        User user = userDao.get(userId);

        user.setAgreementDate(new Date());
        userDao.update(user);

        return user;
	}
	
	@Override
	public void leave() throws SessionManagerException {
		User currentUser = sessionManager.getCurrentUser();
		if (currentUser != null) {
			sessionManager.leave();
		}
	}

	// ------------------------
	// Setters
	// ------------------------
	
	public void setUserDao(UserDao value) {
		this.userDao = value;
	}
	
	public void setSessionManager(SessionManager value) {
		this.sessionManager = value;
	}
	
    public void setUptimeCounter(UptimeCounter uptimeCounter) {
	    this.uptimeCounter = uptimeCounter;
    }
}
