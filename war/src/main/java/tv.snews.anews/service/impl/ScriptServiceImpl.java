package tv.snews.anews.service.impl;

import net.sf.jasperreports.engine.util.ObjectUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.ScriptEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.ClipboardAction;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.ScriptMessenger;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.service.MediaCenterService;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.util.ImageUtil;
import tv.snews.anews.util.ResourceBundle;

import javax.xml.rpc.ServiceException;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.*;

import static org.apache.commons.collections.CollectionUtils.get;
import static tv.snews.anews.domain.ScriptAction.*;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptServiceImpl implements ScriptService, ApplicationEventPublisherAware, ResourceLoaderAware {

	private static final Logger log = LoggerFactory.getLogger(ScriptServiceImpl.class);
	private static final String DEFAULTS_REQUIRED_FIELDS = "defaults.requiredFields";
	private static final String IMAGE_TEMP_DIRECTORY = "/temp/scriptImage/";

	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private ResourceLoader resourceLoader;
	private SessionManager sessionManager;
	private ScriptBlockDao scriptBlockDao;
	private ScriptDao scriptDao;
    private MosMediaDao mosMediaDao;
	private GuidelineDao guidelineDao;
	private ReportageDao reportageDao;
	private ProgramDao programDao;
	private ScriptPlanDao scriptPlanDao;
	private UserDao userDao;
    private GuidelineService guidelineService;
    private ReportageService reportageService;
	private ScriptMessenger scriptMessenger;
	private ApplicationEventPublisher publisher;
	private EntityLocker<Script> scriptLocker;
	private MediaCenterService mediaCenterService;

	public ScriptServiceImpl() {
		scriptLocker = EntityLockerFactory.getLocker(Script.class);
	}

	@Override
	public Script loadById(Long id) {
		Script script = scriptDao.get(id, true);
		script.setEditingUser(scriptLocker.findOwner(script));
		loadScriptImages(script);
		return script;
	}

	@Override
	public synchronized Script create(Long blockId) throws ValidationException, IOException {
		ScriptBlock block = scriptBlockDao.get(blockId);
        Script script = block.createScript(sessionManager.getCurrentUser());
        ScriptEvent scriptEvent = new ScriptEvent(ScriptAction.INSERTED, this);
        // Ao criar um roteiro, é necessário que o programa seja associado na lista de "usado por"
        script.getPrograms().add(block.getScriptPlan().getProgram());
		save(script, scriptEvent);
		return script;
	}

	@Override
	public synchronized void removeScript(long scriptId) throws ValidationException, SessionManagerException {
		Script script = scriptDao.get(scriptId);
		long blockId = script.getBlock().getId();
		remove(script);

		//Reordena os scripts
		//ScriptBlock block = script.getBlock();
		sortScriptsFromIndex(blockId, script.getOrder());
	}

	@Override
	public synchronized void remove(Script script) throws ValidationException, SessionManagerException {
		validateFields(script);

		// Dispara evento para log & MOS
		// Dispara antes que o block seja setado para null
		dispatchEvent(new ScriptEvent(ScriptAction.EXCLUDED, script, this));

        removeScript(script);

		scriptMessenger.notifyDelete(script);
	}

    @Override
    public void removeScripts(Long[] idsOfScripts, Long scriptPlanId) {
        Long[] ids = idsOfScripts;
        List<Script> scripts = scriptDao.listById(ids);

        for (Script script : scripts) {
            dispatchEvent(new ScriptEvent(ScriptAction.EXCLUDED, script, this));

            removeScript(script);
        }

        scriptMessenger.notifyMultipleExclusion(ids, scriptPlanId);
    }

    private void removeScript(Script script) throws ValidationException, SessionManagerException {
        Guideline guideline = script.getGuideline();
        if (guideline != null) {
            removeProgramInGuideline(guideline, script.getProgram());
        }

        Reportage reportage = script.getReportage();
        if (reportage != null) {
            removeProgramInReportage(reportage, script.getProgram());
        }

        if (script.getSourceScript() != null) {
            script.getSourceScript().getPrograms().remove(script.getBlock().getScriptPlan().getProgram());
        }

        script.getPrograms().remove(script.getBlock().getScriptPlan().getProgram());

        script.setBlock(null);
        script.setExcluded(true);

        scriptDao.update(script, sessionManager.getCurrentUser());
    }

    @Override
	public List<ScriptVideo> loadVideosByScriptId(Long id) {
		List<ScriptVideo> scriptVideos = new ArrayList<>();
        Script script = scriptDao.get(id, true);
		scriptVideos.addAll(script.getVideos());
		return scriptVideos;
	}

	@Override
	public void createScriptVideo(Long id, ScriptVideo scriptVideo) throws ValidationException, IOException, IllegalOperationException, WebServiceException, ServiceException {
		if (scriptVideo.getMedia() instanceof MosMedia) {
			MosMedia newMosMedia = mediaCenterService.mosObjCreate(scriptVideo.getMosMedia());
			scriptVideo.setMedia(newMosMedia);
		}

		if (scriptVideo.getMedia() instanceof WSMedia) {
			WSMedia wsMedia = mediaCenterService.createWSVideo(scriptVideo.getWSMedia());
			scriptVideo.setMedia(wsMedia);
		}

		Script script = scriptDao.get(id);
		script.addVideo(scriptVideo);

        ScriptEvent scriptEvent = new ScriptEvent(ScriptAction.UPDATED, this);
		save(script, scriptEvent);
	}

  @Override
	public void updateField(Long scriptId, String field, Object object) throws ValidationException, SessionManagerException {
		Script script = scriptDao.get(scriptId);
		Script.Fields scriptField = Script.Fields.valueOf(field.toUpperCase());

		// Editor pode ser nulo
		if (scriptField != Script.Fields.EDITOR && scriptField != Script.Fields.IMAGE_EDITOR && scriptField != Script.Fields.REPORTER) {
			validateFields(script, field, object);
		}

		boolean pageChanged = false, slugChanged = false;
//        boolean vtChanged = false;

		ScriptEvent scriptEvent = null;
		/*
		 * ATENÇÃO!! Se for adicionar um novo campo neste switch, verifique se a
		 * mudança dele não precisa ser notificada via MOS.
		 */
		switch (scriptField) {
			case PAGE:
				if (!isValidPage((String) object)) {
					throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
				}
				String oldPage = script.getPage();
				String newPage = (String) object;
				pageChanged = !oldPage.equals(newPage);

				script.setPage(newPage);

				if (pageChanged) {
					scriptEvent = new ScriptEvent(ScriptAction.PAGE_CHANGED, this);
                    scriptEvent.registerChange(script, oldPage, newPage);
				}
				break;
			case SLUG:
				String oldSlug = script.getSlug();
				String newSlug = (String) object;
				slugChanged = !Objects.equals(oldSlug, newSlug);

				script.setSlug(newSlug);

				if (slugChanged) {
					scriptEvent = new ScriptEvent(ScriptAction.SLUG_CHANGED, this);
                    scriptEvent.registerChange(script, oldSlug, newSlug);
				}
				break;
			case REPORTER:
				scriptEvent = new ScriptEvent(ScriptAction.REPORTER_CHANGED, script, this);
				script.setReporter((User) object);
				break;
			case EDITOR:
				scriptEvent = new ScriptEvent(ScriptAction.EDITOR_CHANGED, script, this);
				script.setEditor((User) object);
				break;
			case IMAGE_EDITOR:
				scriptEvent = new ScriptEvent(ScriptAction.IMAGE_EDITOR_CHANGED, script, this);
				script.setImageEditor((User) object);
				break;
			case OK:
				script.setOk((Boolean) object);
				scriptEvent = new ScriptEvent(script.isOk() ? ScriptAction.OK : ScriptAction.NOT_OK, script, this);
				break;
			case VT:
				scriptEvent = new ScriptEvent(ScriptAction.VT_CHANGED, script, this);
				script.setVt((Date) object);
				break;
			case TAPE:
				scriptEvent = new ScriptEvent(ScriptAction.TAPE_CHANGED, script, this);
				script.setTape((String) object);
				break;
			case APPROVED:
				script.setApproved((Boolean) object);
				scriptEvent = new ScriptEvent(script.isApproved() ? ScriptAction.APPROVED : ScriptAction.DISAPPROVED, script, this);
				break;
            case DISPLAYING:
                for(ScriptBlock block : script.getBlock().getScriptPlan().getBlocks()){
                    for(Script scriptOfScriptPlan : block.getScripts()){
                        if(scriptOfScriptPlan.getId() != script.getId() && scriptOfScriptPlan.isDisplaying()){
                            scriptOfScriptPlan.setDisplayed(true);
                            scriptOfScriptPlan.setDisplaying(false);
                            scriptDao.save(scriptOfScriptPlan);
                            scriptMessenger.notifyUpdateField(scriptOfScriptPlan.getId(), "displayed", true);
                        }
                    }
                }
                script.setDisplaying((Boolean) object);
                //storyEvent = new StoryEvent(DISPLAYING, story, this);
                break;
			default:
				break;
		}

		scriptDao.save(script, sessionManager.getCurrentUser());
		scriptMessenger.notifyUpdateField(scriptId, field, object);

		if (scriptEvent != null) {
			dispatchEvent(scriptEvent);
		}
	}

    private boolean isValidPage(String value) {
    	if (StringUtils.isEmpty(value)) {
    		return false;
    	}

    	if (value.length() > 3) {
    		return false;
    	}

    	boolean hasNumbers = false;
    	for (int i = 0; i < value.length(); i++) {
    		if ("0123456789".indexOf(value.charAt(i)) != -1) {
    			hasNumbers = true;
    		}
    	}
    	if (!hasNumbers) {
    		return false;
    	}

    	if ("0123456789".indexOf(value.charAt(0)) != -1 && "0123456789".indexOf(value.charAt(1)) != -1) {
    		return true;
    	}
    	
    	if (value.length() == 3) {
    		if ("ABCDEFGHIJKLMNOPQRSTUVXWYZ".indexOf(value.charAt(2)) != -1) {
    			return true;
    		}
    	}

    	return false;
    }

	@Override
	public synchronized void sendToDrawer(Long scriptId) {
		Script script = scriptDao.get(scriptId);
		validateFields(script);

		if (script.getId() != null && script.getId() > 0) {

			// Retira o registro do programa no script original da gaveta
			Script drawerOrigin = script.findDrawerOrigin();
			if (drawerOrigin != null) {
                removeProgramInDrawerOrigin(script, drawerOrigin);
			}

			// Retira o registro do programa na pauta de origem
			Guideline guideline = script.getGuideline();
			if (guideline != null) {
                removeProgramInGuideline(guideline, script.getProgram());
			}

			// Retira o registro do programa na reportagem de origem
			Reportage reportage = script.getReportage();
			if (reportage != null) {
                removeProgramInReportage(reportage, script.getProgram());
			}

            script.getPrograms().remove(script.getBlock().getScriptPlan().getProgram());

			// Dispare o evento antes que o bloco seja alterado para null
			ScriptEvent storyEvent = new ScriptEvent(ScriptAction.ARCHIVED, script, this);
			dispatchEvent(storyEvent);

			script.setBlock(null);

			scriptDao.update(script);
			scriptMessenger.notifyDrawerChange(script);
		}
	}

	@Override
	public void save(Script script, ScriptEvent scriptEvent) throws ValidationException, IOException {
		validateFields(script);
		script.setBlank(script.hasContent());
		script.setHasVideos(script.verifyHasVideos());

		if (script.getId() != null && script.getId() > 0) {
			scriptDao.update(script, sessionManager.getCurrentUser());
			scriptMessenger.notifyUpdate(script);

			dispatchEvent(scriptEvent);
		} else {
			script.setId((Long) scriptDao.save(script, sessionManager.getCurrentUser()));

			scriptMessenger.notifyInsert(script);

			scriptEvent.registerChange(script);
			dispatchEvent(scriptEvent);
		}
	}

	@Override
	public void copyTo(Long[] scriptIds, Date date, int destinyProgramId, boolean move) throws IOException, CloneNotSupportedException, IllegalStateException {
		Program destinyProgram = programDao.get(destinyProgramId);
		ScriptPlan destinyScriptPlan = scriptPlanDao.findByDateAndProgram(date, destinyProgram);
        if (destinyScriptPlan == null) {
            throw new IllegalStateException();
        } else {
            ScriptBlock standBy = destinyScriptPlan.standBy();
            if (move) {
                moveScripts(scriptIds, standBy.getId(), standBy.getScripts().size());
            } else {
                copy(scriptIds, standBy, standBy.getScripts().size());
            }
        }
	}

	@Override
	public synchronized void copyDrawerScriptToBlock(Long scriptId, Long blockId, int index) throws CloneNotSupportedException, IOException {
		Script drawerScript = scriptDao.get(scriptId);
		ScriptBlock targetBlock = scriptBlockDao.get(blockId);

		if (index < 0 || index > targetBlock.getScripts().size()) {
			index = targetBlock.getScripts().size();
		}

		Script copy = createCopyOfScript(drawerScript, targetBlock);
		changePresenters(copy, targetBlock.getScriptPlan().getProgram());
		targetBlock.addScript(copy, index);
        copy.getPrograms().add(targetBlock.getScriptPlan().getProgram());

		addProgramInDrawerOrigin(targetBlock.getScriptPlan().getProgram(), drawerScript);

		scriptDao.save(copy);
		scriptMessenger.notifyInsert(copy);

	}

	@Override
	public synchronized void createScriptFromReportage(long blockId, int index, long reportageId) throws ValidationException, SessionManagerException, IOException {
		ScriptBlock block = scriptBlockDao.get(blockId);
		Reportage reportage = reportageDao.get(reportageId);

		Script script = block.createScript(reportage, sessionManager.getCurrentUser(), index);
		changePresenters(script, block.getScriptPlan().getProgram());

		Program origin = reportage.getProgram();
		Program destiny = script.getBlock().getScriptPlan().getProgram();

        if (origin != null && !Objects.equals(origin.getCgDevice(), destiny.getCgDevice())) {
			disableCredits(script);
		}

		ScriptEvent scriptEvent = new ScriptEvent(ScriptAction.INSERTED, script, this);
		save(script, scriptEvent);

        addProgramInReportage(reportage, script.getProgram());
	}

	@Override
	public synchronized void createScriptFromGuideline(long blockId, int index, long guidelineId) throws ValidationException, SessionManagerException, IOException {
		ScriptBlock block = scriptBlockDao.get(blockId);
		Guideline guideline = guidelineDao.get(guidelineId);

		Script script = block.createScript(guideline, sessionManager.getCurrentUser(), index);
		changePresenters(script, block.getScriptPlan().getProgram());

        ScriptEvent scriptEvent = new ScriptEvent(ScriptAction.INSERTED, this);
		save(script, scriptEvent);

        removeProgramInGuideline(guideline, script.getProgram());
	}

	@Override
	public synchronized void updatePositions(Long[] scriptsIds, Long blockId, int dropIndex) {
		ScriptBlock targetBlock = scriptBlockDao.get(blockId);

		List<Script> scripts = new ArrayList<>();
		List<Script> localScripts = new ArrayList<>();
		List<Script> otherScripts = new ArrayList<>();

        // Dispara o evento de mudança de bloco
        ScriptEvent scriptEventBlockChange = new ScriptEvent(BLOCK_CHANGED, this);


		for (Long scriptId : scriptsIds) {
			Script script = scriptDao.get(scriptId);
			scripts.add(script);

			if (script.getBlock().equals(targetBlock)) {
				localScripts.add(script);
			} else {
				otherScripts.add(script);
                scriptEventBlockChange.registerChange(script, script.getBlock().getOrder(), targetBlock.getOrder());
			}
		}

		for (Script script : localScripts) {
			if (script.getOrder() < dropIndex) {
				dropIndex--;
			}

			targetBlock.removeScript(script);
		}

        ScriptEvent scriptEventOrderChange = new ScriptEvent(ORDER_CHANGED, this);

        for (Script script : otherScripts) {
            script.getBlock().removeScript(script);
        }

        for (int i = 0; i < scripts.size(); i++) {
            Script script = scripts.get(i);
            int oldOrder = script.getOrder();

            targetBlock.addScript(script, dropIndex + i, true);

            // Ajusta o editingUser
            script.setEditingUser(scriptLocker.findOwner(script));

            scriptEventOrderChange.registerChange(script, oldOrder, script.getOrder());
		}

        scriptMessenger.notifyMove(scripts, sessionManager.getCurrentUser());
        dispatchEvent(scriptEventOrderChange);
        dispatchEvent(scriptEventBlockChange);
    }

	@Override
	public synchronized void updatePosition(Long scriptId, Long blockId, int newOrder, boolean dragNDrop) {
		Script movedScript = scriptDao.get(scriptId);
		ScriptBlock newBlock = scriptBlockDao.get(blockId);

		ScriptBlock oldBlock = movedScript.getBlock();
		int oldOrder = movedScript.getOrder();

		if (oldBlock.equals(newBlock)) {

			/* Não alterou o bloco */

			if (oldOrder < newOrder) {
				/*
				 * Fez um drag n' drop para baixo dentro do mesmo bloco. Neste caso os valores das ordens maiores que 'oldOrder'
				 * e menores ou iguais a 'newOrder' devem ser decrementados.
				 */
				for (int i = oldOrder + 1; i <= newOrder; i++) {
					Script current = (Script) get(oldBlock.getScripts(), i);

					current.setOrder(current.getOrder() - 1);
					scriptDao.update(current);
				}
			}

			if (oldOrder > newOrder) {
				/*
				 * Fez um drag n' drop para cima dentro do mesmo bloco. Neste caso os valores das ordens maiores ou iguais
				 * a 'newOrder' e menores que 'oldOrder' devem ser incrementados.
				 */
				for (int i = oldOrder - 1; i >= newOrder; i--) {
					Script current = (Script) get(newBlock.getScripts(), i);

					current.setOrder(current.getOrder() + 1);
					scriptDao.update(current);
				}
			}

			movedScript.setOrder(newOrder);
		} else {

			/* Alterou o bloco */

			oldBlock.removeScript(movedScript);

			/*
			 * No bloco antigo basta corrigir o espaço vago do script que saiu de lá.
			 */
			for (int i = oldOrder; i < oldBlock.getScripts().size(); i++) {
				Script current = (Script) get(oldBlock.getScripts(), i);

				current.setOrder(i);
				scriptDao.update(current);
			}

			/*
			 * No bloco novo tem que incrementar as ordens dos scripts que ficaram após a nova posição do script movido.
			 */
			for (int i = newBlock.getScripts().size() - 1; i >= newOrder; i--) {
				Script current = (Script) get(newBlock.getScripts(), i);

				current.setOrder(current.getOrder() + 1);
				scriptDao.update(current);
			}

			movedScript.setBlock(newBlock);
			movedScript.setOrder(newOrder);

			// Dispara o evento de mudança de bloco
            ScriptEvent scriptEvent = new ScriptEvent(BLOCK_CHANGED, this);
            scriptEvent.registerChange(movedScript, oldBlock.getOrder(), newBlock.getOrder());
            dispatchEvent(scriptEvent);
		}

		scriptDao.update(movedScript);
		movedScript.setEditingUser(scriptLocker.findOwner(movedScript));
		scriptMessenger.notifyMove(movedScript, sessionManager.getCurrentUser(), dragNDrop);

        ScriptEvent scriptEvent = new ScriptEvent(ORDER_CHANGED, this);
        scriptEvent.registerChange(movedScript, oldOrder, movedScript.getOrder());
        dispatchEvent(scriptEvent);
	}

	@Override
	public Script merge(Script script) throws SessionManagerException, IOException {
		Objects.requireNonNull(script, bundle.getMessage("script.msg.invalidId"));
		Objects.requireNonNull(script.getId(), bundle.getMessage("script.msg.invalidId"));

		script.setHasVideos(script.verifyHasVideos());

		Script current = scriptDao.get(script.getId(), true);
		scriptDao.evict(current);

		mergeImages(current.getImages(), script.getImages());

		current.updateMainFields(script);
		current.setBlank(script.hasContent());

		verifyExistMedia(current.getVideos());

		current = scriptDao.merge(current, sessionManager.getCurrentUser());
		loadScriptImages(current);
		scriptMessenger.notifyUpdate(current);

		dispatchEvent(new ScriptEvent(ScriptAction.UPDATED, current, this));

		return current;
	}

    @Override
    public Script updateVideos(Script script) throws SessionManagerException, IOException {
        Objects.requireNonNull(script, bundle.getMessage("script.msg.invalidId"));
        Objects.requireNonNull(script.getId(), bundle.getMessage("script.msg.invalidId"));

        Script current = scriptDao.get(script.getId(), true);
        scriptDao.evict(current);
        current.setVideos(script.getVideos());
        current.updateVTTime();
        current.setBlank(script.hasContent());
        current.setHasVideos(script.verifyHasVideos());

        verifyExistMedia(script.getVideos());

        current = scriptDao.merge(current);
        scriptMessenger.notifyUpdate(current);

        dispatchEvent(new ScriptEvent(ScriptAction.UPDATED, current, this));

        return current;
    }

	private void verifyExistMedia(SortedSet<ScriptVideo> videos) {
		for (ScriptVideo video : videos) {
			if (video.getMedia() != null) {
				if (video.getMedia() instanceof MosMedia)
					video.setMedia(mosMediaDao.get(video.getMedia().getId()));
				else if (video.getMedia() instanceof WSMedia) {
					try {
						video.setMedia(mediaCenterService.loadWSMedia((WSMedia) video.getMedia()));
					} catch (Exception e) {
						log.debug(e.getMessage());
						mediaCenterService.deleteWSMedia((WSMedia) video.getMedia());
						video.setMedia(null);
					}
				}
			}
		}
	}

	@Override
	public synchronized String restoreExcludedScript(Long scriptId, Date date, int programId) throws SessionManagerException {
		Script script = scriptDao.get(scriptId);
		Program destinyProgram = programDao.get(programId);
		ScriptPlan destinyPlan = scriptPlanDao.findByDateAndProgram(date, destinyProgram);

		if (destinyPlan == null) {
			return SCRIPT_PLAN_NOT_FOUND;
		} else {
			script.setApproved(false);
			script.setOk(false);
			script.setExcluded(false);
			changePresenters(script, destinyProgram);
			changeEditor(script, destinyProgram);

			if (!Objects.equals(script.getProgram().getCgDevice(), destinyProgram.getCgDevice())) {
				disableCredits(script);
			}
			verifyExistMedia(script.getVideos());

			// Ajusta o bloco e a ordem
			ScriptBlock standBy = destinyPlan.standBy(); // não deve retornar null, se retornar tem algo errado!
			script.setBlock(standBy);
			//Esse método seta a ordem do script
			standBy.addScript(script);
			script.getPrograms().add(standBy.getScriptPlan().getProgram());

			// Registra o uso do programa na lauda original da gaveta
			Script drawerOrigin = script.findDrawerOrigin();
			if (drawerOrigin != null) {
				addProgramInDrawerOrigin(script.getProgram(), drawerOrigin);
			}

			// Registra o programa na pauta de origem
			Guideline guideline = script.getGuideline();
			if (guideline != null) {
				addProgramInGuideline(guideline, script.getProgram());
			}

			// Registra o programa na reportagem de origem
			Reportage reportage = script.getReportage();
			if (reportage != null) {
				addProgramInReportage(reportage, script.getProgram());
			}

			scriptDao.update(script, sessionManager.getCurrentUser());
			scriptMessenger.notifyRestore(script);

			return OK;
		}
	}

	private Script createCopyOfScript(Script scriptToCopy, ScriptBlock block) throws IOException {
		Script copy = new Script(scriptToCopy);
		copy.setApproved(false);
		copy.setOk(false);

		Program origin = scriptToCopy.getProgram();
		Program destiny = block.getScriptPlan().getProgram();
        copy.getPrograms().clear();
        copy.getPrograms().add(destiny);

		changePresenters(copy, destiny);
        changeEditor(copy, destiny);

		if (!Objects.equals(origin.getCgDevice(), destiny.getCgDevice())) {
			disableCredits(copy);
		}
        verifyExistMedia(copy.getVideos());
		return copy;
	}

	private void removeProgramInDrawerOrigin(Script script, Script drawerOrigin) {
		drawerOrigin.getPrograms().remove(script.getProgram());
		scriptDao.update(drawerOrigin);
		scriptMessenger.notifyUpdate(drawerOrigin);
	}

	private void addProgramInDrawerOrigin(Program program, Script drawerOrigin) {
		drawerOrigin.getPrograms().add(program);
		scriptDao.update(drawerOrigin);
		scriptMessenger.notifyUpdate(drawerOrigin);
	}

	private void removeProgramInGuideline(Guideline guideline, Program program) {
        guidelineService.unregisterProgram(guideline, program);
	}

	private void addProgramInGuideline(Guideline guideline, Program program) {
        guidelineService.registerProgram(guideline, program);
	}

	private void removeProgramInReportage(Reportage reportage, Program program) {
        reportageService.unregisterProgram(reportage, program);
	}

	private void addProgramInReportage(Reportage reportage, Program program) {
        reportageService.registerProgram(reportage, program);
	}

	private void disableCredits(Script script) {
		for (AbstractScriptItem item : script.getCgs()) {
			item.setDisabled(true);
		}
	}

    @Override
	public int countExcludedScripts() {
		return scriptDao.countExcludedScripts();
	}

	@Override
	public PageResult<Script> listExcludedScripts(int pageNumber) {
		List<Script> scripts = scriptDao.listExcludedScripts(pageNumber);
		return new PageResult<>(pageNumber, scriptDao.totalExcluded(), scripts);
	}

	@Override
	public Collection<ScriptRevision> loadChangesHistory(Long scriptId) {
		return scriptDao.get(scriptId).changesHistory();
	}

	@Override
	public void lockEdition(Long scriptId, Integer userId) throws IllegalOperationException {
		Script script = scriptDao.get(scriptId);
		User user = userDao.get(userId);

		try {
			scriptLocker.lock(script, user);
		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(bundle.getMessage("script.msg.scriptLock"));
		}

		scriptMessenger.notifyLock(script, user);
	}

	@Override
	public void unlockEdition(Long scriptId) throws IllegalOperationException {
		Script script = scriptDao.get(scriptId);
		scriptLocker.unlock(script, sessionManager.getCurrentUser());
		scriptMessenger.notifyUnlock(script);
	}

	@Override
	public User isLockedForEdition(Long scriptId) {
		Script script = scriptDao.get(scriptId);
		return scriptLocker.findOwner(script);
	}

    @Override
    public void assumeEdition(Long scriptId, Integer userId) throws IllegalOperationException {
        Script script = scriptDao.get(scriptId);
        User user = userDao.get(userId);

        try {
            scriptLocker.unlock(script, sessionManager.getCurrentUser());
            scriptLocker.lock(script, user);
        } catch (IllegalOperationException e) {
            throw new IllegalOperationException(bundle.getMessage("script.msg.scriptLock"));
        }

        dispatchEvent(new ScriptEvent(ScriptAction.EDITING_USER_CHANGED, this));

        scriptMessenger.notifyLock(script, user);
    }

    @Override
    public void clearClipboard() {
        sessionManager.clipboardFlush();
    }

    @Override
    public Long[] sendScriptsToClipboard(ClipboardAction action, Long[] ids) {
        sessionManager.clipboardPush(action, ids);
        return sessionManager.clipboardPull();
    }

    @Override
    public Long[] pastScriptsFromClipboard(Long blockId, int dropIndex) throws CloneNotSupportedException, IOException {
        Long[] ids = sessionManager.clipboardPull();

        if (ids.length > 0) {
            ClipboardAction action = sessionManager.clipboardAction();
            ScriptBlock block = scriptBlockDao.get(blockId);

            if ((dropIndex == -1 && block != null) || (dropIndex > block.getScripts().size())) {
                dropIndex = block.getScripts().size();
            }

            if (action == ClipboardAction.COPY) {
                copy(ids, block, dropIndex);
            } else {
                moveScripts(ids, blockId, dropIndex);
            }

            sessionManager.clipboardFlush();
        }

        return ids;
    }

	//--------------------------------------------------------------------------
	//	Helpers Methods
	//--------------------------------------------------------------------------

    private void copy(Long[] scriptsIds, ScriptBlock targetBlock, int dropIndex) throws CloneNotSupportedException, IOException {
        // Carrega os scripts
        List<Script> scripts = scriptDao.listById(scriptsIds);

        // Ordena as stories para que não decremente o dropIndex incorretamente
        Collections.sort(scripts);

        for (Script script : scripts) {
            if (script.isExcluded()) {
                continue;
            }
            script.setEditingUser(scriptLocker.findOwner(script));
        }

        ScriptEvent copyEvent = new ScriptEvent(COPIED, this);

        if (!targetBlock.getScriptPlan().isDisplayed()) {
            // Carrega os scripts
            for (Script script : scripts) {
                Program sourceProgram = script.getProgram();
                Program targetProgram = targetBlock.getScriptPlan().getProgram();

                Script copy = new Script(script);
                copy.setSourceScript(script); // link para o script original
                copy.setApproved(false);
                copy.setOk(false);

                // Define o bloco e ordem corretos
                copy.setBlock(targetBlock);
                copy.setOrder(dropIndex);
                copy.setPage(nextPage(targetBlock.getScriptPlan()));
                copy.getPrograms().clear();
                copy.getPrograms().add(targetProgram);

                //Put it on the correct index.
                targetBlock.addScript(copy, dropIndex);
                dropIndex++;

                if (sourceProgram != null) {
                    if (!sourceProgram.equals(targetProgram)) {
                        changePresenters(copy, targetProgram);
                    }

                    // Se não for o mesmo dispositívo de GC, desativa os créditos
                    if (!ObjectUtils.equals(sourceProgram.getCgDevice(), targetProgram.getCgDevice())) {
                        disableCredits(copy);
                    }
                }

                // Caso tenha uma origem da gaveta, registra o uso no programa de destino
                Script drawerOrigin = script.findDrawerOrigin();
                if (drawerOrigin != null) {
                    drawerOrigin.getPrograms().add(targetProgram);
                    scriptDao.update(drawerOrigin);
                    scriptMessenger.notifyUpdate(drawerOrigin, getScriptPlanIdFromScript(drawerOrigin));
                }

                registerProgram(copy, targetProgram);

                if (copy.verifyHasVideos()) {
                    verifyExistMosMedia(copy.scriptVideosOfType(MosMedia.class));
                    verifyExistWSMedia(copy.scriptVideosOfType(WSMedia.class));
                }

                copy.setId((Long) scriptDao.save(copy, sessionManager.getCurrentUser()));
                scriptMessenger.notifyInsert(copy, getScriptPlanIdFromScript(copy));

                //TODO criar tarefa para logs do roteiro.
                copyEvent.registerChange(copy, sourceProgram.getName(), targetProgram.getName());
            }

            scriptDao.flush();

            // Dispara os eventos somente após o flush()
            dispatchEvent(copyEvent);
        }
    }

    @Override
    public void moveScripts(Long[] scriptsIds, Long blockId, int dropIndex) {
        // Carrega os Scripts
        List<Script> scripts = scriptDao.listById(scriptsIds);
        ScriptBlock targetBlock = scriptBlockDao.get(blockId), sourceBlock;

        // Ordena as stories para que não decremente o dropIndex incorretamente
        Collections.sort(scripts);

        for (Script script : scripts) {
            if (script.isExcluded()) {
                continue;
            }
            script.setEditingUser(scriptLocker.findOwner(script));
            sourceBlock = script.getBlock();
            //Put it on the correct index.
            sourceBlock.removeScript(script);
        }


        Set<ScriptBlock> reorder = new HashSet<>();
        reorder.add(targetBlock);

        ScriptEvent blockEvent = new ScriptEvent(BLOCK_CHANGED, this);
        ScriptEvent disapproveEvent = new ScriptEvent(DISAPPROVED, this);
        ScriptEvent orderEvent = new ScriptEvent(ORDER_CHANGED, this);

        for (Script script : scripts) {
            if (!script.isDisplayed() && !script.isDisplaying() && !script.getBlock().getScriptPlan().isDisplayed()) {
                sourceBlock = script.getBlock();
                reorder.add(sourceBlock);

                ScriptEvent scriptEvent = null;
                //ATTENTION, this code is just called if has scripts from different scriptPlan
                //If moving from another scriptPlan, notify that this story was deleted
                if (!(targetBlock.getScriptPlan().getId() == sourceBlock.getScriptPlan().getId())) {
                    //TODO We should insert this information on log of scriptPlan
                    scriptMessenger.notifyDelete(script, script.getBlock().getScriptPlan().getId()); //Notify old scriptPlan story was deleted.
                    script.setPage(nextPage(targetBlock.getScriptPlan()));

                    if (script.isApproved()) {
                        //TODO We should insert this information on log of scriptPlan.
                        script.setApproved(false); //Instead of notify insert, just let user approve again when come from another rundown.
						//TODO Verificar o log do sistema
                        disapproveEvent.registerChange(script);
                    }
                }

                Program sourceProgram = script.getProgram();
                Program targetProgram = targetBlock.getScriptPlan().getProgram();

                script.getPrograms().clear();
                script.getPrograms().add(targetProgram);

                if (sourceProgram != null) {
                    if (!sourceProgram.equals(targetProgram)) {
                        changePresenters(script, targetProgram);
                    }

                    // Se não for o mesmo dispositívo de GC, desativa os créditos
                    if (!ObjectUtils.equals(sourceProgram.getCgDevice(), targetProgram.getCgDevice())) {
                        disableCredits(script);
                    }
                }

                int oldIndex = script.getOrder();

                boolean blockChanged = false;
                if (sourceBlock.getId() == targetBlock.getId()) {
                    if (script.getOrder() < dropIndex) {
                        if(dropIndex > targetBlock.getScripts().size()) {
                            dropIndex = targetBlock.getScripts().size();
                        }
                    }
                } else {
                    blockChanged = true;
                }

                //Valida os Campos
                validateFields(script);
                validateFields(targetBlock);

                //Change the order of the script
                script.setBlock(targetBlock);
                script.setOrder(dropIndex);
                scriptDao.save(script);

                targetBlock.addScript(script, dropIndex, true);

                dropIndex++;

                if (blockChanged) {
                    // Registra no evento a mudança de bloco deste script
                    blockEvent.registerChange(script, sourceBlock.getOrder(), targetBlock.getOrder());
                }

                //Os indices sempre precisam estar ordenados antes de enviar via MOS
                sortBlock(targetBlock);
                if (!(targetBlock.getId() == sourceBlock.getId())) {
                    sortBlock(sourceBlock);
                }
                // Registra no evento a mudança de ordem do bloco deste script
                orderEvent.registerChange(script, oldIndex, script.getOrder());
            }
        }

        scriptDao.flush();

        // Dispara os eventos após o flush() ocorrer com sucesso
        dispatchEvent(disapproveEvent);
        dispatchEvent(blockEvent);
        dispatchEvent(orderEvent);

        // Notificar move de todos os scripts
        scriptMessenger.notifyMove(scripts, getScriptPlanIdFromScripts(scripts));
    }

    private void registerProgram(Script script, Program program) {
        if (program != null) {
            if (script.getGuideline() != null) {
                addProgramInGuideline(script.getGuideline(), program);
            }
            if (script.getReportage() != null) {
                addProgramInReportage(script.getReportage(), program);
            }
        }
    }

    @Override
    public void loadAndUpdateScriptVideo(long scriptId, ScriptVideo scriptVideo) throws RemoteException, ServiceException, WebServiceException {
        WSMedia synchronizedWSMedia = mediaCenterService.loadWSMedia(scriptVideo.getWSMedia());

        Script currentScript = scriptDao.get(scriptId);
        scriptDao.evict(currentScript);

        updateScriptWSVideo(currentScript, synchronizedWSMedia, scriptVideo);

        scriptDao.update(currentScript);
        scriptDao.flush();

        scriptMessenger.notifyUpdate(currentScript, currentScript.getBlock().getScriptPlan().getId());
    }

    private void updateScriptWSVideo(Script currentScript, WSMedia synchronizedWSMedia, ScriptVideo scriptVideo) {
        for (ScriptVideo scriptVideoCurrent : currentScript.scriptVideosOfType(WSMedia.class)) {
            if (scriptVideoCurrent.equals(scriptVideo)) {
                if (synchronizedWSMedia == null) {
                    mediaCenterService.deleteWSMedia(scriptVideoCurrent.getWSMedia());
                    scriptVideoCurrent.setMedia(null);
                } else {
                    synchronizedWSMedia.setId(scriptVideo.getMedia().getId());
                    if (synchronizedWSMedia.isDifferent(scriptVideo.getWSMedia()))
                        scriptVideoCurrent.setMedia(synchronizedWSMedia);
                }
                break;
            }
        }
    }

    private void verifyExistMosMedia(List<ScriptVideo> videos) {
        for (ScriptVideo video : videos) {
            if (video.getMedia() != null) {
                video.setMedia(mosMediaDao.get(video.getMosMedia().getId()));
            }
        }
    }

    private void verifyExistWSMedia(List<ScriptVideo> videos) {
        for (ScriptVideo video : videos) {
            if (video.getMedia() != null) {
                try {
                    video.setMedia(mediaCenterService.loadWSMedia(video.getWSMedia()));
                } catch (Exception e) {
                    log.debug(e.getMessage());
                    mediaCenterService.deleteWSMedia(video.getWSMedia());
                    video.setMedia(null);
                }
            }
        }
    }

    private Long getScriptPlanIdFromScript(Script script) {
        if (script != null) {
            if (script.getBlock() != null && script.getBlock().getScriptPlan() != null) {
                return script.getBlock().getScriptPlan().getId();
            }
        }
        return  -1L;
    }

    private Long getScriptPlanIdFromScripts(Collection<Script> scripts) {
        for (Script script : scripts) {
            if (script.getBlock() != null && script.getBlock().getScriptPlan() != null) {
                return script.getBlock().getScriptPlan().getId();
            }
        }
        return -1L;
    }

    private String nextPage(ScriptPlan scriptPlan) {
        String maxPage = scriptDao.getMaxPage(scriptPlan);
        if (maxPage == null) {
            return "01";
        }

        int aux = Integer.parseInt(maxPage.replaceAll("[^\\d]", ""));
        return ++aux > 9 ? Integer.toString(aux) : "0" + aux;
    }

	/*
	 * Valida campos obrigatórios dos scripts.
	 */
	private void validateFields(Script script) throws ValidationException {
		if (script == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	private void dispatchEvent(ScriptEvent event) {
		publisher.publishEvent(event);
	}

	private void sortScriptsFromIndex(long blockId, long orderRemovedScript) {
		ScriptBlock block = scriptBlockDao.get(blockId);
		List<Script> scripts = scriptDao.listByBlock(block);

		for (Script script : scripts) {
			if (script.getOrder() > orderRemovedScript) {
				script.setOrder(script.getOrder() - 1);
				scriptDao.save(script);
			}
		}
	}

	/*
	 * Valida campos obrigatórios dos campos do script.
	 */
	private void validateFields(Script script, String field, Object object) throws ValidationException {
		if ((script == null) || (field == "") || (object == null)) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	private void sortBlock(ScriptBlock block) {
		int index = -1;
		for (Script script : block.getScripts()) {
			if (script.getOrder() != ++index) {
				script.setOrder(index);
				scriptDao.save(script);
			}
		}
	}

	/*
	 * Valida campos obrigatórios dos blocks.
	 */
	private void validateFields(ScriptBlock block) throws ValidationException {
		if (block == null || block.getScriptPlan() == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	private String getNextPage(ScriptPlan scriptPlan) {
		String maxPage = scriptDao.getMaxPage(scriptPlan);
		Integer aux = maxPage == null ? 0 : Integer.parseInt(maxPage.replaceAll("[^\\d]", ""));
		return ++aux < 10 ? "0" + aux : aux.toString();
	}

	private void changeEditor(Script script, Program destinyProgram) {
		// Atualiza o editor se necessário
		if (!destinyProgram.getEditors().contains(script.getEditor())) {
			script.setEditor(destinyProgram.findFirstEditor());
		}
	}

	private void changePresenters(Script script, Program destinyProgram) {
		if (!destinyProgram.getPresenters().contains(script.getPresenter())) {
			script.setPresenter(destinyProgram.getDefaultPresenter());
		}
	}

    @Override
    public byte[] loadImg(String imgPath) {
        return ImageUtil.loadImg(imgPath);
    }

    @Override
    public void removeImg(String imgPath) throws IOException {
        if (imgPath != null && !imgPath.trim().equals("")) {
            Resource resource = resourceLoader.getResource("file:" + imgPath);
            resource.getFile().delete();
        }
    }

    private void mergeImages(SortedSet<ScriptImage> currentImages, SortedSet<ScriptImage> newImages) throws IOException {
        //Remove imagens do diretório
        if(currentImages.isEmpty()) {
            for (ScriptImage newImage : newImages) {
                //Move imagens para o diretório definitivo
                moveDefinitiveDirectory(newImage);
            }
        } else {
            for (ScriptImage currentImage : currentImages) {
                boolean found = false;
                for (ScriptImage newImage : newImages) {
                    //Move imagens para o diretório definitivo
                    moveDefinitiveDirectory(newImage);
                    if (currentImage.equals(newImage)) {
                        found = true;
                    }
                }
                if (!found) {
                    removeImg(currentImage.getPath());
                    removeImg(currentImage.getThumbnailPath());
                }
            }
        }
    }

    private void loadScriptImages(Script script) {
        for (ScriptImage image : script.getImages()) {
            image.setThumbnailBytes(loadImg(image.getThumbnailPath()));
        }
    }

    private void moveDefinitiveDirectory(ScriptImage scriptImage) throws IOException {
        String newPath = ImageUtil.removeTmpDirectory(scriptImage.getPath(), IMAGE_TEMP_DIRECTORY);
        String newThumbnailPath = ImageUtil.removeTmpDirectory(scriptImage.getThumbnailPath(), IMAGE_TEMP_DIRECTORY);
        File sourceImage = new File(scriptImage.getPath());
        File destination = new File(newPath);
        if (!sourceImage.exists() && !sourceImage.mkdirs() && !sourceImage.canWrite()) {
            throw new IOException(ResourceBundle.INSTANCE.getMessage("storage.moveToFolderError", sourceImage.getPath()));
        }
        if (!destination.exists()) {
            FileUtils.moveFile(new File(scriptImage.getPath()), new File(newPath));
            FileUtils.moveFile(new File(scriptImage.getThumbnailPath()), new File(newThumbnailPath));
        }
        scriptImage.setPath(newPath);
        scriptImage.setThumbnailPath(newThumbnailPath);
    }
	//--------------------------------------------------------------------------
	//	Getters and Setters Methods
	//--------------------------------------------------------------------------

	public SessionManager getSessionManager() {
		return sessionManager;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public ScriptBlockDao getScriptBlockDao() {
		return scriptBlockDao;
	}

	public void setScriptBlockDao(ScriptBlockDao scriptBlockDao) {
		this.scriptBlockDao = scriptBlockDao;
	}

	public ScriptDao getScriptDao() {
		return scriptDao;
	}

	public void setScriptDao(ScriptDao scriptDao) {
		this.scriptDao = scriptDao;
	}

	public GuidelineDao getGuidelineDao() {
		return guidelineDao;
	}

	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public ReportageDao getReportageDao() {
		return reportageDao;
	}

	public void setReportageDao(ReportageDao reportageDao) {
		this.reportageDao = reportageDao;
	}

    public void setReportageService(ReportageService reportageService) {
        this.reportageService = reportageService;
    }

    public void setGuidelineService(GuidelineService guidelineService) {
        this.guidelineService = guidelineService;
    }

	public ScriptMessenger getScriptMessenger() {
		return scriptMessenger;
	}

	public void setScriptMessenger(ScriptMessenger scriptMessenger) {
		this.scriptMessenger = scriptMessenger;
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	public ProgramDao getProgramDao() {
		return programDao;
	}

	public void setProgramDao(ProgramDao programDao) {
		this.programDao = programDao;
	}

	public ScriptPlanDao getScriptPlanDao() {
		return scriptPlanDao;
	}

	public void setScriptPlanDao(ScriptPlanDao scriptPlanDao) {
		this.scriptPlanDao = scriptPlanDao;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public ApplicationEventPublisher getPublisher() {
		return publisher;
	}

	public void setPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

    public void setMediaCenterService(MediaCenterService mediaCenterService) {
        this.mediaCenterService = mediaCenterService;
    }

    public void setMosMediaDao(MosMediaDao mosMediaDao) {
        this.mosMediaDao = mosMediaDao;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

}
