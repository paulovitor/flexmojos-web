package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ReportDao;
import tv.snews.anews.dao.ReportFullTextDao;
import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.ReportMessenger;
import tv.snews.anews.search.ReportParams;
import tv.snews.anews.service.ReportService;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * Implementação padrão da interface {@link tv.snews.anews.service.ReportService}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class ReportServiceImpl implements ReportService {

	private final EntityLocker<Report> locker = EntityLockerFactory.getLocker(Report.class);
	
	private ReportDao reportDao;
	private ReportFullTextDao reportFTSDao;
	private ReportMessenger reportMessenger;
	private SessionManager sessionManager;
	
    @Override
    public PageResult<Report> findByParams(ReportParams params) {
    	List<Report> results; int total;
    	
    	// Só usa o Lucene se tiver texto nos parâmetros da busca
    	if (params.hasText()) {
    	    total = reportFTSDao.countByParams(params);
        	results = reportFTSDao.findByParams(params);
    	} else {
    		total = reportDao.countByParams(params);
        	results = reportDao.findByParams(params);
    	}
    	
    	markLocks(results);
    	
    	return new PageResult<>(params.getPage(), total, results);
    }

    @Override
    public Report findById(Long id) {
	    return reportDao.get(id);
    }

    @Override
    public Report save(Report report) throws SessionManagerException, IllegalOperationException {
    	report.setAuthor(sessionManager.getCurrentUser());
    	report.setLastChange(new Date());
    	
	    if (notValidId(report.getId())) {
	    	reportDao.save(report);
	    	
	 	    locker.lock(report, sessionManager.getCurrentUser());
	 	    markLock(report);
	 	    
	    	reportMessenger.notifyInsert(report);
	    } else {
	    	reportDao.update(report);
	    	reportMessenger.notifyUpdate(report);
	    }
	    
	    return report;
    }

    @Override
    public void deleteById(Long id) {
	    Report report = reportDao.get(id);
	    reportDao.delete(report);
	    reportMessenger.notifyDelete(report);
    }
    
    @Override
    public User checkEditionLock(Long id) {
	    Report report = reportDao.get(id);
	    return locker.findOwner(report);
    }

    @Override
    public void lockEdition(Long id) throws IllegalOperationException {
	    Report report = reportDao.get(id);
	    User user = sessionManager.getCurrentUser();
	    locker.lock(report, user);
    }

    @Override
    public void unlockEdition(Long id) throws IllegalOperationException {
    	Report report = reportDao.get(id);
	    User user = sessionManager.getCurrentUser();
	    locker.unlock(report, user);
    }
    
    //------------------------------------
	//  Helpers
	//------------------------------------

    private void markLock(Report report) {
    	report.setEditingUser(locker.findOwner(report));
    }
    
    private void markLocks(Collection<Report> reports) {
    	for (Report report : reports) {
    		markLock(report);
    	}
    }
    
    //------------------------------------
	//  Setters
	//------------------------------------
    
    public void setReportDao(ReportDao reportDao) {
	    this.reportDao = reportDao;
    }
    
    public void setReportFTSDao(ReportFullTextDao reportFTSDao) {
	    this.reportFTSDao = reportFTSDao;
    }
    
    public void setReportMessenger(ReportMessenger value) {
	    this.reportMessenger = value;

		locker.removeAllEntityLockerListeners();
		locker.addEntityLockerListener(new EntityLockerListener<Report>() {

			@Override
            public void entityLocked(Report entity, User user) {
	            reportMessenger.notifyLock(entity, user);
            }

			@Override
            public void entityUnlocked(Report entity, User user) {
				reportMessenger.notifyUnlock(entity, user);
            }
		});
	    
    }
    
    public void setSessionManager(SessionManager sessionManager) {
	    this.sessionManager = sessionManager;
    }
}
