package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.dao.RssFeedDao;
import tv.snews.anews.dao.RssItemDao;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.infra.rss.FeedRetriveException;
import tv.snews.anews.infra.rss.RssFeedFetcher;
import tv.snews.anews.infra.rss.RssFetchedData;
import tv.snews.anews.messenger.RssMessenger;
import tv.snews.anews.service.RssService;
import tv.snews.anews.util.ResourceBundle;

import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;


/**
 * Implementação padrão da interface {@link RssService}.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class RssServiceImpl implements RssService {

	private static final String URL_PATTERN = 
					"^(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?$";

	private final Pattern urlPattern;
	
	private RssFeedDao rssFeedDao;
	private RssItemDao rssItemDao;
	private RssFeedFetcher feedFetcher;

	private RssMessenger rssMessenger;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
    public RssServiceImpl() {
	    urlPattern = Pattern.compile(URL_PATTERN);
    }
	
	@Override
	public Integer saveRssFeed(RssFeed feed) throws ValidationException {
		Integer feedId;
		try {
			validate(feed);
			
			if (feed.getId() != null && feed.getId() > 0) {
				RssFeed persistedFeed = rssFeedDao.get(feed.getId());
				persistedFeed.setName(feed.getName());
				rssFeedDao.update(persistedFeed);
				feedId = persistedFeed.getId();
				rssMessenger.notifyFeedUpdated(feed);
			} else {
				RssFeed checkExistfeed = rssFeedDao.findByUrl(feed.getUrl());
				if(checkExistfeed != null) {
					throw new ValidationException(bundle.getMessage("rss.msg.alreadyRegistredRssChannel"));
				}
				// Se obter o conteúdo sem lançar exceção a URL está correta e válida
				RssFetchedData data;
	            data = checkFeedLoad(feed);
				feed.setLastBuild(new Date());
				feedId = (Integer) rssFeedDao.save(feed);
				doInitialLoad(feed, data);
				rssMessenger.notifyFeedInserted(feed);
			}

        } catch (FeedRetriveException e) {
        	throw new IllegalArgumentException(bundle.getMessage("rss.msg.feeNotFond"), e);
        }
		return feedId;
	}
	
	@Override
	public void deleteRssFeed(RssFeed feed) {
		rssFeedDao.delete(feed);
		rssMessenger.notifyFeedDeleted(feed);
	}
	
	@Override
	public List<RssFeed> listFeeds() {
	    return rssFeedDao.listAll();
	}
	
	@Override
	public int countItemsFromAllFeeds() {
		return rssItemDao.countAll();
	}

	@Override
	public int countItemsFromCategory(RssCategory category) {
	    return rssItemDao.countFromCategory(category);
	}
	
	@Override
	public int countItemsFromFeed(RssFeed feed) {
		return rssItemDao.countFromFeed(feed);
	}

	@Override
	public List<RssItem> listItemsFromAllFeeds(int start, int count) {
		return rssItemDao.listPage(start, count);
	}

	@Override
	public List<RssItem> listItemsFromCategory(RssCategory category, int start, int count) {
	    return rssItemDao.listPage(category, start, count);
	}
	
	@Override
	public List<RssItem> listItemsFromFeed(RssFeed feed, int start, int count) {
		return rssItemDao.listPage(feed, start, count);
	}
	
    @Override
    public RssItem findRssItemById(Long idEntity) {
    	return rssItemDao.get(idEntity);
    }
	
	//----------------------------------------
	//	Private Methods
	//----------------------------------------
	/*
	 * Faz a validação dos dados do feed.
	 */
	private void validate(RssFeed feed) throws ValidationException {
		if (feed.getCategory() == null) {
			throw new ValidationException(bundle.getMessage("rss.feed.noCategory"));
		}
		if (StringUtils.isBlank(feed.getName())) {
			throw new ValidationException(bundle.getMessage("rss.feed.blankName"));
		}
		if (StringUtils.isBlank(feed.getUrl())) {
			throw new ValidationException(bundle.getMessage("rss.feed.invalidUrl"));
		}
		if (!urlPattern.matcher(feed.getUrl()).matches()) {
			throw new ValidationException(bundle.getMessage("rss.feed.invalidUrlFormat"));
		}
	}
	
	private RssFetchedData checkFeedLoad(RssFeed feed) throws FeedRetriveException {
		return feedFetcher.retrieveFeed(feed.getUrl());
	}
	
	private void doInitialLoad(RssFeed feed, RssFetchedData fetchedData) throws FeedRetriveException {
		for (RssItem item : fetchedData.getItems()) {
			feed.addItem(item);
			rssItemDao.save(item);
		}
	}
	
	//----------------------------------------
	//	Setters
	//----------------------------------------
	
    /**
     * @param rssFeedDao Implementação de {@link RssFeedDao}
     */
    public void setRssFeedDao(RssFeedDao rssFeedDao) {
	    this.rssFeedDao = rssFeedDao;
    }
    
    /**
     * @param rssItemDao Implementação de {@link RssItemDao}
     */
    public void setRssItemDao(RssItemDao rssItemDao) {
	    this.rssItemDao = rssItemDao;
    }
    
    /**
     * @param feedFetcher Carregador dos feeds.
     */
    public void setFeedFetcher(RssFeedFetcher feedFetcher) {
	    this.feedFetcher = feedFetcher;
    }
    
    /**
     * @param rssMessenger Implementação no mensageiro de notificações.
     */
    public void setRssMessenger(RssMessenger rssMessenger) {
	    this.rssMessenger = rssMessenger;
    }
}
