package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.ScriptBlockDao;
import tv.snews.anews.dao.ScriptDao;
import tv.snews.anews.dao.ScriptPlanDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.ScriptBlockEvent;
import tv.snews.anews.event.ScriptEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.ScriptBlockMessenger;
import tv.snews.anews.service.ScriptBlockService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.util.ResourceBundle;

import java.io.IOException;
import java.util.Date;

import static org.apache.commons.collections.CollectionUtils.get;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptBlockServiceImpl implements ScriptBlockService, ApplicationEventPublisherAware {

	private ScriptBlockMessenger scriptBlockMessenger;

	private ProgramDao programDao;

	private ScriptPlanDao scriptPlanDao;
	private ScriptBlockDao scriptBlockDao;
    private ScriptDao scriptDao;
    private SessionManager sessionManager;

    /** Despacha eventos **/
    private ApplicationEventPublisher publisher;

	private ScriptService scriptService;
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private static final String DEFAULTS_REQUIRED_FIELDS = "defaults.requiredFields";
	private static final Logger log = LoggerFactory.getLogger(ScriptBlockServiceImpl.class);
	
	private final EntityLocker<Script> scriptLocker = EntityLockerFactory.getLocker(Script.class);

    @Override
	public ScriptBlock create(Long planId) {
		ScriptPlan scriptPlan = scriptPlanDao.get(planId);
		if (scriptPlan == null) {
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
		ScriptBlock block = scriptPlan.createBlock();
		scriptBlockDao.save(block);
		scriptBlockMessenger.notifyInsert(block);
        dispatchEvent(new ScriptBlockEvent(ScriptBlockAction.INSERTED, block, this));
		return block;
	}

	@Override
	public void updateField(Long blockId, Field field, Object value) {
        ScriptBlock block = scriptBlockDao.loadById(blockId, true);
		if (block == null) {
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
		switch (field) {
			case COMMERCIAL:
				block.setCommercial((Date) value);
			break;
			case SLUG:
				block.setSlug((String) value); 
			break;
			default:
				throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}

		scriptBlockDao.save(block);
        scriptBlockMessenger.notifyUpdate(block);

        // Dispara o evento de mudança de bloco
        ScriptEvent scriptEvent = new ScriptEvent(ScriptAction.BLOCK_CHANGED, this);

        for(Script script : block.getScripts()) {
            scriptEvent.registerChange(script, block.getOrder(), block.getOrder());
        }
        dispatchEvent(scriptEvent);
	}

	@Override
	public void remove(Long blockId) throws IllegalOperationException {
		ScriptBlock block = scriptBlockDao.loadById(blockId, true);
		if (block == null) {
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}

		//Verifica se existe algum script em edição e lança excessão
		for (Script script : block.getScripts()) {
			if (scriptLocker.findOwner(script) != null)  {
				throw new IllegalOperationException(bundle.getMessage("scriptblock.cantDelete"));
			} else {
				scriptService.remove(script);
			}
		}
		
		ScriptPlan scriptPlan = scriptPlanDao.loadById(block.getScriptPlan().getId());
		if (scriptPlan == null) {
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
		
		scriptPlan.removeBlock(block);
        scriptPlanDao.save(scriptPlan);
		scriptBlockMessenger.notifyDelete(block);
        dispatchEvent(new ScriptBlockEvent(ScriptBlockAction.EXCLUDED, block, this));
	}

    @Override
    public ScriptBlockStatus synchronizeStandByBlock(Date currentDate, Date targetDate, Integer programId) throws ValidationException, IOException {

        Program program = programDao.get(programId);
        ScriptPlan targetScriptPlan = scriptPlanDao.findByDateAndProgram(targetDate, program);

        if (targetScriptPlan == null){
            return ScriptBlockStatus.SCRIPT_PLAN_NOT_FOUND;
        } else{
            ScriptPlan currentScriptPlan = scriptPlanDao.findByDateAndProgram(currentDate, program);

            currentScriptPlan.standBy().copyScripts(targetScriptPlan.standBy());
            scriptBlockDao.save(targetScriptPlan.standBy());
            if(targetScriptPlan.standBy().getScripts().size() > 0) {
                scriptBlockMessenger.notifyUpdate(currentScriptPlan.standBy());
                dispatchEvent(new ScriptBlockEvent(ScriptBlockAction.UPDATED, currentScriptPlan.standBy(), this));
            }

            return ScriptBlockStatus.SUCCESSFUL_SYNCHRONIZATION;
        }
    }
    
    @Override
    public void updatePosition(Long blockId, int newOrder, Boolean notifyMove) {
        ScriptBlock movedBlock = scriptBlockDao.loadById(blockId, true);
        int oldOrder = movedBlock.getOrder();

        if(oldOrder != newOrder) {
            // Dispara o evento de mudança de bloco
            ScriptEvent scriptEventBlockChange = new ScriptEvent(ScriptAction.BLOCK_CHANGED, this);
            ScriptEvent scriptEventOrderChange = new ScriptEvent(ScriptAction.ORDER_CHANGED, this);
            ScriptBlockEvent scriptBlockEventOrderChange = new ScriptBlockEvent(ScriptBlockAction.ORDER_CHANGED, this);

            /* Drag para baixo do bloco */
            if (oldOrder < newOrder) {
                /*
                 * Fez um drag n' drop para baixo bloco. Neste caso os valores das ordens maiores que 'oldOrder'
                 * e menores ou iguais a 'newOrder' devem ser decrementados.
                 */
                for (int i = oldOrder + 1; i < newOrder; i++) {
                    ScriptBlock current = (ScriptBlock) get(movedBlock.getScriptPlan().getBlocks(), i);
                    changeScriptBlockOrder(scriptEventBlockChange, scriptEventOrderChange, scriptBlockEventOrderChange, current, current.getOrder() - 1);
                }
            }

            /**
             *  Esta chamada deve ficar abaixo da lógica de drag and drop para baixo do bloco e em cima da lógica de drag and drop para
             *  cima do bloco pois isso interfere na lógica de como foi implementado a mensagem MOS. Se alterado pode não notificar mudanças
             *  de drag and drop de primeiro bloco acima ou a baixo do bloco que está sendo dargado.
             *
             * */
            changeScriptBlockOrder(scriptEventBlockChange, scriptEventOrderChange, scriptBlockEventOrderChange, movedBlock, oldOrder < newOrder ? newOrder -1 : newOrder);

            /* Drag para cima do bloco */
            if (oldOrder > newOrder) {
                /*
                 * Fez um drag n' drop para cima do bloco. Neste caso os valores das ordens maiores ou iguais
                 * a 'newOrder' e menores que 'oldOrder' devem ser incrementados.
                 */
                for (int i = newOrder; i < oldOrder; i++) {
                    ScriptBlock current = (ScriptBlock) get(movedBlock.getScriptPlan().getBlocks(), i);
                    changeScriptBlockOrder(scriptEventBlockChange, scriptEventOrderChange, scriptBlockEventOrderChange, current, current.getOrder() + 1);
                }
            }

            scriptBlockDao.flush();

            if (notifyMove) {
                dispatchEvent(scriptEventOrderChange);
                dispatchEvent(scriptEventBlockChange);
                dispatchEvent(scriptBlockEventOrderChange);
                for (ScriptBlock scriptBlock : scriptBlockEventOrderChange.getBlocks()) {
                    scriptBlockMessenger.notifyMove(scriptBlock, sessionManager.getCurrentUser());
                }
            } else {
                scriptBlockMessenger.notifyUpdate(movedBlock);
                dispatchEvent(new ScriptBlockEvent(ScriptBlockAction.UPDATED, movedBlock, this));
            }
        }
    }

    private void changeScriptBlockOrder(ScriptEvent scriptEventBlockChange, ScriptEvent scriptEventOrderChange, ScriptBlockEvent scriptBlockEventOrderChange, ScriptBlock current, int order) {
        scriptBlockEventOrderChange.registerChange(current, current.getOrder(), order);
        current.setOrder(order);
        for(Script script : current.getScripts()) {
            scriptEventBlockChange.registerChange(script, script.getBlock().getOrder(), current.getOrder());
            script.setBlock(current);
            scriptEventOrderChange.registerChange(script, script.getOrder(), script.getOrder());
        }
        scriptBlockDao.update(current);
    }

    private void dispatchEvent(ScriptBlockEvent blockEvent) {
        publisher.publishEvent(blockEvent);
    }

    private void dispatchEvent(ScriptEvent scriptEvent) {
        publisher.publishEvent(scriptEvent);
    }

    public void setSessionManager(SessionManager sessionManager) {
    	this.sessionManager = sessionManager;
    }
    
    public void setScriptBlockMessenger(ScriptBlockMessenger scriptBlockMessenger) {
    	this.scriptBlockMessenger = scriptBlockMessenger;
    }

    public void setScriptPlanDao(ScriptPlanDao scriptPlanDao) {
    	this.scriptPlanDao = scriptPlanDao;
    }

    public void setScriptBlockDao(ScriptBlockDao scriptBlockDao) {
    	this.scriptBlockDao = scriptBlockDao;
    }

    public void setProgramDao(ProgramDao programDao) {
    	this.programDao = programDao;
    }
    
    public void setScriptService(ScriptService scriptService) {
    	this.scriptService = scriptService;
    }

    public ScriptDao getScriptDao() {
        return scriptDao;
    }

    public void setScriptDao(ScriptDao scriptDao) {
        this.scriptDao = scriptDao;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }
}