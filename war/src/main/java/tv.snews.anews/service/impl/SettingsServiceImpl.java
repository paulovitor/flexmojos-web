package tv.snews.anews.service.impl;

import org.apache.commons.io.IOUtils;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.Assert;
import tv.snews.anews.dao.MailConfigDao;
import tv.snews.anews.dao.SettingsDao;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.exception.DataNotFoundException;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.infra.mail.MailConfig;
import tv.snews.anews.infra.mail.MailSender;
import tv.snews.anews.messenger.SettingsMessenger;
import tv.snews.anews.service.SettingsService;
import tv.snews.anews.util.ResourceBundle;

import java.io.IOException;
import java.util.Locale;

import static tv.snews.anews.util.StorageUtil.anewsDirectory;

/**
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class SettingsServiceImpl implements SettingsService, ResourceLoaderAware {

	private Settings settings;
	private SettingsMessenger settingsMessenger;
	private ResourceLoader resourceLoader;

	private final SettingsDao settingsDao;
	private final ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	private MailSender mailSender;
	private MailConfigDao mailConfigDao;

	//----------------------------
	//	Cosntructor
	//----------------------------
	
	public SettingsServiceImpl(SettingsDao settingsDao) throws DataNotFoundException {
		Assert.notNull(bundle); // IllegalArgumentException
		Assert.notNull(settingsDao); // IllegalArgumentException
		
		this.settingsDao = settingsDao;

		settings = this.settingsDao.loadSettings();
		if (settings == null) {
			throw new DataNotFoundException(bundle.getMessage("settings.notFound"));
		}

		Locale.setDefault(settings.getCurrentLocale());
	}

	//----------------------------
	//	Service Operations
	//----------------------------
	
	@Override
	public Settings loadSettings() {
		settings = this.settingsDao.loadSettings();
		return settings;
	}

	@Override
	public String loadCurrentLocale() {
		return settings.getLocale();
	}
	
	@Override
	public void updateSettings(Settings settings) throws MessageDeliveryException {
		settingsDao.update(settings);
		this.settings = settings;
		
		changeCurrentLocale();
		boolean sent = settingsMessenger.notifySettingsChanged(settings);
		if (!sent) {
			throw new MessageDeliveryException(bundle.getMessage("settings.sendLocaleError"));
		}
	}

	@Override
	public MailConfig loadMailConfig() {
	    return mailConfigDao.loadFirst();
	}
	
	@Override
	public void reindexDatabase(){
		settingsDao.reindexDataBase();
		Settings settings =	settingsDao.loadSettings();
		settings.setDisplayIndexAlert(false);
		settingsDao.update(settings);
		settingsMessenger.notifySettingsChanged(settings);
	}
	
	@Override
	public void saveMailConfig(MailConfig newConfig) {
	    MailConfig config = loadMailConfig();
	    if (config == null) {
	    	config = new MailConfig();
	    }
	    config.copy(newConfig);
	    
	    if (config.getId() == null) {
	    	mailConfigDao.save(config);
	    } else {
	    	mailConfigDao.update(config);
	    }
	    
	    // Recarrega as novas configurações
	    mailSender.resetConfiguration();
	}
	
	@Override
	public byte[] loadImg(String imgFileName) throws IOException {
		if (imgFileName != null && !imgFileName.trim().equals("")) {
			String pathFileImg = anewsDirectory() + "img" + System.getProperty("file.separator");
			Resource resource = resourceLoader.getResource("file:" + pathFileImg  + imgFileName);
            return IOUtils.toByteArray(resource.getInputStream());
		}
		return null;
	}
	
	//----------------------------
	//	Helpers
	//----------------------------
	
	private void changeCurrentLocale() throws MessageDeliveryException {
		Locale.setDefault(settings.getCurrentLocale());
	}

	//----------------------------
	//	Setters
	//----------------------------
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}
	
    public void setSettingsMessenger(SettingsMessenger settingsMessenger) {
	    this.settingsMessenger = settingsMessenger;
    }
    
    public void setMailConfigDao(MailConfigDao mailConfigDao) {
	    this.mailConfigDao = mailConfigDao;
    }
    
    public void setMailSender(MailSender mailSender) {
	    this.mailSender = mailSender;
    }
}
