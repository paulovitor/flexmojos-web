package tv.snews.anews.service.impl;

import tv.snews.anews.dao.NumberTypeDao;
import tv.snews.anews.domain.NumberType;
import tv.snews.anews.service.NumberTypeService;

import java.util.List;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class NumberTypeServiceImpl implements NumberTypeService {

	private NumberTypeDao numberTypeDao;

	@Override
	public void save(NumberType numberType) {
		if (numberType.getId() > 0) {
			numberTypeDao.update(numberType);
		} else {
			numberTypeDao.save(numberType);
		}
	}

	@Override
	public void delete(NumberType numberType) {
		if (numberType != null) {
			numberTypeDao.delete(numberType);
		}
	}

	@Override
	public List<NumberType> listAll() {
		return numberTypeDao.listAll();
	}

	//----------------------------------
	//	Setters
	//----------------------------------

	public void setNumberTypeDao(NumberTypeDao numberTypeDao) {
		this.numberTypeDao = numberTypeDao;
	}

}
