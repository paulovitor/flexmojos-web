package tv.snews.anews.service.impl;

import tv.snews.anews.dao.CompanyDao;
import tv.snews.anews.domain.Company;
import tv.snews.anews.messenger.CompanyMessenger;
import tv.snews.anews.service.CompanyService;

import java.util.List;

/**
 * Serviço responsável pelas ações relacionadas ao cadastro de praças.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.3
 */
public class CompanyServiceImpl implements CompanyService {

	private CompanyDao companyDao;
	private CompanyMessenger companyMessenger;

	@Override
	public Company loadById(Integer idCompany) {
		return companyDao.get(idCompany);
	}

	@Override
	public List<Company> listCompanyByNameHost(String name, String host) {
		return companyDao.listCompanyByNameHost(name, host);
	}
	
	@Override
	public void save(Company company) {
		if (company.getId() > 0) {
			companyDao.update(company);
			companyMessenger.notifyUpdated(company);
		} else {
			companyDao.save(company);
			companyMessenger.notifyInserted(company);
		}
	}

	@Override
	public void delete(Integer id) {
		Company company = companyDao.get(id);
		companyDao.delete(company);
		companyMessenger.notifyDeleted(company);
	}

	@Override
	public List<Company> listAll() {
		return companyDao.listAll();
	}

	//----------------------------------
	//	Setters
	//----------------------------------
	/**
	 * @param companyDao the companyDao to set
	 */
	public void setCompanyDao(CompanyDao companyDao) {
		this.companyDao = companyDao;
	}

	/**
	 * @param companyMessenger the companyMessenger to set
	 */
	public void setCompanyMessenger(CompanyMessenger companyMessenger) {
		this.companyMessenger = companyMessenger;
	}

}
