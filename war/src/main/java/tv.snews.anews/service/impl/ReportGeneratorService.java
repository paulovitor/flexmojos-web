package tv.snews.anews.service.impl;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.report.ScriptBlockReportData;
import tv.snews.anews.report.ScriptPlanReportData;
import tv.snews.anews.search.ChecklistParams;
import tv.snews.anews.search.GuidelineParams;
import tv.snews.anews.search.ReportageParams;
import tv.snews.anews.search.StoryParams;
import tv.snews.anews.util.*;
import tv.snews.anews.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.Calendar.HOUR;
import static java.util.Calendar.MINUTE;
import static org.springframework.web.bind.ServletRequestUtils.*;
import static tv.snews.anews.util.StorageUtil.anewsDirectory;

/**
 * @author Felipe Zap de Mello
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class ReportGeneratorService extends MultiActionController implements ResourceLoaderAware {

	private static final Logger log = LoggerFactory.getLogger(ReportGeneratorService.class);

	private BlockDao blockDao;
	private ChecklistDao checklistDao;
	private ChecklistFullTextDao checklistFTDao;
	private ChecklistTypeDao checklistTypeDao;
	private GuidelineDao guidelineDao;
	private GuidelineClassificationDao guidelineClassificationDao;
	private TeamDao teamDao;
	private GuidelineFullTextDao guidelineFTSDao;
	private ReportageFullTextDao reportageFTSDao;
	private NewsDao newsDao;
	private ProgramDao programDao;
	private ReportageDao reportageDao;
	private ReportDao reportDao;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private ResourceLoader resourceLoader;
	private RoundDao roundDao;
	private RundownDao rundownDao;
    private ScriptDao scriptDao;
    private ScriptPlanDao scriptPlanDao;
	private SettingsDao settingsDao;
	private StoryDao storyDao;
    private StoryFullTextDao storyFTSDao;
	private UserDao userDao;

	/**
	 * O método que mapeaia a requisição localhost:8080/anews/reports/round.do
	 * a palavra Handler é um sufixo necessário conforme configuração de
	 * web-context.xml em resources.
	 */
	public ModelAndView newsHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<News> data = new ArrayList<>();
        loadEntities(ids, newsDao, data);

		String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("newsReport", model);
	}

	public ModelAndView roundHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
		String dateAsString = getStringParameter(req, "date");
		String ids = getStringParameter(req, "ids");

		List<Round> data;
		if (dateAsString != null) {
			Date date = getDateFormat().parse(dateAsString);
			data = roundDao.listAll(date);
		} else {
			data = new ArrayList<>();
			loadEntities(ids, roundDao, data);
		}

		return new ModelAndView("roundReport", createBasicModel(data));
	}

	public ModelAndView taskHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<Guideline> data = new ArrayList<>();
        loadEntities(ids, guidelineDao, data);

		for (Guideline guideline : data) {
            Guide guide = guideline.firstGuide();

            if (guide != null) {
                Calendar guidelineCal = Calendar.getInstance();
                guidelineCal.setTime(guideline.getDate());

                Calendar guideCal = Calendar.getInstance();
                if(guide.getSchedule() != null) {
                    guideCal.setTime(guide.getSchedule());
                }

                guidelineCal.set(HOUR, guideCal.get(HOUR));
                guidelineCal.set(MINUTE, guideCal.get(MINUTE));

                guideline.setGuideDate(guidelineCal.getTime());
            }
		}

		String pathImageCheck = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/check.png";
		String pathImageUncheck = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/unchecked.png";
		
		String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
		model.put("check", getImageStream(pathImageCheck));
		model.put("uncheck", getImageStream(pathImageUncheck));
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("taskReport", model);
	}
	
	public ModelAndView checklistHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<Checklist> data = new ArrayList<>();
		loadEntities(ids, checklistDao, data);
		
		String pathImageCheck = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/check.png";
		String pathImageUncheck = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/unchecked.png";
		
		String subReportDir = getStringParameter(req, "REPORT_DIR");
		
		Map<String, Object> model = createBasicModel(data);
		model.put("check", getImageStream(pathImageCheck));
		model.put("uncheck", getImageStream(pathImageUncheck));
		model.put("SUBREPORT_DIR", subReportDir);
		
		return new ModelAndView("checklistReport", model);
	}


	public ModelAndView checklistGridHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
		List<Checklist> data = new ArrayList<>();
        Date date = getStringParameter(req, "date").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "date"));
        data = checklistDao.findAllNotExcludedOfDate(date, false);
		String subReportDir = getStringParameter(req, "REPORT_DIR");

		Map<String, Object> model = createBasicModel(data);
		model.put("date", date);
		model.put("title", bundle.getMessage("checklist.title"));
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("checklistGridReport", model);
	}

	public ModelAndView checklistArchiveGridHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
		List<Checklist> data;
        ChecklistParams checklistParams = new ChecklistParams();

        Date periodStart = getStringParameter(req, "periodStart").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodStart"));
        Date periodEnd = getStringParameter(req, "periodEnd").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodEnd"));
        User producer = getStringParameter(req, "producerId").isEmpty() ? null : userDao.get(new Integer(getStringParameter(req, "producerId")));
        ChecklistType checklistType = getStringParameter(req, "checklistTypeId").isEmpty() ? null : checklistTypeDao.get(new Integer(getStringParameter(req, "checklistTypeId")));

        checklistParams.setInitialDate(periodStart);
        checklistParams.setFinalDate(periodEnd);
        checklistParams.setProducer(producer);
        checklistParams.setType(checklistType);
        checklistParams.setSlug(getStringParameter(req, "slug"));
        checklistParams.setText(getStringParameter(req, "text"));
        checklistParams.setSearchType(SearchType.valueOf(getStringParameter(req, "searchType")));
        checklistParams.setIgnoreText(getStringParameter(req, "ignoreText"));
        checklistParams.setPage(1);

        if (checklistParams.hasText()) {
            data = checklistFTDao.listTheFirstHundredByParams(checklistParams);
        } else {
            data = checklistDao.findByParams(checklistParams, 100);
        }

		String subReportDir = getStringParameter(req, "REPORT_DIR");

		Map<String, Object> model = createBasicModel(data);
		model.put("date", new Date());
        model.put("title", bundle.getMessage("checklist.archive.areaTitle"));
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("checklistGridReport", model);
	}

	public ModelAndView guidelineGridHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
		String dateAsString = getStringParameter(req, "date");
		Date date = getDateFormat().parse(dateAsString);

		Integer programId = getIntParameter(req, "programId");
		Program program = null;

		if (programId == -1) { // Gaveta Geral
			program = new Program();
			program.setId(-1);
		} else if (programId != null) { // Programa Específico
			program = programDao.get(programId);
		}

		String ids = getStringParameter(req, "ids");
		List<Guideline> data = new ArrayList<>();

		if (StringUtils.isNotEmpty(ids)) {
			loadEntities(ids, guidelineDao, data);
		} else {
			data = guidelineDao.findByDate(date, program, null);
		}

		Settings settings = settingsDao.loadSettings();

		Map<String, Object> model = createBasicModel(data);
		model.put("displayTeam", settings.isEnableGuidelineTeam());
		return new ModelAndView("guidelineGridReport", model);
	}

	public ModelAndView guidelineArchiveGridHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
        GuidelineParams guidelineParams = new GuidelineParams();
        Program program = null;
        String programToCopyId = getStringParameter(req, "programId");

        if (programToCopyId != null && !programToCopyId.isEmpty()) {
            if (programToCopyId.equals("-1")) {
                program = new Program();
                program.setId(-1);
            } else {
                program = programDao.get(new Integer(programToCopyId));
            }
        }

        User producer = getStringParameter(req, "producerId").isEmpty() ? null : userDao.get(new Integer(getStringParameter(req, "producerId")));
        User reporter = getStringParameter(req, "reporterId").isEmpty() ? null : userDao.get(new Integer(getStringParameter(req, "reporterId")));
        GuidelineClassification classification = getStringParameter(req, "classificationText").isEmpty() ? null : guidelineClassificationDao.get(new Integer(getStringParameter(req, "classificationText")));
        Team team = getStringParameter(req, "team").isEmpty() ? null : teamDao.get(new Integer(getStringParameter(req, "team")));

        Date periodStart = getStringParameter(req, "periodStart").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodStart"));
        Date periodEnd = getStringParameter(req, "periodEnd").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodEnd"));


        guidelineParams.setInitialDate(periodStart);
        guidelineParams.setFinalDate(periodEnd);
        guidelineParams.setProgram(program);
        guidelineParams.setProducer(producer);
        guidelineParams.setReporter(reporter);
        guidelineParams.setSlug(getStringParameter(req, "slug"));
        guidelineParams.setText(getStringParameter(req, "text"));
        guidelineParams.setSearchType(SearchType.valueOf(getStringParameter(req, "type")));
        guidelineParams.setIgnoreText(getStringParameter(req, "ignoreText"));
        guidelineParams.setClassification(classification);
        guidelineParams.setTeam(team);
        guidelineParams.setPage(1);
        guidelineParams.setVehicle(getStringParameter(req, "vehicle").isEmpty() ? null : CommunicationVehicle.valueOf(getStringParameter(req, "vehicle")));


        List<Guideline> data = guidelineFTSDao.listTheFirstHundredByParams(guidelineParams);
        String subReportDir = getStringParameter(req, "REPORT_DIR");
        String pathImage = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/";

		Settings settings = settingsDao.loadSettings();

        Map<String, Object> model = createBasicModel(data);
        model.put("PATH_IMAGE", pathImage);
        model.put("SUBREPORT_DIR", subReportDir);
		model.put("displayTeam", settings.isEnableGuidelineTeam());

		return new ModelAndView("guidelineArchiveGridReport", model);
	}

	public ModelAndView reportageArchiveGridHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
        ReportageParams reportageParams = new ReportageParams();
        Program program = null;
        String programToCopyId = getStringParameter(req, "programId");

        if (programToCopyId != null && !programToCopyId.isEmpty()) {
            if (programToCopyId.equals("-1")) {
                program = new Program();
                program.setId(-1);
            } else {
                program = programDao.get(new Integer(programToCopyId));
            }
        }

        User reporter = getStringParameter(req, "reporterId").isEmpty() ? null : userDao.get(new Integer(getStringParameter(req, "reporterId")));
        Date periodStart = getStringParameter(req, "periodStart").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodStart"));
        Date periodEnd = getStringParameter(req, "periodEnd").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodEnd"));


        reportageParams.setInitialDate(periodStart);
        reportageParams.setFinalDate(periodEnd);
        reportageParams.setProgram(program);
        reportageParams.setReporter(reporter);
        reportageParams.setSlug(getStringParameter(req, "slug"));
        reportageParams.setText(getStringParameter(req, "text"));
        reportageParams.setSearchType(SearchType.valueOf(getStringParameter(req, "type")));
        reportageParams.setIgnoreText(getStringParameter(req, "ignoreText"));
        reportageParams.setPage(1);


        List<Reportage> data = reportageFTSDao.listTheFirstHundredByParams(reportageParams);
        String subReportDir = getStringParameter(req, "REPORT_DIR");
        String pathImage = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/";

        Map<String, Object> model = createBasicModel(data);
        model.put("PATH_IMAGE", pathImage);
        model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("reportageArchiveGridReport", model);
	}

	public ModelAndView reportageHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<Reportage> data = new ArrayList<>();
        loadEntities(ids, reportageDao, data);

		String subReportDir = getStringParameter(req, "REPORT_DIR");
        String pathImage = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/";

        Map<String, Object> model = createBasicModel(data);
        model.put("PATH_IMAGE", pathImage);
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("reportageReport", model);
	}

	public ModelAndView rundownHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		Long id = getLongParameter(req, "id");
		Boolean onlyApproved = getBooleanParameter(req, "onlyApproved");
		Boolean includeStandBy = getBooleanParameter(req, "includeStandBy");

		Rundown rundown = rundownDao.get(id);
		Date totalCommercial = DateTimeUtil.minimizeTime(new Date());

		for (Block b : blockDao.listAll(rundown)) {
			totalCommercial = DateTimeUtil.sumTimes(totalCommercial, b.getCommercial());
		}
 
		List<Story> data = storyDao.listStoriesByRundown(rundown, onlyApproved, includeStandBy);
		Collections.sort(data);

		Settings settings = settingsDao.loadSettings();

        Map<String, Object> model = createBasicModel(data);
		model.put("totalBlockComercial", totalCommercial);
		model.put("displayImageEditor", settings.isEnableRundownImageEditor());

		return new ModelAndView("rundownReport", model);
	}


	public ModelAndView storyHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<Story> data = new ArrayList<>();
        loadEntities(ids, storyDao, data);
        Collections.sort(data);
        
        for (Story story : data) {
            story.calculateTotal();
        }

		String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("storyReport", model);
	}

    public ModelAndView storyPlaylistHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
        String ids = getStringParameter(req, "ids");
        List<Story> data = new ArrayList<>();
        loadEntities(ids, storyDao, data);

        for (Story story : data) {
            story.calculateTotal();
        }

        String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
        model.put("SUBREPORT_DIR", subReportDir);

        return new ModelAndView("storyPlaylistReport", model);
    }

    public ModelAndView scriptPlaylistHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
        String ids = getStringParameter(req, "ids");
        List<Script> data = new ArrayList<>();
        loadEntities(ids, scriptDao, data);

        String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
        model.put("SUBREPORT_DIR", subReportDir);

        return new ModelAndView("scriptPlaylistReport", model);
    }
	

	public ModelAndView storyOffHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<Story> data = new ArrayList<>();
        loadEntities(ids, storyDao, data);
        Collections.sort(data);

        for (Story story : data) {
            story.calculateTotal();
        }
		
		String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("storyOffReport", model);
	}

    public ModelAndView storyArchiveGridHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException {
        StoryParams storyParams = new StoryParams();
        Program program = null;
        String programToCopyId = getStringParameter(req, "programId");

        if (programToCopyId != null && !programToCopyId.isEmpty()) {
            if (programToCopyId.equals("-1")) {
                program = new Program();
                program.setId(-1);
            } else {
                program = programDao.get(new Integer(programToCopyId));
            }
        }

        Date periodStart = getStringParameter(req, "periodStart").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodStart"));
        Date periodEnd = getStringParameter(req, "periodEnd").isEmpty() ? null : getDateFormat().parse(getStringParameter(req, "periodEnd"));

        storyParams.setInitialDate(periodStart);
        storyParams.setFinalDate(periodEnd);
        storyParams.setProgram(program);
        storyParams.setSlug(getStringParameter(req, "slug"));
        storyParams.setText(getStringParameter(req, "text"));
        storyParams.setSearchType(SearchType.valueOf(getStringParameter(req, "type")));
        storyParams.setIgnoreText(getStringParameter(req, "ignoreText"));
        storyParams.setPage(1);


        List<Story> data = storyFTSDao.listTheFirstHundredByParams(storyParams);
        String subReportDir = getStringParameter(req, "REPORT_DIR");
        String pathImage = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/";

        Map<String, Object> model = createBasicModel(data);
        model.put("PATH_IMAGE", pathImage);
        model.put("SUBREPORT_DIR", subReportDir);

        return new ModelAndView("storyArchiveGridReport", model);
    }

	public ModelAndView cgsHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
		String ids = getStringParameter(req, "ids");
		List<Story> data = new ArrayList<>();
        loadEntities(ids, storyDao, data);

        for (Story story : data) {
            if (story.getCgSubSections().isEmpty()) {
                story.setCgSubSections(story.subSectionsOfType(StoryCG.class));
            }
        }

		String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
		model.put("SUBREPORT_DIR", subReportDir);

		return new ModelAndView("cgsReport", model);
	}

    public ModelAndView reportHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException {
        String ids = getStringParameter(req, "ids");
        List<Report> data = new ArrayList<>();
        loadEntities(ids, reportDao, data);

        for (Report report : data) {
            if (report instanceof Report) {
                report.setOrigin(bundle.getMessage("report.other"));
            }
            if (report instanceof GuidelineReport) {
                report.setOrigin(bundle.getMessage("report.guideline"));
            }
            if (report instanceof ReportageReport) {
                report.setOrigin(bundle.getMessage("report.reportage"));
            }
            if (report instanceof StoryReport) {
                report.setOrigin(bundle.getMessage("report.story"));
            }
        }

        String subReportDir = getStringParameter(req, "REPORT_DIR");

        Map<String, Object> model = createBasicModel(data);
        model.put("SUBREPORT_DIR", subReportDir);

        return new ModelAndView("reportReport", model);
    }

	public ModelAndView scriptPlanHandler(HttpServletRequest req, HttpServletResponse res) throws ServletRequestBindingException {
		Long id = getLongParameter(req, "id");
		Boolean onlyApproved = getBooleanParameter(req, "onlyApproved");

		ScriptPlan scriptPlan = scriptPlanDao.get(id);
		ScriptPlanReportData scriptPlanReportData = new ScriptPlanReportData(scriptPlan, onlyApproved);

		String reportDir = getStringParameter(req, "REPORT_DIR");

        Settings settings = settingsDao.loadSettings();

		Map<String, Object> model = createBasicModel(scriptPlanReportData);
		model.put("SUBREPORT_DIR", reportDir);
        model.put("displayImageEditor", settings.isEnableScriptPlanImageEditor());

		return new ModelAndView("scriptPlanReport", model);
	}

	public ModelAndView scriptPlanFullHandler(HttpServletRequest req, HttpServletResponse res) throws ServletRequestBindingException {
		Long id = getLongParameter(req, "id");
		Long blockId = getLongParameter(req, "blockId");
		Boolean onlyApproved = getBooleanParameter(req, "onlyApproved");

        ScriptPlan scriptPlan = scriptPlanDao.loadFull(id, blockId);

		ScriptPlanReportData scriptPlanFullReportData = new ScriptPlanReportData(scriptPlan, onlyApproved);

        for(ScriptBlockReportData scriptBlockReportData : scriptPlanFullReportData.getBlocksData()) {
            for (Script script : scriptBlockReportData.getScripts()) {
                for (ScriptImage scriptImage : script.getImages()) {
                    scriptImage.setThumbnailBytes(ImageUtil.loadImg(scriptImage.getThumbnailPath()));
                }
            }
        }

		String reportDir = getStringParameter(req, "REPORT_DIR");

		Map<String, Object> model = createBasicModel(scriptPlanFullReportData);
        Settings settings = settingsDao.loadSettings();
        model.put("fontFamily", FontFamily.parse(settings.getReportFontFamily()));
        model.put("fontSize", FontSize.parse(settings.getReportFontSize()));
        model.put("bundle", bundle);
        model.put("logo", getLogoStream());
		model.put("SUBREPORT_DIR", reportDir);

		return new ModelAndView("scriptPlanFullReport", model);
	}

    public ModelAndView scriptHandler(HttpServletRequest req, HttpServletResponse resp) throws ServletRequestBindingException, ParseException, IOException {
        String ids = getStringParameter(req, "ids");
        List<Script> data = new ArrayList<>();
        loadEntities(ids, scriptDao, data);
        Collections.sort(data);

        Iterator itData = data.iterator();

        while (itData.hasNext()) {
            Script current = (Script) itData.next();
            for (ScriptImage scriptImage : current.getImages()) {
                scriptImage.setThumbnailBytes(ImageUtil.loadImg(scriptImage.getThumbnailPath()));
            }
        }

        Map<String, Object> model = createBasicModel(data);

        String pathImage = "http://" + req.getServerName() + ":" + req.getServerPort() + req.getContextPath() + "/reports/img/";
        String subReportDir = getStringParameter(req, "REPORT_DIR");

        Settings settings = settingsDao.loadSettings();

        model.put("fontFamily", FontFamily.parse(settings.getReportFontFamily()));
        model.put("fontSize", FontSize.parse(settings.getReportFontSize()));

        model.put("bundle", bundle);
        model.put("data", data);
        model.put("logo", getLogoStream());
        model.put("PATH_IMAGE", pathImage);
        model.put("SUBREPORT_DIR", subReportDir);

        return new ModelAndView("scriptReport", model);
    }

    //----------------------------------
    //  Helpers
    //----------------------------------

    @SuppressWarnings("rawtypes")
    private <T extends AbstractEntity> void loadEntities(String ids, GenericDao<T> dao, List<T> data) {
        if (StringUtils.isNotEmpty(ids)) {
            for (String isStr : ids.split("\\|")) {
                try {
                    Long id = Long.parseLong(isStr);
                    T entity = dao.get(id, true);
                    if (entity != null) {
                        data.add(entity);
                    }
                } catch (NumberFormatException e) {
                    log.error("Could not parse the string (ID) to a Long.", e);
                }
            }
        }
    }

	private Map<String, Object> createBasicModel(Object data) {
		List<Object> dataList = new ArrayList<>();
		dataList.add(data);
		return createBasicModel(dataList);
	}

    @SuppressWarnings("rawtypes")
    private Map<String, Object> createBasicModel(List<?> data) {
        Map<String, Object> model = new HashMap<>();

        model.put("bundle", bundle);
        model.put("data", data);
        model.put("logo", getLogoStream());

        Settings settings = settingsDao.loadSettings();

        model.put("fontFamily", FontFamily.parse(settings.getReportFontFamily()));
        model.put("fontSize", FontSize.parse(settings.getReportFontSize()));

        return model;
    }

	private InputStream getLogoStream() {
		String logoPath =  anewsDirectory() + "img" + System.getProperty("file.separator");
        Settings settings = settingsDao.loadSettings();

        if (StringUtils.isNotBlank(settings.getStorageLocationReportLogo())) {
            try {
                Resource resource = resourceLoader.getResource("file:" + logoPath + settings.getStorageLocationReportLogo());
                return resource.getInputStream();
            } catch (IOException e) {
                log.warn("Could not load the report logo: {}.", e.getMessage());
            }
        }

        return null;
	}
	
	private InputStream getImageStream(String pathImage) {
		if (StringUtils.isNotBlank(pathImage)) {
			try {
				Resource resource = resourceLoader.getResource(pathImage);
				return resource.getInputStream();
			} catch (IOException e) {
				log.warn("Could not load the report logo: {}.", e.getMessage());
			}
		}
		
		return null;
	}

    private SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat(bundle.getMessage("defaults.dateFormat"));
    }

    //----------------------------------
    //  Setters
    //----------------------------------

    public void setNewsDao(NewsDao newsDao) {
    	this.newsDao = newsDao;
    }

    public void setProgramDao(ProgramDao programDao) {
        this.programDao = programDao;
    }

	public void setRoundDao(RoundDao roundDao) {
		this.roundDao = roundDao;
	}

    public void setChecklistDao(ChecklistDao checklistDao) {
    	this.checklistDao = checklistDao;
    }

    public void setChecklistFTDao(ChecklistFullTextDao checklistFTDao) {
        this.checklistFTDao = checklistFTDao;
    }

    public void setChecklistTypeDao(ChecklistTypeDao checklistTypeDao) {
        this.checklistTypeDao = checklistTypeDao;
    }

	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

    public void setGuidelineClassificationDao(GuidelineClassificationDao guidelineClassificationDao) {
        this.guidelineClassificationDao = guidelineClassificationDao;
    }

    public void setGuidelineFTSDao(GuidelineFullTextDao guidelineFTSDao) {
        this.guidelineFTSDao = guidelineFTSDao;
    }

    public void setReportageFTSDao(ReportageFullTextDao reportageFTSDao) {
        this.reportageFTSDao = reportageFTSDao;
    }

    public void setReportageDao(ReportageDao reportageDao) {
		this.reportageDao = reportageDao;
	}

    public void setStoryDao(StoryDao storyDao) {
    	this.storyDao = storyDao;
    }
	
	public void setRundownDao(RundownDao rundownDao) {
		this.rundownDao = rundownDao;
	}
	
    public void setBlockDao(BlockDao blockDao) {
    	this.blockDao = blockDao;
    }

    public void setScriptDao(ScriptDao scriptDao) {
        this.scriptDao = scriptDao;
    }

    public void setScriptPlanDao(ScriptPlanDao scriptPlanDao) {
		this.scriptPlanDao = scriptPlanDao;
	}

	public void setSettingsDao(SettingsDao settingsDao) {
		this.settingsDao = settingsDao;
	}

    public void setReportDao(ReportDao reportDao) {
    	this.reportDao = reportDao;
    }
	
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		this.resourceLoader = resourceLoader;
	}

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setStoryFTSDao(StoryFullTextDao storyFTSDao) {
        this.storyFTSDao = storyFTSDao;
    }

    public void setTeamDao(TeamDao teamDao) {
        this.teamDao = teamDao;
    }
}
