package tv.snews.anews.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.dao.ScriptBlockDao;
import tv.snews.anews.dao.ScriptDao;
import tv.snews.anews.dao.ScriptPlanDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.ScriptEvent;
import tv.snews.anews.event.ScriptPlanEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.infra.ClipboardAction;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.infra.intelligentinterface.IntelligentInterface;
import tv.snews.anews.infra.intelligentinterface.TelnetCommand;
import tv.snews.anews.messenger.ScriptMessenger;
import tv.snews.anews.messenger.ScriptPlanMessenger;
import tv.snews.anews.service.ScriptPlanService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.anews.util.ResourceBundle;
import tv.snews.intelligentinterface.exception.TelnetException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static tv.snews.anews.domain.ScriptAction.PAGE_CHANGED;
import static tv.snews.anews.domain.ScriptPlanAction.*;
import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ScriptPlanServiceImpl implements ScriptPlanService, ApplicationEventPublisherAware {

    private final TelnetCommand telnetCommand;
    private static final Logger log = LoggerFactory.getLogger(ScriptPlan.class);
    private ResourceBundle bundle = ResourceBundle.INSTANCE;
    private ApplicationEventPublisher publisher;
    private SessionManager sessionManager;

    private ProgramDao programDao;
    private ScriptPlanDao scriptPlanDao;
    private ScriptBlockDao scriptBlockDao;
    private ScriptDao scriptDao;
    private ScriptService scriptService;
    private User intelligentInterfaceUser;
    private ScriptMessenger scriptMessenger;
    private IntelligentInterface intelligentInterface;

    private ScriptPlanMessenger scriptPlanMessenger;
	private EntityLocker<Script> scriptLocker;


	public ScriptPlanServiceImpl() {
		scriptLocker = EntityLockerFactory.getLocker(Script.class);
        telnetCommand = new TelnetCommand();
	}

    @Override
	public ScriptPlan load(Date date, Integer programId) {
        Program program = programDao.get(programId);

        if (date == null || program == null) {
			String errorMessage = bundle.getMessage("defaults.requiredFields");
            log.error(errorMessage);
            throw new ValidationException(errorMessage);
        }

        ScriptPlan scriptPlan = scriptPlanDao.findByDateAndProgram(date, program);

        if (scriptPlan != null) {
            for (ScriptBlock block : scriptPlan.getBlocks()) {
                for (Script script : block.getScripts()) {
                    script.setEditingUser(scriptLocker.findOwner(script));

                    if(sessionManager.containsOnClipboard(script.getId())) {
                        ClipboardAction action = sessionManager.clipboardAction();
                        script.setCutted(action == ClipboardAction.CUT);
                        script.setCopied(action == ClipboardAction.COPY);
                    }
                }
            }
        }

        return scriptPlan;
	}

	@Override
	public ScriptPlan create(Date date, Integer programId) throws IOException {
        Program program = programDao.get(programId);

        if (date == null || program == null) {
            String errorMessage = bundle.getMessage("defaults.msg.argumentsFail");
            throw new ValidationException(errorMessage);
        } else {
            if (verifyScriptPlan(date, program.getId()) == ScriptPlanStatus.NOT_EXIST) {
                //User author = sessionManager.getCurrentUser();
                ScriptPlan scriptPlan = new ScriptPlan(date, program, program.getStart(), program.getEnd(), getMidnightTime());

                ScriptBlock standBy = scriptBlockDao.findLastStandByBlock(program, scriptPlan.getDate());
                if (standBy == null) {
                    standBy = new ScriptBlock();
                    standBy.setScriptPlan(scriptPlan);
                    standBy.setOrder(0);
                    standBy.setCommercial(DateTimeUtil.getMidnightTime());
                    standBy.setStandBy(true);

                    scriptPlan.getBlocks().add(standBy);
                }else{
                    scriptPlan.getBlocks().add(new ScriptBlock(standBy, scriptPlan));
                    scriptPlan.renumberPages();
                }
                //Deve chamar esse save apenas após criar o bloco standBy
                scriptPlan.setId((Long) scriptPlanDao.save(scriptPlan)); // necessário para não quebrar testes

                // Dispara evento para log & notificação MOS
                dispatchEvent(new ScriptPlanEvent(CREATED, scriptPlan, this));

                return scriptPlan;
            } else {
                String errorMessage = bundle.getMessage("defaults.msg.saveFail");
                throw new ValidationException(errorMessage);
            }
        }
	}

	@Override
	public void updateField(Long planId, String field, Object value) {
		ScriptPlan scriptPlan = scriptPlanDao.get(planId);
		ScriptPlanEvent planEvent = null;

		switch (ScriptPlan.Fields.valueOf(field.toUpperCase())) {
			case SLUG:
				planEvent = new ScriptPlanEvent(SLUG_CHANGED, scriptPlan, this);
				scriptPlan.setSlug((String) value);
				break;
			case CHANNEL:
				scriptPlan.setChannel((String) value);
				break;
			case START:
				planEvent = new ScriptPlanEvent(START_CHANGED, scriptPlan, this);
				scriptPlan.setStart((Date) value);
				break;
			case END:
				planEvent = new ScriptPlanEvent(END_CHANGED, scriptPlan, this);
				scriptPlan.setEnd((Date) value);
				break;
			case PRODUCTION:
				scriptPlan.setProductionLimit((Date) value);
				break;
			case DATE:
				planEvent = new ScriptPlanEvent(DATE_CHANGED, scriptPlan, this);
				scriptPlan.setDate((Date) value);
				scriptPlan.updateStoriesDates();
				break;
		}

		if (planEvent != null) {
			dispatchEvent(planEvent);
		}

		scriptPlanDao.save(scriptPlan);
		scriptPlanMessenger.notifyUpdateField(planId, field, value);
	}

    @Override
    public String sendCredits(Long scriptPlanId) throws SessionManagerException, IOException, TelnetException {
        ScriptPlan scriptPlan = scriptPlanDao.get(scriptPlanId);
        Program program = scriptPlan.getProgram();
        String result = null;
        if (program.getCgDevice() instanceof IIDevice) {
            IIDevice device = (IIDevice) program.getCgDevice();
            if (!device.isVirtual()) {
                result = sendCreditsToIIDevice(scriptPlan, device);
            }
        }
        return result;
    }

	private String sendCreditsToIIDevice(ScriptPlan scriptPlan, IIDevice device) throws IOException, TelnetException {
		Set<String> commands = new HashSet<>();

		intelligentInterfaceUser = sessionManager.getCurrentUser();
		commands.add(telnetCommand.selectMessageDirectory(scriptPlan.getProgram().getCgPath().getPath()));

		boolean containsNonApproved = false;
		boolean containsApproved = false;
		int code = device.getIncrement();

		for (ScriptBlock block : scriptPlan.getBlocks()) {
			for (Script script : block.getScripts()) {
				if (script.isApproved()) {
					code = updateScriptCGs(commands, code, script);
					if (script.getCgs().size() > 0) {
						containsApproved = true;
					} else {
						containsNonApproved = true;
					}
				} else containsNonApproved = true;
			}
		}

		if (commands.size() > 0) {
			intelligentInterface.sendCredits(device, commands);
		}
		// Caso tenha scripts aprovados e não tenha scripts não aprovados significa que todos estão aprovados e foram enviados
		// Caso contrário, verifica se além de aprovados, também tem não aprovados
		// Caso contrário, existem apenas não aprovados, logo, nenhum para ser enviado
		return containsApproved && !containsNonApproved ? "OK" : containsApproved && containsNonApproved ? "CONTAINS_NON_APPROVED" : "NOT_SENT";
	}

	private int updateScriptCGs(Set<String> commands, int code, Script script) {
		boolean updateScriptCGs = false;

		for (AbstractScriptItem cg : script.getCgs()) {
			if (cg instanceof ScriptCG) {
				ScriptCG scriptCg = (ScriptCG) cg;
				if (!scriptCg.isComment() && !scriptCg.isDisabled()) {
					scriptCg.setCode(code++);
					commands.add(telnetCommand.getWrite(scriptCg));
					updateScriptCGs = true;
				}
			}
		}
		if (updateScriptCGs) {
			scriptDao.update(script);
			scriptMessenger.notifyUpdate(script);
		}
		return code;
	}

	@Override
	public void renumberPages(Long planId) {
		// TODO Procurar manter a regra de negócio em ScriptPlan.renumberPages()
	}

    public synchronized ScriptPlanStatus verifyScriptPlan(Date date, Integer programId) throws ValidationException {
        Program program = programDao.get(programId);

        if (date == null || program == null) {
            String errorMessage = bundle.getMessage("defaults.msg.argumentsFail");
            log.error(errorMessage);
            throw new ValidationException(errorMessage);
        }

        int countScriptPlan = scriptPlanDao.countByDateAndProgram(date, program);
        if (countScriptPlan == 0) { //não achou
            Date today = DateTimeUtil.minimizeTime(new Date());

            if (date.equals(today) || date.after(today)) {
                // Se a data for maior ou igual a data de hoje, pode criar
                return ScriptPlanStatus.NOT_EXIST;
            } else {
                // Data antiga, não pode criar
                return ScriptPlanStatus.CANT_CREATE;
            }
        } else {
            return ScriptPlanStatus.EXIST;
        }
    }


    @Override
    public synchronized void sortScripts(Long planId) {
        ScriptPlan scriptPlan = scriptPlanDao.get(planId);
        int page = 0;

        for (ScriptBlock block : scriptPlan.getBlocks()) {
            for (Script script : block.getScripts()) {
                String newPage = ++page < 10 ? "0" + page : "" + page;
                if (!script.getPage().equals(newPage)) {
                    script.setPage(newPage);
                    ScriptEvent scriptEvent = new ScriptEvent(PAGE_CHANGED, this);
                    scriptEvent.registerChange(script, script.getPage(), newPage);
                    scriptDao.save(script);
                    // Dispara evento para log & MOS
                    dispatchEvent(scriptEvent);
                }
            }
        }
        scriptPlanMessenger.notifySort(scriptPlan);
    }

    @Override
    public void resetScriptPlanExhibition(Long scriptPlanId) throws IllegalOperationException {
        ScriptPlan scriptPlan = scriptPlanDao.get(scriptPlanId);
        scriptPlan.setDisplayed(false);
        for (ScriptBlock block : scriptPlan.getBlocks()) {
            for (Script script : block.getScripts()) {
                script.setDisplaying(false);
                script.setDisplayed(false);
                scriptDao.save(script);
            }
        }
        scriptPlanDao.save(scriptPlan);
        scriptPlanMessenger.notifyDisplayResets(scriptPlan);
    }

    @Override
    public void completeScriptPlanExhibition(Long scriptPlanId) throws IllegalOperationException {
        ScriptPlan scriptPlan = scriptPlanDao.get(scriptPlanId);
        scriptPlan.setDisplayed(true);
        for (ScriptBlock block : scriptPlan.getBlocks()) {
            for (Script script : block.getScripts()) {
                script.setEditingUser(null);
                if (script.isDisplaying()) {
                    script.setDisplaying(false);
                    script.setDisplayed(true);
                    scriptDao.save(script);
                    break;
                }
            }
        }
        scriptPlanDao.save(scriptPlan);
        scriptPlanMessenger.notifyCompleteDisplay(scriptPlan);
    }

	@Override
	public void lockScriptPlan(Long planId) {
		ScriptPlan plan = scriptPlanDao.get(planId);
		scriptPlanMessenger.notifyLock(plan);
	}

	@Override
	public void unlockScriptPlan(Long planId) {
		ScriptPlan plan = scriptPlanDao.get(planId);
		scriptPlanMessenger.notifyUnlock(plan);
	}

    @Override
    public void synchronizeStatusVideosWS(Long scriptPlanId) throws RemoteException, ServiceException, WebServiceException {
        ScriptPlan scriptPlan = scriptPlanDao.get(scriptPlanId);
        for (ScriptBlock block : scriptPlan.getBlocks()) {
            for (Script script : block.getScripts()) {
                if(script.verifyHasVideos()) {
                    for(ScriptVideo scriptVideo : script.scriptVideosOfType(WSMedia.class)) {
                        scriptService.loadAndUpdateScriptVideo(script.getId(), scriptVideo);
                    }
                }
            }
        }
    }

    private void dispatchEvent(ScriptEvent scriptEvent) {
        publisher.publishEvent(scriptEvent);
    }

    private void dispatchEvent(ScriptPlanEvent scriptPlanEvent) {
        publisher.publishEvent(scriptPlanEvent);
    }

    //--------------------------------------------------------------------------
    //	Getters & Setters
    //--------------------------------------------------------------------------

    public ScriptPlanDao getScriptPlanDao() {
        return scriptPlanDao;
    }

    public void setScriptPlanDao(ScriptPlanDao scriptPlanDao) {
        this.scriptPlanDao = scriptPlanDao;
    }

    public ProgramDao getProgramDao() {
        return programDao;
    }

    public void setProgramDao(ProgramDao programDao) {
        this.programDao = programDao;
    }

    public ScriptBlockDao getScriptBlockDao() {
        return scriptBlockDao;
    }

    public void setScriptBlockDao(ScriptBlockDao scriptBlockDao) {
        this.scriptBlockDao = scriptBlockDao;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public ScriptPlanMessenger getScriptPlanMessenger() {
        return scriptPlanMessenger;
    }

    public void setScriptPlanMessenger(ScriptPlanMessenger scriptPlanMessenger) {
        this.scriptPlanMessenger = scriptPlanMessenger;
    }

    public SessionManager getSessionManager() {
        return sessionManager;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public ScriptDao getScriptDao() {
        return scriptDao;
    }

    public void setScriptService(ScriptService scriptService) {
        this.scriptService = scriptService;
    }

    public void setScriptDao(ScriptDao scriptDao) {
        this.scriptDao = scriptDao;
    }

    public void setScriptMessenger(ScriptMessenger scriptMessenger) {
        this.scriptMessenger = scriptMessenger;
    }

    public void setIntelligentInterface(IntelligentInterface intelligentInterface) {
        this.intelligentInterface = intelligentInterface;
    }
}
