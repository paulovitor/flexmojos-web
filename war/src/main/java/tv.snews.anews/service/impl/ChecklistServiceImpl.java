package tv.snews.anews.service.impl;

import tv.snews.anews.dao.ChecklistDao;
import tv.snews.anews.dao.ChecklistFullTextDao;
import tv.snews.anews.dao.ChecklistTaskDao;
import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.ChecklistMessenger;
import tv.snews.anews.search.ChecklistParams;
import tv.snews.anews.service.ChecklistService;
import tv.snews.anews.util.ResourceBundle;

import java.util.*;

import static tv.snews.anews.util.EntityUtil.notValidId;

/**
 * @since 1.5
 */
public class ChecklistServiceImpl implements ChecklistService {

	private final EntityLocker<Checklist> locker = EntityLockerFactory.getLocker(Checklist.class);

	private ChecklistDao checklistDao;
    private ChecklistTaskDao checklistTaskDao;
	private ChecklistFullTextDao checklistFTDao;
	private GuidelineDao guidelineDao;

	private ChecklistMessenger checklistMessenger;
	private SessionManager sessionManager;

	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@Override
	public Checklist loadById(Long id) {
		return checklistDao.get(id);
	}
	
	/**
	 * O fluxo de criação de produções direto pelo formulário da pauta não necessita LOCK ao salvar
	 * porque toda vez que salva a pauta é realizado um unLock na produção.
	 */
	@Override
	public synchronized Checklist save(Checklist current) throws IllegalOperationException {
		return save(current, true);
	}

	@Override
	public Checklist save(Checklist checklist, boolean lockOnInsert) throws IllegalOperationException {
		updateAuthorshipInfo(checklist);

		if (notValidId(checklist.getId())) {
			checklist.updateFields(); // copia dados da pauta
			checklistDao.save(checklist);

			if (lockOnInsert) {
				locker.lock(checklist, checklist.getAuthor());
				checklist.setEditingUser(checklist.getAuthor());
			}

			checklistMessenger.notifyInsert(checklist);
		} else {
			Checklist dbChecklist = checklistDao.get(checklist.getId());
			checklistDao.evict(dbChecklist);

			dbChecklist.updateFields(checklist);

			checklist = checklistDao.merge(dbChecklist);
			checklist.updateFields();

			checklistMessenger.notifyUpdate(checklist);
		}

		return checklist;
	}

	/**
	 * O fluxo de criação de produções direto pelo formulário da pauta não necessita LOCK ao salvar
	 * porque toda vez que salva a pauta é realizado um unLock na produção.
	 */
//	public synchronized Checklist save(Checklist current, Boolean allowLockChecklistOnSave) throws IllegalOperationException {
//		updateAuthorshipInfo(current);
//
//		// Faz uma validação para ver se a pauta informada já não possui um produção.
//		if (notValidId(current.getId()) && current.getGuideline() != null) {
//			// Se a pauta já possuir produção recupera a produção já criada e somente atualiza as informações.
//			// Isso é para evitar criações simultâneas
//			Checklist checklist = checklistDao.findByGuideline(current.getGuideline());
//			if (checklist != null) {
//				checklistDao.evict(checklist);
//				checklist.updateFields(current);
//				current = checklist;
//			}
//		}
//
//		//Se possui uma pauta, sincroniza os dados que prescisam ser iguais
//		if(current.getGuideline() != null)
//			// Faz a busca direto no banco de dados para evitar dados desatualizados.
//			// Como o mapeamento é bidirecional de checklist para guideline nem sempre
//			// os programadores não estão configurando checklist.setGuideline(guideline) e
//			// guideline.checklist.setGuideline(guideline) logo, ocorre que uma das pontas pode estar desatualizada.
//			current.updateFields(guidelineDao.get(current.getGuideline().getId()));
//
//
//		if(notValidId(current.getId())) {
//			// Se realmente for uma nova produção salva-a e notifica geral
//			checklistDao.save(current);
//
//			if(allowLockChecklistOnSave) {
//				// Garante que se o notificador da checklist chegar primeiro que o do LOCK
//				// ainda sim já vai constar quem está editando.
//				current.setEditingUser(sessionManager.getCurrentUser());
//				locker.lock(current, sessionManager.getCurrentUser());
//			}
//
//			checklistMessenger.notifyInsert(current);
//		} else {
//            /**
//             * Se houver tasks que foram deletadas.
//             */
//            Checklist checklistOld = checklistDao.get(current.getId());
//            checklistDao.evict(checklistOld);
//
//            for (ChecklistTask checklistTaskOld : checklistOld.getTasks()) {
//                boolean find = false;
//                for (ChecklistTask checklistTaskCurrent : current.getTasks()) {
//                    if(checklistTaskOld.equals(checklistTaskCurrent)) {
//                        find = true;
//                    }
//                }
//                if(!find) {
//                    checklistTaskDao.delete(checklistTaskOld);
//                }
//            }
//
//            current = checklistDao.merge(current);
//            checklistMessenger.notifyUpdate(current);
//        }
//
//		return current;
//	}

	@Override
	public synchronized void remove(Long checklistId) throws ValidationException, SessionManagerException {
		Objects.requireNonNull(checklistId, bundle.getMessage("checklist.nullChecklist"));
		Checklist checklist = checklistDao.get(checklistId);

		updateAuthorshipInfo(checklist);
		checklist.setExcluded(true);

		checklistDao.update(checklist);
		checklistMessenger.notifyDelete(checklist);
	}

	@Override
	public PageResult<Checklist> findByParams(ChecklistParams params) {
		List<Checklist> results;
		int total;

		// Só usa Lucene se tiver texto nos parâmetros da busca
		if (params.hasText()) {
			total = checklistFTDao.countByParams(params);
			results = checklistFTDao.findByParams(params);
		} else {
			total = checklistDao.countByParams(params);
			results = checklistDao.findByParams(params);
		}

		return new PageResult<>(params.getPage(), total, results);
	}

	@Override
	public List<Checklist> findAllNotExcludedOfDate(Date date, boolean notDone) {
		List<Checklist> checklists = checklistDao.findAllNotExcludedOfDate(date, notDone);

		markLocks(checklists);

		return checklists;
	}

	@Override
	public Collection<ChecklistRevision> loadChangesHistory(Long id) {
		return checklistDao.get(id).changesHistory();
	}

	@Override
	public PageResult<Checklist> listExcludedChecklistPage(int pageNumber) {
		List<Checklist> checklists = checklistDao.listExcludedPage(pageNumber);
		return new PageResult<>(pageNumber, checklistDao.countExcluded(), checklists);
	}

	@Override
	public synchronized Checklist restoreExcluded(Long id, Date destinyDate) throws SessionManagerException, IllegalOperationException {
		Checklist checklist = checklistDao.get(id);
		if (checklist != null && !checklist.isExcluded()) { //Já restaurada.
			return null;
		}
        checklist.setDate(destinyDate);
        checklist.setExcluded(false);
        updateAuthorshipInfo(checklist);
        checklistDao.update(checklist);
        checklistMessenger.notifyRestore(checklist);
		return checklist;
	}

	@Override
	public User checkEditionLock(Long id) {
		Checklist checklist = checklistDao.get(id);
		return locker.findOwner(checklist);
	}

	@Override
	public synchronized void lockEdition(Long id) throws IllegalOperationException {
		locker.lock(checklistDao.get(id), sessionManager.getCurrentUser());
	}

	@Override
	public synchronized Checklist findChecklistByGuideline(Long idGuideline) {
		Guideline guideline = guidelineDao.get(idGuideline);
		Checklist checklist = checklistDao.findByGuideline(guideline);

		if (checklist != null) {
//			checklistDao.evict(checklist);
			checklist.setEditingUser(locker.findOwner(checklist));
		}
		return checklist;
	}

	@Override
	public void unlockEdition(Long id) throws IllegalOperationException {
		locker.unlock(checklistDao.get(id), sessionManager.getCurrentUser());
	}

	@Override
	public boolean checklistTasksChanged(Checklist checklistA, Checklist checklistB) {
		if (checklistA.getTasks().size() != checklistB.getTasks().size()) {
			return true;
		}

		Iterator<ChecklistTask> itA = checklistA.getTasks().iterator();
		Iterator<ChecklistTask> itB = checklistB.getTasks().iterator();

		while (itA.hasNext() || itB.hasNext()) {
			ChecklistTask taskA = itA.next();
			ChecklistTask taskB = itB.next();

			if (Objects.equals(taskA.isDone(), taskB.isDone())
					&& Objects.equals(taskA.getLabel(), taskB.getLabel())
					&& Objects.equals(taskA.getInfo(), taskB.getInfo())
					&& Objects.equals(taskA.getDate(), taskB.getDate())
					&& Objects.equals(taskA.getTime(), taskB.getTime())) {
				continue;
			}
			return true;
		}

		return false;
	}

	//--------------------------------------------------------------------------
	//	Helpers
	//--------------------------------------------------------------------------

	private void markLock(Checklist checklist) {
		checklist.setEditingUser(locker.findOwner(checklist));
	}

	private void markLocks(Collection<Checklist> checklists) {
		for (Checklist checklist : checklists) {
			markLock(checklist);
		}
	}

	private void updateAuthorshipInfo(Checklist checklist) throws SessionManagerException {
		checklist.setAuthor(sessionManager.getCurrentUser());
		checklist.setModified(new Date());
	}

	//--------------------------------------------------------------------------
	//	Setters
	//--------------------------------------------------------------------------

	public void setChecklistDao(ChecklistDao checklistDao) {
		this.checklistDao = checklistDao;
	}

    public void setChecklistTaskDao(ChecklistTaskDao checklistTaskDao) {
        this.checklistTaskDao = checklistTaskDao;
    }

	public void setChecklistFTDao(ChecklistFullTextDao checklistFTDao) {
		this.checklistFTDao = checklistFTDao;
	}

	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public void setChecklistMessenger(ChecklistMessenger value) {
		this.checklistMessenger = value;

		locker.removeAllEntityLockerListeners();
		locker.addEntityLockerListener(new EntityLockerListener<Checklist>() {
			@Override
			public void entityLocked(Checklist checklist, User user) {
				checklistMessenger.notifyLock(checklist, user);
			}

			@Override
			public void entityUnlocked(Checklist checklist, User user) {
				checklistMessenger.notifyUnlock(checklist, user);
			}
		});
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

}
