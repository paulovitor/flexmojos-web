package tv.snews.anews.service.impl;

import tv.snews.anews.dao.GroupDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.Group;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.GroupMessenger;
import tv.snews.anews.messenger.UserMessenger;
import tv.snews.anews.service.GroupService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class GroupServiceImpl implements GroupService {
	private UserDao userDao;
	private GroupDao groupDao;
	private GroupMessenger groupMessenger;
    private UserMessenger userMessenger;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	
	@Override
	public List<Group> listAll() {
		return groupDao.listAll();
	}
	
	@Override
	public void save(Group group) {
		if (group.getId() > 0) {
			groupDao.update(group);
			groupMessenger.notifyUpdate(group);
		} else {
			groupDao.save(group);
			groupMessenger.notifyInsert(group);
		}
        userMessenger.notifyUsersChanged();
	}

	@Override
	public void delete(Integer id) throws IllegalOperationException {
		Group group = groupDao.get(id);
		if (group != null) {
			if (checkGroupHasUsers(group)) {
				throw new IllegalOperationException(bundle.getMessage("group.groupHasUsersError"));
			} else {
				groupDao.delete(group);
				groupMessenger.notifyDelete(group);
			}
		}
	}

	public Group listGroupByName(String name) {
		return groupDao.listGroupByName(name);
	}

	/**
	 * Verifica se existe usuários vinculados ao grupo.
	 * 
     * @param group 
     * @return Se existe usuários naquele grupo.
     */
    private boolean checkGroupHasUsers(Group group) {
    	return !userDao.listUsersByGroupAndStatus(group).isEmpty();
    }

	//----------------------------------
	//	Setters
	//----------------------------------
	
	public void setGroupDao(GroupDao groupDao) {
		this.groupDao = groupDao;
	}
    
	/**
     * @param userDao the userDao to set
     */
    public void setUserDao(UserDao userDao) {
	    this.userDao = userDao;
    }
    
    public void setGroupMessenger(GroupMessenger groupMessenger) {
	    this.groupMessenger = groupMessenger;
    }

    public void setUserMessenger(UserMessenger userMessenger) {
        this.userMessenger = userMessenger;
    }
}
