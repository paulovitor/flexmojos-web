package tv.snews.anews.service.impl;

import org.springframework.security.access.prepost.PreAuthorize;
import tv.snews.anews.dao.TweetInfoDao;
import tv.snews.anews.dao.TwitterUserDao;
import tv.snews.anews.domain.TweetInfo;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.infra.social.TwitterException;
import tv.snews.anews.infra.social.TwitterIntegration;
import tv.snews.anews.messenger.TwitterMessenger;
import tv.snews.anews.service.TwitterService;

import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
public class TwitterServiceImpl implements TwitterService {
	
	private TwitterIntegration twitter;
	
	private TwitterUserDao twitterUserDao;
	private TweetInfoDao tweetInfoDao;
	
	private TwitterMessenger twitterMessenger;

	@Override
	public List<TwitterUser> listAllTwitterUser() {
		return twitterUserDao.listAll();
	}
	
	@Override
	public void refreshTweetsFromTwitterUsers() throws TwitterException {
		List<TwitterUser> twitterUsers = twitterUserDao.listAll();
		
		//Somente sincroniza tweets da API se existirem twitterUsers cadastrados.
		for (TwitterUser twitterUser : twitterUsers) {
    		refreshTwitterUserTweets(twitterUser);
		}
	}
	
	private void refreshTwitterUserTweets(TwitterUser twitterUser) throws TwitterException {
		boolean notifyClient = false;
		SortedSet<TweetInfo> newsTweets = twitter.getTweetsByTwitterUser(twitterUser);
		
		SortedSet<TweetInfo> oldTweets = twitterUser.getTweetsInfo();
		for (TweetInfo tweetInfo : newsTweets) {
			tweetInfo.setTwitterUser(twitterUser);
			if (oldTweets.add(tweetInfo)) {
				tweetInfoDao.save(tweetInfo);
				// Notifica o cliente que novos tweets foram adicionados.
				notifyClient = true;
			}
        }
		
		if (notifyClient) {
			twitterMessenger.notifyTweetsLoaded(twitterUser);
		}
	}
	
	@Override
	public TweetInfo findTweetInfoById(Long idEntity) {
		return tweetInfoDao.get(idEntity);
	}
	
    @Override
    public TwitterUser checkTwitterUser(String screenName) throws TwitterException, ValidationException {
	    return twitter.getTwitterUser(screenName);
    }
    
    @Override
    @PreAuthorize("isFullyAuthenticated()")
    public void save(TwitterUser twitterUser) throws TwitterException {
	    twitterUserDao.save(twitterUser);
	    doInitialLoad(twitterUser);
	    twitterMessenger.notifyTwitterUserInserted(twitterUser);
    }
    
    private void doInitialLoad(TwitterUser twitterUser) throws TwitterException {
    	twitterUser.setTweetsInfo(new TreeSet<TweetInfo>());
    	refreshTwitterUserTweets(twitterUser);
    }

    @Override
    @PreAuthorize("isFullyAuthenticated()")
    public void delete(TwitterUser twitterUser) {
		twitterUserDao.delete(twitterUser);
		twitterMessenger.notifyTwitterUserDeleted(twitterUser);
    }

    @Override
    public TwitterUser findTwitterUserByScreenName(String screenName) {
	    return twitterUserDao.findTwitterUserByScreenName(screenName);
    }

    @Override
    public List<TweetInfo> listTweetsInfoFromAllTweetUsers(int start, int count) {
	    return tweetInfoDao.listPage(start, count);
    }
    
    @Override
    public List<TweetInfo> listTweetsInfoFromTwitterUser(TwitterUser twitterUser, int start, int count) {
	    return tweetInfoDao.listPage(twitterUser, start, count);
    }
    
    @Override
	public int countTweetsFromTwitterUser(TwitterUser twitterUser) {
		return tweetInfoDao.countFromTwitterUser(twitterUser);
	}
    
    @Override
    public int countTweetInfoFromAllTwitterUsers() {
	    return tweetInfoDao.countAll();
    }

	/**
     * @param twitter the twitter to set
     */
    public void setTwitter(TwitterIntegration twitter) {
    	this.twitter = twitter;
    }

    /**
     * @param twitterUserDao the twitterUserDao to set
     */
    public void setTwitterUserDao(TwitterUserDao twitterUserDao) {
    	this.twitterUserDao = twitterUserDao;
    }
	
    /**
     * @param tweetInfoDao the tweetInfoDao to set
     */
    public void setTweetInfoDao(TweetInfoDao tweetInfoDao) {
    	this.tweetInfoDao = tweetInfoDao;
    }

    /**
     * @param twitterMessenger the twitterMessenger to set
     */
    public void setTwitterMessenger(TwitterMessenger twitterMessenger) {
	    this.twitterMessenger = twitterMessenger;
    }

}
