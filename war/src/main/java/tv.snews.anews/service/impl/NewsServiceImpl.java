package tv.snews.anews.service.impl;

import tv.snews.anews.dao.GuidelineDao;
import tv.snews.anews.dao.NewsDao;
import tv.snews.anews.dao.UserDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.EntityLocker;
import tv.snews.anews.infra.EntityLockerFactory;
import tv.snews.anews.infra.EntityLockerListener;
import tv.snews.anews.infra.SessionManager;
import tv.snews.anews.messenger.NewsMessenger;
import tv.snews.anews.search.NewsParams;
import tv.snews.anews.service.ContactService;
import tv.snews.anews.service.NewsService;
import tv.snews.anews.util.ResourceBundle;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 * @author Paulo Felipe
 * @author Samuel Guedes
 * @since 1.0.0
 */
public class NewsServiceImpl implements NewsService {

    private static final int FIRST_RESULT = 0;
    private static final int LIMIT_TWENTY = 20;
    private NewsDao newsDao;
    private GuidelineDao guidelineDao;
    private UserDao userDao;
    
	private ContactService contactService;

    private SessionManager sessionManager;
    private NewsMessenger messenger;

    private final EntityLocker<News> lockerNews = EntityLockerFactory.getLocker(News.class);;
    
    private ResourceBundle bundle = ResourceBundle.INSTANCE;

    @Override
    public News loadById(Long newsId) {
    	News news = newsDao.get(newsId);
    	if (news != null) {
    		news.setEditingUser(lockerNews.findOwner(news));
    	}
        return news;
    }

    @Override
    public List<News> listAll() {
    	return markLocks(newsDao.listAll());
    }

    @Override
    public List<News> listFirstTwenty() {
        return newsDao.listNewsOlderThan(FIRST_RESULT, LIMIT_TWENTY);
    }

    @Override
    public PageResult<News> listAllPage(int pageNumber) {
        int total = newsDao.totalByNews();
        List<News> news = newsDao.listAllPage(pageNumber);
        markLocks(news);
        return new PageResult<News>(pageNumber, News.RESULTS_BY_PAGE, total, news);
    }

    @Override
    public PageResult<News> findByParams(NewsParams params) {
        int total = newsDao.totalByParams(params);
        List<News> news = newsDao.findByParams(params);
        markLocks(news);
        return new PageResult<News>(params.getPage(), News.RESULTS_BY_PAGE, total, news);
    }

    @Override
    public News get(Serializable idNews) {
    	News news = newsDao.get(idNews);
    	if (news != null) {
    		news.setEditingUser(lockerNews.findOwner(news));
    	}
        return news;
    }

    @Override
    public void delete(News news) throws ValidationException {
        if (checkNewsInUse(news)) {
            String errorMessage = bundle.getMessage("guideline.existStory");
            throw new ValidationException(errorMessage);
        }

        // Apaga os contatos específicos desta notícia
        for (Iterator<Contact> it = news.getContacts().iterator(); it.hasNext(); ) {
            Contact contact = it.next();
            if (!(contact instanceof AgendaContact)) {
                it.remove();
                contactService.delete(contact);
            }
        }

        newsDao.delete(news);
        messenger.notifyNewsChanged(news, EntityAction.DELETED);
    }

    @Override
    public void save(News news) throws SessionManagerException, ValidationException, IllegalOperationException {
        Objects.requireNonNull(news, bundle.getMessage("news.msg.nullNews"));

        news.setUser(sessionManager.getCurrentUser());
        news.setDate(new Date());

        if (news.getId() > 0) {
            News oldNews = newsDao.get(news.getId());
            newsDao.evict(oldNews);

            // Verifica se algum contato específico desta notícia foi apagado
            for (Iterator<Contact> it = oldNews.getContacts().iterator(); it.hasNext(); ) {
                Contact oldContact = it.next();
                if (!(oldContact instanceof AgendaContact)) {
                    if (!news.containsContact(oldContact)) {
                        it.remove();

                        // Comentei para deixar o contato na agenda
//						contactService.delete(oldContact);
                    }
                }
            }

            newsDao.update(news);
            messenger.notifyNewsChanged(news, EntityAction.UPDATED);
        } else {
            newsDao.save(news);
            messenger.notifyNewsChanged(news, EntityAction.INSERTED);
        }
    }

    @Override
	public void lockEdition(Long newsId, Integer userId) throws IllegalOperationException {
		News news = newsDao.get(newsId);
		User user = userDao.get(userId);

		try {
			lockerNews.lock(news, user);
		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(bundle.getMessage("news.msg.newsLock"));
		}
	}
    
    @Override
	public void unlockEdition(Long newsId) throws IllegalOperationException {
		News news = newsDao.get(newsId);
		lockerNews.unlock(news, sessionManager.getCurrentUser());
	}
    
    @Override
	public User isLockedForEdition(Long newsId) {
		News news = newsDao.get(newsId);	
		return lockerNews.findOwner(news);
	}
    
    
	private List<News> markLocks(List<News> listNews) {
		for (News news : listNews) {
			markLock(news);
		}
		return listNews;
	}
	 	
	private News markLock(News news) {
		news.setEditingUser(lockerNews.findOwner(news));
		return news;
	}

    //----------------------------------
    //	Setters
    //----------------------------------

    private boolean checkNewsInUse(News news) {
        boolean toReturn = false;

        List<Guideline> guidelines = guidelineDao.findByNews(news);

        if (guidelines != null) {
            if (guidelines.size() > 0) toReturn = true;
        }

        return toReturn;
    }

    public void setNewsDao(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    public void setGuidelineDao(GuidelineDao guidelineDao) {
        this.guidelineDao = guidelineDao;
    }

    public void setContactService(ContactService contactService) {
        this.contactService = contactService;
    }

    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    public void setUserDao(UserDao userDao) {
    	this.userDao = userDao;
    }
    
    public void setMessenger(NewsMessenger value) {
        this.messenger = value;
        
		lockerNews.removeAllEntityLockerListeners();
		lockerNews.addEntityLockerListener(new EntityLockerListener<News>() {

			@Override
			public void entityLocked(News news, User user) {
				messenger.notifyLock(news, user);
			}

			@Override
			public void entityUnlocked(News news, User user) {
				messenger.notifyUnlock(news, user);
			}
		});
    }
}
