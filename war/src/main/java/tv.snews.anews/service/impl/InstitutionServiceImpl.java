package tv.snews.anews.service.impl;

import tv.snews.anews.dao.InstitutionDao;
import tv.snews.anews.dao.RoundDao;
import tv.snews.anews.domain.Institution;
import tv.snews.anews.domain.Segment;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.messenger.InstitutionMessenger;
import tv.snews.anews.service.InstitutionService;
import tv.snews.anews.util.ResourceBundle;

import java.util.List;

/**
 * @author Paulo Felipe
 * @since 1.0.0
 */
public class InstitutionServiceImpl implements InstitutionService {

	private InstitutionDao institutionDao;
	private InstitutionMessenger institutionMessenger;
	
	private RoundDao roundDao;
	private ResourceBundle bundle = ResourceBundle.INSTANCE;

	@Override
	public void save(Institution institution) {
		if (institution.getId() > 0) {
			institutionDao.update(institution);
			institutionMessenger.notifyUpdate(institution);
		} else {
			institutionDao.save(institution);
			institutionMessenger.notifyInsert(institution);
		}
	}

	@Override
	public void delete(Integer id) throws IllegalOperationException {
		Institution institution = institutionDao.get(id);
		if (roundDao.countByInstitution(institution) == 0) {
			institutionDao.delete(institution);
			institutionMessenger.notifyDelete(institution);
		} else {
			throw new IllegalOperationException(bundle.getMessage("institution.msg.canNotBeDeleted"));
		}
	}

	@Override
	public List<Institution> listAll() {
		return institutionDao.listAll();
	}

	@Override
	public List<Institution> listBySegment(Segment segment) {
		return institutionDao.listBySegment(segment);
	}

	//----------------------------------
	//	Setters
	//----------------------------------

	public void setInstitutionDao(InstitutionDao institutionDao) {
		this.institutionDao = institutionDao;
	}

	public void setInstitutionMessenger(InstitutionMessenger institutionMessenger) {
		this.institutionMessenger = institutionMessenger;
	}

    public void setRoundDao(RoundDao roundDao) {
	    this.roundDao = roundDao;
    }
}
