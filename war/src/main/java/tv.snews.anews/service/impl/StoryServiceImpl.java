package tv.snews.anews.service.impl;

import net.sf.jasperreports.engine.util.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import tv.snews.anews.dao.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.StoryEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.*;
import tv.snews.anews.messenger.StoryMessenger;
import tv.snews.anews.service.*;
import tv.snews.anews.util.DateTimeUtil;
import tv.snews.anews.util.ResourceBundle;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.*;

import static org.apache.commons.lang.StringUtils.isBlank;
import static tv.snews.anews.domain.StoryAction.*;

/**
 * Implementação do serviço de manipulação de storys.
 * 
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class StoryServiceImpl implements StoryService, ApplicationEventPublisherAware {

	private static final Logger log = LoggerFactory.getLogger(StoryServiceImpl.class);

	private static final String DEFAULTS_REQUIRED_FIELDS = "defaults.requiredFields";

	private final EntityLocker<Story> lockerStory;
	private final EntityLocker<Reportage> lockerReportage;
	private final EntityLocker<Guideline> lockerGuideline;

	private StoryDao storyDao;
	private StoryFullTextDao storyFTSDao;
	private StoryCopyLogDao storyCopyLogDao;

	private StoryMosCreditDao storyMosCreditDao;

	private MosMediaDao mosMediaDao;

	private BlockDao blockDao;
	private RundownDao rundownDao;
	private UserDao userDao;

	private GuidelineDao guidelineDao;
	private GuidelineService guidelineService;
	private StoryKindDao storyKindDao;

	private ReportageDao reportageDao;
	private ReportageService reportageService;
	private ProgramDao programDao;

	private NewsDao newsDao;
	
	private ResourceBundle bundle = ResourceBundle.INSTANCE;
	private SessionManager sessionManager;

	private StoryMessenger storyMessenger;

	private StoryKindService storyKindService;
	private MediaCenterService mediaCenterService;

//	private CCStudioInterface ccStudio;

	// Despacha eventos
	private ApplicationEventPublisher publisher;

	public StoryServiceImpl() {
		lockerStory = EntityLockerFactory.getLocker(Story.class);
		lockerReportage = EntityLockerFactory.getLocker(Reportage.class);
		lockerGuideline = EntityLockerFactory.getLocker(Guideline.class);
	}

	@Override
	public Story loadById(Long id) {
		Story story = storyDao.get(id, true);
		prepareStory(story);
		story.setEditingUser(lockerStory.findOwner(story));
		return story;
	}

	@Override
	public List<StorySubSection> loadVideosByStoryId(Long id) {
		Story story = storyDao.get(id);

		List<StorySubSection> videos = new ArrayList<>();

		if (story.getNcSection() != null) {
			videos.addAll(story.getNcSection().subSectionsOfType(StoryMosVideo.class));
			videos.addAll(story.getNcSection().subSectionsOfType(StoryWSVideo.class));
		}

		if (story.getVtSection() != null) {
			videos.addAll(story.getVtSection().subSectionsOfType(StoryMosVideo.class));
			videos.addAll(story.getVtSection().subSectionsOfType(StoryWSVideo.class));
		}
		return videos;
	}

	@Override
	public void removeVideoByStoryMosObjById(Long idStory, StoryMosVideo storyMosVideo) {
		Story story = storyDao.get(idStory);
		if (story.getNcSection() != null) {
			story.getNcSection().getSubSections().remove(storyMosVideo);
		}
		if (story.getVtSection() != null) {
			story.getVtSection().getSubSections().remove(storyMosVideo);
		}
		storyDao.save(story);
		storyDao.update(story);
	}

	/**
	 * Carrega as seções e sub seções da lauda e calcula tempos.
	 */
	private void prepareStory(Story story) {
		story.loadLazies();
		story.calculateTotal();
	}

	@Override
	public void createStory(long blockId, int index) throws ValidationException, SessionManagerException {
		Story story = new Story();

		createStory(blockId, story);
		setOrder(index, story);

		saveStory(story);
	}

	@Override
	public void createStoryFromNews(long blockId, int index, long newsId) throws ValidationException, SessionManagerException {
		News news = newsDao.get(newsId);
		Story story = new Story(news);
		createStory(blockId, story);
		changeOrderOfStoryOnTheBlock(index, story, blockId);
		saveStory(story);
	}
	
	@Override
	public void createStoryFromReportage(long blockId, int index, long reportageId) throws ValidationException, SessionManagerException {
		Reportage reportage = reportageDao.get(reportageId);
		Story story = new Story(reportage);

		createStory(blockId, story);

		reportageService.registerProgram(reportage, story.getProgram());

		changeOrderOfStoryOnTheBlock(index, story, blockId);
		saveStory(story);
	}

	@Override
	public void createStoryFromGuideline(long blockId, int index, long guidelineId) throws ValidationException, SessionManagerException {
		Guideline guideline = guidelineDao.get(guidelineId);
		Story story = new Story(guideline);

		createStory(blockId, story);

		guidelineService.registerProgram(guideline, story.getProgram());
		changeOrderOfStoryOnTheBlock(index, story, blockId);

		saveStory(story);
	}

	private void saveStory(Story story) throws ValidationException, SessionManagerException {
		save(story);
		StoryEvent storyEvent = new StoryEvent(INSERTED, story, this);
		dispatchEvent(storyEvent);
	}

	private void createStory(long blockId, Story story) throws ValidationException, SessionManagerException {
		Block block = blockDao.get(blockId);

		validateFields(block);

		story.setPage(getNextPage(block.getRundown()));
		story.setExpected(DateTimeUtil.getMidnightTime());
		story.setOk(false);
		story.setBlock(block);
		story.setHead(DateTimeUtil.getMidnightTime());
		story.setVt(DateTimeUtil.getMidnightTime());
		story.setOff(DateTimeUtil.getMidnightTime());
		story.setTotal(DateTimeUtil.getMidnightTime());
		story.setEditor(null);
		story.setApproved(false);
		story.setExcluded(false);
		story.setStoryKind(storyKindDao.getDefaultStoryKind());
		story.setState(DocumentState.PRODUCING);
		story.getPrograms().add(block.getRundown().getProgram());

		// Atribui o apresentador padrão para as cabeças que podem ter sido criadas (da reportagem)
		User presenter = story.getProgram().getDefaultPresenter();
		if (presenter == null) {
			presenter = story.getProgram().getPresenters().iterator().next();
		}
		for (StoryCameraText camText : story.subSectionsOfType(StoryCameraText.class)) {
			camText.setPresenter(presenter);
		}
		
		for (StoryText storyText : story.subSectionsOfType(StoryText.class)) {
			storyText.setPresenter(presenter);
			storyText.calculateReadTime();
		}
	}

	private void changeOrderOfStoryOnTheBlock(int index, Story story, long blockId) {
		Block block = blockDao.loadById(blockId);
		story.setBlock(block);
		setOrder(index, story);

		//Organize the order of stories under the story was inserted.
		synchronized (this) {
			block.getStories().add(index, story);

			for (int i = index; i < block.getStories().size(); i++) {
				Story storyOfBlock = block.getStories().get(i);
				storyOfBlock.setOrder(i);
			}
		}
	}

	private void setOrder(int index, Story story) {
		if (index < 0 || index > story.getBlock().getStories().size()) {
			index = story.getBlock().getStories().isEmpty() ? 0 : story.getBlock().getStories().size();
		}
		story.setOrder(index);
	}

	@Override
	public synchronized String copyDrawerStoryToBlock(Long storyId, Long blockId, int index) throws CloneNotSupportedException {
		Story story = storyDao.get(storyId);
		Block block = blockDao.get(blockId);
		Rundown rundown = block.getRundown();
		Program program = rundown.getProgram();

		Story copy = story.clone();

		copy.setSourceStory(story); // link para a lauda original
		copy.setBlock(block);
		copy.setApproved(false);
		copy.setOk(false);
		copy.setPage(getNextPage(rundown));
		setOrder(index, copy);
        copy.getPrograms().clear();
        copy.getPrograms().add(program);

		// Re-ordena as outras laudas
		changeOrderOfStoryOnTheBlock(index, copy, blockId);

		// verifica programa antigo
		if (story.getProgram() != null) {
			Program storyProgram = story.getProgram();

			if (!storyProgram.equals(program)) {
				changePresenters(copy, program);
			}

			// Se não for o mesmo dispositívo de GC, desativa os créditos
			if (!ObjectUtils.equals(storyProgram.getCgDevice(), program.getCgDevice())) {
				disableCredits(copy);
			}
		}
		if (copy.hasVideos()) {
			verifyExistMosMedia(copy.subSectionsOfType(StoryMosVideo.class));
			verifyExistWSMedia(copy.subSectionsOfType(StoryWSVideo.class));
		}

		if (copy.isVideosAdded()) {
			verifyExistMosMedia(copy.subSectionsOfType(StoryMosVideo.class));
			verifyExistWSMedia(copy.subSectionsOfType(StoryWSVideo.class));
		}

		// Registra na lauda da gaveta o programa para qual a cópia foi criada
		story.getPrograms().add(program);

		storyDao.save(copy, sessionManager.getCurrentUser());
		storyDao.update(story);
		storyDao.flush();

		storyMessenger.notifyInsert(copy, getRundownIdFromStory(copy));
		storyMessenger.notifyUpdate(story, getRundownIdFromStory(copy));

		return OK;
	}

	@Override
	public synchronized void moveStory(long blockId, int dropIndex, long storyId) throws ValidationException {
		Story story = storyDao.get(storyId);
		Block blockSource = story.getBlock();
		Block blockTarget = blockDao.get(blockId);

		int oldIndex = story.getOrder();

		boolean blockChanged = false;
		if (blockSource.equals(blockTarget)) {
			if (story.getOrder() < dropIndex) {
				dropIndex--;
			}
		} else {
			blockChanged = true;
		}

		//Valida os Campos
		validateFields(story);
		validateFields(blockTarget);

		//Change the order of the story
		story.setBlock(blockTarget);
		story.setOrder(dropIndex);
        story.getPrograms().clear();
        story.getPrograms().add(blockTarget.getRundown().getProgram());
		storyDao.save(story);

		//Put it on the correct index.
		blockSource.getStories().remove(story);
		blockTarget.getStories().add(dropIndex, story);

		//Organize indexes
		sortBlock(blockTarget);

		StoryEvent blockEvent = null;
		if (blockChanged) {
			sortBlock(blockSource);

			blockEvent = new StoryEvent(BLOCK_CHANGED, this);
			blockEvent.registerChange(story, blockSource.getOrder(), blockTarget.getOrder());
		}

		storyDao.flush();

		// Tem que disparar mesmo que já tenha disparado o BLOCK_CHANGED (para o MOS)
		StoryEvent orderEvent = new StoryEvent(ORDER_CHANGED, this);
		orderEvent.registerChange(story, oldIndex, story.getOrder());

		// Somente após o flush se pode disparar os eventos
		dispatchEvent(blockEvent);
		dispatchEvent(orderEvent);

		storyMessenger.notifyMove(story, getRundownIdFromStory(story));
	}

	@Override
	public void moveStories(Long[] storiesIds, Long blockId, int dropIndex) {
		// Carrega as stories
		List<Story> stories = storyDao.listById(storiesIds);

		for (Story story : stories) {
			if (story.isExcluded()) {
				continue;
			}
			story.setEditingUser(lockerStory.findOwner(story));
		}

		// Ordena as stories para que não decremente o dropIndex incorretamente
		Collections.sort(stories);

		Block targetBlock = blockDao.get(blockId), sourceBlock;

		Set<Block> reorder = new HashSet<>();
		reorder.add(targetBlock);

		StoryEvent blockEvent = new StoryEvent(BLOCK_CHANGED, this);
		StoryEvent disapproveEvent = new StoryEvent(DISAPPROVED, this);
		StoryEvent orderEvent = new StoryEvent(ORDER_CHANGED, this);

		for (Story story : stories) {
			if (!story.isDisplayed() && !story.isDisplaying() && !story.getBlock().getRundown().isDisplayed()) {
				sourceBlock = story.getBlock();
				reorder.add(sourceBlock);

				StoryEvent storyEvent = null;
				//ATTENTION, this code is just called if has stories from different rundowns
				//If moving from another rundown, notify that this story was deleted
				if (!(targetBlock.getRundown().getId() == sourceBlock.getRundown().getId())) {
					//TODO We should insert this information on log of rundown
					storyMessenger.notifyDelete(story, story.getBlock().getRundown().getId()); //Notify old rundown story was deleted.
					story.setPage(nextPage(targetBlock.getRundown()));

					if (story.isApproved()) {
						//TODO We should insert this information on log of rundown.
						story.setApproved(false); //Instead of notify insert, just let user approve again when come from another rundown.
//						//TODO Verificar o log do sistema
						disapproveEvent.registerChange(story);
					}
				}

				Program sourceProgram = story.getProgram();
				Program targetProgram = targetBlock.getRundown().getProgram();

                story.getPrograms().clear();
                story.getPrograms().add(targetProgram);

				if (sourceProgram != null) {
					if (!sourceProgram.equals(targetProgram)) {
						changePresenters(story, targetProgram);
					}

					// Se não for o mesmo dispositívo de GC, desativa os créditos
					if (!ObjectUtils.equals(sourceProgram.getCgDevice(), targetProgram.getCgDevice())) {
						disableCredits(story);
					}
				}

				int oldIndex = story.getOrder();

				boolean blockChanged = false;
				if (sourceBlock.getId() == targetBlock.getId()) {
					if (story.getOrder() < dropIndex) {
						dropIndex--;
					}
				} else {
					blockChanged = true;
				}

				//Valida os Campos
				validateFields(story);
				validateFields(targetBlock);

				//Change the order of the story
				story.setBlock(targetBlock);
				story.setOrder(dropIndex);
				storyDao.save(story);

				//Put it on the correct index.
				sourceBlock.getStories().remove(story);
				targetBlock.getStories().add(dropIndex, story);
				
				dropIndex++;

				if (blockChanged) {
					// Registra no evento a mudança de bloco desta lauda
					blockEvent.registerChange(story, sourceBlock.getOrder(), targetBlock.getOrder());
				}

				//Os indices sempre precisam estar ordenados antes de enviar via MOS
				sortBlock(targetBlock);
				if (!(targetBlock.getId() == sourceBlock.getId())) {
					sortBlock(sourceBlock);
				}
				
				orderEvent.registerChange(story, oldIndex, story.getOrder());
			}
		}
		
		storyDao.flush();

		// Dispara os eventos após o flush() ocorrer com sucesso
		dispatchEvent(disapproveEvent);
		dispatchEvent(blockEvent);
		dispatchEvent(orderEvent);

		// Notificar move de todas as stories
		storyMessenger.notifyMove(stories, getRundownIdFromStories(stories));
	}

	@Override
	public void updateField(long storyId, String field, Object object) throws ValidationException, SessionManagerException {
		Story story = storyDao.get(storyId);
		Story.Fields storyField = Story.Fields.valueOf(field.toUpperCase());

		// Editor pode ser nulo
		if (storyField != Story.Fields.EDITOR && storyField != Story.Fields.IMAGE_EDITOR && storyField != Story.Fields.REPORTER) {
			validateFields(story, field, object);
		}

		boolean pageChanged, slugChanged, vtChanged = false;

		StoryEvent storyEvent = null;
		/*
		 * ATENÇÃO!! Se for adicionar um novo campo neste switch, verifique se a
		 * mudança dele não precisa ser notificada via MOS.
		 */
		switch (storyField) {
			case PAGE:
				String oldPage = story.getPage();
				String newPage = (String) object;
				pageChanged = !oldPage.equals(newPage);

				story.setPage(newPage);

				if (pageChanged) {
					storyEvent = new StoryEvent(PAGE_CHANGED, this);
					storyEvent.registerChange(story, oldPage, newPage);
				}
				break;
			case SLUG:
				String oldSlug = story.getSlug();
				String newSlug = (String) object;
				slugChanged = !Objects.equals(oldSlug, newSlug);

				story.setSlug(newSlug);

				if (slugChanged) {
					storyEvent = new StoryEvent(SLUG_CHANGED, this);
					storyEvent.registerChange(story, oldSlug, newSlug);
				}
				break;
			case REPORTER:
				storyEvent = new StoryEvent(REPORTER_CHANGED, story, this);
				story.setReporter((User) object);
				break;
			case EDITOR:
				storyEvent = new StoryEvent(EDITOR_CHANGED, story, this);
				story.setEditor((User) object);
				break;
			case OK:
				story.setOk((Boolean) object);
				storyEvent = new StoryEvent(story.isOk() ? StoryAction.OK : NOT_OK, story, this);
				break;
			case EXPECTED:
				story.setExpected((Date) object);
				break;
			case VT:
				Date newVt = (Date) object;
				StoryVTSection vtSection = story.getVtSection();
				StoryNCSection ncSection = story.getNcSection();
				vtChanged = true;
				if (vtSection == null && ncSection == null) {
					story.setVt(newVt);
					story.setVtSection(new StoryVTSection(newVt));
					story.setVtManually(true);
				} else {
					if (vtSection != null) {
						Date oldVt = story.getVtSection().getDuration();
						vtChanged = !oldVt.equals(object);
						story.setVtManually(true);
						story.getVtSection().setDuration(newVt);
						story.setVt(newVt);

						storyEvent = new StoryEvent(VT_CHANGED, this);
						storyEvent.registerChange(story, oldVt, newVt);
					}
					if (ncSection != null) {
						Date oldVt = story.getNcSection().getDuration();
						vtChanged = !oldVt.equals(object);
						story.setVtManually(true);
						story.getNcSection().setDuration(newVt);
						story.setVt(newVt);

						storyEvent = new StoryEvent(VT_CHANGED, this);
						storyEvent.registerChange(story, oldVt, newVt);
					}

				}
				story.calculateTotal();
				break;
			case APPROVED:
				story.setApproved((Boolean) object);
				storyEvent = new StoryEvent(story.isApproved() ? APPROVED : DISAPPROVED, story, this);
				break;
			case DISPLAYING:
				Rundown rundown = story.getBlock().getRundown();
				for (Story current : storyDao.listDisplayingStories(rundown)) {
					if (!current.equals(story)) {
						current.setDisplayed(true);
						current.setDisplaying(false);

						storyDao.save(current);
						storyDao.flush();

						storyMessenger.notifyUpdateField(current.getId(), "displayed", true, rundown.getId());
					}
				}

				story.setDisplaying((Boolean) object);
                try {
                    unlockEdition(story.getId());
                } catch (IllegalOperationException ex) {
                    throw new RuntimeException(bundle.getMessage("story.unlockError"));
                }
				break;
			case IMAGE_EDITOR:
				storyEvent = new StoryEvent(IMAGE_EDITOR_CHANGED, story, this);
				story.setImageEditor((User) object);
				break;
			default:
				break;
		}

		Long rundownId = story.getBlock().getRundown().getId();

		storyDao.save(story);
		storyDao.flush();

		// Eventos e notificações somente após o flush()

		if (vtChanged) {
			storyMessenger.notifyUpdate(story, getRundownIdFromStory(story));
		} else {
			storyMessenger.notifyUpdateField(storyId, field, object, rundownId);
		}

		dispatchEvent(storyEvent);
	}

	@Override
	public void clearClipboard() {
		sessionManager.clipboardFlush();
	}

	@Override
	public Long[] sendStoriesToClipboard(ClipboardAction action, Long[] ids) {
		sessionManager.clipboardPush(action, ids);
		return sessionManager.clipboardPull();
	}

	@Override
	public void copyTo(Long[] storiesIds, Date date, Integer programId, boolean move) throws CloneNotSupportedException {
		Program program = programDao.get(programId);
		Rundown rundown = rundownDao.findByDateAndProgram(date, program, false);

		if (rundown == null) {
			throw new IllegalStateException();
		} else {
			Block standBy = rundown.findStandBy();
			if (move) {
				moveStories(storiesIds, standBy.getId(), standBy.getStories().size());
			} else {
				copy(storiesIds, standBy, standBy.getStories().size());
			}
		}
	}

	@Override
	public Long[] pastStoriesFromClipboard(Long blockId, int dropIndex) throws CloneNotSupportedException {
		Long[] ids = sessionManager.clipboardPull();

		if (ids.length > 0) {
			ClipboardAction action = sessionManager.clipboardAction();
			Block block = blockDao.get(blockId);

			if ((dropIndex == -1 && block != null) || (dropIndex > block.getStories().size())) {
				dropIndex = block.getStories().size();
			}

			if (action == ClipboardAction.COPY) {
				copy(ids, block, dropIndex);
			} else {
				moveStories(ids, blockId, dropIndex);
			}

			sessionManager.clipboardFlush();
		}

		return ids;
	}

	@Override
	public List<Story> listByDateAndProgram(Date date, Program program) {
		Rundown rundown = rundownDao.findByDateAndProgram(date, program, false);
		if (rundown == null) {
			return new ArrayList<>();
		} else {
			return listStoriesByRundown(rundown.getId());
		}
	}

	@Override
	public List<Story> listStoriesByRundown(Long rundownId) throws ValidationException {
		Rundown rundown = rundownDao.get(rundownId);
		if (rundown == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}

		return storyDao.listStoriesByRundown(rundown);
	}

	@Override
	public PageResult<Story> storiesByFullTextSearch(String slug, String text, Program program, Date initialDate, Date finalDate, int pageNumber,
	    SearchType searchType, String textIgnore) {
		int total = storyFTSDao.total(slug, text, program, initialDate, finalDate, searchType, textIgnore);
		List<Story> stories = storyFTSDao.listArchiveStories(slug, text, program, initialDate, finalDate, pageNumber, searchType, textIgnore);
		return new PageResult<>(pageNumber, total, stories);
	}

	@Override
	public List<Story> listStoriesByDateEditorOrderProgram(Date date, User editor) {
		List<Story> stories = storyDao.listByDateAndEditor(date, editor);

		for (Story story : stories) {
			story.setEditingUser(lockerStory.findOwner(story));

			Guideline guideline = story.getGuideline();
			if (guideline != null) {
				guideline.getRevisions().size(); //Força carregamento das revisões da guideline.

				guideline.setEditingUser(lockerGuideline.findOwner(guideline));
			}

			Reportage reportage = story.getReportage();
			if (reportage != null) {
				reportage.setEditingUser(lockerReportage.findOwner(reportage));
				reportage.setBlank(true);
				reportage.getRevisions().size();

				for (ReportageSection section : reportage.getSections()) {
					if (isBlank(section.getContent()) && isBlank(section.getObservation())) {
						reportage.setBlank(false);
						break;
					}
				}
			}

		}

		return stories;
	}

	@Override
	public Story merge(Story story) throws SessionManagerException {
		Objects.requireNonNull(story, bundle.getMessage("story.msg.invalidId"));
		Objects.requireNonNull(story.getId(), bundle.getMessage("story.msg.invalidId"));

		Story current = storyDao.get(story.getId(), true);
		current.getRevisions().size();
		storyDao.evict(current);

		//TODO #AN-769
//		List<Integer> mosMediaIds =	getCurrentMosMediaIds(current.subSectionsOfType(StoryMosCredit.class), 
//														  story.subSectionsOfType(StoryMosCredit.class));

		current.setExpected(story.getExpected());
		current.setTotal(story.getTotal());
		current.setHead(story.getHead());
		current.setVt(story.getVt());
		current.setOff(story.getOff());
		current.setSlug(story.getSlug());
		current.setStoryKind(story.getStoryKind());
		current.setHeadSection(story.getHeadSection());
		current.setNcSection(story.getNcSection());
		current.setVtSection(story.getVtSection());
		current.setFooterSection(story.getFooterSection());
		current.setOffSection(story.getOffSection());
		current.setInformation(story.getInformation());
		current.setTapes(story.getTapes());
		current.setVideosAdded(story.hasVideos());
		current.setVtManually(story.isVtManually());

		if (current.isVideosAdded()) {
			verifyExistMosMedia(current.subSectionsOfType(StoryMosVideo.class));
			verifyExistWSMedia(current.subSectionsOfType(StoryWSVideo.class));
		}

		current.calculateTotal();
		current = storyDao.merge(current, sessionManager.getCurrentUser());
		storyDao.flush();

		//TODO #AN-769
//		if (mosMediaIds != null && mosMediaIds.size() > 0) {
//			excludeNotUsedMosMedia(mosMediaIds);
//		}

		current.loadLazies();
		storyMessenger.notifyUpdate(current, getRundownIdFromStory(current));

		// Dispara o evento para o MOS
		dispatchEvent(new StoryEvent(UPDATED, current, this));

		return current;
	}

//	/**
//	 * Esse método retorna uma lista de IDs de MosMedias
//	 * para serem verificadas na base de dados a
//	 * necessidade de exclusão das mesmas.
//	 *
//	 * @param currentCGs lista de SubSections (CGs) já armazenada no banco
//	 * @param newCGs nova lista de SubSections (CGs) vinda do form.
//	 * @return
//	 */
//	private List<Integer> getCurrentMosMediaIds(List<StoryMosCredit> currentCGs, List<StoryMosCredit> newCGs) {
//		List<Integer> mosMediaIdsToVerify = new ArrayList<>();
//
//		for (StoryMosCredit storyMosCredit : newCGs) {
//			if (storyMosCredit.isEdited()) {
//				for (StoryMosCredit currentStoryMosCredit : currentCGs) {
//					if (currentStoryMosCredit.getId().equals(storyMosCredit.getId())) {
//						mosMediaIdsToVerify.add(currentStoryMosCredit.getMosMedia().getId());
//					}
//				}
//			}
//		}
//
//		return mosMediaIdsToVerify;
//	}

//	/**
//	 * Método que verifica cada mosMediaId se está sendo utilizada por
//	 * outra entidade, caso não esteja, será excluída.
//	 *
//	 * @param mosMediaIds
//	 */
//	private void excludeNotUsedMosMedia(List<Integer> mosMediaIds) {
//		for (Integer mosMediaId : mosMediaIds) {
//			if (mosMediaId > 0) {
//				List<StoryMosCredit> listStoriesMosCredit = storyMosCreditDao.getByMosMedia(mosMediaId);
//				Boolean hasMosMediaInUse = listStoriesMosCredit != null && listStoriesMosCredit.size() > 1;
//
//				if (!hasMosMediaInUse) {
//					MosMedia mosMediaToDelete = mosMediaDao.get(mosMediaId);
//					mosMediaDao.delete(mosMediaToDelete);
//					//TODO ReportageMosCredit
//				}
//			}
//		}
//	}

	private void verifyExistMosMedia(List<StoryMosVideo> videos) {
		for (StorySubSection video : videos) {
			StoryMosVideo storyMosVideo = (StoryMosVideo) video;
			if (storyMosVideo.getMosMedia() == null) {
				storyMosVideo.setMosMedia(null);
			} else {
				storyMosVideo.setMosMedia(mosMediaDao.get(storyMosVideo.getMosMedia().getId()));
			}
		}
	}

	private void verifyExistWSMedia(List<StoryWSVideo> videos) {
		for (StorySubSection video : videos) {
			StoryWSVideo storyWSVideo = (StoryWSVideo) video;
			if (storyWSVideo.getMedia() != null) {
				try {
					storyWSVideo.setMedia(mediaCenterService.loadWSMedia(storyWSVideo.getMedia()));
				} catch (Exception e) {
					log.debug(e.getMessage());
					mediaCenterService.deleteWSMedia(storyWSVideo.getMedia());
					storyWSVideo.setMedia(null);
				}
			}
		}
	}

	@Override
	public int countExcludedStories() {
		return storyDao.countExcludedStories();
	}

	@Override
	public PageResult<Story> listExcludedStories(int pageNumber) {
		List<Story> stories = storyDao.listExcludedStories(pageNumber);
		return new PageResult<>(pageNumber, storyDao.totalExcluded(), stories);
	}

	@Override
	public String restoreExcludedStory(Long storyId, Date date, int destinyProgramId) throws SessionManagerException {
		Program destinyProgram = programDao.get(destinyProgramId);
		Rundown destinyRundown = rundownDao.findByDateAndProgram(date, destinyProgram, false);

		if (destinyRundown == null) {
			return RUNDOWN_NOT_FOUND;
		} else {
			Story story = storyDao.get(storyId);

			story.setApproved(false);
			story.setOk(false);
			story.setExcluded(false);

			// verifica programa antigo
			if (story.getProgram() != null) {
				Program storyProgram = story.getProgram();

				if (!storyProgram.equals(destinyProgram)) {
					changePresenters(story, destinyProgram);
				}

				// Se não for o mesmo dispositívo de GC, desativa os créditos
				if (!ObjectUtils.equals(storyProgram.getCgDevice(), destinyProgram.getCgDevice())) {
					disableCredits(story);
				}
			}
			if (story.hasVideos()) {
				verifyExistMosMedia(story.subSectionsOfType(StoryMosVideo.class));
				verifyExistWSMedia(story.subSectionsOfType(StoryWSVideo.class));
			}

			// Ajusta o bloco e a ordem
			Block standBy = destinyRundown.findStandBy(); // não deve retornar null, se retornar tem algo errado!
			story.setBlock(standBy);
			story.setOrder(standBy.getStories().size());

			// Registra o uso do programa na lauda original da gaveta
			Story drawerOrigin = story.findDrawerOrigin();
			if (drawerOrigin != null) {
				drawerOrigin.getPrograms().add(story.getProgram());
				storyDao.update(drawerOrigin);

				storyMessenger.notifyUpdate(drawerOrigin, getRundownIdFromStory(story));
			}

			registerProgram(story, story.getProgram());
            story.getPrograms().add(story.getProgram());

			if (story.getBlock() != null) {
				Program storyProgram = story.getProgram();

				// Altera se necessário os apresentadores
				if (!storyProgram.equals(destinyProgram)) {
					changePresenters(story, destinyProgram);
				}

				// Se não for o mesmo dispositívo de GC, desativa os créditos
				if (!ObjectUtils.equals(storyProgram.getCgDevice(), destinyProgram.getCgDevice())) {
					disableCredits(story);
				}
			}

			if (story.isVideosAdded()) {
				verifyExistMosMedia(story.subSectionsOfType(StoryMosVideo.class));
				verifyExistWSMedia(story.subSectionsOfType(StoryWSVideo.class));
			}

			storyDao.update(story, sessionManager.getCurrentUser());
			storyDao.flush();

			storyMessenger.notifyRestore(story, getRundownIdFromStory(story));

			return OK;
		}
	}

	@Override
	public void save(Story story) throws ValidationException {
		validateFields(story);

		User author = sessionManager.getCurrentUser();

		if (story.getId() != null && story.getId() > 0) {
			storyDao.update(story, author);

			storyMessenger.notifyUpdate(story, getRundownIdFromStory(story));
		} else {
			story.setId((Long) storyDao.save(story, author));

			storyMessenger.notifyInsert(story, getRundownIdFromStory(story));
		}
		//storyDao.flush();
	}

	private void copy(Long[] storiesIds, Block targetBlock, int dropIndex) throws CloneNotSupportedException {
		// Carrega as stories
		List<Story> stories = storyDao.listById(storiesIds);
		
		for (Story story : stories) {
			if (story.isExcluded()) {
		 		continue;
		}
 			story.setEditingUser(lockerStory.findOwner(story));
		}

		// Ordena as stories para que não decremente o dropIndex incorretamente
		Collections.sort(stories);

		StoryEvent copyEvent = new StoryEvent(COPIED, this);

		if (!targetBlock.getRundown().isDisplayed()) {
			// Carrega as stories
			for (Story story : stories) {
				Program sourceProgram = story.getProgram();
				Program targetProgram = targetBlock.getRundown().getProgram();

				Story copy = story.clone();
				copy.setSourceStory(story); // link para a lauda original
				copy.setApproved(false);
				copy.setOk(false);

				// Define o bloco e ordem corretos
				copy.setBlock(targetBlock);
				copy.setOrder(dropIndex);
				copy.setPage(nextPage(targetBlock.getRundown()));
                copy.getPrograms().clear();
                copy.getPrograms().add(targetProgram);

				//Put it on the correct index.
				targetBlock.getStories().add(dropIndex, copy);
				dropIndex++;

				if (sourceProgram != null) {
					if (!sourceProgram.equals(targetProgram)) {
						changePresenters(copy, targetProgram);
					}

					// Se não for o mesmo dispositívo de GC, desativa os créditos
					if (!ObjectUtils.equals(sourceProgram.getCgDevice(), targetProgram.getCgDevice())) {
						disableCredits(copy);
					}
				}

				// Caso tenha uma origem da gaveta, registra o uso no programa de destino
				Story drawerOrigin = story.findDrawerOrigin();
				if (drawerOrigin != null) {
					drawerOrigin.getPrograms().add(targetProgram);
					storyDao.update(drawerOrigin);
					storyMessenger.notifyUpdate(drawerOrigin, getRundownIdFromStory(drawerOrigin));
				}

				registerProgram(copy, targetProgram);

				if (copy.isVideosAdded()) {
					verifyExistMosMedia(copy.subSectionsOfType(StoryMosVideo.class));
					verifyExistWSMedia(copy.subSectionsOfType(StoryWSVideo.class));
				}

				copy.setId((Long) storyDao.save(copy, sessionManager.getCurrentUser()));
				storyMessenger.notifyInsert(copy, getRundownIdFromStory(copy));

				copyEvent.registerChange(copy, sourceProgram.getName(), targetProgram.getName());
			}

			sortBlock(targetBlock);
			
			// Comentei pois não vi a finalidade deste segundo disparo do mesmo evento
//			for (Story story : stories) {
//				StoryEvent storyEvent = new StoryEvent_(COPIED, story, this);
//				//storyEvent.setOldValue(oldIndex);
//				//storyEvent.setNewValue(story.getOrder());
//				dispatchEvent(storyEvent);
//			}

			storyDao.flush();

			// Dispara os eventos somente após o flush()
			dispatchEvent(copyEvent);
		}
	}

	@Override
	public String copyRemoteStory(Story remoteStory, Date destinyDate, Program destinyProgram) {
		Rundown destinyRundown = rundownDao.findByDateAndProgram(destinyDate, destinyProgram, false);

		if (destinyRundown == null) {
			return RUNDOWN_NOT_FOUND;
		} else {
			User currentUser = sessionManager.getCurrentUser();
			User defaultPresenter = destinyProgram.getDefaultPresenter();

			try {
				Story copy = remoteStory.clone();
				copy.setApproved(false);
				copy.setOk(false);
				copy.setEditor(null);
                copy.setImageEditor(null);
				copy.setGuideline(null);
				copy.setReportage(null);
                copy.setAuthor(currentUser);
				copy.getRevisions().clear();
                copy.setReporter(null);
                copy.getPrograms().clear();
                copy.getDrawerPrograms().clear();
                copy.setChangeDate(new Date());

				// Define o bloco, ordem e página
				Block standBy = destinyRundown.findStandBy();
				copy.setBlock(standBy);
				copy.setOrder(standBy.getStories().size());
				copy.setPage(nextPage(destinyRundown));

				// Substitui o StoryKind por um idêntico da base local
				if (copy.getStoryKind() != null) {
					StoryKind replacement = storyKindService.findOrCreateReplacement(copy.getStoryKind());
					copy.setStoryKind(replacement);
				}

				// Builder utilizado para gerar a representação em texto dos GCs e Videos do MOS
				StringBuilder builder = new StringBuilder();
				if (copy.getInformation() != null) {
					builder.append(copy.getInformation()).append("\n\n");
				}
				if (copy.getTapes() != null) {
					builder.append(copy.getTapes()).append("\n\n");
				}

				// Transforma os créditos InteligentInterface em um texto para que possam ser removidos
				for (StoryCG cgII : copy.subSectionsOfType(StoryCG.class)) {
					builder.append(bundle.getMessage("story.cg")).append(' ');
					if (cgII.getCode() > 0) {
						builder.append(cgII.getCode()).append(' ');
					}

					if (!cgII.getFields().isEmpty()) {
						builder.append('(');

						for (Iterator<StoryCGField> it = cgII.getFields().iterator(); it.hasNext();) {
							StoryCGField fieldII = it.next();

							builder.append(fieldII.getName()).append(": \"").append(fieldII.getValue()).append('"');

							if (it.hasNext()) {
								builder.append(", ");
							}
						}
						builder.append(')');
					}
					builder.append('\n');
				}

				// Remove todos os créditos
				copy.removeSubSectionsOfType(StoryCG.class);

				// Transforma os créditos MOS em um texto para que possam ser removidos
				for (StoryMosCredit cgMos : copy.subSectionsOfType(StoryMosCredit.class)) {
					builder.append(bundle.getMessage("story.cg")).append(' ');
					if (StringUtils.isNotEmpty(cgMos.getSlug())) {
						builder.append(cgMos.getSlug()).append(' ');
					}

					if (!cgMos.getSlug().isEmpty()) {
						builder.append(cgMos.getSlug());
						}
					builder.append('\n');
				}

				// Remove todos os créditos
				copy.removeSubSectionsOfType(StoryMosCredit.class);

				// Transforma os vídeos do MOS em um texto para que possam ser removidos
				for (StoryMosVideo mosObj : copy.subSectionsOfType(StoryMosVideo.class)) {
					builder.append(bundle.getMessage("story.vtUpperCase") + " ");
					if (StringUtils.isNotBlank(mosObj.getSlug())) {
						builder.append(' ').append(mosObj.getSlug());
					}
					if (mosObj.getDurationTime() != null) {
						builder.append(" (").append(DateTimeUtil.dateToTimeFormat(mosObj.getDurationTime())).append(')');
					}
					builder.append('\n');
				}

				// Remove todos os vídeos do MOS
				copy.removeSubSectionsOfType(StoryMosVideo.class);

				// Transforma os vídeos do WS em um texto para que possam ser removidos
				for (StoryWSVideo wsVideo : copy.subSectionsOfType(StoryWSVideo.class)) {
					builder.append(bundle.getMessage("story.vtUpperCase") + " ");
					if (StringUtils.isNotBlank(wsVideo.getSlug())) {
						builder.append(wsVideo.getSlug());
					}
					if (StringUtils.isNotBlank(wsVideo.getSlug())) {
						builder.append(" (").append(DateTimeUtil.dateToTimeFormat(wsVideo.getDurationTime())).append(')');
					}
					builder.append('\n');
				}

				// Remove todos os vídeos do WS
				copy.removeSubSectionsOfType(StoryWSVideo.class);

				// Joga o resultado do builder nas informações da cópia
				copy.setInformation(builder.toString());

				// Altera os apresentadores de todos StoryTexts
				for (StoryText text : copy.subSectionsOfType(StoryText.class)) {
					text.setPresenter(defaultPresenter);
					text.calculateReadTime();
				}

				// Altera os apresentadores de todos StoryCameraTexts
				for (StoryCameraText text : copy.subSectionsOfType(StoryCameraText.class)) {
					text.setPresenter(defaultPresenter);
					text.calculateReadTime();
				}

				// Recalcula o tempo total da lauda
				copy.calculateTotal();

				copy.setId((Long) storyDao.save(copy, sessionManager.getCurrentUser()));
				storyDao.flush();

				storyMessenger.notifyInsert(copy, getRundownIdFromStory(copy));

				return OK;
			} catch (CloneNotSupportedException e) {
				throw new RuntimeException(e);
			}
		}
	}

	@Override
	public Story remoteCopyRequest(Long reportageId, String nickname, String company) {
		Story story = storyDao.get(reportageId, true);

		StoryCopyLog copyLog = new StoryCopyLog(story, nickname, company);
		story.getCopiesHistory().add(copyLog);

		storyCopyLogDao.save(copyLog);
		storyCopyLogDao.flush();

		storyMessenger.notifyUpdate(story, getRundownIdFromStory(story));

		return story;
	}

	@Override
	public void sendToDrawer(Story story) throws ValidationException {
		validateFields(story);

		if (story.getId() != null && story.getId() > 0) {

			// Retira o registro do programa na lauda original da gaveta
			Story drawerOrigin = story.findDrawerOrigin();
			if (drawerOrigin != null) {
				drawerOrigin.getPrograms().remove(story.getProgram());
				storyDao.update(drawerOrigin);

				storyMessenger.notifyUpdate(drawerOrigin, getRundownIdFromStory(drawerOrigin));
			}

			// Remove registro de programas
			unregisterProgram(story);
            story.getPrograms().remove(story.getProgram());

			Rundown rundown = story.getBlock().getRundown();

			story.setBlock(null);

			storyDao.update(story);
			storyDao.flush();

			// Dispare o evento antes que o bloco seja alterado para null
			dispatchEvent(new StoryEvent(ARCHIVED, story, rundown, this));

//          O notificador de documentos se encarregará de enviar a notificação
//			storyMessenger.notifyDrawerChange(story);
		}
	}

	@Override
	public void sendReportageToDrawer(Long reportageId, Integer programId) throws ValidationException {
		Reportage reportage = reportageDao.get(reportageId);
		Program program = programDao.get(programId);

		Story story = new Story(reportage);
		story.setProgram(program);
		story.setPage("0");
		story.setOrder(0);
		story.setExpected(DateTimeUtil.getMidnightTime());
		story.setOk(false);
		story.setBlock(null);
		story.setHead(DateTimeUtil.getMidnightTime());
		story.setVt(DateTimeUtil.getMidnightTime());
		story.setOff(DateTimeUtil.getMidnightTime());
		story.setTotal(DateTimeUtil.getMidnightTime());
		story.setDate(new Date());
		story.setEditor(null);
		story.setApproved(false);
		story.setExcluded(false);
		story.setStoryKind(storyKindDao.getDefaultStoryKind());

		correctCameraTextPresenters(story);

		storyDao.save(story, sessionManager.getCurrentUser());
		storyDao.flush();

		//storyMessenger.notifyDrawerChange(story);
	}

	@Override
	public Collection<StoryRevision> loadChangesHistory(Long storyId) {
		return storyDao.get(storyId).changesHistory();
	}

	@Override
	public StoryRevision loadRevision(Long storyId, int revisionNumber, boolean showDiff) {
		Story story = storyDao.get(storyId);
		StoryRevision current = story.currentRevision();
		StoryRevision revision = story.getRevisionByNumber(revisionNumber);

		if (showDiff && current.getRevisionNumber() != revisionNumber) {
			return current.computeDiff(revision);
		} else {
			return revision;
		}
	}

	@Override
	public void lockEdition(Long storyId, Integer userId) throws IllegalOperationException {
		Story story = storyDao.get(storyId);
		story.loadLazies();
		User user = userDao.get(userId);

		try {
			lockerStory.lock(story, user);
		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(bundle.getMessage("story.msg.storyLock"));
		}
		storyMessenger.notifyLock(story, user, getRundownIdFromStory(story));
	}

	@Override
	public void assumeEdition(Long storyId, Integer userId, Integer droppedUserId) throws IllegalOperationException {
		Story story = storyDao.get(storyId);
		//story.loadLazies();
		User user = userDao.get(userId);
        User droppedUser = userDao.get(droppedUserId);

		try {
            lockerStory.unlock(story, sessionManager.getCurrentUser());
			lockerStory.lock(story, user);

		} catch (IllegalOperationException e) {
			throw new IllegalOperationException(bundle.getMessage("story.msg.storyLock"));
		}
		storyMessenger.notifyLock(story, user, getRundownIdFromStory(story));
        StoryEvent storyEvent = new StoryEvent(EDITING_USER_CHANGED, this);
        storyEvent.registerChange(story, droppedUser.getName(), user.getName());
        dispatchEvent(storyEvent);
	}

	@Override
	public void unlockEdition(Long storyId) throws IllegalOperationException {
		Story story = storyDao.get(storyId);
		lockerStory.unlock(story, sessionManager.getCurrentUser());
		storyMessenger.notifyUnlock(story, getRundownIdFromStory(story));
	}

	@Override
	public User isLockedForEdition(Long storyId) {
		Story story = storyDao.get(storyId);
		return lockerStory.findOwner(story);
	}

	@Override
	public void lockDragging(Long storyId, Integer userId) throws IllegalOperationException {
		Story story = storyDao.get(storyId);
		User user = userDao.get(userId);
		storyMessenger.notifyLockDragging(story, user, getRundownIdFromStory(story));
	}

	@Override
	public void unlockDragging(Long storyId) {
		Story story = storyDao.get(storyId);
		storyMessenger.notifyUnlockDragging(story, getRundownIdFromStory(story));
	}

	@Override
	public void createStoryMosVideo(Long id, StoryMosVideo storyMosVideo) throws ValidationException, IOException, IllegalOperationException {
		MosMedia newMosMedia = mediaCenterService.mosObjCreate(storyMosVideo.getMosMedia());
		storyMosVideo.setMosMedia(newMosMedia);

		Story story = storyDao.get(id);

		if (story.getVtSection() == null) {
			story.setVtSection(new StoryVTSection());
		}
		story.getVtSection().addSubSection(storyMosVideo);
		story.setVideosAdded(story.hasVideos());
		save(story);
	}

	@Override
	public void createStoryWSVideo(Long id, StoryWSVideo storyWSVideo) throws RemoteException, ServiceException, WebServiceException {
		WSMedia newWsMedia = mediaCenterService.createWSVideo(storyWSVideo.getMedia());
		storyWSVideo.setMedia(newWsMedia);

		Story story = storyDao.get(id);
		if (story.getVtSection() == null) {
			story.setVtSection(new StoryVTSection());
		}
		story.getVtSection().addSubSection(storyWSVideo);
		story.setVideosAdded(story.hasVideos());
		save(story);
	}

	@Override
	public void removeStory(long storyId) throws ValidationException, SessionManagerException {
		Story story = storyDao.get(storyId);
		long blockId = story.getBlock().getId();

		//Remove
		remove(story);

		//Reordena as laudas
		//Block block = story.getBlock();
		sortStoriesFromIndex(blockId, story.getOrder());
	}

	@Override
	public void remove(Story story) throws ValidationException, SessionManagerException {
		validateFields(story);

		Rundown rundown = story.getBlock().getRundown(); // Pega agora antes que block fique null

		removeStory(story);

		dispatchEvent(new StoryEvent(EXCLUDED, story, rundown, this));
		storyMessenger.notifyDelete(story, rundown.getId());
	}

	@Override
	public void removeStories(Long[] storyIds, Long rundownId) {
		List<Story> stories = storyDao.listById(storyIds);

		StoryEvent excludeEvent = null;

		for (Story current : stories) {
			if (excludeEvent == null) {
				Rundown rundown = current.getBlock().getRundown(); // Pega agora antes que block fique null
				excludeEvent = new StoryEvent(EXCLUDED, rundown, this);
			}

			// As laudas não devem ser notificadas individualmente
			removeStory(current);

			excludeEvent.registerChange(current);
		}

		dispatchEvent(excludeEvent);
		storyMessenger.notifyMultipleExclusion(storyIds, rundownId);
	}

	// Método usado pelos metodos remove(Story story) e removeStories(List<String> idsOfStories, Integer programId)
	// Pois tem codigo em comum, mas um deve notificar individualmente e o outro nao
	private void removeStory(Story story) {
        story.getPrograms().remove(story.getProgram());

		// Retira o registro de uso do programa na lauda original da gaveta
		Story drawerOrigin = story.findDrawerOrigin();
		if (drawerOrigin != null && story.getProgram() != null) {
			drawerOrigin.getPrograms().remove(story.getProgram());
			storyDao.update(drawerOrigin);
			storyMessenger.notifyUpdate(drawerOrigin, getRundownIdFromStory(story));
		}

		// Remove registro de programas
		unregisterProgram(story);

		story.setBlock(null);
		story.setExcluded(true);
//        story.setApproved(false);

		storyDao.update(story, sessionManager.getCurrentUser());
		storyDao.flush();
	}

	private void unregisterProgram(Story story) {
		if (story.getProgram() != null) {
			if (story.getGuideline() != null) {
				// Remove o registro do programa na pauta de origem
				guidelineService.unregisterProgram(story.getGuideline(), story.getProgram());
			}
			else if (story.getReportage() != null) {
				// Remove o registro do programa na reportagem de origem
				reportageService.unregisterProgram(story.getReportage(), story.getProgram());
			}
			else {
                if (story.getSourceStory() != null) {
                    story.getSourceStory().getPrograms().remove(story.getProgram());
                    storyDao.update(story.getSourceStory());
		}
                story.getPrograms().remove(story.getProgram());
	}
	}
	}

	private void registerProgram(Story story, Program program) {
		if (program != null) {
			if (story.getGuideline() != null) {
				// Registra o programa na pauta de origem
				guidelineService.registerProgram(story.getGuideline(), program);
			}
			if (story.getReportage() != null) {
				// // Registra o programa na reportagem de origem
				reportageService.registerProgram(story.getReportage(), program);
			}
		}
	}

	@Override
	public void loadAndUpdateStoryWSVideo(long storyId, StoryWSVideo storyWSVideo) throws RemoteException, ServiceException, WebServiceException {
		WSMedia synchronizedWSMedia = mediaCenterService.loadWSMedia(storyWSVideo.getMedia());

		Story currentStory = storyDao.get(storyId);
		storyDao.evict(currentStory);

		updateStoryWSVideo(currentStory, synchronizedWSMedia, storyWSVideo);

		storyDao.update(currentStory);
		storyDao.flush();

		storyMessenger.notifyUpdate(currentStory, currentStory.getBlock().getRundown().getId());
	}

	private void updateStoryWSVideo(Story currentStory, WSMedia synchronizedWSMedia, StoryWSVideo storyWSVideo) {
		for (StoryWSVideo storyWSVideoCurrent : currentStory.subSectionsOfType(StoryWSVideo.class)) {
			if (storyWSVideoCurrent.equals(storyWSVideo)) {
				if (synchronizedWSMedia == null) {
					mediaCenterService.deleteWSMedia(storyWSVideoCurrent.getMedia());
					storyWSVideoCurrent.setMedia(null);
				} else {
					synchronizedWSMedia.setId(storyWSVideo.getMedia().getId());
					if (synchronizedWSMedia.isDifferent(storyWSVideo.getMedia()))
						storyWSVideoCurrent.setMedia(synchronizedWSMedia);
				}
				break;
			}
		}
	}

	//--------------------------------------------------------------------------
	//	Helpers Methods
	//--------------------------------------------------------------------------
	private void changePresenters(Story story, Program destinyProgram) {
		User defaultPresenter = destinyProgram.getDefaultPresenter();
		Set<User> presenters = destinyProgram.getPresenters();

		boolean anyPresenterChanged = false;

		for (StoryText text : story.subSectionsOfType(StoryText.class)) {
			if (!presenters.contains(text.getPresenter())) {
				text.setPresenter(defaultPresenter);
				text.calculateReadTime();

				anyPresenterChanged = true;
			}
		}

		for (StoryCameraText text : story.subSectionsOfType(StoryCameraText.class)) {
			if (!presenters.contains(text.getPresenter())) {
				text.setPresenter(defaultPresenter);
				text.calculateReadTime();

				anyPresenterChanged = true;
			}
		}

		if (anyPresenterChanged) {
			story.calculateTotal();
		}
	}

	private void disableCredits(Story story) {
		for (StoryCG cg : story.subSectionsOfType(StoryCG.class)) {
			cg.setDisabled(true);
		}
		for (StoryMosCredit cg : story.subSectionsOfType(StoryMosCredit.class)) {
			cg.setDisabled(true);
		}
	}

	private void sortStoriesFromIndex(long blockId, long orderRemovedStory) {
		Block block = blockDao.get(blockId);
		List<Story> stories = storyDao.listByBlock(block);

		for (Story story : stories) {
			if (story.getOrder() > orderRemovedStory) {
				story.setOrder(story.getOrder() - 1);
				storyDao.save(story);
			}
		}
	}

	private void correctCameraTextPresenters(Story story) {
		// Define o apresentador da cabeça que pode ter sido gerada a partir de uma sugestão de cabeça
		User defaultPresenter = story.getProgram().getDefaultPresenter();
		for (StoryCameraText camText : story.subSectionsOfType(StoryCameraText.class)) {
			camText.setPresenter(defaultPresenter);
		}
	}

	private void sortBlock(Block block) {
		int index = -1;
		for (Story story : block.getStories()) {
			if (story.getOrder() != ++index) {
				story.setOrder(index);
				storyDao.save(story);
			}
		}
	}
	
//	private void reorderBlock(Block block) {
//		int index = -1;
//		for (Story story : block.getStories()) {
//			if (story.getOrder() != ++index) {
//				story.setOrder(index);
//			}
//		}
//	}

	private String getNextPage(Rundown rundown) {
		String maxPage = storyDao.getMaxPage(rundown);
		Integer aux = maxPage == null ? 0 : Integer.parseInt(maxPage.replaceAll("[^\\d]", ""));
		return ++aux < 10 ? "0" + aux : aux.toString();
	}

	/*
	 * Valida campos obrigatórios dos campos da lauda.
	 */
	private void validateFields(Story story, String field, Object object) throws ValidationException {
		if ((story == null) || (field.equals("")) || (object == null)) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	/*
	 * Valida campos obrigatórios dos storys.
	 */
	private void validateFields(Story story) throws ValidationException {
		if (story == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	/*
	 * Valida campos obrigatórios dos blocks.
	 */
	private void validateFields(Block block) throws ValidationException {
		if (block == null || block.getRundown() == null) {
			log.debug(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
			throw new ValidationException(bundle.getMessage(DEFAULTS_REQUIRED_FIELDS));
		}
	}

	private void dispatchEvent(StoryEvent event) {
		if (event != null && !event.isEmpty()) {
		    publisher.publishEvent(event);
	    }
	}

  private Long getRundownIdFromStory(Story story) {
		if (story != null) {
			if (story.getBlock() != null && story.getBlock().getRundown() != null) {
				return story.getBlock().getRundown().getId();
			}
		}
		return  -1L;
	}
	
	private Long getRundownIdFromStories(Collection<Story> stories) {
		for (Story story : stories) {
			if (story.getBlock() != null && story.getBlock().getRundown() != null) {
				return story.getBlock().getRundown().getId();
			}
		}
		return -1L;
	}

	private String nextPage(Rundown rundown) {
		String maxPage = storyDao.getMaxPage(rundown);
		if (maxPage == null) {
			return "01";
		}

		int aux = Integer.parseInt(maxPage.replaceAll("[^\\d]", ""));
		return ++aux > 9 ? Integer.toString(aux) : "0" + aux;
	}

	//--------------------------------------------------------------------------
	//	Setters
	//--------------------------------------------------------------------------
    
	public void setNewsDao(NewsDao newsDao) {
    	this.newsDao = newsDao;
    }
	
	public void setStoryKindDao(StoryKindDao storyKindDao) {
		this.storyKindDao = storyKindDao;
	}

	public void setRundownDao(RundownDao rundownDao) {
		this.rundownDao = rundownDao;
	}

	public void setStoryDao(StoryDao storyDao) {
		this.storyDao = storyDao;
	}

	public void setBlockDao(BlockDao blockDao) {
		this.blockDao = blockDao;
	}

	public void setStoryMessenger(StoryMessenger value) {
		this.storyMessenger = value;

		lockerStory.removeAllEntityLockerListeners();
		lockerStory.addEntityLockerListener(new EntityLockerListener<Story>() {

			@Override
			public void entityLocked(Story story, User user) {
				storyMessenger.notifyLock(story, user, getRundownIdFromStory(story));
			}

			@Override
			public void entityUnlocked(Story story, User user) {
				storyMessenger.notifyUnlock(story, getRundownIdFromStory(story));
			}
		});
	}

	public void setGuidelineService(GuidelineService guidelineService) {
		this.guidelineService = guidelineService;
	}

	public void setReportageService(ReportageService reportageService) {
		this.reportageService = reportageService;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public void setProgramDao(ProgramDao programDao) {
		this.programDao = programDao;
	}

	public void setGuidelineDao(GuidelineDao guidelineDao) {
		this.guidelineDao = guidelineDao;
	}

	public void setReportageDao(ReportageDao reportageDao) {
		this.reportageDao = reportageDao;
	}

	public void setStoryFTSDao(StoryFullTextDao storyFTSDao) {
		this.storyFTSDao = storyFTSDao;
	}

	public void setStoryCopyLogDao(StoryCopyLogDao storyCopyLogDao) {
		this.storyCopyLogDao = storyCopyLogDao;
	}

	public void setMosMediaDao(MosMediaDao mosMediaDao) {
		this.mosMediaDao = mosMediaDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	public void setStoryKindService(StoryKindService storyKindService) {
		this.storyKindService = storyKindService;
	}

	public void setMediaCenterService(MediaCenterService mediaCenterService) {
		this.mediaCenterService = mediaCenterService;
	}

	public void setStoryMosCreditDao(StoryMosCreditDao storyMosCreditDao) {
		this.storyMosCreditDao = storyMosCreditDao;
	}
}
