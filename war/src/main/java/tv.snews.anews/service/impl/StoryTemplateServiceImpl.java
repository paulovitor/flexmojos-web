package tv.snews.anews.service.impl;

import tv.snews.anews.dao.BlockTemplateDao;
import tv.snews.anews.dao.StoryKindDao;
import tv.snews.anews.dao.StoryTemplateDao;
import tv.snews.anews.domain.*;
import tv.snews.anews.messenger.StoryTemplateMessenger;
import tv.snews.anews.service.StoryTemplateService;

import java.util.Date;

import static tv.snews.anews.util.DateTimeUtil.getMidnightTime;

/**
 * Implementação do serviço de manipulação de laudas modelo.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
public class StoryTemplateServiceImpl implements StoryTemplateService {

    private BlockTemplateDao blockTemplateDao;
    private StoryTemplateDao storyTemplateDao;
    private StoryKindDao storyKindDao;

    private StoryTemplateMessenger storyTemplateMessenger;

    @Override
    public StoryTemplate load(Integer storyId) {
        return storyTemplateDao.get(storyId);
    }

    @Override
    public synchronized StoryTemplate createStory(Integer blockId) {
        BlockTemplate block = blockTemplateDao.get(blockId);
        StoryTemplate story = block.createStory();

        story.setVideosAdded(story.hasVideos());
        story.setKind(storyKindDao.getDefaultStoryKind());
        story.setId((Integer) storyTemplateDao.save(story));

        return story;
    }

    @Override
    public StoryTemplate updateStory(StoryTemplate story) {
        story.setVideosAdded(story.hasVideos());
        story = storyTemplateDao.merge(story);
        storyTemplateMessenger.notifyUpdate(story);
        return story;
    }

	@Override
	public boolean updateField(Integer storyId, String field, Object object) {
		StoryTemplate story = storyTemplateDao.get(storyId);
		StoryTemplate.Field storyField = StoryTemplate.Field.valueOf(field);

		switch (storyField) {
			case PAGE:
				story.setPage((String) object);
				return true;
			case SLUG:
				story.setSlug((String) object);
				return true;
			case EDITOR:
				story.setEditor((User) object);
				return true;
			case VT:
				Date newVt = (Date) object;
				StoryVTSection vtSection = story.getVtSection();
				if (vtSection == null) {
					story.setVt(newVt);
					story.setVtSection(new StoryVTSection(newVt));
				} else {
					if (newVt.equals(getMidnightTime())) {
						if (vtSection.getSubSections().isEmpty()) {
							story.setVtSection(null);
						} else {
							story.getVtSection().setDuration(newVt);
						}
						story.setVt(newVt);
					} else {
						if (story.getVtSection().countSubSectionsOfType(StoryMosVideo.class) == 0) {
							story.getVtSection().setDuration(newVt);
							story.setVt(newVt);
						} else {
							return false; // não houve mudança
						}
					}
				}
				return true;
			default:
				throw new IllegalArgumentException("Invalid story field: " + storyField);
		}
	}

    @Override
    public void updatePosition(Integer storyId, Integer blockId, int order) {
        StoryTemplate story = storyTemplateDao.get(storyId);
        BlockTemplate block = blockTemplateDao.get(blockId);

        story.setBlock(block);
        story.setOrder(order);

        updateStory(story);
    }

    @Override
    public void removeStory(Integer storyId) {
        StoryTemplate story = storyTemplateDao.get(storyId);
        BlockTemplate block = story.getBlock();
        block.removeStory(story);
    }
	
	//--------------------------------------------------------------------------
	//	Setters
	//--------------------------------------------------------------------------

	public void setStoryTemplateDao(StoryTemplateDao storyTemplateDao) {
		this.storyTemplateDao = storyTemplateDao;
	}

    public void setBlockTemplateDao(BlockTemplateDao blockTemplateDao) {
        this.blockTemplateDao = blockTemplateDao;
    }

    public void setStoryKindDao(StoryKindDao storyKindDao) {
        this.storyKindDao = storyKindDao;
    }

    public void setStoryTemplateMessenger(StoryTemplateMessenger storyTemplateMessenger) {
        this.storyTemplateMessenger = storyTemplateMessenger;
    }
}
