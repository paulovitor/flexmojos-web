package tv.snews.anews.service;

import tv.snews.anews.domain.User;
import tv.snews.anews.exception.LoginException;
import tv.snews.anews.exception.SessionManagerException;

/**
 * Estabele um contrato para manipulação dos acessos ao sistema.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface LoginService {

	/**
	 * Adquire o usuário logado atualmente no sistema.
	 * 
	 * @return O usuário logado atualmente no sistema.
	 * @throws SessionManagerException
	 */
	User getCurrentUser() throws SessionManagerException;

	/**
	 * Faz o login o usuário e adiciona seus dados ao controle de sessão.
	 * 
	 * @param user objeto com os dados do usuário que está efetuando o login
     * @return o objeto do usuário atualizado e com os grupos carregados
	 * @throws SessionManagerException se houver uma falha ao criar a sessão
	 */
	User login(User user, Long timezoneOffset) throws SessionManagerException, LoginException;

	User agreeWithTerms(int userId);

	/**
	 * Permite o usuário corrente sair da sessão.
	 * 
	 * @throws SessionManagerException
	 */
	void leave() throws SessionManagerException;

}
