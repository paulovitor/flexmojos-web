package tv.snews.anews.service;

import tv.snews.anews.domain.ReportEvent;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Define o contrato do serviço que manipula objetos {@link tv.snews.anews.domain.ReportEvent}.
 *
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportEventService {

	/**
	 * Busca e retorna a lista de todos os eventos de relatório cadastrados
	 * pelos usuários do sistema.
	 *
	 * @return os eventos cadastrados
	 */
	List<ReportEvent> listAll();

	/**
	 * Cadastra ou atualiza os dados de um eventos informados pelo usuário.
	 *
	 * @param event dados do evento
	 */
	void save(ReportEvent event);

	/**
	 * Apaga no banco de dados os dados de um evento cadastrado.
	 *
	 * @param id identificador do registro do evento
	 * @throws tv.snews.anews.exception.IllegalOperationException se o evento estiver em uso por algum
	 *             relatório
	 */
	void remove(Integer id) throws IllegalOperationException;
	
	/**
	 * Verifica se um evento está associada a algum relatório.
	 * 
	 * @param id identificador do registro do evento
	 * @return {@code true} se o evento estiver associado a algum relatório
	 */
	boolean isInUse(Integer id);
}
