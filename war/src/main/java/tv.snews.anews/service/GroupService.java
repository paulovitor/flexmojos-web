package tv.snews.anews.service;

import tv.snews.anews.domain.Group;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Fornece os serviços para manipulação dos grupos.
 * 
 * @author Samuel Guedes de Melo.
 * @author Felipe Zap de Mello.
 * @since 1.0.0
 */
public interface GroupService {

	List<Group> listAll();
	
	Group listGroupByName(String name);
	
	void save(Group group);

	void delete(Integer id) throws IllegalOperationException;
}
