package tv.snews.anews.service;

import tv.snews.anews.domain.ContactNumber;

import java.util.List;

/**
 * Fornece os serviços para manipulação dos Numeros do Contato.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface ContactNumberService {

	/**
	 * Faz o cadastro de um novo numero de contato.
	 * 
	 * @param segment
	 */
	void save(ContactNumber contactNumber);

	/**
	 * Deleta o numero de contato.
	 * 
	 * @param institution
	 */
	void delete(ContactNumber contactNumber);

	/**
	 * Retorna a lista de todos os @{link ContactNumber} cadastrados em ordem
	 * alfabética.
	 * 
	 * @return Lista de numeros de contato cadastrados.
	 */
	List<ContactNumber> listAll();

}
