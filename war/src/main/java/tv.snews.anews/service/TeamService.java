package tv.snews.anews.service;

import tv.snews.anews.domain.Team;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Fornece os serviços para manipulação das equipes
 * 
 * @author Maxuel Ramos.
 * @since 1.7
 */
public interface TeamService {

	List<Team> listAll();

    Team listTeamByName(String name);
	
	void save(Team team);

	void delete(Integer id) throws IllegalOperationException;

    /**
     * Verifica se uma equipe está associado a alguma pauta.
     *
     * @param id identificador do registro da equipe
     * @return {@code true} se  equipe estiver associado a alguma pauta
     */
    boolean isInUse(Integer id);
}
