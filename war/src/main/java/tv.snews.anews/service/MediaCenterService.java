package tv.snews.anews.service;

import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.MosMediaParams;
import tv.snews.anews.search.WSMediaParams;
import tv.snews.mos.client.UnexpectedMessageException;

import javax.xml.rpc.ServiceException;
import java.net.SocketTimeoutException;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Interface que define o contrato que deve ser seguido para a implementação do
 * serviço responsável pelas ações relacionadas ao Media Center.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public interface MediaCenterService {

	/**
	 * Obtém objetos de mídia pela lista de ids.
	 * 
     * @param ids identificadores dos MosMedia.
     */
	List<MosMedia> loadById(List<Integer> ids);
	
	/**
	 * Obtém todos os objetos de mídia do gerenciador de conteúdo multimídia.
	 * 
     * @param mosDevice
     */
    List<MosMedia> mosReqAll(MosDevice mosDevice, Boolean interval);
    
	/**
	 * Requisita campos de busca disponíveis para montar interface para o Media
	 * Center.
	 * 
	 * @param mosDevice dispositivo gerenciador multimídia mos.
	 * @return o objeto atualizado
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	String mosReqSearchableSchema(MosDevice mosDevice) throws SessionManagerException;

	/**
	 * Retorna a página de registros solicitada. Serão retornados não somente os
	 * registros encontrados, como também o total de registos no banco de dados
	 * e o tamanho da página (tudo armazenado em um objeto {@link tv.snews.anews.flex.PageResult}).
	 *
	 * @param params parâmetros de busca e o número da página corrente
	 * @return os resultados encontrados junto com as informações necessárias
	 *         para a paginação
	 */
	PageResult<MosMedia> findByParams(MosMediaParams params);

    /**
     * Retorna a página de registros solicitada. Serão retornados não somente os
     * registros encontrados, como também o total de registos no banco de dados
     * e o tamanho da página (tudo armazenado em um objeto {@link tv.snews.anews.flex.PageResult}).
     *
     * @param params parâmetros de busca e o número da página corrente
     * @return os resultados encontrados junto com as informações necessárias
     *         para a paginação
     */
    PageResult<WSMedia> findByWSParams(WSDevice wsDevice, WSMediaParams params) throws RemoteException, ServiceException, WebServiceException, SocketTimeoutException;

	/**
	 *  Obtem todos os objetos de mídia do gerenciador de conteúdo multimídia
	 *  quando solicitado mosReqAll com {interval == true}.
	 *
     * @param mosDevice
     * @param schema o esquema de busca informado pelo MOS
     * @throws tv.snews.anews.exception.SessionManagerException
     */
    void mosReqObjList(MosDevice mosDevice, String schema) throws SessionManagerException;

	/**
	 *  Cria um objeto de mídia vazio no MAN.
	 *
     * @param mosMedia objeto de mídia a ser criado.
     * @return {@link tv.snews.anews.domain.MosMedia};
     */
    MosMedia mosObjCreate(MosMedia mosMedia) throws UnexpectedMessageException, IllegalOperationException;

    /**
     *  Verifica se existe um objeto de mídia com os parâmetros informados.
     *
     * @param objID id do objeto de mídia a ser verificado.
     * @param mosDevice Device do objeto de mídia a ser verificado.
     * @return {@link tv.snews.anews.domain.MosMedia};
     */
    public Boolean canCreateMosObj(String objID, MosDevice mosDevice);

    WSMedia createWSVideo(WSMedia wsMedia) throws RemoteException, WebServiceException, ServiceException;

    WSMedia loadWSMedia(WSMedia wsMedia) throws RemoteException, ServiceException, WebServiceException;

    String checkUseOfDevice(Integer deviceId);

    void deleteWSMedia(WSMedia wsMedia);
}
