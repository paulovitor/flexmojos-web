package tv.snews.anews.service;

import tv.snews.anews.domain.Round;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface RoundService {

	/**
	 * Lista todas rondas.
	 * 
	 * @return List<Round>
	 */
	List<Round> listAll(Date date);

	/**
	 * Consulta ronda pelo identificador.
	 * 
	 * @param idRound
	 * @return Round
	 */
	Round get(Serializable idRound);
	
	/**
	 * Exclui as rondas.
	 * 
	 * @param Round
	 */
	void remove(Long id);

	/**
	 * Salva a ronda.
	 * 
	 * @param Round
	 */
	void save(Round round) throws SessionManagerException;

	/**
     * @param roundId
     * @param userId
     * @throws IllegalOperationException
     */
    void lockEdition(Long roundId, Integer userId) throws IllegalOperationException;

	/**
     * @param roundId
     * @throws IllegalOperationException
     */
    void unlockEdition(Long roundId) throws IllegalOperationException;

	/**
     * @param roundId
     * @return
     */
    User isLockedForEdition(Long roundId);
}
