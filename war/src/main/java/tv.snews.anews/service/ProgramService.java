package tv.snews.anews.service;

import tv.snews.anews.dao.ProgramDao;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;

/**
 * Fornece os serviços para manipulação dos programas.
 *
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface ProgramService {

    /**
     * Obtém a praça pelo id.
     *
     * @param idPrograma do programa.
     */
    Program loadById(Integer idPrograma);

	/**
	 * Faz o cadastro de um novo programa informado pelo usuário.
	 *
	 * @param program
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	void save(Program program) throws ValidationException;

	/**
	 * Deleta o programa que possui o ID informado. Se o programa possuir pautas
	 * ou espelhos relacionados a ele, a operação será negada.
	 *
	 * @param id identificador único do programa
	 * @throws tv.snews.anews.exception.IllegalOperationException se o programa estiver em uso
	 */
	void delete(Integer id) throws IllegalOperationException;

	/**
	 * Retorna a lista de todos os @{link Program} cadastrados em ordem
	 * alfabética.
	 *
	 * @return Lista de programas cadastrados.
	 */
	List<Program> listAll();

    List<Program> listAllOrderByStart();

	/**
	 * Retorna a lista de todos os @{link Program} cadastrados em ordem
	 * alfabética com os inativos incluídos.
	 *
	 * @return Lista de programas cadastrados.
	 */
	List<Program> listAllWithDisableds(ProgramDao.FetchType... fetchTypes);

	Set<User> listPresenters(Integer programId);

	Set<User> listEditors(Integer programId);

	SortedSet<User> listEditorsOfPrograms();

    Set<User> listImageEditors(Integer programId);
}
