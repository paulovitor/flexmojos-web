/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.service;

import tv.snews.anews.domain.Company;

import java.util.List;

/**
 * Interface que define o contrato que deve ser seguido para a implementação do
 * serviço responsável pelas ações relacionadas ao cadastro de praças.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.3
 */
public interface CompanyService {

	/**
	 * Obtém a praça pelo id.
	 * 
     * @param id da praça.
     */
	Company loadById(Integer idCompany);
	
	/**
	 * Obtém a praça pelo nome e o host.
	 * 
     * @param nome da praça.
     * @param host da praça.
     */
	List<Company> listCompanyByNameHost(String name, String host);
	
	/**
	 * Faz o cadastro de uma nova praça informado pelo usuário verifica pelo
	 * nome se já existi uma praça com o mesmo nome se houver nao salva.
	 * 
	 * @param company
	 */
	void save(Company company);

	/**
	 * Deleta a praça.
	 * 
	 * @param company
	 */
	void delete(Integer idCompany);

	/**
	 * Retorna a lista de todas as @{link Company} cadastradas em ordem
	 * alfabética.
	 * 
	 * @return Lista de praças cadastrados.
	 */
	List<Company> listAll();
	
}
