package tv.snews.anews.service;

import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistRevision;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.ChecklistParams;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public interface ChecklistService {

	Checklist loadById(Long checklistId);

	Checklist save(Checklist checklist) throws IllegalOperationException;

	Checklist save(Checklist checklist, boolean lockOnInsert) throws IllegalOperationException;

	void remove(Long checklistId) throws ValidationException, SessionManagerException;

	PageResult<Checklist> findByParams(ChecklistParams params);

	List<Checklist> findAllNotExcludedOfDate(Date date, boolean notDone);

	/**
	 * Restaura uma produção da lixeira para uma data definida pelo usuário.
	 *
	 * @param id         Produção a ser restaurada.
	 * @param destinyDate Data selecionada.
	 * @throws SessionManagerException
	 */
	Checklist restoreExcluded(Long id, Date destinyDate) throws SessionManagerException, IllegalOperationException;

	Collection<ChecklistRevision> loadChangesHistory(Long id);

	/**
	 * Paginador de produções excluídas.
	 *
	 * @param pageNumber número da página.
	 * @return
	 */
	PageResult<Checklist> listExcludedChecklistPage(int pageNumber);

	User checkEditionLock(Long id);

	void lockEdition(Long id) throws IllegalOperationException;

	void unlockEdition(Long id) throws IllegalOperationException;

	Checklist findChecklistByGuideline(Long idGuideline);

	boolean checklistTasksChanged(Checklist checklistA, Checklist checklistB);
}
