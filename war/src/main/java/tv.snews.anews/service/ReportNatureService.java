package tv.snews.anews.service;

import tv.snews.anews.domain.ReportNature;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Define o contrato do serviço que manipula objetos {@link ReportNature}.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportNatureService {

	/**
	 * Busca e retorna a lista de todas as naturezas de relatório cadastradas 
	 * pelos usuários do sistema.
	 * 
	 * @return as naturezas cadastradas
	 */
	List<ReportNature> listAll();
	
	/**
	 * Cadastra ou atualiza os dados de uma natureza informados pelo usuário.
	 * 
	 * @param nature dados da natureza
	 */
	void save(ReportNature nature);
	
	/**
	 * Apaga no banco de dados os dados de uma natureza cadastrada.
	 * 
	 * @param id identificador do registro da natureza
	 * @throws IllegalOperationException se a natureza estiver em uso por algum
	 *             relatório
	 */
	void remove(Integer id) throws IllegalOperationException;
	
	/**
	 * Verifica se uma natureza está associada a algum relatório.
	 * 
	 * @param id identificador do registro da natureza
	 * @return {@code true} se a natureza estiver associada a algum relatório
	 */
	boolean isInUse(Integer id);
}
