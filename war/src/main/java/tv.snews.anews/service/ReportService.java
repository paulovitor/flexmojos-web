package tv.snews.anews.service;

import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.ReportParams;

/**
 * Interface que define os métodos relacionados à entidade {@link Report} que 
 * devem ser oferecidos pela implementação do serviço.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportService {

	/**
	 * Retorna a página de registros solicitada. Serão retornados não somente os
	 * registros encontrados, como também o total de registos no banco de dados
	 * e o tamanho da página (tudo armazenado em um objeto {@link PageResult}).
	 * 
	 * @param params parâmetros de busca e o número da página corrente
	 * @return os resultados encontrados junto com as informações necessárias
	 *         para a paginação
	 */
	PageResult<Report> findByParams(ReportParams params);
	
	/**
	 * Busca por um relatório que possui um {@code id} informado e retorna os
	 * seus dados caso seja encontrado. Em caso contrário será retornado
	 * {@code null}.
	 * 
	 * @param id identificador do relatório
	 * @return o relatório encontrado ou {@code null} caso não encontre
	 */
	Report findById(Long id);
	
	/**
	 * Cadastra ou atualiza os dados de um relatório que foram informados pelo
	 * usuário.
	 * <p>
	 * O usuário da sessão será atribuido como autor do relatório.
	 * </p>
	 * 
	 * @param report dados do relatório
	 * @return o objeto com os dados persistidos
	 * @throws IllegalOperationException se a operação de lock de edição falhar
	 * @throws SessionManagerException se não conseguir obter os dados da sessão
	 */
	Report save(Report report) throws SessionManagerException, IllegalOperationException;
	
	/**
	 * Exclui do banco de dados o relatório que possui o {@code id} informado.
	 * 
	 * @param id identificador único do relatório
	 */
	void deleteById(Long id);
	
	/**
	 * Verifica se existe uma trava de edição criada para o relatório que possui
	 * o {@code id} informado.
	 * 
	 * @param id identificador do relatório
	 * @return o usuário que criou a trava de edição ou {@code null} caso o
	 *         relatório esteja livre para edição
	 */
	User checkEditionLock(Long id);
	
	/**
	 * Cria uma trava de edição para o relatório de {@code id} informado. O dono
	 * da trava será obtido da sessão corrente.
	 * 
	 * @param id identificador do relatório
	 * @throws IllegalOperationException se outro usuário já estiver editando o
	 *             relatório informado
	 */
	void lockEdition(Long id) throws IllegalOperationException;
	
	/**
	 * Remove a trava de edição para o relatório de {@code id} informado.
	 * 
	 * @param id identificador do relatório
	 * @throws IllegalOperationException se o usuário que solicitou a remoção
	 *             não for o usuário que detêm a trava
	 */
	void unlockEdition(Long id) throws IllegalOperationException;
}
