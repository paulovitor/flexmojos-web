package tv.snews.anews.service;

import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptRevision;
import tv.snews.anews.domain.ScriptVideo;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.ScriptEvent;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.infra.ClipboardAction;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface ScriptService {

    String OK = "OK";
    String SCRIPT_PLAN_NOT_FOUND = "SCRIPT_PLAN_NOT_FOUND";
    String CREDITS_REMOVED = "CREDITS_REMOVED";
    String SCRIPT_PLAN_DISPLAYED = "SCRIPT_PLAN_DISPLAYED";

    void loadAndUpdateScriptVideo(long scriptId, ScriptVideo scriptVideo) throws RemoteException, ServiceException, WebServiceException;

    enum Field { PAGE, SLUG, EDITOR, REPORTER, OK, APPROVED, VT }

    /**
     * Retorna os dados do script que possui o identificador informado.
     *
     * @param id identificador único do script
     * @return objeto com os dados do script ou {@code null} se nenhum registro
     *         for encontrado
     */
    Script loadById(Long id) throws IOException;

    void removeImg(String imgPath) throws IOException;

	Script create(Long blockId) throws ValidationException, IOException;

    void removeScript(long scriptId) throws ValidationException, SessionManagerException;

    void remove(Script script) throws ValidationException, SessionManagerException;

    void removeScripts(Long[] idsOfScripts, Long scriptPlanId);

	void updateField(Long scriptId, String field, Object value);

	void sendToDrawer(Long scriptId);

    void copyDrawerScriptToBlock(Long scriptId, Long blockId, int index) throws CloneNotSupportedException, IOException;

    void createScriptFromGuideline(long blockId, int index, long guidelineId) throws ValidationException, SessionManagerException, IOException;

    void createScriptFromReportage(long blockId, int index, long reportageId) throws ValidationException, SessionManagerException, IOException;

    void clearClipboard();

    Long[] sendScriptsToClipboard(ClipboardAction action, Long[] ids);

    Long[] pastScriptsFromClipboard(Long blockId, int dropIndex) throws CloneNotSupportedException, IOException;

    void moveScripts(Long[] scriptsIds, Long blockId, int dropIndex);

    byte[] loadImg(String imgPath) throws IOException;

    /**
     * Salva um script já existente.
     *
     * @param script
     * @param scriptEvent
     * @return Objeto do script com seus respectivos objetos populados em sua
     *         composição.
     * @throws tv.snews.anews.exception.ValidationException
     * @throws java.io.IOException
     */
    void save(Script script, ScriptEvent scriptEvent) throws ValidationException, IOException;

    /**
     * Copia ou move roteiro para o dia e programa informados. Se o
     * roteiro na data e programa informados.
     *
     * @param scriptIds script(s) a ser(em) copiado(s)
     * @param date data do roteiro de destino
     * @param destinyProgramId Identificador do programa de destino do roteiro.
     * @param move para cópia false para mover true.
     * @return void
     * @throws tv.snews.anews.exception.SessionManagerException caso não consiga obter o usuári
     * @throws java.io.IOException
     * @throws CloneNotSupportedException
     * @throws IllegalStateException
     */
    public void copyTo(Long[] scriptIds, Date date, int destinyProgramId, boolean move) throws IOException, CloneNotSupportedException, IllegalStateException;

	void updatePosition(Long scriptId, Long blockId, int order, boolean dragNDrop);

	void updatePositions(Long[] scriptsIds, Long blockId, int dropIndex);

    Script merge(Script script) throws SessionManagerException, IOException;

    Script updateVideos(Script script) throws SessionManagerException, IOException;

/**
     * Restaura um script que foi excluído para o roteiro do dia e programa
     * informados.
     *
     * @param scriptId script a ser restaurado
     * @param date data destino da restauração
     * @param programId programa destino da restauração
     * @return código do resultado da restauração
     * @throws tv.snews.anews.exception.SessionManagerException se falhar ao obter os dados do usuário
     *             logado
     */
    String restoreExcludedScript(Long scriptId, Date date, int programId) throws SessionManagerException;

    /**
     * Faz a contagem de todos os script marcados como excluídos.
     *
     * @return a quantidade de scripts excluídos
     */
    int countExcludedScripts();

    PageResult<Script> listExcludedScripts(int pageNumber);
    
    Collection<ScriptRevision> loadChangesHistory(Long scriptId);
    
    void lockEdition(Long scriptId, Integer userId) throws IllegalOperationException;
    
    void unlockEdition(Long scriptId) throws IllegalOperationException;
    
    User isLockedForEdition(Long scriptId);

    void assumeEdition(Long scriptId, Integer userId) throws IllegalOperationException;

    List<ScriptVideo> loadVideosByScriptId(Long id);

    void createScriptVideo(Long id, ScriptVideo scriptVideo) throws ValidationException, IOException, IllegalOperationException, WebServiceException, ServiceException;
}
