package tv.snews.anews.service;

import tv.snews.anews.domain.*;
import tv.snews.anews.exception.InvalidChatState;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.exception.SessionManagerException;

import java.util.Date;
import java.util.List;

/**
 * Interface que define os métodos que devem ser implementados para atender as
 * necessidades do sistema de troca de mensagens.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface MessageService {

	/**
	 * Implementa o envio de uma mensagem de texto para um ou mais
	 * destinatários. O usuário que solicitou o envio da mensagem é obtido a
	 * partir da sua sessão. A mensagem será persistida no banco de dados e o
	 * registro gerado será retornado por meio de um objeto {@link tv.snews.anews.domain.TextMessage}.
	 *
	 * @param content Texto do conteúdo da mensagem.
	 * @param chat Referência ao chat da conversa dos usuários.
	 * @return Objeto com os dados da mensagem já persistida.
	 * @throws tv.snews.anews.exception.MessageDeliveryException Exceção lança quando ocorre uma falha ao
	 *         disparar a mensagem para seus destinatários.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 * @throws InvalidChatState Lançada quando a quantidade de membros ativos do
	 *         chat, excluindo o sender da mensagem, é igual a zero.
	 */
	TextMessage sendMessage(String content, Chat chat) throws MessageDeliveryException, SessionManagerException, InvalidChatState;

	/**
	 * Implementa o envio de uma mensagem de arquivo para um ou mais
	 * destinatários. O usuário que solicitou o envio da mensagem é obtido a
	 * partir da sua sessão. A mensagem será persistida no banco de dados e o
	 * registro gerado será retornado por meio de um objeto {@link tv.snews.anews.domain.TextMessage}.
	 *
	 * @param content Texto do conteúdo da mensagem.
	 * @param chat Referência ao chat da conversa dos usuários.
	 * @return Objeto com os dados da mensagem já persistida.
	 * @throws tv.snews.anews.exception.MessageDeliveryException Exceção lança quando ocorre uma falha ao
	 *         disparar a mensagem para seus destinatários.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 * @throws InvalidChatState Lançada quando a quantidade de membros ativos do
	 *         chat, excluindo o sender da mensagem, é igual a zero.
	 */
	FileMessage sendFile(UserFile file, Chat chat) throws MessageDeliveryException, SessionManagerException, InvalidChatState;

	/**
	 * Implementa o envio de uma mensagem de entidades para um ou mais
	 * destinatários. O usuário que solicitou o envio da mensagem é obtido a
	 * partir da sua sessão. A mensagem será persistida no banco de dados e o
	 * registro gerado será retornado por meio de um objeto {@link tv.snews.anews.domain.TextMessage}.
	 *
	 * @param entity Referência a entidade enviada ao(s) usuário(s).
	 * @param chat Referência ao chat da conversa dos usuários.
	 * @return Objeto com os dados da mensagem já persistida.
	 * @throws tv.snews.anews.exception.MessageDeliveryException Exceção lança quando ocorre uma falha ao
	 *         disparar a mensagem para seus destinatários.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 * @throws InvalidChatState Lançada quando a quantidade de membros ativos do
	 *         chat, excluindo o sender da mensagem, é igual a zero.
	 */
	EntityMessage sendEntityMessage(Object obj, Chat chat) throws MessageDeliveryException, SessionManagerException, InvalidChatState;

	/**
	 * Retorna uma lista contendo todas as mensagens offline recebidas pelo
	 * usuário. Caso ele não tenha recebido nenhuma mensagem offline será
	 * retornada uma lista vazia. O usuário do qual serão buscadas as mensagens
	 * offline é obtido da sessão.
	 *
	 * @return Lista de mensagens offline.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	List<AbstractMessage> loadOfflineMessages() throws SessionManagerException;

	/**
	 * Marca a mensagem informada como lida para o usuário da sessão.
	 *
	 * @param message Mensagem alvo.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	void markAsReaded(AbstractMessage message) throws SessionManagerException;

	/**
	 * Marca as mensagens como lida para o usuário da sessão.
	 *
	 * @param messages Lista de mensagens a serem marcadas como lidas.
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	void markAsReadedList(List<AbstractMessage> messages) throws SessionManagerException;
	
	/**
	 * Retorna a mensagem que possui o ID informado ou <code>null</code> caso
	 * não encontre.
	 * 
	 * @param id Identificador da mensagem.
	 * @return Mensagem encontrada ou <code>null</code>.
	 */
	AbstractMessage loadMessageById(long id);
	
	/**
	 * Retorna uma lista das últimas mensagens trocas entre o usuário logado e o
	 * usuário informado. A quantidade limite de mensagens retornadas é definida
	 * pelo parâmetro {@code limit}.
	 * 
	 * @param user Usuário com quem foi feita a conversa.
	 * @param limit limite de mensagens retornadas.
	 * @param firstMessageChatResult data da primeira mensagem do chat atual.
	 */
	List<AbstractMessage> loadLastMessagesWith(User user, int limit, Date firstMessageChatResult) throws SessionManagerException;
	
	/**
	 * Retorna uma lista das mensagens de acordo com o chat passado.
	 * 
	 * @param chatId id do chat desejado.
	 * @return Lista de mensagens encontradas, se houver.
	 */
	List<AbstractMessage> loadMessagesByChat(Chat chat) throws SessionManagerException;
	
	AbstractMessage getLastMessageOfChat(Chat chat) throws SessionManagerException;
	
	/**
	 * Implementa a mudança de status quando um usuário esta digitando uma
	 * mensagem para um destinatário. O usuário que está digitando a mensagem é obtido a partir da sua sessão.
	 * 
	 * @param typing Informa se o usuário está ou não digitando.
	 * @param chat Conversa na qual a ação ocorreu.
	 */
    void notifyUserIsTyping(boolean typing, Chat chat) throws SessionManagerException;
}
