package tv.snews.anews.service;

import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public interface ChecklistTypeService {

    List<ChecklistType> listAll();

    void save(ChecklistType type);

    void remove(Integer id) throws IllegalOperationException;

    boolean isInUse(Integer id);
}
