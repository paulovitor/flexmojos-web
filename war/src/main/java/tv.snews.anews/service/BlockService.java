package tv.snews.anews.service;

import tv.snews.anews.exception.ValidationException;

import java.util.Date;

/**
 * Fornece os serviços para manipulação dos blocos.
 * 
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface BlockService {

	/**
	 * Criar um novo block no Espelho.
	 * 
	 * @param rundown_id
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	void createBlock(long rundownId) throws ValidationException;

	/**
	 * Atualizar a hora do block.
	 *
	 * @param rundown_id
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	void updateBlockTime(long blockId, Date blockTime) throws ValidationException;

	/**
	 * Exclui um block.
	 *
	 * @param block
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	void removeBlock(long blockId) throws ValidationException;
}
