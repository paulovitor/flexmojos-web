package tv.snews.anews.service;

import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.GuidelineParams;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Interface de serviço da pauta.
 * 
 * @author Samuel Guedes de Melo.
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface GuidelineService {
	
	/**
	 * Envia email da pauta para o usuário(reporter) vinculado à pauta.
	 * 
	 * @param guidelineId O identificador da pauta a ser enviada.
	 * @throws tv.snews.anews.exception.ValidationException Qualquer erro de validação
	 * @throws tv.snews.anews.exception.SessionManagerException
	 * @throws tv.snews.anews.exception.MessageDeliveryException Qualquer erro ao enviar uma mensagem para os clientes do sistema
	 */
	void sendGuidelineMail(Long guidelineId) throws ValidationException, SessionManagerException;

	/**
	 * Carrega os dados de uma pauta a partir do seu ID.
	 *
	 * @param guidelineId Identificador da pauta.
	 * @return Objeto com os dados da pauta ou <code>null</code> caso não
	 *         encontre.
	 */
	Guideline loadById(Long guidelineId);

	Guideline save(Guideline guideline) throws IllegalOperationException;

    void registerProgram(Guideline guideline, Program program);

    void unregisterProgram(Guideline guideline, Program program);

    void updateState(Long guidelineId, DocumentState state, String reason);

	/**
	 * Faz a exclusão de uma pauta, o que na verdade apenas marca a mesma com
	 * uma flag de "excluída", podendo ser recuperada pela lixeira.
	 *
	 * @param guidelineId identificador da pauta que será enviada para a lixeira
	 * @throws tv.snews.anews.exception.ValidationException
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	void remove(Long guidelineId) throws ValidationException, SessionManagerException;

	/**
	 * Faz cópia identica de uma pauta.
	 *
     * @param dateToCopy Data de destino da pauta copiada.
     * @param programIdTocopy Identificador do programa de destino da pauta copiada.
     * @param guidelineIdToCopy Identificador da pauta a ser copiada.
     * @throws tv.snews.anews.exception.ValidationException
     * @throws tv.snews.anews.exception.SessionManagerException
     * @throws tv.snews.anews.exception.IllegalOperationException
     */
    void copy(Date dateToCopy, Integer programIdTocopy, Long guidelineIdToCopy) throws ValidationException, SessionManagerException,
            IllegalOperationException;

    /**
     * Copia uma pauta de uma outra praça para o banco de dados local.
     *
     * @param remoteGuideline objeto com os dados da pauta remota
     * @param destinyDate data a ser atribuida para a cópia criada
     * @param destinyProgram programa a ser atribuido para a cópia criada
     * @param producer produtor a ser atribuido para a cópia criada
     */
    void copyRemoteGuideline(Guideline remoteGuideline, Date destinyDate, Program destinyProgram, User producer) throws IllegalOperationException;

    Guideline remoteCopyRequest(Long guidelineId, String userNickname, String company);

	/**
	 * Obtem a lista de pautas do dia atual de todos ou por apenas um programa.
	 *
	 * @return List<Guideline> Lista de pautas.
	 * @param currentDay
	 * @param programId
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	List<Guideline> listAllGuidelineOfDay(Date currentDay, Integer programId, DocumentState state) throws ValidationException;

	/**
	 * Verifica se uma pauta está bloqueada para edição, retornando o usuário
	 * que está editando em caso positivo. Caso ninguém esteja editando retorna
	 * <code>null</code>.
	 *
	 * @param guidelineId Pauta que será verificada.
	 * @return Usuário que está editando ou <code>null</code>.
	 */
	User isLockedForEdition(Long guidelineId);

	/**
	 * Bloqueia a edição de uma pauta para o usuário informado.
	 *
	 * @param guidelineId Pauta que será bloqueada.
	 * @param userId Usuário que irá deter o direito de edição.
	 */
	void lockEdition(Long guidelineId, Integer userId) throws IllegalOperationException;

	/**
	 * Faz o desbloqueio de edição de uma pauta. Automaticamente será disparada
	 * uma mensagem notificando o desbloqueio.
	 *
	 * @param guidelineId Pauta que foi liberada para edição.
	 * @throws tv.snews.anews.exception.IllegalOperationException
	 * @throws tv.snews.anews.exception.SessionManagerException
	 */
	void unlockEdition(Long guidelineId) throws IllegalOperationException;

    /**
     * Restora uma pauta que havia sido excluída para as pautas ativas.
     *
     * @param guidelineId ID da pauta que será restaurada
     * @param programToCopyId identificador do programa para a onde a pauta será restaurada.
     * @param guidelineToCopyDate data para a onde a pauta será restaurada.
     *
     * @throws tv.snews.anews.exception.SessionManagerException
     */
    void restoreExcludedGuideline(Long guidelineId, Integer programToCopyId, Date guidelineToCopyDate) throws SessionManagerException;
    
    /**
     * Faz a contagem de todas as pautas marcadas como excluídas que existem.
     * 
     * @return Quantidade de pautas excluídas.
     */
    int countExcludedGuidelines();
    
    /**
     * Retorna uma lista de pautas que foram marcadas como excluídas de forma 
     * páginada, ou seja, retornando os registros a partir da posição indicada 
     * pelo parâmetro <code>firstResult</code> e a quantidade máxima definida 
     * por <code>maxResults</code>.
     * 
     * @param firstResult Posição inicial do primeiro registro da página.
     * @param maxResults Quantidade máxima pode ser retornada.
     * @return Lista de registros de pauta da página.
     */
    List<Guideline> listExcludedGuidelines(int firstResult, int maxResults);
    
    /**
     * Verifica as diferenças entre os atributos de uma pauta sobre a outra.
     * Caso exista alguma diferença em algum atributo, esse método 
     * cria uma marcação HTML das diferenças encontradas em cada atributo.
     * 
     * <p>
     * Exemplo:<br/>
     * &nbsp;&nbsp;&nbsp; guideline1.proposal = "Nova proposta";<br/>
     * &nbsp;&nbsp;&nbsp; guideline2.proposal = "proposta nova";
     * </p>
     * 
     * <p>
     * O objeto Guideline retornado conterá a seguinte marcação no atributo 'proposal':<br/>
     * {@code guideline2.proposal = "<del style=\"background:red;\">Nova</del> proposta <ins style=\"background:green;\">nova</ins>";}
     * </p>
     * 
     * @param guideline1 Pauta original (geralmente a mais recente)
     * @param guideline2 Compara as diferenças dos atributos Strings sobre a guideline1.
     *  
     * @return 
     * 	Objeto Guideline2 com marcações HTML de todas as inserções ou deleções (existe essa palavra?) 
     * 	de cada atributo String da Guideline1.
     */
    Guideline compareDiffBetweenGuidelines(Guideline guideline1, Guideline guideline2);

    /**
	 * Carrega a lista de revisões da pauta, em ordem decrescente de criação.
	 * Serão marcadas as mudanças feitas em cada revisão em relação a revisão
	 * anterior.
	 * 
	 * @param guidelineId identificador da pauta
	 * @return a lista de mudanças feitas
	 */
    Collection<GuidelineRevision> loadChangesHistory(Long guidelineId);
    
    /**
	 * Carrega uma revisão especifica da pauta, a partir do {@code id} da pauta
	 * e do número da revisão.
	 * <p>
	 * Caso o valor de {@code showDiff} seja {@code true}, serão marcadas mas
	 * mudanças feitas em relação a revisão atual e a revisão solicitada.
	 * </p>
	 * 
	 * @param guidelineId identificador da pauta
	 * @param revisionNumber número da revisão
	 * @param showDiff flag para marcação das mudanças
	 * @return a revisão encontrada
	 */
    GuidelineRevision loadRevision(Long guidelineId, int revisionNumber, boolean showDiff);

    PageResult<Guideline> listExcludedGuidelinesPage(int pageNumber);

    PageResult<Guideline> fullTextSearch(GuidelineParams searchParams);

}
