package tv.snews.anews.service;

import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.ReportageParams;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Interface que define o contrado do serviço para manipulação de reportagens.
 * 
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface ReportageService {

    public static final int RESULTS_BY_PAGE = 50;

    Reportage save(Reportage reportage) throws IllegalArgumentException, SessionManagerException, IllegalOperationException;

    /**
	 * Carrega os dados de uma reportagem a partir do seu ID.
	 * 
	 * @param reportageId Identificador da reportagem.
	 * @return Objeto com os dados da reportagem ou {@code null} caso não
	 *         encontre.
	 */
	Reportage loadById(Long reportageId);

    Reportage remoteCopyRequest(Long reportageId, String userNickname, String company);

	void remove(Long reportageId) throws ValidationException;
	
	/**
	 * Altera o status de aprovação da reportagem.
	 * 
	 * @param id identificador da reportagem
	 * @param status status de aprovação
	 */
	void changeOk(Long id, boolean status);

    PageResult<Reportage> listAll(ReportageParams params);
	
	/**
	 * Obtem a lista de reportagens por dia e por programa.
	 * 
	 * @param currentDay
	 * @param program
     * @return List<Reportage> Lista de reportagens.
	 * @throws ValidationException
	 */
	List<Reportage> listAllReportageOfDay(Date currentDay, Program program) throws ValidationException;
	
	/**
	 * Retorna uma lista de reportagens que foram marcadas como excluídas do
	 * reporter logado de forma páginada, ou seja, retornando os registros a
	 * partir da posição indicada pelo parâmetro {@code firstResult} e a
	 * quantidade máxima definida por {@code maxResults}.
	 */
    PageResult<Reportage> listExcludedReportagesPage(int pageNumber) throws SessionManagerException;
    
    PageResult<Reportage> reportageByFullTextSearch(ReportageParams searchParams);
    
    /**
	 * Carrega a lista de revisões da reportagem, em ordem decrescente de
	 * criação. Serão marcadas as mudanças feitas em cada revisão em relação a
	 * revisão anterior.
	 * 
	 * @param reportageId identificador da reportagem
	 * @return a lista de mudanças feitas
	 */
	Collection<ReportageRevision> loadChangesHistory(Long reportageId);
    
	/**
	 * Carrega uma revisão especifica da reportagem, a partir do {@code id} da
	 * reportagem e do número da revisão.
	 * <p>
	 * Caso o valor de {@code showDiff} seja {@code true}, serão marcadas mas
	 * mudanças feitas em relação a revisão atual e a revisão solicitada.
	 * </p>
	 * 
	 * @param reportageId identificador da reportagem
	 * @param revisionNumber número da revisão
	 * @param showDiff flag para marcação das mudanças
	 * @return a revisão encontrada
	 */
    ReportageRevision loadRevision(Long reportageId, int revisionNumber, boolean showDiff);

    void restoreExcluded(Long storyId, Date date, int programId);
    
    User isLockedForEdition(Long reportageId);

    void lockEdition(Long reportageId, User user) throws IllegalOperationException;

    void unlockEdition(Long reportageId) throws IllegalOperationException;
    
    void sendReportageMail(Long reportageId) throws ValidationException, SessionManagerException;

    void copyReportage(Long reportageId, Date date, Program program) throws IllegalOperationException, CloneNotSupportedException;

    /**
     * Cria uma cópia de uma reportagem vinda de outra praça e salva no banco de
     * dados local. A data, programa e repórter serão alterados para os valores
     * informados via parâmetros.
     *
     * @param remoteReportage dados completos da reportagem remota
     * @param destinyDate data de destino para a reportagem
     * @param destinyProgram programa de destino para a reportagem
     * @param reporter repórter a ser atribuido à reportagem
     */
	void copyRemoteReportage(Reportage remoteReportage, Date destinyDate, Program destinyProgram, User reporter) throws IllegalOperationException, CloneNotSupportedException;

	void updateState(Long guidelineId, DocumentState state, String reason);

    void registerProgram(Reportage reportage, Program program);

    void unregisterProgram(Reportage reportage, Program program);
}
