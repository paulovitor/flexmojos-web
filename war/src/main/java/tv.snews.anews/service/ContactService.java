package tv.snews.anews.service;

import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.exception.ValidationException;

import java.io.Serializable;
import java.util.List;

/**
 * @author Paulo Felipe
 * @author Samuel
 * @since 1.0.0
 */
public interface ContactService {

	int countAll();

	List<AgendaContact> findAll(int firstResult, int maxResult);

	int countByName(String name);

	List<AgendaContact> findByName(String name, int firstResult, int maxResult);

	Contact save(Contact contact) throws ValidationException;

	AgendaContact save(AgendaContact contact) throws ValidationException;

	void delete(Contact contact);

	Contact get(Serializable idContact);

	AgendaContact findAgendaContactById(Integer idEntity);
}
