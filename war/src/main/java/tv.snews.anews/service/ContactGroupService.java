package tv.snews.anews.service;

import tv.snews.anews.domain.ContactGroup;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;

import java.util.List;

/**
 * Interface de serviço de manipulação do grupo de contato.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
public interface ContactGroupService {

	/**
	 * Lista todos o grupos de contatos.
	 * 
	 * @return List<ContactGroup>
	 */
	List<ContactGroup> listAll();
	
	/**
	 * Exclui o grupo de contato.
	 * 
	 * @param id
	 */
	void deleteById(Integer id) throws IllegalOperationException;

	/**
	 * Salva o grupo de contato.
	 * 
	 * @param contactGroup
	 * @throws ValidationException 
	 */
	void save(ContactGroup contactGroup) throws ValidationException;

    /**
     * Procura por um grupo de contato cadastrado com o nome informado. Caso não
     * encontre cadastra um novo grupo com o nome informado.
     *
     * @param name nome do grupo procurado
     * @return o grupo encontrado ou cadastrado
     */
    ContactGroup findOrCreateByName(String name);
}
