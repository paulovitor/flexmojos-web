package tv.snews.anews.service;

import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.domain.ScriptPlanStatus;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.intelligentinterface.exception.TelnetException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface ScriptPlanService {

	enum Field { DATE, START, END, PRODUCTION, SLUG }

	ScriptPlan load(Date date, Integer programId);

	ScriptPlan create(Date date, Integer programId) throws IOException;

	void updateField(Long planId, String field, Object value);

	void renumberPages(Long planId);

	ScriptPlanStatus verifyScriptPlan(Date date, Integer programId) throws ValidationException;

	void sortScripts(Long planId);

	void lockScriptPlan(Long planId);

	void unlockScriptPlan(Long planId);

    /**
     * Envia todos os créditos dos scripts aprovados do roteiro informado.
     *
     * @param scriptPlanId roteiro cujos créditos serão enviados
     * @throws tv.snews.anews.exception.SessionManagerException
     * @throws tv.snews.intelligentinterface.exception.TelnetException
     * @throws java.io.IOException
     */
    String sendCredits(Long scriptPlanId) throws SessionManagerException, IOException, TelnetException;

    void resetScriptPlanExhibition(Long scriptPlanId) throws IllegalOperationException;

    void completeScriptPlanExhibition(Long scriptPlanId) throws IllegalOperationException;

    void synchronizeStatusVideosWS(Long scriptPlanId) throws RemoteException, ServiceException, WebServiceException;
}
