
package tv.snews.anews.service;

import tv.snews.anews.domain.StoryKind;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Fornece os serviços para manipulação dos tipos da lauda.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public interface StoryKindService {

	/**
	 * Faz o cadastro de um novo tipo de lauda informado pelo usuário. Se já existir
	 * um tipo com o mesmo Nome nao grava.
	 * 
	 * @param storyKind
	 */
	void save(StoryKind storyKind);

	/**
	 * Deleta o tipo da lauda.
	 * 
	 * @param id identificador do tipo
	 */
	void delete(Long id) throws IllegalOperationException;

	/**
	 * Retorna a lista de todos os @{link StoryKind} cadastrados em ordem
	 * alfabética.
	 * 
	 * @return Lista de tipos de lauda cadastrados.
	 */
	List<StoryKind> listAll();
	
	/**
	 * Retorna a lista de todos os @{link StoryKind} cadastrados em ordem
	 * alfabética pelo sigla.
	 * 
	 * @return Lista de tipos de lauda cadastrados.
	 */
	List<StoryKind> listAllOrderAcronym();

	/**
	 * Verifica se um StoryKind esta sendo usado em alguma lauda.
	 * 
     * @param id
     * @return
     */
    Boolean verifyDeleteKind(Long id);

    /**
     * Procura por um {@link StoryKind} cadastrado com o nome ou abreviação do
     * tipo informado. Caso não encontre faz o cadastro.
     *
     * @param kind nome do tipo procurado
     * @return o objeto do tipo encontrado ou cadastrado
     */
    StoryKind findOrCreateReplacement(StoryKind kind) throws CloneNotSupportedException;
}
