package tv.snews.anews.service;

import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.MessageDeliveryException;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Interface do serviço responsável pelas ações diretamente ligadas a classe de
 * domínio {@link Chat}.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ChatService {

	/**
	 * Apenas busca pelo objeto Chat que possui o ID informado.
	 * 
	 * @param id Identificador único do chat.
	 * @return Objeto {@link Chat} ou <code>null</code> caso não encontre.
	 */
	Chat loadChat(Long id);
	
	/**
	 * Cria um novo chat para os usuários informados. Caso já exista um chat
	 * criado não se tem a necessidade de criar um novo chat, retornado então o
	 * que já existe.
	 * 
	 * @param members Participantes da conversa.
	 * @return Objeto {@link Chat} com os dados do chat.
	 */
	Chat createChat(Set<User> members);
	
	/**
	 * Procura por um chat existente que possua os usuários informados como
	 * participantes da conversa. Será retornado apenas o chat que possue
	 * exatamente aqueles usuários, não aceitando os que possuem algum usuário a
	 * mais. Caso nenhum chat seja encontrado será retornado <code>null</code>.
	 * 
	 * @param members Participantes da conversa.
	 * @return Objeto {@link Chat} ou <code>null</code>.
	 */
	Chat findChat(Set<User> members);

	/**
	 * Adiciona um ou mais usuários a lista de participantes do chat.
	 * 
	 * @param members Novos membros.
	 * @param chat Chat no qual os usuários ingressaram.
	 */
	void addMembers(Chat chat, User... newMembers) throws MessageDeliveryException;

	/**
	 * Registra o ato de um usuário abandonar uma conversa em grupo. Nesse caso
	 * ele não receberá novas mensagens até que ele seja convidado novamente
	 * para a conversa.
	 * 
	 * @param chat
	 * @param user
	 */
	void registerLeave(Chat chat, User user) throws MessageDeliveryException;
	
	/**
	 * Retorna a lista total de chats.
	 * 
	 * @return Lista de objetos {@link Chat}
	 */
	List<Chat> listAll(int owner);
	
	/**
	 * Retorna a lista total de chats.
	 * 
	 * @return Lista de objetos {@link Chat}
	 */
	List<Chat> listByDate(Date date, int owner);
	
}
