package tv.snews.anews.service;

import tv.snews.anews.domain.*;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface DeviceService {

	<T extends Device> T findById(Integer id);

	List<? extends Device> findAll();

	List<? extends Device> findAllByProtocol(DeviceProtocol protocol);

	List<? extends MosDevice> findAllMosDeviceByType(DeviceType type);

	List<? extends Device> findAllByType(DeviceType type);

	<T extends Device> T save(T device) throws IllegalOperationException;

	<T extends Device> void remove(T device) throws IllegalOperationException;

	boolean checkPathInUse(IIDevicePath path);

    public String checkChannelsInUse(Device device);
}
