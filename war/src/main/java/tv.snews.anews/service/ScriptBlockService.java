package tv.snews.anews.service;

import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.ScriptBlockStatus;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.ValidationException;

import java.io.IOException;
import java.util.Date;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface ScriptBlockService {

	enum Field { COMMERCIAL, SLUG };

	ScriptBlock create(Long blockId);

	void updateField(Long blockId, Field field, Object value);

	void remove(Long blockId) throws IllegalOperationException;

    ScriptBlockStatus synchronizeStandByBlock(Date currentDate, Date targetDate, Integer programId) throws ValidationException, IOException;

    void updatePosition(Long blockId, int order, Boolean notifyMove);
}