
package tv.snews.anews.service;

import tv.snews.anews.domain.Segment;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Fornece os serviços para manipulação dos segmentos.
 * 
 * @author Paulo Felipe.
 * @since 1.0.0
 */
public interface SegmentService {

	/**
	 * Faz o cadastro de um novo segmento informado pelo usuário. Se já existir
	 * um
	 * segmento com o mesmo Nome nao grava.
	 * 
	 * @param segment
	 */
	void save(Segment segment);

	/**
	 * Deleta o segmento.
	 * 
	 * @param segment
	 */
	void delete(Integer id) throws IllegalOperationException;

	/**
	 * Retorna a lista de todos os @{link Segment} cadastrados em ordem
	 * alfabética.
	 * 
	 * @return Lista de segmentos cadastrados.
	 */
	List<Segment> listAll();

}
