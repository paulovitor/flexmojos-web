package tv.snews.anews.service;

import tv.snews.anews.domain.GuidelineClassification;
import tv.snews.anews.exception.IllegalOperationException;

import java.util.List;

/**
 * Define o contrato do serviço que manipula objetos {@link tv.snews.anews.domain.GuidelineClassification}.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public interface GuidelineClassificationService {

	/**
	 * Busca e retorna a lista de todas as classificações cadastradas
	 * pelos usuários do sistema.
	 * 
	 * @return as classificações cadastradas
	 */
	List<GuidelineClassification> listAll();
	
	/**
	 * Cadastra ou atualiza os dados de uma classificação informada pelo usuário.
	 * 
	 * @param classification dados da classificação
	 */
	void save(GuidelineClassification classification);
	
	/**
	 * Apaga no banco de dados os dados de uma classificação cadastrada.
	 * 
	 * @param id identificador do registro da classificação
	 * @throws tv.snews.anews.exception.IllegalOperationException se o recurso estiver em uso por alguma pauta.
	 */
	void remove(Integer id) throws IllegalOperationException;
	
	/**
	 * Verifica se uma classificação está associado a alguma pauta.
	 * 
	 * @param id identificador do registro da classificação
	 * @return {@code true} se  classificação estiver associado a alguma pauta
	 */
	boolean isInUse(Integer id);

	boolean isInUse(GuidelineClassification classification);
}
