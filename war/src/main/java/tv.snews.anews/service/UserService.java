package tv.snews.anews.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import tv.snews.anews.domain.Group;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.DataNotFoundException;
import tv.snews.anews.exception.InvalidEmailException;
import tv.snews.anews.exception.InvalidPasswordException;
import tv.snews.anews.exception.SessionManagerException;

import java.util.List;

/**
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface UserService extends UserDetailsService {

	/**
	 * Carrega o objeto do usuário a partir de seu email e senha. Caso não
	 * encontre um usuário com as credenciais informadas, será retornado
	 * {@code null}.
	 */
	User loadByCredentials(String email, String password);

	/**
	 * Recupera senha da base de dados enviando-a para o email do usuário.
	 */
	void recoveryPassword(String email) throws DataNotFoundException, InvalidEmailException, SessionManagerException;

	/**
	 * Alteração de senha.
	 */
	User changePassword(String email, String oldPassword, String newPassword) throws DataNotFoundException, InvalidPasswordException, InvalidEmailException;

	/**
	 * Retorna a lista de usuários de um determinado grupo.
	 */
	List<User> listUsersByGroupAndStatus(Group group) throws SessionManagerException;

	/**
	 * Retorna a lista de usuários que tem o grupo marcado para exibir como reporter na pauta.
	 */
	List<User> listReporters();

	/**
	 * Retorna a lista de usuários que tem o grupo marcado para exibir como produtor na pauta.
	 */
	List<User> listProducers();

    /**
     * Retorna a lista de usuários que tem o grupo marcado para exibir como produtor na produção.
     */
    List<User> listChecklistProducers();

	/**
	 * Salva ou efetua update em usuário.
	 */
	String save(User user) throws InvalidEmailException, SessionManagerException;

	/**
	 * Deleta usuário pelo identificador.
	 */
	void delete(User user);

	/**
	 * Disconecta um usuário da sessão.
	 */
	void disconnectUser(User user);

	/**
	 * Força o usuário à aceitar novamente o termo.
	 */
	void requestUserToAgreeWithTerms(int userId);

	String saveData(User user, String password, String newPassword, String confirmation) throws InvalidEmailException, SessionManagerException;

	/**
	 * Remove todos os bloqueios de edição ligados ao usuário informado.
	 */
	void releaseLocksFromUser(User user);

	List<User> listAll();

	List<User> listAll(String searchTerm);

	List<User> listEditors();

    List<User> listByGroupName(String searchTerm, int groupId, Boolean belongsToTheGroup);

    List<User> listByTeamName(String searchTerm, int teamId, Boolean belongsToTheTeam);

    void collectMenu(int userId, boolean value);

}
