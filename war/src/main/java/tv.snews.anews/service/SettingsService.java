package tv.snews.anews.service;

import tv.snews.anews.domain.Settings;
import tv.snews.anews.exception.MessageDeliveryException;
import tv.snews.anews.infra.mail.MailConfig;

import java.io.IOException;

/**
 * @author Eliezer Reis
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface SettingsService {

    /**
     * Carrega as configurações de idioma do sistema.
     * 
     */
	String loadCurrentLocale();
	
    /**
     * Carrega as configurações do sistema.
     * 
     */
	Settings loadSettings();
	
	void updateSettings(Settings settings) throws MessageDeliveryException;
	
	/**
	 * Carrega as configuração de envio de email. Caso não tenha sido definida
	 * nenhuma configuração, retorna {@code null}.
	 * 
	 * @return o objeto com as configurações ou {@code null} caso não existam
	 */
	MailConfig loadMailConfig();
	
	/**
	 * Salva ou atualiza as configurações de envio de email.
	 * 
	 * @param config objeto com as configurações
	 */
	void saveMailConfig(MailConfig config);

	/**
     * Reindexa a base de dados do sistema.
     * 
     */
    void reindexDatabase();
    
    /**
     * Carrega as imagens do sistema.
     * 
     */
    byte[] loadImg(String pathImg) throws IOException;
}
