
package tv.snews.anews.service;

import tv.snews.anews.domain.NumberType;

import java.util.List;

/**
 * @author Paulo Felipe
 * @author Samuel
 * @since 1.0.0
 */
public interface NumberTypeService {

	/**
	 * Retorna a lista geral de tipo de numeros.
	 */
	List<NumberType> listAll();

	/**
	 * Salva ou efetua update em tipo de numero.
	 * 
	 * @param numberType
	 */
	void save(NumberType numberType);

	/**
	 * Deleta tipo de numero pelo identificador.
	 * 
	 * @param ids
	 */
	void delete(NumberType numberType);

}
