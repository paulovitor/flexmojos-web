package tv.snews.anews.service;

import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.RundownStatus;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.exception.WebServiceException;
import tv.snews.intelligentinterface.exception.TelnetException;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

/**
 * Interface que define o contrado do serviço para manipulação dos espelhos.
 *
 * @author Paulo Felipe
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface RundownService {

	/**
	 * Cria um novo espelho para o dia e programa informados
	 *
	 * @param date       data do espelho
	 * @param programId  id do programa
	 * @param templateId template do espelho
	 * @return objeto com os dados do espelho criado
	 */
	Rundown createRundown(Date date, Integer programId, Integer templateId) throws ValidationException;

	/**
	 * Retorna o espelho do dia e programa informados. Caso não encontre será
	 * retornado {@code null}.
	 *
	 * @param date      data do espelho
	 * @param programId id do programa
	 * @param fetchAll  se {@code true} carrega todos dos dados das laudas
	 * @return o espelho encontrado ou {@code null}
	 * @throws tv.snews.anews.exception.ValidationException se a data for nula ou o programa não for encontrado
	 */
	Rundown loadRundown(Date date, Integer programId, boolean fetchAll) throws ValidationException;

	/**
	 * Verifica o rundown.
	 *
	 * @return Rundown
	 */
	RundownStatus verifyRundown(Date date, Integer programId) throws ValidationException;

	/**
	 * Retorna a data e a hora atual.
	 *
	 * @return Date
	 */
	Date currentTime();

	/**
	 * Atualiza os campos do espelho.
	 *
	 * @return Rundown
	 */
	void updateField(Long rundownId, String field, Object object) throws ValidationException;

	/**
	 * Reordena as laudas.
	 *
	 * @param rundownId
	 */
	void sortStories(Long rundownId);

	/**
	 * Carrega os dados de todo o conteúdo do espelho e gera o texto no formato
	 * HTML que deve ser apresentado no Teleprompter.
	 *
	 * @param rundownId ID do espelho
	 * @return o texto HTML gerado
	 */
	String generateTeleprompterText(Long rundownId);

	/**
	 * Envia todos os créditos das laudas aprovadas do espelho informado.
	 *
	 * @param rundownId espelho cujos créditos serão enviados
	 * @throws tv.snews.anews.exception.SessionManagerException
	 * @throws tv.snews.intelligentinterface.exception.TelnetException
	 * @throws java.io.IOException
	 */
	void sendCredits(Long rundownId) throws SessionManagerException, IOException, TelnetException;

	/**
	 * Conta a quantidade total de espelhos. Usado na área de auditoria.
	 *
	 * @return o total de espelhos cadastrados
	 */
	int totalRundowns();

	/**
	 * Faz a listagem paginada dos espelhos cadastrados. Usado na área de
	 * auditoria.
	 *
	 * @param firstResult ponto inicial da listagem
	 * @param maxResults  quantidade máxima de registros a serem retornados
	 * @return a lista de registros encontrados de acordo com os parâmetros
	 * informados para a busca
	 */
	List<Rundown> listRundowns(Integer firstResult, Integer maxResults);

	void resetDisplayOfStories(Long rundownId) throws IllegalOperationException;

	void completeStoriesDisplay(Long rundownId) throws IllegalOperationException;

	List<Integer> loadAssetsIds(Long rundownId) throws RemoteException, ServiceException, WebServiceException;

	void synchronizeStatusVideosWS(Long rundownId) throws RemoteException, ServiceException, WebServiceException;
}
