
package tv.snews.anews.service;

import tv.snews.anews.domain.State;
import tv.snews.anews.exception.SessionManagerException;

import java.io.Serializable;
import java.util.List;

/**
 * Interface do serviço de manipulação dos estados.
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface StateService {

	/**
	 * Lista todos os estados.
	 * 
	 * @return List<State>
	 */
	List<State> listAll();

	/**
	 * Consulta stado pelo identificador.
	 * 
	 * @param idState
	 * @return State
	 */
	State get(Serializable idState);
	
	/**
	 * Exclui o estado.
	 * 
	 * @param State
	 */
	void remove(State state);

	/**
	 * Salva o estado.
	 * 
	 * @param State
	 */
	void save(State state) throws SessionManagerException;
	
}
