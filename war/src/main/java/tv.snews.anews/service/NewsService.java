
package tv.snews.anews.service;

import tv.snews.anews.domain.News;
import tv.snews.anews.domain.User;
import tv.snews.anews.exception.IllegalOperationException;
import tv.snews.anews.exception.SessionManagerException;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.NewsParams;

import java.io.Serializable;
import java.util.List;

/**
 * @author Paulo/Samuel
 * @since 1.0.0
 */
public interface NewsService {

    /**
     * @param newsId
     * @return
     */
    News loadById(Long newsId);

    /**
     * Lista todas notícias.
     *
     * @return List<News>
     */
    List<News> listAll();

    List<News> listFirstTwenty();

    /**
     * Busca paginada
     *
     * @param pageNumber
     * @return
     */
    PageResult<News> listAllPage(int pageNumber);

    PageResult<News> findByParams(NewsParams params);

    /**
     * Consulta Notícia pelo identificador.
     *
     * @param idNews
     * @return News
     */
    News get(Serializable idNews);

    /**
     * Exclui a notícia.
     *
     * @param news
     * @throws tv.snews.anews.exception.ValidationException
     */
    void delete(News news) throws ValidationException;

    /**
     * Salva a notícia.
     *
     * @param news
     * @throws tv.snews.anews.exception.IllegalOperationException
     * @throws tv.snews.anews.exception.ValidationException
     */
    void save(News news) throws SessionManagerException, ValidationException, IllegalOperationException;

	/**
     * @param newsId
     * @throws tv.snews.anews.exception.IllegalOperationException
     */
    void unlockEdition(Long newsId) throws IllegalOperationException;

	/**
     * @param newsId
     * @param userId
     * @throws tv.snews.anews.exception.IllegalOperationException
     */
    void lockEdition(Long newsId, Integer userId) throws IllegalOperationException;

	/**
     * @param newsId
     * @return
     */
    User isLockedForEdition(Long newsId);
}
