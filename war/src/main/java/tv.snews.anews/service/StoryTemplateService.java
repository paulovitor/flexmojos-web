package tv.snews.anews.service;

import tv.snews.anews.domain.StoryTemplate;

/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
public interface StoryTemplateService {

    StoryTemplate load(Integer storyId);

    StoryTemplate createStory(Integer blockId);

    StoryTemplate updateStory(StoryTemplate story);

    boolean updateField(Integer storyId, String fieldName, Object value);

    void updatePosition(Integer storyId, Integer blockId, int order);

    void removeStory(Integer storyId);

}
