package tv.snews.anews.service;

import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.exception.ValidationException;
import tv.snews.anews.infra.rss.FeedRetriveException;

import java.util.List;

/**
 * Define os métodos de serviço que serão utilizados para prover ao usuário o
 * acesso aos feeds RSS e seus conteúdos.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssService {

	/**
	 * Faz o cadastro de um novo feed informado pelo usuário. Se já existir um 
	 * feed com a mesma URL, apenas atualiza o seu nome.
	 * 
	 * @param feed
	 * @throws tv.snews.anews.infra.rss.FeedRetriveException
	 * @return O ID do feed.
	 * @throws tv.snews.anews.exception.ValidationException
	 */
	Integer saveRssFeed(RssFeed feed) throws FeedRetriveException, ValidationException;

	/**
	 * Remove o cadastro de um {@link tv.snews.anews.domain.RssFeed} e todos os itens salvos.
	 *
	 * @param feed Registro que será apagado.
	 */
	void deleteRssFeed(RssFeed feed);

	/**
	 * Retorna a lista de todos os @{link RssFeed} cadastrados em ordem
	 * alfabética.
	 *
	 * @return Lista de feeds cadastrados.
	 */
	List<RssFeed> listFeeds();

	/**
	 * Retorna o total de itens atualmente armazenados pelo sistema, de todos os
	 * feeds.
	 *
	 * @return Quantidade encontrada.
	 */
	int countItemsFromAllFeeds();

	/**
	 * Retorna o total de itens atualmente armazendados para todos os canais que
	 * pertencem à categoria informada.
	 *
	 * @param category Categoria desejada
	 * @return Total de itens encontrados dentros dos canais da categoria
	 *         informada.
	 */
	int countItemsFromCategory(RssCategory category);

	/**
	 * Retorna o total de itens atualmente armazenados pelo sistema de um feed
	 * em específico.
	 *
	 * @param feed Feed RSS desejado.
	 * @return Quantidade encontrada.
	 */
	int countItemsFromFeed(RssFeed feed);

	/**
	 * Permite carregar de forma paginada os itens de todos os feeds
	 * cadastrados.
	 *
	 * @param start Posição do primeiro registro a ser retornado.
	 * @param count Quantidade de registros a serem retornados.
	 * @return Lista de registros encontrados.
	 */
	List<RssItem> listItemsFromAllFeeds(int start, int count);

	/**
	 * Permite carregar de forma paginada os itens de todos os feeds da
	 * categoria informada.
	 *
	 * @param category Categoria desejada.
	 * @param start Posição do primeiro registro a ser retornado.
	 * @param count Quantidade de registros a serem retornados.
	 * @return Lista de registros encontrados.
	 */
	List<RssItem> listItemsFromCategory(RssCategory category, int start, int count);

	/**
	 * Permite carregar de forma paginada os itens de um feed em específico.
	 *
	 * @param feed {@link tv.snews.anews.domain.RssFeed} pai.
	 * @param start Posição do primeiro registro a ser retornado.
	 * @param count Quantidade de registros a serem retornados.
	 * @return Lista de registros encontrados.
	 */
	List<RssItem> listItemsFromFeed(RssFeed feed, int start, int count);
	
	/**
	 * Retorna o RssItem pelo identificador
	 * 
     * @param idEntity
     * @return RssItem
     */
	RssItem findRssItemById(Long idEntity);
}
