package tv.snews.anews.messenger;

import tv.snews.anews.domain.Contact;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ContactMessenger {

	/**
	 * Notifica o cadastro de um novo contato.
	 * 
	 * @param contact Dados do contato cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyContactInserted(Contact contact);
	
	/**
	 * Notifica a atualização de um contato.
	 * 
	 * @param contact Dados do contato atualizado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyContactUpdated(Contact contact);
	
	/**
	 * Notifica a exclusão de um contato.
	 * 
	 * @param contact Dados do contato excluido.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyContactDeleted(Contact contact);
}
