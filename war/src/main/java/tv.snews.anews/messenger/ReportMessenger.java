package tv.snews.anews.messenger;

import tv.snews.anews.domain.Report;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * aos relatórios.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportMessenger {

	/**
	 * Notifica quando um relatório é cadastrado no sistema.
	 * 
	 * @param report relatório que foi cadastrado
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyInsert(Report report);
	
	/**
	 * Notifica quando um relatório é atualizado.
	 * 
	 * @param report relatório que foi atualizado
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdate(Report report);
	
	/**
	 * Notifica quando um relatório é excluído.
	 * 
	 * @param reporto relatório que foi excluído
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyDelete(Report report);
	
	/**
	 * Notifica quando um report é bloqueado para edição.
	 * 
	 * @param guideline Pauta que foi bloqueada.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyLock(Report report, User user);

	/**
	 * Notifica quando uma pauta é desbloqueada para edição.
	 * 
	 * @param guideline Pauta que foi desbloqueada.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyUnlock(Report report, User user);
}