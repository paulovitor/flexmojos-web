package tv.snews.anews.messenger;

import tv.snews.anews.domain.Program;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * aos programas.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ProgramMessenger {

	/**
	 * Notifica quando um programa é cadastrado no sistema.
	 * 
	 * @param program O Programa que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Program program);
	
	/**
	 * Notifica quando um programa é atualizado.
	 * 
	 * @param program O programa que foi atualizado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Program program);
	
	/**
	 * Notifica quando um programa é excluído.
	 * 
	 * @param program O programa que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Program program);
}
