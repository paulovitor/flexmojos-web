package tv.snews.anews.messenger;

import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.User;

import java.util.Collection;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao sistema de scripts.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface ScriptMessenger {

	/**
	 * Notifica quando um script é cadastrado no sistema.
	 */
	boolean notifyInsert(Script script);

	/**
	 * Notifica quando uma campo do script é atualizado.
	 */
	boolean notifyUpdate(Script script);

    /**
     * Notifica quando um script é atualizado no sistema.
     *
     * @param script Script que foi atualizado.
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyUpdate(Script script, long scriptPlanId);

    /**
     * Notifica quando um script é cadastrado no sistema.
     *
     * @param script Script que foi cadastrado.
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyInsert(Script script, long scriptPlanId);

    /**
	 * Notifica quando uma campo do script é atualizado.
	 */
	boolean notifyUpdateField(long scriptId, String field, Object value);

	/**
	 * Notifica quando um script é excluído.
	 */
	boolean notifyDelete(Script script);

    /**
     * Notifica quando um script é excluído no sistema.
     *
     * @param script Script que foi excluído.
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyDelete(Script script, long scriptPlanId);

    boolean notifyMultipleExclusion(Long[] ids, Long scriptPlanId);

	/**
	 * Notifica quando um script é movido.
	 */
	boolean notifyMove(Script script, User user, boolean dragNDrop);

	boolean notifyMove(Collection<Script> scripts, User user);

    boolean notifyMove(Collection<Script> scripts, long scriptPlanId);

	/**
	 * Notifica quando um script é enviado ou retirado da gaveta.
	 */
	boolean notifyDrawerChange(Script script);

	/**
	 * Notifica quando um script é restaurado da lixeira para um roteiro.
	 */
	boolean notifyRestore(Script script);

	boolean notifyLock(Script script, User user);

    boolean notifyUnlock(Script script);
}
