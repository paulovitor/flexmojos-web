package tv.snews.anews.messenger;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao sistema da gaveta.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface DocumentMessenger {

	/**
	 * Notifica quando é enviado documentos para uma gaveta no sistema.
	 */
	boolean notifySentToDrawer(Long[] documentIds, Object obj);


}
