package tv.snews.anews.messenger;

import tv.snews.anews.domain.Institution;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * ao sistema de institutions.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface InstitutionMessenger {

	/**
	 * Notifica quando uma institution é cadastrado no sistema.
	 * 
	 * @param institution O Institutiono que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Institution institution);

	boolean notifyUpdate(Institution institution);

	/**
	 * Notifica quando uma institution é excluído.
	 * 
	 * @param institution O institutiono que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Institution institution);
}
