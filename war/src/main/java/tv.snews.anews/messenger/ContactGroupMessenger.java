package tv.snews.anews.messenger;

import tv.snews.anews.domain.ContactGroup;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao sistema de grupos de contatos.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ContactGroupMessenger {

	/**
	 * Notifica quando um grupo de contatos é cadastrado no sistema.
	 * 
	 * @param contactGroup O grupo de conta.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(ContactGroup contactGroup);
	
	/**
	 * Notifica quando um grupo de contatos é atualizado.
	 * 
	 * @param guideline O grupo que foi atualizado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(ContactGroup contactGroup);

	/**
	 * Notifica quando um grupo de contato é excluído.
	 * 
	 * @param guideline O grupo que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(ContactGroup contactGroup);
}
