package tv.snews.anews.messenger;

import tv.snews.anews.domain.GuidelineClassification;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas
 * as classificações da pauta.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public interface GuidelineClassificationMessenger {

	/**
	 * Notifica quando uma classificação é cadastrada no sistema.
	 * 
	 * @param guidelineClassification classificação que foi cadastrada
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyInsert(GuidelineClassification guidelineClassification);

	/**
	 * Notifica quando uma classificação é atualizada.
	 * 
	 * @param guidelineClassification classificação que foi atualizada
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdate(GuidelineClassification guidelineClassification);

	/**
	 * Notifica quando uma classificação é excluída.
	 * 
	 * @param guidelineClassification classificação que foi excluída
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyDelete(GuidelineClassification guidelineClassification);
}
