package tv.snews.anews.messenger;

import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;

/**
 * Interface que define as ações necessárias para as notificações relacionadas 
 * ao sistema de visualização de canais RSS.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface RssMessenger {

	/**
	 * Notifica quando um novo {@link RssFeed} é cadastrado.
	 * 
	 * @param feed Objeto com os dados do novo feed.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyFeedInserted(RssFeed feed);
	
	/**
	 * Notifica quando um {@link RssFeed} é atualizado.
	 * 
	 * @param feed Objeto com os dados do feed atualizado.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyFeedUpdated(RssFeed feed);
	
	/**
	 * Notifica quando um {@link RssFeed} é removido.
	 * 
	 * @param feed Objeto com os dados do feed removido.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyFeedDeleted(RssFeed feed);
	
	/**
	 * Notifica a criação de uma nova categoria de feeds RSS.
	 * 
	 * @param category Categoria que foi cadastrada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyCategoryInserted(RssCategory category);
	
	/**
	 * Notifica a atualização de uma categoria de feeds RSS.
	 * 
	 * @param category Categoria que foi atualizada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyCategoryUpdated(RssCategory category);
	
	/**
	 * Notifica a exclusão de uma categoria.
	 * 
	 * @param category Categoria que foi excluída.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyCategoryDeleted(RssCategory category);
}
