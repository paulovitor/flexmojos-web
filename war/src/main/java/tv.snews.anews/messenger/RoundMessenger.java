package tv.snews.anews.messenger;

import tv.snews.anews.domain.Round;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * ao sistema de rounds.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface RoundMessenger {

	/**
	 * Notifica quando uma round é cadastrado no sistema.
	 * 
	 * @param round O Roundo que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Round round);
	
	/**
	 * Notifica quando uma round é atualizada no sistema.
	 * 
	 * @param round O Roundo que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Round round);
	
	/**
	 * Notifica quando uma round é excluído.
	 * 
	 * @param round O roundo que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Round round);

	/**
     * @param round
     * @param user
	 * @return 
     */
    boolean notifyLock(Round round, User user);

	/**
     * @param round
     * @param user
	 * @return 
     */
    boolean notifyUnlock(Round round, User user);
}
