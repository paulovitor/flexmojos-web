package tv.snews.anews.messenger;

import tv.snews.anews.domain.ChecklistType;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public interface ChecklistTypeMessenger {

    boolean notifyInsert(ChecklistType event);

    boolean notifyUpdate(ChecklistType event);

    boolean notifyDelete(ChecklistType event);
}
