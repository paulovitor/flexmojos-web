package tv.snews.anews.messenger;

import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.User;

import java.util.Collection;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * ao sistema de laudas.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface StoryMessenger {
	
	/**
	 * Notifica quando uma campo da lauda é atualizado.
	 * 
	 * @param storyId Id da lauda.
	 * @param field O campo que foi atualizado
	 * @param value O conteúdo do campo que foi atualizado
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdateField(long storyId, String field, Object value, long rundownId);
	
	/**
	 * Notifica quando uma story é cadastrado no sistema.
	 * 
	 * @param story Story que foi cadastrado.
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyInsert(Story story, long rundownId);
	
	/**
	 * Notifica quando uma story é cadastrado no sistema.
	 * 
	 * @param story Story que foi cadastrado.
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdate(Story story, long rundownId);
	
	/**
	 * Notifica quando uma story é excluído.
	 * 
	 * @param story story que foi excluído.
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyDelete(Story story, long rundownId);

	/**
	 * Notifica quando uma story é movido.
	 * 
	 * @param story story que foi excluído.
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyMove(Story story, long rundownId);

	boolean notifyMove(Collection<Story> stories, long rundownId);

	/**
	 * Notifica quando uma lauda está sendo editada por um usuário e foi
	 * bloqueada para edição.
	 * 
	 * @param story lauda que está sendo editada
	 * @param user usuário que está efetuando a edição
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyLock(Story story, User user, long rundownId);
	
	/**
	 * Notifica quando um usuário encerra a edição de uma lauda, e o bloqueio de
	 * edição foi liberado.
	 * 
     * @param story lauda que foi liberada para edição
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyUnlock(Story story, long rundownId);

    /**
	 * Notifica quando uma lauda é enviada ou retirada da gaveta.
	 * 
     * @param story lauda que foi enviada ou retirada da gaveta
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyDrawerChange(Story story);
    
    /**
     * Notifica quando uma lauda é restaurada da lixeira para um espelho.
     * 
     * @param story lauda que foi restaurada
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyRestore(Story story, long rundownId);
    
    /**
	 * Notifica quando uma lauda está sendo arrastada por um usuário e foi
	 * bloqueada para edição.
	 *
	 * @param story lauda que está sendo editada
	 * @param user usuário que está efetuando a edição
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyLockDragging(Story story, User user, long rundownId);

	/**
	 * Notifica quando um usuário encerra a drag de uma lauda, e o bloqueio de
	 * edição foi liberado.
	 *
     * @param story lauda que foi liberada para edição
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyUnlockDragging(Story story, long rundownId);

    boolean notifyMultipleExclusion(Long[] ids, long rundownId);
}
