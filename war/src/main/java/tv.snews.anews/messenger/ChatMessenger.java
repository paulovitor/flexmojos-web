package tv.snews.anews.messenger;

import tv.snews.anews.domain.AbstractMessage;
import tv.snews.anews.domain.Chat;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para o envio/notificação do sistema
 * de chat.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ChatMessenger {

	/**
	 * Faz o envio de uma mensagem do chat para seus destinatários.
	 * 
	 * @param message Mensagem do chat.
	 * @return <code>true</code> se a mensagem foi enviada com sucesso.
	 */
	boolean sendChatMessage(AbstractMessage message);
	
	/**
	 * Notifica a entrada de um ou mais membros na conversa de um chat.
	 * 
	 * @param chat Chat onde o usuários ingressaram.
	 * @param members Membros que entraram na conversa.
	 * @return <code>true</code> se a mensagem foi enviada com sucesso.
	 */
	boolean notifyMembersJoined(Chat chat, User... members);
	
	/**
	 * Notifica a saída de um usuário da conversa do chat.
	 * 
	 * @param chat Chat de onde o usuário saiu.
	 * @param user Membro que saiu da conversa.
	 * @return <code>true</code> se a mensagem foi enviada com sucesso.
	 */
	boolean notifyMemberLeft(Chat chat, User user);
	
	/**
	 * Permite notificar que o usuário está digitando ou não uma mensagem no
	 * chat.
	 * 
	 * @param user Usuário que executa a ação.
	 * @param chat Chat no qual o usuário está participando.
	 * @param typing Informa se o usuário está digitando ou não.
	 * @return <code>true</code> se a mensagem foi enviada com sucesso.
	 */
	boolean notifyUserIsTyping(User user, Chat chat, boolean typing);
}
