package tv.snews.anews.messenger;


import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas 
 * ao sistema de blocos do roteiro.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface ScriptBlockMessenger {


    /**
     * Notifica quando um bloco é atualizado no sistema.
     *
     * @param scriptBlock ScriptBlock que foi atalizado.
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyUpdate(ScriptBlock scriptBlock);
    
	/**
	 * Notifica quando uma block é cadastrado no sistema.
	 * 
	 * @param block Block que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(ScriptBlock block);
	
	
	/**
	 * Notifica quando uma block é excluído.
	 * 
	 * @param block block que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(ScriptBlock block);

	/**
     * @param block
     * @param currentUser
     */
	boolean notifyMove(ScriptBlock block, User currentUser);

}
