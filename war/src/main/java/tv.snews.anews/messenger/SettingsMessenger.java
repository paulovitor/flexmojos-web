package tv.snews.anews.messenger;

import tv.snews.anews.domain.Settings;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * as configurações do sistema.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface SettingsMessenger {

	/**
	 * Notifica quando as configurações são alteradas.
	 * 
	 * @param settings Objeto com as novas configurações do sistema.
	 * @return <code>true</code> caso a notificação tenha sido enviada com
	 *         sucesso.
	 */
	boolean notifySettingsChanged(Settings settings);
}
