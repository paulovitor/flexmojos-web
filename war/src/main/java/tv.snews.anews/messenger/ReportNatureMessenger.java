package tv.snews.anews.messenger;

import tv.snews.anews.domain.ReportNature;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * às naturezas dos relatórios.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportNatureMessenger {

	/**
	 * Notifica quando uma natureza de relatório é cadastrada no sistema.
	 * 
	 * @param nature natureza que foi cadastrada
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyInsert(ReportNature nature);
	
	/**
	 * Notifica quando um evento de relatório é atualizado.
	 * 
	 * @param nature natureza que foi atualizada
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdate(ReportNature nature);
	
	/**
	 * Notifica quando um evento de relatório é excluído.
	 * 
	 * @param nature natureza que foi excluída
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyDelete(ReportNature nature);
}
