package tv.snews.anews.messenger;

import tv.snews.anews.domain.Team;

/**
 * Interface que define as notificações necessárias para as funções relacionadas
 * às equipes de usuários.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface TeamMessenger {

	boolean notifyInsert(Team team);
	
	boolean notifyUpdate(Team team);

	boolean notifyDelete(Team team);
	
}
