package tv.snews.anews.messenger;

import tv.snews.anews.domain.Company;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * as praças do sistema.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.3
 */
public interface CompanyMessenger {

	/**
	 * Notifica quando uma {@link Company} é cadastrada no sistema.
	 * 
	 * @param company O praça que foi cadastrada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInserted(Company company);
	
	/**
	 * Notifica quando um {@link Company} é atualizado.
	 * 
	 * @param company Objeto com os dados do device atualizado.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyUpdated(Company company);
	
	/**
	 * Notifica quando um {@link Company} é removido.
	 * 
	 * @param company Objeto com os dados da praça removido.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyDeleted(Company company);
}
