package tv.snews.anews.messenger;

import tv.snews.anews.domain.Rundown;

/**
 * Interface que define as ações necessárias para as notificações relacionadas 
 * ao sistema de rundownns.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface RundownMessenger {

	/**
	 * Notifica quando uma rundownm é cadastrada no sistema.
	 * 
	 * @param rundown A rundownm que foi cadastrada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Rundown rundown);
	
	/**
	 * Notifica quando uma rundownm é atualizada.
	 * 
	 * @param rundown A rundownm que foi atualizada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Rundown rundown);

	/**
	 * Notifica quando uma rundownm é excluída.
	 * 
	 * @param rundown A rundownm que foi excluída.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Rundown rundown);
	
	/**
	 * Notifica quando uma campo do espelho é atualizado.
	 * 
	 * @param rundownId Id do espelho.
	 * @param field O campo que foi atualizado
	 * @param object O conteúdo do campo que foi atualizado
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdateField(long rundownId, String field, Object object);
	
	/**
	 * Ordena as páginas do espelho.
	 * 
	 * @param rundown o espelho a ser ordenado
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifySort(Rundown rundown);

    /**
     * Reseta a exibição das laudas do espelho.
     *
     * @param rundown o espelho a ser resetado
     * @return <code>true</code> se a notificação foi enviada com sucesso.
     */
    boolean notifyDisplayResets(Rundown rundown);

    /**
     * Finaliza a exibição das laudas do espelho.
     *
     * @param rundown o espelho a ser finalizado
     * @return <code>true</code> se a notificação foi enviada com sucesso.
     */
    boolean notifyCompleteDisplay(Rundown rundown);
}
