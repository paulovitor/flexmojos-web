package tv.snews.anews.messenger;

import tv.snews.anews.domain.StoryTemplate;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public interface StoryTemplateMessenger {

    boolean notifyUpdate(StoryTemplate story);
}
