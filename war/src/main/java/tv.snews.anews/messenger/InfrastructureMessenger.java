package tv.snews.anews.messenger;

import tv.snews.anews.domain.User;

/**
 * Mensageiro utilizado para notificações relacionadas a eventos originados dos
 * serviços de infra estrutura.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface InfrastructureMessenger {

	/**
	 * Faz a notificação aos usuários de uma informação gerada de algum
	 * componente da infra estrutura. Essa notificação será enviada para todos
	 * os usuários logados.
	 * 
	 * @param infoMessage mensagem descritiva da informação
	 * @return {@code true} se a mensagem foi enviada com sucesso
	 */
	boolean notifyInfo(String infoMessage);
	
	/**
	 * Faz a notificação a um usuário de uma informação gerada de algum
	 * componente da infra estrutura. Essa notificação será enviada apenas para
	 * o usuário informado.
	 * 
	 * @param infoMessage mensagem descritiva da informação
	 * @return {@code true} se a mensagem foi enviada com sucesso
	 */
	boolean notifyInfo(String infoMessage, User user);
	
	/**
	 * Faz a notificação aos usuários de um alerta originado de algum componente
	 * da infra estrutura. Essa notificação será enviada para todos os usuários
	 * logados.
	 * 
	 * @param errorMessage mensagem descritiva do alerta
	 * @return {@code true} se a mensagem foi enviada com sucesso
	 */
	boolean notifyWarn(String warnMessage);
	
	/**
	 * Faz a notificação a um usuário de um alerta originado de algum componente
	 * da infra estrutura. Essa notificação será enviada apenas para o usuário
	 * informado.
	 * 
	 * @param errorMessage mensagem descritiva do alerta
	 * @return {@code true} se a mensagem foi enviada com sucesso
	 */
	boolean notifyWarn(String warnMessage, User user);
	
	/**
	 * Faz a notificação de um erro originado de algum componente da infra
	 * estrutura. Essa notificação será enviada para todos os usuários logados.
	 * 
	 * @param errorMessage mensagem descritiva do erro
	 * @return {@code true} se a mensagem foi enviada com sucesso
	 */
	boolean notifyError(String errorMessage);
	
	/**
	 * Faz a notificação de um erro originado de algum componente da infra
	 * estrutura. Essa notificação será enviada apenas para o usuário informado.
	 * 
	 * @param errorMessage mensagem descritiva do erro
	 * @param user usuário que irá receber a notificação
	 * @return {@code true} se a mensagem foi enviada com sucesso
	 */
	boolean notifyError(String errorMessage, User user);
}
