package tv.snews.anews.messenger;

import tv.snews.anews.domain.MosDevice;

/**
 * Interface que define as ações necessárias para as notificações relacionadas 
 * ao cadastro de dispositivos MOS.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface MosDeviceMessenger {

	/**
	 * Notifica quando um novo {@link tv.snews.anews.domain.MosDevice} é cadastrado.
	 * 
	 * @param device Objeto com os dados do device feed.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyInserted(MosDevice device);
	
	/**
	 * Notifica quando um {@link tv.snews.anews.domain.MosDevice} é atualizado.
	 * 
	 * @param device Objeto com os dados do device atualizado.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyUpdated(MosDevice device);
	
	/**
	 * Notifica quando um {@link tv.snews.anews.domain.MosDevice} é removido.
	 * 
	 * @param device Objeto com os dados do device removido.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyDeleted(MosDevice device);
}
