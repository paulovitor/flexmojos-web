package tv.snews.anews.messenger;

import tv.snews.anews.domain.ReportEvent;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * aos eventos dos relatórios.
 * 
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public interface ReportEventMessenger {

	/**
	 * Notifica quando um evento de relatório é cadastrado no sistema.
	 * 
	 * @param event evento que foi cadastrado
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyInsert(ReportEvent event);
	
	/**
	 * Notifica quando um evento de relatório é atualizado.
	 * 
	 * @param event evento que foi atualizado
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdate(ReportEvent event);
	
	/**
	 * Notifica quando um evento de relatório é excluído.
	 * 
	 * @param evento evento que foi excluído
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyDelete(ReportEvent event);
}