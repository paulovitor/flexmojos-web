package tv.snews.anews.messenger;

import tv.snews.anews.domain.ChecklistResource;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * aos recursos de produção.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public interface ChecklistResourceMessenger {

	/**
	 * Notifica quando um recurso é cadastrada no sistema.
	 * 
	 * @param resource natureza que foi cadastrada
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyInsert(ChecklistResource checklistResource);
	
	/**
	 * Notifica quando um recurso é atualizado.
	 * 
	 * @param checklistResource recurso que foi atualizada
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyUpdate(ChecklistResource checklistResource);
	
	/**
	 * Notifica quando um recurso é excluído.
	 * 
	 * @param checklistResource recurso que foi excluída
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyDelete(ChecklistResource checklistResource);
}
