package tv.snews.anews.messenger;

import tv.snews.anews.domain.Group;

/**
 * Interface que define as notificações necessárias para as funções relacionadas
 * aos grupos de usuários.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface GroupMessenger {

	boolean notifyInsert(Group group);
	
	boolean notifyUpdate(Group group);

	boolean notifyDelete(Group group);
	
}
