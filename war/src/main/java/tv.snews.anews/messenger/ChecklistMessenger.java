
package tv.snews.anews.messenger;

import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * a produção de pautas.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.5
 */
public interface ChecklistMessenger {

	/**
	 * Notifica quando uma produção é cadastrada.
	 * 
	 * @param checklist A produção da pauta que foi cadastrada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Checklist checklist);
	
	/**
	 * Notifica uma produção é atualizada.
	 * 
	 * @param checklist A produção da pauta que foi atualizada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Checklist checklist);

	/**
	 * Notifica quando uma produção da pauta é excluída.
	 * 
	 * @param checklist A produção da pauta que foi excluída.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Checklist checklist);
	
	/**
	 * Notifica quando uma produção da pauta é restaurada da lixeira.
	 * 
	 * @param checklist A produção da pauta que foi restaurada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyRestore(Checklist checklist);
	
	/**
	 * Notifica quando uma produção da pauta é bloqueada para edição.
	 * 
	 * @param checklist A produção da pauta que foi bloqueada.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyLock(Checklist checklist, User user);

	/**
	 * Notifica quando uma produção da pauta é desbloqueada para edição.
	 * 
	 * @param checklist A produção da pauta que foi desbloqueada.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyUnlock(Checklist checklist, User user);
	
}