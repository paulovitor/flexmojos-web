package tv.snews.anews.messenger;

import tv.snews.anews.domain.RundownTemplate;
import tv.snews.anews.domain.User;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public interface RundownTemplateMessenger {

    boolean notifyLock(RundownTemplate template, User user);

    boolean notifyUnlock(RundownTemplate template);
}
