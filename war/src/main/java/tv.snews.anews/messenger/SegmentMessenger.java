package tv.snews.anews.messenger;

import tv.snews.anews.domain.Segment;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * ao sistema de segmentos.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface SegmentMessenger {

	/**
	 * Notifica quando um segmento é cadastrado no sistema.
	 * 
	 * @param segment O Segmento que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Segment segment);
	
	/**
	 * Notifica quando um segmento é atualizado.
	 * 
	 * @param segment O segmento que foi atualizado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Segment segment);
	
	/**
	 * Notifica quando um segmento é excluído.
	 * 
	 * @param segment O segmento que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Segment segment);
}
