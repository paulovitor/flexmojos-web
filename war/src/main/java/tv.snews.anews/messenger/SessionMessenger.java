package tv.snews.anews.messenger;

import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao controle de sessão.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface SessionMessenger {

	/**
	 * Notifica quando um usuário faz o login no sistema.
	 * 
	 * @param user Usuário que logou no sistema.
	 * @return <code>true</code> se a notificação for enviada com sucesso.
	 */
	boolean notifyUserLoggedIn(User user, String sessionId);
	
	/**
	 * Notifica quando um usuário faz o logout no sistema.
	 * 
	 * @param user Usuário que saiu do sistema.
	 * @return <code>true</code> se a notificação for enviada com sucesso.
	 */
	boolean notifyUserLoggedOut(User user, String sessionId);
	
}
