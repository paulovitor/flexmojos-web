package tv.snews.anews.messenger;

import tv.snews.anews.domain.Device;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface DeviceMessenger {

	boolean notifyInsert(Device device);

	boolean notifyUpdate(Device device);

	boolean notifyDelete(Device device);
}
