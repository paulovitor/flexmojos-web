package tv.snews.anews.messenger;

import tv.snews.anews.domain.StoryKind;

/**
 * @author Samuel Guedes de Melo
 * @since 1.2.5
 */
public interface StoryKindMessenger {

	/**
	 * Notifica o cadastro de um novo tipo de lauda.
	 * 
	 * @param kind Dados do tipo de lauda cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyKindInserted(StoryKind kind);
	
	/**
	 * Notifica a atualização de um tipo de lauda.
	 * 
	 * @param contact Dados do tipo de lauda atualizado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyKindUpdated(StoryKind kind);
	
	/**
	 * Notifica a exclusão de um tipo de lauda.
	 * 
	 * @param kind Dados do tipo de lauda excluido.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyKindDeleted(StoryKind kind);
}
