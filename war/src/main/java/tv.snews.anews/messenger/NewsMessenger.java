
package tv.snews.anews.messenger;

import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.News;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas o
 * gerenciamento de notícias.
 * 
 * @author Eliezer Reis
 * @since 1.0.0
 */
public interface NewsMessenger {

	/**
	 * Notifica quando alguma Notícia sofreu alguma alteração no sistema .
	 * 
	 * @param news
	 *            A notícia a ser notificada.
	 * @param type
	 *            O tipo da modificação sofrida.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyNewsChanged(News news, EntityAction type);
	
	/**
	 * Notifica quando uma notícia está sendo editada por um usuário e foi
	 * bloqueada para edição.
	 * 
	 * @param news notícia que está sendo editada
	 * @param user usuário que está efetuando a edição
	 * @return {@code true} se a notificação foi enviada com sucesso
	 */
	boolean notifyLock(News news, User user);
	
	/**
	 * Notifica quando um usuário encerra a edição de uma lauda, e o bloqueio de
	 * edição foi liberado.
	 * 
     * @param story lauda que foi liberada para edição
     * @return {@code true} se a notificação foi enviada com sucesso
     */
    boolean notifyUnlock(News news, User user);
}
