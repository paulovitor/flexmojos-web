
package tv.snews.anews.messenger;

import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao sistema de pautas.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface GuidelineMessenger {

	/**
	 * Notifica quando uma pauta é cadastrada no sistema.
	 * 
	 * @param guideline A pauta que foi cadastrada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Guideline guideline);
	
	/**
	 * Notifica quando uma pauta é atualizada.
	 * 
	 * @param guideline A pauta que foi atualizada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Guideline guideline);

	/**
	 * Notifica quando uma pauta é excluída.
	 * 
	 * @param guideline A pauta que foi excluída.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Guideline guideline);
	
	/**
	 * Notifica quando uma pauta é restaurada da lixeira.
	 * 
	 * @param guideline A pauta que foi restaurada.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyRestore(Guideline guideline);
	
	/**
	 * Notifica quando uma pauta é bloqueada para edição.
	 * 
	 * @param guideline Pauta que foi bloqueada.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyLock(Guideline guideline, User user);

	/**
	 * Notifica quando uma pauta é desbloqueada para edição.
	 * 
	 * @param guideline Pauta que foi desbloqueada.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyUnlock(Guideline guideline, User user);

    boolean notifyFieldUpdate(Guideline guideline, String field, Object value);
}
