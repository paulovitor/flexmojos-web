package tv.snews.anews.messenger;

import tv.snews.anews.domain.Block;

/**
 * Interface que define as ações necessárias paras as notificações relacionadas 
 * ao sistema de blocos.
 * 
 * @author Paulo Felipe
 * @since 1.0.0
 */
public interface BlockMessenger {

	/**
	 * Notifica quando uma block é cadastrado no sistema.
	 * 
	 * @param block Block que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyInsert(Block block);
	
	/**
	 * Notifica quando uma block é cadastrado no sistema.
	 * 
	 * @param block Block que foi cadastrado.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdate(Block block);
	
	/**
	 * Notifica quando uma block é excluído.
	 * 
	 * @param block block que foi excluído.
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyDelete(Block block);
}
