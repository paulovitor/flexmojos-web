package tv.snews.anews.messenger;

import tv.snews.anews.domain.StoryCG;
import tv.snews.anews.domain.User;

import java.util.List;

/**
 * Interface que define as notificações de erros do envio de Creditos para o GC.
 * 
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
public interface IntelligentInterfaceMessenger {

	/**
	 * Notifica um erro ocorrido na execução de alguma ação do
	 * {@code Intelligent Interface}
	 * 
	 * @param error
	 * @param user
	 * @return boolean
	 */
	boolean notifySendError(String error, User user);

    /**
     * Notifica quando os créditos são enviados com sucesso ao gerador de
     * carecteres.
     *
     * @param creditsList
     * @return
     */
	boolean notifyCreditsSent(List<StoryCG> creditsList);
	
}
