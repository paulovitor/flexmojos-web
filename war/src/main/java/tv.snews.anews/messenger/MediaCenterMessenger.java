/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.messenger;

/**
 * Interface que define as ações necessárias para as notificações relacionadas 
 * as requisições do Media Center.
 * 
 * @author Samuel Guedes de Melo
 * @since 1.2.5
 */
public interface MediaCenterMessenger {

	/**
	 * Notifica respostas de {@link ProfileThreeResolverImpl#handleMosListSearchableSchema}.
	 * 
	 * @param obj Objeto com os dados dos campos para montar a busca.
	 * @param username nome do usuário que solicitou a busca.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifySearchSchema(Object obj, String username);

}
