package tv.snews.anews.messenger;

import tv.snews.anews.domain.TwitterUser;

/**
 * Interface que define as ações necessárias para as notificações relacionadas à
 * integração com o Twitter.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface TwitterMessenger {

	/**
	 * Notifica quando um novo usuário do Twitter é registrado no sistema.
	 * 
	 * @param twitterUser Dados do usuário registrado.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyTwitterUserInserted(TwitterUser twitterUser);
	
	/**
	 * Notifica quando um usuário do Twitter é removido no sistema.
	 * 
	 * @param twitterUser Dados do usuário removido.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyTwitterUserDeleted(TwitterUser twitterUser);
	
	/**
	 * Notifica que novos tweets de um usuário foram carregados.
	 * 
	 * @param twitterUser Dados do usuário que recebeu novos tweets.
	 * @return <code>true</code> se a mensagem for enviada com sucesso.
	 */
	boolean notifyTweetsLoaded(TwitterUser twitterUser);
}
