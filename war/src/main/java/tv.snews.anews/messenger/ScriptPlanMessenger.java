package tv.snews.anews.messenger;

import tv.snews.anews.domain.ScriptPlan;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao sistema de roteiros.
 *
 * @author Maxuel Ramos
 * @since 1.7
 */
public interface ScriptPlanMessenger {

	/**
	 * Notifica quando uma campo do roteiro é atualizado.
	 *
	 * @param planId Id do roteiro.
	 * @param field  O campo que foi atualizado
	 * @param value  O conteúdo do campo que foi atualizado
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUpdateField(long planId, String field, Object value);

	/**
	 * Ordena as páginas do roteiro.
	 *
	 * @param scriptPlan o roteiro a ser ordenado
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifySort(ScriptPlan scriptPlan);

	boolean notifyLock(ScriptPlan scriptPlan);

	boolean notifyUnlock(ScriptPlan scriptPlan);

    /**
     * Reseta a exibição do roteiro.
     *
     * @param scriptPlan o roteiro a ser resetado
     * @return <code>true</code> se a notificação foi enviada com sucesso.
     */
    boolean notifyDisplayResets(ScriptPlan scriptPlan);

    /**
     * Finaliza a exibição dao roteiro.
     *
     * @param scriptPlan o roteiro a ser finalizado
     * @return <code>true</code> se a notificação foi enviada com sucesso.
     */
    boolean notifyCompleteDisplay(ScriptPlan scriptPlan);
}
