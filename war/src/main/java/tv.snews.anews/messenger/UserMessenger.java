package tv.snews.anews.messenger;

import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas o
 * gerenciamento de usuários.
 * 
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface UserMessenger {

	/**
	 * Notifica quando a lista de usuários registrados no sistema sofreu alguma
	 * alteração.
	 * 
	 * @return <code>true</code> se a notificação foi enviada com sucesso.
	 */
	boolean notifyUsersChanged();
	
	
	boolean notifyInsert(User user);
	boolean notifyUpdate(User user);
	boolean notifyDelete(User user);

}
