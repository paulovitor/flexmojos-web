package tv.snews.anews.messenger;

import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.User;

/**
 * Interface que define as ações necessárias para as notificações relacionadas
 * ao sistema de reportagens.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public interface ReportageMessenger {

	boolean notifyInsert(Reportage reportage);

	boolean notifyUpdate(Reportage reportage);

	boolean notifyFieldUpdate(Reportage reportage, String field, Object value);

	boolean notifyDelete(Reportage reportage);

	boolean notifyRestore(Reportage reportage);

	boolean notifyLock(Reportage reportage);

	boolean notifyLock(Reportage reportage, User user);

	boolean notifyUnlock(Reportage reportage);
}
