package tv.snews.anews.search;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Armazena os parâmetros da busca por mídias que são informados pelo 
 * usuário na busca da central de mídias.
 * 
 * @author Maxuel Ramos
 * @since 1.7
 */
public class WSMediaParams implements Serializable {

    private String titleText;
    private String classificationText;
    private String cityText;
    private String observationText;
    private String descriptionText;
    private String reporterText;
    private String cameramanText;
//    private String assistanceText;
    private String editorText;
    private String videographerText;

    private Date eventDateStart;
    private Date eventDateEnd;
    private Date changedDateStart;
    private Date changedDateEnd;
    private int page;

	//------------------------------------
	//  Operations
	//------------------------------------
	
	public boolean hasText() {
		return StringUtils.isNotBlank(titleText);
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------


    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public String getClassificationText() {
        return classificationText;
    }

    public void setClassificationText(String programText) {
        this.classificationText = programText;
    }

    public String getObservationText() {
        return observationText;
    }

    public void setObservationText(String observationText) {
        this.observationText = observationText;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public String getReporterText() {
        return reporterText;
    }

    public void setReporterText(String reporterText) {
        this.reporterText = reporterText;
    }

    public String getCameramanText() {
        return cameramanText;
    }

    public void setCameramanText(String cameramanText) {
        this.cameramanText = cameramanText;
    }

//    public String getAssistanceText() {
//        return assistanceText;
//    }
//
//    public void setAssistanceText(String assistanceText) {
//        this.assistanceText = assistanceText;
//    }

    public String getEditorText() {
        return editorText;
    }

    public void setEditorText(String editorText) {
        this.editorText = editorText;
    }

    public String getVideographerText() {
        return videographerText;
    }

    public void setVideographerText(String videographerText) {
        this.videographerText = videographerText;
    }

    public Date getEventDateStart() {
        return eventDateStart;
    }

    public void setEventDateStart(Date eventDateStart) {
        this.eventDateStart = eventDateStart;
    }

    public Date getEventDateEnd() {
        return eventDateEnd;
    }

    public void setEventDateEnd(Date eventDateEnd) {
        this.eventDateEnd = eventDateEnd;
    }

    public Date getChangedDateStart() {
        return changedDateStart;
    }

    public void setChangedDateStart(Date changedDateStart) {
        this.changedDateStart = changedDateStart;
    }

    public Date getChangedDateEnd() {
        return changedDateEnd;
    }

    public void setChangedDateEnd(Date changedDateEnd) {
        this.changedDateEnd = changedDateEnd;
    }

    public String getCityText() {
        return cityText;
    }

    public void setCityText(String cityText) {
        this.cityText = cityText;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    private static final long serialVersionUID = -3017044497452536129L;
}
