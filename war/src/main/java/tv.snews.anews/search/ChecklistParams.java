package tv.snews.anews.search;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.User;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
public class ChecklistParams implements Serializable {

    private Date initialDate;
    private Date finalDate;

    private String slug;
    private String text;
    private String ignoreText;

    private SearchType searchType = SearchType.SHOULD;

    private User producer;
    private ChecklistType type;

    private int page;

    //------------------------------------
    //  Operations
    //------------------------------------

    public boolean hasText() {
        return StringUtils.isNotBlank(slug) || StringUtils.isNotBlank(text);
    }

    //------------------------------------
    //  Getters & Setters
    //------------------------------------

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIgnoreText() {
        return ignoreText;
    }

    public void setIgnoreText(String ignoreText) {
        this.ignoreText = ignoreText;
    }

    public SearchType getSearchType() {
        return searchType == null ? SearchType.SHOULD : searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public User getProducer() {
        return producer;
    }

    public void setProducer(User producer) {
        this.producer = producer;
    }

    public ChecklistType getType() {
        return type;
    }

    public void setType(ChecklistType type) {
        this.type = type;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    private static final long serialVersionUID = -7335763758904940849L;
}
