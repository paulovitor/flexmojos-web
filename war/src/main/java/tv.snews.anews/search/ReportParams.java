package tv.snews.anews.search;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.domain.ReportEvent;
import tv.snews.anews.domain.ReportNature;
import tv.snews.anews.domain.SearchType;

import java.io.Serializable;
import java.util.Date;

/**
 * Armazena os parâmetros da busca por relatórios que são informados pelo 
 * usuário no cliente.
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class ReportParams implements Serializable {

	public static enum Origin { NONE, GUIDELINE, REPORTAGE, STORY }
	
	private Date initialDate;
	private Date finalDate;

    private String slug;
	private String text;
	private String ignoreText;
	
	private SearchType searchType = SearchType.SHOULD;
	
	private Origin origin = Origin.NONE;
	
	private ReportEvent event;
	private ReportNature nature;
	
	private int page;

	//------------------------------------
	//  Operations
	//------------------------------------
	
	public boolean hasText() {
		return StringUtils.isNotBlank(slug) || StringUtils.isNotBlank(text);
	}
	
	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public Date getInitialDate() {
    	return initialDate;
    }

    public void setInitialDate(Date initialDate) {
    	this.initialDate = initialDate;
    }

    public Date getFinalDate() {
    	return finalDate;
    }

    public void setFinalDate(Date finalDate) {
    	this.finalDate = finalDate;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text = text;
    }
    
    public String getIgnoreText() {
	    return ignoreText;
    }
    
    public void setIgnoreText(String ignoreText) {
	    this.ignoreText = ignoreText;
    }

    public SearchType getSearchType() {
	    return searchType == null ? SearchType.SHOULD : searchType;
    }
    
    public void setSearchType(SearchType searchType) {
	    this.searchType = searchType;
    }
    
    public Origin getOrigin() {
    	return origin;
    }

    public void setOrigin(Origin origin) {
    	this.origin = origin == null ? Origin.NONE : origin;
    }

    public ReportEvent getEvent() {
    	return event;
    }

    public void setEvent(ReportEvent event) {
    	this.event = event;
    }

    public ReportNature getNature() {
    	return nature;
    }

    public void setNature(ReportNature nature) {
    	this.nature = nature;
    }

    public int getPage() {
    	return page;
    }

    public void setPage(int page) {
    	this.page = page;
    }
    
    private static final long serialVersionUID = 3779332024160522301L;
}
