package tv.snews.anews.search;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.domain.*;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class GuidelineParams implements Serializable {

    private Date initialDate;
    private Date finalDate;

    private String slug;
    private String text;
    private String ignoreText;

    private SearchType searchType = SearchType.SHOULD;

    private Program program;
    private User producer;
    private User reporter;

    private int page;
    
	private CommunicationVehicle vehicle;
    private GuidelineClassification classification;
    private Team team;


    //------------------------------------
    //  Operations
    //------------------------------------

    public boolean hasText() {
        return StringUtils.isNotBlank(slug) || StringUtils.isNotBlank(text);
    }

    //------------------------------------
    //  Getters & Setters
    //------------------------------------

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIgnoreText() {
        return ignoreText;
    }

    public void setIgnoreText(String ignoreText) {
        this.ignoreText = ignoreText;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public User getProducer() {
        return producer;
    }

    public void setProducer(User producer) {
        this.producer = producer;
    }

    public User getReporter() {
        return reporter;
    }

    public void setReporter(User reporter) {
        this.reporter = reporter;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    
    public CommunicationVehicle getVehicle() {
    	return vehicle;
    }

    public void setVehicle(CommunicationVehicle vehicle) {
    	this.vehicle = vehicle;
    }

    public GuidelineClassification getClassification() {
    	return classification;
    }

    public void setClassification(GuidelineClassification classification) {
    	this.classification = classification;
}

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    private static final long serialVersionUID = -7316418168037001905L;
}
