package tv.snews.anews.search;

import java.io.Serializable;
import java.util.Date;

public class NewsParams implements Serializable {

    private Date initialDate;
    private Date finalDate;
    private String slug;
    private int page;

    public Date getInitialDate() {
        return initialDate;
    }

    public void setInitialDate(Date initialDate) {
        this.initialDate = initialDate;
    }

    public Date getFinalDate() {
        return finalDate;
    }

    public void setFinalDate(Date finalDate) {
        this.finalDate = finalDate;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    private static final long serialVersionUID = -913018526768305699L;
}
