package tv.snews.anews.search;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.domain.SearchType;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Felipe Zap
 * @since 1.7
 */
public class DocumentParams implements Serializable {

	public static enum Origin { DOCUMENT, GUIDELINE, REPORTAGE, STORY, SCRIPT }

	private Date initialDate;
	private Date finalDate;

	private String slug;

	private SearchType searchType = SearchType.SHOULD;

	private int programId;
	private int programUsedId;
    private int programNotUsedId;
	
	private Origin origin = Origin.DOCUMENT;

	private int page;

	private boolean drawer = true;
    private String source;
    
    private boolean hideUsed = false;

	//------------------------------------
	//  Operations
	//------------------------------------

	public boolean hasText() {
		return StringUtils.isNotBlank(slug);
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------

	public Date getInitialDate() {
		return initialDate;
	}

	public void setInitialDate(Date initialDate) {
		this.initialDate = initialDate;
	}

	public Date getFinalDate() {
		return finalDate;
	}

	public void setFinalDate(Date finalDate) {
		this.finalDate = finalDate;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public SearchType getSearchType() {
		return searchType == null ? SearchType.SHOULD : searchType;
	}

	public void setSearchType(SearchType searchType) {
		this.searchType = searchType;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getProgramId() {
		return programId;
	}

	public void setProgramId(int programId) {
		this.programId = programId;
	}
	
	public int getProgramUsedId() {
		return programUsedId;
	}

	public void setProgramUsedId(int programUsedId) {
		this.programUsedId = programUsedId;
	}

    public int getProgramNotUsedId() {
        return programNotUsedId;
    }

    public void setProgramNotUsedId(int programNotUsedId) {
        this.programNotUsedId = programNotUsedId;
    }

    public Origin getOrigin() {
		return origin;
	}

	public void setOrigin(Origin origin) {
		this.origin = origin == null ? Origin.DOCUMENT : origin;
	}

	public boolean isDrawer() {
		return drawer;
	}

	public void setDrawer(boolean drawer) {
		this.drawer = drawer;
	}

	private static final long serialVersionUID = -7335763758904940849L;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
    public boolean getHideUsed() {
    	return hideUsed;
    }

    public void setHideUsed(boolean hideUsed) {
    	this.hideUsed = hideUsed;
    }
}
