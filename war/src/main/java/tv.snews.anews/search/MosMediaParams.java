package tv.snews.anews.search;

import org.apache.commons.lang.StringUtils;
import tv.snews.anews.domain.SearchType;
import tv.snews.mos.domain.MosObjectType;

import java.io.Serializable;
import java.util.Date;

/**
 * Armazena os parâmetros da busca por mídias que são informados pelo 
 * usuário na busca da central de mídias.
 * 
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
public class MosMediaParams implements Serializable {

	private Date initialDate;
	private Date finalDate;
	private String text;
	private String ignoreText;
	private SearchType searchType = SearchType.SHOULD;
    private MosObjectType mosObjectType;
	private int page;
	
	//------------------------------------
	//  Operations
	//------------------------------------
	
	public boolean hasText() {
		return StringUtils.isNotBlank(text); 
	}

	//------------------------------------
	//  Getters & Setters
	//------------------------------------
	
    public Date getInitialDate() {
    	return initialDate;
    }

    public void setInitialDate(Date initialDate) {
    	this.initialDate = initialDate;
    }

    public Date getFinalDate() {
    	return finalDate;
    }

    public void setFinalDate(Date finalDate) {
    	this.finalDate = finalDate;
    }

    public String getText() {
    	return text;
    }

    public void setText(String text) {
    	this.text = text;
    }

    public String getIgnoreText() {
    	return ignoreText;
    }

    public void setIgnoreText(String ignoreText) {
    	this.ignoreText = ignoreText;
    }

    public SearchType getSearchType() {
    	return searchType;
    }

    public void setSearchType(SearchType searchType) {
    	this.searchType = searchType;
    }

    public MosObjectType getMosObjectType() {
        return mosObjectType;
    }

    public void setMosObjectType(MosObjectType mosObjectType) {
        this.mosObjectType = mosObjectType;
    }

    public int getPage() {
    	return page;
    }

    public void setPage(int page) {
    	this.page = page;
    }
    
    private static final long serialVersionUID = -7610890226266929472L;
}
