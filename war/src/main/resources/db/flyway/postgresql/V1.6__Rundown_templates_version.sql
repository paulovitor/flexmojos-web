CREATE TABLE anews.guideline_has_vehicle (
  guideline_id integer NOT NULL,
  vehicle character varying(10) NOT NULL,
  CONSTRAINT pk_guideline_has_vehicle PRIMARY KEY (guideline_id, vehicle),
  CONSTRAINT fk_guideline_has_vehicle_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.guideline_has_vehicle OWNER TO anews;

CREATE TABLE anews.guideline_has_producer (
  guideline_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT pk_guideline_has_producer PRIMARY KEY (guideline_id, user_id),
  CONSTRAINT fk_guideline_has_producer_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_guideline_has_producer_user FOREIGN KEY (user_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.guideline_has_producer OWNER TO anews;

-- Insere o guideline.id e guideline.producer_id na tabela guideline_has_producer
INSERT INTO anews.guideline_has_producer (guideline_id, user_id) SELECT id, producer_id FROM anews.guideline WHERE producer_id IS NOT NULL;

ALTER TABLE anews.guideline DROP COLUMN producer_id;

CREATE TABLE anews.guideline_has_reporter (
  guideline_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT pk_guideline_has_reporter PRIMARY KEY (guideline_id, user_id),
  CONSTRAINT fk_guideline_has_reporter_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_guideline_has_reporter_user FOREIGN KEY (user_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.guideline_has_reporter OWNER TO anews;

-- Insere o guideline.id e guideline.reporter_id na tabela guideline_has_reporter
INSERT INTO anews.guideline_has_reporter (guideline_id, user_id) SELECT id, reporter_id FROM anews.guideline WHERE reporter_id IS NOT NULL;

ALTER TABLE anews.guideline DROP COLUMN reporter_id;

CREATE TABLE anews.checklist_has_vehicle (
  checklist_id integer NOT NULL,
  vehicle character varying(10) NOT NULL,
  CONSTRAINT pk_checklist_has_vehicle PRIMARY KEY (checklist_id, vehicle),
  CONSTRAINT fk_checklist_has_vehicle_checklist FOREIGN KEY (checklist_id) REFERENCES anews.checklist (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.checklist_has_vehicle OWNER TO anews;

CREATE TABLE anews.reportage_has_vehicle (
  reportage_id integer NOT NULL,
  vehicle character varying(10) NOT NULL,
  CONSTRAINT pk_reportage_has_vehicle PRIMARY KEY (reportage_id, vehicle),
  CONSTRAINT fk_reportage_has_vehicle_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.reportage_has_vehicle OWNER TO anews;

ALTER TABLE anews.settings ADD COLUMN session_expiration integer NOT NULL DEFAULT 60;

ALTER TABLE anews.reportage_revision ADD COLUMN vehicles text NOT NULL DEFAULT '';
ALTER TABLE anews.checklist_revision ADD COLUMN vehicles text NOT NULL DEFAULT '';
ALTER TABLE anews.guideline_revision ADD COLUMN vehicles text NOT NULL DEFAULT '';

CREATE TABLE anews.rundown_template (
  id serial NOT NULL,
  name character varying(30) NOT NULL,
  program_id integer NOT NULL,
  CONSTRAINT pk_rundown_template PRIMARY KEY (id),
  CONSTRAINT fk_rundown_template_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.rundown_template OWNER TO anews;

CREATE TABLE anews.block_template (
  id serial NOT NULL,
  "order" integer NOT NULL,
  commercial time without time zone NOT NULL,
  rundown_template_id integer NOT NULL,
  CONSTRAINT pk_block_template PRIMARY KEY (id),
  CONSTRAINT fk_block_template_rundown_template FOREIGN KEY (rundown_template_id) REFERENCES anews.rundown_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.block_template OWNER TO anews;

CREATE TABLE anews.story_template (
  id serial NOT NULL,
  page character varying(3) NOT NULL,
  slug character varying(40) DEFAULT '',
  information text,
  vt time without time zone NOT NULL,
  "order" integer NOT NULL,
  block_template_id integer NOT NULL,
  editor_id integer,
  story_kind_id integer,
  head_section_id integer,
  off_section_id integer,
  footer_section_id integer,
  nc_section_id integer,
  vt_section_id integer,
  CONSTRAINT pk_story_template PRIMARY KEY (id),
  CONSTRAINT fk_story_template_block_template FOREIGN KEY (block_template_id) REFERENCES anews.block_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_story_kind FOREIGN KEY (story_kind_id) REFERENCES anews.story_kind (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_user_editor FOREIGN KEY (editor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_story_section_head FOREIGN KEY (head_section_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_story_section_off FOREIGN KEY (off_section_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_story_section_footer FOREIGN KEY (footer_section_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_story_nc_section FOREIGN KEY (nc_section_id) REFERENCES anews.story_nc_section (story_section_id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_template_story_vt_section FOREIGN KEY (vt_section_id) REFERENCES anews.story_vt_section (story_section_id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_template OWNER TO anews;

ALTER TABLE anews.story_kind ADD COLUMN "default_kind" boolean NOT NULL DEFAULT false;

UPDATE anews.story_kind SET default_kind = true WHERE id = (SELECT id FROM anews.story_kind LIMIT 1);

ALTER TABLE anews.block_template ADD COLUMN standby boolean NOT NULL;

ALTER TABLE anews.cg_device ADD COLUMN provider character varying(10);
UPDATE anews.cg_device SET provider = 'CHYRON' WHERE virtual IS false;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('010713', 'Modelo de Espelho', '0107');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01071301', 'Visualizar', '010713');
