--
--  table anews.script_plan
--

CREATE TABLE anews.script_plan (
   id serial NOT NULL,
   date date NOT NULL,
   program_id integer NOT NULL,
   slug character varying(100),
   channel character varying(50),
   mos_status character varying(10),
   start time without time zone NOT NULL,
   "end" time without time zone NOT NULL,
   production_limit time without time zone NOT NULL,
   CONSTRAINT pk_script_plan PRIMARY KEY (id),
   CONSTRAINT fk_script_plan_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT uniq_script_plan UNIQUE (date, program_id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_plan OWNER TO anews;

--
--  table anews.script_block
--

CREATE TABLE anews.script_block (
  id serial NOT NULL,
  "order" integer NOT NULL,
  slug character varying(50),
  stand_by boolean NOT NULL,
  commercial time without time zone NOT NULL,
  script_plan_id integer NOT NULL,
  CONSTRAINT pk_script_block PRIMARY KEY (id),
  CONSTRAINT fk_script_block_script_plan FOREIGN KEY (script_plan_id) REFERENCES anews.script_plan (id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_block OWNER TO anews;

--
--  table anews.script
--

CREATE TABLE anews.script (
  document_id integer NOT NULL,
  "order" integer,
  page character varying(3) NOT NULL,
  tape character varying(30),
  synopsis text,
  mos_status character varying(10),
  head time without time zone NOT NULL,
  vt time without time zone NOT NULL,
  ok boolean NOT NULL,
  approved boolean NOT NULL,
  editor_id integer,
  reporter_id integer,
  presenter_id integer,
  script_block_id integer,
  guideline_id integer,
  reportage_id integer,
  source_script_id integer,
  CONSTRAINT pk_script PRIMARY KEY (document_id),
  CONSTRAINT fk_script_document FOREIGN KEY (document_id) REFERENCES anews.document (id) ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_script_user_editor FOREIGN KEY (editor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE SET NULL,
  CONSTRAINT fk_script_user_presenter FOREIGN KEY (presenter_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE SET NULL,
  CONSTRAINT fk_script_user_reporter FOREIGN KEY (reporter_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE SET NULL,
  CONSTRAINT fk_script_script_block FOREIGN KEY (script_block_id) REFERENCES anews.script_block (id) ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_script_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE SET NULL,
  CONSTRAINT fk_script_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE SET NULL
) WITH (OIDS = FALSE);
ALTER TABLE anews.script OWNER TO anews;
ALTER TABLE anews.script ADD CONSTRAINT fk_script_script FOREIGN KEY (source_script_id) REFERENCES anews.script (document_id) ON UPDATE NO ACTION ON DELETE SET NULL;

--
--  table anews.script_image
--

CREATE TABLE anews.script_image (
  id serial NOT NULL,
  "order" integer NOT NULL,
  name character varying(255) NOT NULL,
  extension character varying(7) NOT NULL,
  path character varying(255) NOT NULL,
  size bigint NOT NULL,
  script_id integer NOT NULL,
  CONSTRAINT pk_script_image PRIMARY KEY (id),
  CONSTRAINT fk_script_image_script FOREIGN KEY (script_id) REFERENCES anews.script (document_id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_image OWNER TO anews;

--
--  table anews.script_revision
--

CREATE TABLE anews.script_revision (
  id serial NOT NULL,
  revision_number integer NOT NULL,
  revision_date timestamp without time zone NOT NULL,
  revisor_id integer NOT NULL,
  entity_id integer NOT NULL,
  slug character varying(100) NOT NULL,
  tape character varying(30) NOT NULL,
  presenter character varying(20) NOT NULL,
  synopsis text NOT NULL,
  images text NOT NULL,
  videos text NOT NULL,
  CONSTRAINT pk_script_revision PRIMARY KEY (id),
  CONSTRAINT fk_script_revision_user FOREIGN KEY (revisor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_script_revision_script FOREIGN KEY (entity_id) REFERENCES anews.script (document_id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_revision OWNER TO anews;

ALTER TABLE anews.script DROP COLUMN head;

DELETE FROM anews.script_revision;
ALTER TABLE anews.script_revision ADD COLUMN vt character varying(5) NOT NULL;
ALTER TABLE anews.script_revision ADD COLUMN reporter character varying(20) NOT NULL;

ALTER TABLE anews.settings ADD COLUMN color_script integer NOT NULL DEFAULT 11993122;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('0117', 'Roteiro', '01');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('011701', 'Adicionar', '0117');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011702', 'Alterar Data', '0117');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011703', 'Bloco', '0117');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170301', 'Adicionar', '011703');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170302', 'Deletar', '011703');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170303', 'Editar', '011703');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170304', 'Sincronizar', '011703');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('011704', 'Editar', '0117');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011705', 'Enviar para Gaveta', '0117');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011706', 'Imprimir', '0117');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('011707', 'Movimentar', '0117');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011708', 'Ordenar Páginas', '0117');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011709', 'Script', '0117');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170901', 'Adicionar', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170902', 'Alterar depois de aprovado', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170903', 'Aprovar', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170904', 'Copiar', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170905', 'Deletar', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170906', 'Editar', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170907', 'Imprimir', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170908', 'Restaurar', '011709');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170909', 'Visualizar', '011709');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('011710', 'Visualizar', '0117');

ALTER TABLE anews.script ADD COLUMN blank boolean NOT NULL DEFAULT true;

ALTER TABLE anews.script_image ADD COLUMN thumbnail_path character varying(255);

ALTER TABLE anews.mos_device RENAME TO mos_device_2;

--
--  Generic Device
--

CREATE TABLE anews.device (
  id serial NOT NULL,
  name character varying(60) NOT NULL,
  protocol character varying(10) NOT NULL,
  vendor character varying(20) NOT NULL,
  CONSTRAINT pk_device PRIMARY KEY (id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.device OWNER TO anews;

CREATE TABLE anews.device_has_type (
  device_id integer NOT NULL,
  type character varying(10) NOT NULL,
  CONSTRAINT pk_device_has_type PRIMARY KEY (device_id, type),
  CONSTRAINT fk_device_has_type_device FOREIGN KEY (device_id) REFERENCES anews.device (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.device_has_type OWNER TO anews;

ALTER TABLE anews.program RENAME cg_device_id TO cg_device_id_2;
ALTER TABLE anews.program ADD COLUMN cg_device_id integer;
ALTER TABLE anews.program ADD CONSTRAINT fk_program_device FOREIGN KEY (cg_device_id) REFERENCES anews.device (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
--  Intelligent Interface Device
--

CREATE TABLE anews.ii_device (
  device_id integer NOT NULL,
  hostname character varying(100) NOT NULL,
  port integer NOT NULL DEFAULT 23,
  increment integer NOT NULL DEFAULT 0,
  virtual boolean NOT NULL DEFAULT false,
  CONSTRAINT pk_ii_device PRIMARY KEY (device_id),
  CONSTRAINT fk_ii_device_device FOREIGN KEY (device_id) REFERENCES anews.device (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.ii_device OWNER TO anews;

CREATE TABLE anews.ii_device_path (
  id serial NOT NULL,
  path text NOT NULL,
  ii_device_id integer NOT NULL,
  CONSTRAINT pk_ii_device_path PRIMARY KEY (id),
  CONSTRAINT fk_ii_device_path_ii_device FOREIGN KEY (ii_device_id) REFERENCES anews.ii_device (device_id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.ii_device_path OWNER TO anews;

ALTER TABLE anews.program ADD COLUMN cg_path_id integer;
ALTER TABLE anews.program ADD CONSTRAINT fk_program_ii_device_path FOREIGN KEY (cg_path_id) REFERENCES anews.ii_device_path (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE anews.ii_template (
  id serial NOT NULL,
  name character varying(60) NOT NULL,
  template_code integer,
  comment boolean NOT NULL DEFAULT false,
  ii_device_id integer NOT NULL,
  CONSTRAINT pk_ii_template PRIMARY KEY (id),
  CONSTRAINT fk_ii_template_ii_device FOREIGN KEY (ii_device_id) REFERENCES anews.ii_device (device_id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.ii_template OWNER TO anews;

CREATE TABLE anews.ii_template_field (
  id serial NOT NULL,
  name character varying(60) NOT NULL,
  "number" integer NOT NULL,
  size integer NOT NULL,
  ii_template_id integer NOT NULL,
  CONSTRAINT pk_ii_template_field PRIMARY KEY (id),
  CONSTRAINT fk_ii_template_field_ii_template FOREIGN KEY (ii_template_id) REFERENCES anews.ii_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.ii_template_field OWNER TO anews;

--
--  MOS Device
--

CREATE TABLE anews.mos_device (
  device_id integer NOT NULL,
  mos_id character varying(128) NOT NULL,
  hostname character varying(100) NOT NULL,
  lower_port integer NOT NULL DEFAULT 10540,
  upper_port integer NOT NULL DEFAULT 10541,
  query_port integer NOT NULL DEFAULT 10542,
  current_message_id integer NOT NULL DEFAULT 0,
  active_x boolean NOT NULL DEFAULT false,
  profile_one_supported boolean NOT NULL DEFAULT false,
  profile_two_supported boolean NOT NULL DEFAULT false,
  profile_three_supported boolean NOT NULL DEFAULT false,
  profile_four_supported boolean NOT NULL DEFAULT false,
  profile_five_supported boolean NOT NULL DEFAULT false,
  profile_six_supported boolean NOT NULL DEFAULT false,
  profile_seven_supported boolean NOT NULL DEFAULT false,
  revision character varying NOT NULL,
  CONSTRAINT pk_mos_device PRIMARY KEY (device_id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.mos_device OWNER TO anews;

CREATE TABLE anews.mos_device_channel (
  id serial NOT NULL,
  name character varying(50) NOT NULL,
  mos_device_id integer NOT NULL,
  CONSTRAINT pk_mos_device_channel PRIMARY KEY (id),
  CONSTRAINT fk_mos_device_channel_mos_device FOREIGN KEY (mos_device_id) REFERENCES anews.mos_device (device_id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.mos_device_channel OWNER TO anews;

ALTER TABLE anews.mos_device ADD COLUMN default_mos_channel_id integer;
ALTER TABLE anews.mos_device ADD CONSTRAINT pk_mos_device_mos_device_channel FOREIGN KEY (default_mos_channel_id) REFERENCES anews.mos_device_channel (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.story_mos_obj ADD COLUMN mos_device_channel_id integer;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_story_mos_obj_mos_device_channel FOREIGN KEY (mos_device_channel_id) REFERENCES anews.mos_device_channel (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
--  Web Service Device
--

CREATE TABLE anews.ws_device (
  device_id integer NOT NULL,
  address text NOT NULL,
  username character varying(30),
  password character varying(30),
  CONSTRAINT pk_ws_device PRIMARY KEY (device_id),
  CONSTRAINT fk_ws_device_device FOREIGN KEY (device_id) REFERENCES anews.device (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.ws_device OWNER TO anews;

ALTER TABLE anews.reportage_cg ADD COLUMN template_code integer;
ALTER TABLE anews.reportage_cg ADD COLUMN comment boolean NOT NULL DEFAULT false;

ALTER TABLE anews.story_cg ADD COLUMN template_code integer;
ALTER TABLE anews.story_cg ADD COLUMN comment boolean NOT NULL DEFAULT false;

ALTER TABLE anews.reportage_cg_field ADD COLUMN name character varying(60);
ALTER TABLE anews.reportage_cg_field ADD COLUMN size integer NOT NULL DEFAULT 0;

ALTER TABLE anews.story_cg_field ADD COLUMN name character varying(60);
ALTER TABLE anews.story_cg_field ADD COLUMN size integer NOT NULL DEFAULT 0;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

--  Copia 'code' e 'note' de 'cg_template' para 'reportage_cg'
UPDATE anews.reportage_cg SET
  template_code = sub.code,
  comment = sub.note
FROM ( SELECT id, code, note FROM anews.cg_template ) AS sub
WHERE cg_template_id = sub.id;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

--  Copia 'code' e 'note' de 'cg_template' para 'story_cg'
UPDATE anews.story_cg SET
  template_code = sub.code,
  comment = sub.note
FROM ( SELECT id, code, note FROM anews.cg_template ) AS sub
WHERE cg_template_id = sub.id;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

--  Cria uma coluna temporária para guardar o ID do template em 'reportage_cg_field'
ALTER TABLE anews.reportage_cg_field ADD COLUMN aux integer;

--  Copia o ID do template de 'reportage_cg' para 'reportage_cg_field'
UPDATE anews.reportage_cg_field SET
  aux = sub.cg_template_id
FROM ( SELECT id, cg_template_id FROM anews.reportage_cg ) AS sub
WHERE reportage_cg_id = sub.id;

--  Cópia o 'name' e 'size' do 'cg_field' para 'reportage_cg_field'
UPDATE anews.reportage_cg_field SET
  name = sub.name,
  size = sub.size
FROM ( SELECT cg_template_id, name, size FROM anews.cg_field ) AS sub
WHERE aux = sub.cg_template_id;

--  Apaga a coluna temporária
ALTER TABLE anews.reportage_cg_field DROP COLUMN aux;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

--  Cria uma coluna temporária para guardar o ID do template em 'story_cg_field'
ALTER TABLE anews.story_cg_field ADD COLUMN aux integer;

--  Copia o ID do template de 'story_cg' para 'story_cg_field'
UPDATE anews.story_cg_field SET
  aux = sub.cg_template_id
FROM ( SELECT story_sub_section_id, cg_template_id FROM anews.story_cg ) AS sub
WHERE story_cg_id = sub.story_sub_section_id;

--  Cópia o 'name' e 'size' do 'cg_field' para 'story_cg_field'
UPDATE anews.story_cg_field SET
  name = sub.name,
  size = sub.size
FROM ( SELECT cg_template_id, name, size FROM anews.cg_field ) AS sub
WHERE aux = sub.cg_template_id;

--  Apaga a coluna temporária
ALTER TABLE anews.story_cg_field DROP COLUMN aux;

-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------

-- Apaga o vínculo de 'reportage_cg' e 'story_cg' com 'cg_template'
ALTER TABLE anews.reportage_cg DROP COLUMN cg_template_id;
ALTER TABLE anews.story_cg DROP COLUMN cg_template_id;


ALTER TABLE anews.device ADD COLUMN aux integer;
ALTER TABLE anews.ii_device ADD COLUMN aux integer;
ALTER TABLE anews.ii_template ADD COLUMN aux integer;
ALTER TABLE anews.mos_device ADD COLUMN aux integer;
ALTER TABLE anews.mos_device_channel ADD COLUMN aux integer;

--
--  CG DEVICES
--

-- Copia os dados de 'cg_device' para 'device', 'ii_device' e 'device_has_type'
INSERT INTO anews.device (name, vendor, protocol, aux)
  SELECT name, provider, 'II', id FROM anews.cg_device
  WHERE provider IS NOT NULL AND provider != '';
-- Para banco de dados onde não tem um provider definido
INSERT INTO anews.device (name, vendor, protocol, aux)
  SELECT name, 'OTHER', 'II', id FROM anews.cg_device
  WHERE provider IS NULL OR provider = '';
INSERT INTO anews.ii_device (device_id, hostname, port, increment, virtual, aux)
  SELECT _d.id, ip, port, code_increment, virtual, _cg.id
  FROM anews.device AS _d
    JOIN anews.cg_device AS _cg ON _d.aux = _cg.id;
INSERT INTO anews.device_has_type (device_id, type)
  SELECT device_id, 'CG' FROM anews.ii_device;

-- Migra os paths cg dos programas para 'ii_device_path'
INSERT INTO anews.ii_device_path (path, ii_device_id)
  SELECT path_message_directory, device_id
  FROM anews.program
    JOIN anews.ii_device ON cg_device_id_2 = aux
  WHERE path_message_directory IS NOT NULL AND path_message_directory != '';

--
--  CG TEMPLATES
--

-- Copia os dados de 'cg_template' para 'ii_template'
INSERT INTO anews.ii_template (name, template_code, comment, ii_device_id, aux)
  SELECT _t.name, _t.code, _t.note, _d.device_id, _t.id
  FROM anews.cg_template AS _t
    JOIN anews.ii_device AS _d ON _t.cg_device_id = _d.aux
  WHERE _t.excluded = false;

-- Copia os dados de 'cg_field' para 'ii_template_field'
INSERT INTO anews.ii_template_field (name, number, size, ii_template_id)
  SELECT _f.name, number, size, _t.id
  FROM anews.cg_field AS _f
    JOIN anews.ii_template AS _t ON cg_template_id = aux;

--
--  MOS DEVICES
--

-- Copia os dados de 'mos_device_2' para 'device' quando for um dispositivo da SNEWS
INSERT INTO anews.device (name, vendor, protocol, aux)
  SELECT name, 'SNEWS', 'MOS', id FROM anews.mos_device_2 WHERE identifier ILIKE '%snews%';

-- Copia os dados de 'mos_device_2' para 'device' quando for um dispositivo da HARRIS
INSERT INTO anews.device (name, vendor, protocol, aux)
  SELECT name, 'HARRIS', 'MOS', id FROM anews.mos_device_2 WHERE identifier ILIKE '%harris%';

-- Copia os dados de 'mos_device_2' para 'mos_device'
INSERT INTO anews.mos_device (
  device_id, mos_id, hostname, lower_port, upper_port, query_port, current_message_id,
  profile_one_supported, profile_two_supported, profile_three_supported, profile_four_supported,
  profile_five_supported, profile_six_supported, profile_seven_supported, revision, active_x, aux
) SELECT
    _d.id, identifier, host, lower_port, upper_port, query_port, current_message_id,
    profile_one_supported, profile_two_supported, profile_three_supported, profile_four_supported,
    profile_five_supported, profile_six_supported, profile_seven_supported, mos_revision, false, _m.id
  FROM anews.mos_device_2 AS _m
    JOIN anews.device AS _d ON _m.id = _d.aux
  WHERE _d.protocol = 'MOS';

-- Copia os dados de 'mos_channel' para 'mos_device_channel'
INSERT INTO anews.mos_device_channel (name, mos_device_id, aux)
  SELECT _c.name, _d.id, _c.id
  FROM anews.mos_channel AS _c
    JOIN anews.device AS _d ON mos_device_id = aux;

-- Copia os dados de 'mos_device_type' para 'device_has_type'
INSERT INTO anews.device_has_type (device_id, type)
  SELECT device_id, description
  FROM anews.mos_device
    JOIN anews.mos_device_type ON mos_device_id = aux;

--
--  MOS MEDIA
--

ALTER TABLE anews.mos_media RENAME mos_device_id TO mos_device_id_2;
ALTER TABLE anews.mos_media ADD COLUMN mos_device_id integer;
ALTER TABLE anews.mos_media ADD CONSTRAINT fk_mos_media_mos_device FOREIGN KEY (mos_device_id) REFERENCES anews.mos_device (device_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE anews.mos_media SET
  mos_device_id = sub.device_id
FROM ( SELECT device_id, aux FROM anews.mos_device ) AS sub
WHERE mos_device_id_2 = sub.aux;

--
--  PROGRAM
--

-- Copia 'program.cg_device_id_2' para 'cg_device_id' (de 'ii_device')
UPDATE anews.program SET
  cg_device_id = sub.device_id
FROM ( SELECT device_id, aux FROM anews.ii_device ) AS sub
WHERE cg_device_id_2 = sub.aux;

UPDATE anews.program SET
  cg_path_id = sub.id
FROM ( SELECT id, ii_device_id, path FROM anews.ii_device_path ) AS sub
WHERE cg_device_id = sub.ii_device_id AND path_message_directory = sub.path;

--
--  STORY
--

-- Copia 'story_mos_obj.mos_channel_id' para 'mos_device_channel_id'
UPDATE anews.story_mos_obj SET
  mos_device_channel_id = sub.id
FROM ( SELECT id, aux FROM anews.mos_device_channel ) AS sub
WHERE mos_channel_id = sub.aux;

--
--  APAGA AS TABELAS E COLUNAS ANTIGAS OU TEMPORÁRIAS
--

-- Já devia ter sumido faz tempo
DROP TABLE IF EXISTS anews.credit_field;
DROP TABLE IF EXISTS anews.credit;

ALTER TABLE anews.program DROP COLUMN cg_device_id_2;
ALTER TABLE anews.program DROP COLUMN path_message_directory;

DROP TABLE anews.cg_field;
DROP TABLE anews.cg_template;
DROP TABLE anews.cg_device;

ALTER TABLE anews.mos_device_2 DROP COLUMN default_channel_id;
ALTER TABLE anews.mos_media DROP COLUMN mos_device_id_2;
ALTER TABLE anews.story_mos_obj DROP COLUMN mos_channel_id;
ALTER TABLE anews.story_mos_obj DROP COLUMN mos_device_playout_id;

DROP TABLE anews.mos_channel;
DROP TABLE anews.mos_device_type;
DROP TABLE anews.mos_device_2;

-- Colunas temporárias
ALTER TABLE anews.device DROP COLUMN aux;
ALTER TABLE anews.ii_device DROP COLUMN aux;
ALTER TABLE anews.ii_template DROP COLUMN aux;
ALTER TABLE anews.mos_device DROP COLUMN aux;
ALTER TABLE anews.mos_device_channel DROP COLUMN aux;


ALTER TABLE anews.mos_device DROP CONSTRAINT pk_mos_device_mos_device_channel;
ALTER TABLE anews.mos_device ADD CONSTRAINT fk_mos_device_mos_device_channel FOREIGN KEY (default_mos_channel_id) REFERENCES anews.mos_device_channel (id) ON UPDATE NO ACTION ON DELETE SET NULL;


ALTER TABLE anews.program ADD COLUMN playout_device_id integer;

ALTER TABLE anews.program DROP CONSTRAINT fk_program_device;

ALTER TABLE anews.program ADD CONSTRAINT fk_program_device_cg FOREIGN KEY (cg_device_id) REFERENCES anews.device (id) ON UPDATE NO ACTION ON DELETE SET NULL;
ALTER TABLE anews.program ADD CONSTRAINT fk_program_device_playout FOREIGN KEY (playout_device_id) REFERENCES anews.device (id) ON UPDATE NO ACTION ON DELETE SET NULL;


DELETE FROM anews.group_has_permission WHERE permission_id ILIKE '0207%';
DELETE FROM anews.group_has_permission WHERE permission_id ILIKE '0208%';
DELETE FROM anews.group_has_permission WHERE permission_id ILIKE '0209%';
DELETE FROM anews.group_has_permission WHERE permission_id ILIKE '0210%';

DELETE FROM anews.permission WHERE id ILIKE '0207%';
DELETE FROM anews.permission WHERE id ILIKE '0208%';
DELETE FROM anews.permission WHERE id ILIKE '0209%';
DELETE FROM anews.permission WHERE id ILIKE '0210%';

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0207', 'Dispositivos', '02');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('020701', 'Visualizar', '0207');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('020702', 'Alterar', '0207');

ALTER TABLE anews.mos_media RENAME duration TO duration_time;
ALTER TABLE anews.mos_media ADD COLUMN duration integer;
ALTER TABLE anews.mos_media ADD COLUMN time_base numeric;

ALTER TABLE anews.story_mos_obj RENAME TO story_mos_video;
ALTER TABLE anews.mos_media ADD COLUMN mos_external_metadata text;

CREATE TABLE anews.story_mos_credit (
  story_sub_section_id integer NOT NULL,
  mos_media_id integer NOT NULL,
  mos_status character varying(10),
  channel character varying(50),
  slug character varying(128),
  CONSTRAINT pk_story_mos_credit PRIMARY KEY (story_sub_section_id),
  CONSTRAINT fk_story_mos_credit_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_mos_credit OWNER TO anews;

CREATE TABLE anews.story_mos_credit_field (
  id serial NOT NULL,
  name character varying(128),
  value character varying(128),
  index integer,
  story_mos_credit_id integer NOT NULL,
  CONSTRAINT pk_story_mos_credit_field PRIMARY KEY (id),
  CONSTRAINT fk_story_mos_credit FOREIGN KEY (story_mos_credit_id) REFERENCES anews.story_mos_credit (story_sub_section_id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_mos_credit_field OWNER TO anews;

CREATE TABLE anews.script_mos_video (
  id serial NOT NULL,
  "order" integer NOT NULL,
  mos_status character varying(10),
  mos_media_id integer NOT NULL,
  mos_device_channel_id integer,
  script_id integer NOT NULL,
  CONSTRAINT pk_script_mos_video PRIMARY KEY (id),
  CONSTRAINT fk_script_mos_video_mos_media FOREIGN KEY (mos_media_id)
      REFERENCES anews.mos_media (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_script_mos_video_mos_device_channel FOREIGN KEY (mos_device_channel_id)
      REFERENCES anews.mos_device_channel (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_script_mos_video_script FOREIGN KEY (script_id)
      REFERENCES anews.script (document_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
) WITH (OIDS=FALSE);
ALTER TABLE anews.script_mos_video OWNER TO anews;

ALTER TABLE anews."group" ADD COLUMN show_as_checklist_producers boolean;
ALTER TABLE anews."group" ALTER COLUMN show_as_checklist_producers SET DEFAULT false;
update anews."group" set show_as_checklist_producers = false;
ALTER TABLE anews."group" ALTER COLUMN show_as_checklist_producers SET NOT NULL;

ALTER TABLE anews.story ADD COLUMN videos_added boolean NOT NULL DEFAULT false;

ALTER TABLE anews.story ADD COLUMN displaying boolean NOT NULL DEFAULT false;
ALTER TABLE anews.story ADD COLUMN displayed boolean NOT NULL DEFAULT false;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('01071113', 'Pôr em Exibição', '010711');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01071114', 'Reiniciar a exibição', '010711');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('01071115', 'Finalizar a exibição', '010711');

-- Table: script_mos_credit

-- DROP TABLE anews.script_mos_credit;

CREATE TABLE anews.script_mos_credit
(
  id integer NOT NULL,
  "order" integer NOT NULL,
  mos_media_id integer NOT NULL,
  mos_status character varying(10),
  channel character varying(50),
  slug character varying(128),
  script_id integer NOT NULL,
  CONSTRAINT pk_script_mos_credit PRIMARY KEY (id),
  CONSTRAINT fk_script_mos_credit_mos_media FOREIGN KEY (mos_media_id)
  REFERENCES anews.mos_media (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_script_mos_credit_script FOREIGN KEY (script_id)
  REFERENCES anews.script (document_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS=FALSE
);
ALTER TABLE anews.script_mos_credit
OWNER TO anews;

-- Table: script_mos_credit_field

-- DROP TABLE anews.script_mos_credit_field;

CREATE TABLE anews.script_mos_credit_field
(
  id serial NOT NULL,
  name character varying(128),
  value character varying(128),
  index integer,
  script_mos_credit_id integer NOT NULL,
  CONSTRAINT pk_script_mos_credit_field PRIMARY KEY (id),
  CONSTRAINT fk_script_mos_credit FOREIGN KEY (script_mos_credit_id)
      REFERENCES anews.script_mos_credit (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.script_mos_credit_field
  OWNER TO anews;

-- Table: script_cg

-- DROP TABLE anews.script_cg;

CREATE TABLE anews.script_cg
(
  id integer NOT NULL,
  code integer NOT NULL,
  template_code integer,
  comment boolean NOT NULL DEFAULT false,
  "order" integer,
  script_id integer NOT NULL,
  CONSTRAINT pk_script_cg PRIMARY KEY (id),
  CONSTRAINT fk_script_cg_script FOREIGN KEY (script_id)
      REFERENCES anews.script (document_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.script_cg
  OWNER TO anews;

-- Table: script_cg_field

-- DROP TABLE anews.script_cg_field;

CREATE TABLE anews.script_cg_field
(
  id serial NOT NULL,
  "number" integer NOT NULL,
  value text,
  script_cg_id integer NOT NULL,
  name character varying(60),
  size integer NOT NULL DEFAULT 0,
  CONSTRAINT pk_script_cg_field PRIMARY KEY (id),
  CONSTRAINT fk_script_cg_field_script_cg FOREIGN KEY (script_cg_id)
      REFERENCES anews.script_cg (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.script_cg_field
  OWNER TO anews;
  
DROP TABLE IF EXISTS anews.script_cg_field;
DROP TABLE IF EXISTS anews.script_cg;
DROP TABLE IF EXISTS anews.script_mos_credit_field;
DROP TABLE IF EXISTS anews.script_mos_credit;
DROP TABLE IF EXISTS anews.script_item;

CREATE TABLE anews.script_item (
  id        SERIAL  NOT NULL,
  script_id INTEGER NOT NULL,
  "order"   INTEGER NOT NULL,
  CONSTRAINT pk_script_item PRIMARY KEY (id),
  CONSTRAINT fk_script_item_script FOREIGN KEY (script_id)
  REFERENCES anews.script (document_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_item OWNER TO anews;

CREATE TABLE anews.script_mos_credit (
  script_item_id INTEGER NOT NULL,
  mos_media_id   INTEGER NOT NULL,
  mos_status     CHARACTER VARYING(10),
  channel        CHARACTER VARYING(50),
  slug           CHARACTER VARYING(128),
  CONSTRAINT pk_script_mos_credit PRIMARY KEY (script_item_id),
  CONSTRAINT fk_script_mos_credit_script_item FOREIGN KEY (script_item_id)
  REFERENCES anews.script_item (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_script_mos_credit_mos_media FOREIGN KEY (mos_media_id)
  REFERENCES anews.mos_media (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_mos_credit OWNER TO anews;

CREATE TABLE anews.script_mos_credit_field (
  id                   SERIAL  NOT NULL,
  script_mos_credit_id INTEGER NOT NULL,
  name                 CHARACTER VARYING(128),
  value                CHARACTER VARYING(128),
  index                INTEGER,
  CONSTRAINT pk_script_mos_credit_field PRIMARY KEY (id),
  CONSTRAINT fk_script_mos_credit_field_script_mos_credit FOREIGN KEY (script_mos_credit_id)
  REFERENCES anews.script_mos_credit (script_item_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_mos_credit_field OWNER TO anews;

CREATE TABLE anews.script_cg (
  script_item_id INTEGER NOT NULL,
  code           INTEGER NOT NULL,
  template_code  INTEGER,
  comment        BOOLEAN NOT NULL DEFAULT FALSE,
  CONSTRAINT pk_script_cg PRIMARY KEY (script_item_id),
  CONSTRAINT fk_script_cg_script_item FOREIGN KEY (script_item_id)
  REFERENCES anews.script_item (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_cg OWNER TO anews;

CREATE TABLE anews.script_cg_field (
  id           SERIAL  NOT NULL,
  script_cg_id INTEGER NOT NULL,
  "number"     INTEGER NOT NULL,
  value        TEXT,
  name         CHARACTER VARYING(60),
  size         INTEGER NOT NULL DEFAULT 0,
  CONSTRAINT pk_script_cg_field PRIMARY KEY (id),
  CONSTRAINT fk_script_cg_field_script_cg FOREIGN KEY (script_cg_id)
  REFERENCES anews.script_cg (script_item_id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.script_cg_field OWNER TO anews;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('011711', 'Enviar créditos', '010711');

ALTER TABLE anews.script
  ADD COLUMN has_videos boolean NOT NULL DEFAULT false;
  

ALTER TABLE anews.story_template ADD COLUMN videos_added boolean NOT NULL DEFAULT false;


