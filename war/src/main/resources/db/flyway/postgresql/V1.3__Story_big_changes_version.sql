ALTER TABLE anews.settings ADD COLUMN color_media_center integer NOT NULL DEFAULT 0;
UPDATE anews.settings SET color_media_center = 6781116 WHERE id = 1;
INSERT INTO anews.permission (id, name, permission_id) VALUES ('0112', 'Centro de Mídia', '01');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011201', 'Visualizar', '0112');
ALTER TABLE anews.mosdevice ADD COLUMN type integer NOT NULL DEFAULT 2;

ALTER TABLE anews.mosdevice RENAME TO mos_device;
ALTER SEQUENCE anews.mosdevice_id_seq RENAME TO mos_device_id_seq;
ALTER TABLE anews.mos_device ALTER COLUMN id SET DEFAULT nextval('anews.mos_device_id_seq'::regclass);
ALTER TABLE anews.mos_device DROP CONSTRAINT mosdevice_pkey;
ALTER TABLE anews.mos_device DROP CONSTRAINT mosdevice_uniq_identifier;
ALTER TABLE anews.mos_device ADD CONSTRAINT mos_device_pk PRIMARY KEY (id);
ALTER TABLE anews.mos_device ADD CONSTRAINT mos_device_uniq_identifier UNIQUE (identifier);

-- Table: mos_obj

-- DROP TABLE mos_obj;

CREATE TABLE anews.mos_obj
(
  id serial NOT NULL,
  obj_id character varying(128) NOT NULL,
  obj_slug character varying(128) NOT NULL,
  mos_abstract text,
  obj_group character varying(100),
  obj_type character varying(10) NOT NULL,
  obj_rev integer,
  obj_duration time without time zone,
  obj_air character varying(10),
  created_by character varying(128) NOT NULL,
  created timestamp with time zone NOT NULL DEFAULT now(),
  changed_by character varying(100) NOT NULL,
  changed timestamp with time zone,
  description text NOT NULL,
  mos_device_id bigint,
  excluded boolean NOT NULL DEFAULT false,
  CONSTRAINT mos_obj_pk PRIMARY KEY (id),
  CONSTRAINT mos_device_fk FOREIGN KEY (mos_device_id)
      REFERENCES anews.mos_device (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.mos_obj
  OWNER TO anews;

-- Table: mos_obj_path

-- DROP TABLE mos_obj_path;

CREATE TABLE anews.mos_obj_path
(
  id serial NOT NULL,
  tech_description character varying(100),
  url text,
  mos_obj_id bigint,
  CONSTRAINT mos_obj_path_pk PRIMARY KEY (id),
  CONSTRAINT mos_obj_fk FOREIGN KEY (mos_obj_id)
      REFERENCES anews.mos_obj (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.mos_obj_path
  OWNER TO anews;
  
 
-- Table: mos_obj_metadata_path

-- DROP TABLE mos_obj_metadata_path;

CREATE TABLE anews.mos_obj_metadata_path                
(                                                 
  id serial NOT NULL,                             
  tech_description character varying(100),        
  url text,                                       
  mos_obj_id bigint,                              
  CONSTRAINT mos_obj_metadata_path_pk PRIMARY KEY (id),    
  CONSTRAINT mos_obj_fk FOREIGN KEY (mos_obj_id)  
      REFERENCES anews.mos_obj (id) MATCH SIMPLE        
      ON UPDATE NO ACTION ON DELETE NO ACTION     
)                                                 
WITH (                                            
  OIDS=FALSE                                      
);                                                
ALTER TABLE anews.mos_obj_metadata_path                          
  OWNER TO anews; 

 

-- Table: mos_obj_proxy_path

-- DROP TABLE mos_obj_proxy_path;

CREATE TABLE anews.mos_obj_proxy_path
(
  id serial NOT NULL,
  tech_description character varying(100),
  url text,
  mos_obj_id bigint,
  CONSTRAINT mos_obj_proxy_path_pk PRIMARY KEY (id),
  CONSTRAINT mos_obj_fk FOREIGN KEY (mos_obj_id)
      REFERENCES anews.mos_obj (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.mos_obj_proxy_path
  OWNER TO anews;
  
  
  --
-- Create table anews.guideline_revision
--
CREATE TABLE anews.guideline_revision (
  id serial NOT NULL,
  revision_number integer NOT NULL,
  revision_date timestamp with time zone NOT NULL,
  revisor_id integer NOT NULL,
  entity_id integer NOT NULL,
  slug character varying(100) NOT NULL,
  proposal text NOT NULL,
  referral text NOT NULL,
  informations text NOT NULL,
  producer text NOT NULL,
  reporter text NOT NULL,
  guides text NOT NULL,
  CONSTRAINT guideline_revision_pkey PRIMARY KEY (id ),
  CONSTRAINT entity_fkey FOREIGN KEY (entity_id) REFERENCES anews.guideline (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT revisor_fkey FOREIGN KEY (revisor_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS=FALSE);
ALTER TABLE anews.guideline_revision OWNER TO anews;

--
-- Create table anews.reportage_revision
--
CREATE TABLE anews.reportage_revision (
   id serial NOT NULL, 
   revision_number integer NOT NULL, 
   revision_date timestamp with time zone NOT NULL, 
   revisor_id integer NOT NULL, 
   entity_id integer NOT NULL, 
   cameraman character varying(60) NOT NULL,
   sections text NOT NULL,
   CONSTRAINT reportage_revision_pkey PRIMARY KEY (id), 
   CONSTRAINT revisor_fkey FOREIGN KEY (revisor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT entity_fkey FOREIGN KEY (entity_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.reportage_revision OWNER TO anews;

--
-- Create table anews.story_revision
--
CREATE TABLE anews.story_revision (
   id serial NOT NULL, 
   revision_number integer NOT NULL, 
   revision_date timestamp with time zone NOT NULL, 
   revisor_id integer NOT NULL, 
   entity_id integer NOT NULL, 
   slug character varying(100),
   head text DEFAULT ''::text,
   credits text DEFAULT ''::text,
   "off" text DEFAULT ''::text,
   cue text DEFAULT ''::text,
   footer text DEFAULT ''::text,
   CONSTRAINT story_revision_pkey PRIMARY KEY (id), 
   CONSTRAINT revisor_fkey FOREIGN KEY (revisor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT entity_fkey FOREIGN KEY (entity_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_revision OWNER TO anews;

--
-- Adiciona o CASCADE para apagar os guides junto com a guideline
--
ALTER TABLE anews.guide DROP CONSTRAINT fk_tariff;
ALTER TABLE anews.guide ADD CONSTRAINT fk_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- Adiciona o CASCADE para apagar a ligação de guide com contato quando a guide é apagada
--
ALTER TABLE anews.guide_has_contact DROP CONSTRAINT fk_guide_contact;
ALTER TABLE anews.guide_has_contact ADD CONSTRAINT fk_guide_contact FOREIGN KEY (guide_id) REFERENCES anews.guide (id) ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- Adiciona o CASCADE para apagar as reportage_section junto com a reportagem
--
ALTER TABLE anews.reportage_section DROP CONSTRAINT reportage_fkey;
ALTER TABLE anews.reportage_section ADD CONSTRAINT reportage_fkey FOREIGN KEY (reportage_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- Adiciona o CASCADE na foreign key de story_section para story
--
--DELETE FROM anews.story_section WHERE story_id NOT IN (SELECT id FROM anews.story);
ALTER TABLE anews.story_section ADD CONSTRAINT fk_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- Adiciona o CASCADE na foreign key de credit para story
--
ALTER TABLE anews.credit DROP CONSTRAINT fk_credit_story;
ALTER TABLE anews.credit ADD CONSTRAINT fk_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- Adiciona o CASCADE na foreign key de credit_field para credit
--
ALTER TABLE anews.credit_field DROP CONSTRAINT fk_credit;
ALTER TABLE anews.credit_field ADD CONSTRAINT fk_credit FOREIGN KEY (credit_id) REFERENCES anews.credit (id) ON UPDATE NO ACTION ON DELETE CASCADE;

--
-- Apaga todas as versões
--
DELETE FROM anews.guideline WHERE parent_id IS NOT NULL;
DELETE FROM anews.reportage WHERE parent_id IS NOT NULL;
DELETE FROM anews.story WHERE parent_id IS NOT NULL;

--
-- Retira as colunas "parent_id" e "rev" da tabela "guideline"
--
ALTER TABLE anews.guideline DROP CONSTRAINT parent_fkey;
ALTER TABLE anews.guideline DROP COLUMN parent_id;
ALTER TABLE anews.guideline DROP COLUMN rev;

--
-- Retira as colunas "parent_id" e "rev" da tabela "story"
--
ALTER TABLE anews.story DROP COLUMN parent_id;
ALTER TABLE anews.story DROP COLUMN rev;

--
-- Retira as colunas "parent_id" e "rev" da tabela "reportage"
--
ALTER TABLE anews.reportage DROP CONSTRAINT fk_parent_id_reportage;
ALTER TABLE anews.reportage DROP COLUMN parent_id;
ALTER TABLE anews.reportage DROP COLUMN rev;

--
-- Adiciona coluna do autor da reportagem
--
ALTER TABLE anews.reportage ADD COLUMN author_id integer;
ALTER TABLE anews.reportage ADD CONSTRAINT author_fkey FOREIGN KEY (author_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
-- Adiciona a coluna para o controle de versão feito pelo Hibernate nas tabelas guideline, reportage e story
--
ALTER TABLE anews.guideline ADD COLUMN version integer NOT NULL DEFAULT 0;
ALTER TABLE anews.reportage ADD COLUMN version integer NOT NULL DEFAULT 0;
ALTER TABLE anews.story ADD COLUMN version integer NOT NULL DEFAULT 0;

--
-- Remove algumas constraints para trocar a chave reportage.id para reportage.guideline_id
--
ALTER TABLE anews.reportage_section DROP CONSTRAINT reportage_fkey;
ALTER TABLE anews.reportage_revision DROP CONSTRAINT entity_fkey;
ALTER TABLE anews.reportage DROP CONSTRAINT pk_reportage_id CASCADE;

--
-- Faz reportage_section apontar para reportage.guideline_id, e não reportage.id
--
UPDATE anews.reportage_section AS sec SET reportage_id = sub.guideline_id 
FROM (SELECT id, guideline_id FROM anews.reportage) AS sub
WHERE sec.reportage_id = sub.id;

--
-- Troca a referência de reportage_report para reportage
--

--ALTER TABLE anews.reportage_report DROP CONSTRAINT fk_reportage_report_reportage;

--
-- Faz reportage_report apontar para reportage.guideline_id, e não reportage.id
--
UPDATE anews.reportage_report AS rep SET reportage_id = sub.guideline_id 
FROM (SELECT id, guideline_id FROM anews.reportage) AS sub
WHERE rep.reportage_id = sub.id;

--
-- Agora não precisa mais da coluna id
--
ALTER TABLE anews.reportage DROP COLUMN id;

--
-- Cria novamente as constraints, agora apontando para a coluna reportage.guideline_id
--
ALTER TABLE anews.reportage ADD CONSTRAINT reportage_pkey PRIMARY KEY (guideline_id);
ALTER TABLE anews.reportage_section ADD CONSTRAINT reportage_fkey FOREIGN KEY (reportage_id) REFERENCES anews.reportage (guideline_id) ON UPDATE NO ACTION ON DELETE CASCADE;
ALTER TABLE anews.reportage_revision ADD CONSTRAINT entity_fkey FOREIGN KEY (entity_id) REFERENCES anews.reportage (guideline_id) ON UPDATE NO ACTION ON DELETE CASCADE;
ALTER TABLE anews.reportage_report ADD CONSTRAINT fk_reportage_report_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (guideline_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE anews.log (
   id serial NOT NULL, 
   action text NOT NULL, 
   author_id integer NOT NULL, 
   rundown_id integer NOT NULL,
   date timestamp with time zone NOT NULL, 
   CONSTRAINT pk_log PRIMARY KEY (id), 
   CONSTRAINT fk_author_log FOREIGN KEY (author_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_rundown_log_rundown FOREIGN KEY (rundown_id) REFERENCES anews.rundown (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);

ALTER TABLE anews.log OWNER TO anews;

CREATE TABLE anews.rundown_log (
   log_id integer NOT NULL, 
   type_action character varying(100) NOT NULL,
   rundown_id integer NOT NULL, 
   CONSTRAINT pk_log_rundown_log PRIMARY KEY (log_id), 
   CONSTRAINT fk_rundownlog_rundown FOREIGN KEY (rundown_id) REFERENCES anews.rundown (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_rundownlog_log FOREIGN KEY (log_id) REFERENCES anews.log (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);

ALTER TABLE anews.rundown_log OWNER TO anews;

CREATE TABLE anews.block_log (
   log_id integer NOT NULL, 
   type_action character varying(100) NOT NULL, 
   block_id integer NOT NULL, 
   CONSTRAINT pk_log_block_log PRIMARY KEY (log_id), 
   CONSTRAINT fk_blocklog_block FOREIGN KEY (block_id) REFERENCES anews.block (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_block_log FOREIGN KEY (log_id) REFERENCES anews.log (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);

ALTER TABLE anews.block_log OWNER TO anews;

CREATE TABLE anews.story_log (
   log_id integer NOT NULL, 
   type_action character varying(100) NOT NULL, 
   story_id integer NOT NULL, 
   CONSTRAINT pk_log_story_log PRIMARY KEY (log_id), 
   CONSTRAINT fk_storylog_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_storylog_log FOREIGN KEY (log_id) REFERENCES anews.log (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_log OWNER TO anews;


ALTER TABLE anews.story DROP COLUMN type;


ALTER TABLE anews.program DROP CONSTRAINT fk_gc_device;
ALTER TABLE anews.credit  DROP CONSTRAINT IF EXISTS fk_story;
ALTER TABLE anews.credit  DROP CONSTRAINT IF EXISTS fk_credit_story;
ALTER TABLE anews.story_section DROP CONSTRAINT IF EXISTS fk_story;
ALTER TABLE anews.story_revision  DROP CONSTRAINT entity_fkey;
ALTER TABLE anews.story_log DROP CONSTRAINT fk_storylog_story;

ALTER TABLE anews.story DROP CONSTRAINT pk_story CASCADE;
--ALTER TABLE anews.story DROP CONSTRAINT fk_story_author;
--ALTER TABLE anews.story DROP CONSTRAINT fk_story_block;
--ALTER TABLE anews.story DROP CONSTRAINT fk_story_editor;
--ALTER TABLE anews.story DROP CONSTRAINT fk_story_footer_presenter;
--ALTER TABLE anews.story DROP CONSTRAINT fk_story_guideline;
--ALTER TABLE anews.story DROP CONSTRAINT fk_story_kind_id;

ALTER SEQUENCE anews.story_id_seq RENAME TO story_old_id_seq;
ALTER SEQUENCE anews.story_section_id_seq RENAME TO story_section_old_id_seq;

ALTER TABLE anews.story RENAME TO story_old;
ALTER TABLE anews.program RENAME gc_device_id TO cg_device_id;

ALTER TABLE anews.story_old ADD CONSTRAINT pk_story_old PRIMARY KEY (id);
--ALTER TABLE anews.story_old ADD CONSTRAINT fk_story_author FOREIGN KEY (author_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE anews.story_old ADD CONSTRAINT fk_story_block FOREIGN KEY (block_id) REFERENCES anews.block (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE anews.story_old ADD CONSTRAINT fk_story_editor FOREIGN KEY (editor_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE anews.story_old ADD CONSTRAINT fk_story_footer_presenter FOREIGN KEY (footer_presenter_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE anews.story_old ADD CONSTRAINT fk_story_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
--ALTER TABLE anews.story_old ADD CONSTRAINT fk_story_kind_id FOREIGN KEY (story_kind_id) REFERENCES anews.story_kind (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.credit ADD CONSTRAINT fk_credit_story_old FOREIGN KEY (story_id) REFERENCES anews.story_old (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_log ADD CONSTRAINT fk_storylog_story FOREIGN KEY (story_id) REFERENCES  anews.story_old (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_revision ADD CONSTRAINT entity_fkey FOREIGN KEY (entity_id) REFERENCES anews.story_old (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;
      
ALTER TABLE anews.story_section DROP CONSTRAINT pk_story_section;
ALTER TABLE anews.story_section DROP CONSTRAINT fk_story_section_presenter;

ALTER TABLE anews.story_section RENAME TO story_section_old;

ALTER TABLE anews.story_section_old ADD CONSTRAINT pk_story_section_old PRIMARY KEY (id);
ALTER TABLE anews.story_section_old ADD CONSTRAINT fk_story_old FOREIGN KEY (story_id) REFERENCES anews.story_old (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE;
ALTER TABLE anews.story_section_old ADD CONSTRAINT fk_story_section_presenter FOREIGN KEY (presenter_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE anews.story_section (
  id serial NOT NULL,
  CONSTRAINT pk_story_section PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE anews.story_section OWNER TO anews; 
      
CREATE TABLE anews.story (
  id serial NOT NULL,
  "order" integer NOT NULL,
  page character varying(3) NOT NULL,
  expected time without time zone NOT NULL,
  head time without time zone NOT NULL,
  ok boolean NOT NULL,
  block_id integer,
  vt time without time zone NOT NULL,
  footer_note time without time zone NOT NULL,
  off time without time zone NOT NULL,
  total time without time zone NOT NULL,
  date timestamp without time zone NOT NULL,
  author_id integer,
  editor_id integer,
  guideline_id integer,
  approved boolean NOT NULL,
  slug character varying(40) DEFAULT ''::character varying,
  excluded boolean NOT NULL,
  drawer boolean NOT NULL DEFAULT false,
  story_kind_id integer,
  head_id integer,
  off_id integer,
  vt_id integer,
  footer_note_id integer,
  version integer NOT NULL DEFAULT 0,
  CONSTRAINT pk_story PRIMARY KEY (id ),
  CONSTRAINT fk_story_author FOREIGN KEY (author_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_block FOREIGN KEY (block_id) REFERENCES anews.block (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_editor FOREIGN KEY (editor_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_story_kind_id FOREIGN KEY (story_kind_id) REFERENCES anews.story_kind (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_head FOREIGN KEY (head_id) REFERENCES anews.story_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_off FOREIGN KEY (off_id) REFERENCES anews.story_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_vt FOREIGN KEY (vt_id) REFERENCES anews.story_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_footer_note FOREIGN KEY (footer_note_id) REFERENCES anews.story_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS=FALSE);

ALTER TABLE anews.story OWNER TO anews;

CREATE TABLE anews.story_vt_section (
  story_section_id integer NOT NULL,
  cue text DEFAULT ''::text,
 "time" time without time zone NOT NULL,
  CONSTRAINT pk_story_vt_section PRIMARY KEY (story_section_id ),
  CONSTRAINT fk_story_vt_section_id FOREIGN KEY (story_section_id) REFERENCES anews.story_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS=FALSE);

ALTER TABLE anews.story_vt_section OWNER TO anews; 
  
CREATE TABLE anews.story_sub_section (
  id serial NOT NULL,
  "order" integer NOT NULL,
  story_section_id integer NOT NULL, 
  parent_id integer,
  CONSTRAINT pk_story_sub_section PRIMARY KEY (id ),
  CONSTRAINT fk_story_section FOREIGN KEY (story_section_id) REFERENCES anews.story_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS=FALSE);

ALTER TABLE anews.story_sub_section OWNER TO anews; 
  
CREATE TABLE anews.story_text (
  story_sub_section_id integer NOT NULL,
  read_time time without time zone NOT NULL,
  text text DEFAULT ''::text,
  presenter_id integer NOT NULL,
  CONSTRAINT pk_story_text_sub_section PRIMARY KEY (story_sub_section_id),
  CONSTRAINT fk_story_text_sub_section FOREIGN KEY (story_sub_section_id) REFERENCES anews.story_sub_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_story_text_presenter FOREIGN KEY (presenter_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION    
) WITH (OIDS=FALSE);

ALTER TABLE anews.story_text OWNER TO anews; 
  
CREATE TABLE anews.story_camera_text (
  story_sub_section_id integer NOT NULL,
  read_time time without time zone NOT NULL,
  text text DEFAULT ''::text,
  camera integer NOT NULL,
  presenter_id integer NOT NULL,
  CONSTRAINT pk_story_camera_sub_section PRIMARY KEY (story_sub_section_id),
  CONSTRAINT fk_story_camera_sub_section FOREIGN KEY (story_sub_section_id) REFERENCES anews.story_sub_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_story_camera_presenter FOREIGN KEY (presenter_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION    
) WITH (OIDS=FALSE);
ALTER TABLE anews.story_camera_text OWNER TO anews; 
  
CREATE TABLE anews.story_mos_obj (
  story_sub_section_id integer NOT NULL,
  mos_obj_id integer NOT NULL,
  CONSTRAINT pk_story_mos_sub_section PRIMARY KEY (story_sub_section_id),
  CONSTRAINT fk_story_mos_sub_section FOREIGN KEY (story_sub_section_id) REFERENCES anews.story_sub_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_story_mos_obj FOREIGN KEY (mos_obj_id) REFERENCES anews.mos_obj (id) ON UPDATE NO ACTION ON DELETE NO ACTION  
) WITH (OIDS=FALSE);
ALTER TABLE anews.story_mos_obj OWNER TO anews; 

ALTER TABLE anews.gc_device RENAME TO cg_device;
ALTER TABLE anews.gc_device_id_seq RENAME TO cg_device_id_seq;



--CREATE TABLE anews.cg_device (
--  id serial NOT NULL,
--  port integer NOT NULL,
--  name character varying(60) NOT NULL,
--  code_increment integer NOT NULL,
--  ip character varying(20),
--  excluded boolean NOT NULL DEFAULT false,
--  virtual boolean NOT NULL DEFAULT true,
--  CONSTRAINT pk_cg_device PRIMARY KEY (id )
--) WITH (OIDS=FALSE);
--ALTER TABLE anews.cg_device OWNER TO anews;

ALTER TABLE anews.program ADD CONSTRAINT fk_cg_device FOREIGN KEY (cg_device_id) REFERENCES anews.cg_device (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.template_message_base RENAME TO cg_template;
ALTER TABLE anews.template_message_base_id_seq RENAME TO cg_template_id_seq;
ALTER TABLE anews.cg_template RENAME gc_device_id  TO cg_device_id;
ALTER TABLE anews.cg_template DROP CONSTRAINT fk_gc_device;
ALTER TABLE anews.cg_template ADD CONSTRAINT fk_cg_device FOREIGN KEY (cg_device_id) REFERENCES anews.cg_device (id) ON UPDATE NO ACTION ON DELETE NO ACTION;


--CREATE TABLE anews.cg_template (
--  id serial NOT NULL,
--  cg_device_id integer NOT NULL,
--  code integer NOT NULL,
--  name character varying(60) NOT NULL,
--  excluded boolean NOT NULL DEFAULT false,
--  note boolean NOT NULL DEFAULT false,
--  CONSTRAINT pk_cg_template PRIMARY KEY (id),
--  CONSTRAINT fk_template_cg_device FOREIGN KEY (cg_device_id) REFERENCES anews.cg_device (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
--) WITH (OIDS=FALSE);
--ALTER TABLE anews.cg_template OWNER TO anews;  

ALTER TABLE anews.field_template_message RENAME TO cg_field;
ALTER TABLE anews.field_template_message_id_seq RENAME TO cg_field_id_seq;
ALTER TABLE anews.cg_field RENAME template_message_base_id  TO cg_template_id;
ALTER TABLE anews.cg_field DROP CONSTRAINT fk_template_message_base;
ALTER TABLE anews.cg_field ADD CONSTRAINT fk_cg_template FOREIGN KEY (cg_template_id) REFERENCES anews.cg_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION;




--CREATE TABLE anews.cg_field (
--  id serial NOT NULL,
--  "number" integer NOT NULL,
--  name character varying(60) NOT NULL,
--  size integer,
--  cg_template_id integer NOT NULL,
--  CONSTRAINT pk_cg_field PRIMARY KEY (id),
--  CONSTRAINT fk_cg_field_template FOREIGN KEY (cg_template_id) REFERENCES anews.cg_template (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
--) WITH (OIDS=FALSE);
--ALTER TABLE anews.cg_field OWNER TO anews;
  
CREATE TABLE anews.story_cg (
  story_sub_section_id integer NOT NULL,
  code integer NOT NULL,
  cg_template_id integer NOT NULL,
  CONSTRAINT pk_story_cg_sub_section PRIMARY KEY (story_sub_section_id),
  CONSTRAINT fk_story_cg_sub_section FOREIGN KEY (story_sub_section_id) REFERENCES anews.story_sub_section (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_story_cg_template FOREIGN KEY (cg_template_id) REFERENCES anews.cg_template (id) ON UPDATE NO ACTION ON DELETE NO ACTION  
) WITH (OIDS=FALSE);
ALTER TABLE anews.story_cg OWNER TO anews; 
  
CREATE TABLE anews.story_cg_field (
  id serial NOT NULL,
  "number" integer NOT NULL,
  value text NOT NULL,
  story_cg_id integer NOT NULL,
  CONSTRAINT pk_story_cg_field PRIMARY KEY (id),
  CONSTRAINT fk_story_cg FOREIGN KEY (story_cg_id) REFERENCES anews.story_cg (story_sub_section_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS=FALSE);
ALTER TABLE anews.story_cg_field OWNER TO anews; 
  
ALTER TABLE anews.story_revision DROP CONSTRAINT entity_fkey;
ALTER TABLE anews.story_revision DROP CONSTRAINT revisor_fkey;
ALTER TABLE anews.story_revision ADD CONSTRAINT revisor_fkey FOREIGN KEY (revisor_id) REFERENCES anews."user" (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_revision ADD CONSTRAINT entity_fkey FOREIGN KEY (entity_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.story_log DROP CONSTRAINT fk_storylog_story; 
ALTER TABLE anews.story_log ADD CONSTRAINT fk_storylog_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
  
-- Renomea a tabela mos_obj para mos_object
ALTER TABLE anews.mos_obj RENAME TO mos_object;
ALTER TABLE anews.mos_obj_id_seq RENAME TO mos_object_id_seq;

-- Troca a coluna obj_air para ready com o tipo boolean
ALTER TABLE anews.mos_object DROP COLUMN obj_air;
ALTER TABLE anews.mos_object ADD COLUMN ready boolean NOT NULL DEFAULT false;

--
-- Renomeia as seguintes colunas da tabela mos_object:
--
--   1) obj_type para type
--   2) obj_id para object_id
--   3) obj_slug para slug
--   4) mos_abstract para mos_abstract
--   5) obj_group para group
--   6) obj_rev para revision
--   7) obj_duration para duration
--
ALTER TABLE anews.mos_object RENAME obj_type TO type;
ALTER TABLE anews.mos_object RENAME obj_id TO object_id;
ALTER TABLE anews.mos_object RENAME obj_slug TO slug;
ALTER TABLE anews.mos_object RENAME mos_abstract TO abstract_text;
ALTER TABLE anews.mos_object RENAME obj_group TO "group";
ALTER TABLE anews.mos_object RENAME obj_rev TO revision;
ALTER TABLE anews.mos_object RENAME obj_duration TO duration;

-- Renomeia a tabela mos_obj_metadata_path para mos_object_metadata_path
ALTER TABLE anews.mos_obj_metadata_path RENAME TO mos_object_metadata_path;

-- Troca o relacionamento do metadata path para ser one-to-one
ALTER TABLE anews.mos_object_metadata_path DROP CONSTRAINT mos_obj_metadata_path_pk;
ALTER TABLE anews.mos_object_metadata_path DROP CONSTRAINT mos_obj_fk;
ALTER TABLE anews.mos_object_metadata_path DROP COLUMN id;
ALTER TABLE anews.mos_object_metadata_path RENAME mos_obj_id TO mos_object_id;
ALTER TABLE anews.mos_object_metadata_path ADD CONSTRAINT mos_object_metadata_path_id PRIMARY KEY (mos_object_id);
ALTER TABLE anews.mos_object_metadata_path ADD CONSTRAINT mos_object_fkey FOREIGN KEY (mos_object_id) REFERENCES anews.mos_object (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- Renomeia a tabela mos_obj_path para mos_object_path
ALTER TABLE anews.mos_obj_path RENAME TO mos_object_path;
ALTER TABLE anews.mos_obj_path_id_seq RENAME TO mos_object_path_id_seq;
ALTER TABLE anews.mos_object_path RENAME mos_obj_id TO mos_object_id;

-- Renomeia a tabela mos_obj_proxy_path para mos_object_proxy_path
ALTER TABLE anews.mos_obj_proxy_path RENAME TO mos_object_proxy_path;
ALTER TABLE anews.mos_obj_proxy_path_id_seq RENAME TO mos_object_proxy_path_id_seq;
ALTER TABLE anews.mos_object_proxy_path RENAME mos_obj_id TO mos_object_id;

ALTER TABLE anews.mos_object_path RENAME TO mos_path;
ALTER TABLE anews.mos_object_path_id_seq RENAME TO mos_path_id_seq;

ALTER TABLE anews.mos_object_proxy_path RENAME TO mos_proxy_path;
ALTER TABLE anews.mos_object_proxy_path_id_seq RENAME TO mos_proxy_path_id_seq;

ALTER TABLE anews.mos_object_metadata_path RENAME TO mos_metadata_path;

ALTER TABLE anews.mos_object RENAME TO mos_media;
ALTER TABLE anews.mos_object_id_seq RENAME TO mos_media_id_seq;

ALTER TABLE anews.mos_path RENAME mos_object_id TO mos_media_id;
ALTER TABLE anews.mos_proxy_path RENAME mos_object_id TO mos_media_id;
ALTER TABLE anews.mos_metadata_path RENAME mos_object_id TO mos_media_id;

ALTER INDEX anews.mos_obj_pk RENAME TO mos_media_pkey;
ALTER INDEX anews.mos_object_metadata_path_id RENAME TO mos_metadata_path_pkey;
ALTER INDEX anews.mos_obj_path_pk RENAME TO mos_path_pkey;
ALTER INDEX anews.mos_obj_proxy_path_pk RENAME TO mos_proxy_path_pkey;

ALTER TABLE anews.mos_device RENAME type TO type_old;
ALTER TABLE anews.mos_device ADD COLUMN type character varying(10);

UPDATE anews.mos_device SET type = 'MAM' where type_old = 0;
UPDATE anews.mos_device SET type = 'CG' where type_old = 1;
UPDATE anews.mos_device SET type = 'TP' where type_old = 2;
UPDATE anews.mos_device SET type = 'PLAYOUT' where type_old = 3;

ALTER TABLE anews.mos_device DROP COLUMN type_old;

-- Remove a coluna excluded de mos_media
ALTER TABLE anews.mos_media DROP COLUMN excluded;
ALTER TABLE anews.mos_media ALTER COLUMN slug DROP NOT NULL;
ALTER TABLE anews.mos_media ALTER COLUMN created_by DROP NOT NULL;
ALTER TABLE anews.mos_media ALTER COLUMN created DROP NOT NULL;
ALTER TABLE anews.mos_media ALTER COLUMN changed_by DROP NOT NULL;
ALTER TABLE anews.mos_media ALTER COLUMN description DROP NOT NULL;

ALTER TABLE anews.mos_metadata_path DROP CONSTRAINT mos_metadata_path_pkey;
ALTER TABLE anews.mos_metadata_path DROP CONSTRAINT mos_object_fkey;
ALTER TABLE anews.mos_metadata_path DROP COLUMN mos_media_id;
ALTER TABLE anews.mos_metadata_path ADD COLUMN id serial NOT NULL;
ALTER TABLE anews.mos_metadata_path ADD CONSTRAINT mos_metadata_path_pkey PRIMARY KEY (id);

ALTER TABLE anews.mos_media ADD COLUMN metadata_id integer NOT NULL;
ALTER TABLE anews.mos_media ADD CONSTRAINT mos_metadata_path_fkey FOREIGN KEY (metadata_id) REFERENCES anews.mos_metadata_path (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.mos_media ALTER COLUMN mos_device_id SET NOT NULL;
ALTER TABLE anews.mos_media ALTER COLUMN metadata_id DROP NOT NULL;

ALTER TABLE anews.story_vt_section DROP COLUMN "time";
ALTER TABLE anews.story DROP COLUMN head;
ALTER TABLE anews.story DROP COLUMN vt;
ALTER TABLE anews.story DROP COLUMN footer_note;
ALTER TABLE anews.story DROP COLUMN off;

ALTER TABLE anews.story_section ADD COLUMN duration time without time zone;
-- Adiciona a coluna mos_revision à tabela mos_device
ALTER TABLE anews.mos_device ADD COLUMN mos_revision character varying(10) NOT NULL DEFAULT '2.8.4';

ALTER TABLE anews.rundown ADD COLUMN mos_status character varying(10);
ALTER TABLE anews.story ADD COLUMN mos_status character varying(10);

ALTER TABLE anews.story_sub_section ALTER COLUMN story_section_id DROP NOT NULL;
ALTER TABLE anews.story_sub_section DROP CONSTRAINT fk_story_section;
ALTER TABLE anews.story_sub_section ADD CONSTRAINT story_section_fkey FOREIGN KEY (story_section_id)
      REFERENCES anews.story_section (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE;
ALTER TABLE anews.story_sub_section ADD CONSTRAINT sub_section_fkey FOREIGN KEY (parent_id) 
      REFERENCES anews.story_sub_section (id) 
      ON UPDATE NO ACTION ON DELETE CASCADE;


ALTER TABLE anews.story_text ALTER COLUMN presenter_id DROP NOT NULL;

ALTER TABLE anews.story DROP CONSTRAINT fk_footer_note;
ALTER TABLE anews.story DROP CONSTRAINT fk_head;
ALTER TABLE anews.story DROP CONSTRAINT fk_off;
ALTER TABLE anews.story DROP CONSTRAINT fk_story_author;
ALTER TABLE anews.story DROP CONSTRAINT fk_story_block;
ALTER TABLE anews.story DROP CONSTRAINT fk_story_editor;
ALTER TABLE anews.story DROP CONSTRAINT fk_story_guideline;
ALTER TABLE anews.story DROP CONSTRAINT fk_story_kind_id;
ALTER TABLE anews.story DROP CONSTRAINT fk_vt;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_author FOREIGN KEY (author_id) REFERENCES anews."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_block FOREIGN KEY (block_id) REFERENCES anews.block (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_editor FOREIGN KEY (editor_id) REFERENCES anews."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_kind_id FOREIGN KEY (story_kind_id) REFERENCES anews.story_kind (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_footer_note FOREIGN KEY (footer_note_id) REFERENCES anews.story_section (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE anews.story ADD CONSTRAINT fk_head FOREIGN KEY (head_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_vt FOREIGN KEY (vt_id) REFERENCES anews.story_section (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE anews.story ADD CONSTRAINT fk_off FOREIGN KEY (off_id) REFERENCES anews.story_section (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE anews.story_sub_section DROP CONSTRAINT story_section_fkey;
ALTER TABLE anews.story_sub_section DROP CONSTRAINT sub_section_fkey;
ALTER TABLE anews.story_sub_section ADD CONSTRAINT fk_story_section FOREIGN KEY (story_section_id) REFERENCES anews.story_section (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE anews.story_sub_section ADD CONSTRAINT fk_sub_section FOREIGN KEY (parent_id) REFERENCES anews.story_sub_section (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE anews.story_mos_obj DROP CONSTRAINT fk_story_mos_obj;
ALTER TABLE anews.story_mos_obj DROP CONSTRAINT fk_story_mos_sub_section;
ALTER TABLE anews.story_mos_obj RENAME mos_obj_id  TO mos_media_id;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_story_mos_sub_section FOREIGN KEY (story_sub_section_id) REFERENCES anews.story_sub_section (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE anews.story_text DROP CONSTRAINT fk_story_text_presenter;
ALTER TABLE anews.story_text DROP CONSTRAINT fk_story_text_sub_section;
ALTER TABLE anews.story_text ADD CONSTRAINT fk_story_text_presenter FOREIGN KEY (presenter_id) REFERENCES anews."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_text ADD CONSTRAINT fk_story_text_sub_section FOREIGN KEY (story_sub_section_id) REFERENCES anews.story_sub_section (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE anews.story ADD COLUMN "head" time without time zone NOT NULL default now();
ALTER TABLE anews.story ADD COLUMN "vt" time without time zone NOT NULL default now();

-- Table: story_off_section

CREATE TABLE anews.story_off_section (
  story_section_id integer NOT NULL,
  time_image time without time zone NOT NULL,
  CONSTRAINT pk_story_off_section PRIMARY KEY (story_section_id),
  CONSTRAINT fk_story_off_section FOREIGN KEY (story_section_id)
      REFERENCES anews.story_section (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
) WITH (OIDS=FALSE);
ALTER TABLE anews.story_off_section OWNER TO anews;


ALTER TABLE anews.mos_device ADD COLUMN channel character varying(128) NOT NULL DEFAULT 0;

INSERT INTO anews.permission(id, name, permission_id)  VALUES ('010712', 'Ver Logs', '0107');

ALTER TABLE anews.block_log DROP CONSTRAINT fk_blocklog_block;
ALTER TABLE anews.block_log DROP COLUMN block_id;
ALTER TABLE anews.block_log ADD COLUMN "block_name" character varying(30);

--
--	Não dá para manter o que já tem de revisões
--
DELETE FROM anews.story_revision;

ALTER TABLE anews.story_revision DROP COLUMN credits;
ALTER TABLE anews.story_revision RENAME head TO head_section;
ALTER TABLE anews.story_revision RENAME off TO off_section;
ALTER TABLE anews.story_revision RENAME cue TO vt_section;
ALTER TABLE anews.story_revision RENAME footer TO footer_section;

--
--	No banco de testes estava sem a constraint, não sei o porquê
--

DELETE FROM anews.story_report WHERE story_id NOT IN (
	SELECT id FROM anews.story
);

ALTER TABLE anews.story_report DROP CONSTRAINT IF EXISTS fk_story_report_story;
ALTER TABLE anews.story_report ADD CONSTRAINT fk_story_report_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
--	Indexes para acelerar a migração para a nova estrutura da lauda
--
CREATE INDEX idx_credit_story_id ON anews.credit (story_id);
CREATE INDEX idx_story_section_old_story_id ON anews.story_section_old (story_id);
CREATE INDEX idx_credit_field_credit_id ON anews.credit_field (credit_id);

ALTER TABLE anews.reportage ADD COLUMN id serial NOT NULL;

ALTER TABLE anews.reportage_section DROP CONSTRAINT reportage_fkey;
ALTER TABLE anews.reportage_report DROP CONSTRAINT fk_reportage_report_reportage;
ALTER TABLE anews.reportage_revision DROP CONSTRAINT entity_fkey;

update anews.reportage_section as rs set reportage_id = (select id from anews.reportage as r where r.guideline_id = rs.reportage_id);
update anews.reportage_report as rs set reportage_id = (select id from anews.reportage as r where r.guideline_id = rs.reportage_id);
update anews.reportage_revision as rs set entity_id = (select id from anews.reportage as r where r.guideline_id = rs.entity_id);

ALTER TABLE anews.reportage DROP CONSTRAINT reportage_pkey;
ALTER TABLE anews.reportage ADD CONSTRAINT reportage_pkey PRIMARY KEY (id);

ALTER TABLE anews.reportage_report ADD CONSTRAINT fk_reportage_report_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_revision ADD CONSTRAINT fk_reportage_revision_reportage FOREIGN KEY (entity_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_section ADD CONSTRAINT fk_reportage_section_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.reportage ALTER COLUMN guideline_id DROP NOT NULL;

ALTER TABLE anews.reportage ADD COLUMN reporter_id integer;
update anews.reportage as r set reporter_id = (select reporter_id from anews.guideline where r.guideline_id = id );
ALTER TABLE anews.reportage ALTER COLUMN reporter_id SET NOT NULL;

ALTER TABLE anews.reportage ADD COLUMN program_id integer;
update anews.reportage as r set program_id = (select program_id from anews.guideline where r.guideline_id = id);
ALTER TABLE anews.reportage ALTER COLUMN program_id SET NOT NULL;

ALTER TABLE anews.reportage ADD COLUMN slug character varying(40);
update anews.reportage as r set slug = (select slug from anews.guideline as g where r.guideline_id = g.id);
ALTER TABLE anews.reportage ALTER COLUMN slug SET NOT NULL;

SELECT setval('anews.reportage_id_seq', (SELECT MAX(id) FROM anews.reportage));
ALTER TABLE anews.reportage ALTER COLUMN date TYPE date;


ALTER TABLE anews.story DROP CONSTRAINT fk_off;
ALTER TABLE anews.story RENAME off_id  TO nc_id;
ALTER TABLE anews.story_off_section DROP CONSTRAINT pk_story_off_section;
ALTER TABLE anews.story_off_section DROP CONSTRAINT fk_story_off_section;
ALTER TABLE anews.story_off_section RENAME TO story_nc_section;
ALTER TABLE anews.story_revision RENAME off_section  TO nc_section;
ALTER TABLE anews.story_nc_section ADD CONSTRAINT pk_story_nc_section PRIMARY KEY (story_section_id);
ALTER TABLE anews.story_nc_section ADD CONSTRAINT fk_story_nc_section_story_section FOREIGN KEY (story_section_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_story_nc_section FOREIGN KEY (nc_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.story ADD COLUMN information text;

ALTER TABLE anews.story ADD COLUMN off_id integer;
ALTER TABLE anews.story ADD CONSTRAINT fk_off FOREIGN KEY (off_id) REFERENCES anews.story_section (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE  anews.story ADD COLUMN off time without time zone NOT NULL DEFAULT now();

INSERT INTO anews.permission VALUES ('010602', 'Adicionar', '0106');
INSERT INTO anews.permission VALUES ('010604', 'Deletar', '0106');

ALTER TABLE anews.reportage_revision ADD COLUMN date character varying(10) NOT NULL DEFAULT '';
ALTER TABLE anews.reportage_revision ADD COLUMN slug character varying(40) NOT NULL DEFAULT '';
ALTER TABLE anews.reportage_revision ADD COLUMN program character varying(150) NOT NULL DEFAULT '';
ALTER TABLE anews.reportage_revision ADD COLUMN reporter character varying(20) NOT NULL DEFAULT '';

ALTER TABLE anews.guideline_revision ADD COLUMN date character varying(10) NOT NULL DEFAULT '';
ALTER TABLE anews.guideline_revision ADD COLUMN program character varying(150) NOT NULL DEFAULT '';
ALTER TABLE anews.guideline_revision ALTER COLUMN slug TYPE character varying(40);

ALTER TABLE anews.story_kind ALTER COLUMN acronym TYPE character varying(4);

ALTER TABLE anews.story ADD COLUMN reportage_id integer;

ALTER TABLE anews.story_revision ADD COLUMN story_kind character varying(4) DEFAULT '';
ALTER TABLE anews.story_revision ADD COLUMN information text DEFAULT '';
ALTER TABLE anews.story_revision ADD COLUMN off_section text DEFAULT '';

update anews.guideline set proposal = 'proposta'  where proposal is NULL or proposal = ''; 

CREATE INDEX idx_contact_number_contact ON anews.contact_number (contact_id DESC NULLS LAST);

CREATE INDEX idx_guide_guideline ON anews.guide (guideline_id DESC NULLS LAST);

CREATE INDEX idx_reportage_section_reportage ON anews.reportage_section (reportage_id DESC NULLS LAST);

CREATE INDEX idx_mos_path_mos_media ON anews.mos_path (mos_media_id DESC NULLS LAST);
CREATE INDEX idx_mos_proxy_path_mos_media ON anews.mos_proxy_path (mos_media_id DESC NULLS LAST);

CREATE INDEX idx_rundown_program_and_date ON anews.rundown (date DESC NULLS LAST, program_id ASC NULLS LAST);
CREATE INDEX idx_block_rundown ON anews.block (rundown_id DESC NULLS LAST);
CREATE INDEX idx_story_block ON anews.story (block_id DESC NULLS LAST);

CREATE INDEX idx_story_sub_section_parent ON anews.story_sub_section (parent_id DESC NULLS LAST);
CREATE INDEX idx_story_sub_section_story_section ON anews.story_sub_section (story_section_id DESC NULLS LAST);

CREATE INDEX idx_guideline_revision_entity ON anews.guideline_revision (entity_id DESC NULLS LAST);
CREATE INDEX idx_reportage_revision_entity ON anews.reportage_revision (entity_id DESC NULLS LAST);
CREATE INDEX idx_story_revision_entity ON anews.story_revision (entity_id DESC NULLS LAST);

CREATE INDEX idx_report_report_event ON anews.report (event_id DESC NULLS LAST);
CREATE INDEX idx_report_report_nature ON anews.report (nature_id DESC NULLS LAST);

CREATE INDEX idx_log_rundown ON anews.log (rundown_id DESC NULLS LAST);
CREATE INDEX idx_story_log_story ON anews.story_log (story_id DESC NULLS LAST);
