CREATE TABLE anews.checklist (
   id serial NOT NULL,
   version integer NOT NULL DEFAULT 0,
   author_id integer NOT NULL,
   date date NOT NULL,
   schedule time without time zone,
   departure time without time zone,
   arrival time without time zone,
   slug character varying(40) NOT NULL DEFAULT '',
   team character varying(30),
   local character varying(30),
   phone character varying(30),
   done boolean NOT NULL DEFAULT false,
   responsible_id integer,
   reporter_id integer,
   guideline_id integer,
   CONSTRAINT pk_checklist PRIMARY KEY (id),
   CONSTRAINT fk_checklist_user_author FOREIGN KEY (author_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_checklist_user_responsible FOREIGN KEY (responsible_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_checklist_user_reporter FOREIGN KEY (reporter_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_checklist_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);

ALTER TABLE anews.checklist OWNER TO anews;

CREATE TABLE anews.checklist_task (
   id serial NOT NULL,
   label character varying(30) NOT NULL,
   done boolean NOT NULL DEFAULT false,
   checklist_id integer NOT NULL,
   CONSTRAINT pk_checklist_task PRIMARY KEY (id),
   CONSTRAINT fk_checklist_task_checklist FOREIGN KEY (checklist_id) REFERENCES anews.checklist (id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);

ALTER TABLE anews.checklist_task OWNER TO anews;

CREATE TABLE anews.checklist_revision (
   id serial NOT NULL, 
   revision_number integer NOT NULL, 
   revision_date timestamp with time zone NOT NULL, 
   revisor_id integer NOT NULL, 
   entity_id integer NOT NULL, 
   date character varying(10) NOT NULL, 
   schedule character varying(5) NOT NULL, 
   departure character varying(5) NOT NULL, 
   arrival character varying(5) NOT NULL, 
   slug character varying(40) NOT NULL, 
   team character varying(30) NOT NULL, 
   local character varying(30) NOT NULL, 
   phone character varying(30) NOT NULL, 
   responsible character varying(20) NOT NULL, 
   reporter character varying(20) NOT NULL, 
   resources text NOT NULL, 
   tasks text NOT NULL, 
   CONSTRAINT pk_checklist_revision PRIMARY KEY (id), 
   CONSTRAINT fk_checklist_revision_checklist FOREIGN KEY (entity_id) REFERENCES anews.checklist (id) ON UPDATE NO ACTION ON DELETE CASCADE, 
   CONSTRAINT fk_checklist_revision_user FOREIGN KEY (revisor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);

ALTER TABLE anews.checklist_revision OWNER TO anews;

CREATE TABLE anews.checklist_type (
   id serial NOT NULL,
   name character varying(30) NOT NULL,
   CONSTRAINT pk_checklist_type PRIMARY KEY (id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.checklist_type OWNER TO anews;

ALTER TABLE anews.checklist_task ALTER COLUMN label TYPE character varying(100);

ALTER TABLE anews.checklist ADD COLUMN type_id integer;
ALTER TABLE anews.checklist ADD CONSTRAINT fk_checklist_checklist_type FOREIGN KEY (type_id) REFERENCES anews.checklist_type (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.checklist_revision ADD COLUMN type character varying(30) NOT NULL;

CREATE TABLE anews.checklist_resource (
  id serial NOT NULL,
  name character varying(30) NOT NULL,
  CONSTRAINT pk_checklist_resource PRIMARY KEY (id)
) WITH (OIDS=FALSE);

ALTER TABLE anews.checklist_resource OWNER TO anews;

ALTER TABLE anews.checklist ADD COLUMN info text;
ALTER TABLE anews.checklist_revision ADD COLUMN info text;

CREATE TABLE anews.checklist_has_resource (
   checklist_id integer NOT NULL,
   resource_id integer NOT NULL,
   CONSTRAINT pk_checklist_has_resource PRIMARY KEY (checklist_id, resource_id),
   CONSTRAINT fk_checklist_has_resource_checklist FOREIGN KEY (checklist_id) REFERENCES anews.checklist (id) ON UPDATE NO ACTION ON DELETE CASCADE,
   CONSTRAINT fk_checklist_has_resource_checklist_resource FOREIGN KEY (resource_id) REFERENCES anews.checklist_resource (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);

ALTER TABLE anews.checklist_has_resource OWNER TO anews;

ALTER TABLE anews.checklist ADD COLUMN excluded boolean NOT NULL DEFAULT false;
ALTER TABLE anews.checklist ADD COLUMN modified timestamp with time zone NOT NULL DEFAULT now();

ALTER TABLE anews.settings ADD COLUMN cleaning_trash_checklist integer NOT NULL DEFAULT 3;

ALTER TABLE anews.checklist DROP CONSTRAINT fk_checklist_guideline;
ALTER TABLE anews.checklist ADD CONSTRAINT fk_checklist_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE anews.checklist ADD COLUMN tapes character varying(30);

ALTER TABLE anews.checklist DROP COLUMN reporter_id;
ALTER TABLE anews.checklist DROP COLUMN phone;
ALTER TABLE anews.checklist RENAME responsible_id TO producer_id;

ALTER TABLE anews.checklist DROP CONSTRAINT fk_checklist_user_responsible;
ALTER TABLE anews.checklist ADD CONSTRAINT fk_checklist_user_producer FOREIGN KEY (producer_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.checklist_task ADD COLUMN info text;

ALTER TABLE anews.checklist_revision RENAME responsible TO producer;
ALTER TABLE anews.checklist_revision DROP COLUMN phone;
ALTER TABLE anews.checklist_revision DROP COLUMN reporter;

ALTER TABLE anews.checklist_revision ADD COLUMN tapes character varying(30) NOT NULL DEFAULT '';

ALTER TABLE anews.settings    ADD COLUMN color_checklist integer NOT NULL DEFAULT 9008501;

DELETE FROM anews.checklist_task;

ALTER TABLE anews.checklist_task ADD COLUMN index integer NOT NULL;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('0114', 'Mapa de Produção', '01');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011401', 'Visualizar', '0114');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011402', 'Modificar', '0114');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011403', 'Excluir', '0114');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011404', 'Imprimir', '0114');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('0115', 'Recursos de Produção', '01');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011501', 'Visualizar', '0115');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011502', 'Modificar', '0115');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011503', 'Excluir', '0115');

INSERT INTO anews.permission(id, name, permission_id) VALUES ('0116', 'Tipos de Produção', '01');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011601', 'Visualizar', '0116');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011602', 'Modificar', '0116');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011603', 'Excluir', '0116');

ALTER TABLE anews.checklist_task ALTER COLUMN label TYPE character varying(100);

ALTER TABLE anews.checklist ADD COLUMN program_id integer;
ALTER TABLE anews.checklist ADD CONSTRAINT fk_checklist_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE SET NULL;


