ALTER TABLE anews.settings DROP COLUMN cleaning_news;

UPDATE anews.permission SET permission_id = '0117' WHERE id = '01170305';
UPDATE anews.permission SET permission_id = '0117' WHERE id = '01170306';
UPDATE anews.permission SET permission_id = '0117' WHERE id = '01170307';

ALTER TABLE anews.program ALTER COLUMN name TYPE character varying(40);

ALTER TABLE anews.story ADD COLUMN vt_manually boolean NOT NULL DEFAULT true;