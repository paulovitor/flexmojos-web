--
--  Apaga tabelas antigas, se ainda existir
--

DROP TABLE IF EXISTS anews.credit_field;
DROP TABLE IF EXISTS anews.credit;
DROP TABLE IF EXISTS anews.story_section_old;
DROP TABLE IF EXISTS anews.story_old;

--
--  Nova tabela
--

CREATE TABLE anews.document (
   id serial NOT NULL,
   version integer NOT NULL DEFAULT 0,
   date date NOT NULL,
   change_date timestamp without time zone NOT NULL DEFAULT now(),
   slug character varying(100),
   drawer boolean NOT NULL DEFAULT false,
   excluded boolean NOT NULL DEFAULT false,
   author_id integer NOT NULL,
   program_id integer,
   CONSTRAINT pk_document PRIMARY KEY (id),
   CONSTRAINT fk_document_user FOREIGN KEY (author_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_document_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.document OWNER TO anews;

CREATE TABLE anews.document_has_program (
  document_id integer NOT NULL,
  program_id integer NOT NULL,
  CONSTRAINT pk_document_has_program PRIMARY KEY (document_id, program_id),
  CONSTRAINT fk_document_has_program_document FOREIGN KEY (document_id) REFERENCES anews.document (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_document_has_program_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.document_has_program OWNER TO anews;

ALTER TABLE anews.guideline ADD COLUMN document_id integer;
ALTER TABLE anews.reportage ADD COLUMN document_id integer;
ALTER TABLE anews.story ADD COLUMN document_id integer;

-- Coluna provisória para migração dos dados
ALTER TABLE anews.document ADD COLUMN child_id integer;

--
--  MIGRA OS DADOS DA PAUTA
--

-- Copia os dados de "guideline" para "document"
INSERT INTO anews.document (
  version, date, change_date, slug, drawer, excluded, author_id, program_id, child_id
) SELECT
  version, date, modified, slug, true, excluded, author_id, program_id, id
FROM anews.guideline;

-- Copia o "document.id" para "guideline.document_id"
UPDATE anews.guideline AS _this SET
  document_id = _sub.id
FROM ( SELECT id, child_id FROM anews.document ) AS _sub
WHERE _this.id = _sub.child_id;

-- Limpa para fazer a próxima migração
UPDATE anews.document SET child_id = null;

--
--  TROCA AS COLUNAS DAS PRIMARY KEYS & FOREIGN KEY
--

--
-- Apaga as FKs que apontam para "guideline"
--
ALTER TABLE anews.checklist DROP CONSTRAINT fk_checklist_guideline;
ALTER TABLE anews.guide DROP CONSTRAINT fk_guideline;
ALTER TABLE anews.guideline_copy_log DROP CONSTRAINT fk_guideline_copy_log_guideline;
ALTER TABLE anews.guideline_has_producer DROP CONSTRAINT fk_guideline_has_producer_guideline;
ALTER TABLE anews.guideline_has_reporter DROP CONSTRAINT fk_guideline_has_reporter_guideline;
ALTER TABLE anews.guideline_has_vehicle DROP CONSTRAINT fk_guideline_has_vehicle_guideline;
ALTER TABLE anews.guideline_report DROP CONSTRAINT fk_guideline_report_guideline;
ALTER TABLE anews.guideline_revision DROP CONSTRAINT entity_fkey;
ALTER TABLE anews.reportage DROP CONSTRAINT guideline_fkey;
ALTER TABLE anews.story DROP CONSTRAINT fk_story_guideline;

--
-- Apaga as PKs ligadas a "guideline"
--
ALTER TABLE anews.guideline_has_producer DROP CONSTRAINT pk_guideline_has_producer;
ALTER TABLE anews.guideline_has_reporter DROP CONSTRAINT pk_guideline_has_reporter;
ALTER TABLE anews.guideline_has_vehicle DROP CONSTRAINT pk_guideline_has_vehicle;

--
--  Atualiza os valores das colunas para o valor de "guideline.document_id"
--

UPDATE anews.checklist SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guide SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guideline_copy_log SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guideline_has_producer SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guideline_has_reporter SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guideline_has_vehicle SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guideline_report SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.guideline_revision SET
  entity_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE entity_id = _sub.id;

UPDATE anews.reportage SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

UPDATE anews.story SET
  guideline_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.guideline ) AS _sub
WHERE guideline_id = _sub.id;

--
--  Recria a PK de "guideline" e cria a FK para "document"
--
ALTER TABLE anews.guideline DROP COLUMN id;
ALTER TABLE anews.guideline ADD CONSTRAINT pk_guideline PRIMARY KEY (document_id);
ALTER TABLE anews.guideline ADD CONSTRAINT fk_guideline_document FOREIGN KEY (document_id) REFERENCES anews.document (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
--  Recria as FKs apagadas
--
ALTER TABLE anews.checklist ADD CONSTRAINT fk_checklist_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guide ADD CONSTRAINT fk_guide_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guideline_copy_log ADD CONSTRAINT fk_guideline_copy_log_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guideline_has_producer ADD CONSTRAINT fk_guideline_has_producer_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guideline_has_reporter ADD CONSTRAINT fk_guideline_has_reporter_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guideline_has_vehicle ADD CONSTRAINT fk_guideline_has_vehicle_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guideline_report ADD CONSTRAINT fk_guideline_report_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.guideline_revision ADD CONSTRAINT fk_guideline_revision_guideline FOREIGN KEY (entity_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage ADD CONSTRAINT fk_reportage_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (document_id) ON UPDATE NO ACTION ON DELETE SET NULL;

--
--  Recria as PKs apagadas
--
ALTER TABLE anews.guideline_has_producer ADD CONSTRAINT pk_guideline_has_producer PRIMARY KEY (guideline_id, user_id);
ALTER TABLE anews.guideline_has_reporter ADD CONSTRAINT pk_guideline_has_reporter PRIMARY KEY (guideline_id, user_id);
ALTER TABLE anews.guideline_has_vehicle ADD CONSTRAINT pk_guideline_has_vehicle PRIMARY KEY (guideline_id, vehicle);

--
--  Define a flag para ocultar na gaveta as pautas que tem reportagem
--

--UPDATE anews.document SET
--  drawer = false
--FROM ( SELECT guideline_id FROM anews.reportage WHERE guideline_id IS NOT NULL ) AS _sub
--WHERE id = _sub.guideline_id;

--
--  MIGRA OS DADOS DA REPORTAGEM
--

UPDATE anews.reportage SET author_id = (
  SELECT id FROM anews.user WHERE email = 'suporte@snews.tv'
) WHERE author_id IS NULL;

-- Copia os dados de "reportage" para "document" (exclusion_date is not null)
INSERT INTO anews.document (
  version, date, change_date, slug, drawer, excluded, author_id, program_id, child_id
) SELECT
    version, date, exclusion_date, slug, true, excluded, author_id, program_id, id
  FROM anews.reportage WHERE exclusion_date IS NOT NULL;

-- Copia os dados de "reportage" para "document" (exclusion_date is null)
INSERT INTO anews.document (
  version, date, change_date, slug, drawer, excluded, author_id, program_id, child_id
) SELECT
    version, date, now(), slug, true, excluded, author_id, program_id, id
  FROM anews.reportage WHERE exclusion_date IS NULL;

-- Copia o "document.id" para "reportage.document_id"
UPDATE anews.reportage AS _this SET
  document_id = _sub.id
FROM ( SELECT id, child_id FROM anews.document ) AS _sub
WHERE _this.id = _sub.child_id;

-- Limpa para fazer a próxima migração
UPDATE anews.document SET child_id = null;

--
--  TROCA AS COLUNAS DAS PRIMARY KEYS & FOREIGN KEY
--

--
-- Apaga as FKs que apontam para "reportage"
--
ALTER TABLE anews.reportage_cg DROP CONSTRAINT fk_reportage;
ALTER TABLE anews.reportage_copy_log DROP CONSTRAINT fk_reportage_copy_log_reportage;
ALTER TABLE anews.reportage_has_vehicle DROP CONSTRAINT fk_reportage_has_vehicle_reportage;
ALTER TABLE anews.reportage_report DROP CONSTRAINT fk_reportage_report_reportage;
ALTER TABLE anews.reportage_revision DROP CONSTRAINT fk_reportage_revision_reportage;
ALTER TABLE anews.reportage_section DROP CONSTRAINT fk_reportage_section_reportage;

--
-- Apaga as PKs ligadas a "reportage"
--
ALTER TABLE anews.reportage_has_vehicle DROP CONSTRAINT pk_reportage_has_vehicle;

--
--  Atualiza os valores das colunas para o valor de "reportage.document_id"
--

UPDATE anews.reportage_cg SET
  reportage_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE reportage_id = _sub.id;

UPDATE anews.reportage_copy_log SET
  reportage_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE reportage_id = _sub.id;

UPDATE anews.reportage_has_vehicle SET
  reportage_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE reportage_id = _sub.id;

UPDATE anews.reportage_report SET
  reportage_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE reportage_id = _sub.id;

UPDATE anews.reportage_revision SET
  entity_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE entity_id = _sub.id;

UPDATE anews.reportage_section SET
  reportage_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE reportage_id = _sub.id;

UPDATE anews.story SET
  reportage_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.reportage ) AS _sub
WHERE reportage_id = _sub.id;

--
--  Recria a PK de "reportage" e cria a FK para "document"
--
ALTER TABLE anews.reportage DROP COLUMN id;
ALTER TABLE anews.reportage ADD CONSTRAINT pk_reportage PRIMARY KEY (document_id);
ALTER TABLE anews.reportage ADD CONSTRAINT fk_reportage_document FOREIGN KEY (document_id) REFERENCES anews.document (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
--  Recria as FKs apagadas
--
ALTER TABLE anews.reportage_cg ADD CONSTRAINT fk_reportage_cg_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_copy_log ADD CONSTRAINT fk_reportage_copy_log_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_has_vehicle ADD CONSTRAINT fk_reportage_has_vehicle_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_report ADD CONSTRAINT fk_reportage_report_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_revision ADD CONSTRAINT fk_reportage_revision FOREIGN KEY (entity_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.reportage_section ADD CONSTRAINT fk_reportage_section_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (document_id) ON UPDATE NO ACTION ON DELETE SET NULL;


--
--  Recria as PKs apagadas
--

ALTER TABLE anews.reportage_has_vehicle ADD CONSTRAINT pk_reportage_has_vehicle PRIMARY KEY (reportage_id, vehicle);

--
--  MIGRA OS DADOS DA LAUDA
--

UPDATE anews.story SET author_id = (
  SELECT id FROM anews.user WHERE email = 'suporte@snews.tv'
) WHERE author_id IS NULL;

ALTER TABLE anews.story ALTER COLUMN author_id SET NOT NULL;

-- Copia os dados de "story" para "document"
INSERT INTO anews.document (
  version, date, change_date, slug, drawer, excluded, author_id, program_id, child_id
) SELECT
    version, _rundown.date, _story.date, slug, drawer, excluded, author_id, program_id, _story.id
  FROM anews.story AS _story
    JOIN anews.block AS _block ON block_id = _block.id
    JOIN anews.rundown AS _rundown ON rundown_id = _rundown.id;

-- Copia os dados de "story" para "document"
INSERT INTO anews.document (
  version, date, change_date, slug, drawer, excluded, author_id, program_id, child_id
) SELECT
    version, date, date, slug, drawer, excluded, author_id, null, id
  FROM anews.story WHERE block_id IS NULL;

-- Copia o "document.id" para "story.document_id"
UPDATE anews.story AS _this SET
  document_id = _sub.id
FROM ( SELECT id, child_id FROM anews.document ) AS _sub
WHERE _this.id = _sub.child_id;

-- Copia o programa do espelho para a coluna "document.program"

UPDATE anews.document AS _this SET
  program_id = _sub.program_id
FROM (
  SELECT document_id, program_id FROM anews.story AS _story
    JOIN anews.block AS _block ON _story.block_id = _block.id
    JOIN anews.rundown AS _rundown ON _block.rundown_id = _rundown.id ) AS _sub
WHERE _this.id = _sub.document_id;

-- Limpa para fazer a próxima migração
UPDATE anews.document SET child_id = null;

--
--  TROCA AS COLUNAS DAS PRIMARY KEYS & FOREIGN KEY
--

--
-- Apaga as FKs que apontam para "story"
--
ALTER TABLE anews.story DROP CONSTRAINT fk_story_source_story;
ALTER TABLE anews.story_copy_log DROP CONSTRAINT fk_story_copy_log_story;
ALTER TABLE anews.story_log DROP CONSTRAINT fk_storylog_story;
ALTER TABLE anews.story_report DROP CONSTRAINT fk_story_report_story;
ALTER TABLE anews.story_revision DROP CONSTRAINT entity_fkey;

UPDATE anews.story SET
  source_story_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.story ) AS _sub
WHERE source_story_id = _sub.id;

UPDATE anews.story_copy_log SET
  story_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.story ) AS _sub
WHERE story_id = _sub.id;

UPDATE anews.story_log SET
  story_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.story ) AS _sub
WHERE story_id = _sub.id;

UPDATE anews.story_report SET
  story_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.story ) AS _sub
WHERE story_id = _sub.id;

UPDATE anews.story_revision SET
  entity_id = _sub.document_id
FROM ( SELECT document_id, id FROM anews.story ) AS _sub
WHERE entity_id = _sub.id;

--
--  Recria a PK de "story" e cria a FK para "document"
--

ALTER TABLE anews.story DROP COLUMN id;
ALTER TABLE anews.story ADD CONSTRAINT pk_story PRIMARY KEY (document_id);
ALTER TABLE anews.story ADD CONSTRAINT fk_story_document FOREIGN KEY (document_id) REFERENCES anews.document (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

--
--  Recria as FKs apagadas
--
ALTER TABLE anews.story ADD CONSTRAINT fk_story_story FOREIGN KEY (source_story_id) REFERENCES anews.story (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_copy_log ADD CONSTRAINT fk_story_copy_log_story FOREIGN KEY (story_id) REFERENCES anews.story (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_log ADD CONSTRAINT fk_story_log_story FOREIGN KEY (story_id) REFERENCES anews.story (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_report ADD CONSTRAINT fk_story_report_story FOREIGN KEY (story_id) REFERENCES anews.story (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_revision ADD CONSTRAINT fk_story_revision_story FOREIGN KEY (entity_id) REFERENCES anews.story (document_id) ON UPDATE NO ACTION ON DELETE NO ACTION;

UPDATE anews.story SET block_id = null WHERE drawer = true OR excluded = true;

ALTER TABLE anews.guideline DROP COLUMN author_id;
ALTER TABLE anews.guideline DROP COLUMN date;
ALTER TABLE anews.guideline DROP COLUMN excluded;
ALTER TABLE anews.guideline DROP COLUMN modified;
ALTER TABLE anews.guideline DROP COLUMN program_id;
ALTER TABLE anews.guideline DROP COLUMN slug;
ALTER TABLE anews.guideline DROP COLUMN version;

ALTER TABLE anews.reportage DROP COLUMN author_id;
ALTER TABLE anews.reportage DROP COLUMN date;
ALTER TABLE anews.reportage DROP COLUMN excluded;
ALTER TABLE anews.reportage DROP COLUMN exclusion_date;
ALTER TABLE anews.reportage DROP COLUMN program_id;
ALTER TABLE anews.reportage DROP COLUMN slug;
ALTER TABLE anews.reportage DROP COLUMN version;

ALTER TABLE anews.story DROP COLUMN author_id;
ALTER TABLE anews.story DROP COLUMN date;
ALTER TABLE anews.story DROP COLUMN drawer;
ALTER TABLE anews.story DROP COLUMN excluded;
ALTER TABLE anews.story DROP COLUMN slug;
ALTER TABLE anews.story DROP COLUMN version;

ALTER TABLE anews.document DROP COLUMN child_id;

ALTER TABLE anews.checklist ADD CONSTRAINT guideline_one_checklist UNIQUE (id, guideline_id);

CREATE TABLE anews.checklist_has_editor (
  checklist_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT pk_checklist_has_editor PRIMARY KEY (checklist_id, user_id),
  CONSTRAINT fk_checklist FOREIGN KEY (checklist_id)
      REFERENCES anews.checklist (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT pk_editor_checklist FOREIGN KEY (user_id)
      REFERENCES anews."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS=FALSE);

ALTER TABLE anews.checklist_has_editor OWNER TO anews;

ALTER TABLE anews.settings ADD COLUMN guideline_grid_sort character varying(20) NOT NULL DEFAULT 'program'::character varying;

ALTER TABLE anews."group" ADD COLUMN show_as_editors boolean NOT NULL DEFAULT false;

CREATE TABLE anews.guideline_has_editor (
  guideline_id integer NOT NULL,
  user_id integer NOT NULL,
  CONSTRAINT pk_guideline_has_editor PRIMARY KEY (guideline_id, user_id),
  CONSTRAINT fk_guideline_editor FOREIGN KEY (guideline_id)
      REFERENCES anews.guideline (document_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT pk_editor_guideline FOREIGN KEY (user_id)
      REFERENCES anews."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS=FALSE);

ALTER TABLE anews.guideline_has_editor OWNER TO anews;

ALTER TABLE anews.guideline_revision ADD COLUMN editor text NOT NULL DEFAULT '';

ALTER TABLE anews.checklist_revision ADD COLUMN editors text NOT NULL DEFAULT '';

UPDATE anews.checklist
  SET info = info || '  EQUIPE: ' || team
  WHERE team != '';

UPDATE anews.checklist_revision as cr
  SET info = info || '    EQUIPE: ' || team
  WHERE team != '';
  
ALTER TABLE anews.checklist_revision DROP COLUMN team;
ALTER TABLE anews.checklist DROP COLUMN team;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('010607', 'Enviar para gaveta', '0106');

