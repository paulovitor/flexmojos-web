INSERT INTO anews.permission(id, name, permission_id) VALUES ('010504', 'Classificação de Pauta', '0105');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01050401', 'Visualizar', '010504');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01050403', 'Editar', '010504');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01050404', 'Deletar', '010504');

CREATE TABLE anews.guideline_classification (
  id serial NOT NULL,
  name character varying(30) NOT NULL,
  CONSTRAINT pk_guideline_classification PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.guideline_classification
  OWNER TO anews;

INSERT INTO anews.guideline_classification(name) VALUES ('Em produção');
INSERT INTO anews.guideline_classification(name) VALUES ('Pronto para pauta');
INSERT INTO anews.guideline_classification(name) VALUES ('Matérias fechadas');
INSERT INTO anews.guideline_classification(name) VALUES ('Factual');

ALTER TABLE anews.guideline ADD COLUMN guideline_classification_id integer;
ALTER TABLE anews.guideline ADD CONSTRAINT guideline_classification_fkey FOREIGN KEY (guideline_classification_id) REFERENCES anews.guideline_classification (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.guideline_revision ADD COLUMN classification character varying(150);

ALTER TABLE anews.guideline ALTER COLUMN program_id DROP NOT NULL;

ALTER TABLE anews.checklist_task ADD COLUMN date date;
ALTER TABLE anews.checklist_task ADD COLUMN "time" time without time zone;

CREATE TABLE anews.reportage_cg
(
  id serial NOT NULL,
  code integer NOT NULL,
  "order" integer NOT NULL,
  cg_template_id integer NOT NULL,
  reportage_id integer NOT NULL,
  CONSTRAINT pk_reportage_cg PRIMARY KEY (id),
  CONSTRAINT fk_reportage_cg_template FOREIGN KEY (cg_template_id)
      REFERENCES anews.cg_template (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_reportage FOREIGN KEY (reportage_id)
      REFERENCES anews.reportage (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.reportage_cg
  OWNER TO anews;


CREATE TABLE anews.reportage_cg_field
(
  id serial NOT NULL,
  "number" integer NOT NULL,
  value text,
  reportage_cg_id integer NOT NULL,
  CONSTRAINT pk_reportage_cg_field PRIMARY KEY (id),
  CONSTRAINT fk_reportage_cg FOREIGN KEY (reportage_cg_id)
      REFERENCES anews.reportage_cg (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE anews.reportage_cg_field
  OWNER TO anews;

INSERT INTO anews.permission(
            id, name, permission_id)
    VALUES ('010714','Alterar data', '0107');
    
ALTER TABLE anews.settings ADD COLUMN report_font_size integer DEFAULT 12;
ALTER TABLE anews.settings ADD COLUMN report_font_family character varying(20) DEFAULT 'Arial';

UPDATE anews.settings SET report_font_size = 12, report_font_family = 'Arial';

ALTER TABLE anews.story ADD COLUMN tapes character varying(30);
ALTER TABLE anews.story_template ADD COLUMN tapes character varying(30);
