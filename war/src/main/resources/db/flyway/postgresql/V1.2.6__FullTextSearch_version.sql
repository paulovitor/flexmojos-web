CREATE TABLE anews.value (
   id serial NOT NULL, 
   value bytea NOT NULL, 
   CONSTRAINT value_pkey PRIMARY KEY (id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.value OWNER TO anews;

-- Valor inicial para o contador
INSERT INTO anews.value (value) values ('\012u\330\217\212D\206\003/\262)\012OX|\002\306\240Buw[K\337\017\363\246t\323\031\0366\000\000\000\020');

CREATE TABLE anews.story_kind (
   id serial NOT NULL, 
   name character varying(20) NOT NULL DEFAULT '',
   acronym character varying(2) NOT NULL DEFAULT '', 
   CONSTRAINT pk_kind PRIMARY KEY (id)
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE anews.story_kind
  OWNER TO anews;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('0212', 'Tipo da Lauda', '02');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('021201', 'Visualizar', '0212');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('021202', 'Adicionar', '0212');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('021203', 'Editar', '0212');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('021204', 'Deletar', '0212');

ALTER TABLE anews.story ADD COLUMN story_kind_id integer;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_kind_id FOREIGN KEY (story_kind_id) REFERENCES anews.story_kind (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.contact_group DROP COLUMN excluded;
ALTER TABLE anews.agenda_contact ALTER COLUMN contact_group_id SET NOT NULL;
ALTER TABLE anews.contact_group ADD CONSTRAINT contact_group_name_uniq UNIQUE (name);