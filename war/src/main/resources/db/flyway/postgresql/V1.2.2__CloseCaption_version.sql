ALTER TABLE settings ADD COLUMN color_edition integer NOT NULL DEFAULT 0;
UPDATE settings SET color_edition=12346434;

CREATE TABLE anews.entitymessage(
   message_id integer NOT NULL,
   id_entity integer NOT NULL, 
   description character varying(140) NOT NULL,
   type integer NOT NULL, 
   CONSTRAINT pk_entity_message_id PRIMARY KEY (message_id), 
   CONSTRAINT fk_message_id FOREIGN KEY (message_id) REFERENCES message (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);

ALTER TABLE anews.entitymessage OWNER TO anews;

ALTER TABLE anews.settings ADD COLUMN ccstudio_enabled boolean NOT NULL DEFAULT false;
ALTER TABLE anews.settings ADD COLUMN ccstudio_host character varying(255);
ALTER TABLE anews.settings ADD COLUMN ccstudio_port integer DEFAULT 1024;

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0210', 'Closed Caption', '02');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021001', 'Visualizar', '0210');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021002', 'Editar', '0210');

ALTER TABLE anews.reportage ADD COLUMN cameraman character varying(60);

-- seta null para todas as referencias de noticias que nao existem mais
update anews.guideline set news_id = null where id in (SELECT a.id FROM anews.guideline a left join anews.news b ON a.news_id = b.id where a.news_id is not null and b.id is null);

-- adiciona fk para noticia
ALTER TABLE anews.guideline ADD CONSTRAINT fk_news_guideline FOREIGN KEY (news_id) REFERENCES anews.news (id) ON UPDATE NO ACTION ON DELETE NO ACTION;