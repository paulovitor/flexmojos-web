-- #AN-896
ALTER TABLE anews.guide ALTER COLUMN observation TYPE text;

-- #AN-???
DROP TABLE anews.story_mos_credit_field;
DROP TABLE anews.script_mos_credit_field;

-- #AN-904
ALTER TABLE anews.document DROP COLUMN drawer;

-- #AN-904
INSERT INTO anews.document_has_program(document_id, program_id)
  SELECT story.document_id, document.program_id
    FROM anews.story AS story, anews.document AS document
    WHERE story.document_id = document.id
      AND document.program_id IS NOT NULL
      AND (story.document_id, document.program_id) NOT IN (
        SELECT doc_has_program.document_id, doc_has_program.program_id
          FROM anews.document_has_program AS doc_has_program
      );

-- #AN-934
ALTER TABLE anews.story ALTER COLUMN vt_manually SET DEFAULT true;
