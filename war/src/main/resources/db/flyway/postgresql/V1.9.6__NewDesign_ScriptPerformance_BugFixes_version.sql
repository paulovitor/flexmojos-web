-- AN-1000
ALTER TABLE anews.settings ADD COLUMN enable_scriptplan_author BOOLEAN NOT NULL DEFAULT FALSE;

-- AN-1001
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01170910', 'Assumir edição', '011709');

-- AN-998
ALTER TABLE anews.settings ADD COLUMN enable_scriptplan_image_editor BOOLEAN NOT NULL DEFAULT FALSE;

-- AN-1004 e AN-1023
ALTER TABLE anews."user" ADD COLUMN collapsed_menu BOOLEAN NOT NULL DEFAULT TRUE;

UPDATE anews.settings
SET
  color_agency       = 16100149,
  color_agenda       = 1752220,
  color_guideline    = 10181046,
  color_reporter     = 2899536,
  color_rundown      = 8839893,
  color_checklist    = 3447003,
  color_script       = 7109257,
  color_edition      = 13782359,
  color_report       = 3289650,
  color_media_center = 3289650;

-- AN-1040
UPDATE anews.story
SET vt_manually = TRUE
WHERE vt_manually = FALSE;