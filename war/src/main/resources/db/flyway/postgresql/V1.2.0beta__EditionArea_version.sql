UPDATE anews.guideline SET slug = concat(substr(slug, 0, 37), '...') where length(slug) > 40;
ALTER TABLE anews.guideline ALTER COLUMN slug TYPE character varying(40);
UPDATE anews.news SET slug = concat(substr(slug, 0, 37), '...') where length(slug) > 40;
ALTER TABLE anews.news ALTER COLUMN slug TYPE character varying(40);
UPDATE anews.round SET slug = concat(substr(slug, 0, 37), '...') where length(slug) > 40;
ALTER TABLE anews.round ALTER COLUMN slug TYPE character varying(40); 
UPDATE anews.story SET slug = concat(substr(slug, 0, 37), '...') where length(slug) > 40;
ALTER TABLE anews.story ALTER COLUMN slug TYPE character varying(40);   

ALTER TABLE anews.reportage_section ADD COLUMN readtime time without time zone;
UPDATE anews.reportage_section SET readtime = '00:00:00';

ALTER TABLE anews.guideline ADD COLUMN news_id integer;

ALTER TABLE anews.settings ADD COLUMN storage_location_logo character(255) NOT NULL DEFAULT '';

CREATE TABLE anews.agenda_contact (
   contact_id integer NOT NULL, 
   address character varying(150), 
   company character varying(60), 
   email character varying(100), 
   excluded boolean NOT NULL DEFAULT false, 
   city_id integer, 
   contact_group_id integer, 
   profession character varying(50), 
   CONSTRAINT agenda_contact_pkey PRIMARY KEY (contact_id), 
   CONSTRAINT city_fkey FOREIGN KEY (city_id) REFERENCES anews.city (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT contact_group_fkey FOREIGN KEY (contact_group_id) REFERENCES anews.contact_group (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT contact_fkey FOREIGN KEY (contact_id) REFERENCES anews.contact (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.agenda_contact OWNER TO anews;

INSERT INTO anews.agenda_contact SELECT id, address, company, email, excluded, city_id, contact_group_id, profession FROM anews.contact;

ALTER TABLE anews.contact DROP CONSTRAINT fk_city_contact;
ALTER TABLE anews.contact DROP CONSTRAINT fk_contact_group_contact;
ALTER TABLE anews.contact DROP COLUMN address;
ALTER TABLE anews.contact DROP COLUMN company;
ALTER TABLE anews.contact DROP COLUMN email;
ALTER TABLE anews.contact DROP COLUMN excluded;
ALTER TABLE anews.contact DROP COLUMN city_id;
ALTER TABLE anews.contact DROP COLUMN contact_group_id;
ALTER TABLE anews.contact DROP COLUMN profession;

delete from anews.guide_has_contact where guide_id In (select id from anews.guide where guideline_id IN (select id from anews.guideline where parent_id is not null));
delete from anews.guide where guideline_id IN (select id from anews.guideline where parent_id is not null);
delete from anews.guideline where parent_id is not null;

delete from anews.reportage_section where reportage_id in (select id from anews.reportage where parent_id is not null);
delete from anews.reportage where parent_id is not null;

delete from anews.story_section where story_id in (select id from anews.story where parent_id is not null);
delete from anews.story where parent_id is not null;

ALTER TABLE anews.story ADD COLUMN rev integer NOT NULL DEFAULT 1;
ALTER TABLE anews.reportage ADD COLUMN rev integer NOT NULL DEFAULT 1;
ALTER TABLE anews.guideline ADD COLUMN rev integer NOT NULL DEFAULT 1;

ALTER TABLE anews.contact_number ALTER COLUMN number_type_id DROP NOT NULL;
ALTER TABLE anews.reportage ADD COLUMN ok boolean NOT NULL DEFAULT false;

ALTER TABLE anews.story RENAME type_session TO type;
ALTER TABLE anews.reportage_section ADD COLUMN ok boolean NOT NULL DEFAULT false;
ALTER TABLE anews.contact ADD COLUMN profession character varying(50);

UPDATE anews.contact SET profession = anews.agenda_contact.profession FROM anews.agenda_contact WHERE anews.contact.id = anews.agenda_contact.contact_id;

ALTER TABLE anews.agenda_contact DROP COLUMN profession;
UPDATE anews.permission SET name = 'Preview' WHERE anews.permission.id = '03';
INSERT INTO anews.permission (id, name, permission_id) VALUES ('0108', 'Edição', '01');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010801', 'Visualizar', '0108');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010603', 'Editar', '0106');
UPDATE anews.permission SET name = 'Enviar E-mail' WHERE anews.permission.id = '010606';
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010609', 'Versões', '0106');