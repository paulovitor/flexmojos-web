ALTER TABLE anews.rundown ADD COLUMN displayed boolean NOT NULL DEFAULT false;
ALTER TABLE anews.mos_device ADD COLUMN tp_mode boolean NOT NULL DEFAULT false;
ALTER TABLE anews.story_cg ADD COLUMN disabled boolean NOT NULL DEFAULT false;
ALTER TABLE anews.story_mos_credit ADD COLUMN disabled boolean NOT NULL DEFAULT false;
ALTER TABLE anews.script_item ADD COLUMN disabled boolean NOT NULL DEFAULT false;
ALTER TABLE anews.document ADD COLUMN excluded_drawer boolean NOT NULL DEFAULT false;

INSERT INTO anews.permission (id, name, permission_id) VALUES ('010505', 'Equipes', '0105');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01050501', 'Visualizar', '010505');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01050502', 'Adicionar', '010505');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01050503', 'Editar', '010505');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01050504', 'Deletar', '010505');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01050505', 'Adicionar usuário', '010505');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01050506', 'Remover usuário', '010505');

CREATE TABLE anews."team" (
  id serial NOT NULL,
  name character varying(80) NOT NULL,
  CONSTRAINT pk_team PRIMARY KEY (id)
) WITH ( OIDS=FALSE );

ALTER TABLE anews."team" OWNER TO anews;

CREATE TABLE anews.user_has_team (
  user_id integer NOT NULL,
  team_id integer NOT NULL,
  CONSTRAINT pk_team_user PRIMARY KEY (user_id, team_id),
  CONSTRAINT fk_user_has_team_team_id FOREIGN KEY (team_id)
      REFERENCES anews."team" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_has_team_user_id FOREIGN KEY (user_id)
      REFERENCES anews."user" (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH ( OIDS=FALSE );

ALTER TABLE anews.user_has_team OWNER TO anews;

ALTER TABLE anews.guideline
  ADD COLUMN team_id integer,
  ADD CONSTRAINT guideline_team_fkey FOREIGN KEY (team_id)
      REFERENCES anews.team (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.guideline_revision
  ADD COLUMN team character varying(150);

ALTER TABLE anews.settings ADD COLUMN server_streaming boolean NOT NULL DEFAULT false;
ALTER TABLE anews.settings ADD COLUMN other_host_active boolean NOT NULL DEFAULT false;
ALTER TABLE anews.settings ADD COLUMN host character varying(255) NOT NULL DEFAULT 'localhost';
ALTER TABLE anews.settings ADD COLUMN root_directory character varying(255) NOT NULL DEFAULT '/snews/';
ALTER TABLE anews.settings ADD COLUMN protocol character varying(9) NOT NULL DEFAULT 'http://';
ALTER TABLE anews.guide ALTER COLUMN schedule drop not null;
ALTER TABLE anews.story ADD COLUMN image_editor_id INTEGER;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_image_editor FOREIGN KEY (image_editor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- Table: anews.program_has_image_editor

-- DROP TABLE anews.program_has_image_editor;

CREATE TABLE anews.program_has_image_editor
(
  program_id INTEGER NOT NULL,
  user_id    INTEGER NOT NULL,
  CONSTRAINT pk_program_has_image_editor PRIMARY KEY (program_id, user_id),
  CONSTRAINT fk_program_has_image_editor_program FOREIGN KEY (program_id)
  REFERENCES anews.program (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT pk_editor FOREIGN KEY (user_id)
  REFERENCES anews."user" (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
);
ALTER TABLE anews.program_has_image_editor
OWNER TO anews;

CREATE TABLE anews.document_has_program_drawer
(
   document_id integer NOT NULL, 
   program_id integer NOT NULL, 
   CONSTRAINT pk_document_has_program_drawer PRIMARY KEY (document_id, program_id), 
   CONSTRAINT fk_document_has_program_drawer_document FOREIGN KEY (document_id) REFERENCES anews.document (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_document_has_program_drawer_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE anews.document_has_program_drawer
  OWNER TO anews;

ALTER TABLE anews.document
DROP COLUMN excluded_drawer;

insert into anews.permission (id, name, permission_id) values ('01050111', 'Enviar para a Gaveta', '010501');

-- Excluindo e criando a constraint para a tabela SCRIPT_MOS_VIDEO

ALTER TABLE ANEWS.SCRIPT_MOS_VIDEO
DROP CONSTRAINT FK_SCRIPT_MOS_VIDEO_MOS_DEVICE_CHANNEL;

ALTER TABLE ANEWS.SCRIPT_MOS_VIDEO
ADD CONSTRAINT FK_SCRIPT_MOS_VIDEO_MOS_DEVICE_CHANNEL FOREIGN KEY (MOS_DEVICE_CHANNEL_ID)
REFERENCES ANEWS.MOS_DEVICE_CHANNEL (ID) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE SET NULL;

-- Excluindo e criando a constraint para a tabela STORY_MOS_VIDEO

ALTER TABLE ANEWS.STORY_MOS_VIDEO
DROP CONSTRAINT FK_STORY_MOS_OBJ_MOS_DEVICE_CHANNEL;
			
ALTER TABLE ANEWS.STORY_MOS_VIDEO
ADD CONSTRAINT FK_STORY_MOS_OBJ_MOS_DEVICE_CHANNEL FOREIGN KEY (MOS_DEVICE_CHANNEL_ID)
REFERENCES ANEWS.MOS_DEVICE_CHANNEL (ID) MATCH SIMPLE
ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE anews.script ADD COLUMN image_editor_id INTEGER;
ALTER TABLE anews.script ADD CONSTRAINT fk_script_image_editor FOREIGN KEY (image_editor_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE anews.institution_has_contact (
  institution_id integer NOT NULL,
  contact_id integer NOT NULL,
  CONSTRAINT pk_institution_has_contact PRIMARY KEY (institution_id, contact_id),
  CONSTRAINT fk_institution_has_contact_institution FOREIGN KEY (institution_id)
      REFERENCES anews.institution (id) ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_institution_has_contact_contact FOREIGN KEY (contact_id)
      REFERENCES anews.contact (id) ON UPDATE NO ACTION ON DELETE CASCADE
) WITH (OIDS = FALSE);
ALTER TABLE anews.institution_has_contact OWNER TO anews;

ALTER TABLE anews.round ADD COLUMN contact_id INTEGER;
ALTER TABLE anews.round ADD CONSTRAINT fk_round_contact FOREIGN KEY (contact_id) REFERENCES anews.contact (id) ON UPDATE NO ACTION ON DELETE CASCADE;

ALTER TABLE anews.institution ADD COLUMN default_contact_id INTEGER ;

ALTER TABLE anews.institution ADD 
 CONSTRAINT fk_rundown_program FOREIGN KEY (default_contact_id)
      REFERENCES anews.contact (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

-- Cria colunas temporárias para auxiliar a migração
ALTER TABLE anews.contact ADD COLUMN tmp_round_id integer;
ALTER TABLE anews.contact ADD COLUMN tmp_round_number character varying(30);

-- Copia as informações do contato da ronda para a tabela dos contatos
INSERT INTO anews.contact (name, tmp_round_id, tmp_round_number)
  SELECT substr(contact, 0, 60), id, substr(contact_number, 0, 30)
  FROM anews.round
  WHERE contact IS NOT NULL AND contact != '';

-- Insere o número do contato na tabela de números
INSERT INTO anews.contact_number (contact_id, number_type_id, value)
  SELECT id, 3, tmp_round_number
  FROM anews.contact
  WHERE tmp_round_number IS NOT NULL;

-- Cria a ligação entre o registro do contato e a ronda
UPDATE anews.round AS round
  SET contact_id = contact.id
  FROM (
    SELECT id, tmp_round_id
    FROM anews.contact
    WHERE tmp_round_id IS NOT NULL
  ) AS contact
WHERE round.id = contact.tmp_round_id;

-- Cria o vínculo do contato novo com a instituição
INSERT INTO anews.institution_has_contact
  SELECT institution_id, contact_id
  FROM anews.round
  WHERE institution_id IS NOT NULL AND contact_id IS NOT NULL;

-- Apaga as colunas temporárias
ALTER TABLE anews.contact DROP COLUMN tmp_round_id;
ALTER TABLE anews.contact DROP COLUMN tmp_round_number;

-- Apaga as antigas colunas com as informações do contato na ronda
ALTER TABLE anews.round DROP COLUMN contact;
ALTER TABLE anews.round DROP COLUMN contact_number;

ALTER TABLE anews.tweet_info ALTER COLUMN tweet_id TYPE varchar(150) USING tweet_id::varchar(150);

-- Cria as colunas state e status_reason em document
ALTER TABLE anews.document ADD COLUMN state integer NOT NULL DEFAULT 0;
ALTER TABLE anews.document ADD COLUMN status_reason text;

-- Passa os valores de state e status_reason de guideline para document
UPDATE anews.document as d
  SET state = g.state,
  	  status_reason = g.status_reason
  FROM (
    SELECT state, status_reason, document_id 
    FROM anews.guideline
  ) as g
WHERE g.document_id = d.id;

-- Lima as colunas state e status_reason em guideline
ALTER TABLE anews.guideline DROP COLUMN state;
ALTER TABLE anews.guideline DROP COLUMN status_reason;

ALTER TABLE anews.round DROP COLUMN occurrence;
ALTER TABLE anews.round ALTER COLUMN institution_id DROP NOT NULL;

-- Altera tabela story_mos_video
ALTER TABLE anews.story_mos_video ALTER COLUMN mos_media_id DROP NOT NULL;
ALTER TABLE anews.story_mos_video DROP CONSTRAINT fk_mos_media;
ALTER TABLE anews.story_mos_video ADD CONSTRAINT fk_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE anews.story_mos_video ADD COLUMN ready boolean NOT NULL DEFAULT false;
ALTER TABLE anews.story_mos_video ADD COLUMN slug character varying(255);
ALTER TABLE anews.story_mos_video ADD COLUMN duration_time time without time zone;

-- Altera tabela script_mos_video
ALTER TABLE anews.script_mos_video ALTER COLUMN mos_media_id DROP NOT NULL;
ALTER TABLE anews.script_mos_video DROP CONSTRAINT fk_script_mos_video_mos_media;
ALTER TABLE anews.script_mos_video ADD CONSTRAINT fk_script_mos_video_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE anews.script_mos_video ADD COLUMN ready boolean NOT NULL DEFAULT false;
ALTER TABLE anews.script_mos_video ADD COLUMN slug character varying(255);
ALTER TABLE anews.script_mos_video ADD COLUMN duration_time time without time zone;

-- Adiciona colunas displayed e displaying (roteiro)
ALTER TABLE anews.script_plan ADD COLUMN displayed boolean NOT NULL DEFAULT false;
ALTER TABLE anews.script ADD COLUMN displaying boolean NOT NULL DEFAULT false;
ALTER TABLE anews.script ADD COLUMN displayed boolean NOT NULL DEFAULT false;

INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170305', 'Pôr em Exibição', '010711');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170306', 'Reiniciar a exibição', '010711');
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01170307', 'Finalizar a exibição', '010711');

-- CREATE: Table: media

CREATE TABLE anews.media
(
  id            SERIAL                 NOT NULL,
  slug          CHARACTER VARYING(255) NOT NULL,
  changed       TIMESTAMP WITH TIME ZONE,
  duration_time TIME,
  description   CHARACTER VARYING(1024),
  device_id     INTEGER,

  CONSTRAINT pk_media PRIMARY KEY (id),
  CONSTRAINT fk_media_device FOREIGN KEY (device_id)
  REFERENCES anews.device (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
);
ALTER TABLE anews.media
OWNER TO anews;


-- CREATE: Table: ws_media

CREATE TABLE anews.ws_media
(
  media_id       INTEGER                  NOT NULL,
  asset_id       INTEGER                  NOT NULL,
  classification CHARACTER VARYING(32),
  city           CHARACTER VARYING(128),
  event_date     TIMESTAMP WITH TIME ZONE NOT NULL,
  observation    CHARACTER VARYING(1024),
  reporter       CHARACTER VARYING(128),
  cameraman1     CHARACTER VARYING(128),
  cameraman2     CHARACTER VARYING(128),
  cameraman3     CHARACTER VARYING(128),
  assistant      CHARACTER VARYING(128),
  editor         CHARACTER VARYING(128),
  videographer   CHARACTER VARYING(128),
  streaming_url  CHARACTER VARYING(255),
  status   INTEGER,

  CONSTRAINT pk_ws_media PRIMARY KEY (media_id),
  CONSTRAINT fk_ws_media_media FOREIGN KEY (media_id)
  REFERENCES anews.media (id) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
OIDS = FALSE
);
ALTER TABLE anews.ws_media
OWNER TO anews;

--
-- Tables: media and mos_media
--

-- Cria coluna provisória para guardar o id de mos_media
ALTER TABLE anews.media ADD COLUMN child_id INTEGER;

-- Copia dados de mos_media para media
INSERT INTO anews.media (slug, changed, duration_time, description, device_id, child_id) SELECT slug, changed, duration_time, description, mos_device_id, id FROM anews.mos_media;

-- Cria coluna media_id em mos_media
ALTER TABLE anews.mos_media ADD COLUMN media_id INTEGER;

-- Atualiza os valores da coluna media_id de mos_media apartir do id de media
UPDATE anews.mos_media SET media_id = m.id FROM (SELECT id, child_id FROM anews.media) AS m WHERE mos_media.id = m.child_id;

--
-- Tables: mos_path, mos_proxy_path, story_mos_video, story_mos_credit, script_mos_credit, script_mos_video
--

-- Deleta fk das tabelas que dependem de mos_media
ALTER TABLE anews.mos_path DROP CONSTRAINT mos_obj_fk;
ALTER TABLE anews.mos_proxy_path DROP CONSTRAINT mos_obj_fk;
ALTER TABLE anews.story_mos_video DROP CONSTRAINT fk_mos_media;
ALTER TABLE anews.story_mos_credit DROP CONSTRAINT fk_story_mos_credit_mos_media;
ALTER TABLE anews.script_mos_credit DROP CONSTRAINT fk_script_mos_credit_mos_media;
ALTER TABLE anews.script_mos_video DROP CONSTRAINT fk_script_mos_video_mos_media;

-- Atualiza valores das tabelas que dependem de mos_media
UPDATE anews.mos_path SET mos_media_id = m.media_id FROM (SELECT id, media_id FROM anews.mos_media) AS m WHERE mos_media_id = m.id;
UPDATE anews.mos_proxy_path SET mos_media_id = m.media_id FROM (SELECT id, media_id FROM anews.mos_media) AS m WHERE mos_media_id = m.id;
UPDATE anews.story_mos_video SET mos_media_id = m.media_id FROM (SELECT id, media_id FROM anews.mos_media) AS m WHERE mos_media_id = m.id;
UPDATE anews.story_mos_credit SET mos_media_id = m.media_id FROM (SELECT id, media_id FROM anews.mos_media) AS m WHERE mos_media_id = m.id;
UPDATE anews.script_mos_credit SET mos_media_id = m.media_id FROM (SELECT id, media_id FROM anews.mos_media) AS m WHERE mos_media_id = m.id;
UPDATE anews.script_mos_video SET mos_media_id = m.media_id FROM (SELECT id, media_id FROM anews.mos_media) AS m WHERE mos_media_id = m.id;

--
-- Tables: mos_media
--

-- Deleta pk de mos_media
ALTER TABLE anews.mos_media DROP COLUMN id;

-- Cria pk de mos_media
ALTER TABLE anews.mos_media ADD CONSTRAINT pk_mos_media PRIMARY KEY (media_id);
ALTER TABLE anews.mos_media ADD CONSTRAINT fk_mos_media_media FOREIGN KEY (media_id) REFERENCES anews.media (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

-- Deleta colunas
ALTER TABLE anews.mos_media DROP COLUMN slug;
ALTER TABLE anews.mos_media DROP COLUMN duration_time;
ALTER TABLE anews.mos_media DROP COLUMN changed;
ALTER TABLE anews.mos_media DROP COLUMN description;
ALTER TABLE anews.mos_media DROP COLUMN mos_device_id;

--
-- Tables: mos_path, mos_proxy_path, story_mos_video, story_mos_credit, script_mos_credit, script_mos_video
--

-- Cria fk nova das tabelas que dependem de mos_media
ALTER TABLE anews.mos_path ADD CONSTRAINT fk_mos_path_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (media_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.mos_proxy_path ADD CONSTRAINT fk_mos_proxy_path_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (media_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.story_mos_video ADD CONSTRAINT fk_story_mos_video_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (media_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;
ALTER TABLE anews.story_mos_credit ADD CONSTRAINT fk_story_mos_credit_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (media_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.script_mos_credit ADD CONSTRAINT fk_script_mos_credit_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (media_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;
ALTER TABLE anews.script_mos_video ADD CONSTRAINT fk_script_mos_video_mos_media FOREIGN KEY (mos_media_id) REFERENCES anews.mos_media (media_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

-- Altera nome da tabela script_mos_video para script_video
ALTER TABLE anews.script_mos_video RENAME TO script_video;

-- Cria coluna media_id
ALTER TABLE anews.script_video ADD COLUMN media_id INTEGER;

-- Copia valores do mos_media_id para o campo media_id
UPDATE anews.script_video SET media_id = mos_media_id;

-- Deleta constraint pk
ALTER TABLE anews.script_video DROP CONSTRAINT pk_script_mos_video;

-- Deleta fks
ALTER TABLE anews.script_video DROP CONSTRAINT fk_script_mos_video_mos_device_channel;
ALTER TABLE anews.script_video DROP CONSTRAINT fk_script_mos_video_mos_media;
ALTER TABLE anews.script_video DROP CONSTRAINT fk_script_mos_video_script;

-- Cria nova pk
ALTER TABLE anews.script_video ADD CONSTRAINT pk_script_video PRIMARY KEY (id);

-- Cria novas fks
ALTER TABLE anews.script_video ADD CONSTRAINT fk_script_video_mos_device_channel FOREIGN KEY (mos_device_channel_id) REFERENCES anews.mos_device_channel (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE SET NULL;
ALTER TABLE anews.script_video ADD CONSTRAINT fk_script_video_media FOREIGN KEY (media_id) REFERENCES anews.media (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE anews.script_video ADD CONSTRAINT fk_script_video_script FOREIGN KEY (script_id) REFERENCES anews.script (document_id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE;

-- Deleta campo mos_media_id
ALTER TABLE anews.script_video DROP COLUMN mos_media_id;

-- Renomeia sequence
ALTER SEQUENCE anews.script_mos_video_id_seq RENAME TO script_video_id_seq;

-- Deleta campo child_id
ALTER TABLE anews.media DROP COLUMN child_id;

-- Cria tabela story_ws_video
CREATE TABLE anews.story_ws_video (
  story_sub_section_id integer NOT NULL,
  ws_media_id integer NOT NULL,
  CONSTRAINT pk_story_ws_video PRIMARY KEY (story_sub_section_id),
  CONSTRAINT fk_story_ws_video_story_sub_section FOREIGN KEY (story_sub_section_id)
  REFERENCES anews.story_sub_section (id) ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fk_story_ws_video_ws_media FOREIGN KEY (ws_media_id)
  REFERENCES anews.ws_media (media_id) ON UPDATE NO ACTION ON DELETE SET NULL
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_ws_video OWNER TO anews;

-- Adiciona foreign key mos_device_channel (story_ws_video)
ALTER TABLE anews.story_ws_video ADD COLUMN mos_device_channel_id integer;
ALTER TABLE anews.story_ws_video ADD CONSTRAINT fk_story_ws_video_mos_device_channel FOREIGN KEY (mos_device_channel_id) REFERENCES anews.mos_device_channel (id) ON UPDATE NO ACTION ON DELETE SET NULL;

-- Adiciona colunas slug e duration_time (story_ws_video)
ALTER TABLE anews.story_ws_video ADD COLUMN slug character varying(255);
ALTER TABLE anews.story_ws_video ADD COLUMN duration_time time without time zone;

-- Adiciona colunas disabled
ALTER TABLE anews.story_ws_video ADD COLUMN disabled BOOL DEFAULT false NOT NULL;
ALTER TABLE anews.story_mos_video ADD COLUMN disabled BOOL DEFAULT false NOT NULL;
ALTER TABLE anews.script_video ADD COLUMN disabled BOOL DEFAULT false NOT NULL;

-- Retira restrição NOT NULL coluna ws_media_id
ALTER TABLE anews.story_ws_video ALTER COLUMN ws_media_id DROP NOT NULL;

-- Adiciona coluna filename (ws_media)
ALTER TABLE anews.ws_media ADD COLUMN filename character varying(20);

-- Adiciona permissão de sincronizar todos os videos
INSERT INTO anews.permission(id, name, permission_id) VALUES ('011712', 'Finalizar a exibição', '0117');

ALTER TABLE anews.rss_item DROP CONSTRAINT rss_feed_fkey;
ALTER TABLE anews.rss_item ADD CONSTRAINT rss_feed_fkey FOREIGN KEY (rss_feed_id) REFERENCES anews.rss_feed (id) ON UPDATE NO ACTION ON DELETE CASCADE;

