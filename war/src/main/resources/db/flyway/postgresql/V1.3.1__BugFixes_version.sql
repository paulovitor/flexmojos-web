ALTER TABLE anews.reportage ADD COLUMN information text;
ALTER TABLE anews.reportage ADD COLUMN tip_of_head text;

-- Cria a nova coluna e sua foreign key
ALTER TABLE anews.program ADD COLUMN default_presenter_id INTEGER;
ALTER TABLE anews.program ADD CONSTRAINT fk_default_presenter_user FOREIGN KEY (default_presenter_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- Define um apresentador padrão para todos os programas
UPDATE anews.program AS prog SET default_presenter_id = (
	SELECT user_id FROM anews.program_has_presenter WHERE program_id = prog.id LIMIT 1
);

-- Se ainda tiver programa sem apresentador padrão seta qualquer um
UPDATE anews.program SET default_presenter_id = (
	SELECT id FROM anews.user WHERE deleted = FALSE LIMIT 1
) WHERE default_presenter_id IS NULL;

-- Seta a nova coluna como not null
ALTER TABLE anews.program ALTER COLUMN default_presenter_id SET NOT NULL;