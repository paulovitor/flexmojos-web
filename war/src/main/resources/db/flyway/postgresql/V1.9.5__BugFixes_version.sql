-- AN-945
ALTER TABLE anews.settings ADD COLUMN enable_rundown_author boolean NOT NULL DEFAULT false;

-- AN-944
ALTER TABLE anews.story ADD COLUMN news_id integer;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_news_story FOREIGN KEY (news_id) REFERENCES anews.news (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

-- AN-886
ALTER TABLE anews.settings ADD COLUMN enable_script_plan_load_button BOOLEAN NOT NULL DEFAULT FALSE;

-- AN-980
UPDATE anews.reportage_revision SET information = '' WHERE information IS NULL;
UPDATE anews.reportage_revision SET tip_of_head = '' WHERE tip_of_head IS NULL;
ALTER TABLE anews.reportage_revision ALTER COLUMN information SET DEFAULT '';
ALTER TABLE anews.reportage_revision ALTER COLUMN information SET NOT NULL;
ALTER TABLE anews.reportage_revision ALTER COLUMN tip_of_head SET DEFAULT '';
ALTER TABLE anews.reportage_revision ALTER COLUMN tip_of_head SET NOT NULL;
