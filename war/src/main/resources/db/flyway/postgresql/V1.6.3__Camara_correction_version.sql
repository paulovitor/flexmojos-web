ALTER TABLE anews.reportage_revision ADD COLUMN information text;
ALTER TABLE anews.reportage_revision ADD COLUMN tip_of_head text;

ALTER TABLE anews.checklist ALTER COLUMN local TYPE character varying(150);

CREATE INDEX idx_checklist_guideline ON anews.checklist USING btree (guideline_id DESC NULLS LAST);

ALTER TABLE anews.checklist_revision ALTER COLUMN local TYPE character varying(150);
