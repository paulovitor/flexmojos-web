ALTER TABLE anews.guide ALTER COLUMN observation TYPE text;

DROP TABLE IF EXISTS anews.story_mos_credit_field;
DROP TABLE IF EXISTS anews.script_mos_credit_field;

-- #AN-900
ALTER TABLE anews.settings ADD COLUMN enable_guideline_team boolean NOT NULL DEFAULT false;

-- #AN-899
ALTER TABLE anews.settings ADD COLUMN enable_rundown_image_editor boolean NOT NULL DEFAULT false;

-- #AN-919
ALTER TABLE anews.settings ADD COLUMN include_stand_by boolean NOT NULL DEFAULT true;

-- #AN-901
INSERT INTO anews.permission (id, name, permission_id) VALUES ('01071116', 'Assumir edição', '010711');
