CREATE TABLE anews.company(
   id serial, 
   name character varying(30) NOT NULL, 
   host character varying(255) NOT NULL, 
   port integer NOT NULL DEFAULT 8080, 
   email character varying(200) NOT NULL, 
   password character varying(200) NOT NULL, 
   CONSTRAINT pk_company PRIMARY KEY (id)
) WITH (
  OIDS = FALSE
)
;
ALTER TABLE anews.company OWNER TO anews;
  
  	-- administrativo
INSERT INTO anews.permission (id, name, permission_id) VALUES ('0213', 'Praça', '02');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021301', 'Visualizar', '0213');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021302', 'Adicionar', '0213');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021303', 'Editar', '0213');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021304', 'Deletar', '0213');

	-- sistema
INSERT INTO anews.permission (id, name, permission_id) VALUES ('0113', 'Praça', '01');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011301', 'Visualizar', '0113');

ALTER TABLE anews.rundown ADD COLUMN production time without time zone NOT NULL DEFAULT '00:00:00';

ALTER TABLE anews.settings ADD COLUMN default_color integer NOT NULL DEFAULT 13312042;

ALTER TABLE anews.settings RENAME storage_location_logo  TO storage_location_report_logo;
ALTER TABLE anews.settings ADD COLUMN storage_location_system_logo character(255) NOT NULL DEFAULT ''::bpchar;

ALTER TABLE anews.settings ADD COLUMN storage_location_banner_login character(255) NOT NULL DEFAULT ''::bpchar;

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0214', 'Visual', '02');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021401', 'Visualizar', '0214');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021403', 'Editar', '0214');

ALTER TABLE anews.story ADD COLUMN source_story_id integer;
ALTER TABLE anews.story ADD CONSTRAINT fk_story_source_story FOREIGN KEY (source_story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.story_mos_obj ADD COLUMN mos_device_playout_id integer;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_mos_device_story_mos_obj FOREIGN KEY (mos_device_playout_id) REFERENCES anews.mos_device (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE anews.mos_channel (
   id serial,
   name character varying(128) NOT NULL,
   mos_device_id integer NOT NULL,
   CONSTRAINT pk_mos_channel PRIMARY KEY (id),
   CONSTRAINT fk_mos_channel_mos_device FOREIGN KEY (mos_device_id) REFERENCES anews.mos_device (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.mos_channel OWNER TO anews;

ALTER TABLE anews.mos_device DROP COLUMN channel;

ALTER TABLE anews.mos_device ADD COLUMN default_channel_id integer;
ALTER TABLE anews.mos_device ADD CONSTRAINT fk_mos_device_mos_channel FOREIGN KEY (default_channel_id) REFERENCES anews.mos_channel (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE anews.story_mos_obj ADD COLUMN mos_status character varying(10);


CREATE TABLE anews.mos_device_type
(
   id serial NOT NULL, 
   description character varying(30) NOT NULL, 
   mos_device_id integer NOT NULL, 
   CONSTRAINT pk_mos_device_type PRIMARY KEY (id), 
   CONSTRAINT fk_mos_device_id_type FOREIGN KEY (mos_device_id) REFERENCES anews.mos_device (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
)
;
ALTER TABLE anews.mos_device_type OWNER TO anews;

INSERT INTO anews.mos_device_type(description, mos_device_id) SELECT type, id FROM anews.mos_device;

ALTER TABLE anews.mos_device DROP COLUMN type;

ALTER TABLE anews.story_mos_obj ADD COLUMN mos_channel_id integer;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_mos_channel_id FOREIGN KEY (mos_channel_id) REFERENCES anews.mos_channel (id) ON UPDATE NO ACTION ON DELETE NO ACTION;

update anews.mos_device set mos_revision = 'V_' || replace(mos_revision, '.', '');

CREATE TABLE anews.reportage_copy_log (
   id serial NOT NULL,
   reportage_id integer NOT NULL,
   nickname character varying(20) NOT NULL,
   company character varying(80) NOT NULL,
   date timestamp with time zone NOT NULL,
   CONSTRAINT pk_reportage_copy_log PRIMARY KEY (id),
   CONSTRAINT fk_reportage_copy_log_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.reportage_copy_log OWNER TO anews;

CREATE TABLE anews.guideline_copy_log (
   id serial NOT NULL,
   guideline_id integer NOT NULL,
   nickname character varying(20) NOT NULL,
   company character varying(80) NOT NULL,
   date timestamp with time zone NOT NULL,
   CONSTRAINT pk_guideline_copy_log PRIMARY KEY (id),
   CONSTRAINT fk_guideline_copy_log_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.guideline_copy_log OWNER TO anews;

CREATE TABLE anews.story_copy_log (
   id serial NOT NULL,
   story_id integer NOT NULL,
   nickname character varying(20) NOT NULL,
   company character varying(80) NOT NULL,
   date timestamp with time zone NOT NULL,
   CONSTRAINT pk_story_copy_log PRIMARY KEY (id),
   CONSTRAINT fk_story_copy_log_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_copy_log OWNER TO anews;

-- ON DELETE -> SET NULL
ALTER TABLE anews.mos_device DROP CONSTRAINT fk_mos_device_mos_channel;
ALTER TABLE anews.mos_device ADD CONSTRAINT fk_mos_device_mos_channel FOREIGN KEY (default_channel_id) REFERENCES anews.mos_channel (id) ON UPDATE NO ACTION ON DELETE SET NULL;

-- ON DELETE -> SET NULL
ALTER TABLE anews.story_mos_obj DROP CONSTRAINT fk_mos_channel_id;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_story_mos_obj_mos_channel FOREIGN KEY (mos_channel_id) REFERENCES anews.mos_channel (id) ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE anews.story_mos_obj DROP CONSTRAINT fk_mos_device_story_mos_obj;
ALTER TABLE anews.story_mos_obj ADD CONSTRAINT fk_story_mos_obj_mos_device FOREIGN KEY (mos_device_playout_id) REFERENCES anews.mos_device (id) ON UPDATE NO ACTION ON DELETE SET NULL;

ALTER TABLE anews.mos_media ALTER COLUMN mos_device_id TYPE integer;
ALTER TABLE anews.mos_media ALTER COLUMN mos_device_id DROP NOT NULL;

ALTER TABLE anews.mos_media DROP CONSTRAINT mos_device_fk;
ALTER TABLE anews.mos_media ADD CONSTRAINT mos_device_fk FOREIGN KEY (mos_device_id) REFERENCES anews.mos_device (id) ON UPDATE NO ACTION ON DELETE SET NULL;

INSERT INTO anews.permission (id, name, permission_id) VALUES ('010611', 'Copiar', '0106');

ALTER TABLE anews.story_cg_field
   ALTER COLUMN value DROP NOT NULL;
