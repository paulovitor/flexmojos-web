INSERT INTO anews."user"(name, nickname, password, email, deleted, phone_number, read_time) VALUES ('Suporte SNEWS', 'suporte', 'd826125348d24c4739b118bb0e17bba5', 'suporte@snews.tv', false, '+55 (61) 3321-8321', 0.07);

CREATE TABLE anews.report_event (
   id serial NOT NULL, 
   name character varying(30) NOT NULL, 
   CONSTRAINT pk_report_event PRIMARY KEY (id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.report_event OWNER TO anews;

CREATE TABLE anews.report_nature (
   id serial NOT NULL, 
   name character varying(30) NOT NULL, 
   CONSTRAINT pk_report_nature PRIMARY KEY (id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.report_nature OWNER TO anews;

CREATE TABLE anews.report (
   id serial NOT NULL, 
   slug character varying(40), 
   content text, 
   date date NOT NULL DEFAULT now(),
   last_change timestamp with time zone NOT NULL DEFAULT now(), 
   author_id integer NOT NULL, 
   event_id integer NOT NULL, 
   nature_id integer NOT NULL, 
   CONSTRAINT pk_report PRIMARY KEY (id), 
   CONSTRAINT fk_report_user FOREIGN KEY (author_id) REFERENCES anews."user" (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_report_report_event FOREIGN KEY (event_id) REFERENCES anews.report_event (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_report_report_nature FOREIGN KEY (nature_id) REFERENCES anews.report_nature (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.report OWNER TO anews;

CREATE TABLE anews.guideline_report (
   report_id integer NOT NULL, 
   guideline_id integer NOT NULL, 
   CONSTRAINT pk_guideline_report PRIMARY KEY (report_id),
   CONSTRAINT fk_guideline_report_report FOREIGN KEY (report_id) REFERENCES anews.report (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT fk_guideline_report_guideline FOREIGN KEY (guideline_id) REFERENCES anews.guideline (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.guideline_report OWNER TO anews;

CREATE TABLE anews.reportage_report (
   report_id integer NOT NULL, 
   reportage_id integer NOT NULL, 
   CONSTRAINT pk_reportage_report PRIMARY KEY (report_id), 
   CONSTRAINT fk_reportage_report_report FOREIGN KEY (report_id) REFERENCES anews.report (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_reportage_report_reportage FOREIGN KEY (reportage_id) REFERENCES anews.reportage (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.reportage_report OWNER TO anews;

CREATE TABLE anews.story_report (
   report_id integer NOT NULL, 
   story_id integer NOT NULL, 
   CONSTRAINT pk_story_report PRIMARY KEY (report_id), 
   CONSTRAINT fk_story_report_report FOREIGN KEY (report_id) REFERENCES anews.report (id) ON UPDATE NO ACTION ON DELETE NO ACTION, 
   CONSTRAINT fk_story_report_story FOREIGN KEY (story_id) REFERENCES anews.story (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH (OIDS = FALSE);
ALTER TABLE anews.story_report OWNER TO anews;

ALTER TABLE anews.settings ADD COLUMN color_report integer NOT NULL DEFAULT 16304128;

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0109', 'Relatórios', '01');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010901', 'Visualizar', '0109');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010902', 'Modificar', '0109');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010903', 'Excluir', '0109');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('010905', 'Imprimir', '0109');

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0110', 'Eventos dos Relatórios', '01');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011001', 'Visualizar', '0110');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011002', 'Modificar', '0110');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011003', 'Excluir', '0110');

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0111', 'Naturezas dos Relatórios', '01');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011101', 'Visualizar', '0111');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011102', 'Modificar', '0111');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('011103', 'Excluir', '0111');
