-- Limpa a tabela "document_has_program", por segurança
DELETE FROM anews.document_has_program;

-- Preenche as utilizações das reportagens em programas
INSERT INTO anews.document_has_program (document_id, program_id)
  SELECT DISTINCT reportage_id, program_id
    FROM anews.story
    JOIN anews.document
      ON document_id = id
    WHERE reportage_id IS NOT NULL AND program_id IS NOT NULL;

-- Preenche as utilizações das pautas em programas
INSERT INTO anews.document_has_program (document_id, program_id)
  SELECT DISTINCT guideline_id, program_id
    FROM anews.story
    JOIN anews.document
      ON document_id = id
    WHERE guideline_id IS NOT NULL AND program_id IS NOT NULL;
