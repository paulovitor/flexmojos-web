ALTER TABLE anews.settings ADD COLUMN cleaning_version_story integer;

UPDATE anews.settings SET cleaning_version_story = 3;
UPDATE anews.settings SET cleaning_version_guideline = 3;
UPDATE anews.settings SET cleaning_version_reportage = 3;

-- Quando apagar o grupo deve apagar suas permissões
ALTER TABLE anews.group_has_permission DROP CONSTRAINT fk_group_permission_group;
ALTER TABLE anews.group_has_permission ADD CONSTRAINT fk_group FOREIGN KEY (group_id) REFERENCES anews."group" (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- Apaga os grupos marcados como excluídos que não estão em uso
DELETE FROM anews.group AS g WHERE excluded = true AND NOT EXISTS (
	SELECT group_id FROM anews.user_has_group WHERE group_id = g.id
);

ALTER TABLE anews."group" DROP COLUMN excluded;

-- Limpa os editores do programa quando o programa é apagado
ALTER TABLE anews.program_has_editor DROP CONSTRAINT fk_program;
ALTER TABLE anews.program_has_editor ADD CONSTRAINT fk_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- Limpa os apresentadores do programa quando o programa é apagado
ALTER TABLE anews.program_has_presenter DROP CONSTRAINT fk_program_has_user_program;
ALTER TABLE anews.program_has_presenter ADD CONSTRAINT pk_program FOREIGN KEY (program_id) REFERENCES anews.program (id) ON UPDATE NO ACTION ON DELETE CASCADE;

-- Apaga os programas marcados como excluídos que não estão em uso
DELETE FROM anews.program AS prog WHERE excluded = true AND NOT EXISTS (
	SELECT program_id FROM anews.rundown WHERE program_id = prog.id
) AND NOT EXISTS (
	SELECT program_id FROM anews.guideline WHERE program_id = prog.id
);

ALTER TABLE anews.program DROP COLUMN excluded;

-- Apaga as instituições marcadas como excluídas que não estão em uso
DELETE FROM anews.institution AS inst WHERE excluded = true AND NOT EXISTS (
	SELECT institution_id FROM anews.round WHERE institution_id = inst.id
);

-- Apaga os segmentos marcados como excluídos que não estão em uso
DELETE FROM anews.segment AS seg WHERE excluded = true AND NOT EXISTS (
	SELECT segment_id FROM anews.institution WHERE segment_id = seg.id
);

ALTER TABLE anews.institution DROP COLUMN excluded;
ALTER TABLE anews.segment DROP COLUMN excluded;

UPDATE anews.permission SET id = '01010206' WHERE id = '01010205' and name = 'Barra Visualização';
INSERT INTO anews.permission(id, name, permission_id) VALUES ('01010205', 'Imprimir', '010102');

CREATE TABLE anews.mail_config (
   id serial, 
   email character varying(200), 
   host character varying(255), 
   username character varying(50), 
   password character varying(200), 
   port integer, 
   ssl integer, 
   CONSTRAINT mail_config_pkey PRIMARY KEY (id)
) WITH (OIDS = FALSE);
ALTER TABLE anews.mail_config OWNER TO anews;

INSERT INTO anews.permission (id, name, permission_id) VALUES ('0211', 'Servidor SMTP', '02');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021101', 'Visualizar', '0211');
INSERT INTO anews.permission (id, name, permission_id) VALUES ('021102', 'Editar', '0211');

ALTER TABLE anews.mail_config ALTER COLUMN username TYPE character varying(200);

UPDATE anews.news SET lead = concat(substr(lead, 0, 247), '...') where length(lead) > 250;
ALTER TABLE anews.news ALTER COLUMN lead TYPE character varying(250);

UPDATE anews.contact_number SET value = substr(value, 0, 30) where length(value) > 30;
ALTER TABLE anews.contact_number ALTER COLUMN value TYPE character varying(30);
