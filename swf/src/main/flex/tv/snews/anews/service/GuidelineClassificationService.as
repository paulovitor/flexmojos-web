package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.ChecklistResource;
	import tv.snews.anews.domain.GuidelineClassification;
	
	/**
	 * @author Samuel Guedes de Melo
	 * @since 1.5
	 */
	public class GuidelineClassificationService extends ServiceBase {
		
		private static var instance:GuidelineClassificationService;
		
		private const remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private const messagingLocator:MessageLocator = MessageLocator.getInstance();
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		//------------------------------------
		//  Singleton Constructor
		//------------------------------------
		
		public function GuidelineClassificationService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("guidelineClassificationService");
			consumer = messagingLocator.getConsumer("guidelineClassificationDestination");
			instance = this;
		}
		
		public static function getInstance():GuidelineClassificationService {
			return instance != null ? instance : new GuidelineClassificationService();
		}
		
		//------------------------------------
		//  Message Methods
		//------------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler, false, 0, true);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------------
		//  Service Methods
		//------------------------------------
		
		public function listAll(resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function save(classification:GuidelineClassification, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("save").send(classification);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(id:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("remove").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function isInUse(id:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isInUse").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
