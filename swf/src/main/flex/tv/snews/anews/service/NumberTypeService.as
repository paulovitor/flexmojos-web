package tv.snews.anews.service {
	
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	/**
	 * Implementação para acesso ao serviço de manipulação dos numeros do contato. 
	 * 
	 * @author Paulo Felipe.
	 * 
	 * @since 1.0.0
	 */
	public class NumberTypeService extends ServiceBase {
		
		private static var instance:NumberTypeService;
		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var remoteObject:RemoteObject;
		
		public function NumberTypeService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remoteObject = remoteService.getRemoteObject("numberTypeService");
			instance = this;
		}
		
		public static function getInstance():NumberTypeService {
			return instance != null ? instance : new NumberTypeService();
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		public function listAll(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler));
		}
	}
}