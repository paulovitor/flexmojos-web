package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.ContactGroup;

	/**
	 * Implementação para acesso ao serviço de manipulação dos grupos de contatos.
	 *
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	public class ContactGroupService extends ServiceBase {

		private static var instance:ContactGroupService;
		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function ContactGroupService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("contactGroupService");
			consumer = messagingLocator.getConsumer("contactGroupDestination");
			instance = this;
		}

		public static function getInstance():ContactGroupService {
			return instance != null ? instance : new ContactGroupService();
		}

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

		public function listAll(resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function saveContactGroup(contactGroup:ContactGroup, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(contactGroup);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function deleteById(id:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("deleteById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
