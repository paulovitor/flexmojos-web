package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.City;

	/**
	 * Implementação para acesso ao serviço de manipulação das cidades.
	 *
	 * @author Samuel Guedes de Melo.
	 *
	 * @since 1.0.0
	 */
	public class CityService extends ServiceBase {

		private static var instance:CityService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;


		public function CityService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("cityService");
			instance = this;
		}

		public static function getInstance():CityService {
			return instance != null ? instance : new CityService();
		}

		public function listAll(idState:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send(idState);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function save(city:City, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(city);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function remove(city:City, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("remove").send(city);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
