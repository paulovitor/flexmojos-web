package tv.snews.anews.service {

import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.messaging.events.MessageFaultEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.Checklist;
import tv.snews.anews.search.ChecklistParams;

/**
 * @author Felipe Zap de Mello
 * @since 1.5.0
 */
public class ChecklistService extends ServiceBase {

	private static const instances:Object = {};

	private var remoteLocator:RemoteObjectLocator;
	private var messageLocator:MessageLocator;

	private var remote:RemoteObject;
	private var consumer:Consumer;

	public function ChecklistService(serverName:String = null, serverPort:int = 0) {
		var key:String = instanceKey(serverName, serverPort);
		if (instances[key] != null) {
			throw new Error("Singleton exception");
		}

		remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
		messageLocator = MessageLocator.getInstance(serverName, serverPort);

		consumer = messageLocator.getConsumer("checklistDestination");
		remote = remoteLocator.getRemoteObject("checklistService");
	}

	public static function getInstance(serverName:String = null, serverPort:int = 0):ChecklistService {
		var key:String = instanceKey(serverName, serverPort);
		if (instances[key] == null) {
			instances[key] = new ChecklistService(serverName, serverPort);
		}
		return instances[key];
	}

	//------------------------------
	//	Message Methods
	//------------------------------

	public function subscribe(resultHandler:Function):void {
		consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
		consumer.subscribe();
	}

	public function unsubscribe(resultHandler:Function):void {
		consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
	}

	//------------------------------
	//	Service Methods
	//------------------------------

	public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("loadById").send(id);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function save(checklist:Checklist, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("save").send(checklist);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function remove(id:Number, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("remove").send(id);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findByParams(params:ChecklistParams, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("findByParams").send(params);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findChecklistByGuideline(idGuideline:Number, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("findChecklistByGuideline").send(idGuideline);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findAllNotExcludedOfDate(date:Date, notDone:Boolean, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("findAllNotExcludedOfDate").send(date, notDone);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function loadChangesHistory(id:Number, result:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("loadChangesHistory").send(id);
		token.addResponder(getResponder(result, faultHandler));
	}

	public function listExcludedChecklistPage(pageNumber:int, result:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("listExcludedChecklistPage").send(pageNumber);
		token.addResponder(getResponder(result, faultHandler));
	}

	public function restoreExcluded(id:Number, destinyDate:Date, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("restoreExcluded").send(id, destinyDate);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function checkEditionLock(id:int, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("checkEditionLock").send(id);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function lockEdition(id:int, resultHandler:Function = null, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("lockEdition").send(id);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function unlockEdition(id:int, resultHandler:Function = null, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("unlockEdition").send(id);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}
}
}
