package tv.snews.anews.service {
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	public class InfrastructureService extends ServiceBase {
		
		private static var singletonInstance:InfrastructureService;
		
		private var messageLocator:MessageLocator = MessageLocator.getInstance();
		private var consumer:Consumer;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		public function InfrastructureService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messageLocator.getConsumer("infrastructureDestination");
			singletonInstance = this;
		}
		
		public static function getInstance():InfrastructureService {
			return singletonInstance != null ? singletonInstance : new InfrastructureService();
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		/*public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}*/
	}
}