/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Company;
	
	/**
	 * Classe de serviço que faz as invocações dos métodos remotos da classe do 
	 * servidor CompanyService.
	 * 
	 * @author Samuel Guedes de Melo.
	 * @since 1.2.5
	 */
	public class CompanyService extends ServiceBase {
		
		private static var singletonInstance:CompanyService;

		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		
		private var remoteObject:RemoteObject;
		private var consumer:Consumer;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		/**
		 * Construtor padrão. Deve apenas ser usado internamente para a 
		 * construção da instância única do singleton desta classe.
		 */
		public function CompanyService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			remoteObject = remoteLocator.getRemoteObject("companyService");
			consumer = messagingLocator.getConsumer("companyDestination");
			
			singletonInstance = this;
		}
		
		/**
		 * Retorna a instância 'singleton' desta classe.
		 */
		public static function getInstance():CompanyService {
			return singletonInstance != null ? singletonInstance : new CompanyService();
		}
		
		//------------------------------
		//	Message Methods
		//------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function loadById(id:int, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObject.getOperation("loadById").send(id);
			token.addResponder(getResponder(faultHandler));
		}
		
		public function listCompanyByNameHost(name:String, host:String, result:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listCompanyByNameHost").send(name, host);
			token.addResponder(getResponder(result, fault));
		}
		
		public function save(company:Company, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("save").send(company);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(id:int, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("delete").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function listAll(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler));
		}
	}
}
