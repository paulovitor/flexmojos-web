package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.UserTeam;

/**
	 * Implementação para acesso ao serviço de manipulação das equipes.
	 *
	 * @author Maxuel Ramos.
	 * @since 1.7
	 */
	public class TeamService extends ServiceBase {

		private static var instance:TeamService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function TeamService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("teamService");
			consumer = messagingLocator.getConsumer("teamDestination");
			instance = this;
		}

		public static function getInstance():TeamService {
			return instance != null ? instance : new TeamService();
		}

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		public function listAll(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(result, fault));
		}

		public function listTeamByName(name:String, result:Function):void {
			var token:AsyncToken = remote.getOperation("listTeamByName").send(name);
			token.addResponder(getResponder(result, fault));
		}

		public function save(team:UserTeam, result:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(team);
			token.addResponder(getResponder(result, fault));
		}

		public function remove(id:int, result:Function):void {
			var token:AsyncToken = remote.getOperation("delete").send(id);
			token.addResponder(getResponder(result));
		}

        public function isInUse(id:int, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("isInUse").send(id);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }
	}
}
