package tv.snews.anews.service {

import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.messaging.events.MessageFaultEvent;
import mx.rpc.remoting.RemoteObject;
import mx.rpc.AsyncToken;

/**
 * Façada para os serviços do roteiro no servidor. Permite
 * o cliente fazer chamadas remotas aos serviços do espelho.
 *
 * @author Maxuel Ramos
 * @since 1.7
 */
public class ScriptPlanService extends ServiceBase {

	private static const instances:Object = {};

	private var remoteLocator:RemoteObjectLocator;
	private var messageLocator:MessageLocator;

	private var remote:RemoteObject;
	private var consumer:Consumer;

	public function ScriptPlanService(serverName:String = null, serverPort:int = 0) {
		var key:String = instanceKey(serverName, serverPort);
		if (instances[key] != null) {
			throw new Error("Singleton exception");
		}

		remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
		messageLocator = MessageLocator.getInstance(serverName, serverPort);

		consumer = messageLocator.getConsumer("scriptPlanDestination");
		remote = remoteLocator.getRemoteObject("scriptPlanService");
	}

	public static function getInstance(serverName:String = null, serverPort:int = 0):ScriptPlanService {
		var key:String = instanceKey(serverName, serverPort);
		if (instances[key] == null) {
			instances[key] = new ScriptPlanService(serverName, serverPort);
		}
		return instances[key];
	}

	//------------------------------
	//	Message Methods
	//------------------------------

	public function subscribe(resultHandler:Function):void {
		consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
		consumer.subscribe();
	}

	public function unsubscribe(resultHandler:Function):void {
		consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
	}

	//------------------------------
	//	Service Methods
	//------------------------------

	public function create(date:Date, programId:int, resultHandler:Function):void {
		var token:AsyncToken = remote.getOperation("create").send(date, programId);
		token.addResponder(getResponder(resultHandler));
	}

	public function verifyScriptPlan(date:Date, programId:Number, resultHandler:Function):void {
		var token:AsyncToken = remote.getOperation("verifyScriptPlan").send(date, programId);
		token.addResponder(getResponder(resultHandler));
	}

	public function load(date:Date, programId:Number, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("load").send(date, programId);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function updateField(scritpPlanId:int, field:String, value:Object, resultHandler:Function = null, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("updateField").send(scritpPlanId, field, value);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function sortScripts(scriptId:Number, resultHandler:Function = null, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("sortScripts").send(scriptId);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function lockScriptPlan(planId:Number, resultHandler:Function = null, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("lockScriptPlan").send(planId);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function unlockScriptPlan(planId:Number, resultHandler:Function = null, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("unlockScriptPlan").send(planId);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}
    public function sendCredits(scriptPlanId:Number, resultHandler:Function, faultHandler:Function = null):void {
        var token:AsyncToken = remote.getOperation("sendCredits").send(scriptPlanId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function resetScriptPlanExhibition(scriptPlanId:Number, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("resetScriptPlanExhibition").send(scriptPlanId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function completeScriptPlanExhibition(scriptPlanId:Number, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("completeScriptPlanExhibition").send(scriptPlanId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function synchronizeStatusVideosWS(scriptPlanId:Number, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("synchronizeStatusVideosWS").send(scriptPlanId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

}
}
