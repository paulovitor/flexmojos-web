package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.ChecklistResource;
	
	/**
	 * @author Samuel Guedes de Melo
	 * @since 1.5
	 */
	public class ChecklistResourceService extends ServiceBase {
		
		private static var instance:ChecklistResourceService;
		
		private const remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private const messagingLocator:MessageLocator = MessageLocator.getInstance();
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		//------------------------------------
		//  Singleton Constructor
		//------------------------------------
		
		public function ChecklistResourceService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("checklistResourceService");
			consumer = messagingLocator.getConsumer("checklistResourceDestination");
			instance = this;
		}
		
		public static function getInstance():ChecklistResourceService {
			return instance != null ? instance : new ChecklistResourceService();
		}
		
		//------------------------------------
		//  Message Methods
		//------------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler, false, 0, true);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------------
		//  Service Methods
		//------------------------------------
		
		public function listAll(resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function save(event:ChecklistResource, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("save").send(event);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(id:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("remove").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function isInUse(id:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isInUse").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}