package tv.snews.anews.service {

import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.search.DocumentParams;

/**
	 * Façada para os serviços de documents no servidor.
	 *
	 * @author Felipe Zap de Mello
	 * @since 1.6.4
	 */
	public class DocumentService extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function DocumentService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			remote = remoteLocator.getRemoteObject("documentService");
			consumer = messageLocator.getConsumer("documentDestination");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):DocumentService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new DocumentService(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Message Methods
		//------------------------------

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function fullTextSearch(searchParams:DocumentParams, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("fullTextSearch").send(searchParams);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	
		public function findByParams(searchParams:DocumentParams, drawerSearch:Boolean, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("findByParams").send(searchParams, drawerSearch);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

        public function archiveGuidelineToDrawer(documentId:Number, programId:int, toAdd:Boolean, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("archiveGuidelineToDrawer").send(documentId, programId, toAdd);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function archiveReportageToDrawer(documentId:Number, programId:int, toAdd:Boolean, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("archiveReportageToDrawer").send(documentId, programId, toAdd);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function archiveStoryToDrawer(documentId:Number, programId:int, toAdd:Boolean, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("archiveStoryToDrawer").send(documentId, programId, toAdd);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function archiveStoriesToDrawer(idsOfStories:Array, rundownId:int, toAdd:Boolean, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("archiveStoriesToDrawer").send(idsOfStories, rundownId, toAdd);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function archiveScriptToDrawer(documentId:Number, programId:int, toAdd:Boolean, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("archiveScriptToDrawer").send(documentId, programId, toAdd);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function archiveScriptsToDrawer(idsOfScripts:Array, scriptPlanId:int, toAdd:Boolean, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("archiveScriptsToDrawer").send(idsOfScripts, scriptPlanId, toAdd);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

	}
}
