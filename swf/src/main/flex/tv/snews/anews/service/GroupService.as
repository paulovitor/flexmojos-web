package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.User;
	import tv.snews.anews.domain.UserGroup;

	/**
	 * Implementação para acesso ao serviço de manipulação dos Grupos.
	 *
	 * @author Felipe Zap de Mello.
	 * @since 1.0.0
	 */
	public class GroupService extends ServiceBase {

		private static var instance:GroupService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function GroupService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("groupService");
			consumer = messagingLocator.getConsumer("groupDestination");
			instance = this;
		}

		public static function getInstance():GroupService {
			return instance != null ? instance : new GroupService();
		}

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		public function listAll(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(result, fault));
		}

		public function listGroupByName(name:String, result:Function):void {
			var token:AsyncToken = remote.getOperation("listGroupByName").send(name);
			token.addResponder(getResponder(result, fault));
		}

		public function save(group:UserGroup, result:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(group);
			token.addResponder(getResponder(result, fault));
		}

		public function remove(id:int, result:Function):void {
			var token:AsyncToken = remote.getOperation("delete").send(id);
			token.addResponder(getResponder(result));
		}
	}
}
