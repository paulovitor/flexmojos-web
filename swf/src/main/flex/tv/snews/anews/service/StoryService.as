package tv.snews.anews.service {
	
	import mx.collections.ArrayCollection;
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.ClipboardAction;
	import tv.snews.anews.domain.Program;
	import tv.snews.anews.domain.Story;
	import tv.snews.anews.domain.StoryMosVideo;
	import tv.snews.anews.domain.StoryWSVideo;
	import tv.snews.anews.domain.User;
	
	/**
	 * Façada para os serviços do story no servidor.
	 *
	 * @author Paulo Felipe
	 * @since 1.0.0
	 */
	public class StoryService extends ServiceBase {
		
		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		public function StoryService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			remote = remoteLocator.getRemoteObject("storyService");
			consumer = messageLocator.getConsumer("storyDestination");
		}
		
		public static function getInstance(serverName:String=null, serverPort:int=0):StoryService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new StoryService(serverName, serverPort);
			}
			return instances[key];
		}
		
		//------------------------------
		//	Message Methods
		//------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function createStory(blockId:Number, index:int, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("createStory").send(blockId, index);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function createStoryFromReportage(blockId:Number, index:int, reportageId:int, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("createStoryFromReportage").send(blockId, index, reportageId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function createStoryFromGuideline(blockId:Number, index:int, guidelineId:int, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("createStoryFromGuideline").send(blockId, index, guidelineId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function createStoryFromNews(blockId:Number, index:int, newsId:int, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("createStoryFromNews").send(blockId, index, newsId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function copyDrawerStoryToBlock(storyId:Number, blockId:Number, index:int = -1, resultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("copyDrawerStoryToBlock").send(storyId, blockId, index);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function removeStory(storyId:Number, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("removeStory").send(storyId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function updateField(storyId:Number, field:String, value:Object, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("updateField").send(storyId, field, value);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function moveStory(blockDestinationId:Number, blockOrder:Number, storyId:Number):void {
			var token:AsyncToken = remote.getOperation("moveStory").send(blockDestinationId, blockOrder, storyId);
			token.addResponder(getResponder());
		}
		
		public function moveStories(storiesIds:Array, blockId:Number, dropIndex:int, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("moveStories").send(storiesIds, blockId, dropIndex);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function merge(story:Story, resultHandler:Function, faultHandler:Function=null):void {
			story.revisions = null; // resolve problema da conversão do ArrayCollection p/ SortedSet
			var token:AsyncToken = remote.getOperation("merge").send(story);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function save(story:Story, resultHandler:Function, faultHandler:Function=null):void {
			story.revisions = null; // resolve problema da conversão do ArrayCollection p/ SortedSet
			var token:AsyncToken = remote.getOperation("save").send(story);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function countExcludedStories(resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("countExcludedStories").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function isLockedForEdition(storyId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isLockedForEdition").send(storyId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function lockEdition(storyId:Number, userId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(storyId, userId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

        public function assumeEdition(storyId:Number, userId:Number, droppedUserId:Number, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("assumeEdition").send(storyId, userId, droppedUserId);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }
		
		//		public function lockDragging(storyId:Number, userId:Number, resultHandler:Function = null, faultHandler:Function = null):void {
		//			var token:AsyncToken = remote.getOperation("lockDragging").send(storyId, userId);
		//			token.addResponder(getResponder(resultHandler, faultHandler));
		//		}
		
		//		public function unlockDragging(storyId:Number, resultHandler:Function = null, faultHandler:Function = null):void {
		//			var token:AsyncToken = remote.getOperation("unlockDragging").send(storyId);
		//			token.addResponder(getResponder(resultHandler, faultHandler));
		//		}
		
		public function unlockEdition(storyId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(storyId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function listExcludedStories(page:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listExcludedStories").send(page);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function restoreExcludedStory(storyId:Number, date:Date, programId:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("restoreExcludedStory").send(storyId, date, programId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function sendToDrawer(storyId:Number, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("sendToDrawer").send(storyId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function sendReportageToDrawer(reportageId:Number, programId:int, resultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("sendReportageToDrawer").send(reportageId, programId);
			token.addResponder(getResponder(resultHandler));
		}

		public function copyTo(storiesIds:Array, date:Date, programId:int, move:Boolean, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("copyTo").send(storiesIds, date, programId, move);
			token.addResponder(getResponder(resultHandler));
		}

		public function copyRemoteStory(remoteStory:Story, destinyDate:Date, destinyProgram:Program, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("copyRemoteStory").send(remoteStory, destinyDate, destinyProgram);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remoteCopyRequest(id:Number, nickname:String, company:String, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("remoteCopyRequest").send(id, nickname, company);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function sendStoriesToClipboard(action:String, storiesIds:Array, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("sendStoriesToClipboard").send(action, storiesIds);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function pastStoriesFromClipboard(blockId:Number, dropIndex:int, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("pastStoriesFromClipboard").send(blockId, dropIndex);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function clearClipboard(resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("clearClipboard").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function loadChangesHistory(storyId:Number, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("loadChangesHistory").send(storyId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function loadRevision(storyId:Number, revisionNumber:int, showDiffs:Boolean, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadRevision").send(storyId, revisionNumber, showDiffs);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function storiesByFullTextSearch(slug:String, content:String, program:Program, start:Date, end:Date, pageNumber:int, searchType:String, textIgnore:String, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("storiesByFullTextSearch").send(slug, content, program, start, end, pageNumber, searchType, textIgnore);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listStoriesByDateEditorOrderProgram(date:Date, editor:User, resultHandler:Function):void {
			var token:AsyncToken=remote.getOperation("listStoriesByDateEditorOrderProgram").send(date, editor);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listByDateAndProgram(date:Date, program:Program, resultHandler:Function):void {
			var token:AsyncToken=remote.getOperation("listByDateAndProgram").send(date, program);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function loadVideosByStoryId(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadVideosByStoryId").send(id);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function removeVideoByStoryMosObjById(idStory:Number, storyMosObj:StoryMosVideo, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("removeVideoByStoryMosObjById").send(idStory, storyMosObj);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function createStoryMosVideo(storyId:Number, storyMosVideo:StoryMosVideo, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("createStoryMosVideo").send(storyId, storyMosVideo);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function createStoryWSVideo(storyId:Number, storyWSVideo:StoryWSVideo, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("createStoryWSVideo").send(storyId, storyWSVideo);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function removeFromDrawer(storyId:Number, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("removeFromDrawer").send(storyId);
			token.addResponder(getResponder(resultHandler));
		}

        public function loadAndUpdateStoryWSVideo(storyId:Number, storyWSVideo:StoryWSVideo, resultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("loadAndUpdateStoryWSVideo").send(storyId, storyWSVideo);
            token.addResponder(getResponder(resultHandler));
        }

        public function removeStories(idsOfStories:Array, rundownId:int, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("removeStories").send(idsOfStories, rundownId);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

}
}
