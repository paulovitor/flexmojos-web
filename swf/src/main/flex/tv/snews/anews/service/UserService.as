package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.User;
	import tv.snews.anews.domain.UserGroup;

	/**
	 * Implementação para acesso ao serviço de manipulação dos Usuários.
	 *
	 * @author Paulo Felipe.
	 * @author Eliezer Reis
	 *
	 * @since 1.0.0
	 */
	public class UserService extends ServiceBase {
		
		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function UserService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			remote = remoteLocator.getRemoteObject("userService");
			consumer = messageLocator.getConsumer("userDestination");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):UserService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new UserService(serverName, serverPort);
			}
			return instances[key];
		}

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

		public function loadByCredentials(email:String, password:String, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("loadByCredentials").send(email, password);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		/**
		 * Envia e-mail de recuperação de senha para o usuário.
		 */
		public function recoveryPassword(mail:String, result:Function, fault:Function):void {
			var token:AsyncToken = remote.getOperation("recoveryPassword").send(mail);
			token.addResponder(getResponder(result, fault));
		}

		public function changePassword(mail:String, oldPassword:String, newPassword:String, result:Function, fault:Function):void {
			var token:AsyncToken = remote.getOperation("changePassword").send(mail, oldPassword, newPassword);
			token.addResponder(getResponder(result, fault));
		}

		public function listUsersByGroup(group:UserGroup, result:Function):void {
			var token:AsyncToken = remote.getOperation("listUsersByGroupAndStatus").send(group);
			token.addResponder(getResponder(result));
		}

		public function save(user:User, result:Function, error:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(user);
			token.addResponder(getResponder(result, error));
		}

		public function saveData(user:User, pass:String, newPass:String, confirmation:String, result:Function, error:Function):void {
			var token:AsyncToken = remote.getOperation("saveData").send(user, pass, newPass, confirmation);
			token.addResponder(getResponder(result, error));
		}

		public function del(user:User, result:Function):void {
			var token:AsyncToken = remote.getOperation("delete").send(user);
			token.addResponder(getResponder(result));
		}

		public function disconnectUser(user:User, result:Function):void {
			var token:AsyncToken = remote.getOperation("disconnectUser").send(user);
			token.addResponder(getResponder(result));
		}

		public function requestUserToAgreeWithTerms(userId:int, result:Function):void {
			var token:AsyncToken = remote.getOperation("requestUserToAgreeWithTerms").send(userId);
			token.addResponder(getResponder(result));
		}

		public function listReporters(result:Function):void {
			var token:AsyncToken = remote.getOperation("listReporters").send();
			token.addResponder(getResponder(result));
		}
		
		public function listProducers(result:Function):void {
			var token:AsyncToken = remote.getOperation("listProducers").send();
			token.addResponder(getResponder(result));
		}

        public function listChecklistProducers(result:Function):void {
            var token:AsyncToken = remote.getOperation("listChecklistProducers").send();
            token.addResponder(getResponder(result));
        }
		
		public function listEditors(result:Function):void {
			var token:AsyncToken = remote.getOperation("listEditors").send();
			token.addResponder(getResponder(result));
		}

		public function listAllOrderByNickname(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(result));
		}

		public function listAllOrderByName(searchTerm:String, result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send(searchTerm);
			token.addResponder(getResponder(result));
		}

		public function releaseLocksFromUser(user:User, result:Function = null):void {
			var token:AsyncToken = remote.getOperation("releaseLocksFromUser").send(user);
			token.addResponder(getResponder(result));
		}

        public function listByGroupName(searchTerm:String, groupId:int, belongsToTheGroup:Boolean, result:Function):void {
            var token:AsyncToken = remote.getOperation("listByGroupName").send(searchTerm, groupId, belongsToTheGroup);
            token.addResponder(getResponder(result));
        }

        public function listByTeamName(searchTerm:String, teamId:int, belongsToTheTeam:Boolean, result:Function):void {
            var token:AsyncToken = remote.getOperation("listByTeamName").send(searchTerm, teamId, belongsToTheTeam);
            token.addResponder(getResponder(result));
        }

        public function collectMenu(userId:Number, value:Boolean, resultHandler:Function=null, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("collectMenu").send(userId, value);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

	}
}
