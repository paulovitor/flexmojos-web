package tv.snews.anews.service {

	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.UserFile;

	/**
	 *
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class UserFileService extends ServiceBase {
		private static var instance:UserFileService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var remote:RemoteObject;


		public function UserFileService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("userFileService");
			instance = this;
		}

		public static function getInstance():UserFileService {
			return instance != null ? instance : new UserFileService();
		}

		public function saveUserFile(userFile:UserFile, result:Function):void {
			var token:AsyncToken = remote.getOperation("saveUserFile").send(userFile);
			token.addResponder(getResponder(result, fault));
		}

	}
}
