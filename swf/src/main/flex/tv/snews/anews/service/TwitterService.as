package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.TwitterUser;
	
	/**
	 * @author Felipe Zap de Mello
	 * @since 1.0.0
	 */
	public class TwitterService extends ServiceBase {
		
		private static var singletonInstance:TwitterService;
		
		public static const UPDATED_TWEETS_TYPE:String = "newTweetsTwitterUser";
		public static const DELETED_TWITTER_USER_TYPE:String = "deletedTwitterUser";
		public static const INSERTED_TWITTER_USER_TYPE:String = "insertedTwitterUser";
		
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		
		private var consumer:Consumer;
		private var remoteObject:RemoteObject;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		public function TwitterService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messagingLocator.getConsumer("twitterDestination");
			remoteObject = remoteLocator.getRemoteObject("twitterService");
			singletonInstance = this;
		}
		
		public static function getInstance():TwitterService {
			return singletonInstance != null ? singletonInstance : new TwitterService();
		}
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function listAllTwitterUser(resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("listAllTwitterUser").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function saveTwitterUser(twitterUser:TwitterUser, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("save").send(twitterUser);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function deleteTwitterUser(twitterUser:TwitterUser, resultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("delete").send(twitterUser);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function checkTwitterUser(screenName:String, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("checkTwitterUser").send(screenName);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function countTweetsInfoFromAllTwitterUsers(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("countTweetInfoFromAllTwitterUsers").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function countTweetsFromTwitterUser(twitterUser:TwitterUser , resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("countTweetsFromTwitterUser").send(twitterUser);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listTweetsInfoFromAllTweetUsers(start:int, count:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listTweetsInfoFromAllTweetUsers").send(start, count);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, start));
		}
		
		public function listTweetsInfoFromTwitterUser(twitterUser:TwitterUser, start:int, count:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listTweetsInfoFromTwitterUser").send(twitterUser, start, count);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, start));
		}
		
		public function findTwitterUserByScreenName(screenName:String, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("findTwitterUserByScreenName").send(screenName);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function findTweetInfoById(idEntity:Number, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("findTweetInfoById").send(idEntity);
			token.addResponder(getResponder(resultHandler));
		}
	}
}
