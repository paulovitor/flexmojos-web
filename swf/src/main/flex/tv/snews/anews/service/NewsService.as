package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.News;
import tv.snews.anews.search.NewsParams;

/**
	 * Implementação para acesso ao serviço de manipulação das notícias. 
	 * 
	 * @author Felipe Pinheiro
	 * @author Paulo Felipe
	 * @author Samuel Guedes
	 * @since 1.0.0
	 */
	public class NewsService extends ServiceBase {
		private static var instance:NewsService;
		
		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		public function NewsService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteLocator.getRemoteObject("newsService");
			consumer = messagingLocator.getConsumer("newsDestination");
			instance = this;
		}
		
		public static function getInstance():NewsService {
			return instance != null ? instance : new NewsService();
		}
		
		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function listAll(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(result));
		}

		public function listFirstTwenty(result:Function):void {
			var token:AsyncToken = remote.getOperation("listFirstTwenty").send();
			token.addResponder(getResponder(result));
		}

		public function listAllPage(page:int, result:Function):void {
			var token:AsyncToken = remote.getOperation("listAllPage").send(page);
			token.addResponder(getResponder(result));
		}

        public function findByParams(params:NewsParams, result:Function):void {
            var token:AsyncToken = remote.getOperation("findByParams").send(params);
            token.addResponder(getResponder(result));
        }
		
		public function save(news:News, result:Function, fault:Function, area:*):void {
			var token:AsyncToken = remote.getOperation("save").send(news);
			token.addResponder(getAsyncResponder(result, fault, area));
		}
		
		public function exclude(news:News, result:Function, fault:Function, area:*):void {
			var token:AsyncToken = remote.getOperation("delete").send(news);
			token.addResponder(getAsyncResponder(result, fault, area));
		}
	
		public function lockEdition(newsId:Number, userId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(newsId, userId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function unlockEdition(newsId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(newsId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function isLockedForEdition(newsId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isLockedForEdition").send(newsId);
			token.addResponder(getResponder(resultHandler));
		}
	}
}