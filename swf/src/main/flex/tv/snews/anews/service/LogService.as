package tv.snews.anews.service {

	import mx.collections.ArrayCollection;
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.User;

	
	public class LogService extends ServiceBase {

		private static var instance:LogService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function LogService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messagingLocator.getConsumer("logDestination");
			remote = remoteService.getRemoteObject("logService");
			instance = this;
		}

		public static function getInstance():LogService {
			return instance != null ? instance : new LogService();
		}

		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		public function listLogsByRundownAction(rundownId:int, actions:ArrayCollection, pageNumber:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listLogsByRundownAction").send(rundownId, actions, pageNumber);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listLogsByBlockAction(rundownId:int, actions:ArrayCollection, pageNumber:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listLogsByBlockAction").send(rundownId, actions, pageNumber);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listLogsByStoryAction(rundownId:int, actions:ArrayCollection, pageNumber:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listLogsByStoryAction").send(rundownId, actions, pageNumber);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listAll(rundownId:int, pageNumber:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send(rundownId, pageNumber);
			token.addResponder(getResponder(resultHandler));
		}

	}
}