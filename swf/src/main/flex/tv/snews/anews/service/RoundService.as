package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Round;
	
	/**
	 * Implementação para acesso ao serviço de manipulação das rondas. 
	 * 
	 * @author Samuel Guedes de Melo.
	 * 
	 * @since 1.0.0
	 */
	public class RoundService extends ServiceBase {
		
		private static var instance:RoundService;
		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		public function RoundService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("roundService");
			consumer = messagingLocator.getConsumer("roundDestination");
			instance = this;
		}
		
		public static function getInstance():RoundService {
			return instance != null ? instance : new RoundService();
		}
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		public function listAll(date:Date, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send(date);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function save(round:Round, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(round);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(id:int, result:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("remove").send(id);
			token.addResponder(getResponder(result, faultHandler));
		}
		
		public function lockEdition(roundId:Number, userId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(roundId, userId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function unlockEdition(roundId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(roundId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function isLockedForEdition(roundId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isLockedForEdition").send(roundId);
			token.addResponder(getResponder(resultHandler));
		}
	}
}