package tv.snews.anews.service {

import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.net.navigateToURL;

import mx.collections.ArrayCollection;

import mx.core.FlexGlobals;
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
import mx.utils.URLUtil;
	
	import tv.snews.anews.domain.Program;
	import tv.snews.anews.domain.Rundown;
	import tv.snews.anews.domain.RundownTemplate;
	import tv.snews.anews.domain.Story;
	import tv.snews.anews.domain.User;

	/**
	 * Façada para os serviços do espelho no servidor. Permite
	 * o cliente fazer chamadas remotas aos serviços do espelho.
	 *
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class RundownService extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function RundownService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			consumer = messageLocator.getConsumer("rundownDestination");
			remote = remoteLocator.getRemoteObject("rundownService");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):RundownService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new RundownService(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		public function createRundown(date:Date, programId:int, templateId:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("createRundown").send(date, programId, templateId);
			token.addResponder(getResponder(resultHandler));
		}

		public function loadRundown(date:Date, programId:Number, fetchAll:Boolean, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("loadRundown").send(date, programId, fetchAll);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function verifyRundown(date:Date, programId:Number, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("verifyRundown").send(date, programId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function currentTime(resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("currentTime").send();
			token.addResponder(getResponder(resultHandler));
		}

		public function updateField(rundownId:int, field:String, value:Object, resultHandler:Function = null, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("updateField").send(rundownId, field, value);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function sortStories(rundownId:Number):void {
			var token:AsyncToken = remote.getOperation("sortStories").send(rundownId);
			token.addResponder(getResponder());
		}
		
		public function generateTeleprompterText(rundownId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("generateTeleprompterText").send(rundownId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function sendCredits(rundownId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("sendCredits").send(rundownId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function totalRundowns(resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("totalRundowns").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listRundowns(firstResult:int, maxResults:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("listRundowns").send(firstResult, maxResults);
			token.addResponder(getResponder(resultHandler));
		}

        public function resetStoriesDisplay(rundownId:Number, resultHandler:Function, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("resetDisplayOfStories").send(rundownId);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function completeStoriesDisplay(rundownId:Number, resultHandler:Function, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("completeStoriesDisplay").send(rundownId);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

		public function synchronizeStatusVideosWS(rundownId:Number, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("synchronizeStatusVideosWS").send(rundownId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function loadAssetsIds(rundownId:Number, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("loadAssetsIds").send(rundownId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function exportPlaylist(rundownId:Number, mediasIds:ArrayCollection):void {
			var aux:String = URLUtil.getServerNameWithPort(FlexGlobals.topLevelApplication.url);
			var url:String = "http://" + aux + "/flexmojos-web/export/playlist.do";

			var data:URLVariables = new URLVariables();
			data["rundownId"] = rundownId;
			data["mediasIds"] = mediasIds.source.join("|");
			data["time"] = Math.round(Math.random() * 9999);

			var request:URLRequest = new URLRequest(url);
			request.data = data;
			request.method = URLRequestMethod.POST;

			navigateToURL(request, "_blank");
		}
	}
}
