package tv.snews.anews.service {
	
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	/**
	 * Implementação para acesso ao serviço de manipulação de Permissão.
	 * 
	 * @author Eliezer Reis.
	 * @since 0.0.1
	 */
	public class PermissionService extends ServiceBase {
		private static var instance:PermissionService;
		
		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var remote:RemoteObject;
		
		/**
		 * Construtor de PermissionService. 
		 */
		public function PermissionService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("permissionService");
			instance = this;
		}
		
		public static function getInstance():PermissionService {
			if (instance == null) {
				instance = new PermissionService();
			}
			return instance;
		}
		
		/**
		 * Lista todas as permissões cadastradas no sistema.
		 */
		public function listAll(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(result));
		}
	}
}