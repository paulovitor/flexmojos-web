package tv.snews.anews.service {

	import mx.collections.ArrayCollection;
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.AbstractMessage;
	import tv.snews.anews.domain.Chat;
	import tv.snews.anews.domain.EntityType;
	import tv.snews.anews.domain.User;
	import tv.snews.anews.domain.UserFile;

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MessageService extends ServiceBase {

		private static var singletonInstance:MessageService;

		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();

		private var consumer:Consumer;
		private var remoteObject:RemoteObject;

		//------------------------------
		//	Singleton Constructor
		//------------------------------
		public function MessageService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			remoteObject = remoteLocator.getRemoteObject("messageService");
			consumer = messagingLocator.getConsumer("chatDestination");
			singletonInstance = this;
		}

		public static function getInstance():MessageService {
			return singletonInstance != null ? singletonInstance : new MessageService();
		}

		//------------------------------
		//	Messsage Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

		//------------------------------
		//	Service Methods
		//------------------------------
		public function sendMessage(content:String, chat:Chat, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("sendMessage").send(content, chat);
			token.addResponder(getResponder(resultHandler));
		}

		public function sendFile(file:UserFile, chat:Chat, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("sendFile").send(file, chat);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function sendEntityMessage(obj:Object, chat:Chat, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("sendEntityMessage").send(obj, chat);
			token.addResponder(getResponder(resultHandler));
		}

		public function loadLastMessagesWith(user:User, limit:int, firstMessageChatResult:Date, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("loadLastMessagesWith").send(user, limit, firstMessageChatResult);
			token.addResponder(getResponder(resultHandler));
		}

		public function loadLastMessagesByChat(chat:Chat, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("loadMessagesByChat").send(chat);
			token.addResponder(getResponder(resultHandler));
		}

		public function markAsReaded(message:AbstractMessage, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("markAsReaded").send(message);
			token.addResponder(getResponder(resultHandler));
		}

		public function markAsReadedList(messages:ArrayCollection, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("markAsReadedList").send(messages);
			token.addResponder(getResponder(resultHandler));
		}

		public function loadOfflineMessages(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("loadOfflineMessages").send();
			token.addResponder(getResponder(resultHandler));
		}

		public function isTyping(status:Boolean, chat:Chat, resultHandler:Function = null):void {
			var token:AsyncToken = remoteObject.getOperation("notifyUserIsTyping").send(status, chat);
			token.addResponder(getResponder(resultHandler));
		}
	}
}
