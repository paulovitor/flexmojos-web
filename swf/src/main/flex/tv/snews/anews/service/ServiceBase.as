package tv.snews.anews.service {

import mx.managers.CursorManager;
import mx.messaging.events.MessageFaultEvent;
import mx.resources.ResourceManager;
import mx.rpc.AsyncResponder;
import mx.rpc.Fault;
import mx.rpc.Responder;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.ExceptionType;

/**
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class ServiceBase {

	public function faultHandler(event:FaultEvent):void {
		fault(extractFaultDetails(event.fault));
	}

	protected function fault(details:Object):void {
		switch (details.exceptionClass) {
			case ExceptionType.UNAUTHORIZED_EXCEPTION:
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "session.unauthorized"), Info.ERROR);
				break;
			case ExceptionType.ILLEGAL_OPERATION_EXCEPTION:
			case ExceptionType.VALIDATION_EXCEPTION:
				ANews.showInfo(details.exceptionMessage, Info.WARNING);
				break;
			default:
				ANews.showInfo(details.fullMessage, Info.ERROR);
		}
	}

	protected function _faultMessage(event:MessageFaultEvent):void {
		ANews.showInfo(event.faultDetail, Info.ERROR);
	}

	protected function getResponder(resultHandler:Function = null, faultHandler:Function = null, uniqueId:Object = null, verbose:Boolean = true):Responder {
		if (verbose) {
			CursorManager.setBusyCursor();
		}

		var responder:Responder = new Responder(
				function (event:ResultEvent):void {
					if (uniqueId) {
						event.token.uniqueId = uniqueId;
					}
					if (verbose) {
						CursorManager.removeBusyCursor();
					}

					if (resultHandler != null) {
						resultHandler(event);
					}
				},
				function (event:FaultEvent):void {
					var details:Object = extractFaultDetails(event.fault);

					if (uniqueId) {
						event.token.uniqueId = uniqueId;
					}
					if (verbose) {
						CursorManager.removeBusyCursor();
					}

					if (faultHandler != null && details.exceptionClass != ExceptionType.UNAUTHORIZED_EXCEPTION) {
						faultHandler(details);
					} else {
						fault(details);
					}
				}
		);
		return responder;
	}

	protected function getAsyncResponder(resultHandler:Function = null, faultHandler:Function = null, token:Object = null, verbose:Boolean = true):AsyncResponder {
		if (verbose) {
			CursorManager.setBusyCursor();
		}

		var responder:AsyncResponder = new AsyncResponder(
				function (event:ResultEvent, token:Object = null):void {
					if (verbose) {
						CursorManager.removeBusyCursor();
					}

					if (resultHandler != null) {
						// Diferente do método getResponder(), aqui o token é passado como parâmetro
						resultHandler(event, token);
					}
				},
				function (event:FaultEvent, token:Object = null):void {
					var details:Object = extractFaultDetails(event.fault);

					if (verbose) {
						CursorManager.removeBusyCursor();
					}
					if (faultHandler != null && details.exceptionClass != ExceptionType.UNAUTHORIZED_EXCEPTION) {
						// Diferente do método getResponder(), aqui o token é passado como parâmetro
						faultHandler(event, token);
					} else {
						fault(details);
					}
				}, token
		);

		return responder;
	}

	protected static function instanceKey(...args):String {
		return args.join("_");
	}

	private static function extractFaultDetails(fault:Fault):Object {
		var details:Object = {
			exceptionClass: '',
			exceptionMessage: '',
			fullMessage: ''
		};

		if (fault.faultString) {
			// Geralmente a classe da exception vem no início, seguido por ' : ' e então a mensagem da exception
			var tokens:Array = fault.faultString.split(' : ');

			if (tokens.length > 1) {
				details.exceptionClass = tokens[0];

				// Junta os demais tokens para formar a mensagem da exception
				for (var i:int = 1; i < tokens.length; i++) {
					details.exceptionMessage += tokens[i] + ' ';
				}
			} else {
				// Baseando-se no código antigo, parece que a UnauthorizedException não vem no formato padrão
				if (fault.faultString.indexOf(ExceptionType.UNAUTHORIZED_EXCEPTION) != -1) {
					details.exceptionClass = ExceptionType.UNAUTHORIZED_EXCEPTION;
				}
			}
		}

		details.fullMessage =
				'faultCode: [ ' + fault.faultCode + ' ]\n' +
				'faultDetail: [ ' + fault.faultDetail + ' ]\n' +
				'faultString: [ ' + fault.faultString + ' ]';

		return details;
	}
}
}
