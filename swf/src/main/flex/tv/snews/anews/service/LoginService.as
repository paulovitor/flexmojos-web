package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.ChannelEvent;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.User;
	import tv.snews.anews.infra.secure.SecurityHelper;

	/**
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class LoginService extends ServiceBase {

		private static const instances:Object = {};

		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		public function LoginService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);

			remote = remoteLocator.getRemoteObject("loginService");
			consumer = messageLocator.getConsumer("sessionDestination");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):LoginService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new LoginService(serverName, serverPort);
			}
			return instances[key];
		}
		
		//------------------------------
		//	Message Operations
		//------------------------------
		public function subscribe(resultHandler:Function, connectHandler:Function = null, 
								  disconnectHandler:Function = null):void {
			
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.addEventListener(ChannelEvent.DISCONNECT, function(event:ChannelEvent):void {
				if (disconnectHandler != null)
					disconnectHandler(event);
			});

			consumer.addEventListener(ChannelEvent.CONNECT, function(event:ChannelEvent):void {
				if (connectHandler != null)
					connectHandler(event);
			});

			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		public function getCurrentUser(result:Function):void {
			var token:AsyncToken = remote.getOperation("getCurrentUser").send();
			token.addResponder(getResponder(result));
		}

		public function login(user:User, resultHandler:Function, faultHandler:Function=null):void {
			var timezoneOffset:Number = -new Date().getTimezoneOffset() * 60 * 1000;
			var token:AsyncToken = remote.getOperation("login").send(user, timezoneOffset);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function agreeWithTerms(userId:int, result:Function, fault:Function):void {
			var token:AsyncToken = remote.getOperation("agreeWithTerms").send(userId);
			token.addResponder(getResponder(result, fault));
		}

		public function leave(resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("leave").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
