package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.RssCategory;
	import tv.snews.anews.domain.RssFeed;

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class RssService extends ServiceBase {
		
		private static var singletonInstance:RssService;

		public static const FEED_INSERTED:String	 = "FEED_INSERTED";
		public static const FEED_UPDATED:String		 = "FEED_UPDATED";
		public static const FEED_DELETED:String		 = "FEED_DELETED";
		public static const CATEGORY_INSERTED:String = "CATEGORY_INSERTED";
		public static const CATEGORY_UPDATED:String	 = "CATEGORY_UPDATED";
		public static const CATEGORY_DELETED:String	 = "CATEGORY_DELETED";
		
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		
		private var consumer:Consumer;
		private var remoteObject:RemoteObject;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		public function RssService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messagingLocator.getConsumer("rssDestination");
			remoteObject = remoteLocator.getRemoteObject("rssService");
			singletonInstance = this;
		}
		
		public static function getInstance():RssService {
			return singletonInstance != null ? singletonInstance : new RssService();
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		
		public function saveRssFeed(feed:RssFeed, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("saveRssFeed").send(feed);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function deleteRssFeed(feed:RssFeed, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("deleteRssFeed").send(feed);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listFeeds(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listFeeds").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function countItemsFromAllFeeds(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("countItemsFromAllFeeds").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function countItemsFromCategory(category:RssCategory, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("countItemsFromCategory").send(category);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function countItemsFromFeed(feed:RssFeed, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("countItemsFromFeed").send(feed);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listItemsFromAllFeeds(start:int, count:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listItemsFromAllFeeds").send(start, count);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, start));
		}
		
		public function listItemsFromCategory(category:RssCategory, start:int, count:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listItemsFromCategory").send(category, start, count);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, start));
		}
		
		public function listItemsFromFeed(feed:RssFeed, start:int, count:int, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listItemsFromFeed").send(feed, start, count);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, start));
		}
		
		public function findRssItemById(idEntity:Number, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("findRssItemById").send(idEntity);
			token.addResponder(getResponder(resultHandler));
		}
	}
}
