package tv.snews.anews.service {

	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Chat;
	import tv.snews.anews.domain.User;

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class ChatService extends ServiceBase {

		private static var singletonInstance:ChatService;

		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();

		private var remoteObject:RemoteObject;

		//------------------------------
		//	Singleton Constructor
		//------------------------------

		public function ChatService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			remoteObject = remoteLocator.getRemoteObject("chatService");
			singletonInstance = this;
		}

		public static function getInstance():ChatService {
			return singletonInstance != null ? singletonInstance : new ChatService();
		}

		//------------------------------
		//	Service Methods
		//------------------------------

		public function addMembers(users:ArrayCollection, chat:Chat, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("addMembers").send(chat, users);
			token.addResponder(getResponder(resultHandler));
		}

		public function registerLeave(chat:Chat, user:User):void {
			var token:AsyncToken = remoteObject.getOperation("registerLeave").send(chat, user);
			token.addResponder(getResponder());
		}

		public function createChat(members:ArrayList, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("createChat").send(members);
			token.addResponder(getResponder(resultHandler));
		}

		public function loadChat(id:Number, resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("loadChat").send(id);
			token.addResponder(getResponder(resultHandler));
		}

		public function listAll(owner:Number, result:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAll").send(owner);
			token.addResponder(getResponder(result, fault));
		}

		public function listByDate(date:Date, owner:Number, result:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listByDate").send(date, owner);
			token.addResponder(getResponder(result, fault));
		}
	}
}
