package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Institution;
	import tv.snews.anews.domain.Segment;

	/**
	 * Implementação para acesso ao serviço de manipulação dos Institutos.
	 *
	 * @author Paulo Felipe.
	 *
	 * @since 1.0.0
	 */
	public class InstitutionService extends ServiceBase {

		private static var instance:InstitutionService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();

		private var consumer:Consumer;
		private var remoteObject:RemoteObject;

		public function InstitutionService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remoteObject = remoteService.getRemoteObject("institutionService");
			consumer = messagingLocator.getConsumer("institutionDestination");
			instance = this;
		}

		public static function getInstance():InstitutionService {
			return instance != null ? instance : new InstitutionService();
		}

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		public function save(institution:Institution, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("save").send(institution);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function remove(id:int, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("delete").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function listAll(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler));
		}

		public function listBySegmentId(segment:Segment, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObject.getOperation("listBySegment").send(segment);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

	}
}
