package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;

	/**
	 * Façada para os serviços dos blocos no servidor. Permite
	 * o cliente fazer chamadas remotas aos serviços do roteiro.
	 *
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	public class ScriptBlockService extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function ScriptBlockService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			consumer = messageLocator.getConsumer("scriptBlockDestination");
			remote = remoteLocator.getRemoteObject("scriptBlockService");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):ScriptBlockService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new ScriptBlockService(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------

        public function synchronizeStandByBlock(currentDate:Date, targetDate:Date, programId:Number, resultHandler:Function, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("synchronizeStandByBlock").send(currentDate, targetDate, programId);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

		public function create(planId:Number, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("create").send(planId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(blockId:Number, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("remove").send(blockId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function updateField(blocktId:Number, field:String, value:Object):void {
			var token:AsyncToken = remote.getOperation("updateField").send(blocktId, field, value);
			token.addResponder(getResponder());
		}
		
		public function updatePosition(blockId:int, order:int, notifyMove:Boolean, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("updatePosition").send(blockId, order, notifyMove);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

	}
}