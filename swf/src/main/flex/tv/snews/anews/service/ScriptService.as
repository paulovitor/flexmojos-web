package tv.snews.anews.service {

import mx.collections.ArrayCollection;
import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptVideo;

/**
	 * Façada para os serviços do script no servidor.
	 *
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	public class ScriptService extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function ScriptService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			remote = remoteLocator.getRemoteObject("scriptService");
			consumer = messageLocator.getConsumer("scriptDestination");
			
			consumer.subscribe();
	    }

		public static function getInstance(serverName:String=null, serverPort:int=0):ScriptService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new ScriptService(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Message Methods
		//------------------------------

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------

        public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("loadById").send(id);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }
		
		public function createScript(scriptId:Number, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("create").send(scriptId);
			token.addResponder(getResponder(resultHandler));
		}

        public function removeScript(scriptId:Number, resultHandler:Function):void {
            var token:AsyncToken = remote.getOperation("removeScript").send(scriptId);
            token.addResponder(getResponder(resultHandler));
        }

        public function updateField(scriptId:Number, field:String, value:Object, resultHandler:Function=null, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("updateField").send(scriptId, field, value);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function copyTo(scriptIds:Array, date:Date, programId:int, shouldMove:Boolean, resultHandler:Function):void {
            var token:AsyncToken = remote.getOperation("copyTo").send(scriptIds, date, programId, shouldMove);
            token.addResponder(getResponder(resultHandler));
        }

        public function sendToDrawer(scriptId:Number, resultHandler:Function):void {
            var token:AsyncToken = remote.getOperation("sendToDrawer").send(scriptId);
            token.addResponder(getResponder(resultHandler));
        }
		
		public function updatePosition(scriptId:int, blockId:int, order:int, dragNDrop:Boolean = true, resultHandler:Function=null, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("updatePosition").send(scriptId, blockId, order, dragNDrop);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

        public function updatePositions(scriptsIds:Array, blockId:int, dropIndex:int, resultHandler:Function=null, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("updatePositions").send(scriptsIds, blockId, dropIndex);
            token.addResponder(getResponder(resultHandler, faultHandler, null, false));
        }

		public function merge(script:Script, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("merge").send(script);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

        public function updateVideos(script:Script, resultHandler:Function, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("updateVideos").send(script);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }
		
		public function loadChangesHistory(scriptId:Number, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("loadChangesHistory").send(scriptId);
			token.addResponder(getResponder(resultHandler));
		}

        public function restoreExcludedScript(scriptId:Number, date:Date, programId:int, resultHandler:Function):void {
            var token:AsyncToken = remote.getOperation("restoreExcludedScript").send(scriptId, date, programId);
            token.addResponder(getResponder(resultHandler));
        }

        public function listExcludedScripts(page:int, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("listExcludedScripts").send(page);
            token.addResponder(getResponder(resultHandler));
        }
		
		public function isLockedForEdition(scriptId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isLockedForEdition").send(scriptId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function lockEdition(scriptId:Number, userId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(scriptId, userId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function unlockEdition(scriptId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(scriptId);
			token.addResponder(getResponder(resultHandler));
		}

        public function createScriptFromReportage(blockId:Number, index:int, reportageId:int, resultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("createScriptFromReportage").send(blockId, index, reportageId);
            token.addResponder(getResponder(resultHandler));
        }

        public function createScriptFromGuideline(blockId:Number, index:int, guidelineId:int, resultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("createScriptFromGuideline").send(blockId, index, guidelineId);
            token.addResponder(getResponder(resultHandler));
        }

        public function copyDrawerScriptToBlock(storyId:Number, blockId:Number, index:int = -1, resultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("copyDrawerScriptToBlock").send(storyId, blockId, index);
            token.addResponder(getResponder(resultHandler));
        }

        public function loadImg(pathImg:String, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("loadImg").send(pathImg);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function removeImg(pathImg:String, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("removeImg").send(pathImg);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function loadVideosByScriptId(id:Number, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("loadVideosByScriptId").send(id);
            token.addResponder(getResponder(resultHandler));
        }

        public function save(script:Script, resultHandler:Function, faultHandler:Function=null):void {
            script.revisions = null; // resolve problema da conversão do ArrayCollection p/ SortedSet
            var token:AsyncToken = remote.getOperation("save").send(script);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function createScriptVideo(scriptId:Number, scriptMosVideo:ScriptVideo, resultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("createScriptVideo").send(scriptId, scriptMosVideo);
            token.addResponder(getResponder(resultHandler));
        }
		
		public function removeFromDrawer(reportageId:Number, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("removeFromDrawer").send(reportageId);
			token.addResponder(getResponder(resultHandler));
		}

        public function loadAndUpdateScriptVideo(scriptId:Number, scriptVideo:ScriptVideo, resultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("loadAndUpdateScriptVideo").send(scriptId, scriptVideo);
            token.addResponder(getResponder(resultHandler));
        }

        public function sendScriptsToClipboard(action:String, scriptsIds:Array, resultHandler:Function=null, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("sendScriptsToClipboard").send(action, scriptsIds);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function pastScriptsFromClipboard(blockId:Number, dropIndex:int, resultHandler:Function=null, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("pastScriptsFromClipboard").send(blockId, dropIndex);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function clearClipboard(resultHandler:Function=null, faultHandler:Function=null):void {
            var token:AsyncToken = remote.getOperation("clearClipboard").send();
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function removeScripts(idsOfScripts:Array, idScriptPlan:Number, resultHandler:Function = null, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("removeScripts").send(idsOfScripts, idScriptPlan);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }

        public function assumeEdition(scriptId:Number, userId:Number, resultHandler:Function, faultHandler:Function = null):void {
            var token:AsyncToken = remote.getOperation("assumeEdition").send(scriptId, userId);
            token.addResponder(getResponder(resultHandler, faultHandler));
        }
}
}
