package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Report;
	import tv.snews.anews.search.ReportParams;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	public class ReportService extends ServiceBase {
		
		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		//------------------------------------
		//  Singleton Constructor
		//------------------------------------
		
		public function ReportService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			remote = remoteLocator.getRemoteObject("reportService");
			consumer = messageLocator.getConsumer("reportDestination");
		}
		
		public static function getInstance(serverName:String=null, serverPort:int=0):ReportService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new ReportService(serverName, serverPort);
			}
			return instances[key];
		}
		
		//------------------------------------
		//	Message Methods
		//------------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler, false, 0, true);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		//------------------------------------
		//	Service Methods
		//------------------------------------
		
		public function findByParams(params:ReportParams, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("findByParams").send(params);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function findById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("findById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function save(report:Report, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("save").send(report);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function deleteById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("deleteById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function checkEditionLock(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("checkEditionLock").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function lockEdition(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function unlockEdition(id:Number, resultHandler:Function = null, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}