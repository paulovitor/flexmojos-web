package tv.snews.anews.service {
	
	import mx.messaging.ChannelSet;
	import mx.messaging.channels.AMFChannel;
	import mx.rpc.remoting.RemoteObject;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class RemoteObjectLocator {
		
		private static const instances:Object = {};
		
		private var channelSet:ChannelSet = new ChannelSet();
		
		public function RemoteObjectLocator(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			var amfChannel:AMFChannel = null;
			
			if (serverName == null && serverPort == 0) {
				amfChannel = new AMFChannel("my-amf", "http://{server.name}:{server.port}/flexmojos-web/messagebroker/amf");
			} else {
				amfChannel = new AMFChannel("my-amf", "http://" + serverName + ":" + serverPort + "/flexmojos-web/messagebroker/amf");
			}
			
			channelSet.addChannel(amfChannel);
		}
		
		public static function getInstance(serverName:String=null, serverPort:int=0):RemoteObjectLocator {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new RemoteObjectLocator(serverName, serverPort);
			}
			return instances[key];
		}
		
		public function getRemoteObject(destination:String):RemoteObject {
			var ro:RemoteObject = new RemoteObject(destination);
			ro.channelSet = channelSet;
			return ro;
		}
		
		public function disconnectAll():void {
			channelSet.disconnectAll();
		}
		
		private static function instanceKey(... args):String {
			return args.join("_");
		}
	}
}