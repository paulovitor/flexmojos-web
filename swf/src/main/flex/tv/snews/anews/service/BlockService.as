package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;

	/**
	 * Façada para os serviços do block no servidor.
	 *
	 * @author Paulo Felipe
	 * @since 1.0.0
	 */
	public class BlockService extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function BlockService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			consumer = messageLocator.getConsumer("blockDestination");
			remote = remoteLocator.getRemoteObject("blockService");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):BlockService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new BlockService(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

		//------------------------------
		//	Service Methods
		//------------------------------
		public function createBlock(rundownId:Number, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("createBlock").send(rundownId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function removeBlock(blockId:Number, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("removeBlock").send(blockId);
			token.addResponder(getResponder(resultHandler));
		}

		public function updateBlockTime(blockId:Number, date:Date, resultHandler:Function = null, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("updateBlockTime").send(blockId, date);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}