/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.service {

import mx.collections.ArrayCollection;
import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.search.MosMediaParams;
import tv.snews.anews.search.WSMediaParams;

/**
 * Classe de serviço que faz as invocações dos métodos remotos da classe do
 * servidor MediaCenterService.
 *
 * @author Samuel Guedes de Melo.
 * @since 1.2.5
 */
public class MediaCenterService extends ServiceBase {

    private static var singletonInstance:MediaCenterService;

    private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
    private var messagingLocator:MessageLocator = MessageLocator.getInstance();

    private var remoteObject:RemoteObject;
    private var consumer:Consumer;

    //------------------------------
    //	Singleton Constructor
    //------------------------------

    /**
     * Construtor padrão. Deve apenas ser usado internamente para a
     * construção da instância única do singleton desta classe.
     */
    public function MediaCenterService() {
        if (singletonInstance != null) {
            throw new Error("Singleton exception");
        }
        remoteObject = remoteLocator.getRemoteObject("mediaCenterService");

        consumer = messagingLocator.getConsumer("mediaCenterDestination");
        singletonInstance = this;
    }

    /**
     * Retorna a instância 'singleton' desta classe.
     */
    public static function getInstance():MediaCenterService {
        return singletonInstance != null ? singletonInstance : new MediaCenterService();
    }

    //------------------------------
    //	Message Methods
    //------------------------------

    public function subscribe(resultHandler:Function):void {
        consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
        consumer.subscribe();
    }

    public function unsubscribe(resultHandler:Function):void {
        consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
    }

    //------------------------------
    //	Service Methods
    //------------------------------

    /**
     * Invoca o método remoto MediaCenterService.mosObjCreate().
     */
    public function mosObjCreate(mosMedia:MosMedia, resultHandler:Function, faultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("mosObjCreate").send(mosMedia);
        token.addResponder(getResponder(resultHandler));
    }

    /**
     * Invoca o método remoto MediaCenterService.loadById().
     */
    public function loadById(ids:ArrayCollection, faultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("loadById").send(ids);
        token.addResponder(getResponder(faultHandler));
    }

    /**
     * Invoca o método remoto MediaCenterService.mosReqSearchableSchema().
     */
    public function mosReqSearchableSchema(mosDevice:MosDevice, faultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("mosReqSearchableSchema").send(mosDevice);
        token.addResponder(getResponder(faultHandler));
    }

    /**
     * Invoca o método remoto MediaCenterService.mosReqAll().
     */
    public function mosReqAll(mosDevice:MosDevice, interval:Boolean, faultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("mosReqAll").send(mosDevice, interval);
        token.addResponder(getResponder(faultHandler));
    }

    /**
     * Invoca o método remoto MediaCenterService.mediaByFullTextSearch().
     */
    public function findByParams(params:MosMediaParams, resultHandler:Function, faultHandler:Function):void {
        var token:AsyncToken = remoteObject.getOperation("findByParams").send(params);
        token.addResponder(getResponder(resultHandler));
    }

    public function canCreateMosObj(objID:String, mosDevice:MosDevice, resultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("canCreateMosObj").send(objID, mosDevice);
        token.addResponder(getResponder(resultHandler));
    }

    public function findByWSParams(device:WSDevice, params:WSMediaParams, resultHandler:Function, faultHandler:Function):void {
        var token:AsyncToken = remoteObject.getOperation("findByWSParams").send(device, params);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function createWSVideo(wsMedia:WSMedia, resultHandler:Function = null, faultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("createWSVideo").send(wsMedia);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function loadWSMedia(wsMedia:WSMedia, resultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("loadWSMedia").send(wsMedia);
        token.addResponder(getResponder(resultHandler));
    }

    public function checkUseOfDevice(deviceId:Number, resultHandler:Function = null):void {
        var token:AsyncToken = remoteObject.getOperation("checkUseOfDevice").send(deviceId);
        token.addResponder(getResponder(resultHandler));
    }
}
}
