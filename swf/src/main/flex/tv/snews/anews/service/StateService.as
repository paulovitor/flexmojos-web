package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.StateCountry;
	
	/**
	 * Implementação para acesso ao serviço de manipulação dos estados. 
	 * 
	 * @author Samuel Guedes de Melo.
	 * 
	 * @since 1.0.0
	 */
	public class StateService extends ServiceBase {
		
		private static var instance:StateService;
		
		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messagingLocator:MessageLocator = MessageLocator.getInstance();
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		
		public function StateService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			remote = remoteService.getRemoteObject("stateService");
			instance = this;
		}
		
		public static function getInstance():StateService {
			return instance != null ? instance : new StateService();
		}
		
		public function listAll(resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken=remote.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function save(state:StateCountry, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken=remote.getOperation("save").send(state);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(state:StateCountry, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("remove").send(state);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}