package tv.snews.anews.service {

import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.messaging.events.MessageFaultEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.BlockTemplate;

import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownTemplate;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class RundownTemplateService extends ServiceBase {

    private static const instances:Object = {};

    private var remoteLocator:RemoteObjectLocator;
    private var messageLocator:MessageLocator;

    private var remote:RemoteObject;
    private var consumer:Consumer;

    public function RundownTemplateService(serverName:String = null, serverPort:int = 0) {
        var key:String = instanceKey(serverName, serverPort);
        if (instances[key] != null) {
            throw new Error("Singleton exception");
        }

        remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
        messageLocator = MessageLocator.getInstance(serverName, serverPort);

        remote = remoteLocator.getRemoteObject("rundownTemplateService");
        consumer = messageLocator.getConsumer("rundownTemplateDestination");
    }

    public static function getInstance(serverName:String = null, serverPort:int = 0):RundownTemplateService {
        var key:String = instanceKey(serverName, serverPort);
        if (instances[key] == null) {
            instances[key] = new RundownTemplateService(serverName, serverPort);
        }
        return instances[key];
    }

    //------------------------------
    //	Message Methods
    //------------------------------

    public function subscribe(resultHandler:Function):void {
        consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
		consumer.subscribe();
    }

    public function unsubscribe(resultHandler:Function):void {
        consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
    }

    //------------------------------
    //	Service Methods
    //------------------------------

    public function load(rundownId:int, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("load").send(rundownId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function findAllByProgram(program:Program, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("findAllByProgram").send(program);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function nameAvailable(name:String, program:Program, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("nameAvailable").send(name, program);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function createTemplate(name:String, program:Program, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("createTemplate").send(name, program);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function createCopy(originalId:int, name:String, program:Program, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("createCopy").send(originalId, name, program);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function updateTemplate(template:RundownTemplate, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("updateTemplate").send(template);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function removeTemplate(template:RundownTemplate, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("removeTemplate").send(template);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function createBlock(rundownId:int, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("createBlock").send(rundownId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function updateBlock(blockTemplate:BlockTemplate, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("updateBlock").send(blockTemplate);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function removeBlock(blockId:int, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("removeBlock").send(blockId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function lockEdition(template:RundownTemplate, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("lockEdition").send(template);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function unlockAllEditions(resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("unlockAllEditions").send();
        token.addResponder(getResponder(resultHandler, faultHandler));
    }
}
}
