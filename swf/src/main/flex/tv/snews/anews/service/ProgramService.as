package tv.snews.anews.service {
	
	import mx.collections.ArrayCollection;
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Program;
	
	/**
	 * Implementação para acesso ao serviço de manipulação de programas. 
	 * 
	 * @author Paulo Felipe
	 * @since 1.0.0
	 */
	public class ProgramService extends ServiceBase {
		
		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;
		
		private var consumer:Consumer;
		private var remoteObject:RemoteObject;
		
		public function ProgramService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			consumer = messageLocator.getConsumer("programDestination");
			remoteObject = remoteLocator.getRemoteObject("programService");
		}
		
		public static function getInstance(serverName:String=null, serverPort:int=0):ProgramService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new ProgramService(serverName, serverPort);
			}
			return instances[key];
		}
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

        public function loadById(id:int, faultHandler:Function = null):void {
            var token:AsyncToken = remoteObject.getOperation("loadById").send(id);
            token.addResponder(getResponder(faultHandler));
        }

		public function save(program:Program, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObject.getOperation("save").send(program);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function deleteProgram(id:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObject.getOperation("delete").send(id);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listAll(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listAllOrderByStart(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAllOrderByStart").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listAllWithDisableds(fetchTypes:Array,resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listAllWithDisableds").send(fetchTypes);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function listPresenters(programId:int, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("listPresenters").send(programId);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, programId));
		}
		
		public function listEditors(programId:int, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("listEditors").send(programId);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, programId));
		}

		public function listEditorsOfPrograms(resultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("listEditorsOfPrograms").send();
			token.addResponder(getResponder(resultHandler));
		}

        public function listImageEditors(programId:int, resultHandler:Function, faultHandler:Function=null):void {
            var token:AsyncToken = remoteObject.getOperation("listImageEditors").send(programId);
            token.addResponder(getAsyncResponder(resultHandler, faultHandler, programId));
        }
    }
}
