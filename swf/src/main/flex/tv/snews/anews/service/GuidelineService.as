package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.GuidelineParams;

/**
	 * @author Felipe Zap de Mello.
	 * @since 1.0.0
	 */
	public class GuidelineService extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var messageLocator:MessageLocator;

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function GuidelineService(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			messageLocator = MessageLocator.getInstance(serverName, serverPort);
			
			consumer = messageLocator.getConsumer("guidelineDestination");
			remote = remoteLocator.getRemoteObject("guidelineService");
		}

		public static function getInstance(serverName:String=null, serverPort:int=0):GuidelineService {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new GuidelineService(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Message Methods
		//------------------------------

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

		//------------------------------
		//	Service Methods
		//------------------------------
		public function sendMailToReporter(guidelineId:Number, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("sendGuidelineMail").send(guidelineId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

//		public function save(guideline:Guideline, resultHandler:Function, faultHandler:Function = null):void {
//			guideline.revisions = null; // resolve problema da conversão do ArrayCollection p/ SortedSet
//			var token:AsyncToken = remote.getOperation("save").send(guideline);
//			token.addResponder(getResponder(resultHandler, faultHandler));
//		}

	public function save(guideline:Guideline, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("save").send(guideline);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function updateState(guidelineId:Number, state:String, reason:String, resultHandler:Function, faultHandler:Function = null):void {
		var token:AsyncToken = remote.getOperation("updateState").send(guidelineId, state, reason);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

		public function copy(dateToCopy:Date, programIdTocopy:int, guidelineIdToCopy:Number, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("copy").send(dateToCopy, programIdTocopy, guidelineIdToCopy);
			token.addResponder(getResponder(resultHandler));
		}

		public function copyRemoteGuideline(remoteGuideline:Guideline, destinyDate:Date, destinyProgram:Program, producer:User, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("copyRemoteGuideline").send(remoteGuideline, destinyDate, destinyProgram, producer);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function remoteCopyRequest(id:Number, nickname:String, company:String, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("remoteCopyRequest").send(id, nickname, company);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(id:Number, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("remove").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function listAllGuidelineOfDay(date:Date, programId:Number, state:String = null, resultHandler:Function = null, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listAllGuidelineOfDay").send(date, programId, state);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function isLockedForEdition(guidelineId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isLockedForEdition").send(guidelineId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function lockEdition(guidelineId:Number, userId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(guidelineId, userId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function unlockEdition(guidelineId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(guidelineId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function restoreExcludedGuideline(guidelineId:Number, programToCopyId:int, guidelineToCopyDate:Date, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("restoreExcludedGuideline").send(guidelineId, programToCopyId, guidelineToCopyDate);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function countExcludedGuidelines(result:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("countExcludedGuidelines").send();
			token.addResponder(getResponder(result, faultHandler));
		}
		
		public function listExcludedGuidelines(firstResult:int, maxResults:int, result:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listExcludedGuidelines").send(firstResult, maxResults);
			token.addResponder(getAsyncResponder(result, faultHandler, firstResult));
		}
		
		public function loadChangesHistory(guidelineId:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadChangesHistory").send(guidelineId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function loadRevision(guidelineId:Number, revisionNumber:int, showDiffs:Boolean, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadRevision").send(guidelineId, revisionNumber, showDiffs);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function listExcludedGuidelinesPage(pageNumber:int ,result:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listExcludedGuidelinesPage").send(pageNumber);
			token.addResponder(getResponder(result));
		}

		public function fullTextSearch(searchParams:GuidelineParams, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("fullTextSearch").send(searchParams);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function removeFromDrawer(guidelineId:Number, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("removeFromDrawer").send(guidelineId);
			token.addResponder(getResponder(resultHandler));
		}
	}
}
