package tv.snews.anews.service {

import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.messaging.events.MessageFaultEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.BlockTemplate;
import tv.snews.anews.domain.StoryTemplate;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.6
 */
public class StoryTemplateService extends ServiceBase {

    private static const instances:Object = {};

    private var remoteLocator:RemoteObjectLocator;
    private var messageLocator:MessageLocator;

    private var remote:RemoteObject;
    private var consumer:Consumer;

    public function StoryTemplateService(serverName:String=null, serverPort:int=0) {
        var key:String = instanceKey(serverName, serverPort);
        if (instances[key] != null) {
            throw new Error("Singleton exception");
        }

        remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
        messageLocator = MessageLocator.getInstance(serverName, serverPort);

        remote = remoteLocator.getRemoteObject("storyTemplateService");
        consumer = messageLocator.getConsumer("storyTemplateDestination");
    }

    public static function getInstance(serverName:String=null, serverPort:int=0):StoryTemplateService {
        var key:String = instanceKey(serverName, serverPort);
        if (instances[key] == null) {
            instances[key] = new StoryTemplateService(serverName, serverPort);
        }
        return instances[key];
    }

    //------------------------------
    //	Message Methods
    //------------------------------

    public function subscribe(resultHandler:Function):void {
        consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
		consumer.subscribe();
    }

    public function unsubscribe(resultHandler:Function):void {
        consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
    }

    //------------------------------
    //	Service Methods
    //------------------------------

    public function load(storyId:int, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("load").send(storyId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function createStory(blockId:int, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("createStory").send(blockId);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function updateStory(story:StoryTemplate, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("updateStory").send(story);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function updateField(storyId:int, field:String, value:Object, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("updateField").send(storyId, field, value);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function updatePosition(storyId:int, blockId:int, order:int, resultHandler:Function=null, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("updatePosition").send(storyId, blockId, order);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

    public function removeStory(storyId:int, resultHandler:Function=null, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("removeStory").send(storyId);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}
}

}
