package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.StoryKind;

	/**
	 * Implementação para acesso ao serviço de manipulação dos tipos da lauda.
	 *
	 * @author Samuel Guedes de Melo.
	 *
	 * @since 1.2.5
	 */
	public class StoryKindService extends ServiceBase {
		private static var instance:StoryKindService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messageLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function StoryKindService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messageLocator.getConsumer("storyKindDestination");
			remote = remoteService.getRemoteObject("storyKindService");
			instance = this;
		}

		public static function getInstance():StoryKindService {
			return instance != null ? instance : new StoryKindService();
		}

		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		public function save(storyKind:StoryKind, result:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(storyKind);
			token.addResponder(getResponder(result));
		}

		public function del(storyKindId:Number, result:Function):void {
			var token:AsyncToken = remote.getOperation("delete").send(storyKindId);
			token.addResponder(getResponder(result));
		}
		
		public function listAll(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAll").send();
			token.addResponder(getResponder(result));
		}

		public function listAllOrderAcronym(result:Function):void {
			var token:AsyncToken = remote.getOperation("listAllOrderAcronym").send();
			token.addResponder(getResponder(result));
		}
		
		public function verifyDeleteKind(id:Number, result:Function):void {
			var token:AsyncToken = remote.getOperation("verifyDeleteKind").send(id);
			token.addResponder(getResponder(result));
		}

	}
}
