package tv.snews.anews.service {
	
	import mx.messaging.ChannelSet;
	import mx.messaging.Consumer;
	import mx.messaging.channels.AMFChannel;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MessageLocator {
		
		private static const instances:Object = {};
		
		private var channelSet:ChannelSet = new ChannelSet();
		
		public function MessageLocator(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}
			
			var longAmf:AMFChannel, pollAmf:AMFChannel;
			if (serverName == null && serverPort == 0) {
				longAmf = new AMFChannel("my-longpolling-amf", "http://{server.name}:{server.port}/flexmojos-web/messagebroker/amflongpolling");
			} else {
				longAmf = new AMFChannel("my-longpolling-amf", "http://" + serverName + ":" + serverPort + "/flexmojos-web/messagebroker/amflongpolling");
			}
			
			channelSet.addChannel(longAmf);
			channelSet.addChannel(pollAmf);
		}
		
		public static function getInstance(serverName:String = null, serverPort:int=0):MessageLocator {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new MessageLocator(serverName, serverPort);
			}
			return instances[key];
		}
		
		public function getConsumer(destination:String):Consumer {
			var consumer:Consumer = new Consumer();
			consumer.channelSet = channelSet;
			consumer.destination = destination;
			return consumer;
		}
		
		public function disconnectAll():void {
			channelSet.disconnectAll();
		}
		
		private static function instanceKey(...args):String {
			return args.join("_");
		}
	}
}