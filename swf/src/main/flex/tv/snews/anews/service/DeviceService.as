package tv.snews.anews.service {

import mx.messaging.Consumer;
import mx.messaging.events.MessageEvent;
import mx.messaging.events.MessageFaultEvent;
import mx.rpc.AsyncToken;
import mx.rpc.remoting.RemoteObject;

import tv.snews.anews.domain.Device;

import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.IIDevicePath;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.search.GuidelineParams;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class DeviceService extends ServiceBase {

	private static const instances:Object = {};

	private var remoteLocator:RemoteObjectLocator;
	private var messageLocator:MessageLocator;

	private var remote:RemoteObject;
	private var consumer:Consumer;

	//------------------------------
	//	Constructor
	//------------------------------

	public function DeviceService(serverName:String = null, serverPort:int = 0) {
		var key:String = instanceKey(serverName, serverPort);
		if (instances[key] != null) {
			throw new Error("Singleton exception");
		}

		remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
		messageLocator = MessageLocator.getInstance(serverName, serverPort);

		consumer = messageLocator.getConsumer("deviceDestination");
		remote = remoteLocator.getRemoteObject("deviceService");
	}

	public static function getInstance(serverName:String = null, serverPort:int = 0):DeviceService {
		var key:String = instanceKey(serverName, serverPort);
		if (instances[key] == null) {
			instances[key] = new DeviceService(serverName, serverPort);
		}
		return instances[key];
	}

	//------------------------------
	//	Message Methods
	//------------------------------

	public function subscribe(resultHandler:Function):void {
		consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
		consumer.subscribe();
	}

	public function unsubscribe(resultHandler:Function):void {
		consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
	}

	//------------------------------
	//	Service Methods
	//------------------------------

	public function findById(id:int, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("findById").send(id);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findAll(resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("findAll").send();
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findAllByProtocol(protocol:String, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("findAllByProtocol").send(protocol);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findAllByType(type:String, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("findAllByType").send(type);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findAllMosDeviceByType(type:String, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("findAllMosDeviceByType").send(type);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function save(device:Device, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("save").send(device);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

    public function checkChannelsInUse(device:Device, resultHandler:Function, faultHandler:Function=null):void {
        var token:AsyncToken = remote.getOperation("checkChannelsInUse").send(device);
        token.addResponder(getResponder(resultHandler, faultHandler));
    }

	public function remove(device:Device, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("remove").send(device);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function checkPathInUse(path:IIDevicePath, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("checkPathInUse").send(path);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}
}
}
