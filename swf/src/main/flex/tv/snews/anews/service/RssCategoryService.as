package tv.snews.anews.service {
	
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.RssCategory;

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class RssCategoryService extends ServiceBase {
		
		private static var singletonInstance:RssCategoryService;

		public static const FEED_INSERTED_TYPE:String = "feedInserted";
		public static const FEED_UPDATED_TYPE:String = "feedUpdated";
		public static const FEED_DELETED_TYPE:String = "feedDeleted";
		
		private var remoteLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var remoteObject:RemoteObject;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		public function RssCategoryService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			remoteObject = remoteLocator.getRemoteObject("rssCategoryService");
			singletonInstance = this;
		}
		
		public static function getInstance():RssCategoryService {
			return singletonInstance != null ? singletonInstance : new RssCategoryService();
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function insert(category:RssCategory, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("insert").send(category);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function update(category:RssCategory, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("update").send(category);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(category:RssCategory, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remoteObject.getOperation("remove").send(category);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function listAll(resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remoteObject.getOperation("listAll").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
