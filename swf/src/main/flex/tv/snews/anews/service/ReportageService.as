package tv.snews.anews.service {
	
	import flash.system.System;
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Guideline;
	import tv.snews.anews.domain.Program;
	import tv.snews.anews.domain.Reportage;
	import tv.snews.anews.domain.User;
import tv.snews.anews.search.ReportageParams;

/**
	 * Façada para os serviços de reportagem do servidor. Permite
	 * o o cliente fazer chamadas remotas aos serviços de reportagem.
	 *
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class ReportageService extends ServiceBase {

        private static const instances:Object = {};

        private var remoteLocator:RemoteObjectLocator;
        private var messageLocator:MessageLocator;
		
		private var remote:RemoteObject;
		private var consumer:Consumer;
		
		public function ReportageService(serverName:String=null, serverPort:int=0) {
            var key:String = instanceKey(serverName, serverPort);
            if (instances[key] != null) {
                throw new Error("Singleton exception");
            }

            remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
            messageLocator = MessageLocator.getInstance(serverName, serverPort);

			consumer = messageLocator.getConsumer("reportageDestination");
			remote = remoteLocator.getRemoteObject("reportageService");
		}
		
		public static function getInstance(serverName:String=null, serverPort:int=0):ReportageService {
            var key:String = instanceKey(serverName, serverPort);
            if (instances[key] == null) {
                instances[key] = new ReportageService(serverName, serverPort);
            }
            return instances[key];
		}
		
		//------------------------------
		//	Message Methods
		//------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
			System.gc();
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		public function save(reportage:Reportage, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("save").send(reportage);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function loadById(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadById").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function remoteCopyRequest(id:Number, nickname:String, company:String, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("remoteCopyRequest").send(id, nickname, company);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function remove(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("remove").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function copyReportage(reportageId:Number, date:Date, program:Program, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("copyReportage").send(reportageId, date, program);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function copyRemoteReportage(remoteReportage:Reportage, destinyDate:Date, destinyProgram:Program, reporter:User, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("copyRemoteReportage").send(remoteReportage, destinyDate, destinyProgram, reporter);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function changeOk(id:Number, status:Boolean, resultHandler:Function = null, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("changeOk").send(id, status);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function listAll(params:ReportageParams, result:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listAll").send(params);
			token.addResponder(getResponder(result, faultHandler));
		}
		
		public function listAllReportageOfDay(date:Date, program:Program, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listAllReportageOfDay").send(date, program);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}

		public function listExcludedReportagesPage(pageNumber:int,  result:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("listExcludedReportagesPage").send(pageNumber);
			token.addResponder(getResponder(result));
		}
		
		public function reportageByFullTextSearch(searchParams:Object, result:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("reportageByFullTextSearch").send(searchParams);
			token.addResponder(getResponder(result));
		}
		
		public function loadChangesHistory(reportageId:Number, result:Function):void {
			var token:AsyncToken = remote.getOperation("loadChangesHistory").send(reportageId);
			token.addResponder(getResponder(result));
		}
		
		public function loadRevision(reportageId:Number, revisionNumber:int, showDiffs:Boolean, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("loadRevision").send(reportageId, revisionNumber, showDiffs);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function restoreExcluded(id:Number, date:Date, programId:int, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("restoreExcluded").send(id, date, programId);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function isLockedForEdition(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("isLockedForEdition").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function lockEdition(id:Number, user:User, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("lockEdition").send(id, user);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function unlockEdition(id:Number, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("unlockEdition").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function sendMailToReporter(id:Number, resultHandler:Function, faultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("sendReportageMail").send(id);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function removeFromDrawer(reportageId:Number, resultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("removeFromDrawer").send(reportageId);
			token.addResponder(getResponder(resultHandler));
		}
		
		public function updateState(reportageId:Number, state:String, reason:String, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remote.getOperation("updateState").send(reportageId, state, reason);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
