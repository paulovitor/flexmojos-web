package tv.snews.anews.service {
	
	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.Settings;
	import tv.snews.anews.infra.mail.MailConfig;

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class SettingsService extends ServiceBase {
		
		private static var singletonInstance:SettingsService;

		private var messageLocator:MessageLocator = MessageLocator.getInstance();
		private var consumer:Consumer;
		
		private var objectLocator:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var remoteObj:RemoteObject;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		public function SettingsService() {
			if (singletonInstance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messageLocator.getConsumer("settingsDestination");
			remoteObj = objectLocator.getRemoteObject("settingsService");
			singletonInstance = this;
		}
		
		public static function getInstance():SettingsService {
			return singletonInstance != null ? singletonInstance : new SettingsService();
		}
		
		//------------------------------
		//	Service Methods
		//------------------------------
		
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}
		
		public function loadSettings(resultHandler:Function):void {
			var token:AsyncToken = remoteObj.getOperation("loadSettings").send();
			token.addResponder(getResponder(resultHandler));
		}

		public function reindexDatabase(resultHandler:Function):void {
			var token:AsyncToken = remoteObj.getOperation("reindexDatabase").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function loadCurrentLocale(resultHandler:Function):void {
			var token:AsyncToken = remoteObj.getOperation("loadCurrentLocale").send();
			token.addResponder(getResponder(resultHandler));
		}
		
		public function updateSettings(settings:Settings, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObj.getOperation("updateSettings").send(settings);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function loadMailConfig(resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObj.getOperation("loadMailConfig").send();
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function saveMailConfig(config:MailConfig, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObj.getOperation("saveMailConfig").send(config);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function loadImg(pathImg:String, resultHandler:Function, faultHandler:Function = null):void {
			var token:AsyncToken = remoteObj.getOperation("loadImg").send(pathImg);
			token.addResponder(getResponder(resultHandler, faultHandler));
		}
	}
}
