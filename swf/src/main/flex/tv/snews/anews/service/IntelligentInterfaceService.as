package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;

	/**
	 * Façada para os serviços do intelligentInterfaceService no servidor.
	 *
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	public class IntelligentInterfaceService extends ServiceBase {

		private static var instance:IntelligentInterfaceService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messageLocator:MessageLocator = MessageLocator.getInstance();

		private var consumer:Consumer;

		public function IntelligentInterfaceService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messageLocator.getConsumer("intelligentInterfaceDestination");
			instance = this;
		}

		public static function getInstance():IntelligentInterfaceService {
			return instance != null ? instance : new IntelligentInterfaceService();
		}

		//------------------------------
		//	Message Methods
		//------------------------------
		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}

		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

	}
}