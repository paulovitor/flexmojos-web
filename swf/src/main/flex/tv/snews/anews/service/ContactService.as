package tv.snews.anews.service {

	import mx.messaging.Consumer;
	import mx.messaging.events.MessageEvent;
	import mx.messaging.events.MessageFaultEvent;
	import mx.rpc.AsyncToken;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.AgendaContact;
	import tv.snews.anews.domain.Contact;

	/**
	 * Implementação para acesso ao serviço de manipulação dos Usuários.
	 *
	 * @author Paulo Felipe.
	 * @author Eliezer Reis
	 *
	 * @since 1.0.0
	 */
	public class ContactService extends ServiceBase {
		private static var instance:ContactService;

		private var remoteService:RemoteObjectLocator = RemoteObjectLocator.getInstance();
		private var messageLocator:MessageLocator = MessageLocator.getInstance();

		private var remote:RemoteObject;
		private var consumer:Consumer;

		public function ContactService() {
			if (instance != null) {
				throw new Error("Singleton exception");
			}
			consumer = messageLocator.getConsumer("contactDestination");
			remote = remoteService.getRemoteObject("contactService");
			instance = this;
		}

		public static function getInstance():ContactService {
			return instance != null ? instance : new ContactService();
		}

		public function subscribe(resultHandler:Function):void {
			consumer.addEventListener(MessageEvent.MESSAGE, resultHandler);
			consumer.subscribe();
		}
		
		public function unsubscribe(resultHandler:Function):void {
			consumer.removeEventListener(MessageEvent.MESSAGE, resultHandler);
		}

	public function countAll(resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("countAll").send();
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

	public function findAll(firstResult:int, maxResult:int, resultHandler:Function, faultHandler:Function):void {
		var token:AsyncToken = remote.getOperation("findAll").send(firstResult, maxResult);
		token.addResponder(getAsyncResponder(resultHandler, faultHandler, firstResult));
	}

	public function countByName(name:String, resultHandler:Function, faultHandler:Function=null):void {
		var token:AsyncToken = remote.getOperation("countByName").send(name);
		token.addResponder(getResponder(resultHandler, faultHandler));
	}

		public function findByName(name:String, firstResult:int, maxResult:int, resultHandler:Function, faultHandler:Function=null):void {
			var token:AsyncToken = remote.getOperation("findByName").send(name, firstResult, maxResult);
			token.addResponder(getAsyncResponder(resultHandler, faultHandler, firstResult));
		}

		public function save(contact:Contact, result:Function):void {
			var token:AsyncToken = remote.getOperation("save").send(contact);
			token.addResponder(getResponder(result));
		}

		public function del(contact:Contact, result:Function):void {
			var token:AsyncToken = remote.getOperation("delete").send(contact);
			token.addResponder(getResponder(result));
		}
		
		public function findAgendaContactById(idEntity:int, resultHandler:Function):void {
			var token:AsyncToken = remote.getOperation("findAgendaContactById").send(idEntity);
			token.addResponder(getResponder(resultHandler));
		}

	}
}
