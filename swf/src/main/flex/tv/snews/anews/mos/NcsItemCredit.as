package tv.snews.anews.mos {
import mx.collections.ArrayCollection;

import tv.snews.anews.domain.Device;
import tv.snews.anews.domain.AbstractEntity_Number;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.ScriptMosCredit;
import tv.snews.anews.domain.StoryMosCredit;
import tv.snews.anews.utils.DomainCache;

/**
 * @author Samuel Guedes de Melo
 * @author Paulo Vitor
 * @since 1.7
 */
[Bindable]
public class NcsItemCredit extends AbstractEntity_Number {

    public var slug:String;
    public var channel:String;
    public var mosMedia:MosMedia;

    public function NcsItemCredit(xml:XMLList = null) {
        super(this);
        if (xml) {
            var mosObj:MosMedia = new MosMedia();
            for each (var device:Device in DomainCache.cgDevices) {
                if (device is MosDevice) {
                    var mosDevice:MosDevice = device as MosDevice;
                    if (mosDevice.mosId == xml.mosID) {
                        mosObj.device = mosDevice;
                    }
                }
            }
            mosObj.objectId = xml.objID;
            mosObj.slug = xml.objSlug;
            mosObj.duration = xml.objDur;
            mosObj.timeBase = xml.objTB;
            mosObj.abstractText = xml.mosAbstract;
            mosObj.type = xml.objType;
            mosObj.mosExternalMetadata = xml.mosExternalMetadata.toString();

            this.mosMedia = mosObj;
            this.id = new Number(xml.itemID);
            this.slug = xml.itemSlug;
            this.channel = xml.itemChannel;

        }
    }

    public function toXml():XML {
        return <item>
            <itemID>{this.id || ''}</itemID>
            <itemSlug>{slug}</itemSlug>
            <objID>{mosMedia.objectId}</objID>
            <mosID>{(this.mosMedia.device as MosDevice).mosId}</mosID>
            <objSlug>{mosMedia.slug}</objSlug>
            <objDur>{mosMedia.duration}</objDur>
            <objTB>{mosMedia.timeBase}</objTB>
            <mosAbstract>{mosMedia.abstractText}</mosAbstract>
            <itemChannel>{channel}</itemChannel>
            <objType>{mosMedia.type || ''}</objType>
			{new XML(mosMedia.mosExternalMetadata)}
        </item>;
    }

    public function parseNcsItemCreditFromStoryMosCredit(storyMosCredit:StoryMosCredit):void {
        this.id = storyMosCredit.id;
        this.slug = storyMosCredit.slug;
        this.channel = storyMosCredit.channel;
        this.mosMedia = storyMosCredit.mosMedia;
    }

    public function parseNcsItemCreditFromScriptMosCredit(scriptMosCredit:ScriptMosCredit):void {
        this.id = scriptMosCredit.id;
        this.slug = scriptMosCredit.slug;
        this.channel = scriptMosCredit.channel;
        this.mosMedia = scriptMosCredit.mosMedia;
    }
}
}
