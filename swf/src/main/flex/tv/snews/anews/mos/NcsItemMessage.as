package tv.snews.anews.mos {
/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class NcsItemMessage implements MosMessage {

    public var credit:NcsItemCredit;

    public function NcsItemMessage(ncsItemCredit:NcsItemCredit) {
        this.credit = ncsItemCredit;
    }

    public static function parseXml(xmlStr:String):NcsItemMessage {
        var xml:XML = new XML(xmlStr);

        if (!xml.ncsItem) {
            throw new Error("This is not an ncsItem message: " + xmlStr);
        }

        var itemXml:XMLList = xml.ncsItem.item;
        var mosCredit:NcsItemCredit = new NcsItemCredit(itemXml);

        var ncsItemMsg:NcsItemMessage = new NcsItemMessage(mosCredit);
        ncsItemMsg.originalXml = xmlStr;

        return ncsItemMsg;
    }

    public function toXml():XML {
        return <mos>
            <ncsItem>
					{credit.toXml()}
            </ncsItem>
        </mos>;
    }

    public function toXmlString():String {
        return toXml().toXMLString();
    }

    //------------------------------
    //	Properties
    //------------------------------

    private var _originalXml:String;

    public function get originalXml():String {
        return _originalXml;
    }

    public function set originalXml(value:String):void {
        _originalXml = value;
    }
}
}
