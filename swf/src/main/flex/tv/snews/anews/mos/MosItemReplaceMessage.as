package tv.snews.anews.mos {
/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class MosItemReplaceMessage implements MosMessage {

    public var credit:NcsItemCredit;

    public function MosItemReplaceMessage(ncsItemCredit:NcsItemCredit) {
        this.credit = ncsItemCredit;
    }

    public static function parseXml(xmlStr:String):MosItemReplaceMessage {
        var xml:XML = new XML(xmlStr);

        if (!xml.mosItemReplace) {
            throw new Error("This is not an mosItemReplace message: " + xmlStr);
        }

        var itemXml:XMLList = xml.mosItemReplace.item;
        var mosCredit:NcsItemCredit = new NcsItemCredit(itemXml);

        var mosItemReplace:MosItemReplaceMessage = new MosItemReplaceMessage(mosCredit);
        mosItemReplace.originalXml = xmlStr;

        return mosItemReplace;
    }

    public function toXml():XML {
        return <mos>
            <mosItemReplace>
					{credit.toXml()}
            </mosItemReplace>
        </mos>;
    }

    public function toXmlString():String {
        return toXml().toXMLString();
    }

    //------------------------------
    //	Properties
    //------------------------------

    private var _originalXml:String;

    public function get originalXml():String {
        return _originalXml;
    }

    public function set originalXml(value:String):void {
        _originalXml = value;
    }
}
}
