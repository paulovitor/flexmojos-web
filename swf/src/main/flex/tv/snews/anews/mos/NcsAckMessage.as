package tv.snews.anews.mos {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class NcsAckMessage implements MosMessage {

	public static const ACK:String = "ACK";
	public static const ERROR:String = "ERROR";

	public var status:String;
	public var statusDescription:String;

	public function NcsAckMessage(originalXml:String = null, errorMessage:String = null) {
		this.originalXml = originalXml;

		if (errorMessage) {
			this.status = ERROR;
			this.statusDescription = errorMessage;
		} else {
			this.status = ACK;
		}
	}

	public static function parseXml(xmlStr:String):NcsAckMessage {
		var xml:XML = new XML(xmlStr);

		if (!xml.ncsAck) {
			throw new Error("This is not an ncsAck message: " + xmlStr);
		}

		var ncsAck:NcsAckMessage = new NcsAckMessage(xmlStr);
		ncsAck.status = xml.ncsAck.status;
		ncsAck.statusDescription = xml.ncsAck.statusDescription;

		return ncsAck;
	}

	public function toXml():XML {
		if (statusDescription) {
			return <mos>
				<ncsAck>
					<status>{status}</status>
					<statusDescription>{statusDescription}</statusDescription>
				</ncsAck>
			</mos>
		} else {
			return <mos>
				<ncsAck>
					<status>{status}</status>
				</ncsAck>
			</mos>
		}
	}

	public function toXmlString():String {
		return toXml().toXMLString();
	}

	//------------------------------
	//	Properties
	//------------------------------

	private var _originalXml:String;

	public function get originalXml():String {
		return _originalXml;
	}

	public function set originalXml(value:String):void {
		_originalXml = value;
	}
}
}
