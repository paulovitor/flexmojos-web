package tv.snews.anews.mos {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class NcsAppInfoMessage implements MosMessage {

	// Opções para o "runContext"
	public static const BROWSE:String = "BROWSE";
	public static const CREATE:String = "CREATE";
	public static const EDIT:String = "EDIT";

	// Opções para o "mode"
	public static const CONTAINED:String = "CONTAINED";
	public static const MODAL_DIALOG:String = "MODALDIALOG";
	public static const NON_MODAL:String = "NONMODAL";
	public static const TOOLBAR:String = "TOOLBAR";

	public var mosID:String = "orad.mos";
	public var ncsID:String = "anews.snews.tv";
	public var userID:String = "anews";
	public var runContext:String = BROWSE;
	public var manufacturer:String = "SNEWS";
	public var product:String = "ANEWS";
	public var version:String = "1.7";
	public var mosActiveXVersion:String = "2.8.4";
	public var startX:int = 0;
	public var startY:int = 0;
	public var endX:int = 800;
	public var endY:int = 400;
	public var mode:String = CONTAINED;

	public function toXml():XML {
		return <mos>
			<mosID>{mosID}</mosID>
			<ncsID>{ncsID}</ncsID>
			<ncsAppInfo>
				<ncsInformation>
					<userID>{userID}</userID>
					<runContext>{runContext}</runContext>
					<software>
						<manufacturer>{manufacturer}</manufacturer>
						<product>{product}</product>
						<version>{version}</version>
					</software>
					<mosActiveXversion>{mosActiveXVersion}</mosActiveXversion>
					<UImetric num="0">
						<startx>{startX}</startx>
						<starty>{startY}</starty>
						<endx>{endX}</endx>
						<endy>{endY}</endy>
						<mode>{mode}</mode>
					</UImetric>
				</ncsInformation>
			</ncsAppInfo>
		</mos>;
	}

	public function toXmlString():String {
		return toXml().toXMLString();
	}

	//------------------------------
	//	Properties
	//------------------------------

	private var _originalXml:String;

	public function get originalXml():String {
		return _originalXml;
	}

	public function set originalXml(value:String):void {
		_originalXml = value;
	}
}
}
