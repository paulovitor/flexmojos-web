package tv.snews.anews.mos {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public interface MosMessage {

	function get originalXml():String;

	function set originalXml(value:String):void;

	function toXml():XML;

	function toXmlString():String;
}
}
