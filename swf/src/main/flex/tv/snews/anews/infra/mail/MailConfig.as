package tv.snews.anews.infra.mail {
	
	import tv.snews.anews.domain.AbstractEntity_Int;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.infra.mail.MailConfig")]
	public class MailConfig extends AbstractEntity_Int {
		
		public var email:String;
		public var host:String;
		public var ssl:String; // see SSL.as
		public var username:String;
		public var password:String;
		public var port:int;
		
		public function MailConfig() {
			super(this);
		}
	}
}