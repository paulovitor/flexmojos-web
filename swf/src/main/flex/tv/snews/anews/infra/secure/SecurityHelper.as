package tv.snews.anews.infra.secure {
	
	import mx.messaging.MessageAgent;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.remoting.RemoteObject;
	
	import tv.snews.anews.domain.User;
	import tv.snews.anews.service.RemoteObjectLocator;
	import tv.snews.anews.service.ServiceBase;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.4
	 */
	public class SecurityHelper extends ServiceBase {

		private static const instances:Object = {};
		
		private var remoteLocator:RemoteObjectLocator;
		private var remoteObject:RemoteObject;
		
		//------------------------------
		//	Singleton Constructor
		//------------------------------
		
		public function SecurityHelper(serverName:String=null, serverPort:int=0) {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] != null) {
				throw new Error("Singleton exception");
			}			
			
			remoteLocator = RemoteObjectLocator.getInstance(serverName, serverPort);
			remoteObject = remoteLocator.getRemoteObject("securityHelper");
		}
		
		public static function getInstance(serverName:String=null, serverPort:int=0):SecurityHelper {
			var key:String = instanceKey(serverName, serverPort);
			if (instances[key] == null) {
				instances[key] = new SecurityHelper(serverName, serverPort);
			}
			return instances[key];
		}

		//------------------------------
		//	Properties
		//------------------------------

		public function get authenticated():Boolean {
			return remoteObject.channelSet.authenticated;
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		public function getAuthentication(resultHandler:Function, faultHandler:Function = null):void {
			var asyncToken:AsyncToken = remoteObject.getOperation("getAuthentication").send();
			asyncToken.addResponder(getResponder(resultHandler, faultHandler));
		}
		
		public function login(user:User, resultHandler:Function, faultHandler:Function = null):void {
			if (authenticated) {
				// Se já está autenticado, apenas chama o resultHandler diretamente
				resultHandler(null);
			} else {
				var asyncToken:AsyncToken = remoteObject.channelSet.login(user.email, user.password);
				asyncToken.addResponder(getResponder(resultHandler, faultHandler));
			}
		}
		
		public function logout(resultHandler:Function, faultHandler:Function = null, agent:MessageAgent = null):void {
			if (authenticated) {
				var asyncToken:AsyncToken = remoteObject.channelSet.logout(agent);
				asyncToken.addResponder(getResponder(resultHandler, faultHandler));
			} else {
				// Se não está autenticado, apenas chama o resultHandler diretamente
				resultHandler(null);
			}
		}
	}
}