package tv.snews.anews.infra.mail {
	
	import mx.collections.ArrayList;
	import mx.collections.IList;
	
	public class SSL {
		
		public static const NONE:String = "NONE";
		public static const STARTTLS:String = "STARTTLS";
		public static const SSL_TLS:String = "SSL_TLS";
		
		public static function values():IList {
			return new ArrayList([ NONE, STARTTLS, SSL_TLS ]);
		}
	}
}