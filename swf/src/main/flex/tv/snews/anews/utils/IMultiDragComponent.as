package tv.snews.anews.utils {

import flash.geom.PerspectiveProjection;
import flash.geom.Rectangle;

import mx.core.IFactory;
import mx.core.IVisualElement;

import spark.components.IItemRenderer;

/**
 * @author Felipe Pinheiro
 */
public interface IMultiDragComponent {

	function get height():Number;

	function get width():Number;

	function get perspectiveProjection():PerspectiveProjection;

	function get scrollRect():Rectangle;

	function get itemsRenderer():Vector.<IItemRenderer>;

	function get itemRenderer():IFactory;

	function get itemRendererFunction():Function;

	function updateRenderer(renderer:IVisualElement, itemIndex:int, data:Object):void;
}
}
