package tv.snews.anews.utils {

import flash.net.FileFilter;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.net.navigateToURL;

import flashx.textLayout.conversion.TextConverter;

import flashx.textLayout.elements.TextFlow;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.resources.ResourceManager;
import mx.utils.URLUtil;
import mx.validators.Validator;

import spark.components.DataGrid;

import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;
import tv.snews.flexlib.validator.URLValidator;

/**
 * Classe utilitária com funções básicas para todo o sistema.
 *
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class Util {

    public static const CHAT_FILE_FILTER:Array = [
        new FileFilter(
                        ResourceManager.getInstance().getString("Bundle", "defaults.imagesFiles") +
                        " [png, jpeg, jpg, gif, bmp, pdf, txt, rtf, doc, docx, odt, xls, xlsx, ods, pps, ppsx, odp, mp3, wav, wma]",
                "*.png;*.jpeg;*.jpg;*.gif;*.bmp;*.pdf;*.txt;*.rtf;*.doc;*.docx;*.odt;*.xls;*.xlsx;*.ods;*.pps;*.ppsx;*.odp;*.mp3;*.wav;*.wma;"
        )
    ];

    public static const IMAGE_FILE_FILTER:Array = [
        new FileFilter(
                        ResourceManager.getInstance().getString("Bundle", "defaults.imagesFiles") +
                        " [png, jpeg, jpg]",
                "*.png;*.jpeg;*.jpg;"
        )
    ];

    private static var dateUtil:DateUtil = new DateUtil();

    /**
     * Verifica se um grupo de Validadores são validos ou não
     * @return O resultado da verificação
     */
    public static function isValid(validators:Array):Boolean {
        var result:Array = Validator.validateAll(validators);
        return result.length == 0;
    }

    /**
     * URL contexto padrão para upload de arquivos.
     * @return O String da URL.
     */
    public static function urlUpload():String {
        var url:String = URLUtil.getServerNameWithPort(FlexGlobals.topLevelApplication.url);
        return "http://" + url + "/flexmojos-web/files/upload.do";
    }

    /**
     * URL contexto padrão para upload de arquivos.
     * @return O String da URL.
     */
    public static function urlUploadLogo():String {
        var url:String = URLUtil.getServerNameWithPort(FlexGlobals.topLevelApplication.url);
        return "http://" + url + "/flexmojos-web/files/uploadLogo.do";
    }

    /**
     * URL contexto padrão para download de arquivos.
     * @return O String da URL.
     */
    public static function urlDownload():String {
        var url:String = URLUtil.getServerNameWithPort(FlexGlobals.topLevelApplication.url);
        return "http://" + url + "/flexmojos-web/files/download.do";
    }

    /**
     * Adqurie a extensão de um arquivo.
     *
     * @param file O Arquivo a ser verificado.
     * @return A extensão
     */
    public static function getExtension(file:String):String {
        if (StringUtils.isNullOrEmpty(file)) {
            return "";
        }

        var index:int = file.lastIndexOf(".");

        return index != -1 ? file.substr(index) : "";
    }

    /**
     * Adquire todos ids dos itens selecionados de uma grid e uma única
     * String separadas pelo token '|'.
     *
     * @param grid A DataGrid onde será obtido os itens selecionados
     * @return Uma String única contendo os ids separados por |
     */
    public static function getSelectedsAsString(grid:DataGrid):String {
        if (grid == null || grid.selectedItems.length == 0)
            return "";

        var ids:String = "";

        for each (var obj:Object in grid.selectedItems) {
            if (obj.hasOwnProperty("id"))
                ids += obj.id + "|";
        }
        return ids;
    }

    /**
     * Adquire uma coleção dos ids dos itens selecionados de uma grid.
     *
     * @param grid A DataGrid onde será obtido os itens selecionados
     * @return Uma String única contendo os ids separados por |
     */
    public static function getSelecteds(grid:DataGrid):ArrayCollection {
        if (grid && grid.selectedItems && grid.selectedItems.length != 0) {
            return Util.getIds(grid.selectedItems);
        } else {
            return null;
        }
    }

    public static function getIds(items:Vector.<Object>):ArrayCollection {
        var ids:ArrayCollection = new ArrayCollection();
        for each (var obj:Object in items) {
            if (obj.hasOwnProperty("id"))
                ids.addItem(obj.id);
        }
        return ids;
    }

    /**
     * Retorna uma intaância de DateTimeutil
     **/
    public static function getDateUtil():DateUtil {
        if (dateUtil == null) {
            dateUtil = new DateUtil();
        }
        return dateUtil;
    }

    /**
     * Escapa caracteres especiais que causam conflito com
     * o elemento TextFlow
     */
    public static function escapeCharacters(str:String):String {
        if (str == null) {
            return "";
        }

        str = str.replace(/</g, "&#60;");
        str = str.replace(/>/g, "&#62;");
        str = str.replace(/\n/g, "<br />");
        return  str;
    }

    /**
     * Adiciona a tag TextFlow utilizada na propriedade textFlow
     * do componente RichEditableText
     */
    public static function insertTagTextFlow(str:String):String {
        if (StringUtils.isNullOrEmpty(str))
            return "";
        str = escapeCharacters(str);
        if (str.indexOf("<TextFlow") != 0) {
            var builder:Array = new Array();
            builder.push("<TextFlow xmlns='http://ns.adobe.com/textLayout/2008'>");
            builder.push(str
                            .replace(/\|highlight\|/g, "<span backgroundColor='0xFAEF83' color='0x161616' fontWeight='bold'>").replace(/\|\/highlight\|/g, "</span>")
            );
            builder.push("</TextFlow>");
            return builder.join("");
        }
        return "";
    }

    public static function sizeExtension(value:int):String {
        var label:String = "";
        if (value < 1024000) {
            label = Number(value / 1024).toFixed(2) + " " + ResourceManager.getInstance().getString('Bundle', 'defaults.kbyte');
        }
        if (value >= 1024000 && value < 1048000000) {
            label = Number(value / 1024000).toFixed(2) + " " + ResourceManager.getInstance().getString('Bundle', 'defaults.megabyte');
        } else if (value >= 1048000000) {
            label = Number(value / 1048576).toFixed(2) + " " + ResourceManager.getInstance().getString('Bundle', 'defaults.gigabyte');
        }
        return label;
    }

    public static function formatName(name:String, index:int):String {
        return name + " " + (index < 10 ? "0" : "") + index;
    }

    public static function replaceHighlightMarkup(str:String):String {
        return (str || "").replace(/\|\/?highlight\|/g, "");
    }

    public static function concatNames(names:Array):String {
        const AND:String = ResourceManager.getInstance().getString('Bundle', 'defaults.and');
        const NONE:String = ResourceManager.getInstance().getString('Bundle', 'defaults.none');

        // Junta os nomes separando com vírgula
        var aux:String = names.join(", ");

        // Troca a última vírgula por 'e'
        var index:int = aux.lastIndexOf(",");
        if (index != -1) {
            return aux.substr(0, index) + ' ' + AND + aux.substr(index + 1, aux.length);
        } else {
            return StringUtils.trim(aux).length == 0 ? NONE : aux;
        }
    }

    public static function formatLinkInTextContent(value:String):TextFlow {
        var str:String = removeLineBreak(value);
        var urls:Array = URLValidator.searchUrlsInText(str);
        if (urls != null) {
            var formatedLink:String = "<a href='" + urls[0] + "' target='_blank'>" + urls[0] + "</a>";
            str = str.replace(urls[0], formatedLink);
        }
        return TextConverter.importToFlow(str, TextConverter.TEXT_FIELD_HTML_FORMAT);
    }

    private static function removeLineBreak(value:String):String {
        return value.replace(/\n/g, "");
    }

    public static function hasDifferencesInCollections(collection:ArrayCollection, otherCollection:ArrayCollection):Boolean {
        return (collection == null && otherCollection != null) ||
                (collection != null && otherCollection == null) ||
                (collection && otherCollection && collection.length != otherCollection.length);
    }

    public static function sendEmail(to:String, mailSubject:String = null, mailBody:String = null):void{
        var mailMsg:URLRequest = new URLRequest('mailto:' + to);
        var variables:URLVariables = new URLVariables();
        if(mailSubject) variables.subject = mailSubject;
        if(mailBody) variables.body = mailBody;
        mailMsg.data = variables;
        mailMsg.method = URLRequestMethod.GET;
        navigateToURL(mailMsg, "_self");
    }

}
}

