package tv.snews.anews.utils {
	
	import flash.system.Capabilities;
	
	/**
	 * @see http://stackoverflow.com/questions/185477/determine-if-swf-is-in-a-debug-player-or-mode
	 * @since 1.3.0
	 */
	public class ModeCheck {
		
		/**
		 * Returns true if the user is running the app on a Debug Flash Player.
		 * Uses the Capabilities class.
		 */
		public static function isDebugPlayer():Boolean {
			return Capabilities.isDebugger;
		}
		
		/**
		 * Returns true if the swf is built in debug mode.
		 */
		public static function isDebugBuild():Boolean {
			return new Error().getStackTrace().search(/:[0-9]+]$/m) > -1;
		}
		
		/**
		 * Returns true if the swf is built in release mode.
		 */
		public static function isReleaseBuild():Boolean {
			return !isDebugBuild();
		}
	}
}