package tv.snews.anews.utils {
	
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	
	public class ListenersDictionary {
		
		private var dispatcher:EventDispatcher;
		private var dictionary:Dictionary = new Dictionary();
		
		public function ListenersDictionary(dispatcher:EventDispatcher) {
			this.dispatcher = dispatcher;
		}
		
		public function registerListener(event:String, listener:Function):void {
			if (!dictionary[event]) {
				dictionary[event] = new Array();
			}
			dictionary[event].push(listener);
		}
		
		public function deactivateListeners():void {
			for (var event:String in dictionary) {
				for each (var listener:Function in dictionary[event]) {
					dispatcher.removeEventListener(event, listener);
				}
				delete dictionary[event];
			}
		}
	}
}