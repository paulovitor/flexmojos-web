package tv.snews.anews.utils {

import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.net.navigateToURL;

import mx.core.FlexGlobals;
import mx.utils.URLUtil;

/**
 * Classe utilitária que facilita as chamadas à geração de relatórios.
 *
 * @author Eliezer Reis
 * @since 1.0.0
 */
public class ReportsUtil {

	public static const CGS:String = "cgs.pdf";
	public static const CHECKLIST:String = "checklist.pdf";
	public static const CHECKLIST_GRID:String = "checklistGrid.pdf";
	public static const CHECKLIST_ARCHIVE_GRID:String = "checklistArchiveGrid.pdf";
	public static const GUIDELINE:String = "task.pdf";
	public static const GUIDELINE_ARCHIVE_GRID:String = "guidelineArchiveGrid.pdf";
	public static const REPORTAGE_ARCHIVE_GRID:String = "reportageArchiveGrid.pdf";
	public static const GUIDELINE_GRID:String = "guidelineGrid.pdf";
	public static const REPORT:String = "report.pdf";
	public static const REPORTAGE:String = "reportage.pdf";
	public static const ROUND:String = "round.pdf";
	public static const RUNDOWN:String = "rundown.pdf";
	public static const SCRIPT:String = "script.pdf";
	public static const SCRIPT_PLAN:String = "scriptPlan.pdf";
    public static const SCRIPT_PLAN_FUll:String = "scriptPlanFull.pdf";
	public static const STORY:String = "story.pdf";
	public static const STORY_OFF:String = "storyOff.pdf";
	public static const STORY_ARCHIVE_GRID:String = "storyArchiveGrid.pdf";
	public static const NEWS:String = "news.pdf";
    public static const STORY_PLAYLIST:String = "storyPlaylist.pdf";
    public static const SCRIPT_PLAYLIST:String = "scriptPlaylist.pdf";

	private var url:String;

	public function ReportsUtil(serverName:String = null, serverPort:int = 0) {
		if (serverName == null && serverPort == 0) {
			var aux:String = URLUtil.getServerNameWithPort(FlexGlobals.topLevelApplication.url);
			url = "http://" + aux + "/flexmojos-web/reports/";
		} else {
			url = "http://" + serverName + ":" + serverPort + "/flexmojos-web/reports/";
		}
	}

	public function generate(report:String, params:Object = null):void {
		var data:URLVariables = new URLVariables();
		data["reportDir"] = url;

		for (var param:String in params || {}) {
			data[param] = params[param];
		}

		// Evita o cache feito pelo browser
		data.time = Math.round(Math.random() * 9999);

		var request:URLRequest = new URLRequest(url + report);
		request.data = data;
		request.method = URLRequestMethod.POST;

		navigateToURL(request, "_blank");
	}
}
}
