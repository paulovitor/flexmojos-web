package tv.snews.anews.utils {

	import tv.snews.anews.domain.*;
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe utilitária responsável em tratar as permissões do
	 * usuário.
	 *
	 * @author Eliezer Reis
	 * @since 0.0.1
	 */
	public class PermissionUtil {

		/**
		 * Limpa o estado (state) da permissão e suas sub-permissões.
		 * @param permission A permissão a ser modificada.
		 */
		public function clearPermissions(permission:Permission):void {
			permission.state = "unchecked";
			for each (var child:Permission in permission.children) {
				clearPermissions(child);
			}
		}

		/**
		 * Se a lista de permissões possuir a permissão informada, muda o estado (state)
		 * dessa permissão e de suas sub-permissões para "checked".
		 *
		 * @param permissao A permissão a ser macada com checked"
		 * @param permissoes A lista completa de permissões onde será feita a tentativa de localizar a permissao.
		 */
		public function checkPermissions(permission:Permission, permissions:Permission):void {
			if (permissions.id == permission.id) {
				checkChildrenPermissions(permissions);
				return;
			}

			for each (var child:Permission in permissions.children) {
				checkPermissions(permission, child);
			}
		}

		/*
		 * Muda o state de permissão e todas as suas sub-permissões para "checked"
		 */
		private function checkChildrenPermissions(root:Permission):void {
			root.state = "checked";

			for each (var child:Permission in root.children) {
				checkChildrenPermissions(child);
			}
		}

		/**
		 * Adiciona todas as permissões pai com o state igual a checked as permissoes do grupo.
		 *
		 * @param group O Grupo a sofrer as modificações
		 * @param permission O objeto permission a ser copiado as permissões.
		 */
		public function addCheckedPermissionsToGroup(group:UserGroup, permission:Permission):void {
			if (permission.state == "checked") {
				group.permissions.addItem(permission);
				return;
			}

			for each (var permission:Permission in permission.children) {
				addCheckedPermissionsToGroup(group, permission);
			}
		}

		/**
		 * Habilita ou desabilita os menus de acordo com as permissoões
		 *
		 * @param user O usuário que será verificado suas permissões
		 * @param menuData Os dados do menu.
		 */
		public function checkPermissionsOfMenu(user:User, menuData:XMLList):void {
			if (user == null) {
				return;
			}

			for each (var menu:Object in menuData) {
				for each (var menuItem:Object in menu.menuitem) {
					if (StringUtils.isNullOrEmpty(menuItem.@permission)) {
						continue;
					}
					menuItem.@enabled = user.allow(menuItem.@permission);
				}
			}
		}
	}
}