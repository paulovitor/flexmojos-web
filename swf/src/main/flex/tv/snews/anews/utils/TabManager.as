package tv.snews.anews.utils {

	import flash.display.DisplayObject;
	import flash.events.Event;
	
	import flexlib.containers.SuperTabNavigator;
	import flexlib.controls.tabBarClasses.SuperTab;
	
	import mx.core.Container;
	import mx.events.ChildExistenceChangedEvent;
	import mx.events.FlexEvent;
	
	import spark.components.NavigatorContent;

	/**
	 * Classe utilitária para gerênciar as tabs
	 * abertas pelo usuário.
	 *
	 * @author Eliezer Reis.
	 * @since 0.0.1
	 */
	public class TabManager {
		
		private var tabs:Object = new Object();

		private var _tabNavigator:SuperTabNavigator;
		
		[Bindable]
		public function get tabNavigator():SuperTabNavigator {
			return _tabNavigator;
		}
		
		public function set tabNavigator(value:SuperTabNavigator):void {
			_tabNavigator = value;
			_tabNavigator.addEventListener(ChildExistenceChangedEvent.CHILD_REMOVE, 
				
				function(event:ChildExistenceChangedEvent):void {
					for (var key:String in tabs) {
						if (tabs[key] == event.relatedObject) {
							tabs[key] = null;
							delete tabs[key];
							break;
						}
					}
				}
			);
		}

		/**
		 * Verifica se existe alguma tab aberta com a chave informada.
		 */
		public function isTabOpened(key:String):Boolean {
			return key && tabs[key];
		}
			
		/**
		 * Abre um TabClass dentro do TabNavigator. Essa Tab poderá ter um id único
		 * permitindo essa Tab de ser aberta apenas uma vez e/ou parametros iniciais
		 * que serão enviados na inicialização da Tab.
		 *
		 * @param TabClass O NavigatorContent a ser aberto na TabNavigator
		 * @param uniqueID O identificado a ser usando na TAB.
		 * @param parametros Os parametros iniciais da TAB.
		 *
		 * @return O NavigatorContent criado dentro do TabNavigator
		 */
		public function openTab(TabClass:Class, key:String, params:Object = null, closeable:Boolean = true, index:int = -1):NavigatorContent {
			if (key && tabs[key]) {
				tabNavigator.selectedChild = tabs[key];
				return tabs[key];
			}

			var tab:NavigatorContent = new TabClass();
			tab.addEventListener(FlexEvent.CREATION_COMPLETE, 
				function(event:Event):void {
					if (!closeable) {
						setClosePolicyForTab(tabNavigator.getChildIndex(tab), SuperTab.CLOSE_NEVER);
					}
				}
			);
			tab.id = key;

			if (params != null) {
				for (var param:String in params) {
					tab[param] = params[param];
				}
			}

			tabs[key] = tab;

			if (index > -1) {
				tabNavigator.addChildAt(tab, index);
			} else {
				tabNavigator.addChild(tab);	
			}
			
			tabNavigator.selectedChild = tab;

			return tab;
		}

		public function updateTabKey(tab:DisplayObject, newKey:String):void {
			for (var key:String in tabs) {
				// Se não mudou a key de verdade então não pode entrar no "if" senão vai apenas apagar o registro
				if (tabs[key] == tab && key != newKey) {
					tabs[newKey] = tabs[key];
					tabs[newKey].id = newKey;
					
					tabs[key] = null;
					delete tabs[key];
					
					break;
				}
			}
		}
		
		public function removeTabByKey(key:String):void {
			if (tabs[key]) {
				tabNavigator.removeChild(tabs[key]);
			}
			tabs[key] = null;
			delete tabs[key];
		}
		
		public function removeTab(tab:DisplayObject):void {
			for (var key:String in tabs) {
				if (tabs[key] == tab) {
					tabNavigator.removeChild(tab);
					tabs[key] = null;
					delete tabs[key];
					break;
				}
			}
		}
		
		public function removeAllTabs():void {
			tabNavigator.removeAllChildren();
			tabs = new Object();
		}
			
		/**
		 * Permite definir a política de close da tab localizada no "index" 
		 * informado. O valor de "policy" deve ser uma das constantes definidas 
		 * na classe "flexlib.controls.tabBarClasses.SuperTab".
		 */
		public function setClosePolicyForTab(index:int, policy:String):void {
			tabNavigator.setClosePolicyForTab(index, policy);
		}
	}
}
