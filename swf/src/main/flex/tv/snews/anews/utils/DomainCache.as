package tv.snews.anews.utils {

import flash.utils.setTimeout;

	import mx.collections.ArrayCollection;
	import mx.collections.SortField;
    import mx.events.CollectionEvent;
	import mx.messaging.events.MessageEvent;
    import mx.resources.IResourceManager;
    import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	
	import spark.collections.Sort;
	
    import tv.snews.anews.domain.Device;

	import tv.snews.anews.domain.ChecklistResource;
	import tv.snews.anews.domain.ChecklistType;
	import tv.snews.anews.domain.Company;
	import tv.snews.anews.domain.ContactGroup;
	import tv.snews.anews.domain.EntityAction;
	import tv.snews.anews.domain.GuidelineClassification;
	import tv.snews.anews.domain.Institution;
	import tv.snews.anews.domain.Program;
	import tv.snews.anews.domain.ReportEvent;
	import tv.snews.anews.domain.ReportNature;
	import tv.snews.anews.domain.Segment;
	import tv.snews.anews.domain.Settings;
	import tv.snews.anews.domain.StoryKind;
	import tv.snews.anews.domain.UserGroup;
    import tv.snews.anews.domain.UserTeam;
    import tv.snews.anews.service.ChecklistResourceService;
	import tv.snews.anews.service.ChecklistTypeService;
	import tv.snews.anews.service.CompanyService;
	import tv.snews.anews.service.ContactGroupService;
    import tv.snews.anews.service.DeviceService;
	import tv.snews.anews.service.GroupService;
	import tv.snews.anews.service.GuidelineClassificationService;
	import tv.snews.anews.service.InstitutionService;
	import tv.snews.anews.service.NumberTypeService;
	import tv.snews.anews.service.ProgramService;
	import tv.snews.anews.service.ReportEventService;
	import tv.snews.anews.service.ReportNatureService;
	import tv.snews.anews.service.SegmentService;
	import tv.snews.anews.service.SettingsService;
	import tv.snews.anews.service.StoryKindService;
    import tv.snews.anews.service.TeamService;
    import tv.snews.anews.service.UserService;

/**
	 * Cache para os objetos das classes de domínio utilizadas comumente em
	 * várias áreas do sistema.
	 */
	public class DomainCache {

	private static const bundle:IResourceManager = ResourceManager.getInstance();

        private static const checklistTypeService:ChecklistTypeService = ChecklistTypeService.getInstance();
        private static const checklistResourceService:ChecklistResourceService = ChecklistResourceService.getInstance();
	    private static const deviceService:DeviceService = DeviceService.getInstance();
        private static const guidelineClassificationService:GuidelineClassificationService = GuidelineClassificationService.getInstance();
		private static const companyService:CompanyService = CompanyService.getInstance();
		private static const contactGroupService:ContactGroupService = ContactGroupService.getInstance();
		private static const groupService:GroupService = GroupService.getInstance();
        private static const teamService:TeamService = TeamService.getInstance();
		private static const institutionService:InstitutionService = InstitutionService.getInstance();
		private static const numberTypeService:NumberTypeService = NumberTypeService.getInstance();
		private static const programService:ProgramService = ProgramService.getInstance();
		private static const reportEventService:ReportEventService = ReportEventService.getInstance();
		private static const reportNatureService:ReportNatureService = ReportNatureService.getInstance();
		private static const segmentService:SegmentService = SegmentService.getInstance();
		private static const settingsService:SettingsService = SettingsService.getInstance();
		private static const storyKindsService:StoryKindService = StoryKindService.getInstance();
		private static const userService:UserService = UserService.getInstance();

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function DomainCache() {
			throw new Error("This class cannot be instantiated. You can only use it's static methods.");
		}

		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------

	//------------------------------
	//	Yes or No
	//------------------------------

	public static const YES_OR_NO:ArrayCollection = new ArrayCollection([
		{ id: true, label: bundle.getString("Bundle", "defaults.btn.yes") },
		{ id: false, label: bundle.getString("Bundle", "defaults.btn.no") },
	]);

		//----------------------------------
		//	contactGroups
		//----------------------------------

		private static var _contactGroups:ArrayCollection = null;

	[Bindable]
	[ArrayElementType("tv.snews.anews.domain.ContactGroup")]
	public static function get contactGroups():ArrayCollection {
			if (!_contactGroups) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_contactGroups = new ArrayCollection();
				contactGroupService.listAll(onContactGroupsLoaded);
			}
			return _contactGroups;
		}

		public static function set contactGroups(value:ArrayCollection):void {
			_contactGroups = value;
		}

	//------------------------------
	//	devices
	//------------------------------

	private static var _devices:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.Device")]
	public static function get devices():ArrayCollection {
		if (!_devices) {
			// O carregamento é assincrono, mas o databind resolve isso
			_devices = new ArrayCollection();
			deviceService.findAll(onDevicesLoaded);
		}
		return _devices;
	}

	//------------------------------
	//	cg devices
	//------------------------------

	private static var _cgDevices:ArrayCollection = null;

	/*
	 * NOTE Faz um filtro da coleção devices. Não precisa de load e nem de notificações.
	 */
	[ArrayElementType("tv.snews.anews.domain.Device")]
	public static function get cgDevices():ArrayCollection {
		if (!_cgDevices) {
			_cgDevices = new ArrayCollection();
			_cgDevices.addAll(devices);

			_cgDevices.filterFunction = function(item:Object):Boolean {
				return Device(item).isCG();
			};

			_cgDevices.refresh();

			devices.addEventListener(CollectionEvent.COLLECTION_CHANGE,
					function(event:CollectionEvent):void {
						cgDevices.removeAll();
						// O método addAll() causa uma exceção, não me pergunte o porquê.
						for each (var device:Device in devices) {
							cgDevices.addItem(device);
						}
						cgDevices.refresh();
					}
			);
		}

		return _cgDevices;
	}

	//------------------------------
	//	playout devices
	//------------------------------

	private static var _playoutDevices:ArrayCollection = null;

	/*
	 * NOTE Faz um filtro da coleção devices. Não precisa de load e nem de notificações.
	 */
	[ArrayElementType("tv.snews.anews.domain.Device")]
	public static function get playoutDevices():ArrayCollection {
		if (!_playoutDevices) {
			_playoutDevices = new ArrayCollection();
			_playoutDevices.addAll(devices);

			_playoutDevices.filterFunction = function(item:Object):Boolean {
				return Device(item).isPlayout();
			};

			_playoutDevices.refresh();

			devices.addEventListener(CollectionEvent.COLLECTION_CHANGE,
					function(event:CollectionEvent):void {
						playoutDevices.removeAll();
						// O método addAll() causa uma exceção, não me pergunte o porquê.
						for each (var device:Device in devices) {
							playoutDevices.addItem(device);
						}
						playoutDevices.refresh();
					}
			);
		}

		return _playoutDevices;
	}

	//------------------------------
	//	playout devices
	//------------------------------

	private static var _mamDevices:ArrayCollection = null;

	/*
	 * NOTE Faz um filtro da coleção devices. Não precisa de load e nem de notificações.
	 */
	[ArrayElementType("tv.snews.anews.domain.Device")]
	public static function get mamDevices():ArrayCollection {
		if (!_mamDevices) {
			_mamDevices = new ArrayCollection();
			_mamDevices.addAll(devices);

			_mamDevices.filterFunction = function(item:Object):Boolean {
				return Device(item).isMAM();
			};

			_mamDevices.refresh();

			devices.addEventListener(CollectionEvent.COLLECTION_CHANGE,
					function(event:CollectionEvent):void {
						mamDevices.removeAll();
						// O método addAll() causa uma exceção, não me pergunte o porquê.
						for each (var device:Device in devices) {
							mamDevices.addItem(device);
						}
						mamDevices.refresh();
					}
			);
		}

		return _mamDevices;
	}

		//----------------------------------
		//	groups
		//----------------------------------

		private static var _groups:ArrayCollection = null;

	[Bindable]
	[ArrayElementType("tv.snews.anews.domain.UserGroup")]
	public static function get groups():ArrayCollection {
			if (!_groups) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_groups = new ArrayCollection();
				groupService.listAll(onGroupsLoaded);
			}
			return _groups;
		}

		public static function set groups(value:ArrayCollection):void {
			_groups = value;
		}


        //----------------------------------
        //	teams
        //----------------------------------

        private static var _teams:ArrayCollection = null;

        [Bindable]
        [ArrayElementType("tv.snews.anews.domain.UserTeam")]
        public static function get teams():ArrayCollection {
            if (!_teams) {
                // O carregamento é assíncrono, mas o databind resolve isso
                _teams = new ArrayCollection();
                teamService.listAll(onTeamsLoaded);
            }
            return _teams;
        }

        public static function set teams(value:ArrayCollection):void {
            _teams = value;
        }


        //----------------------------------
		//	number types
		//----------------------------------
		
		private static var _numberTypes:ArrayCollection = null;
		
	[Bindable]
	[ArrayElementType("tv.snews.anews.domain.NumberType")]
	public static function get numberTypes():ArrayCollection {
			if (!_numberTypes) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_numberTypes = new ArrayCollection();
				numberTypeService.listAll(onNumberTypesLoaded);
			}
			return _numberTypes;
		}
		
		public static function set numberTypes(value:ArrayCollection):void {
			_numberTypes = value;
		}
		
		//----------------------------------
		//	programs
		//----------------------------------

		private static var _programs:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.Program")]
		public static function get programs():ArrayCollection {
			if (!_programs) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_programs = new ArrayCollection();
				programService.listAll(onProgramsLoaded);
			}
			return _programs;
		}

		//----------------------------------
		//	segments
		//----------------------------------

		private static var _segments:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.Segment")]
		public static function get segments():ArrayCollection {
			if (!_segments) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_segments = new ArrayCollection();
				segmentService.listAll(onSegmentsLoaded);
			}
			return _segments;
		}

		//----------------------------------
		//	report events
		//----------------------------------
		
		private static var _reportEvents:ArrayCollection = null;
		
	[ArrayElementType("tv.snews.anews.domain.ReportEvent")]
		public static function get reportEvents():ArrayCollection {
			if (!_reportEvents) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_reportEvents = new ArrayCollection();
				reportEventService.listAll(onReportEventsLoaded);
			}
			return _reportEvents;
		}
		
		//----------------------------------
		//	report natures
		//----------------------------------
		
		private static var _reportNatures:ArrayCollection = null;
		
	[ArrayElementType("tv.snews.anews.domain.ReportNature")]
		public static function get reportNatures():ArrayCollection {
			if (!_reportNatures) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_reportNatures = new ArrayCollection();
				reportNatureService.listAll(onReportNaturesLoaded);
			}
			return _reportNatures;
		}
		
        //----------------------------------
        //	checklist types
        //----------------------------------

        private static var _checklistTypes:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.ChecklistType")]
        public static function get checklistTypes():ArrayCollection {
            if (!_checklistTypes) {
                // O carregamento é assíncrono, mas o databind resolve isso
                _checklistTypes = new ArrayCollection();
                checklistTypeService.listAll(onChecklistTypesLoaded);
            }
            return _checklistTypes;
        }

		//----------------------------------
		//	checklist resources
		//----------------------------------

		private static var _checklistResources:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.ChecklistResource")]
		public static function get checklistResources():ArrayCollection {
			if (!_checklistResources) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_checklistResources = new ArrayCollection();
				checklistResourceService.listAll(onChecklistResourcesLoaded);
			}
			return _checklistResources;
		}
		
		//----------------------------------
		//	Guideline classification
		//----------------------------------

		private static var _guidelineClassifications:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.GuidelineClassification")]
		public static function get guidelineClassifications():ArrayCollection {
			if (!_guidelineClassifications) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_guidelineClassifications = new ArrayCollection();
				guidelineClassificationService.listAll(onGuidelineClassificationsLoaded);
			}
			return _guidelineClassifications;
		}

		//----------------------------------
		//	companies
		//----------------------------------

		private static var _companies:ArrayCollection = null;
		
	[ArrayElementType("tv.snews.anews.domain.Company")]
		public static function get companies():ArrayCollection {
			if (!_companies) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_companies = new ArrayCollection();
				companyService.listAll(onCompaniesLoaded);
			}
			return _companies;
		}
		
		//----------------------------------
		//	reporters
		//----------------------------------

		private static var _reporters:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.User")]
		public static function get reporters():ArrayCollection {
			if (!_reporters) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_reporters = new ArrayCollection();
				userService.listReporters(onReportersLoaded);
			}
			return _reporters;
		}

		//----------------------------------
		//	producers (pauteiros)
		//----------------------------------

		private static var _producers:ArrayCollection = null;

	[ArrayElementType("tv.snews.anews.domain.User")]
		public static function get producers():ArrayCollection {
			if (!_producers) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_producers = new ArrayCollection();
				userService.listProducers(onProducersLoaded);
			}
			return _producers;
		}

        //----------------------------------
        //	checklist producers
        //----------------------------------

        private static var _checklistProducers:ArrayCollection = null;

        [ArrayElementType("tv.snews.anews.domain.User")]
        public static function get checklistProducers():ArrayCollection {
            if (!_checklistProducers) {
                // O carregamento é assíncrono, mas o databind resolve isso
                _checklistProducers = new ArrayCollection();
                userService.listChecklistProducers(onChecklistProducersLoaded);
            }
            return _checklistProducers;
        }

		//----------------------------------
		//	editors
		//----------------------------------
		
		private static var _editors:ArrayCollection = null;
		
		public static function get editors():ArrayCollection {
			if (!_editors) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_editors = new ArrayCollection();
				userService.listEditors(onEditorsLoaded);
			}
			return _editors;
		}

		//----------------------------------
		//	settings
		//----------------------------------

		private static var _settings:Settings = null;

        [Bindable]
		public static function get settings():Settings {
			if (!_settings) {
				_settings = new Settings();
				// O carregamento é assincrono, mas o databind resolve isso
				settingsService.loadSettings(onSettingsLoaded);
			}
			return _settings;
		}

        public static function set settings(value:Settings):void {
			// Não troca a referência para que o bind funcione
			_settings.updateFields(value);
		}

		//----------------------------------
		//	editors and presenters
		//----------------------------------

		[Bindable]
		public static var programCache:ProgramCache = new ProgramCache();

		//----------------------------------
		//	checklists
		//----------------------------------
		
		private static var _storyKinds:ArrayCollection = null;
		
	[ArrayElementType("tv.snews.anews.domain.StoryKind")]
		public static function get storyKinds():ArrayCollection {
			if (!_storyKinds) {
				// O carregamento é assíncrono, mas o databind resolve isso
				_storyKinds = new ArrayCollection();
				storyKindsService.listAll(onStoryKindsLoaded);
			}
			return _storyKinds;
		}
		
		//----------------------------------------------------------------------
		//	Handlers
		//----------------------------------------------------------------------

		//----------------------------------
		//	Contact Groups Handlers
		//----------------------------------

		/**
		 * Handler invocado quando o método ContactGroupService.listAll() é
		 * concluído com sucesso.
		 */
		private static function onContactGroupsLoaded(event:ResultEvent):void {
			contactGroups.addAll(event.result as ArrayCollection);

			// Configurando o sort da coleção pelo nome do grupo
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			contactGroups.sort = sort;
			contactGroups.refresh();

			// Configura o monitoramento de mudanças feitas no CRUD de grupos
			contactGroupService.subscribe(onContactGroupsChanged);
		}

		private static function onContactGroupsChanged(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:ContactGroup, contactGroup:ContactGroup = event.message.body as ContactGroup;

			switch (action) {
			case EntityAction.INSERTED:
					contactGroups.addItem(contactGroup);
					contactGroups.refresh();
					break;
			case EntityAction.UPDATED:
					for each (aux in contactGroups) {
						if (aux.id == contactGroup.id) {
							aux.copy(contactGroup);
							contactGroups.refresh();
							break;
						}
					}
					break;
			case EntityAction.DELETED:
					for (var i:int = 0; i < contactGroups.length; i++) {
						aux = contactGroups.getItemAt(i) as ContactGroup;
						if (aux.id == contactGroup.id) {
							contactGroups.removeItemAt(i);
							break;
						}
					}
					break;
			default:
				throw new Error("[ImplementationError] Unknow contact group action: " + action);
		}
	}

	//------------------------------
	//	Devices Handlers
	//------------------------------

	private static function onDevicesLoaded(event:ResultEvent):void {
		devices.addAll(ArrayCollection(event.result));

		// Configura a ordenação pelo nome do dispositivo
		var sort:Sort = new Sort();
		sort.fields = [ new SortField("name", true, false, false) ];

		devices.sort = sort;
		devices.refresh();

		deviceService.subscribe(onDeviceChange);
	}

	private static function onDeviceChange(event:MessageEvent):void {
		var action:String = event.message.headers["action"];
		var aux:Device, device:Device = event.message.body as Device;

		switch (action) {
			case EntityAction.INSERTED:
					devices.addItem(device);
					devices.refresh();
				break;
			case EntityAction.UPDATED:
					for each (aux in devices) {
						if (aux.id == device.id) {
							aux.update(device);
							devices.refresh();
							break;
						}
					}
				break;
			case EntityAction.DELETED:
					for (var i:int = 0; i < devices.length; i++) {
						aux = devices.getItemAt(i) as Device;
						if (aux.id == device.id) {
							devices.removeItemAt(i);
							break;
				}
				}
				break;
			}
		}

		//----------------------------------
		//	Groups Handlers
		//----------------------------------

        /**
         * Handler invocado quando o método TeamService.listAll() é concluído
         * com sucesso.
         */
        private static function onTeamsLoaded(event:ResultEvent):void {
            teams.addAll(event.result as ArrayCollection);

            // Configurando o sort da coleção pelo nome da equipe
            var nameField:SortField = new SortField();
            nameField.name = "name";
            nameField.numeric = false;
            nameField.caseInsensitive = true;
            var sort:Sort = new Sort();
            sort.fields = [nameField];
            teams.sort = sort;
            teams.refresh();

            // Configura o monitoramento de mudanças feitas no CRUD de equipes
            teamService.subscribe(onTeamsChanged);
        }

        /**
         * Handler que trata as mensagens recebidas que notificam mudanças CRUD
         * de grupos por qualquer usuário.
         */
        private static function onTeamsChanged(event:MessageEvent):void {
            var action:String = event.message.headers["action"];
            var aux:UserTeam, team:UserTeam = event.message.body as UserTeam;

            switch (action) {
                case EntityAction.INSERTED:
                {
                    teams.addItem(team);
                    teams.refresh();
                    break;
                }
                case EntityAction.UPDATED:
                {
                    for each (aux in teams) {
                        if (aux.id == team.id) {
                            aux.copy(team);
                            groups.refresh();
                            break;
                        }
                    }
                    break;
                }
                case EntityAction.DELETED:
                {
                    for (var i:int = 0; i < teams.length; i++) {
                        aux = teams.getItemAt(i) as UserTeam;
                        if (aux.id == team.id) {
                            teams.removeItemAt(i);
                            break;
                        }
                    }
                    break;
                }
            }
        }

		/**
		 * Handler invocado quando o método GroupService.listAll() é concluído
		 * com sucesso.
		 */
		private static function onGroupsLoaded(event:ResultEvent):void {
			groups.addAll(event.result as ArrayCollection);

			// Configurando o sort da coleção pelo nome do grupo
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			groups.sort = sort;
			groups.refresh();

			// Configura o monitoramento de mudanças feitas no CRUD de grupos
			groupService.subscribe(onGroupsChanged);
		}

		/**
		 * Handler que trata as mensagens recebidas que notificam mudanças CRUD
		 * de grupos por qualquer usuário.
		 */
		private static function onGroupsChanged(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:UserGroup, group:UserGroup = event.message.body as UserGroup;

			switch (action) {
			case EntityAction.INSERTED:
			{
					groups.addItem(group);
					groups.refresh();
					break;
				}
			case EntityAction.UPDATED:
			{
					for each (aux in groups) {
						if (aux.id == group.id) {
							aux.copy(group);
							groups.refresh();
							break;
						}
					}
					break;
				}
			case EntityAction.DELETED:
			{
					for (var i:int = 0; i < groups.length; i++) {
						aux = groups.getItemAt(i) as UserGroup;
						if (aux.id == group.id) {
							groups.removeItemAt(i);
							break;
						}
					}
					break;
				}
			}
		}

		//----------------------------------
		//	Number Types Handlers
		//----------------------------------
		
		/**
		 * Handler invocado quando o método NumberType.listAll() é concluído
		 * com sucesso.
		 */
		private static function onNumberTypesLoaded(event:ResultEvent):void {
			numberTypes.addAll(event.result as ArrayCollection);
			
			// Configurando o sort da coleção pelo nome do grupo
			var nameField:SortField = new SortField();
			nameField.name = "description";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			numberTypes.sort = sort;
			numberTypes.refresh();
		}
		
		//----------------------------------
		//	Programs Handlers
		//----------------------------------

		/**
		 * Handler invocado quando o método ProgramService.listAll() é concluído
		 * com sucesso.
		 */
		private static function onProgramsLoaded(event:ResultEvent):void {
			programs.addAll(event.result as ArrayCollection);

			// Configurando o sort da coleção pelo nome do programa
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			programs.sort = sort;
			programs.refresh();

			// Configura o monitoramento de mudanças feitas no CRUD de programas
			programService.subscribe(onProgramsChanged);
		}

		/**
		 * Handler que trata as mensagens recebidas que notificam mudanças CRUD
		 * de programas por qualquer usuário.
		 */
		private static function onProgramsChanged(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:Program, program:Program = event.message.body as Program;

			switch (action) {
			case EntityAction.INSERTED:
			{
					programs.addItem(program);
					programs.refresh();
					break;
				}
			case EntityAction.UPDATED:
			{
					for each (aux in programs) {
						if (aux.id == program.id) {
							aux.copy(program);
							programs.refresh();
							break;
						}
					}
					break;
				}
			case EntityAction.DELETED:
			{
					for (var i:int = 0; i < programs.length; i++) {
						aux = programs.getItemAt(i) as Program;
						if (aux.id == program.id) {
							programs.removeItemAt(i);
							break;
						}
					}
					break;
				}
			}
		}

		//----------------------------------
		//	Segments Handlers
		//----------------------------------

		/**
		 * Handler invocado quando o método SegmentService.listAll() é concluído
		 * com sucesso.
		 */
		private static function onSegmentsLoaded(event:ResultEvent):void {
			segments.addAll(event.result as ArrayCollection);

			// Configurando o sort da coleção pelo nome do segmento
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			segments.sort = sort;
			segments.refresh();

			// Configura o monitoramento de mudanças feitas no CRUD de segmentos
			segmentService.subscribe(onSegmentsChanged);
			institutionService.subscribe(onInstitutionChanged);
		}

	/**
	 * Handler que trata as mensagens recebidas que notificam mudanças CRUD
	 * de segmentos por qualquer usuário.
	 */
	private static function onSegmentsChanged(event:MessageEvent):void {
		var action:String = event.message.headers["action"];
		var aux:Segment, segment:Segment = event.message.body as Segment;

		switch (action) {
			case EntityAction.INSERTED:
				segments.addItem(segment);
				segments.refresh();
				break;
			case EntityAction.UPDATED:
				for each (aux in segments) {
					if (aux.equals(segment)) {
						aux.copy(segment);
						segments.refresh();
						break;
					}
				}
				break;
			case EntityAction.DELETED:
				for (var i:int = 0; i < segments.length; i++) {
					aux = segments.getItemAt(i) as Segment;
					if (aux.equals(segment)) {
						segments.removeItemAt(i);
						break;
					}
				}
				break;
		}
	}

	/**
	 * Handler que trata as mensagens recebidas que notificam mudanças CRUD
	 * de instituitions por qualquer usuário.
	 */
	private static function onInstitutionChanged(event:MessageEvent):void {
		var action:String = event.message.headers["action"];
		var institution:Institution = event.message.body as Institution;
		var segment:Segment;

		switch (action) {
			case EntityAction.DELETED:
				for each (segment in segments) {
					segment.removeInstitution(institution);
				}
				break;
			case EntityAction.INSERTED:
				for each (segment in segments) {
					if (segment.equals(institution.segment)) {
						segment.addInstitution(institution);
					}
				}
				break;
			case EntityAction.UPDATED:
				// Busca a referência local no domain cache
				var local:Institution = (function():Institution {
					for each (segment in segments) {
						for each (var curr:Institution in segment.institutions) {
							if (curr.equals(institution)) {
								return curr;
							}
						}
					}
					throw new Error("Institution not found.");
				})();

				var oldSegment:Segment = local.segment;
				var newSegment:Segment = institution.segment;

				local.updateFields(institution);

				// Se alterou o segmento tem que mover do antigo para o novo
				if (!newSegment.equals(oldSegment)) {
					oldSegment.removeInstitution(local);
					newSegment.addInstitution(local);
				}

				break;
		}
	}

		//----------------------------------
		//	ReportEvent Handler
		//----------------------------------
		
		/**
		 * Handler invocado quando o método reportEventService.findAll() é 
		 * concluído com sucesso.
		 */
		private static function onReportEventsLoaded(event:ResultEvent):void {
			reportEvents.addAll(ArrayCollection(event.result));
			
			// Configurando o sort da coleção pelo nome do evento
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [ nameField ];
			reportEvents.sort = sort;
			reportEvents.refresh();
			
			// Configura o monitoramento de mudanças feitas no CRUD de eventos
			reportEventService.subscribe(onChangeReportEvent);
		}
		
		private static function onChangeReportEvent(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:ReportEvent, reportEvent:ReportEvent = ReportEvent(event.message.body);
			
			switch (action) {
				case EntityAction.INSERTED:
					reportEvents.addItem(reportEvent);
					reportEvents.refresh();
					break;
				case EntityAction.UPDATED:
					for each (aux in reportEvents) {
						if (aux.equals(reportEvent)) {
							aux.copy(reportEvent);
							reportEvents.refresh();
							break;
						}
					}
					break;
				case EntityAction.DELETED:
					for (var i: int = 0; i < reportEvents.length; i++) {
						aux = ReportEvent(reportEvents.getItemAt(i));
						if (aux.equals(reportEvent)) {
							reportEvents.removeItemAt(i);
							break;
						}
					}
					break;
			}
		}
		
		//----------------------------------
		//	ReportNature Handler
		//----------------------------------
		
		/**
		 * Handler invocado quando o método reportNatureService.findAll() é 
		 * concluído com sucesso.
		 */
		private static function onReportNaturesLoaded(event:ResultEvent):void {
			reportNatures.addAll(ArrayCollection(event.result));
			
			// Configurando o sort da coleção pelo nome da natureza
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [ nameField ];
			reportNatures.sort = sort;
			reportNatures.refresh();
			
			// Configura o monitoramento de mudanças feitas no CRUD de naturezas
			reportNatureService.subscribe(onChangeReportNature);
		}
		
		private static function onChangeReportNature(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:ReportNature, reportNature:ReportNature = ReportNature(event.message.body);
			
			switch (action) {
				case EntityAction.INSERTED:
					reportNatures.addItem(reportNature);
					reportNatures.refresh();
					break;
				case EntityAction.UPDATED:
					for each (aux in reportNatures) {
						if (aux.equals(reportNature)) {
							aux.copy(reportNature);
							reportNatures.refresh();
							break;
						}
					}
					break;
				case EntityAction.DELETED:
					for (var i: int = 0; i < reportNatures.length; i++) {
						aux = ReportNature(reportNatures.getItemAt(i));
						if (aux.equals(reportNature)) {
							reportNatures.removeItemAt(i);
							break;
						}
					}
					break;
			}
		}

        //----------------------------------
        //	Checklist Types Handler
        //----------------------------------

        /**
         * Handler invocado quando o método checklistTypeService.findAll() é
         * concluído com sucesso.
         */
        private static function onChecklistTypesLoaded(event:ResultEvent):void {
            checklistTypes.addAll(ArrayCollection(event.result));

            // Configurando o sort da coleção pelo nome do recurso
            var nameField:SortField = new SortField();
            nameField.name = "name";
            nameField.numeric = false;
            nameField.caseInsensitive = true;
            var sort:Sort = new Sort();
            sort.fields = [ nameField ];
            checklistTypes.sort = sort;
            checklistTypes.refresh();

            // Configura o monitoramento de mudanças feitas no CRUD de tipos
            checklistTypeService.subscribe(onChangeChecklistTypes);
        }

        private static function onChangeChecklistTypes(event:MessageEvent):void {
            var action:String = event.message.headers["action"];
            var aux:ChecklistType, checklistType:ChecklistType = ChecklistType(event.message.body);

            switch (action) {
                case EntityAction.INSERTED:
                    checklistTypes.addItem(checklistType);
                    checklistTypes.refresh();
                    break;
                case EntityAction.UPDATED:
                    for each (aux in checklistTypes) {
                        if (aux.equals(checklistType)) {
                            aux.copy(checklistType);
                            checklistTypes.refresh();
                            break;
                        }
                    }
                    break;
                case EntityAction.DELETED:
                    for (var i: int = 0; i < checklistTypes.length; i++) {
                        aux = ChecklistType(checklistTypes.getItemAt(i));
                        if (aux.equals(checklistType)) {
                            checklistTypes.removeItemAt(i);
                            break;
                        }
                    }
                    break;
            }
        }

		//----------------------------------
		//	Checklist Resources Handler
		//----------------------------------
		
		/**
		 * Handler invocado quando o método checklistResourceService.findAll() é 
		 * concluído com sucesso.
		 */
		private static function onChecklistResourcesLoaded(event:ResultEvent):void {
			checklistResources.addAll(ArrayCollection(event.result));
			
			// Configurando o sort da coleção pelo nome do recurso
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [ nameField ];
			checklistResources.sort = sort;
			checklistResources.refresh();
			
			// Configura o monitoramento de mudanças feitas no CRUD de recursos
			checklistResourceService.subscribe(onChangeChecklistResources);
		}
		
		private static function onChangeChecklistResources(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:ChecklistResource, checklistResource:ChecklistResource = ChecklistResource(event.message.body);
			
			switch (action) {
				case EntityAction.INSERTED:
					checklistResources.addItem(checklistResource);
					checklistResources.refresh();
					break;
				case EntityAction.UPDATED:
					for each (aux in checklistResources) {
						if (aux.equals(checklistResource)) {
							aux.copy(checklistResource);
							checklistResources.refresh();
							break;
						}
					}
					break;
				case EntityAction.DELETED:
					for (var i: int = 0; i < checklistResources.length; i++) {
						aux = ChecklistResource(checklistResources.getItemAt(i));
						if (aux.equals(checklistResource)) {
							checklistResources.removeItemAt(i);
							break;
						}
					}
					break;
			}
		}
		
		//----------------------------------
		//	Guideline Classification Handler
		//----------------------------------
		
		/**
		 * Handler invocado quando o método guidelineClassificationService.findAll() é 
		 * concluído com sucesso.
		 */
		private static function onGuidelineClassificationsLoaded(event:ResultEvent):void {
			guidelineClassifications.addAll(ArrayCollection(event.result));
			
			// Configurando o sort da coleção pelo nome da classificação da pauta
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [ nameField ];
			guidelineClassifications.sort = sort;
			guidelineClassifications.refresh();
			
			// Configura o monitoramento de mudanças feitas no CRUD de classificação da pauta
			guidelineClassificationService.subscribe(onChangeguidelineClassifications);
		}
		
		private static function onChangeguidelineClassifications(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:GuidelineClassification, guidelineClassification:GuidelineClassification = GuidelineClassification(event.message.body);
			
			switch (action) {
				case EntityAction.INSERTED:
					guidelineClassifications.addItem(guidelineClassification);
					guidelineClassifications.refresh();
					break;
				case EntityAction.UPDATED:
					for each (aux in guidelineClassifications) {
						if (aux.equals(guidelineClassification)) {
							aux.copy(guidelineClassification);
							guidelineClassifications.refresh();
							break;
						}
					}
					break;
				case EntityAction.DELETED:
					for (var i: int = 0; i < guidelineClassifications.length; i++) {
						aux = GuidelineClassification(guidelineClassifications.getItemAt(i));
						if (aux.equals(guidelineClassification)) {
							guidelineClassifications.removeItemAt(i);
							break;
						}
					}
					break;
			}
		}
		
		//----------------------------------
		//	Companies Handler
		//----------------------------------
		
		/**
		 * Handler invocado quando o método companyService.listAll() é 
		 * concluído com sucesso.
		 */
		private static function onCompaniesLoaded(event:ResultEvent):void {
			companies.addAll(ArrayCollection(event.result));
			
			// Configurando o sort da coleção pelo nome da praça.
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [ nameField ];
			companies.sort = sort;
			companies.refresh();
			
			// inicia o monitoramento do status do host da praça
			for each (var company:Company in companies) {
				company.startMonitoringTheHost();
			}
			
			
			// Configura o monitoramento de mudanças feitas no CRUD de praça
			companyService.subscribe(onChangeCompanies);
		}
		
		private static function onChangeCompanies(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:Company, company:Company = Company(event.message.body);
			
			switch (action) {
				case EntityAction.INSERTED:
					company.startMonitoringTheHost();
					companies.addItem(company);
					companies.refresh();
					break;
				case EntityAction.UPDATED:
					for each (aux in companies) {
						if (aux.equals(company)) {
							aux.stopMonitoringTheHost();
							aux.updateFields(company);
							aux.startMonitoringTheHost();
							companies.refresh();
							break;
						}
					}
					break;
				case EntityAction.DELETED:
					for (var i: int = 0; i < companies.length; i++) {
						aux = Company(companies.getItemAt(i));
						if (aux.equals(company)) {
							aux.stopMonitoringTheHost();
							companies.removeItemAt(i);
							break;
						}
					}
					break;
			}
		}
		
		//----------------------------------
		//	Reporter Handlers
		//----------------------------------

		/**
		 * Handler invocado quando o método userService.listReporters() é concluído
		 * com sucesso.
		 */
		private static function onReportersLoaded(event:ResultEvent):void {
			reporters.addAll(event.result as ArrayCollection);

			// Configurando o sort da coleção pelo nome do repórter
			var nameField:SortField = new SortField();
			nameField.name = "nickname";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			reporters.sort = sort;
			reporters.refresh();

			// Configura o monitoramento de mudanças feitas no CRUD de usuários e grupos
			groupService.subscribe(onChangeReporter);
			userService.subscribe(onChangeReporter);
		}
		
		/*
		* Handler que trata as mensagens recebidas que notificam mudanças CRUD
		* de segmentos por qualquer usuário.
		*/
		private static function onChangeReporter(event:MessageEvent):void {
			/**
			 * TODO Refatorar esse método para ele comportar comos os outros, ou
			 * seja, identificar a mudança que foi feita e atualizar a coleção
			 * sem ter que comunicar com o servidor.
			 */
			userService.listReporters(function(event:ResultEvent):void {
				if(reporters)
					reporters.removeAll();
				
				var _result:ArrayCollection = event.result as ArrayCollection;
				
				if(_result) {
					reporters.addAll(_result);
					reporters.refresh();
				}
			});
		}
		
		//----------------------------------
		//	Producer Handlers
		//----------------------------------

		/**
		 * Handler invocado quando o método userService.listProducers() é concluído
		 * com sucesso.
		 */
		private static function onProducersLoaded(event:ResultEvent):void {
			producers.addAll(event.result as ArrayCollection);

			// Configurando o sort da coleção pelo nome do segmento
			var nameField:SortField = new SortField();
			nameField.name = "nickname";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			producers.sort = sort;
			producers.refresh();

			// Configura o monitoramento de mudanças feitas no CRUD de usuários e grupos
			groupService.subscribe(onChangeProducer);
			userService.subscribe(onChangeProducer);
		}

		/*
		 * Handler que trata as mensagens recebidas que notificam mudanças CRUD
		 * de segmentos por qualquer usuário.
		 */
		private static function onChangeProducer(event:MessageEvent):void {
			/**
			 * TODO Refatorar esse método para ele comportar comos os outros, ou
			 * seja, identificar a mudança que foi feita e atualizar a coleção
			 * sem ter que comunicar com o servidor.
			 */
			userService.listProducers(function(event:ResultEvent):void {
				if(producers)
					producers.removeAll();
				
				var _result:ArrayCollection = event.result as ArrayCollection;
				
				if(_result) {
					producers.addAll(_result);
					producers.refresh();
				}
			});
		}
		
		//----------------------------------
		//	Editor Handlers
		//----------------------------------
		
		/**
		 * Handler invocado quando o método userService.listProducers() é concluído
		 * com sucesso.
		 */
		private static function onEditorsLoaded(event:ResultEvent):void {
			editors.addAll(event.result as ArrayCollection);
			
			// Configurando o sort da coleção pelo nome do segmento
			var nameField:SortField = new SortField();
			nameField.name = "nickname";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			editors.sort = sort;
			editors.refresh();
			
			// Configura o monitoramento de mudanças feitas no CRUD de usuários e grupos
			groupService.subscribe(onChangeEditor);
			userService.subscribe(onChangeEditor);
		}
		
		/*
		* Handler que trata as mensagens recebidas que notificam mudanças CRUD
		* de segmentos por qualquer usuário.
		*/
		private static function onChangeEditor(event:MessageEvent):void {
			/**
			 * TODO Refatorar esse método para ele comportar comos os outros, ou
			 * seja, identificar a mudança que foi feita e atualizar a coleção
			 * sem ter que comunicar com o servidor.
			 */
			userService.listEditors(function(event:ResultEvent):void {
				if(editors)
					editors.removeAll();
				
				var _result:ArrayCollection = event.result as ArrayCollection;
				
				if(_result) {
					editors.addAll(_result);
					editors.refresh();
				}
			});
		}

        //----------------------------------
        //	Checklist Producer Handlers
        //----------------------------------

        /**
         * Handler invocado quando o método userService.listChecklistProducers() é concluído
         * com sucesso.
         */
        private static function onChecklistProducersLoaded(event:ResultEvent):void {
            checklistProducers.addAll(event.result as ArrayCollection);

            // Configurando o sort da coleção pelo nome do segmento
            var nameField:SortField = new SortField();
            nameField.name = "nickname";
            nameField.numeric = false;
            nameField.caseInsensitive = true;
            var sort:Sort = new Sort();
            sort.fields = [nameField];
            checklistProducers.sort = sort;
            checklistProducers.refresh();

            // Configura o monitoramento de mudanças feitas no CRUD de usuários e grupos
            groupService.subscribe(onChangeChecklistProducer);
            userService.subscribe(onChangeChecklistProducer);
        }

        /*
         * Handler que trata as mensagens recebidas que notificam mudanças CRUD
         * de segmentos por qualquer usuário.
         */
        private static function onChangeChecklistProducer(event:MessageEvent):void {
            /**
             * TODO Refatorar esse método para ele comportar comos os outros, ou
             * seja, identificar a mudança que foi feita e atualizar a coleção
             * sem ter que comunicar com o servidor.
             */
            userService.listChecklistProducers(function(event:ResultEvent):void {
                if(checklistProducers)
                    checklistProducers.removeAll();

                var _result:ArrayCollection = event.result as ArrayCollection;

                if(_result) {
                    checklistProducers.addAll(_result);
                    checklistProducers.refresh();
                }
            });
        }

		//----------------------------------
		//	Settings Handlers
		//----------------------------------

		/**
		 * Handler invocado quando o método SettingsService.listAll() é concluído
		 * com sucesso.
		 */
		private static function onSettingsLoaded(event:ResultEvent):void {
			settings.updateFields(event.result as Settings);
			settingsService.subscribe(onSettingsChanged);
		}

		/**
		 * Handler para as mensagens que notificam as novas configurações ativas
		 * do sistema após um usuário qualquer efetuar uma mudança.
		 */
		private static function onSettingsChanged(event:MessageEvent):void {
			settings.updateFields(event.message.body as Settings);
		}
		
		//----------------------------------
		//	StoryKind Handlers
		//----------------------------------
		
		/**
		 * Handler invocado quando o método storyKindsService.listAll() é concluído
		 * com sucesso.
		 */
		private static function onStoryKindsLoaded(event:ResultEvent):void {
			storyKinds.addAll(event.result as ArrayCollection);
			
			// Configurando o sort da coleção pelo nome do storykind
			var nameField:SortField = new SortField();
			nameField.name = "name";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			storyKinds.sort = sort;
			storyKinds.refresh();
			
			// Configura o monitoramento de mudanças feitas no CRUD de usuários e grupos
			storyKindsService.subscribe(onChangeStoryKinds);
		}
		
		/*
		* Handler que trata as mensagens recebidas que notificam mudanças CRUD
		* de storykinds
		*/
		private static function onChangeStoryKinds(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var aux:StoryKind, storyKind:StoryKind = StoryKind(event.message.body);
			
			switch (action) {
				case EntityAction.INSERTED:
					storyKinds.addItem(storyKind);
					storyKinds.refresh();
					break;
				case EntityAction.UPDATED:
					for each (aux in storyKinds) {
					if (aux.equals(storyKind)) {
						aux.updateFields(storyKind);
						storyKinds.refresh();
						break;
					}
				}
					break;
				case EntityAction.DELETED:
					for (var i: int = 0; i < storyKinds.length; i++) {
						aux = StoryKind(storyKinds.getItemAt(i));
						if (aux.equals(storyKind)) {
							storyKinds.removeItemAt(i);
							break;
						}
					}
					break;
			}
		}
	}
}
