package tv.snews.anews.utils {

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.utils.getTimer;
import flash.utils.setTimeout;

import mx.core.FlexGlobals;
import mx.events.FlexEvent;

import tv.snews.anews.event.UserActivityEvent;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class ActivityMonitor extends EventDispatcher {

	[Bindable] public var active:Boolean = true;
	[Bindable] public var inactive:Boolean = false;

	public var lastActivity:Number = 0;
	public var previousActivity:Number = 0;

	//------------------------------
	//	Constructor
	//------------------------------

	public function ActivityMonitor() {
		FlexGlobals.topLevelApplication.systemManager.addEventListener(FlexEvent.IDLE, onIdleHandler);
		FlexGlobals.topLevelApplication.systemManager.addEventListener(KeyboardEvent.KEY_DOWN, onActivityHandler);
		FlexGlobals.topLevelApplication.systemManager.addEventListener(MouseEvent.MOUSE_MOVE, onActivityHandler);
	}

	//------------------------------
	//	Operations
	//------------------------------

	private function registerActivity():void {
		previousActivity = lastActivity;
		lastActivity = getTimer();
	}

	//------------------------------
	//	Event Handlers
	//------------------------------

	private function onActivityHandler(event:Event):void {
		registerActivity();

		if (inactive) {
			active = !(inactive = false);
			dispatchEvent(new UserActivityEvent(UserActivityEvent.ACTIVATED));
		}
	}

	private function onIdleHandler(event:Event):void {
		if (active) {
			active = !(inactive = true);
			dispatchEvent(new UserActivityEvent(UserActivityEvent.DEACTIVATED));
		}
	}
}
}
