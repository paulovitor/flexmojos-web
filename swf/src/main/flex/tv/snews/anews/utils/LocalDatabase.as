package tv.snews.anews.utils {

	import flash.net.SharedObject;

	/**
	 * Faz o armazenamento e acesso de dados localmente na máquina do usuário
	 * por meio de um "flash.net.SharedObject".
	 *
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class LocalDatabase {

//		public static const GUIDELINE:String = "guideline";
		public static const PREVIEW:String = "preview";
//		public static const REPORTAGE:String = "reportage";
//		public static const STORY:String = "story";

		private static const INSTANCES:Object = new Object();

		private var sharedObject:SharedObject;

		public static function getInstance(label:String):LocalDatabase {
			if (!INSTANCES[label]) {
				INSTANCES[label] = new LocalDatabase(label);
			}
			return INSTANCES[label];
		}

		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------

		//----------------------------------
		//	label (read-only)
		//----------------------------------

		private var _label:String;

		/**
		 * Retorna o label que foi informado para a identificação deste objeto
		 * na sua criação.
		 */
		public function get label():String {
			return _label;
		}

		//----------------------------------
		//	data (read-only)
		//----------------------------------

		/**
		 * Retorna os dados armazenados por meio deste objeto, localmente.
		 */
		public function get data():Object {
			return sharedObject.data[label];
		}

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		/**
		 * Cria ou acessa um SharedObject identificado pelo label informado.
		 *
		 * @param label Nome de identificação do SharedObject.
		 */
		public function LocalDatabase(label:String /*, localPath:String = null, secure:Boolean = false*/) {

			// NÃO USE ESSE CONSTRUTOR! Use o método getInstance(String)!

			/*
			 * Esse construtor deveria ser private, mas o Action Script não
			 * permite. Por isso o controle abaixo é necessário.
			 */
			if (INSTANCES[label]) {
				throw new Error("Factory singleton failure. You should use the static method LocalData#getInstance(), not the constructor LocalData().");
			}

			_label = label;
			sharedObject = SharedObject.getLocal(label /*, localPath, secure*/);
		}

		//----------------------------------------------------------------------
		//	Methods
		//----------------------------------------------------------------------

		/**
		 * Salva os dados informados no storage local identificado pelo "label"
		 * informado na criação deste objeto. Quaisquer dados pré-existentes
		 * serão substituidos.
		 *
		 * @param data Dados que devem ser persistidos.
		 */
		public function save(data:Object):void {
			sharedObject.data[label] = data;
			sharedObject.flush();
		}

		/**
		 * Apaga por completo todos os dados armazendos no storage dentro do
		 * "label" informado na construção deste objeto.
		 */
		public function clear():void {
			/**
			 * FIXME Por algum motivo desconhecido esse método não limpa o cache
			 * em algumas situações. Já tentei salvar "null" antes, utilizar um
			 * "callLater" ou fazer um "flush", mas nenhuma dessas alternativas
			 * corrigiu o problema.
			 */
//			save(null);
			sharedObject.clear();
//			sharedObject.flush();
		}
	}
}
