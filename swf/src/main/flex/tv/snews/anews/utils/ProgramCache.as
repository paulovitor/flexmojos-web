package tv.snews.anews.utils {
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.messaging.events.MessageEvent;
	import mx.rpc.events.ResultEvent;
	
	import tv.snews.anews.domain.EntityAction;
	import tv.snews.anews.domain.Program;
	import tv.snews.anews.service.ProgramService;

	[Bindable]
	public class ProgramCache extends EventDispatcher {
		
		private const service:ProgramService = ProgramService.getInstance();
		
		private var _editors:Object = new Object();
        private var _imageEditors:Object = new Object();
        private var _presenters:Object = new Object();

		public function ProgramCache() {
			service.subscribe(onProgramsChanged);
		}
		
		//----------------------------------
		//	editors
		//----------------------------------
		
		[Bindable("change")]
		public function editors(program:Program):ArrayCollection {
			if (!_editors[program.id]) {
				_editors[program.id] = new ArrayCollection();
				configureSort(_editors[program.id]);
				service.listEditors(program.id, onListEditorsResult);
			}
			return _editors[program.id];
		}

        private function onListEditorsResult(event:ResultEvent, token:Object = null):void {
			_editors[token as int] = event.result as ArrayCollection;
			dispatchEvent(new Event("change"));
		}

        [Bindable("change")]
        public function imageEditors(program:Program):ArrayCollection {
            if (!_imageEditors[program.id]) {
                _imageEditors[program.id] = new ArrayCollection();
                configureSort(_imageEditors[program.id]);
                service.listImageEditors(program.id, onListImageEditorsResult)
            }
            return _imageEditors[program.id];
        }

        private function onListImageEditorsResult(event:ResultEvent, token:Object = null):void {
            _imageEditors[token as int] = event.result as ArrayCollection;
            dispatchEvent(new Event("change"));
        }

		//----------------------------------
		//	presenters
		//----------------------------------
		
		[Bindable("change")]
		public function presenters(program:Program):ArrayCollection {
			if (!_presenters[program.id]) {
				_presenters[program.id] = new ArrayCollection();
				configureSort(_presenters[program.id]);
				service.listPresenters(program.id, onListPresentersResult);
			}
			return _presenters[program.id];
		}
		
		private function onListPresentersResult(event:ResultEvent, token:Object = null):void {
			_presenters[token as int] = event.result as ArrayCollection;
			dispatchEvent(new Event("change"));
		}
		
		//----------------------------------
		//	notifier
		//----------------------------------
		
		private function onProgramsChanged(event:MessageEvent):void {
			var action:String = event.message.headers["action"];
			var program:Program = event.message.body as Program;
			var aux:ArrayCollection;
			switch (action) {
				case EntityAction.UPDATED:  {
					if (_editors[program.id]) {
						aux = _editors[program.id] as ArrayCollection;
						aux.removeAll();
						aux.addAll(program.editors);
						aux.refresh();
					}
                    if (_imageEditors[program.id]) {
                        aux = _imageEditors[program.id] as ArrayCollection;
                        aux.removeAll();
                        aux.addAll(program.imageEditors);
                        aux.refresh();
                    }
					if (_presenters[program.id]) {
						aux = _presenters[program.id] as ArrayCollection;
						aux.removeAll();
						aux.addAll(program.presenters);
						aux.refresh();
					}
					break;
				}
			}
		}
		
		private function configureSort(collection:ArrayCollection):void {
			var nameField:SortField = new SortField();
			nameField.name = "nickname";
			nameField.numeric = false;
			nameField.caseInsensitive = true;
			var sort:Sort = new Sort();
			sort.fields = [nameField];
			collection.sort = sort;
		}
    }
}