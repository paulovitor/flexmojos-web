import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.rpc.events.FaultEvent;

import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.TextInput;

import tv.snews.anews.component.TimeEditable;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.service.RundownService;
import tv.snews.flexlib.utils.DateUtil;

private const COLUMN_WIDTH:Number = 65;

private const dateUtil:DateUtil = new DateUtil(); //Contante de DateUtil para a formatação das horas.
private const TIMER_INTERVAL:int = 983; //Intervalo de um segundo para os timers.
private const TIMER_BASE_INTERVAL:int = 294900; //Inervalo de 5 minutos para a sincronia com servidor.

[Bindable] public var rundown:Rundown; //Espelho do jornal.

[Bindable] public var currentTimeStr:String = dateUtil.timeToString(dateUtil.timeZero()); //String formatada da hora atual. 
private var rundownService:RundownService = RundownService.getInstance(); //Seviço do espelho utilizado para sincronizar hora atual com servidor.
private var timer:Timer; //Timer que irá iniciar o relógio que irá controlar a hora atual.
private var timerBase:Timer; //Timer que irá controlar a atualização do sincronismo da hora atual com servidor.
private var currentTime:Date = null; //Atributo que irá atualizar a hora atual em tempo real.
private var updateTimeBaseCheck:Boolean = true; //Flag de controle para controlar a atualização do sincronismo da hora atual com servidor.
private var hoursCurrentTime:int = 0; //Controle para manipulação da hora da hora atual.
private var minutesCurrentTime:int = 0; //Controle para manipulação do minuto da hora atual.
private var secondsCurrentTime:int = 0; //Controle para manipulação do segundo da hora atual.

[Bindable] public var countdownDurationStr:String = dateUtil.timeToString(dateUtil.timeZero()); //String formatada da contagem regressiva da duração do jornal.
private var timerCountdownDuration:Timer; //Timer que irá iniciar o relógio que irá controlar a contagem regressiva da duração do jornal.
private var countdownDuration:Date = null; //Atributo que irá atualizar a contagem regressiva da duração do jornal em tempo real.
[Bindable] private var sinalOfTheCountdownDuration:Boolean = true; //Flag de controle de sinal da contagem regressiva do jornal.
[Bindable] private var controlCountdownDuration:Boolean = true;
private var hoursCountdownDuration:int = 0; //Controle para manipulação da hora atual.
private var minutesCountdownDuration:int = 0; //Controle para manipulação do minuto da hora atual.
private var secondsCountdownDuration:int = 0; //Controle para manipulação do segundo da hora atual.

private var oldValue:String;
[Bindable] private var endForecastStr:String = dateUtil.timeToString(dateUtil.timeZero()); //previsão de fim.

private function focusFieldIn(event:FocusEvent):void {
	oldValue = TimeEditable(event.currentTarget).editor.text;
}

private function dateOut(event:FocusEvent):void {
	var input:TextInput = TimeEditable(event.currentTarget).editor;
	var date:Date = dateUtil.toDate(input.text, "JJ:NN:SS");

	if (date == null) {
		input.text = oldValue as String;
		return;
	} else {
		input.text = dateUtil.timeToString(date);
	}

	if (input.text == oldValue) {
		return;
	}

	var field:String = event.currentTarget.id;
	rundownService.updateField(rundown.id, field, date, null, function(event:FaultEvent):void {
        input.text = oldValue;
        rundownService.faultHandler(event);
    });
}


private function startTimer():void {
	if (rundown != null) {
		//Atribuindo o state de onair.
		this.currentState = 'onair';

		//Inicializando valores de controles da hora atual.
		currentTime = null;
		updateTimeBaseCheck = true;
		hoursCurrentTime = 0;
		minutesCurrentTime = 0;
		secondsCurrentTime = 0;

		//Verificando tempo do jornal que irá ser regredido.
		if (rundown.total != null) {
			//Atribuindo tempo do jornal que irá ser regredido.
			countdownDuration = ObjectUtil.clone(rundown.total) as Date;

			//Inicializando valores de controles da contagem regressiva da duração do jornal.
			sinalOfTheCountdownDuration = true;
			controlCountdownDuration = true;
			endForecastStr = dateUtil.timeToString(dateUtil.timeZero());
			countdownDurationStr = dateUtil.timeToString(dateUtil.timeZero());
			hoursCountdownDuration = countdownDuration.getHours();
			minutesCountdownDuration = countdownDuration.getMinutes();
			secondsCountdownDuration = countdownDuration.getSeconds();
		}

		//Inicializando timer do controle da contagem regressiva da duração do jornal.
		if (rundown != null && rundown.total) {
			timerCountdownDuration = new Timer(TIMER_INTERVAL);
			timerCountdownDuration.addEventListener(TimerEvent.TIMER, updateTimerCountdownDuration);
			timerCountdownDuration.start();
		}

		//Inicializando timer do controle do sincronismo da hora atual com o servidor.
		timerBase = new Timer(TIMER_BASE_INTERVAL);
		timerBase.addEventListener(TimerEvent.TIMER, updateTimerBase);
		timerBase.start();

		//Inicializando timer do controle da hora atual.
		timer = new Timer(TIMER_INTERVAL);
		timer.addEventListener(TimerEvent.TIMER, updateTimer);
		timer.start();
	}
}

private function stopTimer():void {
	//Atribuindo o state de offair.
	this.currentState = 'offair';

	//Parando timer do controle da contagem regressiva da duração do jornal.
	if (timerCountdownDuration != null) {
		timerCountdownDuration.stop();
	}

	//Parando timer do controle da hora atual.
	if (timer != null) {
		timer.stop();
	}

	//Parando timer do controle do sincronismo da hora atual com o servidor.
	if (timerBase != null) {
		timerBase.stop();
	}
}

private function updateTimerBase(event:TimerEvent):void {
	//Flag de controle de sincronização da data atual com o servidor.
	updateTimeBaseCheck = true;
}

private function updateTimer(event:TimerEvent):void {
	//Verificação do controle de sincronização da data atual com o servidor.
	if (updateTimeBaseCheck) {
		rundownService.currentTime(function(event:ResultEvent):void {
			currentTime = event.result as Date
		});
		if (currentTime != null) {
			hoursCurrentTime = currentTime.getHours();
			minutesCurrentTime = currentTime.getMinutes();
			secondsCurrentTime = currentTime.getSeconds();
			updateTimeBaseCheck = false;
		}
	}

	//Algorítimo de controle do relógio da hora atual.
	if (currentTime != null) {
		if (secondsCurrentTime == 59) {
			if (minutesCurrentTime == 59) {
				minutesCurrentTime = 0;
				currentTime.setMinutes(minutesCurrentTime);
				if (hoursCurrentTime > 23) {
					hoursCurrentTime = 0;
					currentTime.setHours(hoursCurrentTime);
				} else {
					currentTime.setHours(++hoursCurrentTime);
				}
			} else {
				currentTime.setMinutes(++minutesCurrentTime);
			}
			secondsCurrentTime = 0;
			currentTime.setSeconds(secondsCurrentTime);
		} else {
			currentTime.setSeconds(++secondsCurrentTime);
		}

		// NOTE Comentado porque a função NO AR está desabilitada
//		if (currentTimeText != null) {
//			currentTimeStr = dateUtil.timeToString(currentTime);
//		}
	}
}

private function updateTimerCountdownDuration(event:TimerEvent):void {
	if (controlCountdownDuration) {
		//Algorítimo de controle da contagem regressiva da duração do jornal.
		if (secondsCountdownDuration == 0) {
			if (minutesCountdownDuration == 0) {
				minutesCountdownDuration = 59;
				countdownDuration.setMinutes(minutesCountdownDuration);
				if (hoursCountdownDuration == 0) {
					hoursCountdownDuration = 23;
					countdownDuration.setHours(hoursCountdownDuration);
				} else {
					countdownDuration.setHours(--hoursCountdownDuration);
				}
			} else {
				countdownDuration.setMinutes(--minutesCountdownDuration);
			}
			secondsCountdownDuration = 59;
			countdownDuration.setSeconds(secondsCountdownDuration);
		} else {
			countdownDuration.setSeconds(--secondsCountdownDuration);
		}

		//Atribui a previsão de fim ao label da interface.
		// NOTE Comentado porque a função NO AR está desabilitada
//		if (endForecastText != null && currentTime != null) {
//			endForecastStr = dateUtil.timeToString(dateUtil.sumTimes(currentTime, countdownDuration));
//		}

		//Verifica quando começar a contagem progressiva.
		if (hoursCountdownDuration == 0 && minutesCountdownDuration == 0 && secondsCountdownDuration == 0) {
			controlCountdownDuration = false;
		}
	} else {
		//Algorítimo de controle da contagem progressiva da duração do jornal após a negativação.
		if (secondsCountdownDuration == 59) {
			if (minutesCountdownDuration == 59) {
				minutesCountdownDuration = 0;
				countdownDuration.setMinutes(minutesCountdownDuration);
				if (hoursCountdownDuration > 23) {
					hoursCountdownDuration = 0;
					countdownDuration.setHours(hoursCountdownDuration);
				} else {
					countdownDuration.setHours(++hoursCountdownDuration);
				}
			} else {
				countdownDuration.setMinutes(++minutesCountdownDuration);
			}
			secondsCountdownDuration = 0;
			countdownDuration.setSeconds(secondsCountdownDuration);
		} else {
			countdownDuration.setSeconds(++secondsCountdownDuration);
		}

		//Verificação para a inverção de sinal para negativar a contagem regressiva.
		if (hoursCountdownDuration == 0 && minutesCountdownDuration == 0 && secondsCountdownDuration == 1) {
			sinalOfTheCountdownDuration = false;
		}
	}

	//Atribui a contagem regressiva ao label da interface.
	// NOTE Comentado porque a função NO AR está desabilitada
//	if (countdownDurationText != null) {
//		countdownDurationStr = dateUtil.timeToString(countdownDuration);
//	}
}

public function dispose():void {
	if (timerCountdownDuration) {
		timerCountdownDuration.removeEventListener(TimerEvent.TIMER, updateTimerCountdownDuration);
	}
	
	if (timerBase) {
		timerBase.removeEventListener(TimerEvent.TIMER, updateTimerBase);
	}
	
	if (timer) {
		timer.removeEventListener(TimerEvent.TIMER, updateTimer);
	}
}

//------------------------------
//	Helpers
//------------------------------
private function formatTimeStr(time:Date):String {
	return dateUtil.toString(time, this.resourceManager.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}

private function formatTimeDate(time:String):Date {
	return dateUtil.toDate(time, this.resourceManager.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}
