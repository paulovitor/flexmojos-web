package tv.snews.anews.view.reportage {

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.User;
import tv.snews.anews.utils.TabManager;

public class ReportageHelper {

    private const bundle:IResourceManager = ResourceManager.getInstance();

    public function reportageInEditing(user:User, tabManager:TabManager, key:String, area:String):void {
        if (user.equals(ANews.USER)) {
            if (tabManager.isTabOpened(key))
                tabManager.openTab(ReportageForm, key);
            else
                ANews.showInfo(bundle.getString("Bundle", "reportage.msg.areaLockedMessage", [ bundle.getString("Bundle", "defaults.you"), area ]), Info.WARNING);
        } else {
            ANews.showInfo(bundle.getString("Bundle", "reportage.msg.lockedMessage", [ user.nickname ]), Info.WARNING);
        }
    }

}
}
