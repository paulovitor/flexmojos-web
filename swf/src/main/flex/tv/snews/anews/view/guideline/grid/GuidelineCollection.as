package tv.snews.anews.view.guideline.grid {

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;
import mx.events.PropertyChangeEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.utils.ObjectUtil;

import spark.collections.Sort;

import tv.snews.anews.domain.CommunicationVehicle;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.User;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Style(name="groupBy", type="String", inherit="no", enumeration="classification,program,reporters,vehicles")]
public class GuidelineCollection {

	private static const bundle:IResourceManager = ResourceManager.getInstance();

	private static const NO_PROGRAM:String = bundle.getString('Bundle', 'defaults.drawerProgram');
	private static const NONE_REGEXP:RegExp = /^\([^\)]+\)$/;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public function GuidelineCollection() {
		this.groupBy = GuidelineGroup.PROGRAM;
	}

	//----------------------------------
	//  Properties
	//----------------------------------

	//
	//	source
	//

	private var _source:ArrayCollection = new ArrayCollection();

	[Bindable]
	[ArrayElementType("tv.snews.anews.domain.Guideline")]
	public function get source():ArrayCollection {
		return _source;
	}

	/**
	 * Coleção comum de pautas que será agrupada.
	 */
	public function set source(value:ArrayCollection):void {
		if (_source) {
			_source.removeEventListener(CollectionEvent.COLLECTION_CHANGE, sourceChangeHandler);
		}
		if (value) {
			value.addEventListener(CollectionEvent.COLLECTION_CHANGE, sourceChangeHandler);
		}
		_source = value;

		groupData();
	}

	//
	//	mode
	//

	private var _groupBy:String;

	[Bindable]
	public function get groupBy():String {
		return _groupBy;
	}

	/**
	 * Tipo de agrupamento que será utilizado, podendo ser "classification", "program", "vehicles", "reporter" ou "team".
	 */
	public function set groupBy(value:String):void {
		_groupBy = value || _groupBy; // evita valores nulos

		defineSort();
		groupData();
	}

	//
	//	data
	//

	[Bindable]
	[ArrayElementType("tv.snews.anews.view.guideline.grid.GuidelineGroup")]
	public var data:ArrayCollection = new ArrayCollection();

	//----------------------------------
	//  Event Handlers
	//----------------------------------

	/**
	 * As mudanças feitas na coleção "source" serão refletidas na coleção "data"
	 * por meio desse event handler.
	 */
	private function sourceChangeHandler(event:CollectionEvent):void {
		var guideline:Guideline = null;
		var groupChanged:Boolean = false;

		if (event.items.length != 0) {
			if (event.items[0] is Guideline) {
				guideline = Guideline(event.items[0]);
			}
			if (event.items[0] is PropertyChangeEvent) {
				var changeEvent:PropertyChangeEvent = PropertyChangeEvent(event.items[0]);

				// Verifica se mudou o programa, classificação ou veículos
				if (groupBy.indexOf(String(changeEvent.property)) != -1) {
					groupChanged = true;
				}

				guideline = Guideline(changeEvent.source);
			}
		}

		switch (event.kind) {
			case CollectionEventKind.ADD:
					addGuideline(guideline);
				break;
			case CollectionEventKind.REFRESH:
					sortData();
				break;
			case CollectionEventKind.REMOVE:
					removeGuideline(guideline);
				break;
			case CollectionEventKind.UPDATE:
					if (groupChanged) {
						removeGuideline(guideline);
						addGuideline(guideline);
					}
				break;
			default:
					groupData();
		}
	}

	private function addGuideline(guideline:Guideline):void {
		for each (var group:GuidelineGroup in findGroups(guideline)) {
			group.addItem(guideline);
		}
	}

	private function removeGuideline(guideline:Guideline):void {
		for each (var group:GuidelineGroup in data) {
			group.removeItem(guideline);
		}
		removeEmptyGroups();
	}

	//----------------------------------
	//  Helpers
	//----------------------------------

	private function groupData():void {
		data.removeAll();

		if (source && source.length != 0 && groupBy) {
			for each (var guideline:Guideline in source) {
				addGuideline(guideline);
			}
		}
	}

	private function defineSort():void {
		data.removeAll();
		data.sort = new Sort();

		if (groupBy == GuidelineGroup.PROGRAM) {
			data.sort.compareFunction = function(a:Object, b:Object, fields:Array=null):int {
				var labelA:String = GuidelineGroup(a).label;
				var labelB:String = GuidelineGroup(b).label;

				var result:int = ObjectUtil.stringCompare(labelA, labelB, false);

				// Garante que "Gaveta Geral", quando comparado com outro valor, ficarão no final da lista
				if (result != 0) {
					if (labelA.indexOf(NO_PROGRAM) != -1) return 1;
					if (labelB.indexOf(NO_PROGRAM) != -1) return -1;
				}

				return result;
			};
		} else {
			data.sort.compareFunction = function(a:Object, b:Object, fields:Array=null):int {
				var labelA:String = GuidelineGroup(a).label;
				var labelB:String = GuidelineGroup(b).label;

				var result:int = ObjectUtil.stringCompare(labelA, labelB, true);

				// Garante que "(nenhum)" ou "(nenhuma)", quando comparado com outro valor, ficarão no final da lista
				if (result != 0) {
					if (NONE_REGEXP.test(labelA)) return 1;
					if (NONE_REGEXP.test(labelB)) return -1;
				}

				return result;
			};
		}
	}

	private function sortData():void {
		data.refresh();
	}

	/**
	 * Procura pelos grupos correspondentes à guideline. No caso do "groupBy"
	 * valer "vehicles", poderá ser retornado mais de um grupo.
	 *
	 * Caso não existir algum grupo correspondente e "append" for "true", um ou
	 * mais grupos serão criados conforme necessário.
	 */
	private function findGroups(guideline:Guideline, append:Boolean=true):Array {
		var group:GuidelineGroup;

		var groups:Array;
		var found:Boolean;

		if (groupBy == GuidelineGroup.VEHICLE) {
			if (guideline.vehicles.length == 0) {
				for each (group in data) {
					if (group.match(guideline)) {
						return [ group ]; // grupo encontrado
					}
				}

				if (append) {
					group = new GuidelineGroup(groupBy);

					data.addItem(group);
					sortData();

					return [ group ]; // grupo "nenhum"
				} else {
					return []; // não encontrou e não criou
				}
			} else {
				// Deve criar um grupo para cada veículo da guideline
				groups = [];

				for each (var vehicle:String in guideline.vehicles) {
					found = false;

					for each (group in data) {
						if (group.label == CommunicationVehicle.findById(vehicle).name) {
							groups.push(group);
							found = true;
							break;
						}
					}

					if (!found && append) {
						group = new GuidelineGroup(groupBy, vehicle);
						groups.push(group);

						data.addItem(group);
						sortData();
					}
				} // end for

				return groups; // grupos encontrados e/ou criados
			}
		} else if (groupBy == GuidelineGroup.REPORTER) {
			if (guideline.reporters == null || guideline.reporters.length == 0) {
				for each (group in data) {
					if (group.match(guideline)) {
						return [ group ]; // grupo encontrado
					}
				}

				if (append) {
					group = new GuidelineGroup(groupBy);

					data.addItem(group);
					sortData();

					return [ group ]; // grupo "nenhum"
				} else {
					return []; // não encontrou e não criou
				}
			} else {
				// Deve criar um grupo para cada repórter da guideline
				groups = [];

				for each (var reporter:User in guideline.reporters) {
					found = false;
					for each (group in data) {
						if (group.label == reporter.nickname) {
							groups.push(group);
							found = true;
							break;
						}
					}

					if (!found && append) {
						group = new GuidelineGroup(groupBy, reporter);
						groups.push(group);

						data.addItem(group);
						sortData();
					}
				} // end for

				return groups; // grupos encontrados e/ou criados
			}
		} else {
			// Para o caso de classificação e programa, vai criar no máximo um grupo

			for each (group in data) {
				if (group.match(guideline)) {
					return [ group ]; // grupo encontrado
				}
			}

			if (append) {
				switch (groupBy) {
					case GuidelineGroup.CLASSIFICATION:
						group = new GuidelineGroup(groupBy, guideline.classification);
						break;
					case GuidelineGroup.PROGRAM:
						group = new GuidelineGroup(groupBy, guideline.program);
						break;
                    case GuidelineGroup.TEAM:
                        group = new GuidelineGroup(groupBy, guideline.team);
                        break;
					default:
						throw new Error("Unknown group mode");
				}

				data.addItem(group);
				sortData();

				return [ group ]; // grupo criado
			}

			return []; // não encontrou nada e nada foi criado
		} // end else
	}

	private function removeEmptyGroups():void {
		for (var i:int = data.length - 1; i > -1; i--) {
			var group:GuidelineGroup = GuidelineGroup(data.getItemAt(i));
			if (group.data.length == 0) {
				data.removeItemAt(i);
			}
		}
	}
}
}
