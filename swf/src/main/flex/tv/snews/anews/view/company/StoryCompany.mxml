<?xml version="1.0" encoding="utf-8"?>
<s:NavigatorContent xmlns:fx="http://ns.adobe.com/mxml/2009" 
					xmlns:s="library://ns.adobe.com/flex/spark"
					xmlns:mx="library://ns.adobe.com/flex/mx" 
					xmlns:sn="tv.snews.flexlib.components.*"
					xmlns:an="tv.snews.anews.component.*" 
					xmlns:story="tv.snews.anews.view.rundown.story.*"
					creationComplete="onCreationComplete(event)"
					width="100%" height="100%" label="{resourceManager.getString('Bundle' , 'story.title')}">
	<fx:Script>
		<![CDATA[
			import mx.collections.ArrayCollection;
			import mx.events.CalendarLayoutChangeEvent;
			import mx.events.FlexEvent;
			import mx.messaging.events.MessageEvent;
			import mx.resources.*;
			import mx.rpc.events.*;

			import spark.components.TextInput;
			import spark.components.gridClasses.GridColumn;
			import spark.events.*;
			
			import tv.snews.anews.component.Info;
			import tv.snews.anews.domain.*;
			import tv.snews.anews.flex.PageResult;
			import tv.snews.anews.service.*;
			import tv.snews.anews.utils.*;
			import tv.snews.flexlib.utils.*;

			private const bundle:IResourceManager = ResourceManager.getInstance();
			private const dateTimeUtil:DateUtil = new DateUtil();

			private const companyService:CompanyService = CompanyService.getInstance();
			private const localStoryService:StoryService = StoryService.getInstance();

			// Parâmetros da busca
            [Bindable] private var slug:String;
			[Bindable] private var content:String;
			[Bindable] private var program:Program;
			[Bindable] private var start:Date;
			[Bindable] private var end:Date;
			[Bindable] private var searchType:String;
			[Bindable] private var textToIgnore:String;

			[Bindable] private var connected:Boolean = false;
			[Bindable] private var showViewer:Boolean = false;

			[Bindable] private var programs:ArrayCollection = new ArrayCollection();

			private var programService:ProgramService;
			private var storyService:StoryService;
			private var reportUtil:ReportsUtil;

			private var company:Company;

			//------------------------------------------------------------------
			//	Properties
			//------------------------------------------------------------------

			//--------------------------
			//	allPrograms
			//--------------------------

			private var _allPrograms:Program;

			private function get allPrograms():Program {
				if (!_allPrograms) {
					_allPrograms = new Program();
					_allPrograms.id = -1;
					_allPrograms.name = bundle.getString('Bundle', 'defaults.all');
				}
				return _allPrograms;
			}

			//--------------------------
			//	selectedStory
			//--------------------------

			private var _selectedStory:Story = null;

			[Bindable]
			private function set selectedStory(value:Story):void {
				_selectedStory = value;
				this.currentState = value ? "selected" : "normal";
			}

			private function get selectedStory():Story {
				return _selectedStory;
			}

			//------------------------------------------------------------------
			//	Event Handlers
			//------------------------------------------------------------------

			private function onCreationComplete(event:FlexEvent):void {
				companyService.subscribe(
						function(event:MessageEvent):void {
							onCompanyChange();
						}
				);
			}

			private function search(page:int = 1):void {
				page = page < 1 ? 1 : page;
                slug = StringUtils.trim(slug || "");
				content = StringUtils.trim(content || "");
				textToIgnore = StringUtils.trim(textToIgnore || "");

				selectedStory = null;
				showViewer = false;

				storyService.storiesByFullTextSearch(slug, content, program, start, end, page, searchType, textToIgnore, onListSuccess, onListFail);
			}

            //Verifica se os dados do paginador existem, caso contrário, não permite a troca de páginas
            private function onPageChange(page:int=1):void {
                if(pgStoryArchive.data && pgStoryArchive.data.length > 0)search(page);
            }

			private function onListSuccess(event:ResultEvent):void {
				var result:PageResult = event.result as PageResult;

				for each (var story:Story in result.results) {
					if (story.slug == "") {
						story.slug = bundle.getString("Bundle", "defaults.noSlug");
					}
				}

				pgStoryArchive.load(result.results, result.pageSize, result.total, result.pageNumber);
			}

			private function onListFail(event:FaultEvent):void {
				ANews.showInfo(bundle.getString('Bundle', 'story.archive.msg.listFailed'), Info.ERROR);
			}

			private function onCopyRequest(event:MouseEvent):void {
				this.currentState = "copy";
			}

			private function onCopy(event:MouseEvent):void {
				var destinyDate:Date = destinyDateField.selectedDate;
				var destinyProgram:Program = destinyProgramCombo.selectedItem as Program;

				if (destinyDate && destinyProgram && selectedStory) {
					// Recarrega para retirar marcações da busca
					storyService.remoteCopyRequest(selectedStory.id, ANews.USER.nickname, DomainCache.settings.name,
							function(event:ResultEvent):void {
								var remoteStory:Story = Story(event.result);

								// Copia a lauda remota para uma local
								localStoryService.copyRemoteStory(remoteStory, destinyDate, destinyProgram,
										function(event:ResultEvent):void {
											var resultCode:String = event.result as String;

											switch (resultCode) {
												case "OK":
													ANews.showInfo(bundle.getString("Bundle", "rundown.trash.restoreSuccess"), Info.INFO);
													reloadData();
													this.currentState = "selected";
													break;
												case "RUNDOWN_NOT_FOUND":
													ANews.showInfo(bundle.getString("Bundle", "rundown.trash.noRundownError"), Info.WARNING);
													break;
											}
										}
								);
							}
					);
				} else {
					ANews.showInfo(bundle.getString("Bundle", "defaults.msg.requiredFields"), Info.INFO);
				}
			}

			private function onCopyCancel(event:MouseEvent):void {
				this.currentState = "selected";
			}

			private function onPrint(event:MouseEvent):void {
				reportUtil.generate(ReportsUtil.STORY, {ids: selectedStory.id});
			}

			private function onPeriodChange(event:CalendarLayoutChangeEvent):void {
				if (periodIsInvalid()) {
					periodStart.errorString = bundle.getString('Bundle', 'story.archive.msg.invalidPeriod');
					periodEnd.errorString = bundle.getString('Bundle', 'story.archive.msg.invalidPeriod');
				} else {
					periodStart.errorString = null;
					periodEnd.errorString = null;
				}
			}

			private function onSearch():void {
				// Carrega os parâmetros da busca
                slug = slugField.text;
				content = searchText.text;
				program = programCombo.selectedItem as Program;
				start = periodStart.selectedDate;
				end = periodEnd.selectedDate;
				textToIgnore = ignoreField.text;
				searchType = cbSearchType.selectedItem.id;
				
				if (periodIsInvalid()) {
					ANews.showInfo(bundle.getString('Bundle', 'story.archive.msg.invalidPeriod'), Info.WARNING);
				} else {
					reloadData();
				}
				
			}

            private function onSearchKeyDown(event:KeyboardEvent):void {
                if (event.keyCode == Keyboard.ENTER) {
                    onSearch();
                }
            }

			private function onClear(event:MouseEvent):void {
				// Limpa os campos
                slugField.text = "";
				searchText.text = "";
				programCombo.selectedIndex = 0;
				periodStart.selectedDate = null;
				periodEnd.selectedDate = null;
				ignoreField.text = "";
				
				// Limpa os parâmetros de busca
                slug = null;
				content = null;
				program = null;
				start = end = null;
				periodStart.errorString = null;
				periodEnd.errorString = null;
				selectedStory = null;
				cbSearchType.selectedIndex = 0;
				programCombo.selectedIndex = -1;
				pgStoryArchive.load(null, 0,0,0);
			}

			private function onSelectionChangeHandler(event:GridSelectionEvent):void {
				selectedStory = storiesGrid.selectedItem as Story;
			}

			protected function onChangeSearchText(event:TextOperationEvent):void {
				var obj:TextInput = event.target as TextInput;
				if (obj.text.length == 0) {
					obj.errorString = "";
				}
			}

			private function onViewClick(event:MouseEvent):void {
				showViewer = true;
			}

			private function onCancelClick(event:MouseEvent):void {
				showViewer = false;
			}

			//------------------------------------------------------------------
			//	Helper Methods
			//------------------------------------------------------------------

			private function reloadData():void {
				selectedStory = null;
				search();
			}

			private function dateLabelFunction(item:Object, column:GridColumn):String {
				return dateTimeUtil.dateToString((item as Story).block.rundown.date);
			}

			private function periodIsInvalid():Boolean {
				var start:Date = periodStart.selectedDate;
				var end:Date = periodEnd.selectedDate;
				return start && end && start.time > end.time;
			}

			private function onCompanyChange(event:IndexChangeEvent=null):void {
				clearSearchResults();

				if (event == null || event.newIndex == -1) {
					cbCompanies.selectedIndex = -1;
					connected = false;
				} else {
					company = Company(cbCompanies.selectedItem);
					if (company.online) {
						company.connect(onCompanyConnect);
					} else {
						this.currentState = "offline";
					}
				}
			}

			private function onCompanyConnect():void {
				// Cria os serviços que carregam os dados da praça
				programService = ProgramService.getInstance(company.host, company.port);
				storyService = StoryService.getInstance(company.host, company.port);
				reportUtil = new ReportsUtil(company.host, company.port);

				// Carrega a lista de programas
				programService.listAll(
						function (event:ResultEvent):void {
							programs = ArrayCollection(event.result);
							programCombo.selectedIndex = -1;
							programCombo.selectedItem = null;
						}
				);
				// Muda o state
				connected = true;
			}

			private function clearSearchResults():void {
				pgStoryArchive.load(new ArrayCollection(), 1, 0, 1);
			}
			
			protected function onChangeInitialDate(event:CalendarLayoutChangeEvent):void {
				if (periodStart.selectedDate && periodEnd.selectedDate) {
					var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
					if (val > 0) {
						periodEnd.selectedDate = periodStart.selectedDate;
					} 
				}
			}
			
			protected function onChangeFinalDate(event:CalendarLayoutChangeEvent):void {
				if (periodStart.selectedDate && periodEnd.selectedDate) {
					var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
					if (val > 0 ) {
						periodStart.selectedDate = periodEnd.selectedDate;
					}
				}
			}
			
			protected function onChangeProgramCombo(event:IndexChangeEvent):void {
				if (event.target is ComboBox) {
					var cb:ComboBox = event.target as ComboBox;
					if (cb.selectedItem is String) {
						cb.textInput.text = "";
						cb.selectedItem = null;
						cb.selectedIndex = -1;
					}
				}
			}
			
			protected function onKeyDown(event:KeyboardEvent):void {
				if (event.keyCode == Keyboard.ENTER && storiesGrid.selectedItem != null && storiesGrid.selectedIndex != -1) {
					if (showViewer == true) {
						showViewer = false;
					} else {
						showViewer = true;
					}
				}
			}
        ]]>
	</fx:Script>
	
	
	<fx:Style>
		.story_slug	{ fontSize: 20; fontWeight: bold; typographicCase: uppercase; }
	</fx:Style>
	
	<fx:Declarations>
		<fx:Array id="validTextSearch">
			<an:TextSearchValidator id="validText" source="{searchText}" property="text"
									required="true" trigger="{searchText}" triggerEvent="change" minLength="2" />
			
			<s:NumberValidator source="{cbSearchType}" lessThanMinError="{resourceManager.getString('Bundle', 'defaults.msg.requiredFields')}"
							   minValue="0" property="selectedIndex" required="true" trigger="{cbSearchType}"
							   triggerEvent="change"  />
		</fx:Array>
	</fx:Declarations>
	
	<s:states>
		<s:State name="normal"/>
		<s:State name="offline"/>
		<s:State name="selected" stateGroups="selectedStates"/>
		<s:State name="copy" stateGroups="selectedStates"/>
	</s:states>
	
	<s:layout>
		<s:VerticalLayout paddingBottom="5" paddingTop="4"/>
	</s:layout>
	
	<an:HorizontalLine height="3" color="{DomainCache.settings.defaultColor}"/>
	
	<!-- __________ CABEÇALHO DA ÁREA __________ -->
	
	<s:HGroup width="100%" verticalAlign="middle" paddingLeft="10" paddingRight="10">
		<s:Label text="{resourceManager.getString('Bundle', 'story.title2')}" styleName="title"
				 width="100%" paddingTop="5" paddingBottom="5"/>
		
		<s:Button label="{resourceManager.getString('Bundle', 'defaults.btn.viewer')}" useHandCursor="true"
				  buttonMode="true" click="onViewClick(event)" includeIn="selected"/>
		<s:Button label="{bundle.getString('Bundle', 'defaults.btn.copy')}" click="onCopyRequest(event)" includeIn="selectedStates"
				  visible="{ANews.USER.allow('010703')}" includeInLayout="{ANews.USER.allow('010703')}"/>
		<s:Button label="{bundle.getString('Bundle', 'defaults.btn.print')}" click="onPrint(event)" includeIn="selectedStates"/>
	</s:HGroup>
	
	<an:HorizontalLine color="#E2E2E2"/>
	
	<!-- __________ Alerta de servidor offline __________ -->
	<s:Group id="serverOffline" width="100%" includeIn="offline">
		
		<!-- Esse Rect faz um retângulo de cantos arredondados com um background e borda em tóns de vermelho -->
		<s:Rect radiusX="5" radiusY="5" top="0" bottom="0" left="10" right="10">
			<s:stroke>
				<s:SolidColorStroke color="#EDD3D7"/>
			</s:stroke>
			<s:fill>
				<s:SolidColor color="#F1DEDE"/>
			</s:fill>
		</s:Rect>
		
		<!-- Mensagem de alerta -->
		<s:Label color="#B74D46" fontSize="13" fontWeight="bold" lineHeight="17"
				 top="5" bottom="5" right="25" left="25"
				 text="{resourceManager.getString('Bundle', 'company.msg.hostOffline')}"/>
	</s:Group>
	
	<!-- __________ COPIAR PARA __________ -->
	
	<s:Group includeIn="copy" width="100%" height="30">
		<s:Rect left="0" right="0" bottom="0" top="0">
			<s:fill>
				<s:SolidColor color="{DomainCache.settings.colorRundown}"/>
			</s:fill>
		</s:Rect>

		<s:HGroup id="copyToRundown" width="100%" verticalAlign="middle" paddingLeft="10" paddingRight="10"
				  paddingTop="4" paddingBottom="4">

			<an:FormLabel color="0xFFFFFF" fontWeight="bold" text="{bundle.getString('Bundle', 'defaults.btn.copy')}"/>
			<mx:DateField id="destinyDateField" yearNavigationEnabled="true" selectedDate="{new Date()}"/>

			<an:FormLabel color="0xFFFFFF" fontWeight="bold" text="{resourceManager.getString('Bundle', 'story.program')}"/>
			<s:ComboBox id="destinyProgramCombo" dataProvider="{DomainCache.programs}" width="150"
						prompt="{resourceManager.getString('Bundle', 'defaults.selectPrompt')}" labelField="name"
						requireSelection="true"/>

			<s:Spacer width="100%"/>

			<s:Button label="{resourceManager.getString('Bundle', 'defaults.btn.confirm')}" click="onCopy(event)"/>
			<s:Button label="{resourceManager.getString('Bundle', 'defaults.btn.cancel')}" click="onCopyCancel(event)"/>
		</s:HGroup>
	</s:Group>
	
	<mx:VDividedBox width="100%" height="100%">
		<s:HGroup width="100%" height="100%" minHeight="180" paddingLeft="10" paddingRight="10">
            <!-- __________ FILTRO DE RESULTADOS __________ -->
			<s:Scroller height="100%">
				<s:VGroup paddingLeft="5" paddingRight="5" paddingBottom="5" height="100%" minHeight="90" keyDown="onSearchKeyDown(event)">
					<an:FormLabel text="{resourceManager.getString('Bundle', 'company.companies')}" verticalAlign="middle" />
                    <s:DropDownList  id="cbCompanies" width="200" dataProvider="{DomainCache.companies}" 
									 prompt="{resourceManager.getString('Bundle', 'defaults.selectPrompt')}" 
									 labelField="name" change="onCompanyChange(event)"
									 itemRenderer="tv.snews.anews.view.company.CompanyDropDownListRenderer"/>

                    <s:VGroup paddingBottom="5" height="100%" minHeight="90" includeInLayout="{connected}" visible="{connected}">
						<s:HGroup width="100%" verticalAlign="bottom">
							<s:VGroup width="100%" height="35" gap="0" verticalAlign="bottom">
								<an:FormLabel text="{resourceManager.getString('Bundle', 'guideline.archive.searchStartDate')}" styleName="formLabel"/>
								<mx:DateField id="periodStart" yearNavigationEnabled="true" change="onChangeInitialDate(event)" width="50%"/>
							</s:VGroup>
							<s:Button icon="@Embed('/assets/glyphicons_199_ban_2.png')"  buttonMode="true" useHandCursor="true"
									  skinClass="tv.snews.anews.skin.button.CleanButtonSkin" bottom="2"
									  click="{periodStart.selectedDate = null}" width="16" height="16"  />
							
							<s:VGroup width="100%" height="35" gap="0" verticalAlign="middle">
								<an:FormLabel text="{resourceManager.getString('Bundle', 'guideline.archive.searchEndDate')}" styleName="formLabel"/>
								<mx:DateField id="periodEnd" yearNavigationEnabled="true" change="onChangeFinalDate(event)" width="50%" />
							</s:VGroup>
							<s:Button icon="@Embed('/assets/glyphicons_199_ban_2.png')"  buttonMode="true" useHandCursor="true"
									  skinClass="tv.snews.anews.skin.button.CleanButtonSkin" bottom="2"
									  click="{periodEnd.selectedDate = null}" width="16" height="16" />
						</s:HGroup>

                        <!-- PROGRAMA -->
                        <s:Label text="{resourceManager.getString('Bundle', 'guideline.chooseProgram')}" styleName="formLabel"/>
						<s:ComboBox id="programCombo" labelField="name" dataProvider="{programs}" change="onChangeProgramCombo(event)"
									width="100%" prompt="{resourceManager.getString('Bundle', 'defaults.selectPrompt')}"
									requireSelection="false"/>

                        <s:Label text="{resourceManager.getString('Bundle', 'story.slug')}" styleName="formLabel"/>
                        <s:TextInput width="100%" height="25" id="slugField"/>

                        <an:FormLabel text="{resourceManager.getString('Bundle', 'guideline.archive.text')}" styleName="formLabel"/>
                        <s:TextInput id="searchText" width="100%"  change="onChangeSearchText(event)"/>
                        <s:DropDownList id="cbSearchType" dataProvider="{SearchType.values()}" width="100%" labelField="name" requireSelection="true"
                                        prompt="{resourceManager.getString('Bundle', 'defaults.selectPrompt')}"/>

                        <s:Label text="{resourceManager.getString('Bundle', 'guideline.archive.ignore')}" styleName="formLabel"/>
                        <s:TextInput width="100%" height="25" id="ignoreField" enabled="{searchText.text != ''}"/>

                        <s:HGroup width="100%" horizontalAlign="right" paddingTop="5">
                            <s:Button label="{bundle.getString('Bundle', 'defaults.btn.clearFilter')}" click="onClear(event)"/>
                            <s:Button label="{bundle.getString('Bundle', 'defaults.btn.search')}" click="onSearch()"/>
                        </s:HGroup>
                    </s:VGroup>
                </s:VGroup>
			</s:Scroller>
			
			<an:VerticalLine color="#E2E2E2"/>
			
			<s:VGroup height="100%" width="100%">
				<!-- __________ LISTA DE RESULTADOS __________ -->
				<s:DataGrid id="storiesGrid" width="100%" height="100%" horizontalScrollPolicy="off" dataProvider="{pgStoryArchive.data}"
							selectionColor="{DomainCache.settings.colorRundown}" rollOverColor="#C1DEF2" alternatingRowColors="[#FFFFFF, #F1F2F2]"  
							caretColor="#FFFFFF" selectionChange="onSelectionChangeHandler(event)" keyDown="onKeyDown(event)">
					<s:columns>
						<s:ArrayList>
							<s:GridColumn headerText="{bundle.getString('Bundle', 'story.archive.label.slug')}" dataField="slug" itemRenderer="tv.snews.anews.view.guideline.daily.TextFlowColumnRenderer"/>
							<s:GridColumn headerText="{bundle.getString('Bundle', 'story.archive.label.program')}" dataField="block.rundown.program.name" width="180" itemRenderer="tv.snews.anews.view.guideline.daily.TextFlowColumnRenderer" />
							<s:GridColumn headerText="{bundle.getString('Bundle', 'story.archive.label.date')}" dataField="block.rundown.date" width="80" labelFunction="dateLabelFunction"/>
						</s:ArrayList>
					</s:columns>
				</s:DataGrid>
				
				<s:Group width="100%" height="32">
					<s:Rect top="0" right="0" left="0" bottom="0">
						<s:stroke>
							<s:SolidColorStroke color="0xA7A9AC"/>
						</s:stroke>
					</s:Rect>
					<s:Rect top="1" right="1" left="1" bottom="1">
						<s:fill>
							<s:LinearGradient rotation="90">
								<s:GradientEntry color="0xFFFFFF" alpha="0.85"/>
								<s:GradientEntry color="0xD8D8D8" alpha="0.85"/>
							</s:LinearGradient>
						</s:fill>
					</s:Rect>
					
					<sn:PageBar id="pgStoryArchive" width="100%" height="100%" pageChange="onPageChange(event.page)" />
				</s:Group>
			</s:VGroup>
		</s:HGroup>
		
		<!-- __________ VISUALIZADOR DA SELECIONADA __________ -->
		
		<s:VGroup width="100%" height="100%" minHeight="150" includeInLayout="{showViewer}" visible="{showViewer}">
			<an:HorizontalLine height="3" color="{DomainCache.settings.colorRundown}" />
			
			<s:HGroup width="100%" paddingRight="10" verticalAlign="middle">
				<s:Label text="{bundle.getString('Bundle', 'story.archive.viewerTitle')}" styleName="title"
						 paddingTop="5" paddingBottom="5" paddingLeft="10" width="100%"/>
				<s:Button label="{resourceManager.getString('Bundle', 'defaults.btn.hidden')}" click="onCancelClick(event)" />
			</s:HGroup>
			
			<an:HorizontalLine color="0xE2E2E2"/>
			
			<story:StoryViewer story="{selectedStory}" width="100%" height="100%"/>
		</s:VGroup>
	</mx:VDividedBox>
</s:NavigatorContent>
