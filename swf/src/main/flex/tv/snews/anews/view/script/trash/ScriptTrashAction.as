import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import flash.utils.setTimeout;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import spark.components.gridClasses.GridColumn;
import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Script;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const bundle:IResourceManager = ResourceManager.getInstance();

private const scriptService:ScriptService = ScriptService.getInstance();

[Bindable] private var selectedScript:Script;


private const dateUtil:DateUtil = new DateUtil();

//------------------------------------------------------------------
//	Event Handlers
//------------------------------------------------------------------


private function onContentCreationComplete(event:FlexEvent):void {
    loadExcludedScripts();
    scriptService.subscribe(onScriptMessageReceived);
}

private function onRestoreClick(event:MouseEvent):void {
    restoreTo.visible = true;
    restoreTo.includeInLayout = true;
}

private function onConfirmRestoreClick():void {
    var script:Script = excludedGrid.selectedItem as Script;
    var date:Date = destinyDay.selectedDate;
    var program:Program = destinyProgram.selectedItem as Program;

    if (date && program) {
        scriptService.restoreExcludedScript(script.id, date, program.id, function(event:ResultEvent):void {
            var resultCode:String = event.result as String;
            var resourceManager:IResourceManager = ResourceManager.getInstance();

            switch (resultCode) {
                case "OK":
                //case "CREDITS_REMOVED":
                    ANews.showInfo(resourceManager.getString("Bundle", "script.trash.restoreSuccess"), Info.INFO);
                    loadExcludedScripts();
                    restoreTo.visible = false;
                    restoreTo.includeInLayout = false;

                    //if(resultCode == "CREDITS_REMOVED") {
                        //ANews.showInfo(resourceManager.getString('Bundle', 'scriptplan.block.msg.creditsDeleted'), Info.WARNING);
                    //}
                    break;
                case "SCRIPT_PLAN_NOT_FOUND":
                    ANews.showInfo(resourceManager.getString("Bundle", "script.trash.noScriptAreaError"), Info.WARNING);
                    break;
            }
        });
    } else {
        ANews.showInfo(this.resourceManager.getString("Bundle", "defaults.msg.requiredFields"), Info.WARNING); //bug do intellij
    }
}

private function onCancelRestoreClick():void {
    restoreTo.visible = false;
    restoreTo.includeInLayout = false;
}

private function onSelectionChange(event:GridSelectionEvent):void {
    selectedScript = excludedGrid.selectedItem as Script;
    if (selectedScript) {
        scriptService.loadById(selectedScript.id, onLoadScript);
    } else {
        this.currentState = "normal";
    }
}

private function onLoadScript(event:ResultEvent):void {
    selectedScript = Script(event.result);
    this.currentState = "selected";
}

protected function onKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER && excludedGrid.selectedItem != null && excludedGrid.selectedIndex != -1) {
        if (this.currentState == "selected") {
            this.currentState = "normal";
        } else {
            this.currentState = "selected";
        }
    }
}


private function search(page:int = 1):void {
    page = page < 1 ? 1 : page;
    //Caso nenhuma lauda esteja nedo restaurada, permite a troca de páginas
    if(!restoreTo.visible) {
        selectedScript = null;
        this.currentState = "normal";
        scriptService.listExcludedScripts(page, onListSuccess);
    }
}

private function onListSuccess(event:ResultEvent):void {
    var result:PageResult = event.result as PageResult;

    // Corrige scripts sem retranca
    var scripts:ArrayCollection = result.results;
    var noSlug:String = '[ ' + ResourceManager.getInstance().getString("Bundle", "defaults.noSlug") + ' ]';
    for each (var script:Script in scripts) {
        if (StringUtils.isNullOrEmpty(script.slug)) {
        }
    }

    pgScriptTrash.load(scripts, result.pageSize, result.total, result.pageNumber);
}

private function onScriptMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    if (action == EntityAction.DELETED || action == EntityAction.RESTORED) {
        setTimeout(loadExcludedScripts, 1000);
    }
}


//------------------------------------------------------------------
//	Helper Methods
//------------------------------------------------------------------

private function loadExcludedScripts():void {
    this.currentState = "normal";
    search();
}

private function excludedLabelFunction(item:Object, column:GridColumn):String {
    var script:Script = item as Script;
    var authorNickname:String = script.author ? script.author.nickname : "";
    return authorNickname + " (" + dateUtil.fullDateToString(script.changeDate, true) + ")";
}