package tv.snews.anews.view.chat {

	import mx.collections.ArrayCollection;
	import mx.core.IToolTip;
	
	import spark.components.Group;
	import spark.components.supportClasses.SkinnableComponent;
	
	import tv.snews.anews.domain.User;

	public class ToolTipChat extends SkinnableComponent implements IToolTip {

		[SkinPart]
		public var contentGroup:Group;

		private var _users:ArrayCollection;

		public function ToolTipChat() {
			super();
			setStyle("skinClass", ToolTipChatSkin);
		}

		public function get text():String {
			return null;
		}

		public function set text(value:String):void {
		}

		public function get users():ArrayCollection {
			return _users;
		}

		[Bindable] public function set users(value:ArrayCollection):void {
			var users:ArrayCollection = new ArrayCollection();
			for each (var member:User in value) {
				if (member.id != ANews.USER.id) {
					users.addItem(member);
				}
			}
			_users = users;
		}
	}
}
