import flash.display.DisplayObject;
import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.events.MenuEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ButtonBarButton;
import spark.events.IndexChangeEvent;

import tv.snews.anews.component.ChangesHistoryWindow;
import tv.snews.anews.component.FormScrollManager;
import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.agency.news.NewsViewerComponent;
import tv.snews.anews.view.guideline.GuidelineViewer;
import tv.snews.anews.view.reportage.ReportageViewer;
import tv.snews.anews.view.rundown.story.StoryRevisionRenderer;
import tv.snews.anews.view.rundown.story.StoryViewer;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

[Bindable] public var tabManager:TabManager;
[Bindable] public var storyOld:Story;
[Bindable] public var storyKinds:ArrayCollection = new ArrayCollection();

private var _story:Story;
private var _informationCollapsed:Boolean = false;
private var _editionAreaMode:Boolean = false;
private var _editable:Boolean = true;

[ArrayElementType("tv.snews.anews.domain.Story")]
[Bindable] private var revisions:ArrayCollection = new ArrayCollection();
[Bindable] private var modify:String;

private var scrollManager:FormScrollManager;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();
private const dateUtil:DateUtil = new DateUtil();

private const intelligentInterfaceService:IntelligentInterfaceService = IntelligentInterfaceService.getInstance();
private const storyService:StoryService = StoryService.getInstance();
private const rundownService:RundownService = RundownService.getInstance();

private static const SEE_ORIGINAL:String        = ResourceManager.getInstance().getString('Bundle', 'story.seeSourceStory');
private static const SEE_REPORTAGE_LABEL:String = ResourceManager.getInstance().getString('Bundle', 'story.seeReportage');
private static const SEE_GUIDELINE_LABEL:String = ResourceManager.getInstance().getString('Bundle', 'story.seeGuideline');
private static const SEE_NEWS_LABEL:String 		= ResourceManager.getInstance().getString('Bundle', 'story.seeNews');
private static const OK_LABEL:String			= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.ok');
private static const APPROVE_LABEL:String		= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.approve');
private static const PRINT_LABEL:String			= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.print');
private static const PRINT_OFF_LABEL:String		= ResourceManager.getInstance().getString('Bundle', 'rundown.story.printoff');

[Bindable] 
public function get story():Story {
	return _story;
}

public function set story(value:Story):void {
	_story = value;
	toggleButtons(_story);
}

[Bindable] 
public function get informationCollapsed():Boolean {
	return _informationCollapsed;
}

public function set informationCollapsed(value:Boolean):void {
	_informationCollapsed = value;
}

public function get editionAreaMode():Boolean {
	return _editionAreaMode;
}

[Bindable]
public function set editionAreaMode(value:Boolean):void {
	_editionAreaMode = value;
	loadPopUpButtonContent();
}

[Bindable]
public function get editable():Boolean {
//    if(story.displayed || story.displaying || story.block.rundown.displayed) return false;
    return _editable;
}

public function set editable(value:Boolean):void {
    _editable = value;
}

// --------------------------
//  EVENTS
// --------------------------

protected function startUp(event:FlexEvent):void {
	intelligentInterfaceService.subscribe(onIntelligentInterfaceMessageReceived);
	storyService.subscribe(onStoryMessageReceived);
    rundownService.subscribe(onRundownMessageReceived);
	onLoadStoryKinds();
	toggleButtons(_story);

	loadPopUpButtonContent();
	storyOld = ObjectUtil.clone(story) as Story;
	activeControlChange();

	scrollManager = new FormScrollManager(storyScroller);
}

private function save(closeable:Boolean, report:String=null):void {
	markIfChanged();

	timer.stop();
	
	if (!isValid()) {
		timer.start();
		ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
		return;
	}
	
	var storyKind:StoryKind =	cbStoryKinds.selectedItem as StoryKind;
	if (storyKind) {
		story.storyKind = storyKind;
	}

	for each (var section:StorySection in story.sections) {
		if (section) {
			reorderStorySubSection(section.subSections);
		}
	}

	btnSave.enabled = btnClose.enabled = btnSaveAndClose.enabled = false;
	
	storyService.merge(story, 
		function (event:ResultEvent):void {
			ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
            //Caso o usuario coloque alguma lauda em exibicão na hora de salvar, impede que os botões sejam habilitados novamente.
            if(!story.displayed && !story.displaying && !story.block.rundown.displayed)
			    btnSave.enabled = btnSaveAndClose.enabled = true;

            btnClose.enabled = true;
			
			//UPDATES FIELDS 
			story.updateFields(event.result as Story);
			
			// Enable buttons, server already answer the client.
			toggleButtons(story);
			
			//Clone do guideline
			storyOld = ObjectUtil.clone(story) as Story;
			modify = '';

			if (report) {
				print(report);
			}

			if (closeable) {
				onClose();
				return;
			}

			timer.start();
		}, 
		function (event:FaultEvent):void {

            //Caso o usuario coloque alguma lauda em exibicão na hora de salvar, impede que os botões sejam habilitados novamente.
			if(!story.displayed && !story.displaying && !story.block.rundown.displayed)
                btnSave.enabled = btnSaveAndClose.enabled = true;
            btnClose.enabled = true;
            ANews.showInfo(event.fault.faultString, Info.ERROR);
			timer.start();
			// Enable buttons, server already answer the client.
			toggleButtons(story);
		}
	);
}

protected function changeStoryKinds(event:IndexChangeEvent):void {
	event.preventDefault();
	event.stopImmediatePropagation();
	if(!StringUtils.isNullOrEmpty(event.currentTarget.selectedItem)) {
		if(event.currentTarget.selectedItem is StoryKind) {
			story.storyKind = event.currentTarget.selectedItem as StoryKind;
		}
	}
}

public function dispose():void {
	scrollManager.deactivate();

	intelligentInterfaceService.unsubscribe(onIntelligentInterfaceMessageReceived);
	storyService.unsubscribe(onStoryMessageReceived);
	rundownService.unsubscribe(onRundownMessageReceived);
	clearCacheData();
	
	if (headSectionForm) { headSectionForm.dispose(); }
	
	if (ncSectionForm) { ncSectionForm.dispose(); }
	if (vtSectionForm) { vtSectionForm.dispose(); }
	if (offSectionForm) { offSectionForm.dispose(); }
	if (footerNoteForm) { footerNoteForm.dispose(); }
	if (offSectionForm) { offSectionForm.dispose(); }
	
	if (slug) { slug.dispose(); } 
	if (field) { field.dispose(); }
}

public function onCheckVTManually(event:MouseEvent):void {
    story.vtManually = !story.vtManually;
    story.updateTimes();
    vtTime.dispatchEvent(Event(event));
}

private function onIntelligentInterfaceMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	
	switch (action) {
		case IntelligentInterfaceResult.ERROR:
			ANews.showInfo(String(event.message.body), Info.ERROR);
			break;
		default:
			throw new Error("Unknow Intelligent Interface message action: " + action);
	}
}

public function onLoadStoryKinds():void {
	StoryKindService.getInstance().listAll(function (event:ResultEvent):void {
		storyKinds = event.result as ArrayCollection;
	});
	
	if (story.storyKind) {
		cbStoryKinds.selectedItem = story.storyKind;	
	} else {
		for each (var storyKind:StoryKind in storyKinds) {
			if (storyKind.defaultKind) {
				cbStoryKinds.selectedItem = storyKind;
				break;
			}
		}
	}
}

private function addSection():void {
	if (toolBar.selectedIndex == 0) {
		addHeadSection();
	} else if (toolBar.selectedIndex == 1) {
		addNCSection();
		toggleVtOrNCButtons(false);
	} else if (toolBar.selectedIndex == 2) {
		addVTSection();
		toggleVtOrNCButtons(false);
	} else if (toolBar.selectedIndex == 3) {
		addFooterNoteSection();
		toggleNoteButton(false);
	} else if (toolBar.selectedIndex == 4) {
		addOffSection();
	} else if (toolBar.selectedIndex == 5) {
		addInformation();
	}
}

private function getVersions():void {
	if (story != null && story.id > 0) {
		storyService.loadChangesHistory(story.id, 
			function(event:ResultEvent):void {
				revisions = event.result as ArrayCollection;
				if (revisions && revisions.length > 0) {
					var changesWindow:ChangesHistoryWindow = new ChangesHistoryWindow();
					changesWindow.addEventListener(CloseEvent.CLOSE,
						function(event:CloseEvent):void {
							PopUpManager.removePopUp(changesWindow);
						}
					);
					
					changesWindow.dataProvider = revisions;
					changesWindow.itemRenderer = StoryRevisionRenderer;
					
					PopUpManager.addPopUp(changesWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
					PopUpManager.centerPopUp(changesWindow);
				} else {
					ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'story.msg.withoutVersion'), Info.WARNING);
				}
			}
		);
	}
}

private function saveAndPrint(report:String):void {
	if (markIfChanged()) {
		Alert.show(
				bundle.getString("Bundle", "defaults.msg.saveBeforePrint"),
				bundle.getString("Bundle", "defaults.msg.confirmation"),
				Alert.NO | Alert.YES, null,

				function(event:CloseEvent):void {
					if (event.detail == Alert.YES) {
						save(false, report);
					}
				}
		);
	} else {
		print(report);
	}
}

private function print(report:String):void {
	reportUtil.generate(report, { ids: story.id });
}

private function onPopUpButtonItemClick(event:MenuEvent):void {
	switch (event.item as String) {
		case SEE_ORIGINAL:
			openSourceStory();
			break;
		case SEE_REPORTAGE_LABEL:
			openReportage();
			break;
		case SEE_GUIDELINE_LABEL:
			openGuideline();
			break;
		case SEE_NEWS_LABEL:
			openNews();
			break;
		case OK_LABEL:
            if(editable)
                finalize();
            else if(story.displayed || story.block.rundown.displayed) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "rundown.block.displayed"), Info.WARNING);
            else if(story.displaying) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "story.isDisplaying"), Info.WARNING);
            else if(!ANews.USER.equals(story.editingUser)) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "story.editionDropped"), Info.WARNING);
			break;
		case APPROVE_LABEL:
            if(editable)
			    approve();
            else if(story.displayed || story.block.rundown.displayed) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "rundown.block.displayed"), Info.WARNING);
            else if(story.displaying) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "story.isDisplaying"), Info.WARNING);
            else if(!ANews.USER.equals(story.editingUser)) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "story.editionDropped"), Info.WARNING);
			break;
		case PRINT_LABEL:
			saveAndPrint(ReportsUtil.STORY);
			break;
		case PRINT_OFF_LABEL:
			saveAndPrint(ReportsUtil.STORY_OFF);
			break;
	}
}

private function approve():void {
    storyService.updateField(story.id, "approved", true);
    loadPopUpButtonContent();
}

private function finalize():void {
	storyService.updateField(story.id, "ok", true);
	loadPopUpButtonContent();
}

private function openGuideline():void {
	var guideline:Guideline = null;

	if (story.guideline) {
		guideline = story.guideline;
	} else if (story.reportage && story.reportage.guideline) {
		guideline = story.reportage.guideline;
	}

	GuidelineService.getInstance().loadById(guideline.id,
			function(event:ResultEvent):void {
				var result:Guideline = Guideline(event.result);
				tabManager.openTab(GuidelineViewer, "guideline_" + result.id, { guideline: result, tabManager: tabManager });
			}
	);
}

private function openSourceStory():void {
	storyService.loadById(story.sourceStory.id,
			function(event:ResultEvent):void {
				var result:Story = Story(event.result);
				tabManager.openTab(StoryViewer, "story_" + result.id, { story: result, tabManager: tabManager });
			}
	);
}

private function openReportage():void {
	if (story.reportage) {
		ReportageService.getInstance().loadById(story.reportage.id, function(event:ResultEvent):void {
			var result:Reportage = Reportage(event.result);
            tabManager.openTab(ReportageViewer, "reportage_" + result.id, { reportage: result, tabManager: tabManager });
		});	
	}
}

private function openNews():void {
	if (story.news) {
		NewsService.getInstance().loadById(story.news.id, function(event:ResultEvent):void {
			var result:News = News(event.result);
			tabManager.openTab(NewsViewerComponent, "news_" + result.id, { news: result, tabManager: tabManager });
		});	
	}
}

public function close():void {
	if (editable && story.isDifferent(storyOld)) {
		Alert.show(
			bundle.getString("Bundle", "defaults.msg.dataAreNotSaved"),
			bundle.getString("Bundle", "defaults.msg.confirmation"),
			Alert.CANCEL | Alert.NO | Alert.YES, null, onDataAreNotSaved, null, Alert.YES
		);
	} else {
		onClose();
	}
}

public function onClose():void {
	modify = "";
    if (editable && story.id > 0) {
        storyService.unlockEdition(story.id, null);
    }
	dispose();
	
	// Qualquer alteração feita nesse trecho de código reflitira nos notificadores da área de edição e espelho.
	/*if (editionAreaMode == false ) {
		rundownService.unlockEdition(story.id, onUnlockSuccess);
	} else if (editionAreaMode == true && this.currentState == EDIT_STATE) {
		rundownService.unlockEdition(story.id, onUnlockRemoveTabSuccess);
	} else {*/
		tabManager.removeTab(this);
	/*}
	
	onRemove();

	storyService.loadById(story.id, onLoadByIdSucess);*/
}

private function onDataAreNotSaved(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		save(true);
	} else if(event.detail == Alert.NO) {
		onClose();
	}
}

//----------------------------------
//	HELPERS
//----------------------------------
private function addHeadSection():void {
	story.addHead();
}

private function addNCSection():void {
	story.addNC();
}

private function addVTSection():void {
	story.addVT();
}

private function addFooterNoteSection():void {
	story.addFooterNote();
}

private function addOffSection():void {
	story.addOff();
}

private function addInformation():void {
	story.information = "";
    toggleInformationButton();
}

private function removeInformation():void {
    story.information = null;
	toggleInformationButton();
}

public function isValid():Boolean {
	return Util.isValid(validadores) && headSectionForm.isValid() && ncSectionForm.isValid() && vtSectionForm.isValid() && footerNoteForm.isValid()&& offSectionForm.isValid();
}

public function resetValidate():void {
	cbStoryKinds.errorString = '';
	slug.errorString = '';
	information.errorString = '';
//	headSectionForm.resetValidate();
//	ncSectionForm.resetValidate();
//	vtSectionForm.resetValidate();
//	footerNoteForm.resetValidate();
//	offSectionForm.resetValidate();
}

protected function toggleVtOrNCButtons(enabled:Boolean):void {
	var ncButton:ButtonBarButton = toolBar.dataGroup.getElementAt(1) as ButtonBarButton;
	var vtButton:ButtonBarButton = toolBar.dataGroup.getElementAt(2) as ButtonBarButton;
	ncButton.enabled = enabled;
	vtButton.enabled = enabled;
}

protected function toggleNoteButton(enabled:Boolean):void {
	var noteButton:ButtonBarButton = toolBar.dataGroup.getElementAt(3) as ButtonBarButton;
	noteButton.enabled = enabled;
}

protected function toggleInformationButton():void {
	var informationButton:ButtonBarButton = toolBar.dataGroup.getElementAt(5) as ButtonBarButton;
	informationButton.enabled = !story.hasInformation;
}

private function verifyInformation(_story:Story):Boolean {
	return _story.information != null && _story.information.length > 0;
}

private function hideOrShowInformation():void {
	informationCollapsed = !informationCollapsed;
}

public function toggleButtons(_story:Story):void {
	if (toolBar && _story) {
		var ncButton:ButtonBarButton = toolBar.dataGroup.getElementAt(1) as ButtonBarButton;
		var vtButton:ButtonBarButton = toolBar.dataGroup.getElementAt(2) as ButtonBarButton;
		var noteButton:ButtonBarButton = toolBar.dataGroup.getElementAt(3) as ButtonBarButton;
		var informationButton:ButtonBarButton = toolBar.dataGroup.getElementAt(5) as ButtonBarButton;
		
		noteButton.enabled = _story.footerSection == null;
		vtButton.enabled = _story.vtSection == null && _story.ncSection == null;
		ncButton.enabled = _story.vtSection == null && _story.ncSection == null;
		informationButton.enabled = !_story.hasInformation;
	}
}

// -------------------------
// Messenger
// -------------------------

private function onStoryMessageReceived(event:MessageEvent):void {
	var storyId:int = event.message.headers["storyId"];
	var action:String = event.message.headers["action"];
	var serverStory:Story = event.message.body as Story;
	
	//Only EntityAction.UpdateField has serverStory equals null.
	if (serverStory != null)
		storyId = serverStory.id;
	
	if (story == null)
		return;
	
	// This message is not for me.
	if (story.id != storyId)
		return;
	
	var close:Boolean = false;
	switch (action) {
		case EntityAction.FIELD_UPDATED:  {
			var field:String = event.message.headers["field"];
			var value:Object = event.message.headers["value"];
            // O nome do campo que chegará para o imageEditor é diferente do nome real do campo, devendo ser tratado
            if(field.toLowerCase() == 'image_editor')
                story.imageEditor = value as User;
            else
			    story[field.toLowerCase()] = value;
			
			// Se mudou o "ok" ou a aprovação, atualiza o PopUpButton
			if (field == "ok" || field == "approved") {
				loadPopUpButtonContent();
			} else if(field.toLowerCase() == 'displayed') {
                story.displaying = false;
                setSaveEnabled(false);
            } else if(field.toLowerCase() == 'displaying') {
                setSaveEnabled(false);
                ANews.showInfo(bundle.getString("Bundle", "story.msg.displaying", [StringUtils.isNullOrEmpty(story.slug) ? bundle.getString('Bundle', 'defaults.noSlug') : story.slug]), Info.WARNING);
            }
			break;
		}
		case EntityAction.UPDATED:  {
			story.updateFields(serverStory);
			//Clone Story
			storyOld = ObjectUtil.clone(story) as Story;
			modify =  '';
			break;
		}
		case EntityAction.UNLOCKED:  {
			story.editingUser = null;
			break;
		}
		case EntityAction.LOCKED:  {
			story.editingUser = event.message.headers["user"] as User;
            if((event.message.headers["user"] as User).equals(ANews.USER)){
                setSaveEnabled(true);
            } else {
                // exibe a ensagem de acordo com a flag, pois, caso cheguem mais notificações, não será necessário alertar o usuário novamente.
                if(editable) Alert.show(bundle.getString("Bundle", "story.isEditingWithSlug", [ StringUtils.isNullOrEmpty(story.slug) ? '(' + story.page + ') ' + bundle.getString('Bundle', 'defaults.noSlug') : '(' + story.page +') ' + story.slug , (event.message.headers["user"] as User).name ]), null);
                setSaveEnabled(false);
            }
			break;
		}
//		case RundownAction.DRAG_UNLOCKED:  {
//			story.isDragging = false;
//			break;
//		}
//		case RundownAction.DRAG_LOCKED:  {
//			story.isDragging = true;
//			break;
//		}
//		case EntityAction.DELETED:  {
//			ANews.showInfo(bundle.getString("Bundle", "rundown.msg.closeOnDeleted"), Info.WARNING);
//			close = true;
//			break;
//		}
//		case RundownAction.DRAWER:  {
//			ANews.showInfo(bundle.getString("Bundle", "rundown.msg.closeOnSendToDrawer"), Info.WARNING);
//			close = true;
//			break;
//		}
	}
	
	if (close) {
		onClose();
	}
}

private function onRundownMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var rundownUpdated:Rundown = event.message.body as Rundown;

    if (story.block.rundown.id == rundownUpdated.id) {
        switch (action) {
            case RundownAction.DISPLAY_RESETS:
                setSaveEnabled(true);
                story.block.rundown.displayed = false;
                story.displayed = false;
                story.displaying = false;
                break;
            case RundownAction.COMPLETE_DISPLAY:
                setSaveEnabled(false);
                story.block.rundown.displayed = true;
                story.displaying = false;
                ANews.showInfo(bundle.getString("Bundle", "rundown.msg.displaying", [StringUtils.isNullOrEmpty(story.slug) ? bundle.getString('Bundle', 'defaults.noSlug') : story.slug]), Info.WARNING);
                break;
            case RundownAction.SORTED:
                //Necessário para não cair no default, indica que o usuário mandou ordenar as páginas.
                break;
            case RundownAction.FIELD_UPDATED:
                //Necessário para não cair no default, indica que o usuário mandou ordenar as páginas.
                break;
            default:
                //INSERTED, DELETED AND UPDATE are not supported on the rundown.
                throw new Error("You receive an undefined message for a rundown, try to reload you rundown!");
        }
    }
}

//------------------------------------------------------------------------------
//	Cache Control Functions
//------------------------------------------------------------------------------
private var timer:Timer;

private function activeControlChange():void {
	if (!timer) {
		timer = new Timer(5000);
	}
	// Em intervalos de 1 segundos salva o texto do encaminhamento localmente
	timer.addEventListener(TimerEvent.TIMER, onTimerActive);
	timer.start();
}

private function onTimerActive(event:TimerEvent):void {
	markIfChanged();
}

private function clearCacheData():void {
	if (timer != null) {
		timer.stop();
		timer.removeEventListener(TimerEvent.TIMER, onTimerActive);
		timer = null;
	}
}

private function markIfChanged():Boolean {
	modify = editable && storyOld.isDifferent(story)? '* ' : '';
	return modify == '* ';
}

//------------------------------------------------------------------------------
//	Helpers
//------------------------------------------------------------------------------
private function reorderStorySubSection(sections:ArrayCollection):void {
	for (var i:int = 0; i < sections.length; i++) {
		var sub:StorySubSection  = sections[i];
		sub.order = i;
		if(sub.subSections != null && sub.subSections.length > 0){
			reorderStorySubSection(sub.subSections);
		}
	}
}

private function setSaveEnabled(enable:Boolean):void {
    // Metodo chamado em varios lugares, como na reinicialização do espelho, mas so deve habilitar o salvamento se
    // a lauda estiver com o lock para o ususario logado
    if(enable && !ANews.USER.equals(story.editingUser)){
        enable = false;
    }
    btnSaveAndClose.enabled = enable;
    btnSave.enabled = enable;
    editable = enable;
    toolBar.enabled = enable;
}

private function loadPopUpButtonContent():void {
	if (popUpButton && popUpButtonMenu) {
		popUpButtonMenu.dataProvider = popUpButtonMenu.dataProvider || new ArrayCollection();
		
		var dataProvider:ArrayCollection = ArrayCollection(popUpButtonMenu.dataProvider);
		dataProvider.removeAll();

		// botão imprimir
		if (ANews.USER.allow('01071105')) {
			dataProvider.addItem(PRINT_LABEL);
			dataProvider.addItem(PRINT_OFF_LABEL);
		}

		// botão "ver lauda original"
		if (story.sourceStory) {
			dataProvider.addItem(SEE_ORIGINAL);
		}

		// botão "ver reportagem"
		if (story.reportage && !editionAreaMode) {
			dataProvider.addItem(SEE_REPORTAGE_LABEL);
		}
		
		// botão "ver pauta"
		if ((story.guideline || (story.reportage && story.reportage.guideline)) && !editionAreaMode) {
			dataProvider.addItem(SEE_GUIDELINE_LABEL);
		}
		
		// botão "ver news"
		if (story.news && !editionAreaMode) {
			dataProvider.addItem(SEE_NEWS_LABEL);
		}
		
		// botão "ok"
		if (ANews.USER.equals(story.editor) && ANews.USER.allow('01071108') && !story.ok) {
			dataProvider.addItem(OK_LABEL);
		}
		
		// botão "aprovar" 
		if (ANews.USER.allow('01071106') && !story.approved) {
			dataProvider.addItem(APPROVE_LABEL);
		}
		
		popUpButton.includeInLayout = popUpButton.visible = dataProvider.length != 0;
	}
}
