import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import flexlib.controls.tabBarClasses.SuperTab;
import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.events.CalendarLayoutChangeEvent;
import mx.events.CollectionEvent;
import mx.events.FlexEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ComboBox;
import spark.components.TextInput;
import spark.components.gridClasses.GridColumn;
import spark.events.GridSelectionEvent;
import spark.events.IndexChangeEvent;
import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.ClipboardAction;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownStatus;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.RundownService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.view.rundown.StoryHelper;
import tv.snews.anews.view.rundown.story.section.form.StoryForm;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const rundownService:RundownService = RundownService.getInstance();
private const storyService:StoryService = StoryService.getInstance();
private const dateTimeUtil:DateUtil = new DateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();
private const bundle:IResourceManager = ResourceManager.getInstance();

// Parâmetros da busca
[Bindable] private var slug:String;
[Bindable] private var content:String;
[Bindable] private var program:Program;
[Bindable] private var start:Date;
[Bindable] private var end:Date;
[Bindable] private var searchType:String;
[Bindable] private var textToIgnore:String;
[Bindable] private var showViewer:Boolean = false;
[Bindable] private var programs:ArrayList = new ArrayList();
[Bindable] private var helper:StoryHelper = new StoryHelper();
[Bindable] private var tabManager:TabManager = new TabManager();

[Bindable] private var total:int = 0;

private var _allPrograms:Program;

private function get allPrograms():Program {
	if (!_allPrograms) {
		_allPrograms = new Program();
		_allPrograms.id = -1;
		_allPrograms.name = bundle.getString('Bundle', 'defaults.all');
	}
	return _allPrograms;
}

private var _selectedStory:Story = null;

[Bindable] private function set selectedStory(value:Story):void {
	_selectedStory = value;
    this.currentState = value ? "selected" : "normal";
}

private function get selectedStory():Story {
	return _selectedStory;
}

//------------------------------------------------------------------
//	Event Handlers
//------------------------------------------------------------------

private function onContentCreationComplete(event:FlexEvent):void {
	DomainCache.programs.addEventListener(CollectionEvent.COLLECTION_CHANGE, onDomainProgramsChange);
	mainNavigator.addEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false, 0, true);
	tabManager.tabNavigator = mainNavigator;
	callLater(function ():void { tabManager.setClosePolicyForTab(0, SuperTab.CLOSE_NEVER)});
}

private function onRemove(event:FlexEvent):void {
	DomainCache.programs.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onDomainProgramsChange);
	mainNavigator.removeEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false);
	for each (var tab:Object in mainNavigator.getChildren()) {
		if (tab is StoryForm) {
			var obj:StoryForm = tab as StoryForm;
			obj.close();
		}
	}
	tabManager.removeAllTabs();
}

private function tabClosedHandler(event:SuperTabEvent):void {
	if (mainNavigator.getChildAt(event.tabIndex) is StoryForm) {
		event.preventDefault();
		var storyForm:StoryForm = mainNavigator.getChildAt(event.tabIndex) as StoryForm;
		storyForm.close();
	} 
	
}

private function onEdit(event:MouseEvent):void {
	if (ANews.USER.allow('01071103')) {
		if (selectedStory == null) {
			showWarningWasNotSelectedStory();
			return;
		}
        storyService.isLockedForEdition(selectedStory.id, onCheckLockedSuccess);
	}
}

private function onCheckLockedSuccess(event:ResultEvent):void {
	var user:User = event.result as User;
	if (user) {
        helper.storyInEditing(user, tabManager, "story_" + selectedStory.id, selectedStory, bundle.getString("Bundle", "rundown.title"));
    } else {
		storyService.loadById(selectedStory.id, function (event:ResultEvent):void {
			var result:Story = Story(event.result);

            if(result.displayed || result.block.rundown.displayed) ANews.showInfo(bundle.getString("Bundle", "rundown.block.displayed"), Info.WARNING)
            else if (result.displaying) ANews.showInfo(bundle.getString("Bundle", "story.isDisplaying"), Info.WARNING)
            else {
                    var params:Object = { story: result, tabManager: tabManager };
                    tabManager.openTab(StoryForm, "story_" + result.id, params);
                    storyService.lockEdition(selectedStory.id, ANews.USER.id, null);
            }
		});
	}
}

private function search(page:int = 1):void {
	page = page < 1 ? 1 : page;
    slug = StringUtils.trim(slug || "");
	content = StringUtils.trim(content || "");
	textToIgnore = StringUtils.trim(textToIgnore || "");
	
	storyService.storiesByFullTextSearch(slug, content, program, start, end, page, searchType, textToIgnore, onListSuccess, onListFail);
}

//Verifica se os dados do paginador existem, caso contrário, não permite a troca de páginas
private function onPageChange(page:int=1):void {
    if(pgStoryArchive.data)search(page);

    //Após mudar de página, não deve existir nenhuma lauda selecionada
    selectedStory = null;
}

private function onDomainProgramsChange(event:CollectionEvent):void {
	reloadPrograms();
}

private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
    total = result.total;
	for each (var story:Story in result.results) {
		if (story.slug == "") {
			story.slug = bundle.getString("Bundle", "defaults.noSlug");
		}
	}
	
	pgStoryArchive.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function onListFail(event:FaultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', 'story.archive.msg.listFailed'), Info.ERROR);
}

private function onCopyRequest(event:MouseEvent):void {
//	sendStoriesToClipboard(ClipboardAction.COPY);
	this.currentState = "copy";
	enableFiltersAndGrid(false);
}


public function selectedStoriesIds():Array {
	var stories:Array = new Array();
	
	var selectedItems:Vector.<Object> = storiesGrid.selectedItems;
	if (selectedItems) {
		for each (var story:Story in selectedItems) {
			stories.push(story.id);
		}
	}
	
	return stories;
}

private function sendStoriesToClipboard(action:String):void {
	
	storyService.sendStoriesToClipboard(action, selectedStoriesIds(), function(event:ResultEvent):void {
		
		ANews.showInfo(bundle.getString('Bundle', 'story.archive.clipBoardSuccess'), Info.INFO);
		/*var clipboardIds:ArrayCollection =  new ArrayCollection(event.result as Array);*/
		
		/*for each (var block:Block in rundown.blocks) {
			for (var i:int = block.stories.length - 1; i >= 0; i--) {
				var _story:Story = block.stories.getItemAt(i) as Story;
				
				_story.cutted = false;
				_story.copied = false;
				
				if (clipboardIds.contains(_story.id)) {
					if(action == ClipboardAction.COPY)
						_story.copied = true;
					else
						_story.cutted = true;
				}
			}
		}*/
	});
}

private function onCopy(event:MouseEvent):void {
	var storyId:Number = selectedStory.id;
	var date:Date = destinyDay.selectedDate;
	var programId:int = Program(destinyProgram.selectedItem).id;

	if (date && programId && storyId) {
		rundownService.verifyRundown(date, programId,
				function(event:ResultEvent):void {
					var resultCode:String = event.result as String;
					var rm:IResourceManager = ResourceManager.getInstance();

					switch (resultCode) {
						case RundownStatus.EXIST:
							storyService.copyTo(selectedStoriesIds(), date, programId, false,
									function(event:ResultEvent):void {
										onCopyCancel(null);
										ANews.showInfo(rm.getString("Bundle", "story.successCopy"), Info.INFO);
									}
							);
							break;
						default:
							ANews.showInfo(rm.getString("Bundle", "story.noRundown"), Info.WARNING);
					}
				}
		);
	} else {
		ANews.showInfo(bundle.getString("Bundle", "defaults.msg.requiredFields"), Info.INFO);
	}
}

private function onCopyCancel(event:MouseEvent):void {
	this.currentState = "selected";
	enableFiltersAndGrid(true);
}

private function enableFiltersAndGrid(enable:Boolean):void {
    storiesGrid.enabled = enable;
    searchFiltersGroup.enabled = enable;
}

private function onPrint(event:MouseEvent):void {
	reportUtil.generate(ReportsUtil.STORY, {ids: selectedStory.id});
}

private function printGrid():void {
    var params:Object = new Object();
    var program:Program = (programCombo.selectedItem as Program);
    params.periodStart = periodStart.text;
    params.periodEnd = periodEnd.text;
    params.programId =  program != null ? program.id : "";
    params.slug = slugField.text;
    params.text = searchText.text;
    params.type = searchType;
    params.ignoreText = ignoreField.text;

    if(total > 100) {
        ANews.showInfo(bundle.getString('Bundle', "defaults.msg.listTheFirstHundred"), Info.WARNING);
    }

    reportUtil.generate(ReportsUtil.STORY_ARCHIVE_GRID, params);
}


private function onChangeInitialDate(event:CalendarLayoutChangeEvent):void {
	if (periodStart.selectedDate && periodEnd.selectedDate) {
		var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
		if (val > 0) {
			periodEnd.selectedDate = periodStart.selectedDate;
		} 
	}
}

private function onChangeFinalDate(event:CalendarLayoutChangeEvent):void {
	if (periodStart.selectedDate && periodEnd.selectedDate) {
		var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
		if (val > 0 ) {
			periodStart.selectedDate = periodEnd.selectedDate;
		}
	}
}

private function onSearch():void {
	// Carrega os parâmetros da busca
    slug = slugField.text;
	content = searchText.text;
	program = programCombo.selectedItem as Program;
	start = periodStart.selectedDate;
	end = periodEnd.selectedDate;
	textToIgnore = ignoreField.text;
	searchType = cbSearchType.selectedItem.id;
    showViewer = false;
	
	if (periodIsInvalid()) {
		ANews.showInfo(bundle.getString('Bundle', 'story.archive.msg.invalidPeriod'), Info.WARNING);
	} else {
		reloadData();
	}
}

private function onSearchKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        onSearch();
    }
}

private function onClear(event:MouseEvent):void {
	// Limpa os campos
    slugField.text = "";
	searchText.text = "";
	programCombo.selectedIndex = 0;
	periodStart.selectedDate = null;
	periodEnd.selectedDate = null;
	ignoreField.text = "";
    showViewer = false;
	
	// Limpa os parâmetros de busca
    slug = null;
	content = null;
	program = null;
	start = end = null;
	periodStart.errorString = null;
	periodEnd.errorString = null;
	selectedStory = null;
	cbSearchType.selectedIndex = 0;
	programCombo.selectedIndex = -1;
	pgStoryArchive.load(null, 0,0,0);

    total = 0;
}

private function onSelectionChangeHandler(event:GridSelectionEvent):void {
	selectedStory = storiesGrid.selectedItem as Story;
}

private function onChangeSearchText(event:TextOperationEvent):void {
	var obj:TextInput = event.target as TextInput;
	if (obj.text.length == 0) {
		obj.errorString = "";
	}
}

private function onViewClick(event:MouseEvent):void{
	showViewer = true;
}

private function onCancelClick(event:MouseEvent):void {
    showViewer = false;
}

private function onChangeProgramCombo(event:IndexChangeEvent):void {
	if (event.target is ComboBox) {
		var cb:ComboBox = event.target as ComboBox;
		if (cb.selectedItem is String) {
			cb.textInput.text = "";
			cb.selectedItem = null;
			cb.selectedIndex = -1;
		}
	}
}

private function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && storiesGrid.selectedItem != null && storiesGrid.selectedIndex != -1) {
		if (showViewer == true) {
			showViewer = false;
		} else {
			showViewer = true;
		}
	}
}

//------------------------------------------------------------------
//	Helper Methods
//------------------------------------------------------------------

private function reloadData():void {
	selectedStory = null;
	search();
}

private function reloadPrograms():void {
	programs.removeAll();
	programs.addItem(allPrograms);
	for each (var program:Program in DomainCache.programs) {
		programs.addItem(program);
	}
	programCombo.selectedIndex = 0;
}

private function dateLabelFunction(item:Object, column:GridColumn):String {
    if((item as Story).block){
	    return dateTimeUtil.dateToString((item as Story).block.rundown.date);
    }
    return "";
}

private function periodIsInvalid():Boolean {
	var start:Date = periodStart.selectedDate;
	var end:Date = periodEnd.selectedDate;
	return start && end && start.time > end.time;
}

private function showWarningWasNotSelectedStory():void {
	ANews.showInfo(bundle.getString("Bundle", "rundown.story.selectionRequired"), Info.WARNING);
	selectedStory = null;
	this.currentState = "normal";
}
