import flash.events.MouseEvent;

import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.RundownTemplate;
import tv.snews.anews.domain.StoryTemplate;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.RundownTemplateService;
import tv.snews.anews.service.StoryTemplateService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.view.rundown.template.RundownTemplateNameWindow;
import tv.snews.anews.view.rundown.template.StoryTemplateForm;

private const bundle:IResourceManager = ResourceManager.getInstance();

private const rundownService:RundownTemplateService = RundownTemplateService.getInstance();
private const storyService:StoryTemplateService = StoryTemplateService.getInstance();

[Bindable]
private var rundownTemplates:ArrayCollection = new ArrayCollection();

[Bindable] public var tabManager:TabManager = new TabManager();

[Bindable]
private var selectedTemplate:RundownTemplate;

//--------------------------------------
//  Event Handlers
//--------------------------------------

private function onCreateComplete(event:FlexEvent):void {
	tabManager.tabNavigator = mainTabNav;

    mainTabNav.addEventListener(SuperTabEvent.TAB_CLOSE, onTabClose, false, 0, true);

	this.addEventListener(CrudEvent.EDIT,
            function(event:CrudEvent):void {
                if (event.entity is StoryTemplate) {
                    openForm(StoryTemplate(event.entity));
                }
            }
    )

    rundownService.subscribe(onRundownMessageReceived);
}

private function onRemove(event:FlexEvent):void {
    rundownService.unsubscribe(onRundownMessageReceived);
    rundownService.unlockAllEditions();
}

private function onRundownMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var rundown:RundownTemplate = event.message.body as RundownTemplate;

    var editable:Boolean;

    switch (action) {
        case EntityAction.LOCKED:
            editable = false;
            break;
        case EntityAction.UNLOCKED:
            editable = true;
            break;
    }

    for each (var aux:RundownTemplate in rundownTemplates) {
        if (aux.equals(rundown)) {
            aux.editable = editable;
        }
    }
}

private function onTabClose(event:SuperTabEvent):void {
    var form:StoryTemplateForm = mainTabNav.getChildAt(event.tabIndex) as StoryTemplateForm;
    if (form != null) {
        event.preventDefault();
        form.close();
    }
}

private function onChangePrograms(event:IndexChangeEvent):void {
    loadTemplates();
}

private function onTemplatesListChange(event:IndexChangeEvent):void {
    selectedTemplate = templatesList.selectedItem as RundownTemplate;

    if (selectedTemplate) {
        // Recarrega para exibir os dados mais atuais
        rundownService.load(selectedTemplate.id,
                function(event:ResultEvent):void {
                    selectedTemplate = event.result as RundownTemplate;
                }
        );

        // Bloqueia a edição do template
        rundownService.lockEdition(selectedTemplate);
    }

}

private function onViewHandler(event:MouseEvent):void {
}

private function onCreate(event:MouseEvent):void {
    var createWindow:RundownTemplateNameWindow = new RundownTemplateNameWindow();
	
    PopUpManager.addPopUp(createWindow, this, true);
    PopUpManager.centerPopUp(createWindow);

    createWindow.addEventListener(CrudEvent.CREATE,
            function(event:CrudEvent):void {
                requestCreateTemplate(event.entity.name);
            }
    );
}

private function onDelete(event:MouseEvent):void {
	Alert.show(bundle.getString("Bundle", "defaults.msg.confirmAllDelete"), bundle.getString("Bundle", "defaults.msg.confirmation"), Alert.NO | Alert.YES, null, onDeleteConfirm);
}
	
private function onDeleteConfirm(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        rundownService.removeTemplate(selectedTemplate,
                function(event:ResultEvent):void {
                    loadTemplates();

                    ANews.showInfo(bundle.getString("Bundle", "defaults.msg.deleteSuccess"), Info.INFO);
                }
        )
    }
}

private function onCopy(event:MouseEvent):void {
    var copyWindow:RundownTemplateNameWindow = new RundownTemplateNameWindow();
    copyWindow.displayProgram = true;

    PopUpManager.addPopUp(copyWindow, this, true);
    PopUpManager.centerPopUp(copyWindow);

    copyWindow.addEventListener(CrudEvent.CREATE,
            function(event:CrudEvent):void {
                copyTemplate(event.entity.name, event.entity.program);
            }
    );
}

//--------------------------------------
//  Helpers
//--------------------------------------

private function openForm(storyTemplate:StoryTemplate):void {
    storyService.load(storyTemplate.id,
            function(event:ResultEvent):void {
                var result:StoryTemplate = StoryTemplate(event.result);
                tabManager.openTab(StoryTemplateForm, (result.id + ""), { storyTemplate: result, tabManager: tabManager });
            }
    );

}

private function requestCreateTemplate(name:String):void {
    var program:Program = programsCombo.selectedItem as Program;

    rundownService.nameAvailable(name, program,

            function(event:ResultEvent):void {
                if (Boolean(event.result)) {
                    rundownService.createTemplate(name, program,

                            function(event:ResultEvent):void {
                                selectedTemplate = RundownTemplate(event.result);
                                rundownTemplates.addItem(selectedTemplate);
                                templatesList.selectedItem = selectedTemplate;

                                // Bloqueia a edição
                                rundownService.lockEdition(selectedTemplate);
                            }
                    );
                } else {
                    ANews.showInfo(bundle.getString("Bundle", "rundown.template.msg.invalidName"), Info.WARNING);
                }
            }
    );
}

private function copyTemplate(name:String, program:Program):void {
    var selectedProgram:Program = programsCombo.selectedItem as Program;

    rundownService.nameAvailable(name, program,

            function(event:ResultEvent):void {
                if (Boolean(event.result)) {
                    rundownService.createCopy(selectedTemplate.id, name, program,

                            function(event:ResultEvent):void {
                                if (program.equals(selectedProgram)) {
                                    selectedTemplate = RundownTemplate(event.result);
                                    rundownTemplates.addItem(selectedTemplate);
                                    templatesList.selectedItem = selectedTemplate;

                                    // Bloqueia a edição
                                    rundownService.lockEdition(selectedTemplate);
                                }

                                ANews.showInfo(bundle.getString("Bundle", "rundown.template.msg.copySuccess"), Info.INFO);
                            }
                    );
                } else {
                    ANews.showInfo(bundle.getString("Bundle", "rundown.template.msg.invalidName"), Info.WARNING);
                }
            }
    );
}

private function loadTemplates():void {
    var program:Program = programsCombo.selectedItem as Program;
    if (program) {
        rundownService.findAllByProgram(program,
                function(event:ResultEvent):void {
                    selectedTemplate = null;
                    rundownTemplates = ArrayCollection(event.result);

                    // Desbloqueia todas as edições deste usuário
                    rundownService.unlockAllEditions();
                }
        );
    }
}
