package tv.snews.anews.view.preview {
	import tv.snews.anews.utils.LocalDatabase;
	
	/**
	 * Objeto utilizado para armazenar as configurações do preview, como 
	 * tamanhos de fonte e posições dos divisores.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.1.1
	 */
	[Bindable]
	public class PreviewConfiguration {

		private static const PERSISTED_FIELDS:Array = [ 
			"gridFontSize", "storyFontSize", "gridWidth", "nextViewerHeight"
		];
		
		private const localData:LocalDatabase = LocalDatabase.getInstance(LocalDatabase.PREVIEW);
		
		private static const GRID_MAX_FONT_SIZE:int = 40;
		private static const GRID_MIN_FONT_SIZE:int = 8;
		public static const STORY_MAX_FONT_SIZE:int = 20;
		public static const STORY_MIN_FONT_SIZE:int = 8;

		private static const DEFAULT_FIRST_VIEWER_HEIGHT:int = 260;
		private static const DEFAULT_GRID_WIDTH:int = 450;
		private static const DEFAULT_GRID_FONT_SIZE:int = 12;
		private static const DEFAULT_STORY_FONT_SIZE:int = 12;
		private static const DEFAULT_PAGE_GRID_COLUMN_SIZE:int = 40;
		private static const DEFAULT_KIND_GRID_COLUMN_SIZE:int = 50;
		private static const DEFAULT_HEAD_GRID_COLUMN_SIZE:int = 55;
		private static const DEFAULT_VT_GRID_COLUMN_SIZE:int = 50;
		private static const DEFAULT_TOTAL_GRID_COLUMN_SIZE:int = 65;
		private static const DEFAULT_DISPLAY_GRID_COLUMN_SIZE:int = 50;
		private static const PAGE_GRID_COLUMN_MAX_SIZE:int = 100;
		private static const HEAD_GRID_COLUMN_MAX_SIZE:int = 150;
		private static const VT_GRID_COLUMN_MAX_SIZE:int = 110;
		private static const TOTAL_GRID_COLUMN_MAX_SIZE:int = 165;
        private static const DISPLAY_GRID_COLUMN_MAX_SIZE:int = 60;
        private static const PAGE_GRID_COLUMN_MIN_SIZE:int = 32;
        private static const HEAD_GRID_COLUMN_MIN_SIZE:int = 45;
        private static const VT_GRID_COLUMN_MIN_SIZE:int = 32;
        private static const TOTAL_GRID_COLUMN_MIN_SIZE:int = 50;
        private static const DISPLAY_GRID_COLUMN_MIN_SIZE:int = 50;

		private var _changed:Boolean;
		
		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------
		
		//------------------------------
		//	firstViewerHeight
		//------------------------------
		
		private var _nextViewerHeight:int = DEFAULT_FIRST_VIEWER_HEIGHT;

		public function get nextViewerHeight():int {
			return _nextViewerHeight;
		}
		
		public function set nextViewerHeight(value:int):void {
			registerChange(nextViewerHeight, value); 
			_nextViewerHeight = value;
		}
		
		//------------------------------
		//	gridWidth
		//------------------------------
		
		private var _gridWidth:int = DEFAULT_GRID_WIDTH;
		
		public function get gridWidth():int {
			return _gridWidth;
		}
		
		public function set gridWidth(value:int):void {
			registerChange(gridWidth, value);
			_gridWidth = value;
		}
		
		//------------------------------
		//	storyFontSize (read only)
		//------------------------------
		
		private var _storyFontSize:int = DEFAULT_STORY_FONT_SIZE;
		
		public function get storyFontSize():int {
			return _storyFontSize;
		}
		
		private function set storyFontSize(value:int):void {
			registerChange(storyFontSize, value);
			_storyFontSize = value;
		}
		
		//------------------------------
		//	gridFontSize (read only)
		//------------------------------
		
		private var _gridFontSize:int = DEFAULT_GRID_FONT_SIZE;	
		
		public function get gridFontSize():int {
			return _gridFontSize;
		}
		
		private function set gridFontSize(value:int):void {
			registerChange(gridFontSize, value);
			_gridFontSize = value;
		}
		
		//------------------------------
		//	pageGridColumnSize (read only)
		//------------------------------
		
		private var _pageGridColumnSize:int = DEFAULT_PAGE_GRID_COLUMN_SIZE;
		
		public function get pageGridColumnSize():int
		{
			return _pageGridColumnSize;
		}
		
		public function set pageGridColumnSize(value:int):void
		{
			_pageGridColumnSize = value;
		}
		
		//------------------------------
		//	pageGridColumnSize (read only)
		//------------------------------
		
		private var _kindGridColumnSize:int = DEFAULT_KIND_GRID_COLUMN_SIZE;
		
		public function get kindGridColumnSize():int
		{
			return _kindGridColumnSize;
		}
		
		public function set kindGridColumnSize(value:int):void
		{
			_kindGridColumnSize = value;
		}

		//------------------------------
		//	headGridColumnSize (read only)
		//------------------------------
		
		private var _headGridColumnSize:int = DEFAULT_HEAD_GRID_COLUMN_SIZE;
		
		public function get headGridColumnSize():int
		{
			return _headGridColumnSize;
		}
		
		public function set headGridColumnSize(value:int):void
		{
			_headGridColumnSize = value;
		}
		
		//------------------------------
		//	vtGridColumnSize (read only)
		//------------------------------
		
		private var _vtGridColumnSize:int = DEFAULT_VT_GRID_COLUMN_SIZE;
		
		public function get vtGridColumnSize():int
		{
			return _vtGridColumnSize;
		}
		
		public function set vtGridColumnSize(value:int):void
		{
			_vtGridColumnSize = value;
		}

		//------------------------------
		//	vtGridColumnSize (read only)
		//------------------------------
		
		private var _totalGridColumnSize:int = DEFAULT_TOTAL_GRID_COLUMN_SIZE;
		
		public function get totalGridColumnSize():int
		{
			return _totalGridColumnSize;
		}
		
		public function set totalGridColumnSize(value:int):void
		{
			_totalGridColumnSize = value;
		}


		//------------------------------
		//	displayGridColumnSize (read only)
		//------------------------------

		private var _displayGridColumnSize:int = DEFAULT_DISPLAY_GRID_COLUMN_SIZE;

		public function get displayGridColumnSize():int
		{
			return _displayGridColumnSize;
		}

		public function set displayGridColumnSize(value:int):void
		{
			_displayGridColumnSize = value;
		}

		
		//------------------------------
		//	changed (read only)
		//------------------------------
		
		/**
		 * Informa se as configurações sofreram mudanças que precisam ser 
		 * salvas.
		 */
		public function get changed():Boolean {
			return _changed;
		}
		
		private function set changed(value:Boolean):void {
			_changed = value;
		}
		
		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function PreviewConfiguration() {
			// Carrega os valores de configuração já salvos, se existirem
			var savedValues:Object = localData.data;
			
			if (savedValues) {
				/*
				 * Por algum motivo desconhecido o objeto retornado do 
				 * SharedObject não é do tipo PreviewConfiguration. Por isso os 
				 * valores são carregados da forma abaixo.
				 */
				for each (var key:String in PERSISTED_FIELDS) {
					this[key] = savedValues[key] || this[key];
				}
				
				trace("Loaded { gridFontSize:", gridFontSize, ", storyFontSize:", storyFontSize, ", gridWidth:", gridWidth, "nextViewerHeight:", nextViewerHeight, "}");
			} else {
				trace("No preview configuration saved.");
			}
			changed = false;
		}
		
		//----------------------------------------------------------------------
		//	Public Methods
		//----------------------------------------------------------------------
		
		/**
		 * Aumenta a fonte e tamanhos de colunas da DataGrid que lista as laudas do espelho.
		 */
		public function incGrid():void {
			if (gridFontSize < GRID_MAX_FONT_SIZE) {
				gridFontSize += (gridFontSize >= 20) ? 4 : 2;
			}
			if (pageGridColumnSize < PAGE_GRID_COLUMN_MAX_SIZE) {
				pageGridColumnSize += (pageGridColumnSize > 50) ? 8 : 4;
			}
			if (headGridColumnSize < HEAD_GRID_COLUMN_MAX_SIZE) {
				headGridColumnSize += (headGridColumnSize >= 55) ? 12 : 5;
			}
			if (vtGridColumnSize < VT_GRID_COLUMN_MAX_SIZE) {
				vtGridColumnSize += (vtGridColumnSize >= 50) ? 10 : 8;
			}
			if (totalGridColumnSize < TOTAL_GRID_COLUMN_MAX_SIZE) {
				totalGridColumnSize += (totalGridColumnSize >= 65) ? 13 : 5;
			}
			
		}
		
		/**
		 * Reduz a fonte da DataGrid que lista as laudas do espelho.
		 */
		public function decGrid():void {
			if (gridFontSize > GRID_MIN_FONT_SIZE) {
				gridFontSize -= (gridFontSize >= 20) ? 4 : 2;
			}
			if (pageGridColumnSize > PAGE_GRID_COLUMN_MIN_SIZE) {
				pageGridColumnSize -= (pageGridColumnSize > 50) ? 8 : 4;
			}
			if (headGridColumnSize > HEAD_GRID_COLUMN_MIN_SIZE) {
				headGridColumnSize -= (headGridColumnSize > 55) ? 12 : 5;
			}
			if (vtGridColumnSize > VT_GRID_COLUMN_MIN_SIZE) {
				vtGridColumnSize -= (vtGridColumnSize >= 50) ? 10 : 8;
			}
			if (totalGridColumnSize > TOTAL_GRID_COLUMN_MIN_SIZE) {
				totalGridColumnSize -= (totalGridColumnSize >= 65) ? 13 : 5;
			}
		}
		
		/**
		 * Aumenta o tamanho da fonte do visualizador da lauda selecionada e da 
		 * próxima lauda na lista.
		 */
		public function incStoryFontSize():void {
			if (storyFontSize < STORY_MAX_FONT_SIZE) {
				storyFontSize += 2;
			}
		}
		
		/**
		 * Reduz o tamanho da fonte do visualizador da lauda selecionada e da 
		 * próxima lauda na lista.
		 */
		public function decStoryFontSize():void {
			if (storyFontSize > STORY_MIN_FONT_SIZE) {
				storyFontSize -= 2;
			}
		}

		/**
		 * Salva quaisquer alterações nas configurações que tenham sido feitas.
		 */
		public function save():void {
			if (changed) {
				localData.save(this);
				changed = false;
				
				trace("Saved { gridFontSize:", gridFontSize, ", storyFontSize:", storyFontSize, ", gridWidth:", gridWidth, "firstViewerHeight:", nextViewerHeight, "}");
			}
		}
		
		public function reset():void {
			this.nextViewerHeight = DEFAULT_FIRST_VIEWER_HEIGHT;
			this.gridWidth = DEFAULT_GRID_WIDTH;
			this.gridFontSize = DEFAULT_GRID_FONT_SIZE;
			this.storyFontSize = DEFAULT_STORY_FONT_SIZE;
			this.pageGridColumnSize = DEFAULT_PAGE_GRID_COLUMN_SIZE;
			this.headGridColumnSize = DEFAULT_HEAD_GRID_COLUMN_SIZE;
			this.vtGridColumnSize = DEFAULT_VT_GRID_COLUMN_SIZE;
			this.totalGridColumnSize = DEFAULT_TOTAL_GRID_COLUMN_SIZE;
			save();
		}
		
		//----------------------------------------------------------------------
		//	Private Methods
		//----------------------------------------------------------------------
		
		private function registerChange(before:int, after:int):void {
			// Se já teve alteração não precisa verificar se mudou novamente...
			if (!changed) {
				changed = before != after;
			}
		}
	}
}
