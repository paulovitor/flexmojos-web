import flash.events.Event;
import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.agenda.AgendaArea;
import tv.snews.anews.view.agenda.ContactForm;
import tv.snews.flexlib.events.PageEvent;
import tv.snews.flexlib.utils.StringUtils;

private var contactService:ContactService = ContactService.getInstance();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Contact")]
private var contacts:ArrayCollection = new ArrayCollection();

[Bindable] private var timerContact:Timer = new Timer(1000, 1);
[Bindable] private var selectedContact:AgendaContact = null;

private var currentText:String = null;

private function initApp():void {
	timerContact.addEventListener(TimerEvent.TIMER_COMPLETE, updateContactList);
	updateContactList();
	
	// Pré-carrega os grupos de contatos para evitar problemas de delay
	DomainCache.contactGroups;
}

private function onContactPagePendingHandler(event:PageEvent):void {
	if (StringUtils.isNullOrEmpty(txtSearch.text)){
		//Todos
		contactService.findAll(event.firstResult, event.maxResults, onListContactSuccess, onListContactFailed);
	} else {
		//Todos pelo nome
		contactService.findByName(txtSearch.text, event.firstResult, event.maxResults, onListContactSuccess, onListContactFailed);
		//contactService.findByNamePaginated(txtSearch.text, event.firstResult, event.maxResults, onListContactSuccess, onListContactFailed);
	}
}

private function saveHandler(event:CrudEvent):void {
	this.currentState = "view";
	updateContactList();
}

private function edit():void {
	if (selectedContact) {
		var _this:AgendaArea = this;

		contactService.findAgendaContactById(selectedContact.id,
				function(event:ResultEvent):void {
					_this.currentState = "edit";
					_this.form.contact = AgendaContact(event.result);
				}
		);
	}
}

/**
 * Atualiza a grid de contatos.
 */
private function updateContactList(event:TimerEvent = null):void {
//	if (!event) {
//		txtSearch.text = "";
//	}
	if (StringUtils.isNullOrEmpty(txtSearch.text))  {
		refreshAllContactList();
	} else {
		refreshByNameContactList(txtSearch.text);
	}
	selectedContact = null;
}

/**
 * Atualiza o número de registros da grid pelo nome ou parte do nome do contato.
 */
private function refreshByNameContactList(name:String):void {
	if (allContactList.count != 0) {
		allContactList.clear();
	}
	contactService.countByName(name, function(event:ResultEvent):void {
		allContactList.count = event.result as int;
	});
}


/**
 * Atualiza o número de registros da grid do contato.
 */
private function refreshAllContactList():void {
	if (allContactList.count != 0) {
		allContactList.clear();
	}
	contactService.countAll(function(event:ResultEvent):void {
		allContactList.count = event.result as int;
	});
}

private function onSearchTextChange(event:TextOperationEvent):void {
	if (txtSearch.text != currentText) {
		currentText = txtSearch.text;
		timerContact.stop();
		timerContact.start();
	}
}

private function confirmationDellContact(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		//After remove, save it.
		contactService.del(selectedContact, onDeleteContactGroup);
		this.currentState = 'view';
	}
}

private function onDeleteContactGroup(event:ResultEvent):void {
	updateContactList();
	ANews.showInfo(this.resourceManager.getString('Bundle' , "defaults.msg.deleteSuccess"), Info.INFO);
}

public function deleteContact():void {
	Alert.show(
		this.resourceManager.getString('Bundle', 'defaults.msg.confirmAllDelete'), 
		this.resourceManager.getString('Bundle', 'defaults.msg.confirmation'), 
		Alert.NO | Alert.YES, null, confirmationDellContact);
}

private function newContact():void {
	this.currentState = 'edit';
	
	ContactForm(form).contact = new AgendaContact();
	ContactForm(form).contact.name = txtSearch.text;
	allContactList.selectedIndex = -1;
}

private function onFormCancel(event:Event):void {
	this.currentState = "view";
	updateContactList();
}

/**
 * Handler para o carregamento bem sucedido da lista de contatos.
 */
private function onListContactSuccess(event:ResultEvent, token:Object = null):void {
	var contactList:ArrayCollection = event.result as ArrayCollection;
	if (contactList && contactList.length > 0) {
		allContactList.appendItems(contactList, token as int);
	} else {
		allContactList.clear();
	}
}

// -----------------------------
// Asynchronous answers
// -----------------------------

/**
 * Handler para a falha de carregamento da lista de contatos.
 */
private function onListContactFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(this.resourceManager.getString('Bundle' , "agenda.msg.failedToLoadContacts"), Info.ERROR);
}

/**
 * Handler para a falha de carregamento da lista de group contatos.
 */
private function onListContactGroupFailed(event:FaultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle' , "agenda.failedToLoadContactGroup"), Info.ERROR);
}
