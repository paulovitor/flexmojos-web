package tv.snews.anews.view.navigator {

	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	
	import spark.components.supportClasses.SkinnableComponent;

	[SkinState("normal")]
	[SkinState("preview")]
	[SkinState("edition")]

	public class SearchFiltersBar extends SkinnableComponent {

		private var _searchFiltersList:ArrayCollection = new ArrayCollection();

		public function SearchFiltersBar() {
			super();
			setStyle("skinClass", SearchFiltersBarSkin);
		/*_searchFiltersList.addEventListener(CollectionEvent.COLLECTION_CHANGE, onSearchFiltersListChange);*/
		}

		[ArrayElementType("tv.snews.anews.view.navigator.SearchFilter")]
		[Bindable] public function get searchFiltersList():ArrayCollection {
			return _searchFiltersList;
		}

		public function set searchFiltersList(value:ArrayCollection):void {
			_searchFiltersList = value;
		}

		public function selectFilterHandler(event:SelectSearchEvent):void {
			var filter:SearchFilter = event.selectedItem as SearchFilter;
			if (!searchFiltersList.contains(filter))
				addFilter(filter);
		}

		private function addFilter(filter:SearchFilter):void {
			searchFiltersList.addItem(filter);
		}

		private function removeFilter(filter:SearchFilter):void {
			if (searchFiltersList.length > 0) {
				searchFiltersList.removeItemAt(searchFiltersList.getItemIndex(filter));
			}
		}


		private function onSearchFiltersListChange(event:CollectionEvent = null):void {
			switch (event.kind) {

				case CollectionEventKind.ADD:


					break;
				case CollectionEventKind.REMOVE:

					break;
			}
		}

	}
}
