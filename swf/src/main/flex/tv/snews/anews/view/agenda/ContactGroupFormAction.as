import flash.events.Event;

import mx.messaging.messages.ErrorMessage;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.ContactGroup;
import tv.snews.anews.service.ContactGroupService;
import tv.snews.anews.utils.*;

private const service:ContactGroupService = ContactGroupService.getInstance();

private var selected:ContactGroup = null;

private function onSaveContactGroup(event:Event):void {
	if (!Util.isValid(validators)) {
		requiredFields();
	} else {
		selected = selected || new ContactGroup();
		selected.name = txtContactGroup.text
		saveButton.enabled = false;
		service.saveContactGroup(selected, onSucessSaveContactGroup, onFault);
	}
}

private function onSucessSaveContactGroup(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle' , "defaults.msg.saveSuccess"), Info.INFO);
	
	txtContactGroup.text = "";
	saveButton.enabled = true;
	selected = null;
	contactGroupList.enabled = true;
	this.currentState = "normal";
}

private function onNewContactGroup():void {
	contactGroupList.enabled = true;
	selected = null;
	this.currentState = "normal";
	txtContactGroup.text = "";
	txtContactGroup.setFocus();
}

public function editContactGroup(contactGroup:ContactGroup):void {
	contactGroupList.enabled = false;
	this.currentState = "edit";
	
	selected = ContactGroup(ObjectUtil.clone(contactGroup));
	txtContactGroup.text = contactGroup.name;
	txtContactGroup.setFocus();
}

public function deleteContactGroup(contactGroup:ContactGroup):void {
	service.deleteById(contactGroup.id, onDeleteContactGroup, onFault);
}

private function onDeleteContactGroup(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle' , "defaults.msg.deleteSuccess"), Info.INFO);
}

//-----------------------------
//	Asynchronous answers
//-----------------------------

private function onFault(event:FaultEvent):void {
	saveButton.enabled = true;
	
	var error:ErrorMessage =	event.message as ErrorMessage;
	var split:Array =	error.faultString.split(" : ");
	if (split && split.length > 1) {
		ANews.showInfo(split[1], Info.WARNING);	
	} else {
		ANews.showInfo(this.resourceManager.getString('Bundle' , "client.sendError"), Info.WARNING);
	}
	
}

private function requiredFields():void {
	ANews.showInfo(this.resourceManager.getString('Bundle' , "defaults.msg.requiredFields"), Info.WARNING);
}
