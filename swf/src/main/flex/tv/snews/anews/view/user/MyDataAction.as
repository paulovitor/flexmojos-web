
import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.utils.TextFlowUtil;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.service.LoginService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.StringUtils;

private const MIN_MASK:String = "00";
private const SEC_MASK:String = "00";
private const MS_MASK:String = "000";
private const TIMER_INTERVAL:int = 10;

[Bindable] private var readTimer:Timer;
[Bindable] public var user:User;

private var baseTimer:int;

public function startUp():void {
	textArea.textFlow = spark.utils.TextFlowUtil.importFromString(ResourceManager.getInstance().getString('Bundle', 'mydata.textToRead'));

	readTimer = new Timer(TIMER_INTERVAL);
	readTimer.addEventListener(TimerEvent.TIMER, updateTimer);

	LoginService.getInstance().getCurrentUser(onGetCurrent);
}

private function updateTimer(evt:TimerEvent):void {
	var d:Date = new Date(getTimer() - baseTimer);
	var min:String = (String(MIN_MASK + d.minutes)).substr(-MIN_MASK.length);
	var sec:String = (String(SEC_MASK + d.seconds)).substr(-SEC_MASK.length);
	var ms:String = (String(MS_MASK + d.milliseconds)).substr(-MS_MASK.length);
	counter.text = String(min + ":" + sec + "." + ms);
}

private function startTimer():void {
	if (readTimer.running)
		readTimer.stop();
	
	baseTimer = getTimer();
	readTimer.start();
	iniciarLeitura.enabled = false;
	btnSave.enabled = false;
	btnCancel.enabled = false;
}

private function stopTimer():void {
	readTimer.stop();

	// Getting the time based on the text field value;
	var currentTime:Date = dateUtil.toDate(counter.text, "NN:SS.QQQ");

	// How many seconds the user spend to read the text?
	// Why in seconds? 
	// Because all calculation I've ever seen on the system is second base
	// and it doesn't matter because I include the milliseconds on this calculation;
	var readTimeInSecs:Number = (currentTime.getMinutes() * 60) + (currentTime.getSeconds()) + (currentTime.getMilliseconds() / 1000);

	var textRead:String = StringUtils.trim(ResourceManager.getInstance().getString('Bundle', 'mydata.textToRead'));
	textRead = textRead.replace(/\n\t\r/g, "");

	// readTime is a fraction of time that user spend to read each character,
	// so I just divide the total of second by the total of letter we have.
	user.readTime = readTimeInSecs / textRead.length;

	// changing the precision of the readTime value
	var readTimeAsString:String = numberFormatter.format(user.readTime);
	user.readTime = new Number(readTimeAsString);
	
	iniciarLeitura.enabled = true;
	btnSave.enabled = true;
	btnCancel.enabled = true;
}

protected function onReadTimeChange():void {
	var readTime:Number = new Number(userTimer.text);
	if (isNaN(readTime)) {
		readTime = 0;
	}

	// changing the precision of the readTime value
	user.readTime = new Number(numberFormatter.format(readTime));
	
	if (isNaN(user.readTime)) {
		user.readTime = 0;
	}
}

public function validPermission():Boolean {
	return ANews.USER.allow('01030101');
}

// -------------------------------
// Events.
// -------------------------------
private function save():void {
	if (!Util.isValid(validadores)) {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
	} else {
		UserService.getInstance().saveData(user, txtPassword.text, txtNewPassword.text, txtConfirmation.text, onSaveSuccess, OnSaveError);
	}
}


private function edit():void {
	this.currentState = "edition";
}

private function cancel():void {
	LoginService.getInstance().getCurrentUser(onGetCurrent);
	readTimer.reset();
	txtPassword.text = '';
	txtNewPassword.text = '';
	txtConfirmation.text = '';
	
	counter.text = String("00:00:000");

	this.currentState = 'view';
}

// --------------------------
// Asynchronous answers
// --------------------------
private function onGetCurrent(event:ResultEvent):void {
	user = event.result as User;
}

private function onSaveSuccess(event:ResultEvent):void {
	var result:String = event.result as String;

	if (result == "INVALID_EMAIL") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'user.msg.userEmailExist'), Info.WARNING);
	} else if (result == "INVALID_NICKNAME") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'user.msg.nickNameExist'), Info.WARNING);
	} else if (result == "PASSWORD_NOT_MATCH") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.passwordNotMatch'), Info.WARNING);
	} else if (result == "PASSWORD_AND_CONFIRMATION_NOT_MATCH") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.passwordAndConfirmationNotMatch'), Info.WARNING);
	} else if (result == "NEW_PASSWORD_INVALID") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.newPasswordInvalid'), Info.WARNING);
	} else if (!StringUtils.isNullOrEmpty(result)) {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'user.msg.validPassword'), Info.WARNING);
	} else {
		txtPassword.text = '';
		txtNewPassword.text = '';
		txtConfirmation.text = '';
		
		ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
		this.currentState = "view";
		
		// Atualizações necessárias para ANews.USER
		ANews.USER.readTime = user.readTime;
	}
}

private function OnSaveError(event:FaultEvent):void {
	ANews.showInfo(event.fault.faultString, Info.ERROR);
}

//---------------------------
// Helpers
// --------------------------
private function groupAsString(groups:ArrayCollection):String {
	var grupoAsString:String = "";
	for each (var group:tv.snews.anews.domain.UserGroup in groups) {
		grupoAsString += group.name + ", ";
	}

	if (grupoAsString.length > 0) {
		grupoAsString = grupoAsString.substring(0, grupoAsString.length - 2);
	}
	return grupoAsString;
}
