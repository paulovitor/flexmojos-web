package tv.snews.anews.view.chat {

	import flash.events.Event;

	import mx.collections.ArrayList;

	import tv.snews.anews.domain.User;

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class ChatEvent extends Event {

		public static const OPEN_CHAT:String = "open_chat";

		public var users:ArrayList = new ArrayList();

		public function ChatEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
		}

		public function addUser(user:User):void {
			this.users.addItem(user);
		}
	}
}
