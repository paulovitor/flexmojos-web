import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import flash.utils.ByteArray;

import mx.core.FlexGlobals;
import mx.events.FlexEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import flash.net.SharedObject

import spark.components.CheckBox;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Permission;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.service.GroupService;
import tv.snews.anews.service.LoginService;
import tv.snews.anews.service.SettingsService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.utils.*;

private const settingsService:SettingsService = SettingsService.getInstance();

[Bindable] private var readOnlyTerm:Boolean = false;

[Bindable] private var settings:Settings;
[Bindable]
[Embed(source = "/assets/banner_login.jpg")]
public var DefaultLoginImage:Class;

private const groupService:GroupService = GroupService.getInstance();
private const loginService:LoginService = LoginService.getInstance();
private const userService:UserService = UserService.getInstance();

private const bundle:IResourceManager = ResourceManager.getInstance();

private var userToAgree:User = null;

public function startUp(event:FlexEvent):void {
	settingsService.loadSettings(onSettingsResult);
}

public function getCookieUser():String {
    var username:SharedObject = SharedObject.getLocal("cookieUsername");
    return username.size > 0 ? username.data.name : "";
}

private function onKeyDown(event:KeyboardEvent):void {
	if (ModeCheck.isDebugBuild()) {
		if (event.ctrlKey && event.shiftKey) {
			if (event.keyCode == Keyboard.L) {
				emailInput.text = "admin@snews.tv";
				passwordInput.text = "administrador";
				onLoginClick();
			}
			if (event.keyCode == Keyboard.K) {
				emailInput.text = "suporte@snews.tv";
				passwordInput.text = "radio@TV";
				onLoginClick();
			}
            if (event.keyCode == Keyboard.U) {
                emailInput.text = "user@snews.tv";
                passwordInput.text = "123456";
                onLoginClick();
            }
        }
	}
}

public function onSettingsResult(event:ResultEvent):void {
	settings = event.result as Settings;
	DomainCache.settings = settings;
}

/**
 * Chamado quando o usuário já concordou com os termos de uso e a data de 
 * expiração da senha ainda não foi ultrapassada.
 */
private function doLogin(user:User):void {
	loginService.login(user, 
		function(event:ResultEvent):void {
			ANews.USER = User(event.result);
			ANews.USER.online = true;
			
			for each (var group:UserGroup in ANews.USER.groups) {
				for each (var permission:Permission in group.permissions) {
					ANews.PERMISSION.addItem(permission.id);
				}
			}
			
			ANews(FlexGlobals.topLevelApplication).currentState = "System";

		}
	);
}

private function onAgreeClick():void {
    loginService.agreeWithTerms(userToAgree.id, onAgreement, onFault);
}

private function onLoginClick():void {
    var username:String = emailInput.text;
    if(emailInput.text.indexOf("@") == -1 && DomainCache.settings.defaultDomain){
        username += "@" + DomainCache.settings.defaultDomain;
    }
	userService.loadByCredentials(username, passwordInput.text, onLoadByCredentialsSuccess);

    var cookieUsername:SharedObject = SharedObject.getLocal("cookieUsername");
    if(chkRememberUser.selected) {
        cookieUsername.data.name = emailInput.text;
    } else {
        cookieUsername.clear();
    }
}

private function onLoadByCredentialsSuccess(event:ResultEvent):void {
	var user:User = event.result as User;
	if (user == null) {
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.invalidEmail'), Info.WARNING);
	} else if (user.isSupportUser()) {
		// Usuário suporte não precisa ser validado
		doLogin(user);
	} else if (!user.isAccountNonLocked()) {
		// Ainda não concordou com os termos de uso
		this.currentState = "Agreement";
		userToAgree = user;
	} else if (!user.isAccountNonExpired()) {
		// A senha expirou
		this.currentState = "PasswordExpired";
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.passwordExpired'), Info.WARNING);
	} else {
		doLogin(user);
	}
}

private function onForgotPasswordClick(event:MouseEvent):void {
	this.currentState = "ForgotMail";
	txtEmailForgot.setFocus();
}

private function onRecoverPasswordClick():void {
	if (Util.isValid(validadores)) {
		userService.recoveryPassword(txtEmailForgot.text, onRecoveryPasswordSucess, onFault);
	} else {
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.invalidEmail'), Info.WARNING);
	}
}

private function onBackToLoginClick():void {
	emailInput.text = "";
	passwordInput.text = "";
	this.currentState = "Login";
}

protected function onLoginTextInputKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER) {
		if (emailInput.text == "" || passwordInput.text == "") {
			ANews.showInfo(bundle.getString('Bundle', 'login.msg.invalidEmail'), Info.WARNING);
		} else {
			onLoginClick();
		}
	}
}

protected function onPasswordConfirmationKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER) {
		if ((emailInput.text == "") || (passwordConfirmationInput.text == "")) {
			ANews.showInfo(bundle.getString('Bundle', 'login.msg.invalidEmail'), Info.WARNING);
		} else {
			onChangePasswordClick();
		}
	}
}

private function onChangePasswordClick():void {
	if (emailInput.text == "") {
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.invalidEmail'), Info.WARNING);
	} else if (passwordInput.text == "" || passwordConfirmationInput.text == "") {
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.invalidPassword'), Info.WARNING);
	} else if (newPasswordInput.text != passwordConfirmationInput.text) {
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.passwordNotMatch'), Info.WARNING);
	} else if (!Util.isValid(forcePassword)) {
		ANews.showInfo(bundle.getString('Bundle', 'login.msg.weakPassword'), Info.WARNING);
	} else {
		userService.changePassword(emailInput.text, passwordInput.text, newPasswordInput.text, 
			function(event:ResultEvent):void {
				var changedUser:User = User(event.result);
				doLogin(changedUser);
			}, 
			onFault);
		passwordInput.text = passwordConfirmationInput.text;
	}
}

private function onAgreeCheckboxClick(evn:MouseEvent):void {
	lnkbOK.visible = (CheckBox(evn.target).selected);
    lnkbOK.setFocus();
}

// -----------------------------
// Asynchronous answers
// -----------------------------

private function onFault(event:FaultEvent):void {
	var chandePasswordException:String = event.fault.faultString as String;
	var chandePasswordExceptionMessage:String = event.fault.rootCause.message as String;

	if (chandePasswordException.indexOf('InvalidPassword') != -1 || chandePasswordException.indexOf('DataNotFound') != -1 || chandePasswordException.indexOf('InvalidEmail') != -1) {
		ANews.showInfo(chandePasswordExceptionMessage, Info.INFO);
	} else {
		ANews.showInfo(chandePasswordExceptionMessage, Info.WARNING);
	}
}

private function onRecoveryPasswordSucess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', 'login.msg.successEmail'), Info.INFO);
	this.currentState = "Login";
}

private function onAgreement(event:ResultEvent):void {
	var user:User = event.result as User;
	if (user != null) {
		doLogin(user);
	}
}
