import flash.events.MouseEvent;
import flash.text.Font;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.controls.Alert;
import mx.events.FlexEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.service.SettingsService;

private const fonts:ArrayList = new ArrayList(Font.enumerateFonts(true).sortOn("fontName"));
private const settingsService:SettingsService = SettingsService.getInstance();

[Bindable] private var settings:Settings;

protected function creationCompleteHandler(event:FlexEvent):void {
	settingsService.loadSettings(onLoadSettingsHandler);
}

private function onLoadSettingsHandler(event:ResultEvent):void {
	settings = event.result as Settings;
	selectEditorFont();
}

private function selectEditorFont():void {
//	for (var i:int = 0; i < fonts.length; i++) {
//		if (fonts.getItemAt(i).fontName == settings.editorFontFamily) {
//			fontsCombo.selectedIndex = i;
//			return;
//		}
//	}
//	fontsCombo.selectedIndex = -1;
}

protected function onSaveButtonClick(event:MouseEvent):void {
	settingsService.updateSettings(settings, onUpdateResultHandler);
}

private function onUpdateResultHandler(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', 'settings.msg.saveSuccess'), Info.INFO);
}
