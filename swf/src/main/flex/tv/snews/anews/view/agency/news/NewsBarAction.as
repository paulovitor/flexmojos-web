import flash.display.DisplayObjectContainer;

import mx.collections.ArrayCollection;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.domain.News;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.NewsService;
import tv.snews.anews.view.agency.Agency;
import tv.snews.flexlib.utils.*;
import tv.snews.anews.utils.*;

[Bindable] private var news:ArrayCollection = new ArrayCollection();

private const dateTimeUtil:DateUtil = new DateUtil();

private function startUp():void {
	NewsService.getInstance().listAll(onListSuccess);
	NewsService.getInstance().subscribe(onMessage);
}

//----------------------------
// Assynchronous call.
//----------------------------
private function onListSuccess(event:ResultEvent):void {
	news = event.result as ArrayCollection;
}

private function onMessage(event:MessageEvent):void {
	var headers:Object = event.message.headers;
	var entity:News = News(event.message.body);
	var index:int = 0;
	
	switch(headers.type) {
		case "INSERTED": {
			news.addItemAt(entity,0);
			break;
		}
		case "DELETED": {
			index = findNewsIndex(entity);
			if(index < 0)
				break;
			
			news.removeItemAt(index);
			//TODO sort item
			break;
		}
		case "UPDATED": {
			index = findNewsIndex(entity);
			if(index < 0)
				break;
			
			var atual:News = news.getItemAt(index) as News;
			atual.address = entity.address;
			atual.contacts = entity.contacts;
			atual.date = entity.date;
			atual.font = entity.font;
			atual.information = entity.information;
			atual.lead = entity.lead;
			atual.slug = entity.slug;
			atual.user = entity.user;
			
			break;
		}
		default: {
			break;
		}
	}
}

public function dispose():void {
	NewsService.getInstance().unsubscribe(onMessage);
}

//----------------------------
// Helpers.
//----------------------------
private function findNewsIndex(entity:News):int {
	for each(var _news:News in news) {
		if(_news.id == entity.id){
			return news.getItemIndex(_news);
		}
	}
	return -1;
}

private function editNews():void {
	if(newsViewer.selectedIndex != -1){
		var news:News = newsViewer.selectedItem as News;
		var container:DisplayObjectContainer = this.parent;
		while(container!=null && !(container is Agency)) {
			container = container.parent;
		}
		
		if(container == null)
			return;
		
		var agency:Agency = container as Agency;
		agency.tabBar.dispatchEvent(new CrudEvent(CrudEvent.CREATE, news));
	}
}

private function editNewsRecent():void {
	if(news.length > 0 && news.getItemAt(0).id > 0){
		var container:DisplayObjectContainer = this.parent;
		while(container!=null && !(container is Agency)) {
			container = container.parent;
		}
		
		if(container == null)
			return;
		
		var agency:Agency = container as Agency;
		agency.tabBar.dispatchEvent(new CrudEvent(CrudEvent.CREATE, news.getItemAt(0)));
	}
}

//----------------------------
// Formatters.
//----------------------------
private function formatHour(date:Date):String {
	return dateTimeUtil.dateToString(date);
}

private function formatDate(date:Date):String {
	return dateTimeUtil.toString(date, this.resourceManager.getString('Bundle', 'format.D_MMM'));
}

private function stripHtml(str:String):String {
	return StringUtils.stripHtmlTags(str);
}