import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import flash.utils.setTimeout;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import spark.components.gridClasses.GridColumn;
import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Story;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.flexlib.events.PageEvent;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

[Bindable] private var selectedStory:Story;

private const dateUtil:DateUtil = new DateUtil();
private const storyService:StoryService = StoryService.getInstance();

private const domainCache:DomainCache = null; // Apenas garante o import para o arquivo mxml

//------------------------------------------------------------------
//	Event Handlers
//------------------------------------------------------------------

private function onContentCreationComplete(event:FlexEvent):void {
	loadExcludedStories();
	storyService.subscribe(onStoryMessageReceived);
}

private function onRemove(event:Event):void {
	storyService.unsubscribe(onStoryMessageReceived);
	if(storyViewer) {
		storyViewer.onRemove();
	}
}

private function onStoryMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	if (action == EntityAction.DELETED || action == EntityAction.RESTORED) {
		setTimeout(loadExcludedStories, 1000);
	}
}

private function onSelectionChange(event:GridSelectionEvent):void {
	selectedStory = excludedGrid.selectedItem as Story;
	if (selectedStory) {
		this.currentState = "selected";
		storyViewer.story = selectedStory;
	} else {
		this.currentState = "normal";
	}
}

private function onRestoreClick(event:MouseEvent):void {
	restoreTo.visible = true;

	restoreTo.includeInLayout = true;
}

private function onConfirmRestoreClick():void {
	var story:Story = excludedGrid.selectedItem as Story;
	var date:Date = destinyDay.selectedDate;
	var program:Program = destinyProgram.selectedItem as Program;

	if (date && program) {
		storyService.restoreExcludedStory(story.id, date, program.id, function(event:ResultEvent):void {
			var resultCode:String = event.result as String;
			var resourceManager:IResourceManager = ResourceManager.getInstance();

			switch (resultCode) {
				case "OK":
					ANews.showInfo(resourceManager.getString("Bundle", "rundown.trash.restoreSuccess"), Info.INFO);
					loadExcludedStories();
					restoreTo.visible = false;
					restoreTo.includeInLayout = false;
					break;
				case "RUNDOWN_NOT_FOUND":
					ANews.showInfo(resourceManager.getString("Bundle", "rundown.trash.noRundownError"), Info.WARNING);
					break;
			}
		});
	} else {
		ANews.showInfo(this.resourceManager.getString("Bundle", "defaults.msg.requiredFields"), Info.WARNING);
	}
}

private function onCancelRestoreClick():void {
	restoreTo.visible = false;
	restoreTo.includeInLayout = false;
}

private function onNormalState():void {
	excludedGrid.selectedIndex = -1;
	this.currentState = "normal";
}

private function search(page:int = 1):void {
	page = page < 1 ? 1 : page;
	//Caso nenhuma lauda esteja nedo restaurada, permite a troca de páginas
    if(!restoreTo.visible) {
        selectedStory = null;
        this.currentState = "normal";
        storyService.listExcludedStories(page, onListSuccess);
    }
}

private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	
	// Corrige laudas sem retranca
	var stories:ArrayCollection = result.results;
	var noSlug:String = '[ ' + ResourceManager.getInstance().getString("Bundle", "defaults.noSlug") + ' ]';
	for each (var story:Story in stories) {
		if (StringUtils.isNullOrEmpty(story.slug)) {
			story.slug = noSlug;
		}
	}
	
	pgStoryTrash.load(stories, result.pageSize, result.total, result.pageNumber);
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && excludedGrid.selectedItem != null && excludedGrid.selectedIndex != -1) {
		if (this.currentState == "selected") {
			this.currentState = "normal";
		} else {
			this.currentState = "selected";
		}
	}
}

//------------------------------------------------------------------
//	Helper Methods
//------------------------------------------------------------------

private function loadExcludedStories():void {
	this.currentState = "normal";
	search();
}

private function excludedLabelFunction(item:Object, column:GridColumn):String {
	var story:Story = item as Story;
	var authorNickname:String = story.author ? story.author.nickname : "";
	return authorNickname + " (" + dateUtil.fullDateToString(story.changeDate, true) + ")";
}
