import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.service.TwitterService;
import tv.snews.flexlib.utils.StringUtils;
import tv.snews.anews.utils.*;

private const twitterService:TwitterService = TwitterService.getInstance();

[ArrayElementType("tv.snews.anews.domain.TwitterUser")]
[Bindable] public var twitterUsers:ArrayCollection = new ArrayCollection();

private function initTwitter():void {
	twitterService.listAllTwitterUser(onListAllTwitterUser);
}

private function onListAllTwitterUser(event:ResultEvent):void {
	twitterUsers = event.result as ArrayCollection;
}

private function onCheckTwitterUser(event:ResultEvent):void {
	var twitterUser:TwitterUser = event.result as TwitterUser;
	
	if (twitterUser != null) {
		twitterService.saveTwitterUser(twitterUser, onSaveTwitterUser, onFault);
	}
}

private function onSaveTwitterUser(event:ResultEvent):void {
	ANews.showInfo(ResourceManager.getInstance().getString('Bundle' , "defaults.msg.saveSuccess"), Info.INFO);
	
	newTwitterUser.text = "";
	twitterService.listAllTwitterUser(onListAllTwitterUser, onFault);
}

private function saveTwitterUser(event:Event):void {
	if (!StringUtils.isNullOrEmpty(newTwitterUser.text)) {
		twitterService.findTwitterUserByScreenName(newTwitterUser.text, onGetTwitterUserByScreenName);
	}
}

private function onGetTwitterUserByScreenName(event:ResultEvent):void {
	var twitterUser:TwitterUser = event.result as TwitterUser;
	
	if (twitterUser != null) {
		ANews.showInfo(ResourceManager.getInstance().getString('Bundle' , "twitter.msg.alreadyRegistredTwitterUser"), Info.WARNING);
	} else {
		// Antes de Salvar verifica se o usuário existe no twitter.
		twitterService.checkTwitterUser(newTwitterUser.text, onCheckTwitterUser, onFault);
	}
}

private function onFault(event:FaultEvent, token:Object = null):void {
	var twitterException:String = event.fault.faultString as String;
	var twitterExceptionMessage:String = event.fault.rootCause.message as String;
    ANews.showInfo(twitterExceptionMessage, Info.ERROR);
}

public function deleteTwitterUser(twitterUser:TwitterUser):void {
	twitterService.deleteTwitterUser(twitterUser, onDeleteTwitterUser);
}

private function onDeleteTwitterUser(event:ResultEvent):void {
	ANews.showInfo(ResourceManager.getInstance().getString('Bundle' , "defaults.msg.deleteSuccess"), Info.INFO);
	twitterService.listAllTwitterUser(onListAllTwitterUser);
}
