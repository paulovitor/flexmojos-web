import flash.display.DisplayObject;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.events.StateChangeEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Media;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptVideo;
import tv.snews.anews.domain.ScriptVideo;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.service.MediaCenterService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.view.media.CreatePlaceholderWindow;
import tv.snews.anews.view.script.ScriptExpand;
import tv.snews.flexlib.utils.DateUtil;

private const scriptService:ScriptService = ScriptService.getInstance();
private const mediaCenterService:MediaCenterService = MediaCenterService.getInstance();
private const bundle:IResourceManager = ResourceManager.getInstance();
private const dateUtil:DateUtil = new DateUtil();

[Bindable] public var playout:MosDevice;
[Bindable] public var collapsed:Boolean = true;

private var mediasAdded:int;

//------------------------------
//	Properties
//------------------------------

private var program:Program;

private var _script:Script;

[Bindable]
public function get script():Script {
	return _script;
}

public function set script(value:Script):void {
	_script = value;
	program = value ? value.program : null;
}

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Media")]
private var selectedMedias:ArrayCollection = new ArrayCollection();

//------------------------------
//	Event Handlers & Callbacks
//------------------------------

protected function this_creationCompleteHandler(event:FlexEvent):void {
	scriptService.subscribe(scriptService_messageHandler);
    scriptRenderer.addEventListener(CrudEvent.UPDATE, loadAndUpdateScriptVideo);
}

private function this_removedHandler(event:FlexEvent):void {
	scriptService.unsubscribe(scriptService_messageHandler);
    scriptRenderer.removeEventListener(CrudEvent.UPDATE, loadAndUpdateScriptVideo);
}

private function createVideoButton_clickHandler(event:MouseEvent):void {
	this.dispatchEvent(new ANewsPlayEvent(ANewsPlayEvent.CLOSE));

	lockScript();

	var createWindow:CreatePlaceholderWindow = new CreatePlaceholderWindow();
	createWindow.program = program;
	createWindow.slugSuggestion = script.slug;

	createWindow.addEventListener(CrudEvent.CREATE, createWindow_createHandler);
	createWindow.addEventListener(CloseEvent.CLOSE, unlockScript);

//	var mosMedia:MosMedia = new MosMedia();
//	mosMedia.createdBy = ANews.USER.nickname;
//	mosMedia.group = program.name;
//	mosMedia.slug = script.slug && script.slug.length > 32 ? script.slug.substring(0, 31) : script.slug;

//	var createWindow:CreateMosMediaWindow = new CreateMosMediaWindow();
//	createWindow.mosMedia = mosMedia;
//	createWindow.devicePlayout = program.playoutDevice as MosDevice;
//
//	createWindow.addEventListener(CrudEvent.CREATE, createWindow_createHandler);
//	createWindow.addEventListener(CloseEvent.CLOSE, unlockScript);

	PopUpManager.addPopUp(createWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
	PopUpManager.centerPopUp(createWindow);
}

//
//	Windows Handlers
//

private function crudEventCreateHandler(crudEvent:CrudEvent):void {
	var medias:ArrayCollection = crudEvent.entity as ArrayCollection;
	if (medias[0] is WSMedia) {
        selectedMedias = medias;
        createScriptWSVideo();
	} else {
		mediaCenterService.loadById(medias, mediaCenter_loadByIdHandler);
	}
}

private function createWindow_createHandler(crudEvent:CrudEvent):void {
	var data:Object = crudEvent.entity;

	var scriptVideo:ScriptVideo = new ScriptVideo();
	scriptVideo.media = data.media;
	scriptVideo.channel = data.channel;
	scriptVideo.script = script;
	scriptVideo.order = script.videos.length;

	mediasAdded = script.videos.length;

	scriptService.createScriptVideo(script.id, scriptVideo, scriptService_createSuccessHandler);
}

//
//	Script Service Handlers
//

private function scriptService_messageHandler(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var messageScript:Script = event.message.body as Script;

    if (action == EntityAction.UPDATED && messageScript && messageScript.equals(script) && !collapsed) {
        script.videos = messageScript.videos;
        script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
	}
}

private function scriptService_createSuccessHandler(event:ResultEvent):void {
	if (script.videos) {
		ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAddedSucess', [script.videos.length - mediasAdded ]), Info.INFO);
	} else scriptService.loadById(script.id,
			function(event:ResultEvent):void {
				ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAddedSucess', [Script(event.result).videos.length - mediasAdded ]), Info.INFO);
			}
	);
	unlockScript();
}

//
//	Media Center Service Handlers
//

private function mediaCenter_loadByIdHandler(event:ResultEvent):void {
    selectedMedias = event.result as ArrayCollection;
    createScriptWSVideo();
}

private function createScriptWSVideo():void {
    mediasAdded = script.videos.length;
    for each (var media:Media in selectedMedias) {
        if (script.existMediaInScriptVideos(media)) {
            ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAlreadyAdded', [media.slug]), Info.WARNING);
            continue;
        }

        var video:ScriptVideo = new ScriptVideo();
        video.media = media;
        script.addVideo(video);
    }

    script.updateVTTime();
    script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));

    if (script.videos.length - mediasAdded > 0) {
        scriptService.updateVideos(script, scriptService_createSuccessHandler);
    }
}

public function loadAndUpdateScriptVideo(event:CrudEvent):void {
    var scriptVideo:ScriptVideo = event.entity as ScriptVideo;
    scriptService.loadAndUpdateScriptVideo(script.id, scriptVideo, function (event:ResultEvent):void {
        ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.search.ws.SyncScriptSuccess', [ script.slug ? script.slug : '']), Info.INFO);
    });
}

//------------------------------
//	Helpers
//------------------------------

private function unlockScript(closeEvent:CloseEvent = null):void {
	scriptService.unlockEdition(script.id, null);
}

private function lockScript(closeEvent:CloseEvent = null):void {
	scriptService.lockEdition(script.id, ANews.USER.id, null);
}

private function hideOrShowVideos(event:StateChangeEvent):void {
	collapsed = !collapsed;
	script.expanderCollapsed = collapsed;

	if (collapsed) {
		script.videos.removeAll();
	} else {
		var self:ScriptExpand = this;

		self.enabled = false;
		scriptService.loadVideosByScriptId(script.id,
				function(event:ResultEvent):void {
					script.videos = event.result as ArrayCollection;
					script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
					self.enabled = true;
				}
		);
	}
}