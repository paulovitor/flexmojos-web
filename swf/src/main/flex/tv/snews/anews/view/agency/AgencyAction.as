import mx.events.FlexEvent;

import tv.snews.anews.domain.News;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.utils.*;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.view.agency.newness.Newness;
import tv.snews.anews.view.agency.news.NewsArea;
import tv.snews.anews.view.agency.round.RoundArea;
import tv.snews.anews.view.agency.rss.RssItemViewer;

protected function startUp(event:FlexEvent):void {
	var tabManager:TabManager = new TabManager();
	tabManager.tabNavigator = tabBar;
	
	if(ANews.USER.allow('01010101')) {
		tabManager.openTab(Newness, 'newness',  null, false);
	}
	if(ANews.USER.allow('01010201')) {
		tabManager.openTab(NewsArea, 'newsArea', null, false);
	}
	if(ANews.USER.allow('01010301')) {
		tabManager.openTab(RoundArea, 'roundForm', null, false);
	}
	
	if(tabBar.length != -1) {
		tabBar.selectedIndex = 0;
		tabBar.getChildAt(0).addEventListener(CrudEvent.READ, function(event:CrudEvent):void {
			var rssItemViewer:RssItemViewer = new RssItemViewer();
			rssItemViewer.item = event.entity as RssItem;
			rssItemViewer.label = rssItemViewer.item.title.length > 10 ? rssItemViewer.item.title.substring(0, 9) + "..." : rssItemViewer.item.title;
			tabBar.addItem(rssItemViewer);	
			tabBar.selectedIndex = tabBar.getChildIndex(rssItemViewer);
		});
	}
}

protected function onRemove(event:FlexEvent):void {
	var newsArea:NewsArea =	tabBar.getChildAt(1) as NewsArea;
	var roundArea:RoundArea =	tabBar.getChildAt(2) as RoundArea;
	
	if (newsArea) {
		newsArea.onRemove(null);
	}
	
	if (roundArea) {
		roundArea.onRemove(null);
	}
	tabBar.removeAllChildren();
}

protected function hideOrShow():void {
	this.currentState = this.currentState == "normal" ? "hidden" : "normal";
}
