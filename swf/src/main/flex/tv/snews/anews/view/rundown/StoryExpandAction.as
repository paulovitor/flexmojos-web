import flash.display.DisplayObject;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.events.StateChangeEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Media;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.StoryMosObject;
import tv.snews.anews.domain.StoryMosVideo;
import tv.snews.anews.domain.StorySection;
import tv.snews.anews.domain.StorySubSection;
import tv.snews.anews.domain.StoryWSVideo;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.MediaCenterService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.media.CreatePlaceholderWindow;
import tv.snews.flexlib.utils.DateUtil;

private const storyService:StoryService = StoryService.getInstance();
private const mediaCenterService:MediaCenterService = MediaCenterService.getInstance();
private const bundle:IResourceManager = ResourceManager.getInstance();
private const dateUtil:DateUtil = new DateUtil();

[Bindable]
public var collapsed:Boolean = true;

private var mediasAdded:int;

//------------------------------
//	Properties
//------------------------------

private var program:Program;

private var _story:Story;

[Bindable]
public function get story():Story {
	return _story;
}

public function set story(value:Story):void {
	_story = value;
	program = value ? value.program : null;
}

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Media")]
private var selectedMedias:ArrayCollection = new ArrayCollection();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.StorySubSection")]
private var videos:ArrayCollection = new ArrayCollection();

private var createWindow:CreatePlaceholderWindow;

//------------------------------
//	Event Handlers & Callbacks
//------------------------------

private function creationCompleteHandler(event:FlexEvent):void {
    storyService.subscribe(storyService_messageHandler);
    storyExpandRenderer.addEventListener(CrudEvent.UPDATE, crudEventUpdateHandler);

    storyService.loadVideosByStoryId(story.id, storyService_loadVideosHandler);
}

private function removeHandler():void {
    dispose();
    storyService.unsubscribe(storyService_messageHandler);
    storyExpandRenderer.removeEventListener(CrudEvent.UPDATE, crudEventUpdateHandler);

    videos = null;
    collapsed = true;
}

private function crudEventUpdateHandler(event:CrudEvent):void {
    var storyWSVideo:StoryWSVideo = event.entity as StoryWSVideo;
    storyService.loadAndUpdateStoryWSVideo(story.id ,storyWSVideo);
}

private function createVideoButton_clickHandler(event:MouseEvent):void {
	this.dispatchEvent(new ANewsPlayEvent(ANewsPlayEvent.CLOSE));

	lockStory();

	if (!createWindow) {
		createWindow = new CreatePlaceholderWindow();	
	}
	
	createWindow.program = program;
	createWindow.slugSuggestion = story.slug;

	createWindow.addEventListener(CrudEvent.CREATE, createWindow_createHandler);
	createWindow.addEventListener(CloseEvent.CLOSE, unlockStory);

//	var mosMedia:MosMedia = new MosMedia();
//	mosMedia.createdBy = ANews.USER.nickname;
//	mosMedia.group = program.name;
//	mosMedia.slug = story.slug && story.slug.length > 32 ? story.slug.substring(0, 31) : story.slug;
//
//	var createWindow:CreateMosMediaWindow = new CreateMosMediaWindow();
//	createWindow.mosMedia = mosMedia;
//	createWindow.devicePlayout = program.playoutDevice as MosDevice;
//	createWindow.addEventListener(CrudEvent.CREATE, createWindow_createHandler);
//	createWindow.addEventListener(CloseEvent.CLOSE, unlockStory);

	PopUpManager.addPopUp(createWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
	PopUpManager.centerPopUp(createWindow);
}

//
//	Windows Handlers
//
private function crudEventCreateHandler(crudEvent:CrudEvent):void {
    var medias:ArrayCollection = crudEvent.entity as ArrayCollection;
    if (medias[0] is WSMedia) {
        selectedMedias = medias;
    } else {
        mediaCenterService.loadById(medias, mediaCenterService_loadByIdHandler);
    }

    if (!story.displayed && !story.displaying && !story.block.rundown.displayed) {
        storyService.loadById(story.id, storyService_loadByIdHandler);
    } else {
        isDisplayed();
    }
}

private function createWindow_createHandler(crudEvent:CrudEvent):void {
	var data:Object = crudEvent.entity;

	if (story.displayed || story.block.rundown.displayed) {
		ANews.showInfo(bundle.getString("Bundle", "rundown.block.displayed"), Info.WARNING);
	} else if (story.displaying) {
		ANews.showInfo(bundle.getString("Bundle", "story.isDisplaying"), Info.WARNING);
	} else {
		if (data.media is MosMedia) {
			createStoryMosVideo(data);
		}
		if (data.media is WSMedia) {
			createStoryWSVideo(data);
		}
	}
}

private function createStoryMosVideo(data:Object):void {
	var storyMosVideo:StoryMosVideo = new StoryMosVideo();
	storyMosVideo.mosMedia = data.media;
	storyMosVideo.channel = data.channel;

	mediasAdded = videos ? videos.length : 0;

	storyService.createStoryMosVideo(story.id, storyMosVideo, storyService_saveSuccessHandler);
}

private function createStoryWSVideo(data:Object):void {
	var storyWsVideo:StoryWSVideo = new StoryWSVideo();
	storyWsVideo.media = data.media;
	storyWsVideo.channel = data.channel;

	mediasAdded = videos ? videos.length : 0;

	storyService.createStoryWSVideo(story.id, storyWsVideo, storyService_saveSuccessHandler);
}

//
//	Story Service Handlers
//
private function storyService_messageHandler(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var storyServer:Story = event.message.body as Story;

	if (action == EntityAction.UPDATED && !collapsed && story.equals(storyServer)) {
		if (storyServer.ncSection) {
			videos.removeAll();
			videos.addAll(storyServer.ncSection.subSectionsByVideoType());
		} else if (storyServer.vtSection) {
			videos.removeAll();
			videos.addAll(storyServer.vtSection.subSectionsByVideoType());
		} else {
			videos.removeAll();
		}
	}
}

private function storyService_loadByIdHandler(event:ResultEvent):void {
	story = event.result as Story;
	if (story.ncSection) {
		addSubSection(story.ncSection);
	} else if (story.vtSection) {
        addSubSection(story.vtSection);
	} else {
		story.addVT();
        addSubSection(story.vtSection);
	}
	unlockStory();
}

private function storyService_loadVideosHandler(event:ResultEvent):void {
	videos = event.result as ArrayCollection;

	var mosCount:int = 0;
	for each (var storyVideo:StorySubSection in videos) {
		storyVideo.name = Util.formatName(bundle.getString("Bundle", "story.mosObjTitle"), ++mosCount);
	}

	collapsed = false;
}

private function storyService_saveSuccessHandler(event:ResultEvent):void {
	if (story.ncSection) {
		ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAddedSucess', [story.ncSection.subSections.length - mediasAdded]), Info.INFO);
	}
	if (story.vtSection) {
		ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAddedSucess', [story.vtSection.subSections.length - mediasAdded]), Info.INFO);
	}
	unlockStory();
}

//
//	Media Center Service Handlers
//

private function mediaCenterService_loadByIdHandler(event:ResultEvent):void {
	selectedMedias = event.result as ArrayCollection;
}

//------------------------------
//	Helpers
//------------------------------

private function unlockStory(closeEvent:CloseEvent = null):void {
	storyService.unlockEdition(story.id, null);
    dispose();
}

private function lockStory(closeEvent:CloseEvent = null):void {
	storyService.lockEdition(story.id, ANews.USER.id, null);
}

private function isDisplayed():void {
    if (story.displayed || story.block.rundown.displayed) {
        ANews.showInfo(bundle.getString("Bundle", "rundown.block.displayed"), Info.WARNING);
    } else if (story.displaying) {
        ANews.showInfo(bundle.getString("Bundle", "story.isDisplaying"), Info.WARNING);
    }
}

private function addSubSection(section:StorySection):void {
	mediasAdded = section.subSections.length;
    section.addVideoSubSections(selectedMedias, story.program.playoutDevice);
    if (section.subSections.length - mediasAdded > 0) {
        story.videosAdded = true;
        story.updateTimes();
        storyService.save(story, storyService_saveSuccessHandler);
    }
}

private function dispose():void {
    if (createWindow) {
        createWindow.removeEventListener(CrudEvent.CREATE, createWindow_createHandler);
        createWindow.removeEventListener(CloseEvent.CLOSE, unlockStory);
        createWindow = null;
    }
}
