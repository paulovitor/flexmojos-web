import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import spark.components.gridClasses.GridColumn;
import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.flexlib.events.PageEvent;
import tv.snews.flexlib.utils.DateUtil;

private const checklistService:ChecklistService = ChecklistService.getInstance();
private const dateUtil:DateUtil = new DateUtil();

[Bindable] private var showViewer:Boolean = false;
[Bindable] private var selectedChecklist:Checklist;

private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	pgChecklistTrash.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function search(page:int=1):void {
	if(page < 1)
		page = 1;
    if(!restoreTo.visible) checklistService.listExcludedChecklistPage(page, onListSuccess);
}

private function onContentCreationComplete(event:FlexEvent):void {
	checklistService.subscribe(onMessageChecklistTrash);
}

private function onMessageChecklistTrash(event:MessageEvent):void {
	var action:String = event.message.headers.action;
	if (action == EntityAction.DELETED || action == EntityAction.RESTORED) {
		search();
		cancelRestoreTo();
	}
}

private function onRemove(event:Event):void {
	checklistService.unsubscribe(onMessageChecklistTrash);
}

private function onRestoreClick(event:MouseEvent):void {
	restoreTo.visible = true;
	restoreTo.includeInLayout = true;
}

private function confirmRestore():void {
		if (selectedChecklist.date != null) {
		checklistService.restoreExcluded(selectedChecklist.id, checklistDateRestoreTo.selectedDate,  function(event:ResultEvent):void {
			ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "checklist.trash.msg.restoreSuccess"), Info.INFO);
			search();
			cancelRestoreTo();
		});
	} else {
		ANews.showInfo(this.resourceManager.getString('Bundle', "defaults.msg.requiredFields"), Info.INFO);
	}
}

private function cancelRestoreTo():void {
	restoreTo.visible = false;
	restoreTo.includeInLayout = false;
	selectedChecklist = null;
	excludedGrid.selectedIndex = -1;
	showViewer = false;
	this.currentState = "normal";
}

private function onSelectionChange(event:GridSelectionEvent):void {
	selectedChecklist = excludedGrid.selectedItem as Checklist;
	if (selectedChecklist) {
		this.currentState = "selected";
	} else {
		this.currentState = "normal";
	}
}

private function departureLabelFunction(item:Object, column:GridColumn):String {
	const checkList:Checklist = item as Checklist;
	return dateUtil.timeToString(checkList.schedule);
}

private function excludedLabelFunction(item:Object, column:GridColumn):String {
	const checklist:Checklist = item as Checklist;
	return checklist.author.nickname + " (" + dateUtil.fullDateToString(checklist.modified, true) + ")";
}

private function onViewClick(event:MouseEvent):void{
	showViewer = true;
}

private function onCancelClick(event:MouseEvent):void {
	showViewer = false;
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && excludedGrid.selectedItem != null && excludedGrid.selectedIndex != -1) {
		if (showViewer == true) {
			showViewer = false;
		} else {
			showViewer = true;
		}
	}
}
