import flash.events.FocusEvent;

import spark.components.TextInput;

import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.service.ScriptPlanService;
import tv.snews.flexlib.utils.DateUtil;

private const dateUtil:DateUtil = new DateUtil(); //Contante de DateUtil para a formatação das horas.
private var scriptPlanService:ScriptPlanService = ScriptPlanService.getInstance(); //Seviço do roteiro utilizado para sincronizar hora atual com servidor.

[Bindable] private var oldValue:String;
[Bindable] public var scriptPlan:ScriptPlan;

// -----------------------
// Events handler
// -----------------------

private function focusFieldIn(event:FocusEvent):void {
    oldValue = TimeEditable(event.currentTarget).editor.text;
}

private function dateOut(event:FocusEvent):void {
    var input:TextInput = TimeEditable(event.currentTarget).editor;
    var date:Date = dateUtil.toDate(input.text, "JJ:NN:SS");

    if (date == null) {
        input.text = oldValue as String;
        return;
    } else {
        input.text = dateUtil.timeToString(date);
    }

    if (input.text == oldValue) {
        return;
    }

    var field:String = event.currentTarget.id;
    scriptPlanService.updateField(scriptPlan.id, field, date);
}

//------------------------------
//	Helpers
//------------------------------

private function formatTimeStr(time:Date):String {
	return dateUtil.toString(time, resourceManager.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}
