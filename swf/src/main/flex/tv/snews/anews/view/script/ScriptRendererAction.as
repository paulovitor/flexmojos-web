import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;

import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.events.StateChangeEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.CheckBox;
import spark.components.ComboBox;
import spark.components.List;
import spark.components.RichEditableText;
import spark.components.TextInput;
import spark.components.supportClasses.SkinnableTextBase;

import tv.snews.anews.component.Info;
import tv.snews.anews.component.SpellTextArea;
import tv.snews.anews.component.SpellTextInput;
import tv.snews.anews.component.TimeEditable;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.ScriptPlanService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.flexlib.utils.StringUtils;

private static const EDITOR:String = 'EDITOR';
private static const IMAGE_EDITOR:String = 'IMAGE_EDITOR';
private static const REPORTER:String = 'REPORTER';

private const scriptService:ScriptService = ScriptService.getInstance();
private const bundle:IResourceManager = ResourceManager.getInstance();

private var oldValue:Object;

[Bindable]
private var script:Script;

[Bindable]
[Embed(source = "/assets/section-dragdrop-lock.png")]
public var imgDragDropLock:Class;

[Bindable]
[Embed(source = "/assets/section-dragdrop.png")]
public var imgDragDrop:Class;

[Bindable]
[Embed(source = "/assets/collapse.png")]
public var imgCollapse:Class;

[Bindable]
[Embed(source = "/assets/collapseMouseOver.png")]
public var imgCollapseMouseOver:Class;

[Bindable]
[Embed(source = "/assets/expand.png")]
public var imgExpand:Class;

[Bindable]
[Embed(source = "/assets/expandMouseOver.png")]
public var imgExpandMouseOver:Class;

// --------------------
// Overrides
// --------------------
override public function set data(value:Object):void {
	super.data = value;
			script = value as Script;
	}

// ------------------
// Actions
// ------------------

private function onDragManualFocusOut(event:Event):void {
    doDragManual(RichEditableText(event.target));
}

private function onDragManualKeyDown(event:KeyboardEvent):void {
	event.stopPropagation();

	if (event.keyCode == Keyboard.ENTER) {
		doDragManual(RichEditableText(event.target));

		UIComponent(FlexGlobals.topLevelApplication).setFocus();
	}
}

private function doDragManual(input:RichEditableText):void {
    var value:String = input.text;

    if (StringUtils.isNullOrEmpty(value)) {
        input.text = oldValue as String;
    } else {
			var index:int = parseInt(value);
			if (isNaN(index) || index < 1 || index > script.block.scriptPlan.maxGlobalIndex) {
				input.text = oldValue as String;
			} else if (index != script.globalIndex) {
				var target:Script = script.block.scriptPlan.getScriptByGlobalIndex(index);

				scriptService.updatePosition(script.id, target.block.id, target.order, false);
			}
		}
}

private function onChangeDisplaying(event:Event):void {
    var field:String = RadioButton(event.currentTarget).id;
    var selected:Boolean = RadioButton(event.currentTarget).selected;
    scriptService.updateField(script.id, field, selected);
}

public function deleteScript():void {
	if (script.editingUser) {
		ANews.showInfo(bundle.getString("Bundle", "scriptarea.script.lockedMessage"), Info.WARNING);
	} else {
		Alert.show(bundle.getString('Bundle', 'scriptarea.script.confirmDelete'), bundle.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onDeleteConfirm);
	}
}

public function onDeleteConfirm(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
        if(script.displayed || script.displaying || script.block.scriptPlan.displayed)
            ANews.showInfo(bundle.getString('Bundle', 'script.block.displayed'), Info.WARNING);
        else
		    scriptService.removeScript(script.id, onDeleteSuccess);
	}
}

private function onDragStart(event:MouseEvent):void {
	List(this.owner).dragEnabled = true;
}

private function onDragEnd(event:MouseEvent):void {
	List(this.owner).dragEnabled = false;
}

private function focusFieldIn(event:FocusEvent):void {
	if (event.currentTarget is TextInput) {
		oldValue = TextInput(event.currentTarget).text;
	}

	if (event.currentTarget is ComboBox) {
		oldValue = ComboBox(event.currentTarget).selectedItem;
	}

	if (event.currentTarget is CheckBox) {
		oldValue = CheckBox(event.currentTarget).selected;
	}

	if (event.currentTarget is SpellTextArea) {
		oldValue = SpellTextArea(event.currentTarget).text;
	}

	if (event.currentTarget is SpellTextInput) {
		oldValue = SpellTextInput(event.currentTarget).text;
	}

	if (event.currentTarget is TimeEditable) {
		oldValue = TimeEditable(event.currentTarget).editor.text;
	}
}

private function textOut(event:Event):void {
	var input:SkinnableTextBase;
	if (event.currentTarget is SpellTextArea) {
		input = SpellTextArea(event.currentTarget);
	} else if (event.currentTarget is SpellTextInput) {
		input = SpellTextInput(event.currentTarget);
	} else {
		input = TextInput(event.currentTarget);
	}

	var field:String = event.currentTarget.id;
	var text:String = input.text;

	if (text != null && oldValue != null && text.toUpperCase() == oldValue.toUpperCase()) {
		return;
	}

	scriptService.updateField(script.id, field, text);
}

private function dateOut(event:FocusEvent):void {
	var input:TextInput;
	if (event.currentTarget is TimeEditable) {
		input = TimeEditable(event.currentTarget).editor;
	} else {
		input = TextInput(event.currentTarget);
	}

	var field:String = event.currentTarget.id;
	var time:Date = this.dateUtil.stringToMinuteSecond(input.text);

	if (time == null) {
		input.text = oldValue as String;
		return;
	} else {
		input.text = this.dateUtil.minuteSecondToString(time);
	}

	if (input.text == oldValue) {
		return;
	}
	
	scriptService.updateField(script.id, field, time);
}

private function pageOut(event:Event):void {
	var input:SkinnableTextBase;
	if (event.currentTarget is SpellTextArea) {
		input = SpellTextArea(event.currentTarget);
	} else if (event.currentTarget is SpellTextInput) {
		input = SpellTextInput(event.currentTarget);
	} else {
		input = TextInput(event.currentTarget);
	}

	var field:String = event.currentTarget.id;
	var page:String = fixPage(input.text);

	if (StringUtils.isNullOrEmpty(page) || !isValidPage(page)) {
		input.text = oldValue as String;
		return;
	} else {
		input.text = page;
	}

	if (page == oldValue) {
		return;
	}

	scriptService.updateField(script.id, field, page);
}

private function checkChange(event:Event):void {
	var field:String = CheckBox(event.currentTarget).id;
	var selected:Boolean = CheckBox(event.currentTarget).selected;

	scriptService.updateField(script.id, field, selected);
}

private function updateReporter(event:Event):void {
    var user:User = validateComboBox(reporterCombo);
    if (nothingToDo(user)) {
        return;
    }
	scriptService.updateField(script.id, REPORTER, user);
	}

private function updateEditor(event:Event):void {
    var user:User = validateComboBox(editorCombo);
    if (nothingToDo(user)) {
			return;
		}
	scriptService.updateField(script.id, EDITOR, user);
	}
	
private function updateImageEditor(event:Event):void {
    var user:User = validateComboBox(imageEditor);
    if (nothingToDo(user)) {
        return;
    }
	scriptService.updateField(script.id, IMAGE_EDITOR, user);
}

private function validateComboBox(field:ComboBox):User {
	oldValue = oldValue as User;
    var user:User = field.selectedItem as User;
	if (user == null) {
        field.selectedIndex = -1;
        field.selectedItem = undefined;
	}
    return user;
	}
	
private function nothingToDo(user:User):Boolean {
    return (oldValue == null && user == null) || (oldValue != null && user != null && (user.id == oldValue.id));
}

private function onKeyDownInputs(event:KeyboardEvent):void {
	event.stopPropagation();
	if (event.keyCode == Keyboard.ENTER) {
		event.currentTarget.id == "page" ? pageOut(event) : textOut(event);
	}
}

public function onViewStory(story:Script):void {
	if (ANews.USER.allow('01071101') && script.editingUser == null) {
		var crudEvent:CrudEvent = new CrudEvent(CrudEvent.READ);
		crudEvent.entity = story;
		this.dispatchEvent(crudEvent);
	}
}

public function onDeleteSuccess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', 'scriptarea.msg.deleteSucess'), Info.INFO);
}

// ------------------
// Helpers
// ------------------

private function hideOrShowVideos(event:MouseEvent):void {
    scriptExpand.dispatchEvent(new StateChangeEvent(StateChangeEvent.CURRENT_STATE_CHANGE));
}

private function colapseMouseOver(event:MouseEvent):void {
    event.preventDefault();
    event.stopImmediatePropagation();
    event.stopPropagation();
    collapse.source = !scriptExpand.collapsed ? imgCollapseMouseOver : imgExpandMouseOver;
}

private function collapseMouseOut(event:MouseEvent):void {
    event.preventDefault();
    event.stopImmediatePropagation();
    event.stopPropagation();
    collapse.source = !scriptExpand.collapsed ? imgCollapse : imgExpand;
}

private function isValidPage(value:String):Boolean {
	if (StringUtils.isNullOrEmpty(value)) {
		return false;
	}

	if (value.length > 3) {
		return false;
	}

	var hasNumbers:Boolean = false;
	for (var i:int = 0; i < value.length; i++) {
		if ("0123456789".indexOf(value.charAt(i)) != -1) {
			hasNumbers = true;
		}
	}
	if (!hasNumbers) {
		return false;
	}

	if ("0123456789".indexOf(value.charAt(0)) != -1 && "0123456789".indexOf(value.charAt(1)) != -1 && "ABCDEFGHIJKLMNOPQRSTUVXWYZ".indexOf(value.charAt(2)) != -1) {

		return true;
	}

	return false;
}

private function fixPage(value:String):String {
	value = value.toUpperCase();
	if ("0123456789".indexOf(value.charAt(0)) != -1) {
		if (value.length >= 2) {
			if ("0123456789".indexOf(value.charAt(1)) == -1) {
				value = "0" + value.substring(0, 2);
			}
		} else {
			value = "0" + value;
		}
	}
	return value;
}
