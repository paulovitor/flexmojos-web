import flash.events.KeyboardEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.gridClasses.GridColumn;

import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.UserService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.DateUtil;

private const userService:UserService = UserService.getInstance();

private var _dateUtil:DateUtil = new DateUtil();

[Bindable] public var user:User = new User();
[Bindable] public var userToDell:User = new User();
[Bindable] private var users:ArrayCollection = new ArrayCollection();

[ArrayElementType("int")]
private var ids:ArrayCollection = new ArrayCollection();

public function get dateUtil():DateUtil {
	return _dateUtil;
}

private function onCreationComplete():void {
	searchUsers();

	// Click do botão de desconectar da DataGrid
	usersGrid.addEventListener("disconnectUser",
			function(event:Event):void {
				userService.disconnectUser(User(usersGrid.selectedItem),
						function(event:ResultEvent):void {
							refresh();
						}
				);
			}
	);

	// Click do botão de editar da DataGrid
	usersGrid.addEventListener(CrudEvent.EDIT,
			function(event:CrudEvent):void {
				user = User(usersGrid.selectedItem);
				edit();
			}
	);
	usersGrid.addEventListener(CrudEvent.DELETE,
			function(event:CrudEvent):void {
				userToDell = User(usersGrid.selectedItem);
				del();
			}
	);
}

private function searchUsers():void {
	userService.listAllOrderByName(searchInput.text,
			function(event:ResultEvent):void {
				users = event.result as ArrayCollection;
			}
	);
}

// -------------------------------
// Events.
// -------------------------------

private function newUser():void {
	user = new User();
	this.currentState = "edition";
	txtPassword.text = "";
}

private function save():void {

    if(groupsCombo.selectedIndex != -1)
        addGroupToUser();

	if ((user.id == 0 && ANews.USER.allow("020302")) || (user.id != 0 && ANews.USER.allow("020303"))) {
		//Como o save pode demorar devido ao envio de e-mail
		//Desabilita o botão até o fim do evento.
		btnSave.enabled = false;

		if (!Util.isValid(validvalidators)) {
			ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
			if (groupsList.dataProvider.length == 0) {
				groupsCombo.errorString = this.resourceManager.getString('Bundle', 'defaults.msg.requiredField');
			}
			btnSave.enabled = true;
			return;
		}

		//Configura uma propriedade expecifíca para marcar alteraçoes na senha.
        user.collapsedMenu = true;
		user.newPassword = txtPassword.text;
		userService.save(user, onSaveSuccess, onSaveError);
	}
}

public function del():void {
	if ((userToDell == null) || (userToDell.id == 0)) {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'user.msg.selectOneUser'), Info.WARNING);
		return;
	} else {
		Alert.show(this.resourceManager.getString('Bundle', 'defaults.msg.confirmAllDelete'), this.resourceManager.getString('Bundle', 'defaults.msg.confirmation'), Alert.NO | Alert.YES, null, confirmation);
	}

}

private function confirmation(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		userService.del(userToDell, onSuccessDelete);
	}
}

public function edit():void {
	usersGrid.enabled = false;
	txtPassword.text = "";
	groupsCombo.selectedIndex = -1;
	resetErros();
	this.currentState = "edition";
	txtPassword.text = ""

	user = ObjectUtil.copy(user) as User;
}

private function cancel():void {
	user = new User();
	userToDell = new User();
	resetErros();
	groupsCombo.selectedIndex = -1;
	usersGrid.enabled = true;
	this.currentState = "view";
}

private function changeState():void {
	if (txtName.text != '' && user.id == 0) {
		this.currentState = 'new';
	} else {
		if (txtName.text != '' && user.id > 0) {
			this.currentState = 'edition';
		} else {
			this.currentState = 'view';
		}
	}
}

private function resetErros():void {
	groupsCombo.errorString = "";
	groupsList.errorString = "";
	txtPassword.text = "";
	txtName.errorString = "";
	txtNickName.errorString = "";
	txtEmail.errorString = "";
}

private function onGroupsComboChange(event:IndexChangeEvent):void {
	groupsCombo.errorString = "";
}

private function addGroupToUser(event:KeyboardEvent = null):void {
	if (event == null || event.keyCode == Keyboard.ENTER) {
		var group:UserGroup = groupsCombo.selectedItem as UserGroup;
		if (group && !userHasGroup(group)) {
			user.groups.addItem(group);
		}
		groupsCombo.selectedIndex = -1;
	}
}

private function requestUserToAgreeWithTerms():void {
	if (usersGrid.selectedItem != null) {
		var user:User = usersGrid.selectedItem as User;
		userService.requestUserToAgreeWithTerms(user.id, onSuccess);
	}
}

/**
 * Give the chance for any component that has a instance
 * of this classe do remove Groups from User.
 */
public function removeGroupFromUser():void {
	var group:UserGroup = groupsList.selectedItem as UserGroup;
	if (group && user) {
		user.groups.removeItemAt(user.groups.getItemIndex(group));
	}
}

// --------------------------
// Asynchronous answers
// --------------------------

private function onSuccess(event:ResultEvent):void {
	refresh();
}

private function onSuccessDelete(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.deleteSuccess'), Info.INFO);
	refresh();
}

private function onSaveError(event:FaultEvent):void {
	ANews.showInfo(event.fault.faultString, Info.ERROR);
	btnSave.enabled = true;
}

private function onSaveSuccess(event:ResultEvent):void {
	var result:String = event.result as String;
	// For now, we just need to check if the e-mail is valid.
	if (result == "INVALID_EMAIL") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'user.msg.userEmailExist'), Info.ERROR);
	} else if (result == "INVALID_NICKNAME") {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'user.msg.nickNameExist'), Info.ERROR);
	} else {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
		refresh();
	}
	usersGrid.enabled = true;
	btnSave.enabled = true;
}

// --------------------------
// Helpers.
// --------------------------
public function refresh():void {
	//Restore default values.
	user = new User();
	txtPassword.text = '';
	this.currentState = "view";

	//Refresh the list
	searchInput.text = '';
	searchUsers();
}

private function userHasGroup(group:UserGroup):Boolean {
	if (user) {
		for each (var aux:UserGroup in user.groups) {
			if (aux.id == group.id) {
				return true;
			}
		}
	}
	return false;
}

private function dateLabelFunction(item:Object, column:GridColumn):String {
	return item[column.dataField] ? dateUtil.dateToString(item[column.dataField]) : '';
}
