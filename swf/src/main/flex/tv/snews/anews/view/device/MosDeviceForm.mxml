<?xml version="1.0"?>
<s:VGroup xmlns:fx="http://ns.adobe.com/mxml/2009"
					xmlns:s="library://ns.adobe.com/flex/spark"
					xmlns:mx="library://ns.adobe.com/flex/mx"
					xmlns:an="tv.snews.anews.component.*"
					creationComplete="creationCompleteHandler(event)">

	<fx:Script>
		<![CDATA[
		import mx.collections.ArrayCollection;
		import mx.events.FlexEvent;

		import spark.components.CheckBox;

		import tv.snews.anews.component.Info;

		import tv.snews.anews.domain.MosDevice;
		import tv.snews.anews.domain.MosDeviceChannel;

		import tv.snews.anews.domain.MosRevision;
		import tv.snews.anews.event.CrudEvent;
		import tv.snews.anews.utils.DomainCache;
		import tv.snews.anews.utils.Util;

		[Bindable]
		public var device:MosDevice;

		[Bindable]
		public var isPlayout:Boolean = false;

		//------------------------------
		//	Event Handlers
		//------------------------------

		private function creationCompleteHandler(event:FlexEvent):void {
			channelsList.addEventListener(CrudEvent.DELETE, channelsList_deleteHandler);
		}

		private function channelsList_deleteHandler(event:CrudEvent):void {
			var channel:MosDeviceChannel = MosDeviceChannel(event.entity);
			device.removeChannel(channel);

            if(channel.equals(device.defaultChannel)) device.defaultChannel = null;
			event.stopPropagation();
		}

		private function profileCheckBox_changeHandler(event:Event):void {
			if (CheckBox(event.target).selected) {
				profile1CheckBox.selected = true;
				profile2CheckBox.selected = true;
			}
		}

		private function addChannelButton_clickHandler(event:MouseEvent):void {
			var name:String = channelNameInput.text;

			for each (var curr:MosDeviceChannel in device.channels) {
				if (curr.name == name) {
					ANews.showInfo(resourceManager.getString('Bundle', 'device.msg.uniqueChannel'), Info.WARNING);
					return;
				}
			}

			var newChannel:MosDeviceChannel = new MosDeviceChannel(name);
			device.addChannel(newChannel);

			channelNameInput.text = "";
		}

		//------------------------------
		//	Helpers
		//------------------------------

		public function validate():Boolean {
			if (Util.isValid(validators)) {
				const checkboxes:Array = [ profile1CheckBox, profile2CheckBox, profile3CheckBox, profile4CheckBox, profile5CheckBox, profile6CheckBox, profile7CheckBox ];

				var supportAny:Boolean = false;
				for each (var checkbox:CheckBox in checkboxes) {
					supportAny ||= checkbox.selected;
				}

				if (supportAny) {
					return true;
				} else {
					ANews.showInfo(resourceManager.getString('Bundle', 'device.msg.requireProfile'), Info.WARNING);
				}
			}
			return false;
		}

		public function populate():MosDevice {
			var result:MosDevice = device || new MosDevice();

			result.hostname = hostnameInput.text;
			result.mosId = mosIdInput.text;
			result.revision = String(revisionDropDown.selectedItem.id);
			result.activeX = Boolean(activeXDropDown.selectedItem.id);
			result.lowerPort = lowerPortStepper.value;
			result.upperPort = upperPortStepper.value;
			result.queryPort = queryPortStepper.value;
			result.channels = ArrayCollection(channelsList.dataProvider);
			result.tpMode = tpModeCheckBox.selected;
			result.profileOneSupported = profile1CheckBox.selected;
			result.profileTwoSupported = profile2CheckBox.selected;
			result.profileThreeSupported = profile3CheckBox.selected;
			result.profileFourSupported = profile4CheckBox.selected;
			result.profileFiveSupported = profile5CheckBox.selected;
			result.profileSixSupported = profile6CheckBox.selected;
			result.profileSevenSupported = profile7CheckBox.selected;

			return result;
		}

		public function clear():void {
			device = new MosDevice();
		}
		]]>
	</fx:Script>

	<fx:Declarations>
		<fx:Array id="validators">
			<mx:StringValidator source="{hostnameInput}" property="text" required="true" trigger="{hostnameInput}" triggerEvent="change"/>
			<mx:StringValidator source="{mosIdInput}" property="text" required="true" trigger="{mosIdInput}" triggerEvent="change"/>
			<s:NumberValidator source="{lowerPortStepper}" property="value" required="true" trigger="{lowerPortStepper}" triggerEvent="change" minValue="1024" maxValue="65535"/>
			<s:NumberValidator source="{upperPortStepper}" property="value" required="true" trigger="{upperPortStepper}" triggerEvent="change" minValue="1024" maxValue="65535"/>
			<s:NumberValidator source="{queryPortStepper}" property="value" required="true" trigger="{queryPortStepper}" triggerEvent="change" minValue="1024" maxValue="65535"/>
		</fx:Array>
	</fx:Declarations>

	<s:HGroup width="100%" paddingTop="4">
		<!-- hostname -->
		<s:VGroup width="100%">
			<an:FormLabel text="{resourceManager.getString('Bundle', 'device.form.hostname')}" required="true"/>
			<s:TextInput id="hostnameInput" text="{device.hostname}" width="100%" maxChars="100"/>
		</s:VGroup>
		<!-- mosId -->
		<s:VGroup width="100%">
			<an:FormLabel text="{resourceManager.getString('Bundle', 'device.form.mosId')}" required="true"/>
			<s:TextInput id="mosIdInput" text="{device.mosId}" width="100%" maxChars="128"/>
		</s:VGroup>
	</s:HGroup>

	<s:HGroup width="100%" gap="7" paddingTop="4" paddingBottom="4">
		<!-- revision -->
		<s:VGroup>
			<an:FormLabel text="{resourceManager.getString('Bundle', 'device.form.revision')}" required="true"/>
			<an:DropDownListBindable id="revisionDropDown" dataProvider="{MosRevision.values}" selectedItem="{device.revisionObject}" requireSelection="true" valueField="id" labelField="label" width="65"/>
		</s:VGroup>
		<!-- activeX -->
		<s:VGroup>
			<an:FormLabel text="{resourceManager.getString('Bundle', 'device.form.activeX')}" required="true"/>
			<an:DropDownListBindable id="activeXDropDown" dataProvider="{DomainCache.YES_OR_NO}" selectedItem="{device.activeXObject}" requireSelection="true" valueField="id" labelField="label" width="65"/>
		</s:VGroup>
		<!-- lower port -->
		<s:VGroup>
			<an:FormLabel text="lower port" required="true"/>
			<s:NumericStepper id="lowerPortStepper" value="{device.lowerPort}" minimum="1024" maximum="65535" width="75"/>
		</s:VGroup>
		<!-- upper port -->
		<s:VGroup>
			<an:FormLabel text="upper port" required="true"/>
			<s:NumericStepper id="upperPortStepper" value="{device.upperPort}" minimum="1024" maximum="65535" width="75"/>
		</s:VGroup>
		<!-- query port -->
		<s:VGroup>
			<an:FormLabel text="query port" required="true"/>
			<s:NumericStepper id="queryPortStepper" value="{device.queryPort}" minimum="1024" maximum="65535" width="75"/>
		</s:VGroup>
	</s:HGroup>

	<s:CheckBox id="tpModeCheckBox" selected="{device.tpMode}"
							label="{resourceManager.getString('Bundle', 'device.form.tpModes')}"
							toolTip="{resourceManager.getString('Bundle', 'device.form.tpModes.tooltip')}"/>

	<!-- canais -->
	<s:VGroup enabled="{isPlayout}" width="100%">
		<an:FormLabel text="{resourceManager.getString('Bundle', 'device.form.channels')}"/>
		<s:HGroup width="100%">
			<s:TextInput id="channelNameInput" width="100%" maxChars="50"/>
			<s:Button click="addChannelButton_clickHandler(event)" enabled="{channelNameInput.text.length != 0}" label="{resourceManager.getString('Bundle', 'device.btn.add')}"/>
		</s:HGroup>
		<s:List id="channelsList" dataProvider="{device.channels}" borderVisible="false" width="100%"
						itemRenderer="tv.snews.anews.view.device.MosDeviceChannelRenderer">
			<s:layout>
				<s:VerticalLayout gap="3"/>
			</s:layout>
		</s:List>
	</s:VGroup>

	<!-- profiles -->
	<an:FormLabel text="{resourceManager.getString('Bundle', 'device.form.profiles')}" required="true"/>
	<s:Group width="100%">
		<s:layout>
			<s:TileLayout paddingTop="4"/>
		</s:layout>

		<s:CheckBox id="profile1CheckBox" label="profile 1" selected="{device.profileOneSupported}"/>
		<s:CheckBox id="profile2CheckBox" label="profile 2" selected="{device.profileTwoSupported}"   change="profileCheckBox_changeHandler(event)"/>
		<s:CheckBox id="profile3CheckBox" label="profile 3" selected="{device.profileThreeSupported}" change="profileCheckBox_changeHandler(event)"/>
		<s:CheckBox id="profile4CheckBox" label="profile 4" selected="{device.profileFourSupported}"  change="profileCheckBox_changeHandler(event)"/>
		<s:CheckBox id="profile5CheckBox" label="profile 5" selected="{device.profileFiveSupported}"  change="profileCheckBox_changeHandler(event)"/>
		<s:CheckBox id="profile6CheckBox" label="profile 6" selected="{device.profileSixSupported}"   change="profileCheckBox_changeHandler(event)"/>
		<s:CheckBox id="profile7CheckBox" label="profile 7" selected="{device.profileSevenSupported}" change="profileCheckBox_changeHandler(event)"/>
	</s:Group>
</s:VGroup>
