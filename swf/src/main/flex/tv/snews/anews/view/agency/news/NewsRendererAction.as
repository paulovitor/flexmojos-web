import tv.snews.anews.domain.News;
import tv.snews.flexlib.utils.*;
import tv.snews.anews.utils.*;

[Bindable] private var news:News;

private const dateTimeUtil:DateUtil = new DateUtil();

override public function set data(value:Object):void {
	super.data = value;
	news = value as News;
}

private function formatHour(date:Date):String {
	return dateTimeUtil.dateToString(date);
}

private function formatDate(date:Date):String {
	return dateTimeUtil.toString(date, this.resourceManager.getString('Bundle', 'format.D_MMM'));
}

private function stripHtml(str:String):String {
	return StringUtils.stripHtmlTags(str);
}
