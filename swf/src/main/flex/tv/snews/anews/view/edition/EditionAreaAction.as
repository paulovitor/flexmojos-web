// Ações do arquivo EditionArea.mxml

import flexlib.controls.tabBarClasses.SuperTab;
import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.messaging.messages.IMessage;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import spark.components.DataGroup;

import tv.snews.anews.domain.AbstractRevision;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.GuidelineRevision;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.ReportageRevision;
import tv.snews.anews.domain.ReportageSection;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.RundownAction;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.StoryRevision;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.service.ProgramService;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.service.RundownService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.edition.EditionGuidelineView;
import tv.snews.anews.view.edition.EditionReportageView;
import tv.snews.anews.view.edition.EditionStoryView;
import tv.snews.anews.view.reportage.ReportageForm;
import tv.snews.anews.view.rundown.RundownViewer;
import tv.snews.anews.view.rundown.story.section.form.StoryForm;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const dateUtil:DateUtil = new DateUtil();

private const guidelineService:GuidelineService = GuidelineService.getInstance();
private const programService:ProgramService = ProgramService.getInstance();
private const reportageService:ReportageService = ReportageService.getInstance();
private const storyService:StoryService = StoryService.getInstance();
private const rundownService:RundownService = RundownService.getInstance();

private var _editors:ArrayCollection = new ArrayCollection();
private var _storiesOfEditor:ArrayCollection = new ArrayCollection();

[Bindable] public var editTabManager:TabManager = new TabManager();
[Bindable] public var tabManager:TabManager = new TabManager();

[Bindable] public var currentEditor:User;
[Bindable] public var selectedStory:Story;
[Bindable] public var currentStoriesOfEditorDate:Date = new Date();
	
//guarda qual o número da última revisão verificada
[Bindable] public var lastRevReportge:int;
[Bindable] public var lastRevStory:int;
[Bindable] public var lastRevGuideline:int;

//Guarda a revisão ao carregar a área. 
[Bindable] public var localGuidelineRevs:ArrayCollection = new ArrayCollection();
[Bindable] public var localStoryRevs:ArrayCollection  = new ArrayCollection();
[Bindable] public var localReportageRevs:ArrayCollection = new ArrayCollection();

private function onCreationComplete(event:FlexEvent):void {
	programService.subscribe(onProgramMessageReceived);
	guidelineService.subscribe(onGuidelineMessageReceived);
	storyService.subscribe(onStoryMessageReceived);
    rundownService.subscribe(onRundownMessageReceived);
	reportageService.subscribe(onReportageMessageReceived);
	
	editTabManager.tabNavigator = mainNavigator;
	editTabManager.setClosePolicyForTab(0, SuperTab.CLOSE_NEVER);
	
	tabManager.tabNavigator = bottomNavigator;
	if (ANews.USER.allow("010701", true)) {
		tabManager.openTab(RundownViewer, "rundown",  null, false);
	}
	
	listStory.addEventListener(CrudEvent.READ, onEntityView);
	bottomNavigator.addEventListener(CrudEvent.EDIT, onEntityEdit);
	editors.addEventListener(CollectionEvent.COLLECTION_CHANGE, onChangeCurrentEditor);
	mainNavigator.addEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false, 0, true);
	bottomNavigator.addEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false, 0, true);
}

private function onRemove(event:FlexEvent):void {
	programService.unsubscribe(onProgramMessageReceived);
	guidelineService.unsubscribe(onGuidelineMessageReceived);
	storyService.unsubscribe(onStoryMessageReceived);
    rundownService.unsubscribe(onRundownMessageReceived);
	reportageService.unsubscribe(onReportageMessageReceived);
	UserService.getInstance().releaseLocksFromUser(ANews.USER);
}

private function onEntityView(event:CrudEvent):void {
	closeOpenTab();
	
	// Visualizar Pauta
	if (event.entity is Guideline) {
		var guideline:Guideline = event.entity as Guideline;
		
		guidelineService.loadById(guideline.id, function(event:ResultEvent):void {
			var result:Guideline = Guideline(event.result);
			guideline.guides = result.guides;
			
			tabManager.openTab(EditionGuidelineView, guideline.id + " ", { guideline: guideline }, true, bottomNavigator.getChildren().length == 1 ? 1 : 0);
		});
	}
	
	// Visualizar Reportagem
	if (event.entity is Reportage) {
		var reportage:Reportage = event.entity as Reportage;
		
		reportageService.loadById(reportage.id, function(event:ResultEvent):void {
			var result:Reportage = Reportage(event.result);
			reportage.sections = result.sections;
			
			tabManager.openTab(EditionReportageView, reportage.id + " ", { reportage: reportage , currentEditor: currentEditor }, true, bottomNavigator.getChildren().length == 1 ? 1 : 0);
		});
	}
	
	// Visualizar Lauda
	if (event.entity is Story) {
		var story:Story = event.entity as Story;
		
		storyService.loadById(story.id, function(event:ResultEvent):void {
			var result:Story = Story(event.result);
			story.headSection = result.headSection;
			//story.credits = result.credits;
			
			tabManager.openTab(EditionStoryView, story.id + " ", { story: story , currentEditor: currentEditor }, true, bottomNavigator.getChildren().length == 1 ? 1 : 0 );
		});
	}
}

protected function tabClosedHandler(event:SuperTabEvent):void {
	//Verifica detalhes do objeto da tab antes de fechar
	if (mainNavigator.getChildAt(event.tabIndex) is StoryForm) {
		event.preventDefault();
		var storyForm:StoryForm = mainNavigator.getChildAt(event.tabIndex) as StoryForm;
		storyForm.close();
	}
	//Verifica detalhes do objeto da tab antes de fechar
	if (mainNavigator.getChildAt(event.tabIndex) is ReportageForm) {
		event.preventDefault();
		var reportageForm:ReportageForm = mainNavigator.getChildAt(event.tabIndex) as ReportageForm;
		reportageForm.close();
	}
	//Verifica detalhes do objeto da tab antes de fechar
	if (bottomNavigator.getChildAt(event.tabIndex) is EditionStoryView) {
		event.preventDefault();
		var editionStoryView:EditionStoryView = bottomNavigator.getChildAt(event.tabIndex) as EditionStoryView;
		editionStoryView.view.onRemove();
		bottomNavigator.removeChildAt(event.tabIndex);
	}
}

private function onChangeCurrentEditor(event:CollectionEvent):void {
	//Verificação se existe algum editor selecionado se houver reseleciona.
	if (currentEditor != null) {
		cbEditors.selectedIndex = editors.getItemIndex(currentEditor);
	}
}

private function onEntityEdit(event:CrudEvent):void {
	// Abre o formulário de edição da reportagem e bloqueia a edição
	if (event.entity is Reportage) {
		var reportage:Reportage = Reportage(event.entity);
		reportageService.loadById(reportage.id, function(event:ResultEvent):void {
			var result:Reportage = Reportage(event.result);
			result.calculateReadTime();
			editTabManager.openTab(ReportageForm, "reportage_" + result.id, { reportage: result, tabManager: editTabManager } );
		});
		
		reportageService.lockEdition(reportage.id, ANews.USER, function(event:ResultEvent):void {});
	}
	
	// Abre o formulário de edição da lauda e bloqueia a edição
	if (event.entity is Story) {
		var story:Story = Story(event.entity);
		
		storyService.loadById(story.id, function(event:ResultEvent):void {
			var result:Story = Story(event.result);
			var params:Object = { story: result, editionAreaMode: true, tabManager: editTabManager };
			
			editTabManager.openTab(StoryForm, "story_" + result.id, params);
		});
		
		storyService.lockEdition(story.id, ANews.USER.id, function(event:ResultEvent):void {});
	}
}

private function onReportageMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var reportage:Reportage = event.message.body as Reportage;
	var userEditing:User = event.message.headers["user"] as User;
	var field:String = event.message.headers.field;
	var value:Boolean = event.message.headers.value as Boolean;
	var localReportage:Reportage = null;
	
	switch (action) {
		case EntityAction.UPDATED:  {
			localReportage = getReportageById(reportage.id);
			if (localReportage) {
				var abstractRevision:AbstractRevision = getLocalRev(reportage);
				var localRev:int = abstractRevision ? (abstractRevision	 as ReportageRevision).revisionNumber : 0;
				var actualRev:int = (reportage.revisions.getItemAt(reportage.revisions.length-1) as ReportageRevision).revisionNumber;
				if (actualRev  > localRev && lastRevReportge != actualRev) {
					localReportage.totalDiffs++;
					lastRevReportge = actualRev;
			}
				localReportage.updateFields(reportage);
				refreshReportageStatus(localReportage);
			}
			break;
		}
		case EntityAction.FIELD_UPDATED: {
			localReportage = getReportageById(reportage.id);
			if (localReportage && field == "ok") {
				if (value) {
					localReportage.approve();
				} else {
					localReportage.disapprove();
				}
			}
			break;
		}
		case EntityAction.UNLOCKED:  {
			localReportage = getReportageById(reportage.id);
			if (localReportage) {
				localReportage.editingUser = null;
				refreshReportageStatus(localReportage);
			}
			break;
		}
		case EntityAction.LOCKED:  {
			localReportage = getReportageById(reportage.id);
			if (localReportage) {
				localReportage.editingUser = userEditing;
				refreshReportageStatus(localReportage);
			}
			break;
		}
		default:  {}
	}
}

private function onGuidelineMessageReceived(event:MessageEvent):void {
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var guideline:Guideline = message.body as Guideline
	var userEditing:User = event.message.headers["user"] as User;
	var localGuideline:Guideline = null;

	switch (action) {
		case EntityAction.UPDATED: {
			localGuideline = getGuidelineById(guideline.id);

			if (localGuideline) {
				var abstractRevision:AbstractRevision = getLocalRev(guideline);
				var localRev:int = abstractRevision ? (abstractRevision	 as GuidelineRevision).revisionNumber : 0;
				var actualRev:int = (guideline.revisions.getItemAt(guideline.revisions.length-1) as GuidelineRevision).revisionNumber;
				if (actualRev  > localRev && lastRevGuideline != actualRev) {
					localGuideline.totalDiffs++;
					lastRevGuideline = actualRev;
				}
				localGuideline.updateFields(guideline);
			}
			break;
		}
			
		case EntityAction.UNLOCKED:  {
			localGuideline = getGuidelineById(guideline.id);
			if (localGuideline) {
				localGuideline.editingUser = null;
			}
			break;
		}
		case EntityAction.LOCKED:  {
			localGuideline = getGuidelineById(guideline.id);
			if (localGuideline) {
				localGuideline.editingUser = userEditing;
			}
			break;
		}
		default:  {}
	}
}

private function onStoryMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var story:Story = event.message.body as Story;
	var userEditing:User = event.message.headers["user"] as User;
	var localStory:Story = null;

	var rundownId:Number;

	switch (action) {
		case EntityAction.UPDATED:
			localStory = getStoryById(story.id);
			if (localStory) {
				var abstractRevision:AbstractRevision = getLocalRev(story);
				var localRev:int = abstractRevision ? (abstractRevision as ReportageRevision).revisionNumber : 0;
				var actualRev:int = (story.revisions.getItemAt(story.revisions.length - 1) as StoryRevision).revisionNumber;
				if (actualRev > localRev && lastRevReportge != actualRev) {
					localStory.totalDiffs++;
					lastRevReportge = actualRev;
				}
				localStory.updateFields(story);
			}

			break;

		case EntityAction.DELETED:
			localStory = getStoryById(story.id);
			if (localStory) {
				if (localStory.editor) {
					if (cbEditors.selectedItem as User) {
						if (localStory.editor.id == (cbEditors.selectedItem as User).id) {
							ANews.showNotifier(bundle.getString('Bundle', 'story.notifier.deleted', [formatSlug(localStory.slug)]), 0x000000);
							storiesOfEditor.removeItemAt(storiesOfEditor.getItemIndex(localStory));
						}
					}
				}
			}
			break;
		case RundownAction.DRAWER:
			//this message isn't for this user.
//			 foi enviada para a gaveta
			localStory = getStoryById(story.id);
			if (localStory) {
				ANews.showNotifier(bundle.getString('Bundle', 'story.notifier.drower', [formatSlug(localStory.slug)]), 0x000000);
				storiesOfEditor.removeItemAt(storiesOfEditor.getItemIndex(localStory));
			}
			break;
		case EntityAction.FIELD_UPDATED:
			rundownId = event.message.headers["rundownId"];
			var storyId:Number = event.message.headers["storyId"];
			var field:String = event.message.headers["field"];
			var value:Object = event.message.headers["value"];

			localStory = getStoryById(storyId);
			if (localStory) {
				if (field == "editor") {
					localStory = updateEditor(value, storyId, localStory);
				} else {
					localStory[(field.toLowerCase() != "image_editor" ? field.toLowerCase() : "imageEditor")] = value;
					if (field.toLowerCase() == 'displayed')
						localStory.displaying = false;
				}
			} else if (equalsRundownId(rundownId) && field == "editor" && cbEditors && cbEditors.selectedItem) {
				listStoriesOfEditor();
			}
			break;
		case EntityAction.UNLOCKED:
			localStory = getStoryById(story.id);
			if (localStory) {
				localStory.editingUser = null;
			}
			break;
		case EntityAction.LOCKED:
			localStory = getStoryById(story.id);
			if (localStory) {
				localStory.editingUser = userEditing;
			}
			break;
		case EntityAction.MULTIPLE_EXCLUSION:
			var affectedStories:ArrayCollection = event.message.body as ArrayCollection;
			rundownId = event.message.headers["rundownId"];
			if (storiesOfEditor != null && storiesOfEditor.length > 0 && equalsRundownId(rundownId)) {
				for (var i:int = storiesOfEditor.length - 1; i >= 0; i--) {
					if (affectedStories.contains(storiesOfEditor.getItemAt(i).id)) {
						storiesOfEditor.removeItemAt(i);
					}
				}
			}
			break;
		default:
			break;
	}
}

private function updateEditor(value:Object, storyId:Number, localStory:Story):Story {
// value é o editor atualizado que veio do banco
    if (cbEditors && cbEditors.selectedItem) {
        // o id do editor atualizado = id do editor que esta no combo
        if (value && value.id == (cbEditors.selectedItem as User).id) {
            storyService.loadById(storyId, onStoryById);
        }
        else {
            // story com os valores antigos.
            if (localStory.editor && localStory.editor.id == (cbEditors.selectedItem as User).id) { //a lauda antiga estava com o usuário atual
                storiesOfEditor.removeItemAt(storiesOfEditor.getItemIndex(localStory));
                if (localStory.editor.id == ANews.USER.id) {
                    if (value == null) {
                        ANews.showNotifier(bundle.getString('Bundle', 'story.notifier.unatribuided', [formatSlug(localStory.slug)]), 0x000000);
                    }
                    else {
                        if (value.id != ANews.USER.id) {
                            ANews.showNotifier(bundle.getString('Bundle', 'story.notifier.unatribuided', [formatSlug(localStory.slug)]), 0x000000);
                        }
                    }
                }

            }
        }
    } else {
        // remove a matéria quando não for setado o editor
        if (localStory.editor.id == (cbEditors.selectedItem as User).id) { //a lauda antiga estava com o usuário atual
            storiesOfEditor.removeItemAt(storiesOfEditor.getItemIndex(localStory));
        }
    }
    return localStory;
}

private function onRundownMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var rundownUpdated:Rundown = event.message.body as Rundown;

    if (rundownUpdated == null || !equalsRundownId(rundownUpdated.id))
        return;

    switch (action) {
        case RundownAction.DISPLAY_RESETS:
            resetDisplay();
            break;
        case RundownAction.COMPLETE_DISPLAY:
            completeDisplay();
            break;
        default:
    }
}

private function resetDisplay():void {
    var dataGroup:DataGroup = listStory.dataGroup;
    for (var index:int = 0; index < dataGroup.numElements; index++) {
        var editionItemRenderer:EditionItemRenderer = dataGroup.getElementAt(index) as EditionItemRenderer;
        editionItemRenderer.resetDisplay();
    }
}

private function completeDisplay():void {
    var dataGroup:DataGroup = listStory.dataGroup;
    for (var index:int = 0; index < dataGroup.numElements; index++) {
        var editionItemRenderer:EditionItemRenderer = dataGroup.getElementAt(index) as EditionItemRenderer;
        editionItemRenderer.completeDisplay();
    }
}

private function listStoriesOfEditor():void {
	if (currentEditor != null && currentStoriesOfEditorDate != null) {
		storyService.listStoriesByDateEditorOrderProgram(currentStoriesOfEditorDate, currentEditor, onListStoriesOfEditor);
	}
}

private function onProgramMessageReceived(event:MessageEvent):void {
	//Carrega novamente a lista de editores atualizada.
	loadCollectionEditors();
	
	//Verificação se existe algum editor selecionado se houver reseleciona.
	if (currentEditor != null) {
		cbEditors.selectedIndex = editors.getItemIndex(currentEditor);
	}
}

// -----------------------------
// Helper`s
// -----------------------------
private function equalsRundownId(rundownId:int):Boolean {
	for each (var story:Story in storiesOfEditor) {
		if (story.block.rundown.id == rundownId) {
			return true;
		}
	}
	return false;
}

private function getStoryById(storyId:int):Story {
	for each (var story:Story in storiesOfEditor) {
		if (story.id == storyId) {
			return story;
		}
	}
	return null;
}

private function formatSlug(slug:String):String {
	if (slug) {
		if (slug.length > 40) {
			slug = slug.substr(0, 37);
			slug = slug + "...";
		}
	}
	return slug;
}



private function onLoadEditorsOfProgramsSuccess(event:ResultEvent):void {
	editors = event.result as ArrayCollection;
	if (containsEditor(ANews.USER)) {
		currentEditor = getEditorById(ANews.USER.id);
		cbEditors.selectedIndex = editors.getItemIndex(currentEditor);
	}
	
	listStoriesOfEditor();
}

private function loadCollectionEditors():void {
	editors.removeAll();
	programService.listEditorsOfPrograms(onLoadEditorsOfProgramsSuccess);
	
	/*for each (var program:Program in DomainCache.programs) {
		for each (var editor:User in program.editors) {
			if (!containsEditor(editor)) {
				editors.addItem(editor);
			}
		}
	}*/	
}

private function containsEditor(editor:User):Boolean {
	for each (var current:User in editors) {
		if (current.equals(editor)) {
			return true;
		}
	}
	return false;
}

private function getEditorById(editorId:int):User {
	for each (var current:User in editors) {
		if (current.id == editorId) {
			return current;
		}
	}
	return null;
}

private function getReportageById(reportageId:int):Reportage {
	for each (var story:Story in storiesOfEditor) {
		if (story.reportage && story.reportage.id == reportageId) {
			return story.reportage;
		}
	}
	return null;
}

private function getGuidelineById(guidelineId:int):Guideline {
	for each (var story:Story in storiesOfEditor) {
		if (story.guideline && story.guideline.id == guidelineId) {
			return story.guideline;
		}
	}
	return null;
}


/** Método responsável por atualizar o status de cada reportagem
 * 	no PagedDataGrid.
 * 	Os status podem ser:
 *  - Vermelho se a reportagem está em edição.
 *  - Amarelo se a reportagem contém algum conteúdo em qualquer uma das suas seções.
 *  - Branco se a reportagem não contém nenhum conteúdo.
 * 	O status é controlado pelas variáveis transient: isBlank e isEditing.
 */ 
private function refreshReportageStatus(reportage:Reportage):void {
	if (reportage.editingUser == null) {
		reportage.blank = true;
		for each (var section:ReportageSection in reportage.sections) {
			if (!StringUtils.isNullOrEmpty(section.content)  || !StringUtils.isNullOrEmpty(section.observation)) {
				reportage.blank = false;
				break;
			}
		}
	}
}

private function closeOpenTab():void {
	/*if (bottomNavigator.getChildren().length == 1) {
		bottomNavigator.removeChildAt(0);
	} else */if (bottomNavigator.getChildren().length > 1)  {
		bottomNavigator.removeChildAt(1);
	}
}

/**
 * Guarda a revisão recém carregada da lista de editores
 * para mais tarde ser verificada as mudanças.
 */
private function populateLocalRevs(storiesOfEditor:ArrayCollection):void{
	if (storiesOfEditor) {
		for each (var story:Story in storiesOfEditor) {
			/*var tmp1:Object = new Object();
			tmp1.story = story;
			tmp1.storyRevision = story.revisions.getItemAt(story.revisions.length-1);*/
			
			if (story.revisions.length > 0)
				localStoryRevs.addItem(story.revisions.getItemAt(story.revisions.length-1));
			if (story.guideline) {
				/*var tmp2:Object = new Object();
				tmp2.guideline = story.guideline;
				tmp2.guidelineRevision = story.guideline.revisions.getItemAt(story.guideline.revisions.length-1);*/
				if (story.guideline.revisions.length > 0)
					localGuidelineRevs.addItem(story.guideline.revisions.getItemAt(story.guideline.revisions.length-1));
			}

			if (story.reportage) {
				/*var tmp3:Object = new Object();
				tmp3.reportage = story.reportage;
				tmp3.reportageRevision = story.reportage.revisions.getItemAt(story.reportage.revisions.length-1);*/
				if (story.reportage.revisions.length > 0) 
					localReportageRevs.addItem(story.reportage.revisions.getItemAt(story.reportage.revisions.length-1));
			}
		}
	}
}

private function getLocalRev(entity:Object):AbstractRevision {
	if (entity) {
		var localRevision:AbstractRevision;
		if (entity is Guideline) {
			var guideline:Guideline =  entity as Guideline;
			for each (localRevision in localGuidelineRevs) {
				if (localRevision.entity.id == guideline.id) {
					return localRevision;
				}
			}
		} else if (entity is Reportage) {
			var reportage:Reportage =  entity as Reportage;
			for each (localRevision in localReportageRevs) {
				if (localRevision.entity.id == reportage.id) {
					return localRevision;
				}
			}
		} else if (entity is Story) {
			var story:Story =  entity as Story;
			for each (localRevision in localGuidelineRevs) {
				if (localRevision.entity.id == story.id) {
					return localRevision;
				}
			}
		}
	}
	return null;
}

// -----------------------------
// Methods result
// -----------------------------

private function onListStoriesOfEditor(event:ResultEvent):void {
	storiesOfEditor = event.result as ArrayCollection;
	populateLocalRevs(storiesOfEditor);
}

private function onStoryById(event:ResultEvent):void {
	var storyLoaded:Story = event.result as Story;
	if (dateUtil.compareDates(storyLoaded.block.rundown.date, currentStoriesOfEditorDate) == 0) {
		storiesOfEditor.addItem(storyLoaded);
		if(storyLoaded) {
			if (storyLoaded.editor) {
				if (storyLoaded.editor.id == ANews.USER.id) {
					ANews.showNotifier(bundle.getString('Bundle', 'story.notifier.atribuided', [formatSlug(storyLoaded.slug)]), 0x000000);
				}
			}
		}
	}
}

// -----------------------------
// Getter`s and Setter`s
// -----------------------------

public function get editors():ArrayCollection {
	return _editors;
}

[Bindable]
[ArrayElementType("tv.snews.anews.domain.User")]
public function set editors(value:ArrayCollection):void {
	_editors = value;
}

public function get storiesOfEditor():ArrayCollection {
	return _storiesOfEditor;
}

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Story")]
public function set storiesOfEditor(value:ArrayCollection):void {
	_storiesOfEditor = value;
}
