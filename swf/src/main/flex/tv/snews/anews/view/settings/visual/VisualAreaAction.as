import flash.events.*;
import flash.net.*;
import flash.text.Font;
import flash.utils.ByteArray;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.controls.Alert;
import mx.events.ColorPickerEvent;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.Logo;

private const MAX_SIZE:int = 512000;
private const settingsService:SettingsService = SettingsService.getInstance();

[Bindable] private var systemLogo:ByteArray;
[Bindable] private var bannerLogin:ByteArray;
[Bindable] private var reportLogo:ByteArray;
[Bindable] private var settings:Settings;
[Bindable] private var reload:Boolean = false;
[Bindable] private var reloadTemp:Boolean = false;
[Bindable] private var fileBannerLoginRef:FileReference;
[Bindable] private var fileBannerLogin:UserFile = null; //File uploading.
[Bindable] private var fileSystemLogoRef:FileReference;
[Bindable] private var fileSystemLogo:UserFile = null; //File uploading.
[Bindable] private var fileReportLogoRef:FileReference;
[Bindable] private var fileReportLogo:UserFile = null; //File uploading.

protected function startUp(event:FlexEvent):void {
	settingsService.loadSettings(onLoadSettingsHandler);
}

private function onLoadSettingsHandler(event:ResultEvent):void {
	settings = event.result as Settings;
	loadImages();
}

protected function onSaveButtonClick(event:MouseEvent):void {
	settingsService.updateSettings(settings, onUpdateResultHandler);
	reload = reloadTemp;
}

protected function onRestoreButtonClick(event:MouseEvent):void {
	settings.defaultColor = 13312042;
	settings.storageLocationBannerLogin = '';
	settings.storageLocationSystemLogo = '';
	settings.storageLocationReportLogo = '';
	loadImages();
	reloadTemp = true;
}

private function onUpdateResultHandler(event:ResultEvent=null):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', 'settings.msg.saveSuccess'), Info.INFO);
}

//----------------------------------------------------------------------
//
//	Upload Methods
//
//----------------------------------------------------------------------

public function uploadFile(event:Event, img:String):void {
	reload = false;
	event.stopPropagation();

	switch(img)	{
		case "BannerLogin": {
			fileBannerLoginRef = new FileReference();
			fileBannerLoginRef.addEventListener(Event.SELECT, onSelectFileBannerLogin);
			fileBannerLoginRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileBannerLoginUploadComplete);
			fileBannerLoginRef.addEventListener(IOErrorEvent.IO_ERROR, errorFileBannerLoginEvent);
			fileBannerLoginRef.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorFileBannerLoginEvent);
			fileBannerLoginRef.browse(Util.IMAGE_FILE_FILTER);
			break;
		}
		case "SystemLogo": {
			fileSystemLogoRef = new FileReference();
			fileSystemLogoRef.addEventListener(Event.SELECT, onSelectFileSystemLogo);
			fileSystemLogoRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileSystemLogoUploadComplete);
			fileSystemLogoRef.addEventListener(IOErrorEvent.IO_ERROR, errorFileSystemLogoEvent);
			fileSystemLogoRef.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorFileSystemLogoEvent);
			fileSystemLogoRef.browse(Util.IMAGE_FILE_FILTER);
			break;
		}
		case "ReportLogo": {
			fileReportLogoRef = new FileReference();
			fileReportLogoRef.addEventListener(Event.SELECT, onSelectFileReportLogo);
			fileReportLogoRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileReportLogoUploadComplete);
			fileReportLogoRef.addEventListener(IOErrorEvent.IO_ERROR, errorFileReportLogoEvent);
			fileReportLogoRef.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorFileReportLogoEvent);
			fileReportLogoRef.browse(Util.IMAGE_FILE_FILTER);
			break;
		}
			
		default: {
			throw new Error("Unknown file upload: " + img);
		}
	}
}

public function loadImages():void {
	settingsService.loadImg(settings.storageLocationBannerLogin, function(event:ResultEvent):void {
		bannerLogin	 = event.result as ByteArray;
	}
	);
	settingsService.loadImg(settings.storageLocationSystemLogo, function(event:ResultEvent):void {
		systemLogo = event.result as ByteArray;
	}
	);
	settingsService.loadImg(settings.storageLocationReportLogo, function(event:ResultEvent):void {
		reportLogo = event.result as ByteArray;
	}
	);
}

private function onSelectFileBannerLogin(event:Event):void {
	fileBannerLogin = getFileFromFileReference(fileBannerLoginRef);
	if(verifyImg(fileBannerLogin)) {
		sendFileLogo(fileBannerLogin, "BannerLogin");
	}
}

private function onSelectFileSystemLogo(event:Event):void {
	fileSystemLogo = getFileFromFileReference(fileSystemLogoRef);
	if(verifyImg(fileSystemLogo)) {
		sendFileLogo(fileSystemLogo, "SystemLogo");
	}
}

private function onSelectFileReportLogo(event:Event):void {
	fileReportLogo = getFileFromFileReference(fileReportLogoRef);
	if(verifyImg(fileReportLogo)) {
		sendFileLogo(fileReportLogo, "ReportLogo");
	}
}

private function verifyImg(userFile:UserFile):Boolean {
	if(userFile == null) {
		return false;
	} else {
		if (userFile.size <= 0) {
			ANews.showInfo(this.resourceManager.getString('Bundle', "chat.msg.fileNotValid"), Info.WARNING);
			return false;
		}
		if (userFile.size >= MAX_SIZE) {
			ANews.showInfo(this.resourceManager.getString('Bundle', "chat.msg.fileOverSize"), Info.WARNING);
			return false;
		}
		return true;
	}
}

private function afterSentFile(sentFile:UserFile):void {
	sentFile = null;
	loadImages();
	reloadTemp = true;
}

private function errorSendFile(sentFile:UserFile, event:ErrorEvent):void {
	sentFile = null;

	var errorMsg:String = this.resourceManager.getString('Bundle', "settings.visual.msg.errorSendFile");
	errorMsg += "\n\n[Error ID] " + event.errorID;
	errorMsg += "\n[Text] " + event.text;

	ANews.showInfo(errorMsg, Info.ERROR);
}

private function onFileBannerLoginUploadComplete(event:DataEvent):void {
	afterSentFile(fileBannerLogin);
}

private function onFileSystemLogoUploadComplete(event:DataEvent):void {
	afterSentFile(fileSystemLogo);
}

private function onFileReportLogoUploadComplete(event:DataEvent):void {
	afterSentFile(fileReportLogo);
}

private function errorFileBannerLoginEvent(event:ErrorEvent):void {
	errorSendFile(fileBannerLogin, event);
}

private function errorFileSystemLogoEvent(event:ErrorEvent):void {
	errorSendFile(fileSystemLogo, event);
}

private function errorFileReportLogoEvent(event:ErrorEvent):void {
	errorSendFile(fileReportLogo, event);
}

private function sendFileLogo(file:UserFile, img:String):void {
	if (file == null)
		return;
	
	// prepara algumas váriaveis que devem ser enviadas para o servidor.
	var vars:URLVariables = new URLVariables();
	vars.extension = file.extension;
	vars.fileName = file.name;
	vars.size = file.size;

	// faz o upload.
	var url:URLRequest = new URLRequest(Util.urlUploadLogo());
	vars.source = img;
	url.data = vars;
	switch(img)	{
		case "BannerLogin": {
			fileBannerLoginRef.upload(url);
			settings.storageLocationBannerLogin = img + file.extension;
			break;
		}
		case "SystemLogo": {
			fileSystemLogoRef.upload(url);
			settings.storageLocationSystemLogo = img + file.extension;
			break;
		}
		case "ReportLogo": {
			fileReportLogoRef.upload(url);
			settings.storageLocationReportLogo = img + file.extension;
			break;
		}
		default: {
			throw new Error("Unknown file upload: " + img);
		}
	}
}

private function getFileFromFileReference(fileRef:FileReference):UserFile {
	var _myFile:UserFile = new UserFile();
	_myFile.extension = Util.getExtension(fileRef.name);
	_myFile.name = fileRef.name.replace(_myFile.extension, "");
	try {
		_myFile.size = fileRef.size;
	} catch (e:Error) {
		_myFile.size = 0;
	}
	
	_myFile.loaded = 0;
	_myFile.total = _myFile.size;
	return _myFile;
}

