import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.events.FlexEvent;
import mx.rpc.events.ResultEvent;

import spark.components.NumericStepper;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.service.SettingsService;
import tv.snews.anews.utils.*;

private const fonts:ArrayList = new ArrayList([ "Arial", "Georgia", "Tahoma", "Times New Roman", "Verdana" ]);
private const settingsService:SettingsService = SettingsService.getInstance();
private const locales:ArrayCollection = new ArrayCollection([{value: "en_US", label: "English"}, {value: "es_ES", label: "Español"}, {value: "pt_BR", label: "Português"}]);

[Bindable] private var settings:Settings;

protected function creationCompleteHandler(event:FlexEvent):void {
	settingsService.loadSettings(onLoadSettingsHandler);
}

private function onLoadSettingsHandler(event:ResultEvent):void {
	settings = event.result as Settings;
}

protected function onSaveButtonClick(event:MouseEvent):void {
	settingsService.updateSettings(settings, onUpdateResultHandler);
}

private function onUpdateResultHandler(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', 'settings.msg.saveSuccess'), Info.INFO);
}

private function onNumericStepperChange(event:flash.events.Event):void {
	var stepper:NumericStepper = NumericStepper(event.target);
	if (!stepper.value) {
		stepper.value = (stepper.maximum + stepper.minimum) / 2;
	}
}
