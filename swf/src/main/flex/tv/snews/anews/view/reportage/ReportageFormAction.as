import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.events.TimerEvent;
import flash.utils.Timer;

import flexlib.containers.SuperTabNavigator;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.messaging.messages.IMessage;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ButtonBarButton;

import tv.snews.anews.component.ChangesHistoryWindow;
import tv.snews.anews.component.FormScrollManager;
import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.event.PluginActiveXEvent;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.service.ProgramService;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.cg.ii.CGTemplatesWindow;
import tv.snews.anews.view.reportage.ReportageForm;
import tv.snews.anews.view.reportage.ReportageRevisionRenderer;

private const reportageService:ReportageService = ReportageService.getInstance();

// FIXME Cache desativado
//private const localData:LocalData = LocalData.getInstance(LocalData.REPORTAGE);
private static var timer:Timer = null;

private const guidelineService:GuidelineService = GuidelineService.getInstance();

private static const APPROVE_LABEL:String		= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.approve');
private static const DISAPPROVE_LABEL:String		= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.disapprove');
private static const PRINT_LABEL:String			= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.print');

[Bindable] public var tabManager:TabManager;
[Bindable] private var reportageOld:Reportage = new Reportage();
[Bindable] private var programOld:Program = null;
[Bindable] private var modify:String;
[Bindable] private var mosDevice:MosDevice;

[Bindable] private var selectedVehicles:ArrayCollection = new ArrayCollection();
[Bindable] private var vehicles:ArrayCollection;
[Bindable] private var versions:ArrayCollection = new ArrayCollection();

[Bindable] private var enableButtons:Boolean = true;
[Bindable] private var _collapsed:Boolean = false;

private var _informationCollapsed:Boolean = false;
private var _informationGroup:Boolean = false;
private var _tipOfHeadCollapsed:Boolean = false;
private var _tipOfHeadGroup:Boolean = false;

[Bindable] public var programs:ArrayCollection = DomainCache.programs;
private var fakeProgram:Program = null;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

private var _reportage:Reportage;

private var scrollManager:FormScrollManager;

[Bindable]
public function get reportage():Reportage {
    return _reportage;
}

public function set reportage(value:Reportage):void {
    _reportage = value;

    if (value) {
        selectedVehicles.removeAll();
        for each (var vehicle:String in value.vehicles) {
            selectedVehicles.addItem(CommunicationVehicle.findById(vehicle));
        }
    }
}

private function startUp():void {
	guidelineService.subscribe(onGuidelineMessageReceived);
	reportageService.subscribe(onReportageMessageReceived);
    sectionsList.addEventListener(CrudEvent.UPDATE, updateReadTime);
	
	//Clone da Reportagem
	reportageOld = ObjectUtil.clone(reportage) as Reportage;
	markChanges();
	activeTimer();

	this.callLater(function():void {
		toggleButtons(reportage);
		informationGroup = verifyInformation(reportage);
		tipOfHeadGroup = verifyTipOfHead(reportage);
	});

	//load progrma fake
	loadProgramFake();
	
    vehicles = ArrayCollection(ObjectUtil.clone(CommunicationVehicle.values()));
    reportageCGList.addEventListener(CrudEvent.DELETE, onRemoveReportageCG);

	loadPopUpButtonContent();

	scrollManager = new FormScrollManager(formScroller);
}

//---------------------------
//	Actions
//---------------------------

/**
 * Remove um NavigatorContent de seu TabBar.
 */
public static function removeFromSuperTab(element:DisplayObject):void{
	var container:DisplayObjectContainer = element.parent;
	while (container != null && !(container is SuperTabNavigator)) {
		container = container.parent;
	}
	
	if (container == null)
		return;
	
	var tab:SuperTabNavigator = container as SuperTabNavigator;
	tab.removeChild(element);
}

public function close():void {
	if (reportageOld.isDifferent(reportage)) {
		Alert.show(
			bundle.getString("Bundle", "defaults.msg.dataAreNotSaved"),
			bundle.getString("Bundle", "defaults.msg.confirmation"),
			Alert.CANCEL | Alert.NO | Alert.YES, null, onDataAreNotSaved, null, Alert.YES
		);
	} else {
		onRemove();
	}
}
public function updateReadTime(event:CrudEvent):void {
    reportage.calculateReadTime();
}

private function onDataAreNotSaved(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		save(true);
	} else if(event.detail == Alert.NO) {
		onRemove();
	}
}

private function loadPopUpButtonContent():void {
	if (popUpButton && popUpButtonMenu) {
		popUpButtonMenu.dataProvider = popUpButtonMenu.dataProvider || new ArrayCollection();

		var dataProvider:ArrayCollection = ArrayCollection(popUpButtonMenu.dataProvider);
		dataProvider.removeAll();

		var printEnabled:Boolean = false;
		var approveEnabled:Boolean = false;

		// botão imprimir
		if (ANews.USER.allow("010605")) {
			dataProvider.addItem(PRINT_LABEL);
			printEnabled = true;
		}

		// botão aprovar
		if (ANews.USER.allow("010612")) {
			if (reportage.ok) {
				dataProvider.addItem(DISAPPROVE_LABEL);
				approveEnabled = true;
			} else {
				dataProvider.addItem(APPROVE_LABEL);
				approveEnabled = true;
			}
		}

		switch (dataProvider.length) {
			case 0:
			case 1:
				approveButton.visible = approveButton.includeInLayout = approveEnabled;
				printButton.visible = printButton.includeInLayout = printEnabled;
				popUpButton.visible = popUpButton.includeInLayout = false;
				break;
			case 2:
				approveButton.visible = approveButton.includeInLayout = false;
				printButton.visible = printButton.includeInLayout = false;
				popUpButton.visible = popUpButton.includeInLayout = true;
				break;
		}
	}
}

private function popUpButtonItemClick(event:MenuEvent):void {
	switch (event.item as String) {
		case APPROVE_LABEL:
		case DISAPPROVE_LABEL:
			changeApproval();
			break;
		case PRINT_LABEL:
			saveAndPrint();
			break;
	}
}

private function changeApproval():void {
	if (reportage.id) {
		reportageService.changeOk(reportage.id, !reportage.ok);
	} else {
		ANews.showInfo(bundle.getString('Bundle', 'reportage.msg.cannotApprove'), Info.WARNING);
	}
}

private function save(closeable:Boolean, printAfterSave:Boolean = false):void {
	if (!Util.isValid(validadores)) {
		ANews.showInfo(bundle.getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
		return;
	}

    timer.stop();
    enableButtons = false;

	copyDataFromForm(reportage);
	
    if (markChanges()) {
        var self:ReportageForm = this;
        reportageService.save(reportage, function(event:ResultEvent):void {
            ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
            enableButtons = true;

            var result:Reportage = event.result as Reportage;

            if (!reportage.id || reportage.id <= 0) {
                tabManager.updateTabKey(self, "reportage_" + result.id);
            }

            // Configure new ids.
            reportage = result;

            //Clone da Reportagem
            reportageOld = ObjectUtil.copy(reportage) as Reportage;
            modify = '';

            if (printAfterSave) {
                print();
            }

            if (closeable) {
                onRemove();
            }
        }, function (event:FaultEvent):void {
            ANews.showInfo(event.fault.faultString, Info.ERROR);
            timer.start();

            // Enable buttons, server already answer the client.
            enableButtons = true;
        });
    } else {
        enableButtons = true;
        if (closeable) {
            onRemove();
            return;
        }
    }

    timer.start();
}

private function getVersions():void {
    if(reportage && reportage.id > 0) {
	    reportageService.loadChangesHistory(reportage.id, onGetVersions);
    }
}

private function saveAndPrint():void {
	if (reportage.id == 0 || markChanges()) {
		Alert.show(
				bundle.getString("Bundle", "defaults.msg.saveBeforePrint"),
				bundle.getString("Bundle", "defaults.msg.confirmation"),
						Alert.NO | Alert.YES, null,

				function(event:CloseEvent):void {
					if (event.detail == Alert.YES) {
						save(false, true);
					}
				}
		);
	} else {
		print();
	}
}

private function print():void {
	reportUtil.generate(ReportsUtil.REPORTAGE, { ids: reportage.id });
}

private function expandAll():void{
	collapsed = false;
	collapse(false);
}

private function collapseAll():void {
	collapsed = true;
	collapse(true);
}

private function collapse(collapsed:Boolean):void {
	for each(var section:ReportageSection in reportage.sections) {
		section.collapsed = collapsed;
	}
}

private function activeTimer():void {
	if (timer) {
		stopTimer();
	}
	
	timer = new Timer(5000);
	timer.addEventListener(TimerEvent.TIMER,
		function(event:TimerEvent):void {
// 			FIXME Cache desativado
//			localData.save(reportage);
			copyDataFromForm(reportage);
			markChanges();
		}
	);
	timer.start();
}

private function markChanges():Boolean {
	modify = reportageOld.isDifferent(reportage) ? '* ' : '';
	return modify == '* ';
}

private function stopTimer():void {
	timer.stop();
// 	FIXME Cache desativado
//	localData.clear();
}

public function onRemove():void {
	scrollManager.deactivate();

    reportageCGList.removeEventListener(CrudEvent.DELETE, onRemoveReportageCG);
    sectionsList.removeEventListener(CrudEvent.UPDATE, updateReadTime);
    tabManager.removeTab(this);
    guidelineService.unsubscribe(onGuidelineMessageReceived);
	reportageService.unsubscribe(onReportageMessageReceived);
	
	stopTimer();

	if (reportage.id > 0) {
		// Remove o bloqueio de edição
		reportageService.unlockEdition(reportage.id, function(event:ResultEvent):void {});
	}
}

private function addCG():void {
    var program:Program = cmbProgram.selectedItem ? cmbProgram.selectedItem as Program: null;
    if(program != null)
    ProgramService.getInstance().loadById(program.id, onLoadGc);
}

private function onLoadGc(event:ResultEvent):void {
    var program:Program = event.result as Program;
    if(program.cgDevice) {
        if (program.cgDevice is IIDevice) {
            this.dispatchEvent(new ANewsPlayEvent(ANewsPlayEvent.CLOSE));
            var iiWindow:CGTemplatesWindow = new CGTemplatesWindow();
            iiWindow.device = program.cgDevice as IIDevice;
            iiWindow.addEventListener(CrudEvent.CREATE, onAddStoryCGII);
            PopUpManager.addPopUp(iiWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
            PopUpManager.centerPopUp(iiWindow);
        } else if (program.cgDevice is MosDevice) {
            mosDevice = program.cgDevice as MosDevice;
            if (mosDevice.activeX) {
                openActiveX();
            } else {
                // Outros CGDeivices MOS
                ANews.showInfo(bundle.getString('Bundle', 'cg.msg.verifyConfigDevice'), Info.WARNING);
            }
        }
    } else {
        ANews.showInfo(bundle.getString('Bundle', 'cg.notFound'), Info.WARNING);
    }
}

private function openActiveX():void {
    this.dispatchEvent(new PluginActiveXEvent(PluginActiveXEvent.OPEN, mosDevice, reportage));
}

private function onAddStoryCGII(crudEvent:CrudEvent):void {
    var template:IITemplate = crudEvent.entity as IITemplate;
    var reportageCG:ReportageCG = new ReportageCG(template);
    reportage.addCG(reportageCG);
    this.callLater(scrollerToEnd);
}

//private function onAddReportageCG(crudEvent:CrudEvent):void {
//	throw new Error("CORRIGIR");
//    var template:CGTemplate = crudEvent.entity as CGTemplate;
//    var reportageCG:ReportageCG = new ReportageCG(template);
//    reportage.addCG(reportageCG);
//    this.callLater(scrollerToEnd);
//}

private function onRemoveReportageCG(crudEvent:CrudEvent):void {
    var reportageCG:ReportageCG = crudEvent.entity as ReportageCG;
    reportage.removeCG(reportageCG);
}

//---------------------------
//	Actions of toolbar
//---------------------------
private function addSection():void {
	var section:ReportageSection = new ReportageSection();
	switch (sectionsToolBar.selectedIndex) {
		case 0:	section.type = SectionType.OFF; addReportageSection(section); break;
		case 1: section.type = SectionType.INTERVIEW; addReportageSection(section); break;
		case 2:	section.type = SectionType.APPEARANCE; addReportageSection(section); break;
		case 3: section.type = SectionType.ART; addReportageSection(section); break;
		case 4: section.type = SectionType.SOUNDUP; addReportageSection(section); break;
		case 5: addInformation(); toggleInformationButton(false); break;
		case 6: addTipOfHead(); toggleTipOfHeadButton(false); break;
		case 7: addCG(); break;
        default : break;
	}
    if(sectionsToolBar.selectedIndex == 5 || sectionsToolBar.selectedIndex == 6) {
        this.callLater(scrollerToBegin);
    }
    if(sectionsToolBar.selectedIndex >= 0 && sectionsToolBar.selectedIndex <= 4) {
        this.callLater(scrollerToEnd);
    }
}

public function toggleButtons(_reportage:Reportage):void {
	if (sectionsToolBar && _reportage) {
		var informationButton:ButtonBarButton = sectionsToolBar.dataGroup.getElementAt(5) as ButtonBarButton;
		var tipOfHeadButton:ButtonBarButton = sectionsToolBar.dataGroup.getElementAt(6) as ButtonBarButton;
		var cgButton:ButtonBarButton = sectionsToolBar.dataGroup.getElementAt(7) as ButtonBarButton;

		informationButton.enabled = _reportage.information == null || _reportage.information.length == 0;
		tipOfHeadButton.enabled = _reportage.tipOfHead == null || _reportage.tipOfHead.length == 0;
		cgButton.enabled = _reportage.program != null;
	}
}

private function addReportageSection(section:ReportageSection):void {
	section.reportage = reportage;	
	reportage.sections.addItem(section);	
}

private function addInformation():void {
	informationGroup = true;
	reportage.information = "";
}

private function addTipOfHead():void {
	tipOfHeadGroup = true;
	reportage.tipOfHead = "";
}

private function removeInformation():void {
	reportage.removeInformation();
	informationGroup = verifyInformation(reportage);
	informationCollapsed = verifyInformation(reportage);
	toggleInformationButton(true);
}

private function removeTipOfHead():void {
	reportage.removeTipOfHead();
	tipOfHeadGroup = verifyTipOfHead(reportage);
	informationCollapsed = verifyTipOfHead(reportage);
	toggleTipOfHeadButton(true);
}

protected function toggleInformationButton(enabled:Boolean):void {
	var informationButton:ButtonBarButton = sectionsToolBar.dataGroup.getElementAt(5) as ButtonBarButton;
	informationButton.enabled = enabled;
}

protected function toggleTipOfHeadButton(enabled:Boolean):void {
	var tipOfHeadButton:ButtonBarButton = sectionsToolBar.dataGroup.getElementAt(6) as ButtonBarButton;
	tipOfHeadButton.enabled = enabled;
}

protected function toggleCGButton(enabled:Boolean):void {
	var cgButton:ButtonBarButton = sectionsToolBar.dataGroup.getElementAt(7) as ButtonBarButton;
	cgButton.enabled = enabled;
}

private function programChange():void {
    programOld = reportage.program != null ? reportage.program : null;
    reportage.program = cmbProgram.selectedItem ? cmbProgram.selectedItem as Program : null;
    var oldDevice:Device = programOld != null && programOld.cgDevice != null ? programOld.cgDevice : null;
    var newDevice:Device = reportage.program != null && reportage.program.cgDevice != null ? reportage.program.cgDevice : null;

    stateCGChange(newDevice != null);

    if(reportage.cgs != null && reportage.cgs.length > 0 && newDevice != null && oldDevice != null && newDevice.id != oldDevice.id) {
        //TODO MARCAR CG COM COR DIFERENTE INDICADO QUE NÃO SÃO DO MESMO DEVICE.
    }
}

private function verifyInformation(_reportage:Reportage):Boolean {
	return _reportage.information != null;
}

private function verifyTipOfHead(_reportage:Reportage):Boolean {
	return _reportage.tipOfHead != null;
}

private function stateCGChange(enabled:Boolean):void {
   toggleCGButton(enabled);
}

private function hideOrShowInformation():void {
	informationCollapsed = !informationCollapsed;
}

private function hideOrShowTipOfHead():void {
	tipOfHeadCollapsed = !tipOfHeadCollapsed;
}

private function scrollerToBegin():void {
	formScroller.verticalScrollBar.value = 0.0;
}

private function scrollerToEnd():void {
	formScroller.verticalScrollBar.value = UIComponent.DEFAULT_MAX_HEIGHT;
}

private function readGuideline():void {
	if (ANews.USER.allow("01050101")) {
		var self:ReportageForm = this;
		guidelineService.loadById(reportage.guideline.id, function(event:ResultEvent):void {
			self.currentState = "preview";
			reportage.guideline = Guideline(event.result);
		});
	}
}

protected function onDrop(event:DragEvent):void {
	reportage.sections.refresh();
}

private function loadProgramFake():void {
	programs = ObjectUtil.copy(DomainCache.programs) as ArrayCollection;	
	if (programs) {
		programs.addItemAt(getFakeProgram(), 0);
		if (reportage && reportage.program) {
			cmbProgram.selectedItem = reportage.program;
		} else {
			cmbProgram.selectedIndex = 0;	
		}
	}
}

private function getFakeProgram():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL" no form.
	if (fakeProgram == null) {
		fakeProgram = new Program();
		fakeProgram.id = -1;
		fakeProgram.name = bundle.getString("Bundle", "defaults.drawerProgram");
	}
	return fakeProgram;
}

//--------------------------
//	Drag n' drop
//--------------------------

private function onCGDragOver(event:DragEvent):void {
    var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;

    if (items && items.length > 0) {
        var dragItem:Object = items[0];
        if (!(dragItem is ReportageCG)) {
            event.preventDefault();
            event.stopPropagation();
            DragManager.showFeedback(DragManager.NONE);
        }
    }
}

private function onCGDragDrop(event:DragEvent):void {
//    event.currentTarget.layout.hideDropIndicator();
    callLater(function ():void {
        reportage.reorderCGs();
    });
}

//---------------------------
//	Asynchronous answer
//---------------------------
private function onGetVersions(event:ResultEvent):void {
	versions = event.result as ArrayCollection;

	if (versions == null || versions.length == 0) {
		ANews.showInfo(bundle.getString('Bundle', 'reportage.msg.noVersions'), Info.WARNING);
	} else {
		var changesWindow:ChangesHistoryWindow = new ChangesHistoryWindow();
		changesWindow.addEventListener(CloseEvent.CLOSE, 
			function(event:CloseEvent):void {
				PopUpManager.removePopUp(changesWindow);
			}
		);
		
		changesWindow.dataProvider = versions;
		changesWindow.itemRenderer = ReportageRevisionRenderer;
		
		PopUpManager.addPopUp(changesWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
		PopUpManager.centerPopUp(changesWindow);
	}
}

/**
 * @private
 * Handler para as mensagens enviadas pelo servidor para notificar ações sobre
 * as pautas feitas pelos usuários, como operações de CRUD e bloqueio de edição.
 */
private function onGuidelineMessageReceived(event:MessageEvent):void {
	if(!reportage.guideline)
		return;
	
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var current:Guideline = message.body as Guideline;
	
	// Sugestão para a correção do problema de sincronia nas atualizações
	if (action == EntityAction.UPDATED && current.id == reportage.guideline.id) {
		reportage.guideline.updateFields(current);
	}
}


/**
 * @private
 * Handler para as mensagens enviadas pelo servidor para notificar ações sobre
 * as pautas feitas pelos usuários, como operações de CRUD e bloqueio de edição.
 */
private function onReportageMessageReceived(event:MessageEvent):void {
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var current:Reportage = message.body as Reportage;
	var field:String = message.headers.field;

	// Sugestão para a correção do problema de sincronia nas atualizações
	if (action == EntityAction.UPDATED && current.id == reportage.id) {
		reportage.updateFields(current);
	}

	if (action == EntityAction.FIELD_UPDATED && current.id == reportage.id) {
		if (field == "ok") {
            var ok:Boolean = message.headers.value as Boolean;
			if (ok) {
				reportage.approve();
				reportageOld.approve();
			} else {
				reportage.disapprove();
				reportageOld.disapprove();
			}
			loadPopUpButtonContent();
		} else if (field == "programs") {
            var programs:ArrayCollection = message.headers.value as ArrayCollection;
            reportage.programs.removeAll();
            reportage.programs.addAll(programs);
        }
	}
}

private function copyDataFromForm(reportage:Reportage):void {
	reportage.reporter = cmbReporter.selectedItem as User;
	reportage.date = dfDate.selectedDate;

	var program:Program = cmbProgram.selectedItem as Program;
	if (program && program.id == -1) {
		reportage.program = null;
	} else {
		reportage.program = program;
	}
	
    reportage.vehicles.removeAll();
    for each (var vehicle:Object in acVehicles.selectedItems) {
        reportage.vehicles.addItem(vehicle.id);
    }
}


//---------------------------
//	Getters and Setters
//---------------------------

[Bindable] 
public function get informationCollapsed():Boolean {
	return _informationCollapsed;
}

public function set informationCollapsed(value:Boolean):void {
	_informationCollapsed = value;
}

[Bindable] 
public function get informationGroup():Boolean {
	return _informationGroup;
}

public function set informationGroup(value:Boolean):void {
	_informationGroup = value;
}

[Bindable] 
public function get tipOfHeadCollapsed():Boolean {
	return _tipOfHeadCollapsed;
}

public function set tipOfHeadCollapsed(value:Boolean):void {
	_tipOfHeadCollapsed = value;
}

[Bindable] 
public function get tipOfHeadGroup():Boolean {
	return _tipOfHeadGroup;
}

public function set tipOfHeadGroup(value:Boolean):void {
	_tipOfHeadGroup = value;
}

[Bindable] 
public function get collapsed():Boolean {
	return _collapsed;
}

public function set collapsed(value:Boolean):void {
	_collapsed = value;
}
