import flash.events.Event;

import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.ContactNumber;
import tv.snews.anews.domain.NumberType;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.CityService;
import tv.snews.anews.service.ContactService;
import tv.snews.anews.service.StateService;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.StringUtils;
import tv.snews.anews.utils.DomainCache;

[Bindable] public var buttonsVisible:Boolean = true;
[Bindable] public var margin:int = 10;

private var contactToDell:ContactNumber = new ContactNumber();

private const contactService:ContactService = ContactService.getInstance();

//------------------------------------------------------------------
//	Public Methods & Properties
//------------------------------------------------------------------

private var _contact:Contact = new Contact();

[Bindable]
public function get contact():Contact {
	return _contact;
}

public function set contact(value:Contact):void {
	_contact = value;
	txtPhone.text = "";
}

public function deleteContactNumber(contactNumber:ContactNumber):void {
	contactToDell = contactNumber;
	Alert.show(
		this.resourceManager.getString('Bundle' , 'defaults.msg.confirmAllDelete'), 
		this.resourceManager.getString('Bundle' , 'defaults.msg.confirmation'), 
		Alert.NO | Alert.YES, null, confirmationDellContactNumber);
}

public function save():void {
	if (!StringUtils.isNullOrEmpty(txtPhone.text)) {
		addContactNumber();
	}
	if (contact.numbers.length == 0) {
		txtPhone.errorString = this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields');
	} else {
		txtPhone.errorString = "";
	}
	
	if (!Util.isValid(simpleValidators )) {
		ANews.showInfo(this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
		return;
	}
	// Contato simple da pauta ou notícia
	var simpleContact:Contact = new Contact();
	simpleContact.id = contact.id;
	simpleContact.name = contact.name;
	simpleContact.profession = contact.profession;
	simpleContact.numbers = contact.numbers;
	
	contactService.save(simpleContact, onSaveSuccess);

}

public function cancel():void {
	contact = null;
	
	var event:Event = new Event(Event.CANCEL);
	this.dispatchEvent(event);
}


//------------------------------------------------------------------
//	Private Actions
//------------------------------------------------------------------


private function onCreationComplete(event:FlexEvent):void {
}


private function onFault(event:FaultEvent):void {
	var exceptionMessage:String = (event.fault.faultString as String).split(':')[1];
	ANews.showInfo(exceptionMessage, Info.ERROR);
}

private function addContactNumber():void {
	if (StringUtils.isNullOrEmpty(txtPhone.text)) {
		txtPhone.errorString = this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields');
		return;
	} else {
		txtPhone.errorString = "";
	}
	
	//Add the number to contact's numbers list.
	var number:ContactNumber = new ContactNumber;
	number.numberType = comboNumberType.selectedItem as NumberType;
	number.value = txtPhone.text;
	contact.numbers.addItem(number);
	
	//Clear fields.
	txtPhone.text = "";
	comboNumberType.selectedIndex = 0;
	txtPhone.setFocus();
}

private function confirmationDellContactNumber(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		contact.numbers.removeItemAt(contact.numbers.getItemIndex(contactToDell));
	}
}

private function onSaveSuccess(event:ResultEvent):void {
	var returnedContact:Contact = event.result as Contact;
	var crudEvent:CrudEvent = new CrudEvent(CrudEvent.UPDATE, returnedContact);
	this.dispatchEvent(crudEvent);
	ANews.showInfo(this.resourceManager.getString('Bundle' , 'defaults.msg.saveSuccess'), Info.INFO);
}