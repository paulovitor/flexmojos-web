import flash.events.*;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.view.guideline.grid.GuidelineGroup;

private const bundle:IResourceManager = ResourceManager.getInstance();

private const settingsService:SettingsService = SettingsService.getInstance();

private const guidelineSortOptions:ArrayCollection = new ArrayCollection(
		[
			{ value: GuidelineGroup.CLASSIFICATION, label: bundle.getString('Bundle', 'guideline.sortByClassification') },
			{ value: GuidelineGroup.PROGRAM, label: bundle.getString('Bundle', 'guideline.sortByProgram') + " " + bundle.getString('Bundle', 'defaults.inParenthesis') },
			{ value: GuidelineGroup.REPORTER, label: bundle.getString('Bundle', 'guideline.sortByReporter') },
			{ value: GuidelineGroup.VEHICLE, label: bundle.getString('Bundle', 'guideline.sortByVehicle') }
		]
);

private const yesOrNoOptions:ArrayCollection = new ArrayCollection(
		[
			bundle.getString("Bundle", "defaults.btn.yes"),
			bundle.getString("Bundle", "defaults.btn.no")
		]
);

[Bindable]
private var settings:Settings;

protected function startUp(event:FlexEvent):void {
	settingsService.loadSettings(onLoadSettingsHandler);
}

protected function onRemove(event:FlexEvent):void {
}

private function onLoadSettingsHandler(event:ResultEvent):void {
	settings = Settings(event.result);
	fillFields();
}

private function fillFields():void {
	// ordenação das pautas
	for (var i:int = 0; i < guidelineSortOptions.length; i++) {
		if (guidelineSortOptions[i].value == settings.defaultGuidelineGridSort) {
			guidelineSortCombo.selectedIndex = i;
		}
	}

    // exibição do equipe nas pautas
    guidelineTeamCombo.selectedIndex = settings.enableGuidelineTeam ? 0 : 1;

	// ativação do botão "carregar" do espelho
	loadRundownCombo.selectedIndex = settings.enableRundownLoadButton ? 0 : 1;

	// exibição do editor de imagens no espelho
	rundownImageEditorCombo.selectedIndex = settings.enableRundownImageEditor ? 0 : 1;

	// exibição do "últ. alteração" no espelho
	rundownAuthorCombo.selectedIndex = settings.enableRundownAuthor ? 0 : 1;

    // exibição do editor de imagens no roteiro
    scriptPlanImageEditorCombo.selectedIndex = settings.enableScriptPlanImageEditor ? 0 : 1;

    // exibição do "últ. alteração" no roteiro
    scriptPlanAuthorCombo.selectedIndex = settings.enableScriptPlanAuthor ? 0 : 1;

    // ativação do botão "carregar" do roteiro
    loadScriptPlanCombo.selectedIndex = settings.enableScriptPlanLoadButton ? 0 : 1;

	// incluir, por padrão, o bloco stand by na impressão
	includeStandByCombo.selectedIndex = settings.includeStandBy ? 0 : 1;
}

protected function onSaveButtonClick(event:MouseEvent):void {
	settings.defaultGuidelineGridSort = guidelineSortCombo.selectedItem.value;
    settings.enableGuidelineTeam = guidelineTeamCombo.selectedIndex == 0;           // 0 == Sim
    settings.enableRundownLoadButton = loadRundownCombo.selectedIndex == 0;         // 0 == Sim
	settings.enableRundownImageEditor = rundownImageEditorCombo.selectedIndex == 0; // 0 == Sim
	settings.enableRundownAuthor = rundownAuthorCombo.selectedIndex == 0;           // 0 == Sim
    settings.enableScriptPlanImageEditor = scriptPlanImageEditorCombo.selectedIndex == 0; // 0 == Sim
	settings.enableScriptPlanAuthor = scriptPlanAuthorCombo.selectedIndex == 0;           // 0 == Sim
    settings.enableScriptPlanLoadButton = loadScriptPlanCombo.selectedIndex == 0;       // 0 == Sim
	settings.includeStandBy = includeStandByCombo.selectedIndex == 0;               // 0 == Sim

	settingsService.updateSettings(settings, onUpdateResultHandler);
}

private function onUpdateResultHandler(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', 'settings.systemSettings.refreshAlert'), Info.WARNING);
	ANews.showInfo(bundle.getString('Bundle', 'settings.msg.saveSuccess'), Info.INFO);
}
