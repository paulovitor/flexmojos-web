import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.DragSource;
import mx.core.IFactory;
import mx.core.IFlexDisplayObject;
import mx.events.CalendarLayoutChangeEvent;
import mx.managers.DragManager;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ComboBox;
import spark.components.Group;
import spark.components.RichEditableText;
import spark.components.TextInput;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.IGridItemRenderer;
import spark.events.GridEvent;
import spark.events.IndexChangeEvent;
import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.GuidelineParams;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.agency.Agency;
import tv.snews.anews.view.guideline.GuidelineForm;
import tv.snews.anews.view.guideline.GuidelineHelper;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const dateTimeUtil:DateUtil = new DateUtil();
private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

private var guidelineService:GuidelineService = GuidelineService.getInstance();
private var searchParams:GuidelineParams = new GuidelineParams();

[Bindable] private var programDrawer:Program;
[Bindable] private var programsWithFake:ArrayCollection;
[Bindable] private var total:int = 0;
[Bindable] private var showViewer:Boolean = false;
[Bindable] private var vehicles:ArrayCollection;
[Bindable] private var guidelineStates:ArrayCollection;
[Bindable] private var guidelineToEdit:Guideline = null;
[Bindable] private var helper:GuidelineHelper = new GuidelineHelper();
[Bindable] public var tabManager:TabManager = new TabManager();

private function startUp():void {
	loadProgramsWithFake();
	vehicles =	ArrayCollection(ObjectUtil.clone(CommunicationVehicle.values()));
	guidelineStates = ArrayCollection(ObjectUtil.clone(DocumentState.values()));
	this.addEventListener(CrudEvent.EDIT, onEditEvent);
	tabManager.tabNavigator = navigator;
	navigator.addEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false, 0, true);
}

protected function onRemove():void {
	navigator.removeEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false);
	for each (var tab:Object in navigator.getChildren()) {
		if (tab is GuidelineForm) {
			var obj:GuidelineForm = tab as GuidelineForm;
			obj.onRemove();
		}
	}
	tabManager.removeAllTabs();
}

private function onGridCreationComplete():void {
	// Oculta a coluna "equipe" caso for configurado para isso
	teamColumn.visible = DomainCache.settings.enableGuidelineTeam;
}

private function onEditEvent(event:CrudEvent):void{
	if (event.entity is Guideline) {
		var guideline:Guideline = event.entity as Guideline;
		
		// Bloqueia a edição da pauta para os outros usuários
		guidelineService.lockEdition(guideline.id, ANews.USER.id, function(event:ResultEvent):void {
			tabManager.openTab(GuidelineForm, "guideline_" + guideline.id,
				{
					guideline: Guideline(ObjectUtil.copy(guideline)),
					tabManager: tabManager
				}
			);
		});
	}
}

protected function tabClosedHandler(event:SuperTabEvent):void {
	//Verifica detalhes do objeto da tab antes de fechar
	if (navigator.getChildAt(event.tabIndex) is GuidelineForm) {
		event.preventDefault();
		var obj:Object = navigator.getChildAt(event.tabIndex) as Object;
		if (obj) {
			obj.close();
		}
	}
}


//-- 
//-	Services Methods
//--

/**
 * Efetua a busca.
 */
private function searchGuidelines():void {
	// Carrega os parâmetros da busca
    loadFieldsToSearch();
	
	if (periodIsInvalid()) {
		ANews.showInfo(bundle.getString('Bundle', 'story.archive.msg.invalidPeriod'), Info.WARNING);
	} else {
		search();
	}
}

/**
 * Limpar filtro.
 */
private function clearFilter():void {
	listArchivedGuidelines.selectedIndex = -1;
	periodStart.text = '';
	periodStart.selectedDate = null;

	periodEnd.text = '';
	periodEnd.selectedDate = null;

    slugInput.text = '';
	searchText.text = '';
	ignoreText.text = '';
	searchType.selectedIndex = 0;
    total = 0;

	if (producerCombo.dataProvider && producerCombo.dataProvider.length > 0)
		producerCombo.selectedIndex = -1;
	
	if (reporterCombo.dataProvider && reporterCombo.dataProvider.length > 0)
		reporterCombo.selectedIndex = -1;
	
	if (programCombo.dataProvider && programCombo.dataProvider.length > 0)
		programCombo.selectedIndex = -1;
	
	if (classificationCombo.dataProvider && classificationCombo.dataProvider.length > 0)
		classificationCombo.selectedIndex = -1;

    if (teamCombo.dataProvider && teamCombo.dataProvider.length > 0)
        teamCombo.selectedIndex = -1;
	
	if (vehicleCombo.dataProvider && vehicleCombo.dataProvider.length > 0)
		vehicleCombo.selectedIndex = -1;
	
	loadFieldsToSearch();
	
	//reseta os valores do compoenente PageBar
	pgGuidelineArchive.load(null, 0, 0, 0);
}

/**
 * Gera impressão da pauta selecionada.
 */
private function print():void {
	var params:Object = new Object();
	params.ids = Util.getSelectedsAsString(listArchivedGuidelines);

	reportUtil.generate(ReportsUtil.GUIDELINE, params);
}

private function printGrid():void {
    var params:Object = new Object();
    var program:Program = (programCombo.selectedItem as Program);
    var producer:User = (producerCombo.selectedItem as User);
    var reporter:User = (reporterCombo.selectedItem as User);
    var classification:GuidelineClassification = (classificationCombo.selectedItem as GuidelineClassification);
    var team:UserTeam = (teamCombo.selectedItem as UserTeam);
    var vehicle:Object = vehicleCombo.selectedItem;
    params.periodStart = periodStart.text;
    params.periodEnd = periodEnd.text;
    params.programId =  program != null ? program.id : "";
    params.producerId = producer != null ? producer.id : "";
    params.reporterId = reporter != null ? reporter.id : "";
    params.classificationText = classification != null ? classification.id : "";
    params.vehicle = vehicle != null ? vehicle.id : "";
    params.team = team != null ? team.id : "";
    params.slug = slugInput.text;
    params.text = searchText.text;
    params.type = (searchType.selectedItem as Object).id;
    params.ignoreText = ignoreText.text;

    if(total > 100) {
        ANews.showInfo(bundle.getString('Bundle', "defaults.msg.listTheFirstHundred"), Info.WARNING);
    }

    reportUtil.generate(ReportsUtil.GUIDELINE_ARCHIVE_GRID, params);
}

private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
    total = result.total;
	pgGuidelineArchive.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function search(page:int=1):void {
		showViewer = false;
    listArchivedGuidelines.selectedIndex = -1;

    searchParams.slug = StringUtils.trim(searchParams.slug || "");
    searchParams.text = StringUtils.trim(searchParams.text || "");
    searchParams.ignoreText = StringUtils.trim(searchParams.ignoreText || "");
    searchParams.page = page < 1 ? 1 : page;

	if (programCombo.selectedIndex < 0) {
		programCombo.selectedIndex = -1;
		programCombo.selectedItem = null;
		programCombo.textInput.text = "";
	} else {
		var program:Program =	programCombo.selectedItem as Program;
		if (program.id == -1) {
			searchParams.program = program;
		}
	}

	guidelineService.fullTextSearch(searchParams, onListSuccess);
}

//Verifica se os dados do paginador existem e se alguma pauta não está sendo copiada, caso contrário, não permite a troca de páginas
private function onPageChange(page:int=1):void {
    if(pgGuidelineArchive.data && !sendTo.visible) search(page);
}

// -------------------------
// Handler's
// -------------------------

private function onEdit(event:Event):void {
	if (!ANews.USER.allow('01050103'))
		return;
	
	var selectedGuideline:Guideline = listArchivedGuidelines.selectedItem as Guideline;
	
	if (selectedGuideline) {
		guidelineService.loadById(selectedGuideline.id, function(event:ResultEvent):void{
			guidelineToEdit = event.result as Guideline;
			if (guidelineToEdit) {
				
				if (guidelineToEdit.excluded) {
					ANews.showInfo(bundle.getString("Bundle", "defaults.msg.noEditDeleted"), Info.WARNING);
					return;
				}
				
				if (guidelineToEdit.state == DocumentState.COMPLETED) {
					ANews.showInfo(bundle.getString("Bundle", "guideline.msg.editCompleteError"), Info.WARNING);
					return;
				}
				if (guidelineToEdit.state == DocumentState.INTERRUPTED) {
					ANews.showInfo(bundle.getString("Bundle", "guideline.msg.editInterruptedError"), Info.WARNING);
					return;
				}
				guidelineService.isLockedForEdition(guidelineToEdit.id, onCheckLockedSuccess);	
			}
		});
	} else {
		ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.selectOneGuideline"), Info.WARNING);
		guidelineToEdit = null;
	}
}

private function onCheckLockedSuccess(event:ResultEvent):void {
	var user:User = event.result as User;
	if (user) {
        helper.guidelineInEditing(user, tabManager, "guideline_" + guidelineToEdit.id, bundle.getString("Bundle", "guideline.daily.title"));
	} else {
		if (guidelineToEdit) {
			var crudEvent:CrudEvent = new CrudEvent(CrudEvent.EDIT);
			crudEvent.entity = guidelineToEdit;
			this.dispatchEvent(crudEvent);
		} else {
			ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.selectOneGuideline"), Info.WARNING);
		}
	}
}

private function onSaveSuccess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', "defaults.msg.saveSuccess"), Info.INFO);
}

private function onCopySuccess(event:ResultEvent):void {
	sendTo.visible = false;
	sendTo.includeInLayout = false;
	validGuidelineDateCopyTo.enabled = true;
	validCbProgramsCopyTo.enabled = true;
	ANews.showInfo(bundle.getString('Bundle', "defaults.msg.copySuccess"), Info.INFO);
}

protected function onChangeSearchText(event:TextOperationEvent):void {
	var obj:TextInput = event.target as TextInput;
	if (obj.text.length == 0) {
		obj.errorString = "";
	}
}

private function onViewClick(event:MouseEvent):void{
	showViewer = true;
}

private function onCancelClick(event:MouseEvent):void {
	showViewer = false;
}

protected function onChangeProgramCombo(event:IndexChangeEvent):void {
	if (event.target is ComboBox) {
		var cb:ComboBox = event.target as ComboBox;
		if (cb.selectedItem is String) {
			cb.textInput.text = "";
			cb.selectedItem = null;
			cb.selectedIndex = -1;
		}
	}
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && listArchivedGuidelines.selectedItem != null && listArchivedGuidelines.selectedIndex != -1) {
		if (showViewer == true) {
			showViewer = false;
		} else {
			showViewer = true;
		}
	}
}

private function onSearchKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        searchGuidelines();
    }
}
//-- 
//-	Utilities Methods
//--

public function saveCopy():void {
	if (!Util.isValid(validCopyTo)) {
		ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
	} else {
		var dateToCopy:Date = guidelineDateCopyTo.selectedDate;
		var programIdTocopy:int = (cbProgramsCopyTo.selectedItem as Program).id;
		var guidelineIdToCopy:Number = (listArchivedGuidelines.selectedItem as Guideline).id;
		guidelineService.copy(dateToCopy, programIdTocopy, guidelineIdToCopy, onCopySuccess);
	}
}

public function copyTo():void {
	validGuidelineDateCopyTo.enabled = true;
	validCbProgramsCopyTo.enabled = true;
	sendTo.visible = true;
	sendTo.includeInLayout = true;
}

public function cancelCopyTo():void {
	sendTo.visible = false;
	sendTo.includeInLayout = false;
	validGuidelineDateCopyTo.enabled = false;
	validCbProgramsCopyTo.enabled = false;
}

/**
 * Popula os filtros de busca para seu objeto correspondente.
 */
private function loadFieldsToSearch():void {
    searchParams.initialDate = periodStart.selectedDate;
    searchParams.finalDate = periodEnd.selectedDate;
	searchParams.producer = producerCombo.selectedItem as User;
    searchParams.reporter = reporterCombo.selectedItem as User;
    searchParams.program = programCombo.selectedItem as Program;
    searchParams.slug = slugInput.text;
    searchParams.text = searchText.text;
    searchParams.ignoreText = ignoreText.text;
    searchParams.searchType = searchType.selectedItem.id;
	searchParams.classification =  classificationCombo.selectedItem as GuidelineClassification;
    searchParams.team =  teamCombo.selectedItem as UserTeam;
	searchParams.vehicle = vehicleCombo.selectedItem ?  vehicleCombo.selectedItem.id : null;
}

/**
 * Método que verifica se é necessário atribuir o campo 'final' com a data de 'inicial'
 */
private function dateChanged(date:Date):void {
	if (periodEnd.selectedDate == null)
		return;
	if (dateTimeUtil.compareDates(date, periodEnd.selectedDate) > 0) {
		periodEnd.selectedDate = periodStart.selectedDate;
	}
/*disabledRanges="{[ {rangeEnd	: dateTimeUtil.subtractDays(this.guidelineDate1.selectedDate, 1)  } ]}"*/
}

/**
 * Formata a apresentação da data.
 */
private function dateLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;

	if (guideline != null && guideline.date != null)
		return dateTimeUtil.dateToString(guideline.date);
	return "";
}

/**
 * Formata a apresentação do status da pauta
 */
private function droppedDownLabelFunction(item:Object, column:GridColumn):String {
	return DocumentState.i18nOf((item as Guideline).state).toUpperCase();
}

/**
 * Formata a apresentação do horário do primeiro roteiro cadastrado
 */
private function timeLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	var time:String = "";

	var i:int = 0;
	var firstDate:Date;

	if (guideline != null && guideline.guides != null) {
		for each (var guide:Guide in guideline.guides) {
			if (i == 0) {
				firstDate = guide.schedule;
			} else {
				if (dateTimeUtil.compareDates(guide.schedule, firstDate) > -1) {
					firstDate = guide.schedule;
				}
			}
			i++;
		}
		return dateTimeUtil.toString(firstDate, "JJhMM");
	}

	return "teste";
}

protected function onChangeInitialDate(event:CalendarLayoutChangeEvent):void {
	if (periodStart.selectedDate && periodEnd.selectedDate) {
		var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
		if (val > 0) {
			periodEnd.selectedDate = periodStart.selectedDate;
		} 
	}
}

protected function onChangeFinalDate(event:CalendarLayoutChangeEvent):void {
	if (periodStart.selectedDate && periodEnd.selectedDate) {
		var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
		if (val > 0 ) {
			periodStart.selectedDate = periodEnd.selectedDate;
		}
	}
}

protected function onFocusOutCombo(event:FocusEvent):void {
		if (event.currentTarget is ComboBox && event.target is RichEditableText) {
		var cb:ComboBox = event.currentTarget as ComboBox;
		var rich:RichEditableText = event.target as RichEditableText;
		rich.text = "";
		if (cb.selectedIndex < 0) {
			cb.selectedIndex = -1;
			cb.selectedItem = null;
		}
	}
}

private function startDragDrop(event:GridEvent):void {
	if (DragManager.isDragging || listArchivedGuidelines.selectedItem as Guideline == null)
		return;
	
	if (event.itemRenderer != null) {
		startDragDropGuideline(event);
	}
}

private function startDragDropGuideline(event:GridEvent):void {
	var sourceItens:Vector.<Object> = new Vector.<Object>();
	sourceItens.push(ObjectUtil.copy(listArchivedGuidelines.selectedItem) as Guideline)
	
	var dataSource:DragSource = new DragSource();
	dataSource.addData(sourceItens, "itemsByIndex");
	
	//Create the proxy (visual element that will be drag with cursor over the elements)
	var proxy:Group = new Group();
	proxy.width = listArchivedGuidelines.grid.width;
	
	for each (var columnIndex:int in listArchivedGuidelines.grid.getVisibleColumnIndices() as Vector.<int>) {
		var currentRenderer:IGridItemRenderer = listArchivedGuidelines.grid.getItemRendererAt(listArchivedGuidelines.selectedIndex, columnIndex);
		var factory:IFactory = listArchivedGuidelines.columns.getItemAt(columnIndex).itemRenderer;
		if (!factory)
			factory = listArchivedGuidelines.itemRenderer;
		var renderer:IGridItemRenderer = IGridItemRenderer(factory.newInstance());
		renderer.visible = true;
		renderer.column = currentRenderer.column;
		renderer.rowIndex = currentRenderer.rowIndex;
		renderer.label = currentRenderer.label;
		renderer.x = currentRenderer.x;
		renderer.y = currentRenderer.y;
		renderer.width = currentRenderer.width;
		renderer.height = currentRenderer.height;
		renderer.prepare(false);
		proxy.addElement(renderer);
		renderer.owner = listArchivedGuidelines;
	}
	proxy.height = renderer.height;
	
	//Start the drag.
	DragManager.doDrag(listArchivedGuidelines, dataSource, event, proxy as IFlexDisplayObject, 0, -listArchivedGuidelines.columnHeaderGroup.height);
}

private function loadProgramsWithFake():void {
	programsWithFake =  ObjectUtil.copy(DomainCache.programs) as ArrayCollection;
	programsWithFake.addItemAt(getProgramDrawer(), 0);
}

private function getProgramDrawer():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL" 
	if (programDrawer == null) {
		programDrawer = new Program();
		programDrawer.id = -1;
		programDrawer.name = bundle.getString("Bundle", "defaults.drawerProgram");
	}
	return programDrawer;
}

private function stateLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	var stateLabel:String;
	switch (guideline.state) {
		case DocumentState.COMPLETED:  {
			stateLabel = bundle.getString("Bundle", "document.states.complete")
			break;
		}
		case DocumentState.COVERING:  {
			stateLabel = bundle.getString("Bundle", "document.states.covering")
			break;
		}
		case DocumentState.PRODUCING:  {
			stateLabel = bundle.getString("Bundle", "document.states.producing")
			break;
		}
		case DocumentState.INTERRUPTED:  {
			stateLabel = bundle.getString("Bundle", "document.states.interrupt")
			break;
		}
	}
	return stateLabel;
}

private function pendencyLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	return guideline && guideline.checklist ? guideline.checklist.tasksAsStr : '';
}

//private function formatTextSearch(str:String):String {
//	str = str.replace('+', '');
//	str = str.replace('-', '');
//	str = str.replace('&', '');
//	str = str.replace('|', '');
//	str = str.replace('!', '');
//	str = str.replace('(', '');
//	str = str.replace(')', '');
//	str = str.replace('{', '');
//	str = str.replace('}', '');
//	str = str.replace('[', '');
//	str = str.replace(']', '');
//	str = str.replace('^', '');
//	str = str.replace('/"', '');
//	str = str.replace('~', '');
//	str = str.replace('*', '');
//	str = str.replace('?', '');
//	str = str.replace(':', '');
//	str = str.replace('\\', '');
//	return str;
//}

private function periodIsInvalid():Boolean {
	var start:Date = periodStart.selectedDate;
	var end:Date = periodEnd.selectedDate;
	return start && end && start.time > end.time;
}
