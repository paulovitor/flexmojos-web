package tv.snews.anews.view.chat {

	import flash.events.Event;

	/**
	 * Possíveis eventos das janelas do Chat
	 *
	 * @author Felipe Zap de Mello
	 * @since 1.0.0
	 */
	public class ChatWindowEvent extends Event {

		public static const MINIMIZE_ALL_WINDOWS:String = "minimizeAllWindowsEvent";
		public static const MINIMIZE_WINDOW:String = "minimizeWindowEvent";
		public static const MAXIMIZE_WINDOW:String = "maximizeWindowEvent";
		public static const AUTO_SCROLLER:String = "autoScrollerEvent";

		public function ChatWindowEvent(type:String) {
			super(type);
		}
	}
}
