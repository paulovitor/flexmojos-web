//------------------------------------------------------------------------------
//
//  SoftwareNews Soluções para Jornalismo http://www.snews.com.br 
//  Copyright (C) 2012 SoftwareNews Comércio e Serviços de Informática Ltda. 
// 
//	@author Felipe Zap de Mello
// 	@since 1.0.0
//------------------------------------------------------------------------------
package tv.snews.anews.view.chat {

	import mx.binding.utils.ChangeWatcher;
	import mx.core.FlexGlobals;
	import mx.events.PropertyChangeEvent;
	import mx.events.ResizeEvent;
	
	import spark.components.HGroup;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.events.ElementExistenceEvent;

	/*
	* if (programmer.name == 'Eliezer')
	*	Alert.show("Don't touch in my code! motherfucker _|_ ");
	*/
	public class ChatScroller extends SkinnableComponent {
		// Container onde são adiocionadas/removidas as Chat Windows
		[SkinPart]
		public var containerChatWindows:HGroup;

		// Referência do objeto Chat para manipular a 'chatWindows'
		[Bindable] public var chat:ChatBar;

		// Total de janelas ocultadas à esquerda 
		[Bindable] public var windowsOcultedLeft:uint = 0;
		// Total de janelas ocultadas à direita
		[Bindable] public var windowsOcultedRight:uint = 0;
		// Flag para mostrar/esconder o scroller
		[Bindable] public var showScroller:Boolean = false;

		// Contadores de mensagens para janelas ocultas
		[Bindable] public var totalMessagesLeft:int = 0;
		[Bindable] public var totalMessagesRight:int = 0;

		// Armazena o total de janelas visíveis no scroller
		[Bindable] public var MAX_NUMBER_WINDOWS_VISIBLE:uint;
		// Monitora se a variavel MAX_NUMBER_WINDOWS_VISIBLE teve seu valor alterado 
		private var watcherVariable:ChangeWatcher;

		// Variáveis utilizadas para calcular o total da área disponível para criação de uma nova Chat Window

		//Largura ocupada pelos botões do Menu e Chat
		private var btnChatWidth:int = 340;
		//Total de gaps entre botões do scroller e janelas
		private var totalGaps:int = 36;
		//Total da largura de cada botão do scroller
		private var totalButtonsWidth:int = 64;
		//Diferença da largura da chatwindow maximizada pra minimizada
		private var differenceWidthStates:int = 95;
		//Divido pela largura da janela minimizada 
		private var windowMinimizedWidth:int = 165;

		public function ChatScroller() {
			super();
			setStyle("skinClass", ChatScrollerSkin);
			recalcMaxWindowsVisible();
			FlexGlobals.topLevelApplication.addEventListener(ResizeEvent.RESIZE, onResizeWindow);
			watcherVariable = ChangeWatcher.watch(this, ["MAX_NUMBER_WINDOWS_VISIBLE"], onChangeValue);
		}

		// -----------------------------
		// Events 
		// -----------------------------		

		/**
		 * Handler para tratar a nova largura da Application.
		 */
		private function onResizeWindow(event:ResizeEvent):void {
			recalcMaxWindowsVisible();
		}

		/**
		 * Handler para recarregar o scroller caso
		 * a variável tenha mudado de valor
		 */
		private function onChangeValue(event:PropertyChangeEvent):void {
			if (showScroller) {
				invalidateScroller();
			} else {
				//Apresenta o scroller caso ocorra resize na janela
				if (MAX_NUMBER_WINDOWS_VISIBLE == getTotalVisibleWindows() - 1) {
					invalidateScroller();
					showScroller = true;
					updateCounterHiddenWindows();
				}
			}
		}

		// -----------------------------
		// Position Controller
		// -----------------------------

		/**
		 * Trata a visibilidade/posição da janela quando
		 * um elemento é adicionado ao scroller.
		 */
		public function onAddElement(event:ElementExistenceEvent):void {
			showOrHideScroller();
			hideWindow();
			updateCounterHiddenWindows();
		}

		/**
		 * Trata a visibilidade/posição da janela quando
		 * um elemento é removido ao scroller.
		 */
		public function onRemoveElement(event:ElementExistenceEvent):void {
			moveWindowHiddenOnRemove();
			showOrHideScroller();
			updateCounterHiddenWindows();
		}

		/**
		 * Move as janelas à esquerda.
		 */
		public function moveLeft():void {
			var leftWindow:ChatWindow = chat.chatWindows.getItemAt(getFirstIndexWindowVisible() - 1) as ChatWindow;
			leftWindow.isVisible = true;
			var rightWindow:ChatWindow = chat.chatWindows.getItemAt(getLastIndexWindowVisible()) as ChatWindow;
			rightWindow.isVisible = false;

			showOrHideScroller();
			updateCounterHiddenWindows();
			minimizeWindowsHidden();
		}

		/**
		 * Move as janelas à direita.
		 */
		public function moveRight():void {
			var leftWindow:ChatWindow = chat.chatWindows.getItemAt(getFirstIndexWindowVisible()) as ChatWindow;
			leftWindow.isVisible = false;
			var rightWindow:ChatWindow = chat.chatWindows.getItemAt(getLastIndexWindowVisible() + 1) as ChatWindow;
			rightWindow.isVisible = true;

			showOrHideScroller();
			updateCounterHiddenWindows();
			minimizeWindowsHidden();
		}

		/**
		 * Torna visível uma janela que estava oculta no lugar da janela removida.
		 */
		private function moveWindowHiddenOnRemove():void {
			if (showScroller) {
				if (windowsOcultedLeft > 0) {
					var leftWindow:ChatWindow = chat.chatWindows.getItemAt(getFirstIndexWindowVisible() - 1) as ChatWindow;
					leftWindow.isVisible = true;
				} else {
					if (windowsOcultedRight > 0) {
						var rightWindow:ChatWindow = chat.chatWindows.getItemAt(getLastIndexWindowVisible() + 1) as ChatWindow;
						rightWindow.isVisible = true;
					}
				}
			}
		}

		/**
		 * Torna visível uma janela que estava oculta
		 * quando clicada na lista de usuários.
		 */
		public function showHiddenWindow(chatWindow:ChatWindow):void {
			var auxChatWindow:ChatWindow = null;

			//Pego o índice da aba corrente
			var indexWindow:int = chat.chatWindows.getItemIndex(chatWindow);
			var oldFirstIndex:int = getFirstIndexWindowVisible();
			var oldLastIndex:int = getLastIndexWindowVisible();

			var counter:int = 0;
			var i:int = 0;
			var sideHidden:String = "";

			if (indexWindow < oldFirstIndex) {
				sideHidden = "left";
			} else {
				if (indexWindow > oldLastIndex) {
					sideHidden = "right";
				}
			}

			switch (sideHidden) {
				case "left":
					//Percorro do índice corrente até o primeiro índice visível,
					//setando 'true'
					for (i = indexWindow; i < oldFirstIndex; i++) {
						auxChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
						auxChatWindow.isVisible = true;
						//Enquanto percorro, gravo a quantidade de janelas que foram "desocultadas"
						counter++;
					}

					//Percorro do último índice visível (antigo) até quando o 'counter' for igual a 0.
					//Oculto as janelas
					for (i = counter; i > 0; i--) {
						auxChatWindow = chat.chatWindows.getItemAt(oldLastIndex) as ChatWindow;
						auxChatWindow.isVisible = false;
						oldLastIndex--;
					}
					break;
				case "right":
					//Para o lado direito é a mesma lógica, só que invertendo os 'lados'
					//das variáveis
					for (i = indexWindow; i > oldLastIndex; i--) {
						auxChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
						auxChatWindow.isVisible = true;
						counter++;
					}

					for (i = counter; i > 0; i--) {
						auxChatWindow = chat.chatWindows.getItemAt(oldFirstIndex) as ChatWindow;
						auxChatWindow.isVisible = false;
						oldFirstIndex++;
					}
					break;
			}

			updateCounterHiddenWindows();
		}

		/**
		 * Método responsável por "recarregar" o objeto scroller na tela
		 * de acordo com o número máximo de janelas visíveis.
		 */
		private function invalidateScroller():void {
			var totalVisibleWindows:int = getTotalVisibleWindows();

			if (!invalidateIsNeed(totalVisibleWindows))
				return;

			showOrHideScroller();

			var chatWindow:ChatWindow = null;
			var firstIndexVisible:int = getFirstIndexWindowVisible();

			if (MAX_NUMBER_WINDOWS_VISIBLE > totalVisibleWindows) {
				if (windowsOcultedLeft > 0) {
					chatWindow = chat.chatWindows.getItemAt(firstIndexVisible - 1) as ChatWindow;
					chatWindow.isVisible = true;
				}
			} else {
				chatWindow = chat.chatWindows.getItemAt(firstIndexVisible) as ChatWindow;
				chatWindow.isVisible = false;
			}

			updateCounterHiddenWindows();
			minimizeWindowsHidden();
			checkMaxWindowsVisibleVariable();
		}


		// -----------------------------
		// Utilities Methods
		// -----------------------------

		/**
		 * Método que verifica se existe diferença entre o total de
		 * janelas visíveis na tela e o máximo permitido. A diferença pode
		 * existir quando  o resize feito pelo usuário é executado rapidamente
		 * ao ponto de não dar tempo de recalcular a variável.
		 */
		private function checkMaxWindowsVisibleVariable():void {
			//Se existir diferença, o valor máximo de janelas visíveis 
			//será o valor atual de janelas visíveis na tela.
			if (MAX_NUMBER_WINDOWS_VISIBLE != getTotalVisibleWindows())
				MAX_NUMBER_WINDOWS_VISIBLE = getTotalVisibleWindows();
		}

		/**
		 * Método que verifica todas as situações que impedem
		 * a atualização do scroller.
		 * Se elas não existirem, retorna true.
		 */
		private function invalidateIsNeed(totalVisibleWindows:int = 2):Boolean {
			if (MAX_NUMBER_WINDOWS_VISIBLE == 2) {
				if (totalVisibleWindows == 2) {
					return false;
				}
			}

			if ((getFirstIndexWindowVisible() - 1) < 0 && showScroller == true && windowsOcultedLeft > 0)
				return false;

			if (MAX_NUMBER_WINDOWS_VISIBLE == totalVisibleWindows) {
				return false;
			}

			if (chat.chatWindows.length == 0) {
				return false;
			}

			return true;
		}

		/**
		 * Recalcula a quantidade máxima de janelas visíveis na tela.
		 */
		private function recalcMaxWindowsVisible():uint {
			//** FlexGlobals.topLevelApplication.width - Total da largura ocupada pela Application *//
			var auxValue:uint = Math.floor((FlexGlobals.topLevelApplication.width - btnChatWidth - totalGaps - totalButtonsWidth - differenceWidthStates) / windowMinimizedWidth);
			if (auxValue == 0 || auxValue == 1)
				auxValue = 2;
			MAX_NUMBER_WINDOWS_VISIBLE = auxValue;
			return MAX_NUMBER_WINDOWS_VISIBLE;
		}


		/**
		 * Controla a exibição do scroller
		 * baseado-se no número máximo de janelas visíveis
		 */
		private function showOrHideScroller():void {
			if (chat.chatWindows.length <= MAX_NUMBER_WINDOWS_VISIBLE) {
				showScroller = false;
				totalMessagesLeft = 0;
				totalMessagesRight = 0;
			} else {
				showScroller = true;
			}
		}

		/**
		 * Oculta uma nova aba no scroller.
		 */
		private function hideWindow():void {
			if (showScroller) {
				//Esconde a próxima janela 'isVisible=true' apartir do índice 0	
				for (var i:int = 0; i < chat.chatWindows.length; i++) {
					var chatWindow:ChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
					if (chatWindow.isVisible == true) {
						chatWindow.isVisible = false;
						break;
					}
				}
			}
		}

		/**
		 * Retorna o índice da primeira janela visível no scroller.
		 */
		public function getFirstIndexWindowVisible():int {
			for (var i:int = 0; i <= chat.chatWindows.length; i++) {
				var chatWindow:ChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
				if (chatWindow.isVisible == true) {
					return i;
				}
			}
			return 0;
		}

		/**
		 * Retorna o índice da última janela visível no scroller.
		 */
		public function getLastIndexWindowVisible():int {
			for (var i:int = chat.chatWindows.length - 1; i >= 0; i--) {
				var chatWindow:ChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
				if (chatWindow.isVisible == true) {
					return i;
				}
			}
			return 0;
		}

		/**
		 * Retorna o total de janelas visíveis no scroller.
		 */
		public function getTotalVisibleWindows():int {
			var total:int = 0;
			for each (var chatWindow:ChatWindow in chat.chatWindows) {
				if (chatWindow.isVisible == true) {
					total++;
				}
			}
			return total;
		}

		/**
		 * Atualiza os contadores de cada botão do scroller
		 * (Contador de janelas ocultas e mensagens das janelas).
		 */
		private function updateCounterHiddenWindows():void {
			if (showScroller) {
				var firstIndex:int = getFirstIndexWindowVisible();
				var lastIndex:int = getLastIndexWindowVisible();
				updateCounterLeftButton(firstIndex);
				updateCounterRightButton(lastIndex);
			}
		}

		private function updateCounterLeftButton(firstIndex:int):void {
			windowsOcultedLeft = 0;
			totalMessagesLeft = 0;
			for (var i:int = 0; i <= firstIndex; i++) {
				var chatWindow:ChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
				if (chatWindow.isVisible == false) {
					windowsOcultedLeft++;
					if (chatWindow.messageCounter > 0) {
						totalMessagesLeft += chatWindow.messageCounter;
					}
				}
			}
		}

		private function updateCounterRightButton(lastIndex:int):void {
			windowsOcultedRight = 0;
			totalMessagesRight = 0;
			for (var i:int = chat.chatWindows.length - 1; i >= lastIndex; i--) {
				var chatWindow:ChatWindow = chat.chatWindows.getItemAt(i) as ChatWindow;
				if (chatWindow.isVisible == false) {
					windowsOcultedRight++;
					if (chatWindow.messageCounter > 0) {
						totalMessagesRight += chatWindow.messageCounter;
					}
				}
			}
		}

		/**
		 * Minimiza todas as janelas que estão maximizadas e ocultadas.
		 */
		private function minimizeWindowsHidden():void {
			if (showScroller) {
				for each (var chatWindow:ChatWindow in chat.chatWindows) {
					if (chatWindow.state == "normal" && chatWindow.isVisible == false) {
						chatWindow.dispatchChangeStateWindowEvent(new ChatWindowEvent(ChatWindowEvent.MINIMIZE_WINDOW));
					}
				}
			}
		}

	}
}
