import flash.display.DisplayObject;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.ui.Keyboard;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.styles.CSSStyleDeclaration;
import mx.utils.ObjectUtil;

import spark.components.gridClasses.GridColumn;

import tv.snews.anews.component.Info;

import tv.snews.anews.domain.*;
import tv.snews.anews.event.StoryEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.preview.HelpPreview;
import tv.snews.anews.view.preview.PreviewConfiguration;
import tv.snews.flexlib.utils.DateUtil;

[Bindable] private var rundown:Rundown;
[Bindable] private var stories:ArrayCollection;
[Bindable] private var config:PreviewConfiguration = new PreviewConfiguration();

private static const dateUtil:DateUtil = new DateUtil();

private static const rundownService:RundownService = RundownService.getInstance();
private static const blockService:BlockService = BlockService.getInstance();
private static const storyService:StoryService = StoryService.getInstance();

private var configTimer:Timer;

//------------------------------------------------------------------------------
//	Interfacer Event Handlers
//------------------------------------------------------------------------------

//----------------------------------
//	currentStory
//----------------------------------

private var _currentStory:Story;

private function set currentStory(value:Story):void {
	_currentStory = value;
	changeState();
}

[Bindable]
private function get currentStory():Story {
	return _currentStory;
}

//----------------------------------
//	nextStory
//----------------------------------

private var _nextStory:Story;

private function set nextStory(value:Story):void {
	_nextStory = value;
	changeState();
}

[Bindable]
private function get nextStory():Story {
	return _nextStory;
}

//------------------------------------------------------------------------------
//	Interfacer Event Handlers
//------------------------------------------------------------------------------

/**
 * Handler para o evento "contentCreationComplete" da área. Faz os subscribes 
 * para as notificações de mundanças nos espelhos, blocos e laudas. 
 */
private function onContentCreationComplete(event:FlexEvent):void {
	rundownService.subscribe(onRundownMessageReceive);
	blockService.subscribe(onBlockMessageReceive);
	storyService.subscribe(onStoryMessageReceive);
    storiesGrid.addEventListener(StoryEvent.DISPLAYING, onStoryDisplay);
	
	activeConfigTimer();
}

/**
 * Handler chamando quando o campo de data ou o combobox de programa é alterado.
 */
private function onDateOrProgramChange():void {
	var date:Date = dateField.selectedDate;
	var program:Program = programsCombo.selectedItem as Program;
	
	if (date && program) {
		rundownService.loadRundown(date, program.id, true, onRundownLoad);
	}
}

/**
 * Handler chamado no evento de sucesso da chamada do método do serviço 
 * "loadRundown".
 */
private function onRundownLoad(event:ResultEvent):void {
	rundown = event.result as Rundown;
	stories = null;
	currentStory = null;
	nextStory = null;
	
	if (rundown) {
		stories = new ArrayCollection();
		
		refreshApprovedStories();
	}
}

private function onChangeStoriesGrid():void {
	defineCurrentStory();
}

private function onKeyDown(event:KeyboardEvent):void {
	if (event.ctrlKey && event.shiftKey) {
		// Aumenta a fonte [Ctrl + Shift + .]
		if (event.keyCode == Keyboard.PERIOD) {
			config.incGrid();
		}
		// Reduz a fonte [Ctrl + Shift + ,]
		if (event.keyCode == Keyboard.COMMA) {
			config.decGrid();
		}
		if (event.keyCode == Keyboard.BACKSLASH) {
			config.reset();
		}
	} else if (event.shiftKey) {
		// Aumenta a fonte [Shift + .]
		var currentValue:int;
		if (event.keyCode == Keyboard.PERIOD) {
			currentValue = dividedPreviews.getStyle("fontSize") + 3;
			if (currentValue < PreviewConfiguration.STORY_MAX_FONT_SIZE) {
				dividedPreviews.setStyle("fontSize", currentValue);
				changeStyle(true);
			}
		}
		// Reduz a fonte [Shift + ,]
		if (event.keyCode == Keyboard.COMMA) {
			currentValue = dividedPreviews.getStyle("fontSize") - 3;
			if (currentValue > PreviewConfiguration.STORY_MIN_FONT_SIZE) {
				dividedPreviews.setStyle("fontSize", currentValue);
				changeStyle(false);
			}
		}
	}
}

private function onOpenHelp(event:MouseEvent):void {
	var help:HelpPreview = new HelpPreview();
	PopUpManager.addPopUp(help, DisplayObject(FlexGlobals.topLevelApplication), true);
	PopUpManager.centerPopUp(help);
	help.addEventListener(CloseEvent.CLOSE, onCloseHelp);
}

private function onCloseHelp(event:CloseEvent):void{
	PopUpManager.removePopUp(event.target as HelpPreview);
}

//------------------------------------------------------------------------------
//	Message Event Handlers
//------------------------------------------------------------------------------

/**
 * Handler para a notificação de mensagens para mudanças nos espelhos. Atualiza 
 * os atributos do espelho quando alterado ou renumera as páginas das laudas 
 * exibidas.
 */
private function onRundownMessageReceive(event:MessageEvent):void {
	if (rundown) {
		var action:String = event.message.headers["action"];

		var rundownUpdated:Rundown;

		switch (action) {
			case EntityAction.INSERTED:
			case EntityAction.UPDATED:
			case EntityAction.DELETED:
				// Nada a fazer
				break;
			
			case RundownAction.SORTED:
				var _rundown:Rundown = Rundown(event.message.body);
				if (rundown.equals(_rundown)) {
					sortPages();
				}
				break;
			
			case EntityAction.FIELD_UPDATED:
				var headers:Object = event.message.headers;
				if (rundown.id == headers.rundownId) {
					rundown[headers.field] = headers.value;
				}
				break;
            case RundownAction.DISPLAY_RESETS:
							rundownUpdated = event.message.body as Rundown;
                if (rundown.id == rundownUpdated.id) {
                    rundown.displayed = false;
                    for each(var story:Story in stories){
                        story.displayed = false;
                        story.displaying = false;
                    }
                }
                break;
            case RundownAction.COMPLETE_DISPLAY:
							rundownUpdated = event.message.body as Rundown;
                if (rundown.id == rundownUpdated.id) {
                    rundown.displayed = true;
                }
                break;
		}
	}
}

/**
 * Handler para a notificação de mensagens para as mundanças nos blocos. Trata a
 * adição, atualização ou remoção de um bloco.
 */
private function onBlockMessageReceive(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var block:Block = event.message.body as Block;
	
	if (rundown && rundown.equals(block.rundown)) {
		switch (action) {
			case EntityAction.INSERTED:
				addBlock(block);
				break;
			
			case EntityAction.UPDATED:
				updateBlock(block);
				break;
			
			case EntityAction.DELETED:
				removeBlock(block);
				break;
		}
		
		defineCurrentStory();
		
//		traceRundown();
//		traceStories();
	}
}

/**
 * Handler para a notificação de mensagens para as mundanças nas laudas. Trata 
 * todas as alterações possíveis que uma lauda pode sofrer.
 */
private function onStoryMessageReceive(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var story:Story = event.message.body as Story;
	var multiple:Boolean = event.message.headers["multiple"] as Boolean;
	var storiesFromJava:ArrayCollection = event.message.body as ArrayCollection;
	var rundownId:Number = Number(event.message.headers["rundownId"]);
	var localStory:Story = null;
	
	/*
	 * Situações em que irá entrar no switch:
	 * 	- tem espelho aberto (rundown != null)
	 * 	- a story é nula (mensagem de atualizar campo)
	 * 	- a story não tem block (exclusão)
	 * 	- o espelho da story é o aberto (ids iguais)
	 */

	if (rundown != null && rundownId == rundown.id) {
		
		switch (action) {
			case EntityAction.FIELD_UPDATED:
				var id:Number = event.message.headers["storyId"];
				var field:String = event.message.headers["field"];
				var value:Object = event.message.headers["value"];
				
				updateStoryField(id, field, value);
				
				if (field == "approved") {
					story = findStoryById(id);
					if (story) {
						if (Boolean(value)) { // aprovada aparece no preview
							displayStory(story);
						} else {
							hideStory(story); // desaprovada sai do preview
						}
					}
				}
				break;
			case EntityAction.INSERTED:
			case EntityAction.RESTORED:
				addStory(story);
				break;
			
			case EntityAction.UPDATED:
				updateStory(story);
				break;
			
			case EntityAction.MOVED:
			if (multiple) {
				// Deve remover todas as laudas primeiro para depois inserir. Isso evita que sejam inseridas em posições erradas.
				
				for each (var curr:Story in storiesFromJava) {
					rundown.removeStory(curr);
				}
				for each (curr in storiesFromJava) {
					rundown.addStory(curr);
				}
				
				for each (var block:Block in rundown.blocks) {
					block.reorder();
					block.updateTimers();
				}
				rundown.updateTimers();
				rundown.reorderGlobalIndexes();
				
				refreshApprovedStories();
			} else {
				localStory = findStoryById(story.id);
				if (localStory) {
					var blockId:Number = story.block.id;
					var newIndex:int = story.order;
					
					moveStory(blockId, newIndex, localStory);
				}
			}
			break;
			
			case EntityAction.DELETED:
			case RundownAction.DRAWER:
				removeStory(story);
				rundown.updateTimers();
			break;
			case EntityAction.MULTIPLE_EXCLUSION:  {
				var affectedStories:Array = event.message.body as Array;
                rundown.removeStories(affectedStories);
				refreshApprovedStories();
			break;
			}
			
		}

		defineCurrentStory();
	}
}

private function onStoryDisplay(event:StoryEvent):void {
    var story:Story = ObjectUtil.copy(event.story) as Story;

    storyService.updateField(story.id, 'displaying', true,
            function (event:ResultEvent):void {
//                radioButton.enabled = false;
            },
            function (event:FaultEvent):void {
//                radioButton.enabled = false;
            }
    );
}

//------------------------------------------------------------------------------
//	Label Functions
//------------------------------------------------------------------------------

private function smallHourLabel(item:Object, column:GridColumn):String {
	var time:Date = item[column.dataField];
	return dateUtil.minuteSecondToString(time);
}

private function fullHourLabel(item:Object, column:GridColumn):String {
	var time:Date = item[column.dataField];
	return dateUtil.timeToString(time);
}

//------------------------------------------------------------------------------
//	Helper Methods
//------------------------------------------------------------------------------
private function refreshApprovedStories():void{
	if (stories) {
		stories.removeAll();
		for each (var block:Block in rundown.blocks) {
			for each (var story:Story in block.stories) {
				if (story.approved) {
					stories.addItem(story);
				}
			}
		}
	}
}

private function getBlockById(blockId:int):Block {
	if (rundown) {
		for each (var block:Block in rundown.blocks) {
			if (block.id == blockId) {
				return block;
			}
		}
	}
	return null;
}

/**
 * Em intervalos de 2 segundos salva as configurações, se foram alteradas.
 */
private function activeConfigTimer():void {
	configTimer = new Timer(2000);
	configTimer.addEventListener(TimerEvent.TIMER, 
		function(event:TimerEvent):void {
			if (storiesGrid && currentViewer) {
				config.gridWidth = storiesGrid.width;
				config.nextViewerHeight = nextViewer ? nextViewer.height : config.nextViewerHeight;
			}
			if (config.changed) {
				config.save();
			}
		}
	);
	configTimer.start();
}

private function changeState():void {
	this.currentState = nextStory ? "normal" : currentStory ? "single" : "none";
}

/**
 * Armazena as referências para a lauda selecionada e a lauda posterior a ela.
 */
private function defineCurrentStory():void {
	if (storiesGrid.selectedIndex != -1) {
		currentStory = storiesGrid.selectedItem as Story;
		nextStory = findNextStory(currentStory);
	} else {
		if (currentStory) {
			// Tenta re-selecionar a que estava selecionada
			for (var index:int = 0; index < stories.length; index++) {
				var aux:Story = Story(stories.getItemAt(index));
				if (aux.equals(currentStory)) {
					storiesGrid.selectedIndex = index;
					defineCurrentStory(); // agora com uma seleção na grid
					return;
				}
			}
		}
		currentStory = null;
		nextStory = null;
	}
}

/**
 * Procura e retorna a lauda que possui o ID informado.
 */
private function findStoryById(id:Number):Story {
	for each (var block:Block in rundown.blocks) {
		for each (var current:Story in block.stories) {
			if (current.id == id) {
				return current;
			}
		}
	}
	return null;
}

/**
 * Procura a retorna a lauda que se encontra posicionada após a lauda informada.
 */
private function findNextStory(story:Story):Story {
	var found:Boolean = false;
	for each (var current:Story in stories) {
		if (found) {
			return current;
		} else {
			found = current.equals(story);
		}
	}
	return null;
}

/**
 * Retorna o objeto do bloco que possui o ID informado. Utilizado para obter a 
 * referência ao objeto que está no objeto "rundown".
 */
private function findBlockById(id:Number):Block {
	for each (var block:Block in rundown.blocks) {
		if (block.id == id) {
			return block;
		}
	}
	return null;
}

/**
 * Cálcula e retorna o index global de uma lauda dentre as laudas aprovadas do 
 * espelho.
 */
private function calculateGlobalIndex(target:Story):int {
	var count:int = -1;
	for each (var block:Block in rundown.blocks) {
		for each (var story:Story in block.stories) {
			if (story.approved) {
				count++;
			}
			if (story.equals(target)) {
				return count;
			}
		}
	}
	return count;
}

/**
 * Aplica os estilos alterando diretamente seus seletores
 */
private function changeStyle(isIncreasing:Boolean=true):void {
	for each (var myStyle:String in ['.font13', '.font15']) {
		var element:CSSStyleDeclaration = CSSStyleDeclaration(this.styleManager.getStyleDeclaration(myStyle));
		if(element) {
			var currentValue:* = element.getStyle("fontSize");
			currentValue = 	isIncreasing ? currentValue+3 : currentValue-3;
			element.setStyle("fontSize",  currentValue);
		}
	}
}

/**
 * Renúmera as páginas de todas as laudas do espelho baseando-se na ordem atual.
 */
private function sortPages():void {
	var count:int = 0;
	for each (var block:Block in rundown.blocks) {
		for each (var story:Story in block.stories) {
			story.page = (++count < 10 ? "0" : "") + count; 
		}
	}
}

private function storyKindLabelFunction(item:Object, column:GridColumn):String {
	return item && item.storyKind ? item.storyKind.acronym : "";
}

//--------------------------------------
//	Blocks Changes Methods
//--------------------------------------

/**
 * Adiciona um novo bloco a lista de blocos do espelho aberto.
 */
private function addBlock(block:Block):void {
	if (block.order < 0 || block.order > rundown.blocks.length - 1) {
		throw new Error("ArrayOutBoundException: The block has a order out of bound of the mirror.");
	}
	
	block.rundown = rundown;
	rundown.blocks.addItemAt(block, block.order); // TODO funciona?
	rundown.reorder();
}

/**
 * Faz a atualização dos dados de um bloco.
 */
private function updateBlock(block:Block):void {
	for each (var current:Block in rundown.blocks) {
		if (current.equals(block)) {
			current.commercial = block.commercial;
			break;
		}
	}
}

/**
 * Efetua a remoção de um bloco do espelho que está sendo exibido.
 */
private function removeBlock(block:Block):void {
	rundown.blocks.removeItemAt(block.order);
	rundown.reorder();
	
	for (var i:int = stories.length - 1; i >= 0; i--) {
		var aux:Story = Story(stories.getItemAt(i));
		if (aux.block.equals(block)) {
			stories.removeItemAt(i);
		}
	}
}

//--------------------------------------
//	Stories Changes Methods
//--------------------------------------

/**
 * Adiciona uma lauda que foi aprovada à lista de laudas do espelho exibido.
 */
private function addStory(story:Story):void {
	for each (var block:Block in rundown.blocks) {
		if (block.equals(story.block)) {
			if (story.order < 0 || story.order > block.stories.length) {
				throw new Error("ArrayOutBoundException: The story has a order out of bound of the block.");
			}
			block.stories.addItemAt(story, story.order);
			block.reorder();
		}
	}
}

/**
 * Faz a atualização dos dados de uma lauda que está sendo exibida.
 */
private function updateStory(story:Story):void {
	for each (var block:Block in rundown.blocks) {
		for each (var current:Story in block.stories) {
			if (current.equals(story)) {
				current.updateFields(story);
				return;
			}
		}
	}
}

/**
 * Atualiza o valor de um campo alterado de uma lauda.
 */
private function updateStoryField(id:Number, field:String, value:Object):void {
	for each (var block:Block in rundown.blocks) {
		for each (var story:Story in block.stories) {
			if (story.id == id) {
                // O nome do campo que chegará para o imageEditor é diferente do nome real do campo, devendo ser tratado
                if(field.toLowerCase() == 'image_editor')
                	story.imageEditor = value as User;
                else{
                    story[field.toLowerCase()] = value;
                    if(field.toLowerCase() == 'displayed')
                        story.displaying = false;
                }
				// Já atualiza na coleção "stories"
				break;
			}
		}
	}
}

/**
 * Move a list of story to a new location on the same block or in different block
 * @param story
 */
public function moveStory(newBlockId:Number, newIndex:int, oldStory:Story):void {
	//Get the current block used on the rundown. He has a differente UID,
	//so is prefered to work with the local copy of the block
	var localBlock:Block = getBlockById(newBlockId);
	
	if (localBlock == null) {
		throw new Error("IllegalArgumentException: The story has a undefined block so it couldn't be inserted.");
		return;
	}
	
	//If the user is moving itens on the same block, we should check if He's moving down on the list.
	//If the user is moving down, the dropIndex will decriased.
	//if (story.block.id == blockId) {
	//	if (story.order < newIndex) {
	//		newIndex--;
	//	}
	//}
	
	var oldBlock:Block = oldStory.block;
	
	//remove it from its old block
	oldBlock.stories.removeItemAt(oldBlock.stories.getItemIndex(oldStory));
	oldStory.block = null;
	
	//add it on its new block
	localBlock.stories.addItemAt(oldStory, newIndex);
	oldStory.block = localBlock;
	
	oldBlock.reorder(); // Organize indexes of the old block.
	oldBlock.updateTimers(); // Organize timers of the old block.
	
	if (oldBlock.id != localBlock.id) {
		localBlock.reorder(); // Organize indexes of the block.
		localBlock.updateTimers(); // Organize timers of block.
	}
	
	rundown.updateTimers(); //Organize timers of rundown.
	rundown.reorderGlobalIndexes(); //Organize global indexes
}

/**
 * Remove uma lauda da lista de laudas do espelho atual.
 */
private function removeStory(story:Story):void {
	for each (var block:Block in rundown.blocks) {
		for each (var current:Story in block.stories) {
			if (current.equals(story)) {
				var index:int = block.stories.getItemIndex(current);
				block.stories.removeItemAt(index);
				
				block.reorder();
				break;
			}
		}
	}
	
	hideStory(story);
}

private function displayStory(story:Story):void {
	stories.addItemAt(story, calculateGlobalIndex(story));
}

private function hideStory(story:Story):void {
	for (var i:int = 0; i < stories.length; i++) {
		var aux:Story = Story(stories.getItemAt(i));
		if (aux.equals(story)) {
			stories.removeItemAt(i);
			break;
		}
	}
}

private function onCompleteDisplay():void {
    if (rundown != null) {
        Alert.show(resourceManager.getString('Bundle', 'rundown.msg.storyCompleteDisplay'), resourceManager.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onCompleteDisplayConfirm);
    }
}

private function onCompleteDisplayConfirm(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        rundownService.completeStoriesDisplay(rundown.id, function (event:Event):void {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "rundown.completeDisplaySuccess"), Info.INFO);
        });
    }
}

private function onResetDisplay():void {
    if (rundown != null) {
        Alert.show(resourceManager.getString('Bundle', 'rundown.msg.storyDisplayReset'), resourceManager.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onResetDisplayConfirm);
    }
}

private function onResetDisplayConfirm(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        rundownService.resetStoriesDisplay(rundown.id, function (event:Event):void {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "rundown.displayResetsSuccess"), Info.INFO);
        });

    }
}

//------------------------------------------------------------------------------
//	Debug Methods
//------------------------------------------------------------------------------

private function traceRundown():void {
	trace('__________________________________________________________________');
	for each (var block:Block in rundown.blocks) {
		trace('[ order:', block.order, ', count:', block.stories.length, ', name:', block.name, ']');
		for each (var story:Story in block.stories) {
			trace(' |__ [ page:', story.page, ', order:', story.order, ', approved:', (story.approved ? 'Y' : 'N'), ', slug:', story.slug, ']');
		}
	}
}

private function traceStories():void {
	trace('__________________________________________________________________');
	var lastBlock:Block = null;
	for each (var story:Story in stories) {
		if (!lastBlock || !lastBlock.equals(story.block)) {
			lastBlock = story.block;
			trace('[ order:', lastBlock.order, ', count:', lastBlock.stories.length, ', name:', lastBlock.name, ']');
		}
		trace(' |__ [ page:', story.page, ', order:', story.order, ', approved:', (story.approved ? 'Y' : 'N'), ', slug:', story.slug, ']');
	}
}
