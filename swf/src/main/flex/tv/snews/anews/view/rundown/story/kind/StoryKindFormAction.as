import flash.display.DisplayObjectContainer;

import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import spark.components.DataGroup;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.StoryKind;
import tv.snews.anews.service.StoryKindService;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.rundown.story.kind.StoryKindArea;

[Bindable] public var storyKind:StoryKind = new StoryKind();

private const storyKindService:StoryKindService = StoryKindService.getInstance();

// ----------------------
// Actions
// ----------------------

private function save():void {
	if ((storyKind.id == 0 && ANews.USER.allow("021202")) || (storyKind.id != 0 && ANews.USER.allow("021203"))) {
		if (!Util.isValid(validatorsKindForm)) {
			ANews.showInfo(this.resourceManager.getString('Bundle', "defaults.msg.requiredFields"), Info.WARNING);
			txtStoryKindName.setFocus();
		} else {
			storyKindService.save(storyKind, onStoryKindSuccess);
			var container:DisplayObjectContainer = this.parent;
			while (container != null && !(container is StoryKindArea)) {
				container = container.parent;
			}

			if (container == null)
				return;

			var kindArea:StoryKindArea = container as StoryKindArea;
			var dataGroup:DataGroup = kindArea.dataGroupStoryKinds.dataGroup;
			dataGroup.enabled = true;
		}
	}
}

private function cancelEdition():void {
	storyKind = new StoryKind();
	txtStoryKindName.errorString = "";
	txtStoryKindAcronym.errorString = "";
	var container:DisplayObjectContainer = this.parent;
	while (container != null && !(container is StoryKindArea)) {
		container = container.parent;
	}

	if (container == null)
		return;

	var storyKindArea:StoryKindArea = container as StoryKindArea;
	var dataGroup:DataGroup = storyKindArea.dataGroupStoryKinds.dataGroup;
	dataGroup.enabled = true;
	this.currentState = "default";
}

// ----------------------
// Asynchronous Answer
// ----------------------
private function onStoryKindSuccess(event:ResultEvent):void {
	ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "defaults.msg.saveSuccess"), Info.INFO);
	cancelEdition();
}