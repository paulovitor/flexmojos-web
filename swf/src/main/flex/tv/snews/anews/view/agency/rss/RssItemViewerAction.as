
import flash.display.DisplayObjectContainer;
import flash.events.MouseEvent;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import flashx.textLayout.conversion.TextConverter;
import flashx.textLayout.elements.TextFlow;

import mx.containers.TabNavigator;

import spark.components.Label;

import tv.snews.anews.domain.News;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.utils.*;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const extractInvalidHtmlTags:RegExp = /(<\/?)(?i:(?<element>(abbr|acronym|address|applet|area)?|(base(font)?|bdo|big|blockquote|body|button)?|c(aption|enter|ite|(o(de|l(group)?)))|d(d|el|fn|i(r|v)|l|t)|em|f(ieldset|orm|rame(set)?)|h([1-6]|ead|r|tml)|(iframe|in(put|s)|sindex)?|kbd|l(abel|egend|ink)|m(ap|e(nu|ta))|no(frames|script)|o(bject|l|pt(group|ion))|(param|pre)?|q|s(amp|cript|elect|mall|pan|t(r(ike|ong)|yle)|u(b|p))|t(able|body|d|extarea|foot|h|itle|r|t)|(ul)?|var))(\s(?<attr>.+?))*>/gi;

private const dtUtil:DateUtil = new DateUtil();

//--------------------------------------
//	Properties
//--------------------------------------

private var _item:RssItem;

//--------------------------------------
//	Methods
//--------------------------------------

private function buildOrigin(item:RssItem):String {
	if (StringUtils.isNullOrEmpty(item.authors)) {
		return item.rssFeed.name;
	} else {
		return item.authors + " (" + item.rssFeed.name + ")";
	}
}

private function publishRss():void {
	var news:News = new News();
	news.slug = _item.title.length > 40 ? _item.title.substring(0, 37) + '...' : _item.title;
	news.font = _item.rssFeed.name.length > 254 ? _item.rssFeed.name.substring(0, 251) + '...' : _item.rssFeed.name;
	news.information = _item.content.length == 0 ? StringUtils.stripHtmlTags(_item.description) : StringUtils.stripHtmlTags(_item.content);
	news.address = _item.link.length > 254 ? _item.link.substring(0, 251) + '...' : _item.link;
	var container:DisplayObjectContainer = this.parent;
	while (container != null && !(container is TabNavigator)) {
		container = container.parent;
	}

	if (container == null)
		return;

	var tabBar:TabNavigator = container as TabNavigator;

	tabBar.dispatchEvent(new CrudEvent(CrudEvent.CREATE, news));
}

private function formatTimestamp(timestamp:Date):String {
	return dtUtil.toString(timestamp, resourceManager.getString('Bundle', "defaults.prettyDateFormat"));
}

private function stripHtml(str:String):String {
	return StringUtils.stripHtmlTags(str);
}

private function openLink(link:String):void {
	navigateToURL(new URLRequest(link), "_blank");
}

private function getCorrectText(item:RssItem):TextFlow {
	var aux:String;
	if (StringUtils.isNullOrEmpty(item.content)) {
		aux = stripNotAllowedHtmlTags(item.description);
	} else {
		aux = stripNotAllowedHtmlTags(item.content);
	}
	aux = aux.replace("<a ", "<a target=\"_blank\" "); // faz os links abrirem em uma nova janela
//	aux = Util.escapeCharacters(aux);
	aux = StringUtils.convertEntities(aux);
	return TextConverter.importToFlow(aux, TextConverter.TEXT_FIELD_HTML_FORMAT);
}

private function stripNotAllowedHtmlTags(str:String):String {
	var aux:String = str.replace(extractInvalidHtmlTags, "");
	return StringUtils.trim(aux);
}

private function changeStyle(event:MouseEvent):void {
	var label:Label = event.target as Label;
	var style:String = event.type == MouseEvent.MOUSE_OVER ? "underline" : "none";
	label.setStyle("textDecoration", style);
}

//--------------------------------------
//	Getter's and Setter's
//--------------------------------------
[Bindable] public function get item():RssItem {
	return _item;
}

public function set item(value:RssItem):void {
	_item = value;
	this.currentState = value == null ? "none" : "display";
}
