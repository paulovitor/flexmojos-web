import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Point;
import flash.utils.Timer;

import mx.core.FlexGlobals;
import mx.events.DragEvent;
import mx.events.FlexEvent;

import spark.components.List;

import tv.snews.anews.domain.News;
import tv.snews.anews.view.agency.newsviewer.NewsViewer;
import tv.snews.anews.view.agency.newsviewer.NewsViewerTooltip;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

[Bindable] public var newsViewArea:NewsViewer;

[Bindable] private var news:News;
private var currentToolTipNews:NewsViewerTooltip = null;
private const dtUtil:DateUtil = new DateUtil();
private var isRunning:Boolean = false;
private var isRunning2:Boolean = false;
private var toolTipIsOpen:Boolean = false;
private var tooltipIsFocused:Boolean = false;
private var itemRendererFocused:Boolean = false;
private var timer:Timer;
private var timer2:Timer;
private var app:Object = FlexGlobals.topLevelApplication;
private var mouseDown:Boolean = false;

override public function set data(value:Object):void {
	super.data = value;
	news = value as News;
}

protected function initApp(event:FlexEvent):void {
	newsViewArea = (this.owner as List).parentDocument as NewsViewer;

	newsViewArea.listNewsChat.addEventListener(DragEvent.DRAG_COMPLETE, onStopDrag);
	newsViewArea.listNewsChat.addEventListener(DragEvent.DRAG_START, onStartDrag);
}

private function onStopDrag(event:Event):void {
	newsViewArea.isDraggin = false;

}

private function onStartDrag(event:Event):void {
	newsViewArea.isDraggin = true;
}

//----------------------
// Formatters.
//----------------------
private function formatTime(timestamp:Date):String {
	return dtUtil.toString(timestamp, this.resourceManager.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}

private function formatDate(timestamp:Date):String {
	return dtUtil.toString(timestamp, this.resourceManager.getString('Bundle', "defaults.dateFormat"));
}

private function stripHtml(str:String):String {
	return StringUtils.stripHtmlTags(str);
}

//----------------------
// Events.
//----------------------

protected function createToolTipEvent(event:MouseEvent):void {
	if (!newsViewArea.isDraggin) { // nao criar a tooltip quando estiver arrastando.
		event.stopPropagation();
		event.preventDefault();
		itemRendererFocused = true;

		if (!toolTipIsOpen) {
			if (!isRunning) {
				isRunning = true;
				timer = new Timer(400);
				timer.addEventListener(TimerEvent.TIMER, onTimer);
				timer.start();
			}
		}

	} else {
		destroyTooltipFromLabel(event);
	}
}

private function onTimer(evt:TimerEvent):void {
	if (isRunning) {
		createToolTip();
	}
	timer.stop();
	isRunning = false;
}

private function onMouseOutTooltip(event:MouseEvent):void {
	tooltipIsFocused = false;

	if (mouseDown) {
		isRunning2 = true;
		timer2 = new Timer(1000);
		timer2.addEventListener(TimerEvent.TIMER, checkDestroyTooltip);
		timer2.start();
	} else {
		destroyToolTip();
	}
}

private function onMouseOverTooltip(event:MouseEvent):void {
	tooltipIsFocused = true;
}

private function checkDestroyTooltip(event:TimerEvent):void {
	if (toolTipIsOpen && !tooltipIsFocused) {
		destroyToolTip();
	}
	timer2.stop();
	isRunning2 = false;
}

//----------------------
// Hellpers.
//----------------------
private function createToolTip():void {

	currentToolTipNews = new NewsViewerTooltip();
	currentToolTipNews.news = this.news;
	currentToolTipNews.toolTipContent.addEventListener(MouseEvent.ROLL_OUT, onMouseOutTooltip);
	currentToolTipNews.toolTipContent.addEventListener(MouseEvent.ROLL_OVER, onMouseOverTooltip);
	currentToolTipNews.toolTipContent.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownTooltip);
	currentToolTipNews.toolTipContent.addEventListener(MouseEvent.MOUSE_UP, onMouseUpTooltip);

	app.main.addElement(currentToolTipNews);
	fadeIn.play([currentToolTipNews]);
	toolTipIsOpen = true;
	var newPosition:Point = this.localToGlobal(new Point(content.x, content.y));
	currentToolTipNews.x = newPosition.x - currentToolTipNews.width - 10;
	currentToolTipNews.arrowToolTip.top = newPosition.y + 10;
	currentToolTipNews.toolTipContent.y = calcPositionTooltip(newPosition.y);
}

private function onMouseDownTooltip(event:MouseEvent):void {
	mouseDown = true;
}

private function onMouseUpTooltip(event:MouseEvent):void {
	mouseDown = false;
}

private function calcPositionTooltip(y:Number):int {
	var metade:int = app.height / 2;
	if (y <= metade) {
		return 60;
	} else {
		return app.height - metade - 15;
	}
}

private function destroyToolTip():void {
	if (isRunning) {
		isRunning = false;
	}

	if (isRunning2) {
		isRunning2 = false;
	}

	if (toolTipIsOpen == true && currentToolTipNews != null) {
		var indexs:int = app.main.getElementIndex(currentToolTipNews);
		app.main.removeElementAt(indexs);
		currentToolTipNews = null;
		toolTipIsOpen = false;
		tooltipIsFocused = false;
	}
}

private function destroyTooltipFromLabel(event:MouseEvent = null):void {
	event.stopPropagation();
	event.preventDefault();


	if (isRunning) {
		isRunning = false;
	}

	if (!isRunning2) {
		isRunning2 = true;
		timer2 = new Timer(200);
		timer2.addEventListener(TimerEvent.TIMER, checkDestroyTooltip);
		timer2.start();
	}
}
