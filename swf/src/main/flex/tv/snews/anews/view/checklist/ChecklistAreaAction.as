import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import flexlib.controls.tabBarClasses.SuperTab;
import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.controls.Alert;
import mx.core.IUIComponent;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.messaging.events.MessageEvent;
import mx.messaging.messages.IMessage;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.collections.Sort;
import spark.collections.SortField;
import spark.components.gridClasses.GridColumn;
import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistTask;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Guide;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.ChecklistService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.checklist.ChecklistForm;
import tv.snews.flexlib.utils.DateUtil;

[Bindable]
private var checklists:ArrayCollection = new ArrayCollection();

[Bindable]
public var tabManager:TabManager = new TabManager();

private const dtUtil:DateUtil = new DateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();
private const checklistService:ChecklistService = ChecklistService.getInstance();
private const bundle:IResourceManager = ResourceManager.getInstance();

[Bindable]
private var isOpened:Boolean = false;

private var _selectedChecklist:Checklist = null;

[Bindable]
public function get selectedChecklist():Checklist {
	return _selectedChecklist;
}

public function set selectedChecklist(value:Checklist):void {
	_selectedChecklist = value;
	this.currentState = value ? "selected" : "normal";
}

private function onCreationComplete(event:FlexEvent):void {
	// Código banal para carregar coleções lazy do DomainCache
	DomainCache.checklistResources == DomainCache.checklistTypes == DomainCache.checklistProducers;

	this.addEventListener(CrudEvent.CREATE, onCreateAndEditEntity);
	this.addEventListener(CrudEvent.EDIT, onEditEntity);

	mainNavigatorChecklist.setClosePolicyForTab(0, SuperTab.CLOSE_NEVER);

	checklistService.subscribe(onChecklistMessageReceived);
	listAllOfDay();

	tabManager.tabNavigator = mainNavigatorChecklist;
}

private function onFormTabClose(event:SuperTabEvent):void {
	var form:ChecklistForm = mainNavigatorChecklist.getChildAt(event.tabIndex) as ChecklistForm;

	// Se estiver fechando um form, cancela o evento e chama a função que verifica se existem dados não salvos
	if (form) {
		event.preventDefault();
		form.close();
	}
}

/**
 * Handler para as mensagens enviadas pelo servidor para notificar ações sobre
 * as produções feitas pelos usuários, como operações de CRUD e bloqueio de edição.
 */
private function onChecklistMessageReceived(event:MessageEvent):void {
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var checklist:Checklist = message.body as Checklist, current:Checklist;
	var listAllChecklist:Boolean = !(checklist.done && this.checkBoxNotDone.selected);

	switch (action) {
        case EntityAction.INSERTED:
                //Quando uma pauta é incluída ou copiada no formulário das pautas, é disparado o evento, mas, caso exista uma produção para aquela pauta, ele não deve
                //ser inserido na grid se tiver com o excluded = true. Pois quando a pauta é copiada, é disparado o evento de inclusão e a produção deve ser tratada
			if (isCurrentDate(checklist) && !checklist.excluded && listAllChecklist) {
				checklists.addItem(checklist);
			}
			break;

		case EntityAction.UPDATED:
			for each (current in checklists) {
				if (current.equals(checklist)) {
					break;
				}
				current = null; // garante que seja null caso não encontre
			}

			if (current == null) {
				if (isCurrentDate(checklist) && !checklist.excluded && listAllChecklist) {
					checklists.addItem(checklist);
				}
			} else {
				if (isCurrentDate(checklist) && listAllChecklist) {
					current.updateFields(checklist);
				} else {
					checklists.removeItemAt(checklists.getItemIndex(current));
				}
			}

			break;
		case EntityAction.DELETED:
			for (var i:int = 0; i < checklists.length; i++) {
				current = Checklist(checklists.getItemAt(i));
				if (current.equals(checklist)) {
					checklists.removeItemAt(i);
					break;
				}
			}

			break;
		case EntityAction.RESTORED:
			var found:Boolean = false;
			for each (var aux:Checklist in checklists) {
				if (aux.equals(checklist)) {
					found = true;
					break;
				}
			}
			
			if (!found && isCurrentDate(checklist) && listAllChecklist) {
				checklists.addItem(checklist);
			}
			break;

		case EntityAction.LOCKED:
			for each (current in checklists) {
				if (current.equals(checklist)) {
					current.editingUser = User(message.headers["user"]);
					break;
				}
			}
			break;

		case EntityAction.UNLOCKED:
			for each (current in checklists) {
				if (current.equals(checklist)) {
					current.editingUser = null;
					break;
				}
			}
			break;
	}
}

private function isCurrentDate(checklist:Checklist):Boolean {
	if (dtUtil.compareDates(checklist.date, dtCheckList.selectedDate) == 0) {
		return true;
	}
	for each (var task:ChecklistTask in checklist.tasks) {
		if (task.date && dtUtil.compareDates(task.date, dtCheckList.selectedDate) == 0) {
			return true;
		}
	}
	return false;
}

private function removeSelectedState():void {
	selectedChecklist = null;
	this.currentState = "normal";
}

private function listAllOfDay():void {
	isOpened = false;
	selectedChecklist = null;
	
	checklistService.findAllNotExcludedOfDate(dtCheckList.selectedDate, checkBoxNotDone.selected, updateCheckLists);
}

private function updateCheckLists(event:ResultEvent):void {
	if (!checklists.sort) {
		var sort:Sort = new Sort();
		sort.fields = [ new SortField("schedule"), new SortField("slug") ];
		checklists.sort = sort;
	}

	checklists.removeAll();
	checklists.addAll(IList(event.result))
	checklists.refresh();
}

private function onRemove(event:FlexEvent):void {
	for each (var tab:Object in mainNavigatorChecklist.getChildren()) {
		if (tab is ChecklistForm) {
			var obj:ChecklistForm = tab as ChecklistForm;
			obj.onRemove();
		}
	}

	tabManager.removeAllTabs();

	checklistService.unsubscribe(onChecklistMessageReceived);

	this.setFocus();
}

public function remove(event:MouseEvent):void {
	checklistService.checkEditionLock(selectedChecklist.id,
			function (event:ResultEvent):void {
				var user:User = event.result as User;
				if (user) {
					var name:String = user.equals(ANews.USER) ? bundle.getString("Bundle", "defaults.you") : ("'" + user.nickname + "'");
					ANews.showInfo(bundle.getString("Bundle", "checklist.msg.cannotDelete", [ name ]), Info.WARNING);
				} else {
					Alert.show(
							bundle.getString("Bundle", "defaults.msg.doYouWantDeleteItem"),
							bundle.getString("Bundle", "defaults.msg.confirmDelete"),
							Alert.NO | Alert.YES, null, onDeleteChecklistConfirm, null, Alert.NO
					);
				}
			}
	);
}

public function editClick(event:MouseEvent):void {
	edit();
}

private function edit(forceOpenFormIfUserIsMe:Boolean = false, taskDate:Date=null):void {
	if (selectedChecklist && ANews.USER.allow('011402')) {

		// Verifica se já está em edição por outro usuário
		checklistService.checkEditionLock(selectedChecklist.id,
				function (event:ResultEvent):void {
					var user:User = event.result as User;
					
					// Sim,  tem alguem editando.
					if(user != null) {
						if (user.equals(ANews.USER)) {
							//Se é eu mesmo, apenas abre o formulário
							var id:String = selectedChecklist.id + "";
							if (forceOpenFormIfUserIsMe || tabManager.isTabOpened(id == "NaN" || id == "0" ? "NEW" : id)) {
								openForm(selectedChecklist);
							
							//Se é eu mesmo, mas na pauta avisa que estou editando em outro lugar.	
							} else {
								ANews.showInfo(bundle.getString("Bundle", "checklist.msg.locked", [ bundle.getString("Bundle", "defaults.you") ]), Info.WARNING);
								
								/*ANews.showInfo(bundle.getString("Bundle", "checklist.msg.lockForGuidelineForm"), Info.WARNING);*/
							}
							
							//Se não sou eu, informa quem é
						} else {
							ANews.showInfo(bundle.getString("Bundle", "checklist.msg.locked", [ ("'" + user.nickname + "'") ]), Info.WARNING);
						}
						
						return;
					}
					
					// Bloqueia a edição para outros usuários
					checklistService.lockEdition(selectedChecklist.id,
							function (event:ResultEvent):void {

								// Recarrega os dados do banco (carrega lazies)
								checklistService.loadById(selectedChecklist.id,
										function (event:ResultEvent):void {
											openForm(Checklist(event.result), taskDate);
										}
								);
							}
					);
				}
		);
	}
}

private function printGrid():void {
    var params:Object = new Object();
    params.date = dtCheckList.text;
    reportUtil.generate(ReportsUtil.CHECKLIST_GRID, params);
}

protected function printChecklists(event:MouseEvent=null):void {
	var params:Object = new Object();
	params.ids = selectedChecklist.id + "|";
	params.date = dtCheckList.text;
	reportUtil.generate(ReportsUtil.CHECKLIST, params);
}

/**
 * Handler para o evento itemClick do menu do popUpButton.
 */
private function onMenuItemClick(event:MenuEvent):void {
	if (event.item == bundle.getString("Bundle", "defaults.btn.printGrid")) {
		printGrid();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.print")) {
		printChecklists();
	}
}

private function initPopUpButtonMenu():void {
	var options:ArrayCollection = new ArrayCollection();

	if (ANews.USER.allow('011404')) {
		options.addItem(bundle.getString('Bundle', 'defaults.btn.printGrid'));
	}
	if (ANews.USER.allow("011404")) {
		options.addItem(bundle.getString("Bundle", "defaults.btn.print"));
	}

	menu.dataProvider = options;

	popUpButton.includeInLayout = popUpButton.visible = options.length != 0;
}

protected function newChecklist(event:MouseEvent):void {
	var checklist:Checklist = new Checklist();
	checklist.author = ANews.USER;
	checklist.date = dtCheckList.selectedDate;

	openForm(checklist);
}

private function onViewHandler(event:MouseEvent):void {
	isOpened = true;
}

private function onCancelHandler(event:MouseEvent):void {
	isOpened = false;
}

private function openForm(_value:Checklist, taskDate:Date = null):void {
	var id:String = _value.id + "";
	var checklist:Checklist = ObjectUtil.copy(_value) as Checklist;

	tabManager.openTab(ChecklistForm, (id == "NaN" || id == "0" ? "NEW" : "checklist_" + id),
			{
				checklist: checklist,
				tabManager: tabManager,
				newTaskDate: taskDate // informa se é para criar uma tarefa vazia
			}
	);
}

/**
 * Listener responsável por receber uma checklist
 * nova criada a partir do drag and drop de uma pauta
 * e persistir os dados na data selecionada da produção.
 *
 */
private function onCreateAndEditEntity(event:CrudEvent):void {
	var newChecklist:Checklist = event.entity as Checklist
	var selectedDate:Date = dtCheckList.selectedDate;

	checklistService.save(newChecklist,
			function (event:ResultEvent):void {
				selectedChecklist = event.result as Checklist;
				edit(true, selectedDate);
			}
	);
}


/**
 * Listener responsável por receber uma pauta para abrir a edição
 * da checklist informada.
 *
 */
private function onEditEntity(event:CrudEvent):void {
	var _checklist:Checklist = event.entity as Checklist
	var selectedDate:Date = dtCheckList.selectedDate;
	selectedChecklist =  _checklist;
	edit(true, selectedDate);
}

private function onDeleteChecklistConfirm(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		checklistService.remove(selectedChecklist.id, onRemoveSucess);
	}
}

private function onRemoveSucess(event:ResultEvent):void {
	isOpened = false;
	ANews.showInfo(bundle.getString("Bundle", "defaults.msg.deleteSuccess"), Info.INFO);
}

private function timeLabelFunction(item:Object, column:GridColumn):String {
	return item[column.dataField] ? formatTime(item[column.dataField]) : "";
}

private function formatTime(timestamp:Date):String {
	return Util.getDateUtil().toString(timestamp, bundle.getString('Bundle', "defaults.timeFormatHourMinute"));
}

private function onSelectionChange(event:GridSelectionEvent):void {
	selectedChecklist = checklistsGrid.selectedItem as Checklist;
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && checklistsGrid.selectedItem != null && checklistsGrid.selectedIndex != -1) {
		if (isOpened == true) {
			isOpened = false;
		} else {
			isOpened = true;
		}
	}
}

// ---------------------------------
// Drag n Drop Methods
// ---------------------------------
private function onDragOver(event:DragEvent):void {
	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
	if (items && items.length > 0) {
		var dragItem:Object = items[0];
		if (dragItem is Guideline) {
			DragManager.showFeedback(DragManager.LINK);
		} else {
			event.preventDefault();
			event.stopPropagation();
			DragManager.showFeedback(DragManager.NONE);
		}
	}
}

private function onDragDrop(event:DragEvent):void {
	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;

	if (items && items.length > 0) {

		var guideline:Guideline = items[0] as Guideline;
		if (guideline) {
			findChecklist(guideline);
		}
	}
}

private function onDragEnter(event:DragEvent):void {
	event.preventDefault();
	DragManager.acceptDragDrop(event.target as IUIComponent);
}

private function findChecklist(guideline:Guideline):void {
	checklistService.findChecklistByGuideline(guideline.id,
			function(event:ResultEvent):void {
				var result:Checklist = event.result as Checklist;

				// Se não existe checklist criada para a pauta. Cria um nova e abre o formulário para edição
				if (result == null) {
					dispatchEditEvent(CrudEvent.CREATE, new Checklist(guideline));
				
				// Se já existir ainda existe algumas validações
				} else {
				
					// 1º verifica se ela está excluída. Se estiver da opção de recupera-lá
					if (result.excluded) {
						confirmRestore(result);
						return;
					}
					
					// 2º Verifica se já não está aberta, se estiver reexibe o formulário
                    var id:String = result.id + "";
                    if (tabManager.isTabOpened(id == "NaN" || id == "0" ? "NEW" : id)) {
                        openForm(result);
						return;
				    } 
					
					// 3º Verifica se já não está em edição
					if(result.editingUser != null) {
						if (result.editingUser.equals(ANews.USER)) {
                        	ANews.showInfo(bundle.getString("Bundle", "checklist.msg.lockForGuidelineForm"), Info.WARNING);
                    	} else {
                        	ANews.showInfo(bundle.getString("Bundle", "checklist.msg.locked", [ result.editingUser.nickname ]), Info.WARNING);
                    	}
						return;
					}
			
					// 4º Enfim, abre o formulário
					dispatchEditEvent(CrudEvent.EDIT, result);
				}
			}
	);
}

private function dispatchEditEvent(type:String, target:Checklist):void {
	this.dispatchEvent(new CrudEvent(type, target));
}

private function confirmRestore(checklist:Checklist):void {
	Alert.show(
			bundle.getString("Bundle", "checklist.msg.restoreFromGuideline"),
			bundle.getString("Bundle", "defaults.msg.confirmation"),
					Alert.NO | Alert.YES, null,

			// Se o usuário confirmar, restaura a produção para a data selecionada
			function(event:CloseEvent):void {
				if (event.detail == Alert.YES) {
					var selectedDate:Date = dtCheckList.selectedDate;
					
					checklistService.restoreExcluded(checklist.id, selectedDate,
							function(event:ResultEvent):void {
								var checklist:Checklist = event.result as Checklist;
								if (checklist) {
                                    selectedChecklist = checklist;
                                    edit(true, selectedDate);
                                } else {
                                    alreadyRestored();
                                }
							}
					);
				}
			}, null, Alert.NO
	);
}

private function alreadyRestored():void {
    ANews.showInfo(bundle.getString("Bundle", "checklist.msg.alreadyRestored"), Info.WARNING);
}

private function pendencyLabelFunciton(item:Object, column:GridColumn):String {
    var checklist:Checklist = item as Checklist;
    return !checklist ? "" : checklist.tasksAsStr;
}
