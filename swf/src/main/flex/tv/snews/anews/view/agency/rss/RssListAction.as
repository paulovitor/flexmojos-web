import flash.utils.setTimeout;

import mx.collections.ArrayCollection;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.domain.RssItem;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.RssCategoryService;
import tv.snews.anews.service.RssService;
import tv.snews.flexlib.components.PagedList;
import tv.snews.flexlib.events.PageEvent;

private const categoryService:RssCategoryService = RssCategoryService.getInstance();
private const rssService:RssService = RssService.getInstance();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.RssCategory")]
private var categories:ArrayCollection = new ArrayCollection();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.RssFeed")]
private var feeds:ArrayCollection = new ArrayCollection();

private const ALL_CATEGORIES:RssCategory = new RssCategory();

[Bindable] private var currentCategory:RssCategory = null;

[Bindable] private var currentFeed:RssFeed = null;

[Bindable] private var singleCategoryColor:uint = 0x000000;

/**
 * Executa as ações de inicialização dos componentes RSS.
 */
private function initRss():void {
	ALL_CATEGORIES.name = resourceManager.getString('Bundle' , 'rss.allFeedsItemsTitle');
	ALL_CATEGORIES.color = 0x000000;

	// Carrega os conteúdos dos comboboxes
	refreshFeedsCombo();
	refreshCategoriesCombo();

	rssService.subscribe(onMessageReceivedRssHandler);
}

private function onMessageReceivedRssHandler(event:MessageEvent):void {
	var feed:RssFeed = event.message.body as RssFeed;
	var type:String = event.message.headers["type"] as String;

	if (type == RssService.CATEGORY_INSERTED || type == RssService.CATEGORY_UPDATED || type == RssService.CATEGORY_DELETED) {
		refreshCategoriesCombo(500);
	} else if (type == RssService.FEED_UPDATED) {
		// Se estiver selecionada a opção de todos ou se a categoria do feed for a categoria selecionada, faz o refresh
		if (currentCategory.id == ALL_CATEGORIES.id || currentCategory.id == feed.category.id) {
			refreshAllFeedsItemsList();
		}
		// Se tiver um feed selecionado e ele for o que foi atualizado, faz o refresh
		if (currentFeed && currentFeed.id == feed.id) {
			refreshSingleFeedItemsList();
		}
	} else if (type == RssService.FEED_INSERTED || type == RssService.FEED_DELETED) {
		// Se estiver selecionada a opção de todos ou se a categoria do feed for a categoria selecionada, faz o refresh
		if (currentCategory.id == ALL_CATEGORIES.id || currentCategory.id == feed.category.id) {
			refreshAllFeedsItemsList();
		}
		refreshFeedsCombo(500);
	}
}

private function onRssItemDoubleClick(list:PagedList):void {
	// Não permite que as duas lista tenham itens selecionados simultaneamente
	if (list == allFeedsItemsList) {
		singleFeedItemsList.selectedIndex = -1;
	} else if (list == singleFeedItemsList) {
		allFeedsItemsList.selectedIndex = -1;
	}
	if(list.count > 0 && list.selectedItem is RssItem) {
		var item:RssItem = list.selectedItem as RssItem;
		var readEvent:CrudEvent = new CrudEvent(CrudEvent.READ, item);
		this.dispatchEvent(readEvent);
	}
}

public function dispose():void {
	rssService.unsubscribe(onMessageReceivedRssHandler);
}

private function formatFeedLabel(item:Object):String {
	var rssFeed:RssFeed = item as RssFeed;
	if (rssFeed != null) {
		return rssFeed.name + " ("+rssFeed.category.name+")";
	}
	return "";
}

//--------------------------------------
//	Methods for "allFeedsItemsList"
//--------------------------------------

private function refreshCategoriesCombo(delay:int = 0):void {
	if (delay > 0) {
		flash.utils.setTimeout(refreshCategoriesCombo, delay);
	}

	categoriesCombo.selectedIndex = -1;

	categoryService.listAll(function(event:ResultEvent):void {
		categories = event.result as ArrayCollection;
		categories.addItemAt(ALL_CATEGORIES, 0);

		if (currentCategory != null) {
			for (var i:int = 0; i < categories.length; i++) {
				if (categories.getItemAt(i).id == currentCategory.id) {
					setSelectedCategoryIndex(i);
					return;
				}
			}
		}

		setSelectedCategoryIndex(0);
	});
}


private function setSelectedCategoryIndex(index:int):void {
	categoriesCombo.selectedIndex = index;
	categoriesCombo.dispatchEvent(new IndexChangeEvent(IndexChangeEvent.CHANGE));
}

private function onCategoriesComboChange(event:IndexChangeEvent):void {
	var newCategory:RssCategory = categoriesCombo.selectedItem as RssCategory;
	if (currentCategory == null || currentCategory.id != newCategory.id) {
		currentCategory = newCategory;
		currentCategory.feeds.removeAll(); // Corrige problema de conversão da ArrayCollection para SortedSet
		refreshAllFeedsItemsList();
	} else {
		currentCategory = newCategory; // apenas para o caso de ter mudado a cor
	}
}

/**
 * Atualiza a lista de itens de todos os feeds.
 */
private function refreshAllFeedsItemsList():void {
	allFeedsItemsList.clear();
	if (currentCategory.id == ALL_CATEGORIES.id) {
		rssService.countItemsFromAllFeeds(function(event:ResultEvent):void {
			allFeedsItemsList.count = event.result as int;
		});
	} else {
		rssService.countItemsFromCategory(currentCategory, function(event:ResultEvent):void {
			allFeedsItemsList.count = event.result as int;
		});
	}
}

/**
 * Handler do evento "pagePending" do "allFeedsPagePending" para carregar uma
 * página de itens que ainda não foi carregada.
 */
private function onAllFeedsPagePendingHandler(event:PageEvent):void {
	if (currentCategory.id == ALL_CATEGORIES.id) {
		rssService.listItemsFromAllFeeds(event.firstResult, event.maxResults, onListItemsFromAllFeedsSuccess, onListItemsFromAllFeedsFailed);
	} else {
		rssService.listItemsFromCategory(currentCategory, event.firstResult, event.maxResults, onListItemsFromAllFeedsSuccess, onListItemsFromAllFeedsFailed);
	}
}

/**
 * Handler para o carregamento bem sucedido da lista de itens de todos os feeds.
 */
private function onListItemsFromAllFeedsSuccess(event:ResultEvent, token:Object = null):void {
	allFeedsItemsList.appendItems(event.result as ArrayCollection, token as int);
}

/**
 * Handler para a falha de carregamento da lista de itens de todos os feeds.
 */
private function onListItemsFromAllFeedsFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(resourceManager.getString('Bundle' , "rss.failedToLoadAllFeedsItems"), Info.ERROR);
}

//--------------------------------------
//	Methods for "singleFeedItemsList"
//--------------------------------------

private function refreshFeedsCombo(delay:int = 0):void {
	if (delay > 0) {
		flash.utils.setTimeout(refreshFeedsCombo, delay);
	}

	feedsCombo.selectedIndex = -1;

	rssService.listFeeds(function(event:ResultEvent):void {
		feeds = event.result as ArrayCollection;

		if (feeds.length == 0) {
			currentFeed = null;
			singleFeedItemsList.clear();
		} else {
			if (currentFeed != null) { // Seleciona novamente o que estava selecionado
				for (var i:int = 0; i < feeds.length; i++) {
					if (feeds.getItemAt(i).id == currentFeed.id) {
						setSelectedFeedIndex(i);
						return;
					}
				}
			}

			setSelectedFeedIndex(0);
		}
	});
}

private function setSelectedFeedIndex(index:int):void {
	feedsCombo.selectedIndex = index;
	feedsCombo.dispatchEvent(new IndexChangeEvent(IndexChangeEvent.CHANGE));
}

private function onFeedsComboChange(event:IndexChangeEvent):void {
	var newFeed:RssFeed = feedsCombo.selectedItem as RssFeed;
	if (currentFeed == null || currentFeed.id != newFeed.id) {
		currentFeed = newFeed;
		currentFeed.category.feeds.removeAll(); // Corrige problema de conversão da ArrayCollection para SortedSet
		refreshSingleFeedItemsList();
	}
	
	colorBoxSingleCategory.setStyle("topBorderColor", newFeed.category.color);
}

/**
 * Verifica se o feed selecionado foi alterado. Se sim recarrega a lista com
 * seus itens.
 */
private function refreshSingleFeedItemsList():void {
	singleFeedItemsList.clear();
	rssService.countItemsFromFeed(currentFeed, function(event:ResultEvent):void {
		singleFeedItemsList.count = event.result as int;
	});
}

/**
 * Handler do evento "pagePending" do "singleFeedPagePending" para carregar uma
 * página de itens que ainda não foi carregada.
 */
private function onSingleFeedPagePendingHandler(event:PageEvent):void {
	rssService.listItemsFromFeed(currentFeed, event.firstResult, event.maxResults, onListItemsFromSingleFeedSuccess, onListItemsFromSingleFeedFailed);
}

/**
 * Handler para o carregamento bem sucedido da lista de itens de todos os feeds.
 */
private function onListItemsFromSingleFeedSuccess(event:ResultEvent, token:Object = null):void {
	singleFeedItemsList.appendItems(event.result as ArrayCollection, token as int);
}

/**
 * Handler para a falha de carregamento da lista de itens de todos os feeds.
 */
private function onListItemsFromSingleFeedFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(resourceManager.getString('Bundle' , "rss.failedToLoadFeedItems"), Info.ERROR);
}
