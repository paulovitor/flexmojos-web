
import flash.events.Event;
import flash.external.ExternalInterface;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.effects.SoundEffect;
import mx.events.CloseEvent;
import mx.events.EffectEvent;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.domain.*;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.view.chat.*;

[ArrayElementType("tv.snews.anews.view.chat.ChatWindow")]
[Bindable] public var chatWindows:ArrayCollection = new ArrayCollection();

[Bindable]
[Embed('/assets/chat/sound/beep.mp3')]
private var beepMP3:Class;
private var beep:SoundEffect = new SoundEffect();

[Bindable] public var chatScroller:ChatScroller;
[Bindable] public static var USER:User = ANews.USER;

private var chatService:ChatService = ChatService.getInstance();
private var loginService:LoginService = LoginService.getInstance();
private var messageService:MessageService = MessageService.getInstance();
private var userService:UserService = UserService.getInstance();

// States names
private static const MINIMIZED_STATE:String = "minimized";
private static const NORMAL_STATE:String = "normal";
private static const DISABLED_STATE:String = "disabled";

private function startUp():void {
	loginService.subscribe(onUsersAtSessionChange);
	messageService.subscribe(onMessageReceived);

	messageService.loadOfflineMessages(onLoadOfflineMessages);

	groupsList.addEventListener(ChatEvent.OPEN_CHAT, onOpenChat);
	this.addEventListener(ChatWindowEvent.MINIMIZE_ALL_WINDOWS, onMinimizeAllWindows);
}

private function onRemove(event:Event):void {
	loginService.unsubscribe(onUsersAtSessionChange);
	messageService.unsubscribe(onMessageReceived);
}

/**
 * Método chamado para efetuar a atualização das informações das ChatWindows 
 * como, por exemplo, o status de online dos participantes das conversas.
 */
private function updateChatWindowsStatus():void {
	for each (var chatWindow:ChatWindow in chatWindows) {
		chatWindow.updateOnlineStatus();
	}
}

// -----------------------------
// Events
// -----------------------------

private function onOpenChat(event:ChatEvent):void {
	if (event.users.length == 1) {
		openChatWithUser(event.users.getItemAt(0) as User);
	} else {
		openChatWithUsers(event.users);
	}
}

private function onMinimizeAllWindows(event:ChatWindowEvent):void {
	for each (var window:ChatWindow in chatWindows) {
		window.changeState(MINIMIZED_STATE);
	}
}

private function onMaximizeWindow(event:ChatWindowEvent):void {
	this.dispatchEvent(new ChatWindowEvent(ChatWindowEvent.MINIMIZE_ALL_WINDOWS));
	var currentChatWindow:ChatWindow = event.target as ChatWindow;
	currentChatWindow.messageCounter = 0;
	currentChatWindow.txtMessageSend.setFocus();
	currentChatWindow.changeState(NORMAL_STATE);
}

private function onMinimizeWindow(event:ChatWindowEvent):void {
	var currentChatWindow:ChatWindow = event.target as ChatWindow;
	currentChatWindow.changeState(MINIMIZED_STATE);
}

private function onWindowClose(event:CloseEvent):void {
	/**
	 * Se for uma conversa em grupo e o usuário fechar a janela, ele será 
	 * ignorado das próximas conversas até que seja convidado novamente para a 
	 * conversa.
	 */
	var currentChatWindow:ChatWindow = event.target as ChatWindow;
	var chat:Chat = currentChatWindow.chat;
	if (chat.members.length > 2) {
		chatService.registerLeave(chat, USER);
	}
	
	currentChatWindow.addEventListener(EffectEvent.EFFECT_END, onCloseEffectEnd);
	currentChatWindow.fadeOut.play([currentChatWindow]);
}

private function onCloseEffectEnd(event:EffectEvent):void {
	var currentChatWindow:ChatWindow = event.target as ChatWindow;
		
	// Desabilita o 'Está digitando' se a janela for fechada.
	if (currentChatWindow.isTyping == true)
		currentChatWindow.isTyping = false;
	
	currentChatWindow.contentGroup.removeAllElements();
//	currentChatWindow.removeEventListener(EffectEvent.EFFECT_END, onCloseEffectEnd);
//	currentChatWindow.removeEventListener(CloseEvent.CLOSE, onWindowClose);
//	currentChatWindow.removeEventListener(ChatWindowEvent.MAXIMIZE_WINDOW, onMaximizeWindow);
//	currentChatWindow.removeEventListener(ChatWindowEvent.MINIMIZE_WINDOW, onMinimizeWindow);

	chatWindows.removeItemAt(chatWindows.getItemIndex(currentChatWindow));
	chatScroller.containerChatWindows.removeElement(currentChatWindow);
	
	currentChatWindow.dispose();
}

// -----------------------------
// Messenger
// -----------------------------

private function onMessageReceived(event:MessageEvent):void {
	if (event.message.headers.notification) {
		handleNotification(event);
	} else {
		handleMessage(event);
	}
}

private function handleNotification(event:MessageEvent):void {
	var headers:Object = event.message.headers;
	
	var chat:Chat = headers.chat as Chat;
	var window:ChatWindow = getChatWindow(chat);
	var user:User;
	if (window != null) {
		if (headers.type == "join") {
			var users:ArrayCollection = headers.users as ArrayCollection;
			window.addNewMembers(users);
		} else if (headers.type == "leave") {
			user = headers.user as User;
			window.removeUser(user);
		} else if (headers.type == "typing") {
			user = headers.user as User;
			var status:Boolean = headers.status as Boolean;
			
			if (user.id != USER.id) {
				var targetWindow:ChatWindow = getChatWindow(chat);
				if (targetWindow != null) {
					targetWindow.isTyping = status;
				}
			}
		} else {
			// Não vai acontecer em produção
			throw new Error("Erro de implementação: tipo de notificação do chat desconhecida.");
		}
	}
}

private function handleMessage(event:MessageEvent):void {
	var message:AbstractMessage = AbstractMessage(event.message.body);
	var showToMe:Boolean = message.sender.id == USER.id;
	
	var destination:MessageDestination;
	
	if (!showToMe) {
		for each (destination in message.destinations) {
			if (destination.receiver.id == USER.id) {
				showToMe = true;
				break;
			}
		}
	}
	
	// Se a mensagem não for para o usuário logado todo o resto do método é desnecessário
	if (showToMe) {
		var chat:Chat = message.chat;
		
		/* 
		* If who send this message is the current user
		* just show the same message on chat's where
		* he type the message.
		*/
		if (message.sender.id == USER.id) {
			var senderWindow:ChatWindow = getChatWindow(chat); // nunca deve retornar null
			senderWindow.displayMessage(message);
		} else {
			// We have to check if the current user is on the list of destinations.
			destination = message.getDestinationOf(USER);
			if (destination != null) {
				/*
				* Otherwise, open a window with the message that de current 
				* user receive. If the window isn't opened open it. If opened 
				* but minimized, show alert.
				*/
				var targetWindow:ChatWindow = getChatWindow(chat);
				if (targetWindow == null) {
					targetWindow = createChatWindow(chat, message);
					targetWindow.changeState(MINIMIZED_STATE); // only in this case the new window is minimized on your own creation
				} else {
					targetWindow.displayMessage(message);
				}
				
				// Apenas marca como lida se a janela estiver maximizada.
				if (targetWindow.state == NORMAL_STATE) {
					messageService.markAsReaded(message, null);
				}
				
				incrementCounterMessages(targetWindow);
				alertBrowser(targetWindow);
			} else {
				throw new Error();
			}
		}
	}
}

private function incrementCounterMessages(chatWindow:ChatWindow):void{
	// Contabiliza mensagens não lidas para o scroller
	if (!chatWindow.isVisible) {
		if (chatWindows.getItemIndex(chatWindow) < chatScroller.getFirstIndexWindowVisible()) {
			chatScroller.totalMessagesLeft++;
		} else {
			chatScroller.totalMessagesRight++;
		}
	}
	
	if (chatWindow.state == MINIMIZED_STATE) {
		chatWindow.messageCounter += 1;
	}	
}

private function onUsersAtSessionChange(event:MessageEvent):void {
	var action:String = event.message.headers.action as String;
	var userId:int = event.message.body as int;
	var user:User = getChatUser(userId);
	
	if (user) {
		user.online = SessionActions.USER_IN == action;
		updateOtherInstancesUsersStatus(user);
		updateChatWindowsStatus();
	}
}

private function updateOtherInstancesUsersStatus(user:User):void{
	for each (var group:UserGroup in DomainCache.groups) {
		for each (var chatUser:User in group.users) {
			if (chatUser.id == user.id && chatUser != user) {
				chatUser.online = user.online; 
			}
		}
	}
}

// -----------------------------
// Asynchronous Answers
// -----------------------------
private function onGetCurrentUser(event:ResultEvent):void {
	USER = event.result as User;
}

private function onLoadOfflineMessages(event:ResultEvent):void {
	var messages:ArrayCollection = event.result as ArrayCollection;
	
	if (messages != null) { 
		for each(var message:AbstractMessage in messages) {
			var targetWindow:ChatWindow = getChatWindow(message.chat);
			if (targetWindow == null) {
				targetWindow = createChatWindow(message.chat, message);
				targetWindow.changeState(MINIMIZED_STATE); // only in this case the new window is minimized on your own creation
			} else {
				targetWindow.displayMessage(message);
			}
			incrementCounterMessages(targetWindow);
		}
		messageService.markAsReadedList(messages, null);
	}
}


// -----------------------------
// Utilities Methods
// -----------------------------

/**
 * Método responsável por verificar se a janela está com foco ativo e, caso não 
 * esteja, exibir o alerta no título da janela e executar o alerta sonoro.
 * 
 * A verificação do estado da janela e a troca do seu título e feita por meio de 
 * chamadas externas à métodos javascript.
 */
private function alertBrowser(chatWindow:ChatWindow):void {
	if (ExternalInterface.call("windowIsActived") == "false") {
		var title:String = "";
		title = chatWindow.state == MINIMIZED_STATE ?  "(" + chatWindow.messageCounter + ") "+ chatWindow.title : chatWindow.title;
		ExternalInterface.call("newMSG", title);
		beep.source = beepMP3;
		beep.play([beep]);
	}
}

/**
 * Abre uma nova janela de chat com o usuário informado.
 */
public function openChatWithUser(user:User):void {
	/*
	 * Primeiramente procura uma possível ChatWindow já criada entre o usuário 
	 * logado e o usuário informado.
	 */
	for each (var window:ChatWindow in chatWindows) {
		if (window.chat.members.length == 2) {
			for each (var member:User in window.chat.members) {
				if (member.id == user.id) {
					window.dispatchChangeStateWindowEvent(new ChatWindowEvent(ChatWindowEvent.MAXIMIZE_WINDOW));
					if (!window.isVisible) {
						chatScroller.showHiddenWindow(window);	
					}
					return;
				}
			}
		}
	}
	
	/*
	 * Se chegou aqui é porque não tem nenhuma janela aberta para o usuário 
	 * desejado, então busca pelo objeto chat que contém o usuário logado e o 
	 * usuário informado no servidor e, se o chat não existir, ele será criado.
	 */
	var members:ArrayList = new ArrayList();
	members.addItem(USER);
	members.addItem(user);
	
	/*
	 * Agora que o chat já foi criado/obtido, cria a ChatWindow.
	 */
	openChatWithUsers(members);
}

/**
 * Abre uma nova janela de chat com o usuário informado.
 */
public function openChatWithUsers(users:ArrayList):void {
	chatService.createChat(users, function(event:ResultEvent):void {
		var chat:Chat = event.result as Chat;
		var chatWindow:ChatWindow = getChatWindow(chat);
		if (chatWindow == null) createChatWindow(chat);
	});
}

/**
 * Cria uma nova ChatWindow para o objeto Chat informado e a exibe. Não é 
 * responsabilidade deste método verificar se a ChatWindow existe. Para isso 
 * utilize antes o "getChatWindow()".
 */
private function createChatWindow(chat:Chat, message:AbstractMessage = null):ChatWindow {
	var chatWindow:ChatWindow = new ChatWindow(ChatBar(this));
	
	if (message == null) {
		this.dispatchEvent(new ChatWindowEvent(ChatWindowEvent.MINIMIZE_ALL_WINDOWS));
	}
	
	chatWindows.addItem(chatWindow);
	chatScroller.containerChatWindows.addElement(chatWindow);
	chatWindow.fadeIn.play([chatWindow]);
	
	chatWindow.chat = chat;
	
	chatWindow.addEventListener(CloseEvent.CLOSE, onWindowClose);
	chatWindow.addEventListener(ChatWindowEvent.MAXIMIZE_WINDOW, onMaximizeWindow);
	chatWindow.addEventListener(ChatWindowEvent.MINIMIZE_WINDOW, onMinimizeWindow);
	
	// Verifica se é necessário carregar o objeto real do chat
	if (chat.members == null || chat.members.length == 0) {
		chatService.loadChat(chat.id, function(event:ResultEvent):void {
			var chat:Chat = event.result as Chat;
			if (chat != null) {
				if (message != null) {
					chatWindow.displayMessage(message);
				}
				chatWindow.chat = chat;
			} else {
				throw new Error(this.resourceManager.getString('Bundle', "chat.msg.chatNotFound"));
			}
		});
	} else {
		if (message != null) {
			chatWindow.displayMessage(message);
		}
	}
	
	//	chatWindow.changeState(ChatWindow.MINIMIZED_STATE);
	if(chatWindow.currentState == "normal") {	
		chatWindow.txtMessageSend.setFocus();
	}
	return chatWindow;
}

/**
 * Procura a ChatWindow para o objeto Chat informado. Se não encontrar retorna 
 * null.
 */
private function getChatWindow(chat:Chat):ChatWindow {
	for each (var chatWindow:ChatWindow in chatWindows) {
		if (chatWindow.chat.id == chat.id) {
			if(chatWindow.currentState == "normal") {	
				chatWindow.txtMessageSend.setFocus();
			}
			return chatWindow;
		}
	}
	return null;
}

/**
 * Busca pelo usuário, através do seu ID, na lista de usuários do chat.
 * NOTE: Agora o método retorna o usuário procurando em todos os grupos.
 */
public function getChatUser(id:int):User {
	if (USER.id == id) {
		return USER;
	}
	
	if (id > 0) {
		for each (var group:UserGroup in DomainCache.groups) {
			for each (var user:User in group.users) {
				if (user.id == id) {
					return user;
				}
			}
		}
	}
	return null;
}
