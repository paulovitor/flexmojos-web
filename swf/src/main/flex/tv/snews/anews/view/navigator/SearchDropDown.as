package tv.snews.anews.view.navigator {

	import flash.events.MouseEvent;
	
	import mx.core.IVisualElement;
	import mx.core.mx_internal;
	
	import spark.components.DropDownList;
	import spark.components.IItemRenderer;

	use namespace mx_internal;

	public class SearchDropDown extends DropDownList {
		private var _stayOpen:Boolean;


		public function SearchDropDown() {
			super();
			dropDownController = new SearchDropDownController();
		}


		public function get stayOpen():Boolean {
			return _stayOpen;
		}

		public function set stayOpen(value:Boolean):void {
			_stayOpen = value;
			dropDownController['stayOpen'] = _stayOpen;
		}

		/**
		 *  @private
		 *  Returns true if v is null or an empty Vector.
		 */
		private function isEmpty(v:Vector.<int>):Boolean {
			return v == null || v.length == 0;
		}

		private function calculateSelectedIndicesInterval(renderer:IVisualElement, shiftKey:Boolean, ctrlKey:Boolean):Vector.<int> {
			var i:int;
			var interval:Vector.<int> = new Vector.<int>();
			var index:Number = dataGroup.getElementIndex(renderer);

			if (!shiftKey) {
				if (ctrlKey) {
					if (!isEmpty(selectedIndices)) {
						// Quick check to see if selectedIndices had only one selected item
						// and that item was de-selected
						if (selectedIndices.length == 1 && (selectedIndices[0] == index)) {
							// We need to respect requireSelection 
							if (!requireSelection)
								return interval;
							else {
								interval.splice(0, 0, selectedIndices[0]);
								return interval;
							}
						} else {
							// Go through and see if the index passed in was in the 
							// selection model. If so, leave it out when constructing
							// the new interval so it is de-selected. 
							var found:Boolean = false;
							for (i = 0; i < selectedIndices.length; i++) {
								if (selectedIndices[i] == index)
									found = true;
								else if (selectedIndices[i] != index)
									interval.splice(0, 0, selectedIndices[i]);
							}
							if (!found) {
								// Nothing from the selection model was de-selected. 
								// Instead, the Ctrl key was held down and we're doing a  
								// new add. 
								interval.splice(0, 0, index);
							}
							return interval;
						}
					}
					// Ctrl+click with no previously selected items 
					else {
						interval.splice(0, 0, index);
						return interval;
					}
				}
				// A single item was newly selected, add that to the selection interval.  
				else {
					interval.splice(0, 0, index);
					return interval;
				}
			} else // shiftKey
			{
				// A contiguous selection action has occurred. Figure out which new 
				// indices to add to the selection interval and return that. 
				var start:int = (!isEmpty(selectedIndices)) ? selectedIndices[0] : 0;
				var end:int = index;
				if (start < end) {
					for (i = start; i <= end; i++) {
						interval.splice(0, 0, i);
					}
				} else {
					for (i = start; i >= end; i--) {
						interval.splice(0, 0, i);
					}
				}
				return interval;
			}
		}

		override protected function item_mouseDownHandler(event:MouseEvent):void {
			//super.item_clickHandler(event);
			var newIndex:Number;

			if (!allowMultipleSelection) {
				// Single selection case, set the selectedIndex 
				newIndex = dataGroup.getElementIndex(event.currentTarget as IVisualElement);

				var currentRenderer:IItemRenderer;
				if (caretIndex >= 0) {
					currentRenderer = dataGroup.getElementAt(caretIndex) as IItemRenderer;
					if (currentRenderer)
						currentRenderer.showsCaret = false;
				}

				// Check to see if we're deselecting the currently selected item 
				if (event.ctrlKey && selectedIndex == newIndex)
					selectedIndex = NO_SELECTION;
				// Otherwise, select the new item 
				else
					selectedIndex = newIndex;
			} else {
				// Multiple selection is handled by the helper method below				
				selectedIndices = calculateSelectedIndicesInterval(event.currentTarget as IVisualElement, event.shiftKey, event.ctrlKey);
			}
			userProposedSelectedIndex = selectedIndex;
			if (!stayOpen) {
				closeDropDown(true);
			}

		}

		override public function closeDropDown(commit:Boolean):void {
			if (!stayOpen) {
				super.closeDropDown(commit);
			}
		}

	}
}
