package tv.snews.anews.view.chat {

	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.FileReference;
	import flash.net.URLRequest;
	import flash.net.URLVariables;
	import flash.ui.Keyboard;
	import flash.utils.setTimeout;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.controls.Alert;
	import mx.core.IUIComponent;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.events.CollectionEvent;
	import mx.events.DragEvent;
	import mx.events.ResizeEvent;
	import mx.managers.DragManager;
	import mx.rpc.events.ResultEvent;
	
	import spark.components.ComboBox;
	import spark.components.HGroup;
	import spark.components.Label;
	import spark.components.List;
	import spark.components.RichEditableText;
	import spark.components.SkinnableContainer;
	import spark.components.VGroup;
	import spark.core.NavigationUnit;
	import spark.effects.Fade;
	
	import tv.snews.anews.component.Info;
	import tv.snews.anews.domain.*;
	import tv.snews.anews.service.*;
	import tv.snews.anews.utils.*;
	import tv.snews.flexlib.components.Progress;
	import tv.snews.flexlib.utils.StringUtils;

	[SkinState("normal")]
	[SkinState("minimized")]

	/**
	 * @author Felipe Zap de Mello
	 * @author Eliezer Reis
	 *
	 * @see ChatWindowSkin.mxml
	 * @version 1.0.0
	 */
	public class ChatWindow extends SkinnableContainer {

		// States names
		private static const MINIMIZED_STATE:String = "minimized";
		private static const NORMAL_STATE:String = "normal";
		private static const DISABLED_STATE:String = "disabled";

		//Used in the 'Autocomplete'
		public static const AC_USER:String = "user";
		public static const AC_GROUP:String = "group";

		// Fields that is required on ChatWindow.
		[SkinPart]
		public var txtIsTyping:Label;
		[SkinPart]
		public var txtMessageSend:RichEditableText;
		[SkinPart]
		public var messageList:List;
		[SkinPart]
		public var progressUpload:Progress;
		[SkinPart]
		public var lastMessagesLabel:Label;
		[SkinPart]
		public var lastMessagesVgroup:VGroup;
		[SkinPart]
		public var inviteAreaUser:HGroup;
		[SkinPart]
		public var inviteAreaGroup:HGroup;
		[SkinPart]
		public var inviteInputUser:ComboBox;
		[SkinPart]
		public var inviteInputGroup:ComboBox;

		[SkinPart]
		public var fadeIn:Fade;
		[SkinPart]
		public var fadeOut:Fade;

		private var scrollOnTheEnd:Boolean = true;
		private var hiddenMessages:Array = null;

		private var _chatBar:ChatBar;

		//Internal
		private var _fileRef:FileReference;

		private var _onlineStatus:Boolean;
		private var _title:String;
		private var _messageCounter:int;
		private var _isOnline:Boolean;
		private var _isVisible:Boolean = true; //Don't touch!
		private var _file:UserFile = null; //File uploading.
		private var _scrollPosition:int;
		private var _isTyping:Boolean = false;
		private var _chat:Chat;
		private var _firstMessageChatResult:Date = null;
		private var _moreMessages:int = 10;

		[Bindable]
		[ArrayElementType("tv.snews.anews.domain.AbstractMessage")]
		public var messages:ArrayCollection = new ArrayCollection();

		[Bindable] public var state:String = NORMAL_STATE;

		[Bindable] public var users:ArrayCollection = new ArrayCollection();

		[Bindable] public var groups:ArrayCollection = new ArrayCollection();

		private var chatService:ChatService = ChatService.getInstance();
		private var messageService:MessageService = MessageService.getInstance();

		public function ChatWindow(parentChatBar:ChatBar) {
			super();
			setStyle("skinClass", ChatWindowSkin);
			_chatBar = parentChatBar;

			DomainCache.groups.addEventListener(CollectionEvent.COLLECTION_CHANGE, updateAutocompleteList);
			messages.addEventListener(CollectionEvent.COLLECTION_CHANGE, onMessagesChange);
		}

		//----------------------------------------------------------------------
		//
		//	Overrides
		//
		//----------------------------------------------------------------------
		override protected function getCurrentSkinState():String {
			return state;
		}

		private const listeners:ListenersDictionary = new ListenersDictionary(EventDispatcher(this));

		override public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
			super.addEventListener(type, listener, useCapture, priority, useWeakReference);

			// Armazena o listener adicionado
			listeners.registerListener(type, listener);
		}

		//----------------------------------------------------------------------
		//
		//	Drag event
		//
		//----------------------------------------------------------------------
		
		public function dragEnter(event:DragEvent):void {
			//Suppress the out of the box prevention of the drag & drop.
			event.preventDefault();
			
			var dragItem:Object = event.dragSource.dataForFormat("itemsByIndex")[0];

			if (!(dragItem is ScriptBlock)) {
				//Notify the drag manager of the target 
				//the component upon which you will drop the data.
				DragManager.acceptDragDrop(event.target as IUIComponent);
			}
		
		}
		
		public function dragDrop(event:DragEvent):void {
			//Provide an appropriate icon for the drag & drop action
			//I use a link icon.       
			DragManager.showFeedback(DragManager.LINK);
			var dragItem:Object = event.dragSource.dataForFormat("itemsByIndex")[0];
			messageService.sendEntityMessage(dragItem, chat, null);
		}
		
		public function dragOver(event:DragEvent):void {
			
			//Suppress the default Flex behavior of preventing 
			//the drag & drop as you move around the component.            
			event.preventDefault();	
		}
		
		//----------------------------------------------------------------------
		//
		//	Event Handlers
		//
		//----------------------------------------------------------------------

		public function dispose():void {
			listeners.deactivateListeners();
			DomainCache.groups.removeEventListener(CollectionEvent.COLLECTION_CHANGE, updateAutocompleteList);

			/**
			 * NOTE: Provavelmente não é necessário remover os event listeners
			 * abaixo, por eles serem de objetos internos da chat window, mas
			 * optei por remover por segurança.
			 */

			messages.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onMessagesChange);

			chat.members.removeEventListener(CollectionEvent.COLLECTION_CHANGE, updateAutocompleteList);
			chat.leftMembers.removeEventListener(CollectionEvent.COLLECTION_CHANGE, updateAutocompleteList);

			/**
			 * Limpando referências (teoricamente isso é código desnecessário,
			 * mas adicionei para testes).
			 */
			groups.removeAll();
			users.removeAll();
			messages.removeAll();
			chat.messages.removeAll();
			chat.members.removeAll();
			chat.leftMembers.removeAll();

			groups = null;
			users = null;
			messages = null;
			_chat = null;
			_chatBar = null;

//			for each (var property:String in this) {
//				this[property] = null;
//			}
		}

		/**
		 * Efetua a operação de obter a lista de usuários da ChatBar e remove
		 * aqueles que não devem ser apresentados no autocomplete, ou seja, quem
		 * já está participando da conversa.
		 */
		public function updateAutocompleteList(event:CollectionEvent = null):void {
			//** Popula o atributo grupos para a lista do autocomplete **/
			groups.removeAll();
			groups.addAll(DomainCache.groups);

			users.removeAll();
			var canJoin:Boolean, canReallyJoin:Boolean;

			for each (var group:UserGroup in DomainCache.groups) {
				for each (var userFromGroup:User in group.users) {
					// Primeiro verifica se o usuário já não está ativo na conversa
					canJoin = true;
					for each (var activeUser:User in chat.activeMembers()) {
						if (userFromGroup.id == activeUser.id) {
							canJoin = false;
							break;
						}
					}

					// Agora verifica se o usuário já não foi adicionado em uma 
					// iteração anterior da lista de usuários de outro grupo
					if (canJoin) {
						canReallyJoin = true;
						for each (var user:User in users) {
							if (userFromGroup.id == user.id) {
								canReallyJoin = false;
								break;
							}
						}

						if (canReallyJoin) {
							users.addItem(userFromGroup);
						}
					}
				}
			}

			// Aproveita para também atualizar o título da janela
			updateTitle();
			updateOnlineStatus();
			setLastMessagesVisibility();
		}

		private function setLastMessagesVisibility():void {
			lastMessagesVgroup.includeInLayout = chat.members.length == 2;
			lastMessagesVgroup.visible = chat.members.length == 2;
		}

		/**
		 * Evento para a mensagem "... está digitando".
		 */
		public function onKeyUp(event:KeyboardEvent):void {
			var indexLastMessage:int = 0;
			// Essa notificação é apenas para as conversas entre duas pessoas
			if (chat.members.length == 2) {
				// Verifica se houve realmente uma mudança no status do typing 
				// para evitar requisições desnecessárias ao servidor.
				var aux:Boolean = txtMessageSend.text.length > 0;
				if (aux != _isTyping) {
					_isTyping = aux;

					messageService.isTyping(_isTyping, chat);
				}
			}
		}

		/**
		 * Evento para o envio da mensagem.
		 */
		public function onKeyDown(event:KeyboardEvent):void {
			var indexLastMessage:int = 0;
			if (event.keyCode == Keyboard.ENTER) {
				var message:String = txtMessageSend.text;
				if (!StringUtils.isNullOrEmpty(message)) {
					txtMessageSend.text = "";
					messageService.sendMessage(message, chat, null);
				}
			}
		}

		/**
		 * Event Handler responsável por carregar as mensagens que os dois
		 * usuários da conversa trocaram anteriormente.
		 */
		public function lastMessages(event:Event):void {
			if (messages != null && messages.length > 0) {
				_firstMessageChatResult = Object(messages.getItemAt(0)).date as Date;
			}
			if (chat.members.length == 2) {
				for each (var member:User in chat.members) {
					if (member.id != ANews.USER.id) {
						lastMessagesLabel.text = resourceManager.getString('Bundle', "chat.moreMessages");
						messageService.loadLastMessagesWith(member, moreMessages, _firstMessageChatResult, lastMessagesReceived);
						moreMessages += 5;
					}
				}
			}
		}

		/**
		 * Result handler para o método MessageService#loadLastMessagesWith(),
		 * que é chamado pelo método lastMessages().
		 */
		private function lastMessagesReceived(event:ResultEvent):void {
			var lastMessagesList:ArrayCollection = ArrayCollection(event.result);
			var lastMessages:InformMessage;
			if (lastMessagesList.length > 0 && moreMessages <= 15) {
				lastMessages = new InformMessage(InformMessage.LAST_MESSAGES);
				messages.addItemAt(lastMessages, 0);
			}
			if (lastMessagesList.length == 0) {
				lastMessagesVgroup.visible = false;
				lastMessagesVgroup.includeInLayout = false;
			}
			messages.addAllAt(lastMessagesList, 0);
		}

		/**
		 * Chamado quando o campo de envio de mensagem sofre um resize. Corrige
		 * a posição do scroll para o ponto correto.
		 */
		public function onMessageSendResizeHandler(event:ResizeEvent):void {
			correctScrollPosition()
		}

		/**
		 * Handler para o evento de change da coleção de mensagens do objeto
		 * Chat. Verifica se o scroll está no final e armazena na variavel
		 * "scrollOnTheEnd".
		 */
		private function onMessagesChange(event:CollectionEvent):void {
			correctScrollPosition();
		}

		public function changeState(state:String = NORMAL_STATE):void {
			this.state = state;
			invalidateSkinState();
		}

		public function dispatchChangeStateWindowEvent(event:ChatWindowEvent):void {
			this.dispatchEvent(event);
		}

		public function dispatchCloseEvent():void {
			this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
		}

		/**
		 * Exibe o campo autocomplete para adicionar usuários ao chat.
		 *
		 * Note: modificado para mostrar esconder a área de grupo ou usuário
		 * permitindo somente uma área visível por vez.
		 */
		public function displayInviteInput(event:Event, searchFor:String):void {
			event.stopPropagation();
			hideOtherInviteArea(searchFor != AC_USER ? AC_USER : AC_GROUP);
			changeInviteAreaVisibility(searchFor);
		}

		/**
		 * Pela a lista de usuários dos grupos que podem entrar na conversa, ou
		 * seja, os que não entraram ainda no chat ou que entrou mais saiu.
		 */
		private function getUsersFromGroups(groups:ArrayCollection, chat:Chat):ArrayCollection {
			var usersToInvite:ArrayCollection = new ArrayCollection();

			for each (var group:UserGroup in groups) {
				for each (var user:User in group.users) {
					if (chat.containsMember(user)) {
						if (chat.userLeftChat(user)) {
							usersToInvite.addItem(user);
						}
					} else {
						usersToInvite.addItem(user);
					}
				}
			}

			return usersToInvite;
		}

		/**
		 * Note: o mesmo método que convidava um usuário foi refatorado
		 * para aceitar convites de grupos. Conforme o parâmetro passado em inviteFor (usuário ou grupo).
		 */
		public function inviteUserOrGroup(event:Event, inviteFor:String):void {

			//var selecteds:ArrayCollection = inviteFor == AC_USER ? inviteInputUser.selectedItems as ArrayCollection : getUsersFromGroups(inviteInputGroup.selectedItems, chat) as ArrayCollection;
			
			
			
			var selecteds:ArrayCollection = new ArrayCollection();
			
			if (inviteFor == AC_USER) {
				for each (var elem:User in inviteInputUser.selectedItems) {
					selecteds.addItem(elem);
				}
				
				//selecteds = inviteInputUser.selectedItems as ArrayCollection;
			}	
			else {
				var selectedsGroups:ArrayCollection = new ArrayCollection();
				
				for each (var elemGrop:UserGroup in inviteInputGroup.selectedItems) {
					selectedsGroups.addItem(elemGrop);
				}
				
				selecteds = getUsersFromGroups(selectedsGroups, chat);
			}

			if (selecteds.length != 0) {
				if (chat.members.length == 2) { // conversa entre 2 pessoas
					// Nesse caso deve criar um novo chat com todos o membro e 
					// abrir uma nova ChatWindow

					var users:ArrayList = new ArrayList();
					users.addAll(chat.members);
					users.addAll(selecteds);
					chatBar.openChatWithUsers(users);
				} else { // conversa em grupo
					// Nesse caso deve alterar o objeto chat atual para 
					// adicionar os novos membros

					chatService.addMembers(selecteds, chat, function(event:ResultEvent):void {
						// Código não mais necessário pois ele irá receber do 
						// servidor a notificação do ingresso no novo participante
						//						chat.members.addAll(selecteds);
					});
				}
				changeInviteAreaVisibility(inviteFor == AC_USER ? AC_USER : AC_GROUP);
			}

			//inviteInputUser.clear();
			//inviteInputGroup.clear();
		}


		//----------------------------------------------------------------------
		//
		//	Upload Methods
		//
		//----------------------------------------------------------------------

		public function uploadFile(event:Event):void {
			event.stopPropagation();

			if (file != null) {
				ANews.showInfo(resourceManager.getString('Bundle', 'chat.msg.multiplyFilesWarning'), Info.WARNING);
				return;
			}

			progressUpload.label = resourceManager.getString('Bundle', 'chat.msg.uploadProgress');

			_fileRef = new FileReference();
			_fileRef.addEventListener(Event.SELECT, onSelectFile);
			_fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onFileUploadComplete);
			_fileRef.addEventListener(ProgressEvent.PROGRESS, onFileUploadProgress);
			_fileRef.addEventListener(Event.CANCEL, onCancel);
			_fileRef.addEventListener(IOErrorEvent.IO_ERROR, errorEvent);
			_fileRef.addEventListener(SecurityErrorEvent.SECURITY_ERROR, errorEvent);
			_fileRef.browse(Util.CHAT_FILE_FILTER);
		}

		public function cancelUpload():void {
			if (_fileRef != null) {
				_fileRef.cancel()
				file = null;
					// TODO Enviar mensagem avisando somente o usuário corrente que ele cancelou o envio do arquivo.
			}
		}

		private function onSelectFile(event:Event):void {
			file = getFileFromFileReference(_fileRef);

			if (file.size <= 0) {
				ANews.showInfo(resourceManager.getString('Bundle', "chat.msg.fileNotValid"), Info.WARNING);
				return;
			}
			if (file.size >= int.MAX_VALUE) {
				ANews.showInfo(resourceManager.getString('Bundle', "chat.msg.fileOverSize"), Info.WARNING);
				return
			}

			sendFile();
		}

		private function onFileUploadComplete(event:DataEvent):void {
			file = null;

			var data:XML = new XML(event.data);
			var _myFile:UserFile = new UserFile();
			_myFile.name = data.fileName.text();
			_myFile.extension = data.extension.text();
			_myFile.size = new Number(data.size.text());
			_myFile.path = data.path.text();

			UserFileService.getInstance().saveUserFile(_myFile, function(event:ResultEvent):void {
				messageService.sendFile(event.result as UserFile, chat, null);
			});

		}

		private function onFileUploadProgress(event:ProgressEvent):void {
			file.loaded = event.bytesLoaded;
		}

		private function onCancel(event:Event):void {
			file = null;
		}

		private function errorEvent(event:Event):void {
			file = null;
			//TODO Enviar mensagem informando erro no envio do envio
		}

		private function sendFile():void {
			if (file == null)
				return;

			// prepara algumas váriaveis que devem ser enviadas para o servidor.
			var vars:URLVariables = new URLVariables();
			vars.extension = file.extension;
			vars.fileName = file.name;
			vars.size = file.size;
			vars.source = "chatfile";
			// faz o upload.
			var url:URLRequest = new URLRequest(Util.urlUpload());
			url.data = vars;

			_fileRef.upload(url);
		}

		private function getFileFromFileReference(fileRef:FileReference):UserFile {
			var _myFile:UserFile = new UserFile();
			_myFile.extension = Util.getExtension(fileRef.name);
			_myFile.name = fileRef.name.replace(_myFile.extension, "");
			try {
				_myFile.size = fileRef.size;
			} catch (e:Error) {
				_myFile.size = 0;
			}

			_myFile.loaded = 0;
			_myFile.total = _myFile.size;
			return _myFile;
		}

		//----------------------------------------------------------------------
		//
		//	Utilities
		//
		//----------------------------------------------------------------------

		private function correctScrollPosition():void {
			messageList.dataGroup.clipAndEnableScrolling = true;
			checkScrollPosition();
			flash.utils.setTimeout(scrollDown, 0); // apenas faz um delay rolagem do scroller
		}

		private function checkScrollPosition():void {
			scrollOnTheEnd = messageList.scroller.viewport.getVerticalScrollPositionDelta(NavigationUnit.END) == 0;
		}

		/**
		 * Função que verifica se o scroller estava no final antes da coleção de
		 * mensagens ser alterada; se sim, move ele novamente para o final.
		 */
		public function scrollDown():void {
			if (scrollOnTheEnd) {
				messageList.scroller.verticalScrollBar.value = UIComponent.DEFAULT_MAX_HEIGHT;
			}
		}

		/**
		 * Método chamado pela ChatAction para adicionar uma nova mensagem. Ela
		 * será adicionada a lista de mensagens da ChatWindow e à lista de
		 * mensagens do objeto Chat.
		 */
		public function displayMessage(message:AbstractMessage):void {
			messages.addItem(message);
			chat.addMessage(message);
		}

		/**
		 * Método chamado quando um ou mais novos membros ingressam a uma
		 * conversa em grupo. Exibe a notificação da entrada de cada usuário.
		 */
		public function addNewMembers(users:ArrayCollection):void {
			chat.addMembers(users);
			for each (var user:User in users) {
				var joinMsg:InformMessage = new InformMessage(InformMessage.JOIN_MESSAGE);
				joinMsg.user = user;
				messages.addItem(joinMsg);
			}

			checkSendable();
		}

		/**
		 * Método chamado quando um usuário sai de uma conversa em grupo. Exibe
		 * a notificação da saída do usuário.
		 */
		public function removeUser(user:User):void {
			chat.registerLeave(user);
			var leftMsg:InformMessage = new InformMessage(InformMessage.LEFT_MESSAGE);
			leftMsg.user = user;
			messages.addItem(leftMsg);

			checkSendable();
		}

		/**
		 * Desabilita o envio de mensagens se necessário
		 */
		private function checkSendable():void {
			var enabled:Boolean = chat.members.length - 1 > chat.leftMembers.length;
			txtMessageSend.enabled = enabled;
			txtMessageSend.editable = enabled;
		}

		/**
		 * Esconde a área de convidar grupo ou usuário caso alguma delas
		 * já esteja visível na tela.
		 */
		private function hideOtherInviteArea(disableFor:String):void {
			var inviteArea:HGroup = null;
			var inviteInput:ComboBox = null;

			if (disableFor == AC_USER) {
				inviteArea = inviteAreaUser;
				inviteInput = inviteInputUser;
			} else {
				inviteArea = inviteAreaGroup;
				inviteInput = inviteInputGroup;
			}

			inviteArea.visible = false;
			inviteArea.includeInLayout = false;
		}

		/**
		 * Método chamado quando clicado no botão de convidar grupo
		 * ou usuário.
		 * Mostra/Esconde a área de convidar.
		 */
		private function changeInviteAreaVisibility(searchFor:String):void {
			var inviteArea:HGroup = null;
			var inviteInput:ComboBox = null;

			if (searchFor == AC_USER) {
				inviteArea = inviteAreaUser;
				inviteInput = inviteInputUser;
			} else {
				inviteArea = inviteAreaGroup;
				inviteInput = inviteInputGroup;
			}

			inviteArea.visible = !inviteArea.visible;
			inviteArea.includeInLayout = !inviteArea.includeInLayout;

			if (inviteArea.visible) {
				focusManager.setFocus(inviteInput);
			}
		}

		/**
		 * Verifica se algum dos usuários do chat está online e atualiza a
		 * variável me mantem essa informação.
		 */
		public function updateOnlineStatus():void {
			onlineStatus = false;
			for each (var user:User in chat.activeMembers()) {
				// Pega o status online da ChatBar
				var chatUser:User = chatBar.getChatUser(user.id);
				if (chatUser != null) {
					user.online = chatUser.online;
				}

				if (user.id != ANews.USER.id && user.online) {
					onlineStatus = true;
				}
			}
		}

		private function updateTitle():void {
			var users:ArrayCollection = new ArrayCollection();
			for each (var user:User in chat.activeMembers()) {
				if (user.id != ANews.USER.id) {
					users.addItem(user);
				}
			}

			// Gera o titulo no formato "Fulano, Beltrano e Ciclano"
			var aux:Array = new Array();
			for (var i:int = 0; i < users.length; i++) {
				user = users.getItemAt(i) as User;
				aux.push(user.getFirstName());
				if (i < users.length - 2) {
					aux.push(", ");
				} else if (i < users.length - 1) {
					aux.push(" " + resourceManager.getString('Bundle', "chat.and") + " ");
				}
			}
			title = aux.join("");
		}

		private function setIsTypingMessage():void {
			if (chat.members.length == 2) {
				for each (var member:User in chat.members) {
					if (member.id != ANews.USER.id) {
						txtIsTyping.text = member.name + ' ' + resourceManager.getString('Bundle', 'chat.msg.isTyping');
					}
				}
			}
		}
		
		/////////////

		//----------------------------------------------------------------------
		//
		//	Getters & Setters
		//
		//----------------------------------------------------------------------

		/**
		 * (Somente leitura)
		 */
		public function get chatBar():ChatBar {
			return _chatBar;
		}

		public function get scrollPosition():int {
			return _scrollPosition;
		}

		public function set scrollPosition(value:int):void {
			_scrollPosition = value;
		}

		public function get chat():Chat {
			return _chat;
		}

		[Bindable] public function set chat(value:Chat):void {
			_chat = value;
			_chat.members.addEventListener(CollectionEvent.COLLECTION_CHANGE, updateAutocompleteList);
			_chat.leftMembers.addEventListener(CollectionEvent.COLLECTION_CHANGE, updateAutocompleteList);

			updateTitle();
			updateOnlineStatus();
			setIsTypingMessage();
			updateAutocompleteList();
		}

		public function get onlineStatus():Boolean {
			return _onlineStatus;
		}

		[Bindable] public function set onlineStatus(value:Boolean):void {
			_onlineStatus = value;
		}

		[Bindable] public function set isVisible(value:Boolean):void {
			_isVisible = value;
			this.visible = _isVisible;
			this.includeInLayout = _isVisible;
		}

		public function get isVisible():Boolean {
			return _isVisible;
		}

		public function get title():String {
			return _title;
		}

		[Bindable] public function set title(value:String):void {
			_title = value;
		}

		public function get messageCounter():int {
			return _messageCounter;
		}

		[Bindable] public function set messageCounter(value:int):void {
			_messageCounter = value;
		}

		public function get isTyping():Boolean {
			return _isTyping;
		}

		[Bindable] public function set isTyping(value:Boolean):void {
			if (value && !_isTyping) {
				correctScrollPosition();
			}
			_isTyping = value;
		}

		public function get file():UserFile {
			return _file;
		}

		[Bindable] public function set file(value:UserFile):void {
			_file = value;
		}

		public function get firstMessageChatResult():Date {
			return _firstMessageChatResult;
		}

		public function set firstMessageChatResult(value:Date):void {
			_firstMessageChatResult = value;
		}

		public function get moreMessages():int {
			return _moreMessages;
		}

		[Bindable] public function set moreMessages(value:int):void {
			_moreMessages = value;
		}
	}
}
