import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import spark.events.GridSelectionEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.gridClasses.GridColumn;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.User;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.utils.*;
import tv.snews.flexlib.utils.DateUtil;

private const reportageService:ReportageService = ReportageService.getInstance();
private const dateUtil:DateUtil = Util.getDateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();

[Bindable] private var showViewer:Boolean = false;
[Bindable] private var selectedReportage:Reportage;

private function onContentCreationComplete(event:FlexEvent):void {
	reportageService.subscribe(onMessageReportageTrash);
}

private function onRemove(event:Event):void {
	reportageService.unsubscribe(onMessageReportageTrash);
}
//------------------------------------------------------------------------------
//	Actions
//------------------------------------------------------------------------------
private function search(page:int=1):void {
	if(page < 1)
		page = 1;
	if(!restoreTo.visible){
        selectedReportage = null;
        reportageService.listExcludedReportagesPage(page, onListSuccess);
    }

}

/**
 * Gera impressão da pauta selecionada.
 */
private function print(event:Event):void {
	var reportage:Reportage = reportageGrid.selectedItem as Reportage;
	
	if(reportage) {
		var params:Object = new Object();
		params.ids = reportage.id + "|";
		reportUtil.generate(ReportsUtil.REPORTAGE, params);
	}
}

/**
 * Gera impressão da pauta selecionada.
 */
private function onViewClick(event:MouseEvent):void{
	showViewer = true;
}

private function onCancelClick(event:MouseEvent):void {
	showViewer = false;
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && reportageGrid.selectedItem != null && reportageGrid.selectedIndex != -1) {
		if (showViewer == true) {
			showViewer = false;
		} else {
			showViewer = true;
		}
	}
}

// -----------------------------------------------------------------------------
// Asynchronous Answers.
// -----------------------------------------------------------------------------
private function onMessageReportageTrash(event:MessageEvent):void {
	var reportage:Reportage = event.message.body as Reportage;
	var action:String = event.message.headers.action;
	switch (action) {
		case EntityAction.RESTORED:
			search();	
		break;
		case EntityAction.DELETED:
			search();	
		break;
	}
}

private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	pageBar.load(result.results, result.pageSize, result.total, result.pageNumber);
	reportageGrid.selectedIndex = -1;
}

private function onRestoreSuccess(event:ResultEvent):void {
	ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "reportage.trash.msg.restoreSuccess"), Info.INFO);
	reportageGrid.selectedIndex = -1;
}

// -----------------------------------------------------------------------------
// Formatters
// -----------------------------------------------------------------------------
private function scheduleLabelFunction(item:Object, column:GridColumn):String {
	const reportage:Reportage = item as Reportage;
	return dateUtil.dateToString(reportage.date);
}

private function excludedLabelFunction(item:Object, column:GridColumn):String {
	const reportage:Reportage = item as Reportage;
	const author:String = reportage.author ? reportage.author.nickname : "";
	return author + " (" + dateUtil.fullDateToString(reportage.changeDate, true) + ")";
}

private function onSelectionChange(event:GridSelectionEvent):void {
    selectedReportage = reportageGrid.selectedItem as Reportage;
}

private function confirmRestore():void {
    var date:Date = destinyDay.selectedDate;
    var program:Program = destinyProgram.selectedItem as Program;

    reportageService.restoreExcluded(selectedReportage.id, date, program.id, function(event:ResultEvent):void {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "checklist.trash.msg.restoreSuccess"), Info.INFO);
        cancelRestoreTo();
        search();
    });
}

private function cancelRestoreTo():void {
    restoreTo.visible = false;
    restoreTo.includeInLayout = false;
    selectedReportage = null;
    reportageGrid.selectedIndex = -1;
    showViewer = false;
}

private function onRestoreClick(event:MouseEvent):void {
    restoreTo.visible = true;
    restoreTo.includeInLayout = true;
}
