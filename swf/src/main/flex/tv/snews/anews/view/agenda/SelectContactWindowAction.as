import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.ui.Keyboard;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.FocusManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.core.IEditableText;
import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.ContactService;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.agenda.ContactForm;
import tv.snews.anews.view.agenda.SimpleContactForm;

[Bindable]
public var forceAgendaSave:Boolean = false;

[Bindable]
private var contacts:ArrayCollection;

private const contactService:ContactService = ContactService.getInstance();
private const timer:Timer = new Timer(1000, 1);

private var currentText:String = null;

override public function set currentState(value:String):void {
	super.currentState = value;
	
	if (value == "search" && contactsCombo) {
		this.focusManager.setFocus(contactsCombo);
	} else if (fullForm) {
		ContactForm(fullForm).setInitialFocus();
	}
}

private function onCreationComplete(event:FlexEvent):void {
	timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerComplete);
	contactsCombo.textInput.addEventListener(TextOperationEvent.CHANGE, onSearchTextChange);
}

private function onTimerComplete(event:TimerEvent):void {
//	var searchText:String = getSearchInputText();
//	result.text = "result: '" + searchText + "'";
	contactService.findByName(currentText, 0, 50, onListContactsComplete, onListContactsFailed);
}

private function onListContactsComplete(event:ResultEvent, token:Object = null):void {
	contacts = event.result as ArrayCollection;
}

private function onListContactsFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(this.resourceManager.getString("Bundle", "agenda.msg.failedToLoadContacts"), Info.ERROR);
}

private function onSearchTextChange(event:TextOperationEvent):void {
	var newText:String = getSearchInputText();
	if (newText != currentText) {
		currentText = newText;
		
		timer.stop();
		timer.start();
	}
}

private function replaceHighlightMarkups(contact:AgendaContact):void {
	if (contact) {
		contact.name = Util.replaceHighlightMarkup(contact.name);
		contact.profession = Util.replaceHighlightMarkup(contact.profession);
		contact.company = Util.replaceHighlightMarkup(contact.company);
		contact.info = Util.replaceHighlightMarkup(contact.info);
		contact.email = Util.replaceHighlightMarkup(contact.email);
		contact.address = Util.replaceHighlightMarkup(contact.address);
	}
}

private function contactLabelFunction(item:Object):String {
	var contact:AgendaContact = item as AgendaContact;
	if (contact) {
		replaceHighlightMarkups(contact);

		var builder:Array = [];
		builder.push(contact.name);
		if (contact.profession && contact.profession.length > 0) {
			builder.push(" (");
			builder.push(contact.profession);
			builder.push(")");
		}
		return builder.join("");
	}
	return item as String;
}

private function onSelectClick(event:MouseEvent):void {
	var contact:AgendaContact = contactsCombo.selectedItem as AgendaContact;
	if (contact) {
		replaceHighlightMarkups(contact);
		this.dispatchEvent(new CrudEvent(CrudEvent.COPY, contact));
		close();
	} else {
		ANews.showInfo(this.resourceManager.getString('Bundle', 'agenda.window.noContactSelected'), Info.WARNING);
	}
}

private function onSaveClick(event:MouseEvent):void {
	
	if (fullContactCheck.selected) {
		ContactForm(fullForm).save();
	} else { 
		SimpleContactForm(simpleForm).save();
	}
}

private function onEditClick(event:MouseEvent):void {
	this.currentState = "insert";
	if (fullContactCheck) {
		this.fullContactCheck.selected = true;
		this.fullContactCheck.visible = false;
		this.fullContactCheck.includeInLayout = false;	
	}
	
	fullForm.contact = contactsCombo.selectedItem as AgendaContact;
	
}

private function btnNew_clickHandler(event:MouseEvent):void  {
    if (forceAgendaSave) {
        if (fullForm) {
            fullForm.contact = new AgendaContact();
        }
    } else {
        if (fullForm) {
            fullForm.contact = new AgendaContact();
        }

        if (simpleForm) {
            simpleForm.contact = new Contact();
        }

        if (fullContactCheck) {
            this.fullContactCheck.selected = false;
            this.fullContactCheck.visible = true;
            this.fullContactCheck.includeInLayout = true;
        }
    }

	currentState = 'insert';
}

private function btnSearch_clickHandler(event:MouseEvent):void  {
	currentState = 'search';
}

protected function onFullContactCheckClick(event:MouseEvent):void  {
    if (simpleForm && simpleForm.visible) {
        simpleForm.contact.name = fullForm.contact.name;
        simpleForm.contact.profession = fullForm.contact.profession;
        simpleForm.contact.numbers = fullForm.contact.numbers;
    } else if (fullForm && fullForm.visible) {
        fullForm.contact.name = simpleForm.contact.name;
        fullForm.contact.profession = simpleForm.contact.profession;
        fullForm.contact.numbers = simpleForm.contact.numbers;
    }
}

private function close():void {
	this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
}

private function onContactSave(event:CrudEvent):void {
	this.dispatchEvent(new CrudEvent(CrudEvent.COPY, event.entity));
	close();
}

/**
 * Pega o texto que foi digitado no combobox, retirando o trecho auto-completado
 * que é adicionado.
 */
private function getSearchInputText():String {
	var text:String = contactsCombo.textInput.text;
	
	// Pega a posição do início do texto selecionado
	var textDisplay:IEditableText = contactsCombo.textInput.textDisplay;
	var beginIndex:int = Math.min(textDisplay.selectionActivePosition, textDisplay.selectionAnchorPosition);
	
	if (beginIndex != -1 && beginIndex < text.length - 1) {
		return text.substr(0, beginIndex).toLowerCase();
	} else {
		return text.toLocaleLowerCase();
	}
}
