package tv.snews.anews.view.navigator {

	import flash.events.Event;

	public class SearchEvent extends Event {

		public static const SEARCH_TEXT:String = "searchTextEvent";
		public static const SELECT_FILTER:String = "selectFilterEvent";

		public var searchText:String;
		public var selectedIndex:int;
		public var selectedItem:*;

		public function SearchEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, searchText:String = "", selectedIndex:int = -1, selectedItem:* = null) {
			this.searchText = searchText;
			this.selectedIndex = selectedIndex;
			this.selectedItem = selectedItem;
			super(type, bubbles, cancelable);

		}

		override public function clone():Event {
			return new SearchEvent(type, bubbles, cancelable, searchText, selectedIndex, selectedItem);
		}

	}
}
