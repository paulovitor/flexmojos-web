import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Company;
import tv.snews.anews.domain.Permission;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.CompanyService;
import tv.snews.anews.service.GroupService;
import tv.snews.anews.service.PermissionService;
import tv.snews.anews.utils.*;

[Bindable] private var company:Company = new Company();

private var companyService:CompanyService = CompanyService.getInstance();

private function startUp():void {
	comanyList.addEventListener(CrudEvent.EDIT, onEditCompany);
}

private function onRemove(event:Event):void {
	comanyList.removeEventListener(CrudEvent.EDIT, onEditCompany);
}

//----------------------------
// Events
//----------------------------

private function onEditCompany(event:CrudEvent):void {
	cancel();
	company = ObjectUtil.copy(event.entity) as Company;
	this.currentState = "edition";
	comanyList.enabled = false;
}

private function newCompany():void {
	company = new Company();
	this.currentState = 'edition';
}

private function save():void {
	comanyList.enabled = false;
	if (!Util.isValid(validators)) {
		ANews.showInfo(this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
		comanyList.enabled = true;
	} else {
		checkExistsCompany();
	}
}

private function checkExistsCompany():void {
	companyName.errorString = '';
	hostName.errorString = '';
	hostPort.errorString = '';
	email.errorString = '';
	password.errorString = '';
	companyService.listCompanyByNameHost(company.name, company.host, function(event:ResultEvent):void {
		var companies:ArrayCollection = event.result as ArrayCollection;
		for each (var companyCheck:Company in companies) {
			if (companyCheck != null && company != null && companyCheck.id != company.id) {
				btnSave.enabled = true;
				if(companyCheck.name == company.name) {
					ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'company.msg.nameAlreadyExists'), Info.WARNING);
					companyName.errorString = ResourceManager.getInstance().getString('Bundle', 'company.msg.nameAlreadyExists');
					return;
				}
				if(companyCheck.host == company.host) {
					ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'company.msg.hostAlreadyExists'), Info.WARNING);
					hostName.errorString = ResourceManager.getInstance().getString('Bundle', 'company.msg.hostAlreadyExists');
					return;
				}
			}
		}
		companyService.save(company, onSaveSuccess);
	});
}

private function cancel():void {
	company = new Company();
	this.currentState = "view";
	comanyList.enabled = true;
	companyName.errorString = '';
	hostName.errorString = '';
	hostPort.errorString = '';
	email.errorString = '';
	password.errorString = '';
}

// -----------------------------
// Asynchronous answers
// -----------------------------

private function onSaveSuccess(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
	refresh();
}

// --------------------------
// Helpers.
// --------------------------
private function refresh():void {
	//Restore default values.
	company = new Company();
	this.currentState = "view";
	comanyList.enabled = true;
}
