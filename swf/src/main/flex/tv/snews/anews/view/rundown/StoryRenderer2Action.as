import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.IList;
import mx.controls.Alert;
import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.IFocusManagerComponent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.components.ComboBox;
import spark.components.Group;
import spark.components.Label;
import spark.components.List;
import spark.components.TextInput;

import tv.snews.anews.component.Info;
import tv.snews.anews.component.LabelWithFocus;
import tv.snews.anews.component.TimeInput;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.User;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.view.rundown.StoryExpand;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private static const service:StoryService = StoryService.getInstance();
private static const dateUtil:DateUtil = new DateUtil();
private static const timeFormat:String = "NN:SS";

private var created:Boolean = false;
private var changePending:Boolean = false;
private var canEditHeader:Boolean;
private var canEditApproved:Boolean;
private var story:Story;
private var expander:StoryExpand;

//------------------------------
//	Event Handlers
//------------------------------

private function creationCompleteHandler(event:FlexEvent):void {
	created = true;

	if (changePending) {
		populate();
	}

	configureEventHandlers();
}

private function dataChangeHandler(event:FlexEvent):void {
	if (created) {
		populate();
	} else {
		changePending = true;
	}
}

//------------------------------
//	Methods
//------------------------------

private function populate():void {
	changePending = false;

	story = data as Story;
	if (story != null) {
		canEditApproved = story.approved ? ANews.USER.allow('01071110') : true;
		canEditHeader = story.editingUser == null && ANews.USER.allow('01071108');

		defineVisibility(story);
		defineVisualStyle(story);
		defineEnabledState(story);
		defineToolTip(story);
		displayProperties(story);
	}
}

private function defineVisibility(story:Story):void {
	footerGroup.visible = story.footerSection != null;
//	dragImage.visible = dragImage.includeInLayout = !story.isDragging;
//	lockImage.visible = lockImage.includeInLayout = story.isDragging;
	collapseLabel.visible = DomainCache.mamDevices.length > 0;
	displayingLabel.visible = !story.block.standBy;
	authorLabel.visible = authorLabel.includeInLayout = DomainCache.settings.enableRundownAuthor;
	imageEditorGroup.visible = imageEditorGroup.includeInLayout = DomainCache.settings.enableRundownImageEditor;
}

private function defineVisualStyle(story:Story):void {
	backgroundColor.color = story.getColor();
//	this.alpha = story.cutted ? 0.3: 1.0;
}

public function completeRundownDisplay():void {
    if(story.displaying){
        story.displaying = false;
        story.displayed = true;
    }
    story.block.rundown.displayed = true;
    story.editingUser = null;
    defineEnabledState(story);
}

public function resetRundownDisplay():void {
    story.displaying = false;
    story.block.rundown.displayed = false;
    story.displayed = false;
    defineEnabledState(story);
}

private function defineEnabledState(story:Story):void {
	valuesGroup.enabled = !story.displayed && !story.displaying && !story.block.rundown.displayed;

	setEnabledState(dragLabel, story.editingUser == null && ANews.USER.allow('01071107') && canEditApproved);

	setEnabledState(okLabel, canEditHeader && canEditApproved);
	setEnabledState(approvalLabel, ANews.USER.allow('01071106') && canEditApproved);

	setEnabledState(collapseLabel, story.editingUser == null && ANews.USER.allow('011201'));

	if (displayingLabel.visible) {
		setEnabledState(displayingLabel, ANews.USER.allow('01071113'));
	}

	setEnabledState(deleteLabel, story.editingUser == null && ANews.USER.allow('01071104') && canEditHeader && !story.isDragging);

	setEnabledState(pageLabel, canEditApproved && canEditHeader);
	setEnabledState(slugLabel, canEditApproved && canEditHeader);
	setEnabledState(reporterLabel, canEditApproved && canEditHeader);
	setEnabledState(imageEditorLabel, canEditApproved && canEditHeader);
	setEnabledState(editorLabel, canEditApproved && canEditHeader);
	setEnabledState(expectedLabel, canEditApproved && canEditHeader);
	setEnabledState(vtLabel, canEditApproved && canEditHeader);

	setEnabledState(indexLabel, ANews.USER.allow('01071107') && canEditApproved);
}

private function defineToolTip(story:Story):void {
	const rm:IResourceManager = ResourceManager.getInstance();

	toolTip = function():String {
		if (story.displayed) {
			return rm.getString('Bundle', 'rundown.block.displayed');
		}
		if (story.displaying) {
			return rm.getString('Bundle', 'story.isDisplaying');
		}
		if (story.editingUser) {
			return rm.getString('Bundle', 'story.isEditing', [ story.editingUser.nickname ]);
		}
		return "";
	}();

	var lastChange:String = rm.getString('Bundle', 'rundown.block.author');

	if (toolTip != "") {
		toolTip += "\n\n";
	}

	toolTip += lastChange + ": " + story.author.nickname;
}

private function displayProperties(story:Story):void {
	pageLabel.text = story.page;
	kindLabel.text = kindLabel.toolTip = story.storyKind ? story.storyKind.acronym : "";
	slugLabel.text = story.slug;

	authorLabel.text = story.author.nickname;

	reporterLabel.text = reporterLabel.toolTip = story.reporter ? story.reporter.nickname : story.reportage ? story.reportage.reporter.nickname : story.guideline ? story.guideline.reportersAsStr : "";
	imageEditorLabel.text = imageEditorLabel.toolTip = userNickname(story.imageEditor);
	editorLabel.text = editorLabel.toolTip = userNickname(story.editor);

	okLabel.text = story.ok ? CHECK_SELECTED_ICON : CHECK_ICON;
	approvalLabel.text = story.approved ? CHECK_SELECTED_ICON : CHECK_ICON;

	expectedLabel.text = formatTime(story.expected);
	vtLabel.text = formatTime(story.vt);
	headLabel.text = formatTime(story.head);
	totalLabel.text = formatTime(story.total);

	indexLabel.text = story.globalIndex + "";

	displayingLabel.text = story.displaying ? RADIO_SELECTED_ICON : RADIO_ICON;
}

/*
 * Configura os event handlers em um momento após ao início da rendenização
 * dos componentes.
 *
 * Isso é feito para tentar reduzir o tempo que leva para os componentes
 * serem apresentados na tela, apesar de não dar uma diferença significativa.
 */
private function configureEventHandlers():void {
	setTimeout(function ():void {
		dragLabel.addEventListener(MouseEvent.MOUSE_DOWN, dragImage_mouseDownHandler);
		dragLabel.addEventListener(MouseEvent.MOUSE_UP, dragImage_mouseUpHandler);
		dragLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
		dragLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

        pageLabel.configureLabelEvents(pageLabelEditHandler);
        slugLabel.configureLabelEvents(slugLabelEditHandler);

        reporterLabel.configureLabelEvents(reporterLabelEditHandler);
        imageEditorLabel.configureLabelEvents(imageEditorLabelEditHandler);
        editorLabel.configureLabelEvents(editorLabelEditHandler);

        expectedLabel.configureLabelEvents(expectedLabelEditHandler);
        vtLabel.configureLabelEvents(vtLabelEditHandler);

		okLabel.addEventListener(MouseEvent.CLICK, okLabel_clickHandler);
		okLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
		okLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

		approvalLabel.addEventListener(MouseEvent.CLICK, approvalLabel_clickHandler);
		approvalLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
		approvalLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

		displayingLabel.addEventListener(MouseEvent.CLICK, displayingLabel_clickHandler);
		displayingLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
		displayingLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

        indexLabel.configureLabelEvents(indexLabelEditHandler);
		collapseLabel.addEventListener(MouseEvent.CLICK, collapseLabel_clickHandler);
		collapseLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
		collapseLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

		deleteLabel.addEventListener(MouseEvent.CLICK, deleteLabel_clickHandler);
		deleteLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
		deleteLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);
	}, 500);
}

//------------------------------
//	Fields Event Handlers
//------------------------------

/*
 Drag Enable
 */
private function dragImage_mouseDownHandler(event:MouseEvent):void {
	List(owner).dragEnabled = true;
}

/*
 Drag Disable
 */
private function dragImage_mouseUpHandler(event:MouseEvent):void {
	List(owner).dragEnabled = true;
}

private function pageLabelEditHandler():void {
	var oldPage:String = story.page;
	var pageInput:TextInput = createTextInput("page", oldPage, pageGroup.width, 3);
    configureTextInput(pageGroup, pageInput, 2, pageLabel, null, slugLabel, function ():void {
        var newPage:String = fixPage(pageInput.text);
        if (isValidPage(newPage) && newPage != oldPage) {
            pageLabel.text = newPage; // refresh imediato
            service.updateField(story.id, "page", newPage, null, function(event:FaultEvent):void {
                faultFunction(pageLabel, oldPage, event);
            });
        }

        escapeFunction(pageInput, pageGroup);
    });
}

private function slugLabelEditHandler():void {
	const uppercase:Boolean = DomainCache.settings.editorUpperCase;

	var oldSlug:String = story.slug;
	var slugInput:TextInput = createTextInput("slug", oldSlug, slugGroup.width, 40, uppercase);

    configureTextInput(slugGroup, slugInput, 4, slugLabel, pageLabel, reporterLabel, function ():void {
        var newSlug:String = uppercase ? slugInput.text.toUpperCase() : slugInput.text;
        if (newSlug != oldSlug) {
            slugLabel.text = newSlug; // refresh imediato
            service.updateField(story.id, "slug", newSlug, null, function(event:FaultEvent):void {
                faultFunction(slugLabel, oldSlug, event);
            });
        }

        escapeFunction(slugInput, slugGroup);
    });
}

private function indexLabelEditHandler():void {
	var oldIndex:String = indexLabel.text;

	var indexInput:TextInput = createTextInput("index", oldIndex, indexGroup.width, 3);
	indexInput.restrict = "0-9";
    configureTextInput(indexGroup, indexInput, 14, indexLabel, vtLabel, null, function ():void {
        var newIndex:String = indexInput.text;

        if (newIndex != oldIndex && !StringUtils.isNullOrEmpty(newIndex)) {
            var index:int = parseInt(newIndex);
            if (!isNaN(index) && index < story.block.rundown.maxGlobalIndex) {
                var storyRef:Story = story.block.rundown.getStoryByGlobalIndex(index);

                if (!story.equals(storyRef)) {
                    var dropIndex:int = storyRef.order;
                    if (story.block.equals(storyRef.block) && story.order < storyRef.order) {
                        dropIndex++;
                    }
                    service.moveStory(storyRef.block.id, dropIndex, story.id);
                }
            }
        }
        escapeFunction(indexInput, indexGroup);
    });
}

private function reporterLabelEditHandler():void {
	var oldReporter:User = story.reporter;
	var dataProvider:IList = DomainCache.reporters;
	var reporterCombo:ComboBox = createComboBox(dataProvider, oldReporter, reporterGroup.width);

    configureComboBox(reporterGroup, reporterCombo, 5, reporterLabel, slugLabel, imageEditorGroup.visible ? imageEditorLabel : editorLabel, oldReporter, "reporter");
}

private function imageEditorLabelEditHandler():void {
	var oldImageEditor:User = story.imageEditor;
	var dataProvider:IList = DomainCache.programCache.imageEditors(story.program);
	var imageEditorCombo:ComboBox = createComboBox(dataProvider, oldImageEditor, imageEditorGroup.width);

    configureComboBox(imageEditorGroup, imageEditorCombo, 6, imageEditorLabel, reporterLabel, editorLabel, oldImageEditor, "image_editor");
}

private function editorLabelEditHandler():void {
	var oldEditor:User = story.editor;
	var dataProvider:IList = DomainCache.programCache.editors(story.program);
	var editorCombo:ComboBox = createComboBox(dataProvider, oldEditor, editorGroup.width);

    configureComboBox(editorGroup, editorCombo, 7, editorLabel, imageEditorGroup.visible ? imageEditorLabel : reporterLabel, expectedLabel, oldEditor, "editor");
}

private function expectedLabelEditHandler():void {
	var oldExpected:Date = story.expected;
	var expectedInput:TimeInput = createTimeInput(story.expected, expectedGroup.width);

    configureTimeInput(expectedGroup, expectedInput, 9, expectedLabel, editorLabel, vtLabel, oldExpected, "expected");
}

private function vtLabelEditHandler():void {
	var oldVt:Date = story.vt;
	var vtInput:TimeInput = createTimeInput(story.vt, vtGroup.width);

    configureTimeInput(vtGroup, vtInput, 10, vtLabel, expectedLabel, indexLabel, oldVt, "vt");
}

private function okLabel_clickHandler(event:MouseEvent):void {
	setEnabledState(okLabel, false);

	service.updateField(story.id, "ok", !story.ok,
			function (event:ResultEvent):void {
				setEnabledState(okLabel, true);
			}, function (event:FaultEvent):void {
				faultEnabledFunction(okLabel, event);
			}
	);
}

private function approvalLabel_clickHandler(event:MouseEvent):void {
	setEnabledState(approvalLabel, false);

	service.updateField(story.id, "approved", !story.approved,
			function (event:ResultEvent):void {
				setEnabledState(approvalLabel, true);
			}, function (event:FaultEvent):void {
				faultEnabledFunction(approvalLabel, event);
			}
	);
}

private function displayingLabel_clickHandler(event:MouseEvent):void {
	setEnabledState(displayingLabel, false);

	service.updateField(story.id, "displaying", !story.displaying,
			function (event:ResultEvent):void {
				setEnabledState(displayingLabel, true);
			}, function (event:FaultEvent):void {
				faultEnabledFunction(displayingLabel, event);
			}
	);
}

private function collapseLabel_clickHandler(event:MouseEvent):void {
	if (expander) {
		contentGroup.removeElement(expander);
		expander = null;

		collapseLabel.text = EXPAND_ICON;
	} else {
		expander = new StoryExpand();
		expander.story = story;
		expander.enabled = story.editingUser == null && !story.displayed && !story.displaying;

		contentGroup.addElementAt(expander, 1);

		collapseLabel.text = COLLAPSE_ICON;
	}
}

private function iconLabel_mouseOverHandler(event:MouseEvent):void {
	event.target.setStyle("color", OVER_COLOR);
}

private function iconLabel_mouseOutHandler(event:MouseEvent):void {
	event.target.setStyle("color", NORMAL_COLOR);
}

private function deleteLabel_clickHandler(event:MouseEvent):void {
	deleteLabel.enabled = false;

	var bundle:IResourceManager = ResourceManager.getInstance();

	Alert.show(
			bundle.getString('Bundle', 'story.msg.confirmDelete'),
			bundle.getString('Bundle', 'defaults.msg.confirmation'),
					Alert.YES | Alert.NO, null,

			function (event:CloseEvent):void {
				if (event.detail == Alert.YES) {
					if (story.displayed || story.displaying || story.block.rundown.displayed) {
						ANews.showInfo(bundle.getString('Bundle', 'rundown.block.displayed'), Info.WARNING);
						deleteLabel.enabled = true;
					} else {
						service.removeStory(story.id,
								function (event:ResultEvent):void {
									ANews.showInfo(bundle.getString('Bundle', 'story.msg.deleteSuccess'), Info.INFO);
								}
						);
					}
				}
			}
	);
}

//------------------------------
//	Helpers
//------------------------------

private static function userNickname(user:User):String {
	return user ? user.nickname : "";
}

private static function formatTime(value:Date):String {
	return value ? dateUtil.toString(value, timeFormat) : "00:00";
}

private function createTextInput(id:String, value:String, width:int, maxChars:int, spelling:Boolean=false):TextInput {
	var input:TextInput = new TextInput();
	input.setStyle("typographicCase", spelling ? "uppercase" : "default");
	input.setStyle("fontSize", 12);
	input.setStyle("borderVisible", false);
	input.setStyle("paddingTop", 2);
	input.setStyle("paddingBottom", 2);
	input.setStyle("paddingLeft", 2);
	input.setStyle("paddingRight", 2);
    input.tabEnabled = false;
    input.tabFocusEnabled = false;
	input.width = width;
	input.maxChars = maxChars;
	input.text = value;
	input.id = id;
	return input;
}

private function createComboBox(dataProvider:IList, selectedItem:User, width:int):ComboBox {
	var comboBox:ComboBox = new ComboBox();
	comboBox.setStyle("fontSize", 12);
	comboBox.setStyle("borderVisible", false);
	comboBox.setStyle("paddingTop", 2);
	comboBox.setStyle("paddingBottom", 2);
	comboBox.setStyle("paddingLeft", 2);
	comboBox.setStyle("paddingRight", 2);
    comboBox.tabEnabled = false;
    comboBox.tabFocusEnabled = false;
	comboBox.width = width;
	comboBox.labelField = "nickname";
	comboBox.dataProvider = dataProvider;
	comboBox.selectedItem = findItem(dataProvider, selectedItem);
//			comboBox.prompt = resourceManager.getString('Bundle', 'defaults.selectPrompt');
	return comboBox;
}

private function createTimeInput(date:Date, width:int):TimeInput {
	var input:TimeInput = new TimeInput();
	input.setStyle("fontSize", 12);
	input.setStyle("borderVisible", false);
	input.setStyle("paddingTop", 2);
	input.setStyle("paddingBottom", 2);
	input.setStyle("paddingLeft", 2);
	input.setStyle("paddingRight", 2);
    input.tabEnabled = false;
    input.tabFocusEnabled = false;
	input.format = "NN:SS";
	input.date = date;
	input.width = width;
	return input;
}

private function configureInputEvents(input:TextInput, beforeField:UIComponent, nextField:UIComponent, callback:Function, escape:Function):void {
    const focusOutHandler:Function = function (event:FocusEvent):void {
        callback();
        input.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
        input.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
        event.stopPropagation();
    };

    const keyDownHandler:Function = function (event:KeyboardEvent):void {
        if (event.keyCode == Keyboard.ESCAPE) {
            escape();
            input.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
            input.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
            event.stopPropagation();
        }
        if (event.keyCode == Keyboard.ENTER || event.keyCode == Keyboard.TAB) {
            callback();
            if (event.keyCode == Keyboard.TAB && event.shiftKey) {
                if (beforeField) {
                    setFocusOn(beforeField);
                } else {
                    escape();
                }
            } else {
                if (nextField) {
                    setFocusOn(nextField);
                } else {
                    escape();
                }
            }
            input.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
            input.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
            event.stopPropagation();
        }
    };

    input.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
    input.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
}

private function findItem(dataProvider:IList, user:User):User {
	if (user == null) {
		return null;
	}
	for each (var current:User in dataProvider) {
		if (current.equals(user)) {
			return current;
		}
	}
	return user;
}

private function fixPage(value:String):String {
	value = value.toUpperCase();
	if ("0123456789".indexOf(value.charAt(0)) != -1) {
		if (value.length >= 2) {
			if ("0123456789".indexOf(value.charAt(1)) == -1) {
				value = "0" + value.substring(0, 2);
			}
		} else {
			value = "0" + value;
		}
	}
	return value;
}

private function isValidPage(value:String):Boolean {
	return /^\d{2,3}[A-Z]?$/.test(value);
}

private function setEnabledState(label:Label, enabled:Boolean):void {
	label.mouseEnabled = label.enabled = enabled;
}

private function configureComboBox(comboBoxGroup:Group, comboBox:ComboBox, index:int, comboBoxLabel:LabelWithFocus, beforeField:UIComponent, nextField:UIComponent, oldUser:User, field:String):void {
    comboBoxGroup.visible = comboBoxGroup.includeInLayout = false;
    valuesGroup.addElementAt(comboBox, index);

    setFocusOn(comboBox.textInput);

    configureComboBoxEvents(comboBox, beforeField, nextField, function ():void {
                var newUser:User = comboBox.selectedItem as User;
                var comboBoxText:String = comboBox.textInput.text;
                var verifyComboBoxLabel:Boolean = comboBoxText == "" || !(comboBoxText == comboBoxLabel.text);
                if ((newUser && !newUser.equals(oldUser)) || (!newUser && oldUser && verifyComboBoxLabel)) {
                    comboBoxLabel.text = newUser ? newUser.nickname : ""; // refresh imediato
                    service.updateField(story.id, field, newUser, null, function(event:FaultEvent):void {
                        faultFunction(comboBoxLabel, oldUser ? oldUser.nickname : "", event);
                    });
                } else {
                    if(oldUser)
                        comboBoxLabel.text = oldUser.nickname;
                }
                escapeFunction(comboBox, comboBoxGroup);
            },
            function ():void {
                escapeFunction(comboBox, comboBoxGroup, comboBoxLabel);
            }
    );
}

private function configureTimeInput(timeGroup:Group, timeInput:TimeInput, index:int, timeLabel:LabelWithFocus, beforeField:UIComponent, nextField:UIComponent, oldDate:Date, field:String):void {
    configureTextInput(timeGroup, timeInput, index, timeLabel, beforeField, nextField, function ():void {
        var newDate:Date = timeInput.date;
        var newStr:String = newDate ? dateUtil.minuteSecondToString(newDate) : "00:00";
        var oldStr:String = oldDate ? dateUtil.minuteSecondToString(oldDate) : "00:00";

        if (newStr != oldStr) {
            timeLabel.text = newStr;
            service.updateField(story.id, field, newDate, null, function(event:FaultEvent):void {
                faultFunction(timeLabel, oldStr, event);
            });
        }

        escapeFunction(timeInput, timeGroup);
    });
}

private function configureTextInput(textInputGroup:Group, textInput:TextInput, index:int, label:UIComponent, beforeField:UIComponent, nextField:UIComponent, callback:Function):void {
    textInputGroup.visible = textInputGroup.includeInLayout = false;
    valuesGroup.addElementAt(textInput, index);

    setFocusOn(textInput);

    configureInputEvents(textInput, beforeField, nextField, callback, function ():void {
        escapeFunction(textInput, textInputGroup, label);
    });
}

private function configureComboBoxEvents(comboBox:ComboBox, beforeField:UIComponent, nextField:UIComponent, callback:Function, escape:Function):void {
    const focusOutHandlerComboBox:Function = function (event:FocusEvent):void {
        callback();
        comboBox.textInput.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
        comboBox.textInput.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
        event.stopPropagation();
    };

    const keyDownHandlerComboBox:Function = function (event:KeyboardEvent):void {
        if (event.keyCode == Keyboard.ESCAPE) {
            escape();
            comboBox.textInput.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
            comboBox.textInput.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
            event.stopPropagation();
        }
        if (event.keyCode == Keyboard.ENTER || event.keyCode == Keyboard.TAB) {
            callback();
            if (event.keyCode == Keyboard.TAB && event.shiftKey) {
                setFocusOn(beforeField);
            } else {
                setFocusOn(nextField);
            }
            comboBox.textInput.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
            comboBox.textInput.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
            event.stopPropagation();
        }
    };

    comboBox.textInput.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
    comboBox.textInput.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
}

private function escapeFunction(component:UIComponent, group:Group, label:UIComponent = null):void {
    if (valuesGroup.containsElement(component)) {
        valuesGroup.removeElement(component);
    }
    group.visible = group.includeInLayout = true;
    if (label)
        setFocusOn(label);
}

private function faultFunction(label:LabelWithFocus, oldValue:String, event:FaultEvent):void {
    label.text = oldValue;
    service.faultHandler(event);
}

private function faultEnabledFunction(label:Label, event:FaultEvent):void {
    setEnabledState(label, true);
    service.faultHandler(event);
}

public function setFocusInFirstElementOfItemRenderer():void {
    callLater(function ():void {
        focusManager.setFocus(pageLabel);
        focusManager.showFocus();
    });
}

public function setFocusInLastElementOfItemRenderer():void {
    callLater(function ():void {
        focusManager.setFocus(indexLabel);
        focusManager.showFocus();
    });
}

private function setFocusOn(component:UIComponent):void {
    callLater(function ():void {
        focusManager.setFocus(component as IFocusManagerComponent);
    });
}
