import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.events.CloseEvent;
import mx.managers.PopUpManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.service.ChatService;
import tv.snews.anews.utils.DomainCache;

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Chat")]
private var chats:ArrayCollection = new ArrayCollection();

private function search():void {
	ChatService.getInstance().listByDate(dtFieldHistory.selectedDate, ANews.USER.id, onSearch);
}

private function closeWndow(event:Event = null):void {
	if (event == null) {
		this.dispatchEvent(new CloseEvent(CloseEvent.CLOSE));
	}
	PopUpManager.removePopUp(this);
}

//-------------------------
// Asynchronous answers
//-------------------------
private function onSearch(event:ResultEvent):void {
	chats = event.result as ArrayCollection;
}
