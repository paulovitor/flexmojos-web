package tv.snews.anews.view.navigator {

	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	import spark.components.supportClasses.SkinnableComponent;


	public class MyTextInput extends SkinnableComponent {

		public var dropdown:ScopedDropDown;

		public function MyTextInput() {
			super();
			setStyle("skinClass", NavTextInputItemSkin)
		}

		override protected function getCurrentSkinState():String {
			return super.getCurrentSkinState();
		}

		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);
		}

		override protected function partRemoved(partName:String, instance:Object):void {
			super.partRemoved(partName, instance);
		}

		//EVENTS
		public function rollOver(event:MouseEvent):void {
//			dropdown.closeDropDown(false);					
			//dropdown.dataProvider = this.menu;
//			dropdown.stayOpen = true;
//			dropdown.selectedIndex = 0;
//			dropdown.addEventListener(ListEvent.CHANGE, itemSelectHandler);
//			dropdown.x = globalToLocal(parent.localToGlobal(new Point(this.x))).x
//			callLater(dropdown.openDropDown);
		}


		public function keyDown(event:KeyboardEvent):void {
		}

		private function itemSelectHandler(event:Event):void {

		}

		public function rollOut(event:MouseEvent):void {
			Mouse.cursor = MouseCursor.AUTO;
		}

		public function click(event:MouseEvent):void {

		}
	}
}
