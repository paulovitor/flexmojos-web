import flash.external.ExternalInterface;
import flash.system.Security;

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import org.osmf.events.MediaPlayerStateChangeEvent;

import org.osmf.layout.ScaleMode;
import org.osmf.media.MediaPlayerState;
import org.osmf.net.StreamType;

import spark.components.Image;
import spark.components.VideoDisplay;
import spark.components.VideoPlayer;
import spark.components.mediaClasses.DynamicStreamingVideoItem;
import spark.components.mediaClasses.DynamicStreamingVideoSource;

import tv.snews.anews.component.Info;

import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.MosObjectType;
import tv.snews.anews.domain.PlayerMedia;
import tv.snews.anews.domain.Settings;
import tv.snews.anews.event.MediaCenterPlayEvent;
import tv.snews.anews.utils.DomainCache;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const dateUtil:DateUtil = new DateUtil();
private const bundle:IResourceManager = ResourceManager.getInstance();
private const httpProtocol:String = "http://";
private var settings:Settings = DomainCache.settings;
[Bindable] public var playerMedia:PlayerMedia = new PlayerMedia();
[Bindable] public var enableVisible:Boolean = false;
[Bindable] public var canNotBeDisplayed:Boolean = false;
[Bindable] public var mediType:String;
[Bindable] public var mediaUrlRun:String;
[Embed(source = "/assets/no_image.jpg")]
private var NoImage:Class;
public var imgPlayer:VideoDisplay;
public var mediaPlayer:VideoPlayer;
public var audioPlayer:VideoPlayer;
public var img:Image;
private var videoStr:String = null;

public function start(event:MediaCenterPlayEvent):void {
    playerMedia = event.playerMedia;
    if(validURL(playerMedia.url)) {
        canNotBeDisplayed = false;
        enableVisible = true;
        this.callLater(function ():void {
            resize();
        });
        playerMedia = event.playerMedia;
        loadVT();
        if (ExternalInterface.available) {
            ExternalInterface.addCallback("notifyVideoLoad", loadVideo);
        }
    } else {
        ANews.showInfo(bundle.getString("Bundle", "anewsPlay.msg.invalidMedia"), Info.WARNING);
        return;
    }
}

/*
 * Gera tag html do video.
 */
protected function getSource(src:String, mimeType:String, codecs:String):String {
    return  "<video id=\"videoPlayer\" width=\"100%\" height=\"100%\" style=\"top: 0px; bottom: 0px; left: 0px; right: 10px;\" controls autoplay preload=\"true\">" +
            "<source type=\""+mimeType+"\" codecs=\""+ codecs +"\" src=\"" + src + "\">" +
            "</video>";
}

/*
 * Verifica qual play chamar dependendo do tipo de mídia e browser.
 */
protected function loadVT():void {
    var mediaUrlBase:String = null;
    verifyServerStreaming();
    mediaUrlBase = settings.host + settings.rootDirectory;
    mediType = canNotBeDisplayed ? "NO_DISPLAYED" : playerMedia.type;

	var videoSource:DynamicStreamingVideoSource;
	var streamItems:Vector.<DynamicStreamingVideoItem>;
	var videoItem:DynamicStreamingVideoItem;

    switch (mediType) {
        case "AUDIO":
            this.currentState = "flash";
            audioPlayer = new VideoPlayer();
            audioPlayer.scaleMode = ScaleMode.STRETCH;
            audioPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, audioStateChangeHandler);
            if(verifyProtocolRTMP(getUrlBase(mediaUrlBase))) {
                mediaUrlRun = getUrlBase(mediaUrlBase) + getFileExtension() + ":" + getDirectoryRoot() + getFileNameWithExtension();
                Security.allowDomain(mediaUrlRun);
							videoSource = new DynamicStreamingVideoSource();
                videoSource.host = getUrlBase(mediaUrlBase);
                videoSource.streamType = StreamType.RECORDED;
							streamItems = new Vector.<DynamicStreamingVideoItem>();
							videoItem = new DynamicStreamingVideoItem();
                videoItem.streamName = getFileExtension() + ":" + getDirectoryRoot() + getFileNameWithExtension();
                streamItems.push(videoItem);
                videoSource.streamItems = streamItems;
                audioPlayer.source = videoSource;
            } else if(verifyProtocolHTTPorHTTPS(getUrlBase(mediaUrlBase))) {
                mediaUrlRun = getUrlBase(mediaUrlBase) + getDirectoryRoot().substring(1, getDirectoryRoot().length) + getFileNameWithExtension();
                Security.allowDomain(mediaUrlRun);
                audioPlayer.source = mediaUrlRun;
            }
            audioPlayer.autoRewind = true;
            audioPlayer.autoPlay = true;
            audioPlayerContent.addElement(audioPlayer);
            flashContainer.height = 45;
            audioPlayer.height = 24;
            audioPlayer.width = ((this.width / 2.8) * (1080/1920)) * (1920/1080);
            break;
        case "VIDEO":
//            if (new Boolean(ExternalInterface.call("function(){return html5VideoCompatibility()}"))
//                    && verifyVideoFormat(httpProtocol + mediaUrlBase + getFileNameWithExtension())) {
//                // Se Browser suportar HTML5
//                this.currentState = "html5";
//                Security.allowDomain(httpProtocol + mediaUrlBase + getFileNameWithExtension());
//                anewsPlayIFrame.callIFrameFunction('reload');
//            } else {
            this.currentState = "flash";
            mediaPlayer = new VideoPlayer();
            mediaPlayer.scaleMode = ScaleMode.STRETCH;
            mediaPlayer.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, videoStateChangeHandler);
            if(verifyProtocolRTMP(getUrlBase(mediaUrlBase))) {
                mediaUrlRun = getUrlBase(mediaUrlBase) + getFileExtension() + ":" + getDirectoryRoot() + getFileNameWithExtension();
                Security.allowDomain(mediaUrlRun);
							videoSource = new DynamicStreamingVideoSource();
                videoSource.host = getUrlBase(mediaUrlBase);
                videoSource.streamType = StreamType.RECORDED;
							streamItems = new Vector.<DynamicStreamingVideoItem>();
							videoItem = new DynamicStreamingVideoItem();
                videoItem.streamName = getFileExtension() + ":" + getDirectoryRoot() + getFileNameWithExtension();
                streamItems.push(videoItem);
                videoSource.streamItems = streamItems;
                mediaPlayer.source = videoSource;
            } else if(verifyProtocolHTTPorHTTPS(getUrlBase(mediaUrlBase))) {
                mediaUrlRun = getUrlBase(mediaUrlBase) + getDirectoryRoot().substring(1, getDirectoryRoot().length) + getFileNameWithExtension();
                Security.allowDomain(mediaUrlRun);
                mediaPlayer.source = mediaUrlRun;
            }
            mediaPlayer.autoRewind = true;
            mediaPlayer.autoPlay = true;
            videoPlayerContent.addElement(mediaPlayer);
            mediaPlayer.height = (this.width / 2.8) * (1080/1920);
            mediaPlayer.width = mediaPlayer.height * (1920/1080);
//            }
            break;
        case "STILL":
            this.currentState = "flash";
            img = new Image();
            mediaUrlRun = httpProtocol + mediaUrlBase + getDirectoryRoot().substring(1, getDirectoryRoot().length) + getFileNameWithExtension();
            Security.allowDomain(mediaUrlRun);
            img.source = mediaUrlRun;
            img.scaleMode = ScaleMode.STRETCH;
            img.addEventListener(MediaPlayerStateChangeEvent.MEDIA_PLAYER_STATE_CHANGE, imgStateChangeHandler);
            imgContent.addElement(img);
            img.height = (this.width / 2.8) * (1080/1920);
            img.width = img.height * (1920/1080);
            break;
        default:
            canNotBeDisplayed = true;
            mediType = "NO_DISPLAYED";
            img = new Image();
            img.scaleMode = ScaleMode.STRETCH;
            img.source = "@Embed('/assets/no_image.jpg')";
            imgContent.addElement(img);
            img.height = (this.width / 2.8) * (1080/1920);
            img.width = img.height * (1920/1080);
            break;
    }
}

/*
 * Verifica qual stop chamar dependendo do tipo de mídia e browser.
 */
public function stop(close:Boolean = false):void {
    var type:String = canNotBeDisplayed ? "NO_DISPLAYED" : playerMedia.type;
    switch (type) {
        case "VIDEO":
            if (anewsPlayIFrame) {
                stopVideoIframe();
            } else {
                if(mediaPlayer) {
                    mediaPlayer.stop();
                }
                mediaPlayer = null;
                videoPlayerContent.removeAllElements();
            }
            break;
        case "AUDIO":
            if(audioPlayer) {
                audioPlayer.stop();
            }
            audioPlayer = null;
            audioPlayerContent.removeAllElements();
            break;
        case "STILL":
            img = null;
            imgContent.removeAllElements();
            break;
        default:
            img = null;
            imgContent.removeAllElements();
            break;
    }
    if(close) {
        onClose();
    }
}

/*
 * Dimenciona o container da mídia.
 */
private function resize():void {
    if(anewsPlayIFrame) {
        anewsPlayIFrame.width = anewsPlayIFrame.height * (1920/1080);
        anewsPlayIFrame.height = this.width / 2.6 * 1080/1920;
    }
    if(flashContainer) {
        flashContainer.width = flashContainer.height * (1920/1080);
        flashContainer.height = (this.width / 2.6) * 1080/1920;
        if(audioPlayer) {
            flashContainer.height = 45;
            flashContainer.width = flashContainer.height * (1920/1080);
            audioPlayer.height = 24;
            audioPlayer.width = ((this.width / 2.8) * (1080/1920)) * (1920/1080);
        }
        if(mediaPlayer) {
            mediaPlayer.height = (this.width / 2.8) * (1080/1920);
            mediaPlayer.width = mediaPlayer.height * (1920/1080);
        }
        if(img) {
            img.height = (this.width / 2.8) * (1080/1920);
            img.width = img.height * (1920/1080);
        }
    }
}

/*
 * Dispara a chamada para fechar o container da midia.
 */
private function onClose():void {
    enableVisible = false;
    canNotBeDisplayed = false;
}

/*
 * Dispara a interrupção da execução do video.
 */
private function stopVideoIframe():void {
    anewsPlayIFrame.callIFrameFunction('stopVideo');
}

/*
 * Verifica os formatos de video e carrega a string se compatível.
 */
private function verifyVideoFormat(path:String):Boolean {
    var extension:String = getFileExtension();
    if(extension == "mp4") {
        videoStr = getSource(path, "video/mp4", "avc1.42E01E, mp4a.40.2");
    }
    if(extension == "ogg") {
        videoStr = getSource(path, "video/ogg", "theora, vorbis");
    }
    if(extension == "webm") {
        videoStr = getSource(path, "video/webm", "vp8, vorbis");
    }
    return extension == "mp4" || extension == "ogg" || extension == "webm";
}

/*
 * Função de callback para ler a string do video e chamar a função
 * que ler o video após o carregar do html do iframe.
 */
private function loadVideo():void {
    anewsPlayIFrame.callIFrameFunction('loadVideo', [ videoStr ]);
}

/*
 * Verifica o protocolo da url do video se é rtmp, rtmpe,
 * rtmpt, rtmpte ou rtmps.
 */
private function verifyProtocolRTMP(path:String):Boolean {
    var pattern:RegExp = /(rtmp|rtmpe|rtmpt|rtmpte|rtmps):\/\//;
    var proxyPath:String = path;
    var verifyPattern:Array = null;
    if (proxyPath != null && proxyPath.length > 0) {
        verifyPattern = pattern.exec(proxyPath);
        if (verifyPattern != null && verifyPattern[0]) {
            return true;
        }
    }
    return false;
}

/*
 * Verifica e altera dados do settings local para a execuão
 * transparente do video que vem do proxy do mosMedia quando
 * não ativado o servidor de streaming.
 */
protected function verifyServerStreaming():void {
    var url:String = playerMedia.url;
    var indexProtocolEnd:int = 0;
    var indexHostBegin:int = 0;
    var urlVerify:String = "";
    var indexHostEnd:int = 0;
    var indexRootDirectoryBegin:int = 0;
    var indexRootDirectoryEnd:int = 0;
    if (!settings.serverStreaming) {
        if(url.indexOf("//") != -1) {
            indexProtocolEnd = url.indexOf("//") + 2;
            indexHostBegin = indexProtocolEnd;
            urlVerify = url.substring(indexHostBegin, url.length);
            indexHostEnd = indexHostBegin + urlVerify.indexOf("/");
            indexRootDirectoryBegin = indexHostEnd;
            urlVerify = url.substring(indexHostEnd + 1, url.length);
            indexRootDirectoryEnd = indexRootDirectoryBegin + urlVerify.indexOf("/") + 2;
        } else if(url.indexOf("\\\\") != -1) {
            indexProtocolEnd = url.indexOf("\\\\") + 2;
            indexHostBegin = indexProtocolEnd;
            urlVerify = url.substring(indexHostBegin, url.length);
            indexHostEnd = indexHostBegin + urlVerify.indexOf("\\");
            indexRootDirectoryBegin = indexHostEnd;
            urlVerify = url.substring(indexHostEnd + 1, url.length);
            indexRootDirectoryEnd = indexRootDirectoryBegin + urlVerify.indexOf("\\") + 2;
        }
        var protocol:String = (url.substring(0, indexProtocolEnd)).replace(/\\/g, "/");
        if(!(verifyProtocolRTMP(protocol) || verifyProtocolHTTPorHTTPS(protocol))) {
            settings.protocol = httpProtocol;
        } else {
            settings.protocol = protocol;
        }
        settings.host = (url.substring(indexHostBegin, indexHostEnd)).replace(/\\/g, "/");
        settings.rootDirectory = (url.substring(indexRootDirectoryBegin, indexRootDirectoryEnd)).replace(/\\/g, "/");
    }
}

private function validURL(url:String):Boolean {
    return url != null && url.length > 0;
}

/*
 * Verifica o protocolo da url do arquivo se é http ou https.
 */
private function verifyProtocolHTTPorHTTPS(path:String):Boolean {
    var pattern:RegExp = /(http|https):\/\//;
    var proxyPath:String = path;
    var verifyPattern:Array = null;
    if (proxyPath != null && proxyPath.length > 0) {
        verifyPattern = pattern.exec(proxyPath);
        if (verifyPattern != null && verifyPattern[0]) {
            return true;
        }
    }
    return false;
}

/*
 * Retorna a url base do arquivo.
 */
private function getUrlBase(path:String):String {
    var hrlPath:String = path.substring(0, path.lastIndexOf("/") +1);
    return settings.protocol + hrlPath;
}

/*
 * Retorna o diretório base do arquivo.
 */
private function getDirectoryRoot():String {
    var url:String = playerMedia.url;
    var directoryRoot:String = "";
    var indexProtocolEnd:int = 0;
    var indexHostBegin:int = 0;
    var urlVerify:String = "";
    var indexHostEnd:int = 0;
    var indexRootDirectoryBegin:int = 0;
    var indexRootDirectoryEnd:int = 0;
    if(url.indexOf("//") != -1) {
        indexProtocolEnd = url.indexOf("//") + 2;
        indexHostBegin = indexProtocolEnd;
        urlVerify = url.substring(indexHostBegin, url.length);
        indexHostEnd = indexHostBegin + urlVerify.indexOf("/") + 1;
        urlVerify = url.substring(indexHostEnd, url.length);
        indexRootDirectoryBegin = indexHostEnd + urlVerify.indexOf("/");
        directoryRoot = url.substring(indexRootDirectoryBegin, url.lastIndexOf("/") + 1);
    } else if(url.indexOf("\\\\") != -1) {
        indexProtocolEnd = url.indexOf("\\\\") + 2;
        indexHostBegin = indexProtocolEnd;
        urlVerify = url.substring(indexHostBegin, url.length);
        indexHostEnd = indexHostBegin + urlVerify.indexOf("\\") + 1;
        urlVerify = url.substring(indexHostEnd, url.length);
        indexRootDirectoryBegin = indexHostEnd + urlVerify.indexOf("\\");
        directoryRoot = url.substring(indexRootDirectoryBegin, url.lastIndexOf("\\") + 1);
        directoryRoot = directoryRoot.replace(/\\/g, "/");
    }
    return directoryRoot;
}

/*
 * Retorna extensão do arquivo.
 */
private function getFileExtension():String {
    var fileExtension:String = "";
    var path:String = playerMedia.url;
    fileExtension = path.substring(path.lastIndexOf(".") + 1, path.length);
    return fileExtension;
}

/*
 * Retorna o nome do arquivo.
 */
private function getFileName():String {
    var fileFullName:String = "";
    var path:String = playerMedia.url;
    fileFullName = path.lastIndexOf("/") ? path.substring(path.lastIndexOf("/") +1, path.lastIndexOf(".")) : path.substring(path.lastIndexOf("\\") +1, path.lastIndexOf("."));
    return fileFullName;
}

/*
 * Retorna o nome do arquivo e extensão.
 */
private function getFileNameWithExtension():String {
    var fileFullName:String = "";
    var path:String = playerMedia.url;
    fileFullName = path.lastIndexOf("/") != -1 ? path.substring(path.lastIndexOf("/") + 1, path.length) : path.substring(path.lastIndexOf("\\") + 1, path.length);
    return fileFullName;
}

/*
 * Handler que monitora mudanças na execução da imagem.
 */
protected function imgStateChangeHandler(evt:MediaPlayerStateChangeEvent):void {
    switch (evt.state) {
        case MediaPlayerState.LOADING:
            imgPlayer.errorString = "";
            break;
        case MediaPlayerState.PLAYBACK_ERROR:
            imgPlayer.errorString = bundle.getString("Bundle", "anewsPlay.msg.unableLoadMedia", [mediaUrlRun]);
            break;
    }
}

/*
 * Handler que monitora mudanças na execução do video.
 */
protected function videoStateChangeHandler(evt:MediaPlayerStateChangeEvent):void {
    switch (evt.state) {
        case MediaPlayerState.LOADING:
            mediaPlayer.errorString = "";
            break;
        case MediaPlayerState.PLAYBACK_ERROR:
            mediaPlayer.errorString = bundle.getString("Bundle", "anewsPlay.msg.unableLoadMedia", [mediaUrlRun]);
            break;
    }
}

/*
 * Handler que monitora mudanças na execução do audio.
 */
protected function audioStateChangeHandler(evt:MediaPlayerStateChangeEvent):void {
    switch (evt.state) {
        case MediaPlayerState.LOADING:
            audioPlayer.errorString = "";
            break;
        case MediaPlayerState.PLAYBACK_ERROR:
            audioPlayer.errorString = bundle.getString("Bundle", "anewsPlay.msg.unableLoadMedia", [mediaUrlRun]);
            break;
    }
}
