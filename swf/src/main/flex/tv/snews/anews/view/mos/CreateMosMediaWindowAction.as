import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;

import tv.snews.anews.domain.DeviceType;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.MosObjectType;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.DeviceService;
import tv.snews.anews.service.MediaCenterService;
import tv.snews.anews.utils.Util;

private const deviceService:DeviceService = DeviceService.getInstance();
private const mediaCenterService:MediaCenterService = MediaCenterService.getInstance();

[Bindable] public var mosMedia:MosMedia;
[Bindable] public var devicePlayout:MosDevice;

[Bindable]
[ArrayElementType("tv.snews.anews.domain.MosDevice")]
[Bindable] private var devicePlayoutCB:ArrayCollection = new ArrayCollection();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.MosDevice")]
[Bindable] private var devicesMedia:ArrayCollection = new ArrayCollection();

[Bindable] private var data:Array = new Array();


private function startUp(event:FlexEvent):void {
    deviceService.findAllMosDeviceByType(DeviceType.MAM,
            function(event:ResultEvent):void {
                devicesMedia = event.result as ArrayCollection;
                if(devicesMedia.length > 0 ) cbDeviceMedia.selectedIndex = 0;
            }
    );
    devicePlayoutCB.addItem(devicePlayout);
    cbPlayout.selectedIndex = 0;
}

private function createMosMedia(event:MouseEvent):void {
	if(Util.isValid(validators)) {
        mosMedia.device = cbDeviceMedia.selectedItem;
        mosMedia.type = cbMosObjectType.selectedItem.name;

        data.mosMedia = mosMedia;
        data.playout = cbPlayout.selectedItem;
        data.channel = cbChannels.selectedItem;

        mediaCenterService.canCreateMosObj(mosMedia.slug, mosMedia.device as MosDevice, onVerificationSuccess);
	} else {
        ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
		return;
	}
}

private function onVerificationSuccess(event:ResultEvent):void {
    var result:Boolean = event.result as Boolean;
    if(result){
        this.dispatchEvent(new CrudEvent(CrudEvent.CREATE, data));
        closeWindow();
    }else {
        ANews.showInfo(this.resourceManager.getString('Bundle', 'mediaCenter.msg.mosMediaSlugAlreadyUsed'), Info.WARNING);
    }
}

private function closeWindow():void {
	var closeEvent:CloseEvent = new CloseEvent(CloseEvent.CLOSE)
	this.dispatchEvent(closeEvent);
}

private function removePopUp(event:Event):void {
	PopUpManager.removePopUp(this);
}
