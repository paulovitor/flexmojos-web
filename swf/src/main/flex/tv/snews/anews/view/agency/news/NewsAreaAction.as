import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.containers.TabNavigator;
import mx.controls.Alert;
import mx.core.DragSource;
import mx.core.FlexGlobals;
import mx.core.IFactory;
import mx.core.IFlexDisplayObject;
import mx.core.UIComponent;
import mx.events.CalendarLayoutChangeEvent;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.Group;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.IGridItemRenderer;
import spark.events.GridEvent;
import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.DocumentState;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Guide;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.News;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.NewsParams;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.service.NewsService;
import tv.snews.anews.utils.*;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.agency.news.NewsArea;
import tv.snews.anews.view.agenda.SelectContactWindow;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.validator.URLValidator;

[Bindable] private var _selectedNews:News;
[Bindable] private var newsList:ArrayCollection = new ArrayCollection();
[Bindable] public var programs:ArrayCollection = DomainCache.programs;
[Bindable] public var guidelineFromNews:Guideline;

private var fakeProgram:Program = null;
private const newsService:NewsService = NewsService.getInstance();
private const guidelineService:GuidelineService = GuidelineService.getInstance();
private const dateTimeUtil:DateUtil = new DateUtil();

private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

private function onCreationComplete(event:FlexEvent):void {
	var tabBar:TabNavigator = this.parent as TabNavigator;
	tabBar.addEventListener(CrudEvent.CREATE, onPublishingNewsRound);
	this.addEventListener(CrudEvent.COPY, onContactSelected);
	newsService.subscribe(onNewsMessageHandler);
}

public function onRemove(event:Event):void {
	newsService.unsubscribe(onNewsMessageHandler);
	if (selectedNews && selectedNews.id > 0) {
		newsService.unlockEdition(selectedNews.id, function(event:ResultEvent):void {});
	}
}

// -----------------------------
// Get and Set
// -----------------------------

[Bindable]
public function set selectedNews(value:News):void {
	_selectedNews = value;

	if (!value) {
		this.currentState = "normal";
	} else if(this.currentState != "preview") {
		this.currentState = "selected";
	}

	changeAddressLabel();
    loadPopUpButtonContent();
}

public function get selectedNews():News {
	return _selectedNews;
}

//--------------------------------------
//	Methods
//--------------------------------------


private function onNewsMessageHandler(event:MessageEvent):void {
	var news:News = event.message.body as News;
	var action:String = event.message.headers.action;
	
	var newsDatagrid:ArrayCollection =	newsDataGrid.dataProvider as ArrayCollection;
	if (news == null)
		return;
	
	switch (action) {
		case EntityAction.LOCKED:
			for each (var current:News in newsDatagrid) {
			if (current.equals(news)) {
				current.editingUser = event.message.headers["user"] as User;
				break;
			}
		}
			break;
		
		case EntityAction.UNLOCKED:
			for each (current in newsDatagrid) {
			if (current.equals(news)) {
				current.editingUser = null;
				break;
			}
		}
			break;
	}
	
	if (pageBar && !(action == EntityAction.UNLOCKED || action == EntityAction.LOCKED))  {
		loadNewsList();
	}
}
protected function onKeyUp():void {
	this.callLater(updateScrooler);
}

private function updateScrooler():void {
	//FIXME + TODO... REFUGO.
	newsScroller.verticalScrollBar.value = UIComponent.DEFAULT_MAX_HEIGHT;	
}

private function loadNewsList():void {
    var params:NewsParams = new NewsParams();
    params.slug = slugSearchInput.text;
    params.initialDate = periodStart.selectedDate;
    params.finalDate = periodEnd.selectedDate;
	newsService.findByParams(params, onListNewsSuccess);
}

private function loadProgramFake():void {
    programs = ObjectUtil.copy(DomainCache.programs) as ArrayCollection;
    if (programs) {
        programs.addItemAt(getFakeProgram(), 0);
        if (guidelineFromNews && guidelineFromNews.program) {
            destinyProgram.selectedItem = guidelineFromNews.program;
        } else {
            destinyProgram.selectedIndex = 0;
        }
    }
}

private function getFakeProgram():Program {
    // Retorna um programa fake para ser tratado como "GAVETA GERAL" no form.
    if (fakeProgram == null) {
        fakeProgram = new Program();
        fakeProgram.id = -1;
        fakeProgram.name = bundle.getString("Bundle", "defaults.drawerProgram");
    }
    return fakeProgram;
}

private function onSearchKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        loadNewsList();
    }
}

private function clearFilter():void {
    slugSearchInput.text = '';
    periodStart.selectedDate = null;
    periodEnd.selectedDate = null;
    pageBar.load(null, 0, 0, 0);
}

private function setButtonsEnabled(enabled:Boolean):void {
	if (this.currentState == "editing" || this.currentState == "creating") {
		hiddenButton.enabled = enabled;
		saveButton.enabled = enabled;
	} else {
		viewButton.enabled = enabled;
		newButton.enabled = enabled;
		editButton.enabled = enabled;
		deleteButton.enabled = enabled;
	}
}

private function fillNewsWithFormData(news:News):void {
	news.slug = slugInput.text;
	news.font = fontInput.text;
	news.lead = leadInput.text;
	news.address = addressInput.text;
	news.information = informationArea.text;
}

private function search(page:int=1):void {
    var params:NewsParams = new NewsParams();
    params.page = page;
    params.slug = slugSearchInput.text;
    params.initialDate = periodStart.selectedDate;
    params.finalDate = periodEnd.selectedDate;
    newsService.findByParams(params, onListNewsSuccess);
}

//--------------------------------------
//	Event Handlers
//--------------------------------------

private function onListContactFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(bundle.getString('Bundle', "agenda.msg.failedToLoadContacts"), Info.WARNING);
}

private function onListNewsSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	pageBar.load(result.results, result.pageSize, result.total, result.pageNumber);

    //Atualizar o visualizador quando já estiver uma notícia selecionada
    if (selectedNews != null) {
        for each (var n:News in newsList) {
            if (selectedNews.id == n.id) selectedNews = n;
        }
    }
}

private function onPublishingNewsRound(event:CrudEvent):void {
	var tabBar:TabNavigator = this.parent as TabNavigator;
	tabBar.selectedChild = this;
	selectedNews = event.entity as News;
	this.currentState = "creating";
}

private function onDataGridSelectionChange(event:GridSelectionEvent):void {
	selectedNews = newsDataGrid.selectedItem as News;
}

private function viewClickHandler(event:MouseEvent):void {
	this.currentState = "preview";
}

private function loadPopUpButtonContent():void {
    popUpButtonMenu.dataProvider = popUpButtonMenu.dataProvider || new ArrayCollection();

    var dataProvider:ArrayCollection = ArrayCollection(popUpButtonMenu.dataProvider);
    dataProvider.removeAll();

    // Verifica se deve exibir o botão de criar pauta
    if (selectedNews && ANews.USER.allow('01050110')  && ANews.USER.allow('01050102')) {
        dataProvider.addItem(bundle.getString("Bundle", "agency.news.btn.createGuideline"));
    }

    // Verifica a permissão de imprimir
    if (selectedNews && ANews.USER.allow("01010205")) {
        dataProvider.addItem(bundle.getString("Bundle", "defaults.btn.print"));
    }

    popUpButton.includeInLayout = popUpButton.visible = dataProvider.length != 0;
}

private function createGuidelineFromNews():void {
    if(Util.isValid(validCreateGuidelineFromNews) && selectedNews) {
        var guidelineFromNews:Guideline = new Guideline();
        guidelineFromNews.date = destinyDay.selectedDate;
        if(destinyProgram.selectedIndex == 0) {
            guidelineFromNews.program = null;
        } else {
            guidelineFromNews.program = destinyProgram.selectedItem as Program;
        }
        guidelineFromNews.slug = selectedNews.slug;
        guidelineFromNews.proposal = selectedNews.lead;
        guidelineFromNews.state = DocumentState.PRODUCING;
        guidelineFromNews.informations = selectedNews.information;
        guidelineFromNews.news = selectedNews;

        if(selectedNews.contacts.length > 0){
            var guide:Guide = new Guide();
            guide.contacts.addAll(selectedNews.contacts);
            guide.guideline = guidelineFromNews;
            guide.orderGuide = 0;
            guidelineFromNews.guides.addItem(guide);
        }
        guidelineService.save(guidelineFromNews, onSaveSuccess);
        cancelGuidelineFromNews();
    } else {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
    }
}

public function cancelGuidelineFromNews():void {
    this.currentState = "normal";
    newsDataGrid.selectedIndex = -1;
    selectedNews = null;
}

public function copyNewsFromGuideline():void {
    this.currentState = "copy";
    //Carrega programa para criação de pautas.
    loadProgramFake();
}

private function newClickHandler(event:MouseEvent):void {
	newsDataGrid.selectedIndex = -1;
	selectedNews = new News();
	this.currentState = "creating";
}

private function saveClickHandler(event:MouseEvent):void {
	if (Util.isValid(validators)) {
		setButtonsEnabled(false);
		fillNewsWithFormData(selectedNews);
		newsService.save(selectedNews,
			// success
			function(event:ResultEvent, area:NewsArea):void {
				if ( selectedNews && selectedNews.id > 0) {
					newsService.unlockEdition(selectedNews.id, function(event:ResultEvent):void {});
				}
				ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "defaults.msg.saveSuccess"), Info.INFO);
				loadNewsList();
				setButtonsEnabled(true);
				area.currentState = "normal";
			},
			// fail
			function(event:FaultEvent, area:NewsArea):void {
				if (selectedNews && selectedNews.id > 0) {
					newsService.unlockEdition(selectedNews.id, function(event:ResultEvent):void {});
				}
				ANews.showInfo(event.fault.faultString, Info.ERROR);
				setButtonsEnabled(true);
			}, this);
	} else {
		ANews.showInfo(bundle.getString('Bundle', "defaults.msg.requiredFields"), Info.WARNING);
	}
}

private function cancelClickHandler(event:MouseEvent):void {
	this.currentState = "normal";
	newsDataGrid.selectedIndex = -1;
	if ( selectedNews && selectedNews.id > 0) {
		// Remove o bloqueio de edição
		newsService.unlockEdition(selectedNews.id, function(event:ResultEvent):void {});
	}
	selectedNews = null;
}

private function editClickHandler(event:MouseEvent):void {
	
	if (selectedNews != null) {
		// Verifica se a notícia já está sendo editada por outro
		newsService.isLockedForEdition(selectedNews.id,
			function(event:ResultEvent):void {
				var user:User = event.result as User;
				if (user == null) {
					// Bloqueia a edição da notícia para os outros usuários
					newsService.lockEdition(selectedNews.id, ANews.USER.id,
						function(event:ResultEvent):void {
							currentState = "editing";
							callLater(function():void {
								informationArea.heightInLines = NaN;
							});
						}
					);
				} else {
					ANews.showInfo(bundle.getString("Bundle", "news.msg.lockedMessage", [ user.nickname ]), Info.WARNING);
				}
			}
		);
	}
	
	
}

private function deleteClickHandler(event:MouseEvent):void {
    if (selectedNews != null) {
        newsService.isLockedForEdition(selectedNews.id,
                function(event:ResultEvent):void {
                    var user:User = event.result as User;
                    if (user == null) {
                        setButtonsEnabled(false);
                        Alert.show(bundle.getString('Bundle', 'defaults.msg.confirmAllDelete'), bundle.getString('Bundle', "defaults.msg.confirmDelete"), Alert.NO | Alert.YES, null, onDeleteConfirm);
                    } else {
                        ANews.showInfo(bundle.getString("Bundle", "news.msg.lockedMessage", [ user.nickname ]), Info.WARNING);
                    }
                }
        );
    }
}

private function onPopUpButtonItemClick(event:MenuEvent):void {
    if (event.item == bundle.getString("Bundle", "agency.news.btn.createGuideline")) {
        copyNewsFromGuideline();
    }
    if (event.item == bundle.getString("Bundle", "defaults.btn.print")) {
        print();
    }
}

private function print():void {
	if (selectedNews != null) {
		var params:Object = new Object();
		params.ids = selectedNews.id + "|";
		reportUtil.generate(ReportsUtil.NEWS, params);
	}
}

private function startDragDrop(event:GridEvent):void {
	if (DragManager.isDragging || selectedNews == null)
		return;
	
	if (event.itemRenderer != null) {
		startDragDropNews(event);
	}
}

private function startDragDropNews(event:GridEvent):void {
	var sourceItens:Vector.<Object> = new Vector.<Object>();
	sourceItens.push(ObjectUtil.copy(newsDataGrid.selectedItem) as News)
	
	var dataSource:DragSource = new DragSource();
	dataSource.addData(sourceItens, "itemsByIndex");
	
	//Create the proxy (visual element that will be drag with cursor over the elements)
	var proxy:Group = new Group();
	proxy.width = newsDataGrid.grid.width;
	
	for each (var columnIndex:int in newsDataGrid.grid.getVisibleColumnIndices() as Vector.<int>) {
		var currentRenderer:IGridItemRenderer = newsDataGrid.grid.getItemRendererAt(newsDataGrid.selectedIndex, columnIndex);
		var factory:IFactory = newsDataGrid.columns.getItemAt(columnIndex).itemRenderer;
		if (!factory)
			factory = newsDataGrid.itemRenderer;
		var renderer:IGridItemRenderer = IGridItemRenderer(factory.newInstance());
		renderer.visible = true;
		renderer.column = currentRenderer.column;
		renderer.rowIndex = currentRenderer.rowIndex;
		renderer.label = currentRenderer.label;
		renderer.x = currentRenderer.x;
		renderer.y = currentRenderer.y;
		renderer.width = currentRenderer.width;
		renderer.height = currentRenderer.height;
		renderer.prepare(false);
		proxy.addElement(renderer);
		renderer.owner = newsDataGrid;
	}
	proxy.height = renderer.height;
	
	//Start the drag.
	DragManager.doDrag(newsDataGrid, dataSource, event, proxy as IFlexDisplayObject, 0, -newsDataGrid.columnHeaderGroup.height);
}

private function onDeleteConfirm(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		newsService.exclude(selectedNews,
			// success
			function(event:ResultEvent, area:NewsArea):void {
				ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "defaults.msg.deleteSuccess"), Info.INFO);
				loadNewsList();
				setButtonsEnabled(true);
				area.currentState = "normal";
			},
			// fail
			function(event:FaultEvent, area:NewsArea):void {
				ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "agency.news.guidelineExists"), Info.WARNING);
				setButtonsEnabled(true);
			}, this);
	} else {
		setButtonsEnabled(true);
	}
}

private function onSaveSuccess(event:ResultEvent):void {
    ANews.showInfo(bundle.getString("Bundle", "defaults.msg.saveSuccess"), Info.INFO);
	
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && newsDataGrid.selectedItem != null && newsDataGrid.selectedIndex != -1) {
		if (this.currentState == "normal") {
			this.currentState = "preview";
		} else {
			this.currentState = "normal";
		}
	}
}

//------------------------------------------------------------------------------
//	Contact Functions
//------------------------------------------------------------------------------

private var window:SelectContactWindow;

private function onAddContactOver(event:MouseEvent):void {
	addContact.setStyle("textDecoration", "underline");	
}

private function onAddContactOut(event:MouseEvent):void {
	addContact.setStyle("textDecoration", "none");
}

private function onAddContactClick(event:MouseEvent):void {
	window = new SelectContactWindow();
	window.setStyle("windowColor", DomainCache.settings.colorAgency);
	window.addEventListener(CrudEvent.COPY, onContactSelected);
	window.addEventListener(CloseEvent.CLOSE, onClose);
	
	PopUpManager.addPopUp(window, FlexGlobals.topLevelApplication as DisplayObject, true);
	PopUpManager.centerPopUp(window);
}

private function onContactSelected(event:CrudEvent):void {
	updateScrooler();
	selectedNews.addContact(event.entity as Contact);
}

private function onClose(event:CloseEvent):void {
	updateScrooler();
	PopUpManager.removePopUp(window);
}

private function onNewsContactsListCreated(event:FlexEvent):void {
    newsContacts.addEventListener(CrudEvent.DELETE, onContactDeleteHandler);
}

private function onContactDeleteHandler(event:CrudEvent):void {
    selectedNews.removeContact(event.entity as Contact);
}

//--------------------------------------
//	Label Functions
//--------------------------------------

private function userLabelFunction(item:Object, column:GridColumn):String {
	var news:News = News(item);
	return news.user.nickname;
}

private function fontLabelFunction(item:Object, column:GridColumn):String {
	var news:News = News(item);
	return news.font.length > 30 ? news.font.substring(0,29) + "..." : news.font;
}

private function dateLabelFunction(item:Object, column:GridColumn):String {
	var news:News = News(item);
	return formatDate(news.date) + ' ' + formatTime(news.date);
}

private function formatTime(timestamp:Date):String {
	return dateTimeUtil.toString(timestamp, bundle.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}

private function formatDate(timestamp:Date):String {
	return dateTimeUtil.toString(timestamp, bundle.getString('Bundle', "defaults.dateFormat"));
}

private function changeAddressLabel():void {
	var isUrlAddress:Boolean = (_selectedNews != null && _selectedNews.address != null) ? URLValidator.isUrl(_selectedNews.address) : false;

	if (address) {
		address.buttonMode = isUrlAddress;
		address.useHandCursor = isUrlAddress;
		if (isUrlAddress) {
			address.setStyle("textDecoration", "underline");
			address.setStyle("color", 0x345CAA);
			address.addEventListener(MouseEvent.CLICK, openUrl);
		} else {
			address.setStyle("textDecoration", "none");
			address.setStyle("color", 0xB2B2B2);
			address.removeEventListener(MouseEvent.CLICK, openUrl);
		}
	}
}

private function openUrl(event:MouseEvent):void {
    navigateToURL(URLRequest(new URLRequest(selectedNews.address)), '_blank');
}

private function onChangeInitialDate(event:CalendarLayoutChangeEvent):void {
    if (periodStart.selectedDate && periodEnd.selectedDate) {
        var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
        if (val > 0) {
            periodEnd.selectedDate = periodStart.selectedDate;
        }
    }
}

private function onChangeFinalDate(event:CalendarLayoutChangeEvent):void {
    if (periodStart.selectedDate && periodEnd.selectedDate) {
        var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
        if (val > 0 ) {
            periodStart.selectedDate = periodEnd.selectedDate;
        }
    }
}
