import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.states.State;

import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.AgendaContact;
import tv.snews.anews.domain.City;
import tv.snews.anews.domain.Contact;
import tv.snews.anews.domain.ContactGroup;
import tv.snews.anews.domain.ContactNumber;
import tv.snews.anews.domain.NumberType;
import tv.snews.anews.domain.StateCountry;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.CityService;
import tv.snews.anews.service.ContactService;
import tv.snews.anews.service.NumberTypeService;
import tv.snews.anews.service.StateService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.StringUtils;

[Bindable]
[ArrayElementType("tv.snews.anews.domain.City")]
private var cityList:ArrayCollection = new ArrayCollection();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.StateCountry")]
private var stateList:ArrayCollection = new ArrayCollection();

[Bindable] public var buttonsVisible:Boolean = true;
[Bindable] public var margin:int = 10;

private var contactToDell:ContactNumber = new ContactNumber();

private const contactService:ContactService = ContactService.getInstance();
private const cityService:CityService = CityService.getInstance();
private const stateService:StateService = StateService.getInstance();

//------------------------------------------------------------------
//	Public Methods & Properties
//------------------------------------------------------------------

private var _contact:AgendaContact = new AgendaContact();

[Bindable]
public function get contact():AgendaContact {
	return _contact;
}

public function set contact(value:AgendaContact):void {
	_contact = value;
	
	callLater(setSelectState);
	callLater(setSelectContactGroup);
	
	txtPhone.text = "";
}

public function deleteContactNumber(contactNumber:ContactNumber):void {
	contactToDell = contactNumber;
	Alert.show(
		this.resourceManager.getString('Bundle' , 'defaults.msg.confirmAllDelete'), 
		this.resourceManager.getString('Bundle' , 'defaults.msg.confirmation'), 
		Alert.NO | Alert.YES, null, confirmationDellContactNumber);
}

public function save():void {
	if (!StringUtils.isNullOrEmpty(txtPhone.text)) {
		addContactNumber();
	}
	if (contact.numbers.length == 0) {
		txtPhone.errorString = this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields');
	} else {
		txtPhone.errorString = "";
	}
	
	if (!Util.isValid(fullValidators)) {
		ANews.showInfo(this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
		return;
	}
	
	// Contato da agenda
	contact.city = cityCombo.selectedItem as City;
	contact.contactGroup = contactGroupCombo.selectedItem as ContactGroup;
	
	contactService.save(contact, onSaveSuccess);
	
}

public function cancel():void {
	contact = null;
	
	var event:Event = new Event(Event.CANCEL);
	this.dispatchEvent(event);
}

public function setInitialFocus():void {
	if (txtName) {
		focusManager.setFocus(txtName);
	}
}

//------------------------------------------------------------------
//	Private Actions
//------------------------------------------------------------------

private function setSelectContactGroup():void {
	if (contact && contact.contactGroup) {
		for (var i:int = 0; i < DomainCache.contactGroups.length; i++) {
			if (DomainCache.contactGroups.getItemAt(i).id == contact.contactGroup.id) {
				contactGroupCombo.selectedIndex = i;
				break;
			}
		}
	} else {
		contactGroupCombo.selectedIndex = -1;
	}
}

private function setSelectState():void {
	if (contact && contact.city) {
		for (var i:int = 0; i < stateList.length; i++) {
			if (stateList.getItemAt(i).id == contact.city.state.id) {
				stateCombo.selectedIndex = i;
				stateCombo.dispatchEvent(new IndexChangeEvent(IndexChangeEvent.CHANGE));
				break;
			}
		}
	} else {
		cityList = new ArrayCollection();
		cityCombo.selectedIndex = -1;
		stateCombo.selectedIndex = -1;
	}
}

private function setSelectedCity():void {
	if (contact && contact.city) {
		for (var i:int = 0; i < cityList.length; i++) {
			if (cityList.getItemAt(i).id == contact.city.id) {
				cityCombo.selectedIndex = i;
				break;
			}
		}
	} else {
		cityCombo.selectedIndex = -1;
	}
}

private function onCreationComplete(event:FlexEvent):void {
	listAllStates();
}

private function listAllStates():void {
	stateService.listAll(onListStates, onFault);
}

private function listAllCitys(idState:int):void {
	cityCombo.selectedIndex = -1;
	cityService.listAll(idState, onListCitys, onFault);
}

private function onListStates(event:ResultEvent):void {
	stateList = event.result as ArrayCollection;
	setSelectState();
}

private function onListCitys(event:ResultEvent):void {
	cityList = event.result as ArrayCollection;
	setSelectedCity();
}

private function onFault(event:FaultEvent):void {
	var exceptionMessage:String = (event.fault.faultString as String).split(':')[1];
	ANews.showInfo(exceptionMessage, Info.ERROR);
}

private function changeState():void {
	var state:StateCountry = stateCombo.selectedItem as StateCountry;
	if (state) {
		listAllCitys(state.id);
	}
}

private function addContactNumber():void {
	if (StringUtils.isNullOrEmpty(txtPhone.text)) {
		txtPhone.errorString = this.resourceManager.getString('Bundle' , 'defaults.msg.requiredFields');
		return;
	} else {
		txtPhone.errorString = "";
	}
	
	//Add the number to contact's numbers list.
	var number:ContactNumber = new ContactNumber;
	number.numberType = comboNumberType.selectedItem as NumberType;
	number.value = txtPhone.text;
	contact.numbers.addItem(number);
	
	//Clear fields.
	txtPhone.text = "";
	comboNumberType.selectedIndex = 0;
	txtPhone.setFocus();
}

private function confirmationDellContactNumber(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		contact.numbers.removeItemAt(contact.numbers.getItemIndex(contactToDell));
	}
}

private function onSaveSuccess(event:ResultEvent):void {
	var returnedContact:Contact = event.result as Contact;
	var crudEvent:CrudEvent = new CrudEvent(CrudEvent.UPDATE, returnedContact);
	this.dispatchEvent(crudEvent);
	ANews.showInfo(this.resourceManager.getString('Bundle' , 'defaults.msg.saveSuccess'), Info.INFO);
	contact = new AgendaContact();
}