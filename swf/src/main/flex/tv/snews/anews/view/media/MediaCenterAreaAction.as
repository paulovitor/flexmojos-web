/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.media.*;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.controls.DateField;
import mx.core.FlexGlobals;
import mx.events.*;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.*;
import mx.rpc.events.*;

import spark.components.TextInput;
import spark.components.gridClasses.GridColumn;

import tv.snews.anews.component.*;
import tv.snews.anews.domain.*;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.WSDevice;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.event.MediaCenterPlayEvent;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.MosMediaParams;
import tv.snews.anews.search.WSMediaParams;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.media.ANewsPlay;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private const mediaCenterService:MediaCenterService = MediaCenterService.getInstance();
private const deviceService:DeviceService = DeviceService.getInstance();
private const widthField:int = 100;

private const dtUtil:DateUtil = new DateUtil();
private const bundle:IResourceManager = ResourceManager.getInstance();

[Bindable] public var selectedDevice:Device;
[Bindable] public var selectedMedia:Media;
[Bindable] public var medias:ArrayCollection = new ArrayCollection();
[Bindable] private var devices:ArrayCollection = new ArrayCollection();
[Bindable] private var fieldsSearch:ArrayCollection = new ArrayCollection();

[Bindable] private var mosParams:MosMediaParams = new MosMediaParams();
[Bindable] private var wsParams:WSMediaParams = new WSMediaParams();

[Bindable] private var selectedType:String;
[Bindable] private var mediaViewer:Boolean = false;

[Embed(source = "/assets/showDown.png")]
private static const SHOW_DOWN:Class;
[Embed(source = "/assets/showUp.png")]
private static const SHOW_UP:Class;

private var _transforma:SoundTransform = new SoundTransform();
private var _canal:SoundChannel;
private var _som:Sound;

// ---------------------------------------
// Event's
// ---------------------------------------

protected function startUp(event:FlexEvent):void {
    this.addEventListener(MediaCenterPlayEvent.OPEN, onOpenMediaCenterPlay);
    this.addEventListener(MediaCenterPlayEvent.CLOSE, onCloseMediaCenterPlay);
	mediaCenterService.subscribe(onMediaCenterMessageReceived);
	deviceService.subscribe(onDeviceMessageReceived);
	listDevices();
}

protected function onRemove(event:FlexEvent):void {
	mediaCenterService.unsubscribe(onMediaCenterMessageReceived);
    deviceService.unsubscribe(onDeviceMessageReceived);
}

private function onChangeCbDevice(event:Event):void {
    selectedDevice = cbDevice.selectedItem;
	if (selectedDevice && selectedDevice is MosDevice) {
		if ((selectedDevice as MosDevice ).profileThreeSupported) {
			this.currentState = "mosProfile3";
			getSearchableSchema();
		} else if ((selectedDevice as MosDevice).profileOneSupported) {
            clearFilter();
            this.currentState = "mosProfile1";
		}
	}else if(selectedDevice && selectedDevice is WSDevice){
        clearFilter();
        this.currentState = "WSDevice";
    }
}

/**
 * Notificador de MediaCenter, monitora as mensagens MosListSearchableSchema do MediaCenter.
 *
 **/
private function onMediaCenterMessageReceived(event:MessageEvent):void {
	var action:String = String(event.message.headers["action"]);
	var username:String = String(event.message.headers["username"]);
	var objArray:ArrayCollection = ArrayCollection(event.message.body);

	switch (action) {
		case MediaCenter.SEARCH_SCHEMA: {
			if(username == ANews.USER.nickname) {
				fieldsSearch = new ArrayCollection();
				fieldsSearch = objArray;
				mountFieldsSearch();
			}
			break;
		}
	}
}

/**
 * Notificador de MosDevice, monitora os inserts, updates e deletes dos devices.
 *
 **/
private function onDeviceMessageReceived(event:MessageEvent):void {
	var action:String = String(event.message.headers["action"]);
	var device:Device = Device(event.message.body);
	var aux:Device;

	if(device.containsType(DeviceType.MAM) && device.isMos()) {
		switch (action) {
			case EntityAction.INSERTED: {
				devices.addItem(device);
				break;
			}
			case EntityAction.UPDATED: {
				for each (aux in devices) {
					if (aux.equals(device)) {
						aux.update(device);
						break;
					}
				}
				break;
			}
			case EntityAction.DELETED: {
				for (var i:int = 0; i < devices.length; i++) {
					aux = devices.getItemAt(i) as MosDevice;
					if (aux.equals(device)) {
						devices.removeItemAt(i);
						break;
					}
				}
				break;
			}
		}
	}
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER &&
            (mediaCenterMediaList.selectedItem != null && mediaCenterMediaList.selectedIndex != -1)){
		if (mediaViewer == true) {
            hideViewerSelectionOfMedia();
		} else {
            getMediaPreview();
		}
	}
}

// --------------------------------------
// Services
// --------------------------------------

/**
 * Efetua a busca.
 */
private function search(page:int = 1):void {

    if (selectedDevice && selectedDevice is MosDevice) {
        mosParams.initialDate = periodStart.selectedDate;
        mosParams.finalDate = periodEnd.selectedDate;
        mosParams.text = searchText.text;
        mosParams.ignoreText = ignoreText.text;
        mosParams.searchType = searchType.selectedItem.id;
        mosParams.page = page;
        if ((selectedDevice as MosDevice).profileThreeSupported) {
			getSearchableSchema(page);
		} else if ((selectedDevice as MosDevice).profileOneSupported) {
			selectedMedia = null;
			this.currentState = "mosProfile1";

			mediaCenterService.findByParams(mosParams, onListSuccess, onListFailed);
		}
	} else if (selectedDevice && selectedDevice is WSDevice){
        wsParams.titleText = titleText.text;
        wsParams.classificationText = classificationText.text;
        wsParams.observationText = observationText.text;
        wsParams.descriptionText = descriptionText.text;
        wsParams.reporterText = reporterText.text;
        wsParams.cameramanText = cameramanText.text;
//        wsParams.assistanceText = assistanceText.text;
        wsParams.editorText = editorText.text;
        wsParams.videographerText = videographerText.text;
        wsParams.cityText = cityText.text;

        wsParams.eventDateStart = eventDateStart.selectedDate;
        wsParams.eventDateEnd = eventDateEnd.selectedDate;
        wsParams.changedDateStart = changedDateStart.selectedDate;
        wsParams.changedDateEnd = changedDateEnd.selectedDate;
        wsParams.page = page;

        mediaCenterService.findByWSParams(selectedDevice as WSDevice, wsParams, onListSuccess, onListFailed);
    }
}

private function onSearchKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        search();
    }
}

//Verifica se os dados do paginador existem, caso contrário, não permite a troca de páginas
private function onPageChange(page:int=1):void {
    if(pgMediaCenter.data)search(page);
}
	
private function listDevices():void {
    deviceService.findAllByType(DeviceType.MAM,
		function(event:ResultEvent):void {
			devices = event.result as ArrayCollection;
		}
	);
	this.currentState = "normal";
}

private function getSearchableSchema(page:int=1):void {
	this.currentState == "mosProfile3"
	mediaCenterService.mosReqSearchableSchema(selectedDevice as MosDevice);
}

// -------------------------------------
// Result's
// -------------------------------------
private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	pgMediaCenter.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function onListFailed(event:FaultEvent):void {
	if(event.fault.faultString.indexOf('SocketTimeoutException') != -1)
        ANews.showInfo(bundle.getString('Bundle' , "mediaCenter.msg.timeoutOccurred"), Info.WARNING);
}

// -------------------------------------
// Helpers
// -------------------------------------
private function getMediaPreview():void {
    var playerMedia:PlayerMedia = new PlayerMedia();
    if(selectedMedia is WSMedia) {
        playerMedia.parseToWSMedia(selectedMedia as WSMedia);
    } else if(selectedMedia is MosMedia) {
        playerMedia.parseToMosMedia(selectedMedia as MosMedia);
    }
    if (playerMedia.url != null && playerMedia.url.length > 0) {
        this.dispatchEvent(new MediaCenterPlayEvent(MediaCenterPlayEvent.OPEN, playerMedia));
    } else {
        ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.msg.hasNoMedia'), Info.WARNING);
        return;
    }
}

private function onWsShowHideAdvancedSearch():void {
    wsAdvancedSearch.includeInLayout = wsAdvancedSearch.visible = !wsAdvancedSearch.visible;
    showHideWsAdvancedSearchGroup.toolTip = wsAdvancedSearch.visible ?  bundle.getString('Bundle', 'mediaCenter.search.ws.hideAdvancedSearch')
                                                                     : bundle.getString('Bundle', 'mediaCenter.search.ws.showAdvancedSearch');
    showHideWsAdvancedSearchImage.source = wsAdvancedSearch.visible ?  SHOW_UP : SHOW_DOWN;
}

/**
 * Mudança de seleção de mídia.
 * 
 **/
private function changingSelectionOfMedia():void {
    if(selectedDevice is MosDevice) this.currentState = "mosMediaSelected";
    if(selectedDevice is WSDevice) this.currentState = "wsMediaSelected";
	selectedMedia = mediaCenterMediaList.selectedItems[0] as Media;
}

/**
 * Previwer da mídia selecionada.
 *
 **/
public function hideViewerSelectionOfMedia():void {
    this.dispatchEvent(new MediaCenterPlayEvent(MediaCenterPlayEvent.CLOSE));
}

private function onCloseMediaCenterPreview(event:CloseEvent):void {
    this.dispatchEvent(new MediaCenterPlayEvent(MediaCenterPlayEvent.CLOSE));
}

/**
 * Monta campos de busca do MediaCenter.
 * 
 **/
private function mountFieldsSearch():void {
	fields.removeAllElements();
	for each (var obj:Object in fieldsSearch) {
		for (var key:String in obj) {
			var formLabel:FormLabel = new FormLabel();
			formLabel.text = bundle.getString('Bundle', "mos.search." + key) != null ? bundle.getString('Bundle', "mos.search."+key) : key;
			fields.addElement(formLabel);
			switch(obj[key]) {
				case "string" :
					var textInputStr:TextInput = new TextInput();
					textInputStr.id = key;
					textInputStr.percentWidth = widthField;
					fields.addElement(textInputStr);
					break;
				case "decimal" :
					var textInputDec:TextInput = new TextInput();
					textInputDec.id = key;
					textInputDec.percentWidth = widthField;
					textInputDec.restrict="[0-9]*\.?[0-9]{0,5}";
					fields.addElement(textInputDec);
					break;
				case "int" :
				case "integer" :
					var textInputInt:TextInput = new TextInput();
					textInputInt.id = key;
					textInputInt.percentWidth = widthField;
					textInputInt.restrict="[0-9]";
					textInputInt.maxChars = 11;
					fields.addElement(textInputInt);
					break;
				case "long" :
					var textInputLong:TextInput = new TextInput();
					textInputLong.id = key;
					textInputLong.percentWidth = widthField;
					textInputLong.restrict="[0-9]";
					textInputLong.maxChars = 20;
					fields.addElement(textInputLong);
					break;
				case "date" :
					var dateField:DateField = new DateField();
					dateField.id = key;
					dateField.percentWidth = widthField;
					fields.addElement(dateField);
					break;
				default :
					var textInput:TextInput = new TextInput();
					textInput.id = key;
					textInput.percentWidth = widthField;
					fields.addElement(textInput);
					break;
			}
		}
	}
}

/**
 * Limpar filtro.
 */
private function clearFilter():void {
	if(this.currentState == "mosProfile1" || this.currentState == "mosMediaSelected" &&
            !(selectedDevice is MosDevice && (selectedDevice as MosDevice).profileThreeSupported) ){
        pgMediaCenter.load(null, 0, 0, 0);
        mediaCenterMediaList.selectedIndex = -1;
        periodStart.text = '';

		periodStart.selectedDate = null;
        periodEnd.text = '';

		periodEnd.selectedDate = null;
        searchText.text = '';
        ignoreText.text = '';

		searchType.selectedIndex = 0;
        mosParams.clear();

		selectedMedia = null;
		
		this.currentState = "mosProfile1";
	} else if(this.currentState == "WSDevice" || this.currentState == "wsMediaSelected"){
        pgMediaCenter.load(null, 0, 0, 0);
        mediaCenterMediaList.selectedIndex = -1;
        titleText.text = '';
        classificationText.text = '';
        observationText.text = '';
        descriptionText.text = '';
        reporterText.text = '';
        cameramanText.text = '';
//        assistanceText.text = '';
        editorText.text = '';
        videographerText.text = '';

        cityText.text = '';
        eventDateStart.selectedDate = null;
        eventDateEnd.selectedDate = null;
        changedDateStart.selectedDate = null;
        changedDateEnd.selectedDate = null;
    }
}

private function periodIsInvalid():Boolean {
	var start:Date = periodStart.selectedDate;
	var end:Date = periodEnd.selectedDate;
	return start && end && start.time > end.time;
}

private function dateColumnLabelFunction(item:Object, column:GridColumn):String {
	return dtUtil.dateToString(item[column.dataField]);
}

private function timeColumnLabelFunction(item:Object, column:GridColumn):String {
	return dtUtil.timeToString(item[column.dataField]);
}

//------------------------------------------------------------------------------
// MediaCenterPlay visualizador de videos.
//------------------------------------------------------------------------------
private function onOpenMediaCenterPlay(event:MediaCenterPlayEvent):void {
    closeMediaCenterPlay();
    mediaViewer = true;
    mediaCenterPlay.start(event);
}

private function onCloseMediaCenterPlay(event:MediaCenterPlayEvent):void {
    closeMediaCenterPlay();
}

private function closeMediaCenterPlay():void {
    mediaCenterPlay.enableVisible = false;
    if(mediaCenterPlay.mediaPlayer || mediaCenterPlay.audioPlayer || mediaCenterPlay.img || mediaCenterPlay.anewsPlayIFrame) {
        mediaCenterPlay.stop(true);
    }
    mediaViewer = false;
}
