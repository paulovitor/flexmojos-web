package tv.snews.anews.view.navigator {

	import spark.components.supportClasses.DropDownController;

	public class SearchDropDownController extends DropDownController {
		public var stayOpen:Boolean;

		public function SearchDropDownController() {
			super();
		}

		override public function closeDropDown(commit:Boolean):void {
			if (!stayOpen) {
				super.closeDropDown(commit);
			}
		}
	}
}
