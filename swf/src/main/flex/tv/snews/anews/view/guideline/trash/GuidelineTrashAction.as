import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceBundle;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.gridClasses.GridColumn;
import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.flexlib.events.PageEvent;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.ObjectUtils;

private const guidelineService:GuidelineService = GuidelineService.getInstance();
private const dateUtil:DateUtil = new DateUtil();

public const bundle:IResourceManager = ResourceManager.getInstance();

[Bindable] public var programsWithFake:ArrayCollection = DomainCache.programs;
[Bindable] public var programDrawer:Program = getProgramDrawer();
[Bindable] private var showViewer:Boolean = false;

private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	pgGuidelineTrash.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function search(page:int=1):void {
	if (page < 1)
		page = 1;
	guidelineService.listExcludedGuidelinesPage(page, onListSuccess);
}

private function onContentCreationComplete(event:FlexEvent):void {
	programsWithFake =  ObjectUtil.copy(DomainCache.programs) as ArrayCollection;
	programsWithFake.addItemAt(getProgramDrawer(), 0);
	guidelineService.subscribe(onMessageGuidelineTrash);
}

private function onMessageGuidelineTrash(event:MessageEvent):void {
	var action:String = event.message.headers.action;
	if (action == EntityAction.DELETED || action == EntityAction.RESTORED) {
		search();
	}
}

private function onRemove(event:Event):void {
	guidelineService.unsubscribe(onMessageGuidelineTrash);
}

private function onRestoreClick(event:MouseEvent):void {
	restoreTo.visible = true;
	restoreTo.includeInLayout = true;
}

private function confirmRestore():void {
	var selectedGuideline:Guideline = excludedGrid.selectedItem as Guideline;

	if (selectedGuideline.date != null && cbProgramsCopyTo.selectedIndex != -1) {
		guidelineService.restoreExcludedGuideline(selectedGuideline.id, (cbProgramsCopyTo.selectedItem as Program).id , guidelineDateCopyTo.selectedDate,  function(event:ResultEvent):void {
			ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "guideline.trash.msg.restoreSuccess"), Info.INFO);
			search();
			restoreTo.visible = false;
			restoreTo.includeInLayout = false;
            selectedGuideline = null;
            excludedGrid.selectedItem = -1;
            onSelectionChange();
		});
	} else {
		ANews.showInfo(this.resourceManager.getString('Bundle', "defaults.msg.requiredFields"), Info.INFO);
	}
}

private function cancelRestoreTo():void {
	restoreTo.visible = false;
	restoreTo.includeInLayout = false;
}

private function onSelectionChange():void {
	var selectedGuideline:Guideline = excludedGrid.selectedItem as Guideline;
	if (selectedGuideline) {
		this.currentState = "selected";
		guidelineViewer.guideline = selectedGuideline;
	} else {
		this.currentState = "normal";
	}
}

private function scheduleLabelFunction(item:Object, column:GridColumn):String {
	const guideline:Guideline = item as Guideline;
	const firstGuide:Guide = guideline.firstGuide();

	var result:String = dateUtil.dateToString(guideline.date);
	if (firstGuide) {
		result += " " + dateUtil.timeToString(firstGuide.schedule);
	}
	return result;
}

private function onNormalState():void {
	excludedGrid.selectedIndex = -1;
	this.currentState = "normal";
}

private function excludedLabelFunction(item:Object, column:GridColumn):String {
	const guideline:Guideline = item as Guideline;
	return guideline.author.nickname + " (" + dateUtil.fullDateToString(guideline.changeDate, true) + ")";
}

private function onViewClick(event:MouseEvent):void{
	showViewer = true;
}

private function onCancelClick(event:MouseEvent):void {
	showViewer = false;
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && excludedGrid.selectedItem != null && excludedGrid.selectedIndex != -1) {
		if (showViewer == true) {
			showViewer = false;
		} else {
			showViewer = true;
		}
	}
}

private function getProgramDrawer():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL" 
	if (programDrawer == null) {
		programDrawer = new Program();
		programDrawer.id = -1;
		programDrawer.name = ResourceManager.getInstance().getString("Bundle", "defaults.drawerProgram");
	}
	return programDrawer;
}
