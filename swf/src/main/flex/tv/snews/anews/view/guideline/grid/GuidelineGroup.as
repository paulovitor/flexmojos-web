package tv.snews.anews.view.guideline.grid {

import mx.collections.ArrayCollection;
import mx.formatters.DateFormatter;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import spark.collections.Sort;
import spark.collections.SortField;

import tv.snews.anews.domain.CommunicationVehicle;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.GuidelineClassification;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserTeam;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class GuidelineGroup {

    public static const CLASSIFICATION:String = "classification";
    public static const PROGRAM:String = "program";
    public static const REPORTER:String = "reporters";
    public static const VEHICLE:String = "vehicles";
    public static const TEAM:String = "teams";
	private static const bundle:IResourceManager = ResourceManager.getInstance();

	private static const NO_ID:int = -1;
	private static const NO_VEHICLE:String = bundle.getString('Bundle', 'guideline.grid.noVehicle');

	public var mode:String;

	private var classification:GuidelineClassification = null;
	private var program:Program = null;
	private var reporter:User = null;
	private var vehicle:String = null;
    private var team:UserTeam = null;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public function GuidelineGroup(mode:String, groupProperty:Object = null) {
		this.mode = mode;

		switch (mode) {
			case CLASSIFICATION:
				this.classification = (groupProperty as GuidelineClassification) || noClassification();
				break;
			case PROGRAM:
				this.program = (groupProperty as Program) || noProgram();
				break;
			case REPORTER:
				this.reporter = (groupProperty as User) || noReporters();
				break;
			case VEHICLE:
				this.vehicle = (groupProperty as String) || noVehicle();
				break;
            case TEAM:
                this.team = (groupProperty as UserTeam) || noTeam();
                break;
			default:
				throw new Error("Unknown group mode");
		}

		data.sort = new Sort();
		data.sort.fields = [ new SortField("firstGuideSchedule"), new SortField("slug") ];
	}

	//----------------------------------
	//  Properties
	//----------------------------------

	//
	//	data
	//

	[Bindable]
	[ArrayElementType("tv.snews.anews.domain.Guideline")]
	public var data:ArrayCollection = new ArrayCollection();

	//
	//	label
	//

	public function get label():String {
		switch (mode) {
			case CLASSIFICATION:
				return classification.name;
			case PROGRAM:
				return formatTime(program.start) + " " + program.name;
			case REPORTER:
				return reporter.nickname;
			case VEHICLE:
				return vehicle == NO_VEHICLE ? vehicle : CommunicationVehicle.findById(vehicle).name;
            case TEAM:
                return team.name;
			default:
				throw new Error("Unknown group mode");
		}
	}

	//----------------------------------
	//  Operations
	//----------------------------------

	public function match(guideline:Guideline):Boolean {
		switch (mode) {
            case CLASSIFICATION:
                if (guideline.classification) {
                    return guideline.classification.equals(classification);
                } else {
                    return classification.id == NO_ID;
                }
            case PROGRAM:
                if (guideline.program) {
                    return guideline.program.equals(program);
                } else {
                    return program.id == NO_ID;
                }
            case REPORTER:
                for each (var currentReporter:User in guideline.reporters) {
                    if (currentReporter.equals(reporter)) {
                        return currentReporter.equals(reporter);
                    }
                }
                return guideline.reporters != null && guideline.reporters.length == 0 && reporter == noReporters();
			case VEHICLE:
                for each (var currentVehicle:String in guideline.vehicles) {
                    if (currentVehicle == vehicle) {
                        return true;
                    }
                }
                return guideline.vehicles.length == 0 && vehicle == noVehicle();
            case TEAM:
                if (guideline.team) {
                    return guideline.team.equals(team);
                } else {
                    return team.id == NO_ID;
                }
            default:
				throw new Error("Unknown group mode");
		}
	}

	public function addItem(guideline:Guideline):void {
		data.addItem(guideline);
		data.refresh();
	}

	public function removeItem(guideline:Guideline):void {
		for each (var current:Guideline in data) {
			if (current.equals(guideline)) {
				data.removeItemAt(data.getItemIndex(current));
				break;
			}
		}
	}

	//----------------------------------
	//  Helpers
	//----------------------------------

	private static var _noClassification:GuidelineClassification;

	private static function noClassification():GuidelineClassification {
		if (!_noClassification) {
			_noClassification = new GuidelineClassification();
			_noClassification.id = NO_ID;
			_noClassification.name = bundle.getString('Bundle', 'guideline.grid.noClassification');
		}
		return _noClassification;
	}

	private static var _noProgram:Program;

	private static function noProgram():Program {
		if (!_noProgram) {
			_noProgram = new Program();
			_noProgram.id = NO_ID;
			_noProgram.name = bundle.getString('Bundle', 'defaults.drawerProgram');
		}
		return _noProgram;
	}

    private static var _noTeam:UserTeam;

    private static function noTeam():UserTeam {
        if (!_noTeam) {
            _noTeam = new UserTeam();
            _noTeam.id = NO_ID;
            _noTeam.name = bundle.getString('Bundle', 'guideline.grid.noTeam');
        }
        return _noTeam;
    }

    private static var _noReporter:User;

	private static function noReporters():User {
		if (!_noReporter) {
            _noReporter = new User();
            _noReporter.id = NO_ID;
            _noReporter.nickname = bundle.getString('Bundle', 'guideline.grid.noReporter');
		}
		return _noReporter;
	}

	private static function noVehicle():String {
		return NO_VEHICLE;
	}

	private static function formatTime(time:Date):String {
		var formatter:DateFormatter = new DateFormatter();
		formatter.formatString = "JJ:NN";
		return formatter.format(time);
	}
}
}
