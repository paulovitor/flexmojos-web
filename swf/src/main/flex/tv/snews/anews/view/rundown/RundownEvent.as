package tv.snews.anews.view.rundown {

	import flash.events.Event;

	/**
	 * Evento para notificiar as ações no Espelho.
	 *
	 * @author Paulo Felipe
	 * @since 1.0.0
	 */
	public class RundownEvent extends Event {

//		public static const SELECTION_CHANGE_STORY_RUNDOWN:String = "selectionChangeRundown";
		public static const SELECTION_CHANGE_STORY_SIMPLE_RUNDOWN:String = "selectionChangeStorySimpleRundown";
        public static const COMPLETE_RUNDOWN_DISPLAY:String = "COMPLETE_RUNDOWN_DISPLAY";

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		public function RundownEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
		}

	}
}
