import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Rectangle;
import flash.utils.Timer;

import mx.collections.ArrayCollection;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.core.IVisualElement;
import mx.effects.AnimateProperty;
import mx.events.CloseEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.messaging.messages.IMessage;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.DataGroup;
import spark.components.RichEditableText;
import spark.components.TextArea;

import tv.snews.anews.component.ChangesHistoryWindow;
import tv.snews.anews.component.FormScrollManager;
import tv.snews.anews.component.Info;
import tv.snews.anews.component.TimeMachine;
import tv.snews.anews.domain.Checklist;
import tv.snews.anews.domain.ChecklistResource;
import tv.snews.anews.domain.ChecklistTask;
import tv.snews.anews.domain.City;
import tv.snews.anews.domain.CommunicationVehicle;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Guide;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.GuidelineClassification;
import tv.snews.anews.domain.News;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.StateCountry;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserTeam;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.guideline.GuideForm;
import tv.snews.anews.view.guideline.GuidelineForm;
import tv.snews.anews.view.guideline.GuidelineFormChecklistRenderer;
import tv.snews.anews.view.guideline.GuidelineRevisionRenderer;
import tv.snews.flexlib.utils.DateUtil;

[ArrayElementType("tv.snews.anews.domain.GuidelineRevision")]
[Bindable] private var versions:ArrayCollection = new ArrayCollection();

[Bindable] private var guidelineOld:Guideline = new Guideline();
[Bindable] private var modify:String;
[Bindable] public var viewNews:Boolean = false;
[Bindable] public var superTabStopClose:Boolean = false;
[Bindable] public var isAddTask:Boolean = false;
[Bindable] public var programs:ArrayCollection = DomainCache.programs;
[Bindable] private var selectedVehicles:ArrayCollection = new ArrayCollection();

private var fakeProgram:Program = null;

public var _guideline:Guideline = null;

[Bindable]
public function set guideline(value:Guideline):void {
    if (value) {
        // Corrige os veículos da guideline para que possam ser apresentados no form
        selectedVehicles.removeAll();
        for each (var vehicle:String in value.vehicles) {
            selectedVehicles.addItem(CommunicationVehicle.findById(vehicle));
        }
    }

    _guideline = value;
}

public function get guideline():Guideline {
	return _guideline;
}

public var tabManager:TabManager;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const dateTimeUtil:DateUtil = new DateUtil();
private const guidelineService:GuidelineService = GuidelineService.getInstance();
private const newsService:NewsService = NewsService.getInstance();
private const checklistService:ChecklistService = ChecklistService.getInstance();
private const dtUtil:DateUtil = new DateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();

// FIXME Cache desativado
//private const localData:LocalData = LocalData.getInstance(LocalData.GUIDELINE);
private var timer:Timer = null;
private var guidesTabManager:TabManager = new TabManager();

private var scrollManager:FormScrollManager;

private function startUp():void {
	guidelineService.subscribe(onGuidelineMessageReceived);
	newsService.subscribe(onNewsMessageReceived);
    // Trata a remoção de recursos ou tasks no form
    checklistTasks.addEventListener(CrudEvent.DELETE, onDeleteChecklistTask);

	checklistService.subscribe(onChecklistMsgReceived);

	guidesTabManager.tabNavigator = guidesTabNavigator;
	guidesTabNavigator.selectedIndex = 0;

	//Clone do guideline
	guidelineOld = ObjectUtil.clone(guideline) as Guideline;
	markChanges();

	// cache control
	activeAutoSave();
	refreshGuide();

	// load program fake
	loadProgramFake();

    cmbGuidelineTeam.selectedItem = guideline.team ? guideline.team : null;

	scrollManager = new FormScrollManager(formScroller);
}

// ---------------------------------
// Events.
// ---------------------------------
private function formatTime(timestamp:Date):String {
	return dtUtil.toString(timestamp, bundle.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}

private function formatDate(timestamp:Date):String {
	return dtUtil.toString(timestamp, bundle.getString('Bundle', "defaults.dateFormat"));
}

private function refreshGuide():void {
	guidesTabNavigator.removeAllChildren();

	if (guideline != null) {
		// Varre os roteiros da pauta selecionada.
		for each (var guide:Guide in guideline.guides) {
			var guideForm:GuideForm = new GuideForm();
			guideForm.guide = guide;
			
			guidesTabNavigator.addItem(guideForm);
		}
	}
}

private function orderGuide():void {
	var i:int= -1;
	for each (var object:Object in guidesTabNavigator.getChildren()) {
		if(object is GuideForm) {
			var _form:GuideForm = object as GuideForm;
			_form.guide.orderGuide = ++i;
		}
	}
}		

private function save(closable:Boolean, printAfterSave:Boolean = false):void {
	timer.stop();
	btnSave.enabled = false;
	btnSaveAndClose.enabled = false;
	
	for each (var object:Object in guidesTabNavigator.getChildren()) {
		if (object is GuideForm) {
			var guideForm:GuideForm = object as GuideForm;
			if (guideForm.containsInvalidFields()) {
				timer.start();
				btnSave.enabled = true;
				btnSaveAndClose.enabled = true;

				ANews.showInfo(bundle.getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
				return;
			}
		}
	}

    if (!Util.isValid(validadores)  || !checklistTasks.tasksAreValid()) {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle' , 'defaults.msg.requiredFields'), Info.WARNING);
		timer.start();
		btnSave.enabled = true;
		btnSaveAndClose.enabled = true;
		return;	
	} 
	
	copyDataFormTo(guideline);
	
	var self:GuidelineForm = this;

    guidelineService.save(guideline,
			function (event:ResultEvent):void {
				ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);

                // Habilita botões que foram desativados durante o envio dos dados
				btnSave.enabled = true;
				btnSaveAndClose.enabled = true;

                // Atualiza os dados
				var aux:Guideline = Guideline(event.result);
				guideline.updateFields(aux);

                // Atualiza o ID da TAB.
				if (guideline.id && guideline.id > 0) {
					updateGuidesForms(guideline.guides);
					tabManager.updateTabKey(self, "guideline_" + guideline.id + "");
				}
		
//				FIXME Cache desativado
//				localData.clear();

                // Muda o status de edição do formulário
				guidelineOld = ObjectUtil.clone(guideline) as Guideline;
				modify = '';

                // Se tiver sido executado o comando de imprimir, salva e imprime
				if (printAfterSave) {
					print();
				}

                // Se tiver sido executado o comando de salvar e fechar, fecha
				if (closable) {
					close();
				} else {
					timer.start();
				}

                // Desbloqueia a edição da checklist
		        unlockChecklist(guideline.checklist);
            },
			function (event:FaultEvent):void {
				ANews.showInfo(event.fault.faultString, Info.ERROR);
				timer.start();
		
				// Enable buttons, server already answer the client.
				btnSave.enabled = true;
				btnSaveAndClose.enabled = true;

                // Desbloqueia a edição da checklist
		        unlockChecklist(guideline.checklist);
            }
	);
}

private function copyDataFormTo(guideline:Guideline):void {
	guideline.date = dfDate.selectedDate;
	var program:Program = cmbProgram.selectedItem as Program
	if (program && program.id == -1) {
		guideline.program = null;
	} else {
		guideline.program = program;
	}

	guideline.classification = cmbGuidelineClassification.selectedItem as GuidelineClassification;
    guideline.team = cmbGuidelineTeam.selectedItem as UserTeam;

	guideline.vehicles.removeAll();
	for each (var vehicle:Object in acVehicles.selectedItems) {
		guideline.vehicles.addItem(vehicle.id);
	}

	guideline.guides.removeAll();

	var i:int = -1;
	for each (var object:Object in guidesTabNavigator.getChildren()) {
		if (object is GuideForm) {
			var _form:GuideForm = GuideForm(object);
			var _guide:Guide = _form.guide as Guide;
			var _city:City = _form.selectedCity as City;
			var _state:StateCountry = _form.selectedState as StateCountry;

			if (_city != null) {
				_guide.city = _city;
				if (_state != null)
					_guide.city.state = _state;
			}

			_guide.orderGuide = ++i;
			_guide.guideline = guideline;
			guideline.guides.addItem(_guide);
		}
	}
}

/*
 * Após salvar uma pauta com roteiro(s) pela primeira vez, deve-se pegar os 
 * objetos salvos (que agora possuem ID) e jogar no formulário de cada roteiro 
 * correspondente.
 */
private function updateGuidesForms(guides:ArrayCollection):void {
	var counter:int = 0;
	for each (var child:DisplayObject in guidesTabNavigator.getChildren()) {
		if (child is GuideForm) {
			var form:GuideForm = child as GuideForm;
			form.guide = guides.getItemAt(counter++) as Guide;
		}
	}
}

public function close():void {
	copyDataFormTo(guideline)
	
	if (guidelineOld.isDifferent(guideline)) {
		Alert.show(
			bundle.getString("Bundle", "defaults.msg.dataAreNotSaved"),
			bundle.getString("Bundle", "defaults.msg.confirmation"),
			Alert.CANCEL | Alert.NO | Alert.YES, null, onDataAreNotSaved, null, Alert.YES
		);
	} else {
		onRemove();
	}
}

private function onDeleteChecklistTask(event:CrudEvent):void {
    if (event.entity is ChecklistTask) {
        // Remove a task da produção
        guideline.checklist.removeTask(ChecklistTask(event.entity));
    }
}

private function onDataAreNotSaved(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		save(true);
	} else if(event.detail == Alert.NO) {
		onRemove();
	}
}

private function createGuidelineFromNews(news:News):void {
	startUp();
	guideline.proposal = news.slug;
}

private function addGuide(event:MouseEvent):void {
	var guideForm:GuideForm = new GuideForm();
	var guide:Guide = new Guide();
	guide.orderGuide = guidesTabNavigator.getChildren().length; 
	guideForm.guide = guide;
	guidesTabNavigator.addItem(guideForm);
}

public function onRemove():void {
	if (guideline.id > 0) {
		guidelineService.unlockEdition(guideline.id, null);
		unlockChecklist(guideline.checklist);
    }
	
	tabManager.removeTab(this);
	
	guidelineService.unsubscribe(onGuidelineMessageReceived);
	newsService.unsubscribe(onNewsMessageReceived);
	checklistService.unsubscribe(onChecklistMsgReceived);

	clearCacheData();

	scrollManager.deactivate();
}


private function onEditChecklist():void {
	var checklist:Checklist = guideline.checklist || new Checklist(guideline);

	// Agora, se a pauta já existir e o usuário quiser editar a lista de tarefas
	// é necessário criar a lista de tarefas no servidor e bloqueá-la para edição
	// para não fazer merda na outra área. Neste caso, automaticamente é criada uma
	// produção na área mapa de produção mesmo que o usuário cancele a ação
	if(checklist.id < 1) {
        if(guideline.id > 0) {
            checklistService.save(checklist, function (event:ResultEvent):void {
                checklist = event.result as Checklist;
				checklist.addTask(new ChecklistTask());
				isAddTask = true;
				guideline.checklist = checklist;
            });
        } else {
            if(checklist.tasks.length == 0)
                checklist.addTask(new ChecklistTask());
            isAddTask = true;
        }
        guideline.checklist = checklist;
    } else {
		// Se já existir, menos problema. Apenas bloqueia a edição na outra área.
		lockChecklist(checklist);
		isAddTask = true;
	}
}

private function lockChecklist(checklist:Checklist):void {
	checklistService.lockEdition(checklist.id, function (result:ResultEvent):void {
        if(checklist.tasks.length == 0)
            checklist.addTask(new ChecklistTask());
		isAddTask = true;
		guideline.checklist = checklist;
	});
}

private function unlockChecklist(checklist:Checklist):void {
	if(checklist && checklist.editingUser && checklist.editingUser.equals(ANews.USER) && isAddTask) {
		checklistService.unlockEdition(checklist.id, null);
	}
    isAddTask = false;
}

private function onAddTask():void {
    var checklist:Checklist = guideline.checklist || new Checklist(guideline);
    checklist.addTask(new ChecklistTask());
}

private function onDoneClick():void {
    if (guideline.checklist && doneCheckBox.selected) {
        for each (var task:ChecklistTask in guideline.checklist.tasks) {
            task.done = true;
        }
    }
}

private function checkIsDone():void {
    var doneCount:int = 0;
    if(isAddTask) {
        for each (var task:ChecklistTask in guideline.checklist.tasks) {
            if (task.done) {
                doneCount++;
            }
        }

        if (guideline.checklist.tasks.length > 0) {
            guideline.checklist.done = doneCount == guideline.checklist.tasks.length;
        }
    }
}


private function onNewsViewer(event:Event):void {
	newsService.loadById(guideline.news.id, function(event:ResultEvent):void {
		guideline.news = News(event.result);
		viewerNews.showing = true;
	});
}

// ---------------------------------
// Auxiliares.
// ---------------------------------

private function activeAutoSave():void {
	// Em intervalos de 5 segundos salva o texto do encaminhamento localmente
	timer = new Timer(1000);
	timer.addEventListener(TimerEvent.TIMER, 
		function(event:TimerEvent):void {
			copyDataFormTo(guideline);
			
			// FIXME Cache desativado
//			localData.save(guideline);
			markChanges();
            checkIsDone();
		}
	);
	timer.start();
}

private function markChanges():Boolean {
	modify = guidelineOld.isDifferent(guideline) ? '* ' : '';
	return modify == '* ';
}

private function clearCacheData():void {
	timer.stop();
	
	// FIXME Cache desativado
//	localData.clear();
}

public function changeState(state:String):void {
	this.currentState = state;
}

private function getVersions():void {
	if (guideline != null && guideline.id > 0) {
		guidelineService.loadChangesHistory(guideline.id, function(event:ResultEvent):void {
			versions = event.result as ArrayCollection;
			if (versions && versions.length > 0) {
				var changesWindow:ChangesHistoryWindow = new ChangesHistoryWindow();
				changesWindow.addEventListener(CloseEvent.CLOSE,
					function(event:CloseEvent):void {
						PopUpManager.removePopUp(changesWindow);
					}
				);
				
				changesWindow.dataProvider = versions;
				changesWindow.itemRenderer = GuidelineRevisionRenderer;
				
				PopUpManager.addPopUp(changesWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
				PopUpManager.centerPopUp(changesWindow);
			} else {
				ANews.showInfo(ResourceManager.getInstance().getString('Bundle' , 'guideline.msg.withoutVersion'), Info.INFO);
			}
		});
	}
}

private function saveAndPrint():void {
	if (guideline.id == 0 || markChanges()) {
		Alert.show(
				bundle.getString("Bundle", "defaults.msg.saveBeforePrint"),
				bundle.getString("Bundle", "defaults.msg.confirmation"),
						Alert.NO | Alert.YES, null,

				function(event:CloseEvent):void {
					if (event.detail == Alert.YES) {
						save(false, true);
					}
				}
		);
	} else {
		print();
	}
}

private function print():void {
	reportUtil.generate(ReportsUtil.GUIDELINE, { ids: guideline.id });
}

/**
 * @private
 * Handler para as mensagens enviadas pelo servidor para notificar ações sobre
 * as pautas feitas pelos usuários, como operações de CRUD e bloqueio de edição.
 */

//FIXME bolar maneira mais inteligente, essa sugestão é bem doidona.
private function onGuidelineMessageReceived(event:MessageEvent):void {
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var current:Guideline = message.body as Guideline;

    if (current.id == guideline.id) {
        switch (action) {
            // Pauta foi atualizada
            case EntityAction.UPDATED:
                guideline.updateFields(current);

                updateGuidesForms(guideline.guides);
                guidelineOld = ObjectUtil.clone(guideline) as Guideline;
                modify = '';
                break;
            case EntityAction.FIELD_UPDATED:
                var field:String = message.headers.field;
                if (field == "programs") {
                    var programs:ArrayCollection = message.headers.value as ArrayCollection;
                    guideline.programs.removeAll();
                    guideline.programs.addAll(programs);
                    guidelineOld = ObjectUtil.clone(guideline) as Guideline;
                }
                break;
        }
    }
}

private function onNewsMessageReceived(event:MessageEvent):void {
	if (guideline.news) {
        var action:String = event.message.headers.type;
        var current:News = event.message.body as News;

        // Sugestão para a correção do problema de sincronia nas atualizações
        if (guideline.news.id == current.id && action == EntityAction.UPDATED) {
            guideline.news = current;
        }
	}
}

private function onChecklistMsgReceived(event:MessageEvent):void {
    var action:String = event.message.headers.action;
    var user:User = event.message.headers.user as User;
    var current:Checklist = event.message.body as Checklist;

	// Se a notificação da produção não for da produção corrente, ignora.
    if (!guideline.equals(current.guideline)) {
		return;
	}
	
	switch (action) {
		case EntityAction.INSERTED:
		case EntityAction.UPDATED:
		case EntityAction.LOCKED:
			// Para os três casos tem que atualizar a checklist da pauta
			guideline.checklist == null ? guideline.checklist = current : guideline.checklist.updateFields(current);
			
			// Para o LOCKED tem que definir o editingUser
			if(action==EntityAction.LOCKED)
				guideline.checklist.editingUser = user;
			
			// Atualiza o guidelineOld para que as mudanças do notificador não indiquem uma mudança do usuário
			guidelineOld.checklist = ObjectUtil.clone(current) as Checklist;
			break;
		
		// Checklist foi desbloqueada para edição
		case EntityAction.UNLOCKED:
			if(guideline.checklist)
				guideline.checklist.editingUser = null;
			isAddTask = false;
			break;
		
		// Checklist foi excluída
		case EntityAction.DELETED:
			guideline.checklist.excluded = true;
			break;
	}
    
}

private function loadProgramFake():void {
	programs = ObjectUtil.copy(DomainCache.programs) as ArrayCollection;	
	if (programs) {
		programs.addItemAt(getFakeProgram(), 0);
		if (guideline && guideline.program) {
			cmbProgram.selectedItem = guideline.program;
		} else {
			cmbProgram.selectedIndex = 0;	
		}
	}
}
	
private function onCloseTimeMachine(event:CloseEvent):void {
	var timeMachine:TimeMachine =  event.target as TimeMachine;
	PopUpManager.removePopUp(timeMachine);
}

private function getFakeProgram():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL" no form.
	if (fakeProgram == null) {
		fakeProgram = new Program();
		fakeProgram.id = -1;
		fakeProgram.name = bundle.getString("Bundle", "defaults.drawerProgram");
	}
	return fakeProgram;
}
// ------------------------
// Formatters
// ------------------------

private function formatHour(date:Date):String {
	return dateTimeUtil.toString(date, "JJhNN");
}
