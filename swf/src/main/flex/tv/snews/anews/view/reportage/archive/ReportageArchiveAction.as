import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import flexlib.controls.tabBarClasses.SuperTab;
import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.collections.Sort;
import mx.collections.SortField;
import mx.core.DragSource;
import mx.core.IFactory;
import mx.core.IFlexDisplayObject;
import mx.events.CalendarLayoutChangeEvent;
import mx.events.CollectionEvent;
import mx.managers.DragManager;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ComboBox;
import spark.components.Group;
import spark.components.TextInput;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.IGridItemRenderer;
import spark.events.GridEvent;
import spark.events.GridSelectionEvent;
import spark.events.IndexChangeEvent;
import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.ReportageParams;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.reportage.ReportageForm;
import tv.snews.anews.view.reportage.ReportageHelper;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

[ArrayElementType("tv.snews.anews.domain.User")]
[Bindable] private var reportersWithFake:ArrayCollection = null;
[Bindable] private var selectedType:String = null;
[Bindable] private var showViewer:Boolean = false;
[Bindable] private var selectedReportage:Reportage;
[Bindable] private var total:int = 0;
[Bindable] private var helper:ReportageHelper = new ReportageHelper();

[Bindable] public var tabManager:TabManager = new TabManager();
[Bindable] public var programs:ArrayCollection = DomainCache.programs;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportageService:ReportageService	= ReportageService.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

private var dateUtil:DateUtil = Util.getDateUtil();
private var fakeProgram:Program = null;
private var reporterFake:User;

private function startUp():void {
	DomainCache.reporters.addEventListener(CollectionEvent.COLLECTION_CHANGE, onReportersCollectionChange);
	navigator.addEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false, 0, true);
	loadProgramFake();
	this.addEventListener(CrudEvent.EDIT, onEditEvent);
	tabManager.tabNavigator = navigator;
	callLater(function ():void { tabManager.setClosePolicyForTab(0, SuperTab.CLOSE_NEVER)});
}

//------------------------------------------------------------------------------
//	Actions
//------------------------------------------------------------------------------
private function tabClosedHandler(event:SuperTabEvent):void {
	//Verifica detalhes do objeto da tab antes de fechar	
	if (navigator.getChildAt(event.tabIndex) is ReportageForm) {
		event.preventDefault();
		var obj:ReportageForm = navigator.getChildAt(event.tabIndex) as ReportageForm;
		obj.close();
	}
}

private function onClickEdit():void {
	if (!ANews.USER.allow("010603")) {
		return;
	}
	
	var selectedReportage:Reportage = reportageGrid.selectedItem as Reportage;
	if (selectedReportage) {
		reportageService.isLockedForEdition(selectedReportage.id, onCheckLockedSuccess);
	} else {
		ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.selectOneGuideline"), Info.WARNING);
	}
}

private function onCheckLockedSuccess(event:ResultEvent):void {
	var user:User = event.result as User;
	if (user) {
        helper.reportageInEditing(user, tabManager, "reportage_" + selectedReportage.id, bundle.getString("Bundle", "reportage.title"));
	} else {
		var crudEvent:CrudEvent = new CrudEvent(CrudEvent.EDIT);
		crudEvent.entity = selectedReportage;
		this.dispatchEvent(crudEvent);
	}
}

private function onEditEvent(event:CrudEvent):void {
	if (event.entity is Reportage) {
		var reportage:Reportage = event.entity as Reportage;
		
		if (reportage.excluded) {
			ANews.showInfo(bundle.getString("Bundle", "defaults.msg.noEditDeleted"), Info.WARNING);
			return;
		}
		
		reportageService.loadById(reportage.id, function(event:ResultEvent):void {
			
			var result:Reportage = event.result as Reportage;
			tabManager.openTab(ReportageForm, "reportage_" + result.id,
				{
					reportage: Reportage(ObjectUtil.copy(result)),
					tabManager: tabManager
				}
			);
			
		});
		
		// Bloqueia a edição da pauta para os outros usuários
		reportageService.lockEdition(reportage.id, ANews.USER, null);
	}
}

public function onRemove():void {
	DomainCache.reporters.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onReportersCollectionChange);
	navigator.removeEventListener(SuperTabEvent.TAB_CLOSE, tabClosedHandler, false);
	for each (var tab:Object in navigator.getChildren()) {
		if (tab is ReportageForm) {
			var obj:ReportageForm = tab as ReportageForm;
			obj.onRemove();
		}
	}
	tabManager.removeAllTabs();
}

/**
 * Handler para mudança na lista de repórteres do DomainCahe.
 */
private function onReportersCollectionChange(event:CollectionEvent):void {
	refreshListReportersWithFake();	
}

/**
 * Carrega lista de reportagens no state Reportagens Gerais.
 */
private function loadSearchListReportage():void {
	showViewer = false;
	if (periodIsInvalid()) {
		ANews.showInfo(bundle.getString('Bundle', 'story.archive.msg.invalidPeriod'), Info.WARNING);
	} else {
		refreshDataGrid();
	}
}

/**
 * Gera impressão da pauta selecionada.
 */
private function print(event:Event):void {
	var reportage:Reportage = reportageGrid.selectedItem as Reportage;
	
	if(reportage) {
		var params:Object = new Object();
		params.ids = reportage.id + "|";
		reportUtil.generate(ReportsUtil.REPORTAGE, params);
	}
}

private function printGrid():void {
    var params:Object = new Object();
    var program:Program = (programCombo.selectedItem as Program);
    var reporter:User = (reporterCombo.selectedItem as User);
    params.periodStart = periodStart.text;
    params.periodEnd = periodEnd.text;
    params.programId =  program != null ? program.id : "";
    params.reporterId = reporter != null ? reporter.id : "";
    params.slug = slugInput.text;
    params.text = searchText.text;
    params.type = (searchType.selectedItem as Object).id;
    params.ignoreText = ignoreText.text;

    if(total > 100) {
        ANews.showInfo(bundle.getString('Bundle', "defaults.msg.listTheFirstHundred"), Info.WARNING);
    }

    reportUtil.generate(ReportsUtil.REPORTAGE_ARCHIVE_GRID, params);
}


private function onClear(event:MouseEvent):void {
	// Limpa os campos
	periodStart.selectedDate = null;
	periodEnd.selectedDate  = null;
    slugInput.text = "";
	searchText.text = "";
	programCombo.selectedIndex = -1;
	reporterCombo.selectedIndex = -1;
	selectedType = "";
	pageBar.data = null;
	searchType.selectedIndex = 0;
	ignoreText.text = "";
	pageBar.load(null,0,0,0);
	showViewer = false;

    total = 0;
}

protected function onChangeSearchText(event:TextOperationEvent):void {
	var obj:TextInput = event.target as TextInput;
	if (obj.text.length == 0) {
		obj.errorString = "";
	}
}

/**
 * Método que atualiza  o componente com a lista de reportagens conforme
 * o state.
 */
private function refreshDataGrid():void {
	selectedType = searchType.selectedItem.id;
	search();
}

private function search(page:int=1):void {
	page = page < 1 ? 1 : page;
	reportageGrid.selectedIndex = -1;
	
	var searchParams:ReportageParams = new ReportageParams();
	searchParams.initialDate = periodStart.selectedDate;
	searchParams.finalDate = periodEnd.selectedDate;
    searchParams.slug = StringUtils.trim(slugInput.text);
	searchParams.text = StringUtils.trim(searchText.text);
	searchParams.ignoreText = StringUtils.trim(ignoreText.text);
	searchParams.searchType = selectedType;
	searchParams.program = programCombo.selectedItem as Program;
	searchParams.reporter = reporterCombo.selectedItem as User;
	searchParams.page = page;
    selectedReportage = null;
	
	reportageService.reportageByFullTextSearch(searchParams, onListSuccess);
}

//Verifica se os dados do paginador existem e se não existe alguma reportagem sendo copiada, caso contrário, não permite a troca de páginas
private function onPageChange(page:int=1):void {
    if(pageBar.data && !copyTo.visible) {
        search(page);
    }
}

private function onViewClick(event:MouseEvent):void{
	showViewer = true;
}

private function onCancelClick(event:MouseEvent):void {
	showViewer = false;
}

protected function onChangeProgramCombo(event:IndexChangeEvent):void {
	if (event.target is ComboBox) {
		var cb:ComboBox = event.target as ComboBox;
		if (cb.selectedItem is String) {
			cb.textInput.text = "";
			cb.selectedItem = null;
			cb.selectedIndex = -1;
		}
	}
}

/**
 * Drag n' drop to chat
 */
private function onMouseDrag(event:GridEvent):void {
	if (DragManager.isDragging || (reportageGrid.selectedItem as Reportage) == null)
		return;
	
	// Start the dragging.
	if (event.itemRenderer != null) {
		var sourceItens:Vector.<Object> = new Vector.<Object>();
		sourceItens.push(ObjectUtil.copy(reportageGrid.selectedItem) as Reportage)
		
		var dataSource:DragSource = new DragSource();
		dataSource.addData(sourceItens, "itemsByIndex");
		
		// Create the proxy (visual element that will be drag with cursor over the elements)
		var proxy:Group = new Group();
		proxy.width = reportageGrid.grid.width;
		
		for each (var columnIndex:int in reportageGrid.grid.getVisibleColumnIndices() as Vector.<int>) {
			var currentRenderer:IGridItemRenderer = reportageGrid.grid.getItemRendererAt(reportageGrid.selectedIndex, columnIndex);
			var factory:IFactory = reportageGrid.columns.getItemAt(columnIndex).itemRenderer;
			if (!factory)
				factory = reportageGrid.itemRenderer;
			var renderer:IGridItemRenderer = IGridItemRenderer(factory.newInstance());
			renderer.visible = true;
			renderer.column = currentRenderer.column;
			renderer.rowIndex = currentRenderer.rowIndex;
			renderer.label = currentRenderer.label;
			renderer.x = currentRenderer.x;
			renderer.y = currentRenderer.y;
			renderer.width = currentRenderer.width;
			renderer.height = currentRenderer.height;
			renderer.prepare(false);
			proxy.addElement(renderer);
			renderer.owner = reportageGrid;
		}
		proxy.height = renderer.height;
		
		// Start the drag.
		DragManager.doDrag(reportageGrid, dataSource, event, proxy as IFlexDisplayObject, 0, -reportageGrid.columnHeaderGroup.height);
	}
}


private function loadProgramFake():void {
	programs = ObjectUtil.copy(DomainCache.programs) as ArrayCollection;	
	if (programs) {
		programs.addItemAt(getFakeProgram(), 0);
	}
}

private function getFakeProgram():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL" no form.
	if (fakeProgram == null) {
		fakeProgram = new Program();
		fakeProgram.id = -1;
		fakeProgram.name = this.resourceManager.getString("Bundle", "defaults.drawerProgram");
	}
	return fakeProgram;
}

// -----------------------------------------------------------------------------
// Asynchronous Answers.
// -----------------------------------------------------------------------------
private function onListSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
    total = result.total;
	pageBar.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function onFault(event:FaultEvent):void {
	var exceptionMessage:String = event.fault.rootCause.message as String;
	ANews.showInfo(exceptionMessage, Info.ERROR);
}

protected function onChangeInitialDate(event:CalendarLayoutChangeEvent):void {
	if (periodStart.selectedDate && periodEnd.selectedDate) {
		var val:int = dateUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
		if (val > 0) {
			periodEnd.selectedDate = periodStart.selectedDate;
		} 
	}
}

protected function onChangeFinalDate(event:CalendarLayoutChangeEvent):void {
	if (periodStart.selectedDate && periodEnd.selectedDate) {
		var val:int = dateUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
		if (val > 0 ) {
			periodStart.selectedDate = periodEnd.selectedDate;
		}
	}
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && reportageGrid.selectedItem != null && reportageGrid.selectedIndex != -1) {
		if (showViewer == true) {
			showViewer = false;
		} else {
			showViewer = true;
		}
	}
}

private function onSearchKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        loadSearchListReportage();
    }
}

// -----------------------------------------------------------------------------
// Helpers.
// -----------------------------------------------------------------------------

/**
 * Atualiza a lista de reporters com o usuário fake.
 */
private function refreshListReportersWithFake():void {
	reportersWithFake = ObjectUtil.copy(DomainCache.reporters) as ArrayCollection;	
	if(reportersWithFake) {
		var nameField:SortField = new SortField();
		nameField.name = "nickname";
		nameField.numeric = false;
		nameField.caseInsensitive = true;
		var sort:Sort = new Sort();
		sort.fields = [nameField];
		reportersWithFake.sort = sort;
		reportersWithFake.refresh();
	}
	
	reportersWithFake.addItemAt(getReporterFake(), 0);
}

/**
 * Retorna um reporter fake para opção 'todos' do combobox.
 */
private function getReporterFake():User {
	if (reporterFake == null) {
		reporterFake = new User();
		reporterFake.id = 0;
		reporterFake.nickname = " " + bundle.getString('Bundle', 'defaults.all');
	}
	return reporterFake;
}

private function periodIsInvalid():Boolean {
	var start:Date = periodStart.selectedDate;
	var end:Date = periodEnd.selectedDate;
	return start && end && start.time > end.time;
}

// -----------------------------------------------------------------------------
// Formatters
// -----------------------------------------------------------------------------
private function dateLabelFunction(item:Object, column:GridColumn):String {
	return item[column.dataField] ? dateUtil.dateToString(item[column.dataField]) : "";
}


private function onSelectionChange(event:GridSelectionEvent):void {
    selectedReportage = reportageGrid.selectedItem as Reportage;
}

private function confirmCopy():void {
    var date:Date = destinyDay.selectedDate;
    var program:Program = destinyProgram.selectedItem as Program;

    reportageService.copyReportage(selectedReportage.id, date, program, function(event:ResultEvent):void {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "defaults.msg.copySuccess"), Info.INFO);
        cancelCopyTo();
        onPageChange();
    });
}

private function cancelCopyTo():void {
    copyTo.visible = false;
    copyTo.includeInLayout = false;
    selectedReportage = null;
    reportageGrid.selectedIndex = -1;
    showViewer = false;
}

private function onCopyClick(event:MouseEvent):void {
    copyTo.visible = true;
    copyTo.includeInLayout = true;
}
