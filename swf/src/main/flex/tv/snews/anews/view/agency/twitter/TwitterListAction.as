import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.TwitterUser;
import tv.snews.anews.service.TwitterService;
import tv.snews.flexlib.events.PageEvent;
import tv.snews.anews.utils.*;

private const twitterService:TwitterService = TwitterService.getInstance();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.TwitterUser")]
private var twitterUsers:ArrayCollection = new ArrayCollection();

private var currentTwitterUser:TwitterUser = null;

/** 
 * referência utilizada para controlar quando deve-se listar
 * todos twitters no componente de apresentação.
 */
private var fakeTwitterUser:TwitterUser = null;


/**
 * Cria um usuário fake para ser adicionado ao componente de visualização
 * para ser o  'TODOS'
 * Não responsabildiade do método adicionar o usuário ao componente. 
 */
private function loadFakeTwitterUser():void {
	if (fakeTwitterUser != null) {
		return;
	}
	
	fakeTwitterUser = new TwitterUser();
	fakeTwitterUser.id = 0;
	fakeTwitterUser.screenName = resourceManager.getString('Bundle' , "twitter.title").toUpperCase();
}

/**
 * Executa as ações de inicialização dos componentes Twitter User.
 */
private function initTwitter():void {
	loadFakeTwitterUser();
	configureListenersTwitter();
	
	// Carrega a lista de twitterUsers cadastrados para o combobox
	refreshTwitterUserCombo();
	
	twitterService.subscribe(onMessageReceivedTwitterHandler);
}

public function dispose():void {
	twitterService.unsubscribe(onMessageReceivedTwitterHandler);
}

private function onMessageReceivedTwitterHandler(event:MessageEvent):void {
	var twitterUser:TwitterUser = event.message.body as TwitterUser;
	var type:String = event.message.headers["type"] as String;
	
	if (type == TwitterService.UPDATED_TWEETS_TYPE) {
		
		//Se é usuário fake está selecionado, atualizar todos.
		if (currentTwitterUser && currentTwitterUser.id == 0) {
			refreshAllTweetsList();
		} else {
			if (twitterUser && currentTwitterUser == null) {
				currentTwitterUser = twitterUser;
				refreshSingleTweetsList();
			}
	
			if (currentTwitterUser && currentTwitterUser.id == twitterUser.id) {
				refreshSingleTweetsList();
			} 
		}
	} else {
		if (type == TwitterService.DELETED_TWITTER_USER_TYPE || type == TwitterService.INSERTED_TWITTER_USER_TYPE) {
			refreshTwitterUserCombo();
			if (currentTwitterUser && type == TwitterService.DELETED_TWITTER_USER_TYPE 
				&& currentTwitterUser.id == twitterUser.id) {
				if (twitterUsers.length > 0) {
					currentTwitterUser = twitterUsers.getItemAt(0) as TwitterUser;
					refreshAllTweetsList();
				}
			} else {
				if (twitterUsers.length == 1) {
					currentTwitterUser = twitterUsers.getItemAt(0) as TwitterUser;
					refreshAllTweetsList();
				}
			}
		}
	}
}

private function configureListenersTwitter():void {
	twitterUsers.addEventListener(CollectionEvent.COLLECTION_CHANGE, onTwitterUserChange);
}

//--------------------------------------
//	Methods for single Twitter User
//--------------------------------------

private function refreshTwitterUserCombo():void {
	twitterService.listAllTwitterUser(function(event:ResultEvent):void {
		twitterUsers.removeAll();
		//Por segurança será adicionado a coleção de usuários uma vez apenas, 
		//para evitar chamada indevida no listener de collection change
		var twitterUsersAux:ArrayCollection = new ArrayCollection();
		
		twitterUsersAux.addAll(event.result as ArrayCollection);
		//Adiciona sempre o usuário fake na primeira posição
		twitterUsersAux.addItemAt(fakeTwitterUser, 0);
		
		//Se liga que tem listenter CollectionEvent.COLLECTION_CHANGE
		twitterUsers.addAll(twitterUsersAux);
		
		if (twitterUsers.length == 1) {
			allTweetsInfoList.clear();
		} else {
			// Quando havia um item selecionado, e ele ainda existe, seleciona ele novamente
			if (currentTwitterUser != null && !currentTwitterUser.id == 0) {
				for (var i:int = 0, aux:TwitterUser; i < twitterUsers.length; i++) {
					aux = twitterUsers.getItemAt(i) as TwitterUser;
					if (aux.id == currentTwitterUser.id) {
						twittersCombo.selectedIndex = i;
						break;
					}
				}
			} 
		}
	});
}

/**
 * Recarrega os tweets de um twitter user.
 */
private function refreshSingleTweetsList():void {
	if (allTweetsInfoList.count != 0) {
		allTweetsInfoList.clear();
	}
	twitterService.countTweetsFromTwitterUser(currentTwitterUser, function(event:ResultEvent):void {
		allTweetsInfoList.count = event.result as int;
	});
}

/**
 * Recarrega os tweets de Todos twitter users.
 */
private function refreshAllTweetsList():void {
	if (allTweetsInfoList.count != 0) {
		allTweetsInfoList.clear();
	}
	twitterService.countTweetsInfoFromAllTwitterUsers(function(event:ResultEvent):void {
		allTweetsInfoList.count = event.result as int;
	});
}

/**
 * Verifica se o twitter user selecionado .
 */
private function updateTweetsList():void {
	var selectedTwitterUser:TwitterUser = twittersCombo.selectedItem as TwitterUser;
	
	if (selectedTwitterUser) {
		if (selectedTwitterUser.id == 0)  { //fakeTwitterUser
			currentTwitterUser = selectedTwitterUser;
			refreshAllTweetsList();
		} else {
			if (currentTwitterUser == null || selectedTwitterUser.id != currentTwitterUser.id) {
				currentTwitterUser = selectedTwitterUser;
				refreshSingleTweetsList();
			}
		}
	} else {
		twittersCombo.selectedIndex = 0;
	}
}

private function onTwitterUserChange(event:CollectionEvent):void {
	updateTweetsList();
}

private function onTwitterUserComboChange(event:IndexChangeEvent):void {
	updateTweetsList();
}

/**
 * Handler do evento "pagePending" para buscar a página tweets que ainda não foi carregada
 * Serve tanto para todos twitter users quanto para um único selecionado.
 */
private function onTwitterUserPagePendingHandler(event:PageEvent):void {
	
	if (currentTwitterUser.id == 0) { //Todos
		twitterService.listTweetsInfoFromAllTweetUsers(event.firstResult, event.maxResults,
			onListTweetsSuccess, onListTweetsFailed
		);
	} else {
		twitterService.listTweetsInfoFromTwitterUser(currentTwitterUser, event.firstResult, event.maxResults,
			onListTweetsSuccess, onListTweetsFailed
		);
	}
}

/**
 * Handler para o carregamento bem sucedido da lista de tweets.
 */
private function onListTweetsSuccess(event:ResultEvent, token:Object = null):void {
	allTweetsInfoList.appendItems(event.result as ArrayCollection, token as int);
}

/**
 * Handler para a falha de carregamento da lista de tweets.
 */
private function onListTweetsFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(resourceManager.getString('Bundle' , "twitter.msg.failedToLoadTweetsInfo"), Info.ERROR);
}

private function formatLabelTwitter(item:Object):String {
	var twitterUser:TwitterUser = item as TwitterUser;
	if (twitterUser != null) {
		return twitterUser.id != 0 ? twitterUser.name + '   ' + '@' + twitterUser.screenName : twitterUser.screenName;
	}
	return "";
}
