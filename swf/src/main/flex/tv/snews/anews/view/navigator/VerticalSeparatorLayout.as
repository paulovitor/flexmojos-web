package tv.snews.anews.view.navigator {

	import spark.components.supportClasses.GroupBase;
	import spark.layouts.VerticalLayout;

	public class VerticalSeparatorLayout extends VerticalLayout {
		private var _cachedItem:Object;

		public function VerticalSeparatorLayout() {
			super();
		}

		override public function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			if (_cachedItem) {
				_cachedItem.visible = true;
				_cachedItem = null;
			}

			super.updateDisplayList(unscaledWidth, unscaledHeight);

			var layoutTarget:GroupBase = target;
			if (!layoutTarget)
				return;

			var count:uint = layoutTarget.numElements;

			if ((count == 0) || (unscaledWidth == 0) || (unscaledHeight == 0)) {

				return;
			}

			var rend:Object = layoutTarget.getElementAt(count - 1);

			if (rend.hasOwnProperty("separator")) {
				rend["separator"].visible = false;
				_cachedItem = rend;
			}
		}
	}
}
