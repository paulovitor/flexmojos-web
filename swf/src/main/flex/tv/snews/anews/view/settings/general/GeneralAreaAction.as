import flash.events.*;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const settingsService:SettingsService = SettingsService.getInstance();
private const mediaCenterService:MediaCenterService = MediaCenterService.getInstance();
private const deviceService:DeviceService = DeviceService.getInstance();

private const locales:ArrayCollection = new ArrayCollection(
		[
			{ value: "en_US", label: "English" },
			{ value: "es_ES", label: "Español" },
			{ value: "pt_BR", label: "Português" }
		]
);

private const protocols:ArrayCollection = new ArrayCollection(
		[
			{ value: "http://", label: "http://" },
			{ value: "https://", label: "https://" },
			{ value: "rtmp://", label: "rtmp://" },
			{ value: "rtmpe://", label: "rtmpe://" },
			{ value: "rtmpt://", label: "rtmpt://" },
			{ value: "rtmpte://", label: "rtmpte://" },
			{ value: "rtmps://", label: "rtmps://" }
		]
);

[Bindable] private var settings:Settings;
[Bindable] private var selectedDevice:MosDevice = new MosDevice();
[Bindable] private var devices:ArrayCollection = new ArrayCollection();

protected function startUp(event:FlexEvent):void {
	settingsService.loadSettings(onLoadSettingsHandler);
	deviceService.subscribe(onDeviceMessageReceived);
	listDevices();
}

protected function onRemove(event:FlexEvent):void {
	deviceService.unsubscribe(onDeviceMessageReceived);
}

private function onLoadSettingsHandler(event:ResultEvent):void {
	settings = Settings(event.result);
	selectCurrentLocale();
	selectCurrentProtocol(settings.protocol);
}

private function selectCurrentLocale():void {
	for (var i:int = 0; i < locales.length; i++) {
		if (locales[i].value == settings.locale) {
			localesCombo.selectedIndex = i;
		}
	}
}

private function selectCurrentProtocol(protocol:String):void {
	for (var i:int = 0; i < protocols.length; i++) {
		if (protocols[i].value == protocol) {
			protocolsCombo.selectedIndex = i;
		}
	}
}

protected function onSaveButtonClick(event:MouseEvent):void {
    var hostStr:String = hostInput.text;
    var rootDirectoryStr:String = rootDirectoryInput.text;
	if (Util.isValid(validators)) {
        if(hostStr.length == 0 || hostStr.search("/") > -1) {
            hostInput.errorString = bundle.getString('Bundle', 'settings.msg.invalidHost');
            ANews.showInfo(bundle.getString('Bundle', 'settings.msg.invalidHost'), Info.WARNING);
            return;
        } else {
            hostInput.errorString = "";
        }
        if(rootDirectoryStr.length == 0
                || !(rootDirectoryStr.indexOf("/") == 0 &&
                rootDirectoryStr.lastIndexOf("/") + 1 == rootDirectoryStr.length)) {
            rootDirectoryInput.errorString = bundle.getString('Bundle', 'settings.msg.invalidRootDirectory');
            ANews.showInfo(bundle.getString('Bundle', 'settings.msg.invalidRootDirectory'), Info.WARNING);
            return;
        } else {
            rootDirectoryInput.errorString = "";
        }
        settings.locale = localesCombo.selectedItem.value;
        settings.protocol = protocolsCombo.selectedItem.value;
        settingsService.updateSettings(settings, onUpdateResultHandler);
	} else {
		ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
	}
}

private function onUpdateResultHandler(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', 'settings.msg.saveSuccess'), Info.INFO);
}

private function reindexDatabase(event:Event):void {
	reindexButton.enabled = false;

	settingsService.reindexDatabase(
		function(event:ResultEvent):void {
			reindexButton.enabled = true;
		}
	);
}

private function listDevices():void {
	deviceService.findAllMosDeviceByType(DeviceType.MAM,
		function(event:ResultEvent):void {
			devices = ArrayCollection(event.result);
		}
	);
}

private function synchronizeMediaServer(event:Event):void {
	if (selectedDevice != null && selectedDevice.profileOneSupported) {
		mediaCenterService.mosReqAll(selectedDevice, true);
	}
}

/**
 * Notificador de MosDevice, monitora os inserts, updates e deletes dos devices.
 * 
 **/
private function onDeviceMessageReceived(event:MessageEvent):void {
    if (event.message.body is MosDevice) {
        var action:String = String(event.message.headers["action"]);
        var device:MosDevice = MosDevice(event.message.body);
        var aux:MosDevice;

        if (device.containsType(DeviceType.MAM)) {
            switch (action) {
                case EntityAction.INSERTED:
                    devices.addItem(device);
                    break;
                case EntityAction.UPDATED:
                    for each (aux in devices) {
                        if (aux.equals(device)) {
                            aux.update(device);
                            break;
                        }
                    }
                    break;
                case EntityAction.DELETED:
                    for (var i:int = 0; i < devices.length; i++) {
                        aux = devices.getItemAt(i) as MosDevice;
                        if (aux.equals(device)) {
                            devices.removeItemAt(i);
                            break;
                        }
                    }
                    break;
            }
        }
    }
}

private function handleDefaultStreaming(event:Event):void {
        settings.otherHostActive = event.target.id == "anews" || event.target.id == "activeStreaming" ? false : true;
        if(settings.otherHostActive) {
            settings.protocol = "";
            settings.host = "";
            settings.rootDirectory = "";
        } else {
            selectCurrentProtocol("rtmp://");
            settings.host = getURLStreamingRoot();
            settings.rootDirectory = "/snews/";
        }
}

public function getURLStreamingRoot():String {
    var host:String = FlexGlobals.topLevelApplication.url;
    var splitURL:Array = host.split('/');
    if(host) {
        host = splitURL[2];
        host = host.substring(0, host.lastIndexOf(":"));
    }
    return host;
}