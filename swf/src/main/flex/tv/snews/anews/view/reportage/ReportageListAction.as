import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.DragSource;
import mx.core.IFactory;
import mx.core.IFlexDisplayObject;
import mx.core.IUIComponent;
import mx.events.CalendarLayoutChangeEvent;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.Group;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.IGridItemRenderer;
import spark.events.GridEvent;
import spark.events.GridSelectionEvent;
import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.component.TaskBoardEvent;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.search.ReportageParams;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.reportage.ReportageForm;
import tv.snews.flexlib.utils.DateUtil;

private const dateUtil:DateUtil = Util.getDateUtil();
private const reportageService:ReportageService = ReportageService.getInstance();
private const guidelineService:GuidelineService = GuidelineService.getInstance();
private const storyService:StoryService = StoryService.getInstance();
private const documentService:DocumentService = DocumentService.getInstance();

[Bindable] private var programDrawer:Program;
[Bindable] private var programsWithFake:ArrayCollection;
[Bindable] private var helper:ReportageHelper = new ReportageHelper();
[Bindable] private var guidelines:ArrayCollection = new ArrayCollection();
[Bindable] private var reportages:ArrayCollection = new ArrayCollection();
[Bindable] private var taskBoardReportages:ArrayCollection = new ArrayCollection();
[Bindable] private var currentGuidelineStateIndex:int = 0;
[Bindable] private var isOpened:Boolean = false;
private const dateTimeUtil:DateUtil = new DateUtil();

[Bindable] public var tabManager:TabManager = null;

private var _selectedReportage:Reportage;

[Bindable]
public function get selectedReportage():Reportage {
	return _selectedReportage;
}

public function set selectedReportage(value:Reportage):void {
	_selectedReportage = value;
	currentState = value ? "selected" : "normal";

	cancelSendToDrawer();
}

private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

// ---------------------------------
// Events
// ---------------------------------
protected function startUp(event:FlexEvent):void {
	reportageService.subscribe(onReportageMessageHandler);
	guidelineService.subscribe(onGuidelineMessageHandler);

	loadReportages();
	loadProgramsWithFake();

	// Carrega o cache antecipadamente.
	DomainCache.reporters == DomainCache.programs;
	
	// Event Handler para o double-click da visualização de todos os programas e do taskBoard
	this.addEventListener(CrudEvent.EDIT, function(event:CrudEvent):void {
		edit();
	});
	
	this.addEventListener(CrudEvent.CREATE, function(event:CrudEvent):void {
		var newReportage:Reportage = new Reportage();
		if (event.entity is Guideline) {
			var guideline:Guideline = Guideline(event.entity);
			newReportage = new Reportage(guideline);
			newReportage.date = new Date();
		} else { // tipo desconhecido
			return;
		}
		
		newReportage.author = ANews.USER;
		openForm(newReportage);
	});
}

protected function onRemove(event:Event):void {
	reportageService.unsubscribe(onReportageMessageHandler);
	guidelineService.unsubscribe(onGuidelineMessageHandler);
}

/**
 * Drag n' drop to chat
 */
private function onMouseDrag(event:GridEvent):void {
	if (DragManager.isDragging || selectedReportage == null)
		return;
	
	// Start the dragging.
	if (event.itemRenderer != null) {
		var sourceItens:Vector.<Object> = new Vector.<Object>();
		sourceItens.push(ObjectUtil.copy(selectedReportage));
		
		var dataSource:DragSource = new DragSource();
		dataSource.addData(sourceItens, "itemsByIndex");
		
		// Create the proxy (visual element that will be drag with cursor over the elements)
		var proxy:Group = new Group();
		proxy.width = reportageGrid.grid.width;
		
		for each (var columnIndex:int in reportageGrid.grid.getVisibleColumnIndices() as Vector.<int>) {
			var currentRenderer:IGridItemRenderer = reportageGrid.grid.getItemRendererAt(reportageGrid.selectedIndex, columnIndex);
			var factory:IFactory = reportageGrid.columns.getItemAt(columnIndex).itemRenderer;
			if (!factory)
				factory = reportageGrid.itemRenderer;
			var renderer:IGridItemRenderer = IGridItemRenderer(factory.newInstance());
			renderer.visible = true;
			renderer.column = currentRenderer.column;
			renderer.rowIndex = currentRenderer.rowIndex;
			renderer.label = currentRenderer.label;
			renderer.x = currentRenderer.x;
			renderer.y = currentRenderer.y;
			renderer.width = currentRenderer.width;
			renderer.height = currentRenderer.height;
			renderer.prepare(false);
			proxy.addElement(renderer);
			renderer.owner = reportageGrid;
		}
		proxy.height = renderer.height;
		
		// Start the drag.
		DragManager.doDrag(reportageGrid, dataSource, event, proxy as IFlexDisplayObject, 0, -reportageGrid.columnHeaderGroup.height);
	}
}

private function onViewHandler(event:MouseEvent):void {
//	this.currentState = "selected";
	isOpened = true;
    reportageService.loadById(selectedReportage.id, function (event:ResultEvent):void {
        reportageViewer.reportage = Reportage(event.result);
    });
}

private function onCancelHandler(event:MouseEvent):void {
//	this.currentState = "normal";
	isOpened = false;
}

private function onOpenOrCloseDrawer(event:Event):void {
	reportageGrid.selectedIndex = -1;
	currentState = currentState == "drawer" ? "normal" : "drawer";
//	isOpened ? this.currentState = "normal" : this.currentState = "drawer";
//	isOpened = !isOpened;
}


private function buttonBarChange(event:IndexChangeEvent):void {
	loadReportages();
}

private function onDragOver(event:DragEvent):void {
	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
	if (items && items.length > 0) {
		var dragItem:Object = items[0];
		if (dragItem is Guideline) {
			DragManager.showFeedback(DragManager.LINK);
		} else {
			event.preventDefault();
			event.stopPropagation();
			DragManager.showFeedback(DragManager.NONE);
		}
	}
}

private function onDragDrop(event:DragEvent):void {
	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
	if (items && items.length > 0) {
		var newReportage:Reportage;
		var dragItem:Object = items[0];
		
		if (dragItem is Guideline) {
			var guideline:Guideline = Guideline(dragItem);
			newReportage = new Reportage(guideline);
			newReportage.date = new Date();
		} else { // tipo desconhecido
			return;
		}
		
		newReportage.author = ANews.USER;
		openForm(newReportage);
	}
}

private function onDragEnter(event:DragEvent):void {
	event.preventDefault();
	DragManager.acceptDragDrop(event.target as IUIComponent);
}

// ---------------------------------
// Actions
// ---------------------------------

private function loadProgramsWithFake():void {
	programsWithFake =  ObjectUtil.copy(DomainCache.programs) as ArrayCollection;
	programsWithFake.addItemAt(getProgramDrawer(), 0);
}

private function getProgramDrawer():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL" 
	if (programDrawer == null) {
		programDrawer = new Program();
		programDrawer.id = -1;
		programDrawer.name = bundle.getString("Bundle", "defaults.drawerProgram");
	}
	return programDrawer;
}

private function loadReportages(page:int = 1):void {
    if(copyReportageTo.visible) cancelCopyTo();

    if(buttonBarReportage.selectedIndex == 0) {
        var params:ReportageParams = new ReportageParams();
        params.slug = slugSearchInput.text;
        params.initialDate = periodStart.selectedDate;
        params.finalDate = periodEnd.selectedDate;
        params.page = page;
        reportageService.listAll(params, onReportageList);
    }
    if(buttonBarReportage.selectedIndex == 1) {
        reportageService.listAllReportageOfDay(date.selectedDate, null, onReportageListOfDay);
    }

}

private function newReportage():void {
	if (ANews.USER.allow("010602")) {
		var reportage:Reportage = new Reportage();
		reportage.date = dateUtil.timeZero();
		openForm(reportage);
	}
}

private function edit():void {
	if (!ANews.USER.allow("010603")) {
		return;
	}

	if (selectedReportage != null) {
		// Verifica se a reportagem já está sendo editada por outro
		reportageService.isLockedForEdition(selectedReportage.id,
				function(event:ResultEvent):void {
					var user:User = event.result as User;
					if (user == null) {
						// Bloqueia a edição da reportagem para os outros usuários
						reportageService.lockEdition(selectedReportage.id, ANews.USER,
								function(event:ResultEvent):void {
									var copy:Reportage = ObjectUtil.copy(selectedReportage) as Reportage;
									copy.calculateReadTime();
									openForm(copy);
								}
						);
					} else {
                        helper.reportageInEditing(user, tabManager, "reportage_" + selectedReportage.id, bundle.getString("Bundle", "reportage.archive.title"));
					}
				}
		);
	}
}

private function removeReportage():void {
	if (selectedReportage != null) {
		reportageService.isLockedForEdition(selectedReportage.id,
				function(event:ResultEvent):void {
					var user:User = event.result as User;
					if (user) {
						ANews.showInfo(bundle.getString("Bundle", "defaults.msg.lockedMessage", [user.nickname]), Info.WARNING);
					} else {
						Alert.show(
							bundle.getString("Bundle", "defaults.msg.doYouWantDeleteItem"),
							bundle.getString("Bundle", "defaults.msg.confirmDelete"),
							Alert.NO | Alert.YES, null, confirmation);
					}
				}
		);
	}
}

private function copyTo():void {
	copyReportageTo.visible = true;
	copyReportageTo.includeInLayout = true;

	copyDateValidator.enabled = true;
	copyProgramValidator.enabled = true;
}

private function cancelCopyTo():void {
	copyReportageTo.visible = false;
	copyReportageTo.includeInLayout = false;

	copyDateValidator.enabled = false;
	copyProgramValidator.enabled = false;
}

private function saveCopy():void {
	if (!Util.isValid(copyFormValidators)) {
		ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
	} else {
		var date:Date = copyDateField.selectedDate;
		var program:Program = Program(copyProgramCombo.selectedItem);
		
		if (program && program.id == -1) { //Gaveta Geral
			program = null; 
		}

		// Copia a pauta remota para uma local
		reportageService.copyReportage(selectedReportage.id, date, program,
				function (event:ResultEvent):void {
					copyReportageTo.visible = false;
					copyReportageTo.includeInLayout = false;

					copyDateValidator.enabled = false;
					copyProgramValidator.enabled = false;

					ANews.showInfo(bundle.getString('Bundle', "defaults.msg.copySuccess"), Info.INFO);
				}
		);
	}
}

private function confirmation(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		reportageService.remove(selectedReportage.id, onRemoveSuccess);
	}
}
private function print():void {
	var params:Object = new Object();
	params.ids = selectedReportage.id + "|";
	reportUtil.generate(ReportsUtil.REPORTAGE, params);
}

/**
 * Esse método controla os formulários de reportagens que são abertos em tabs
 * para evitar abrir documentos repetidos. Ele é publico para que outras classes
 * possam fazer isso através de ReportageList.
 * 
 * @param reportage a reportagem a ser exibida no formulário.
 */ 

private function openForm(reportage:Reportage):void {
	var tabId:String;

	if (reportage.id) {
        reportageService.loadById(reportage.id, function (event:ResultEvent):void {
            var result:Reportage = Reportage(event.result);
            tabId = "reportage_" + result.id;
            tabManager.openTab(ReportageForm, tabId, { reportage: result, tabManager: tabManager });
        });

	} else {
		if (reportage.guideline) {
			tabId = "NEW|guideline_" + reportage.guideline.id;
		} else {
			tabId = "NEW";
		}
        tabManager.openTab(ReportageForm, tabId, { reportage: reportage, tabManager: tabManager });
	}
}

public function getStateByIndex(i:int):String{
	switch(i) {
		case 0:  return DocumentState.COMPLETED;
		case 1:  return DocumentState.COVERING;
		case 2:  return DocumentState.PRODUCING;
		case 3:  return DocumentState.INTERRUPTED;
		default: return null;
	}
}

private function onKeyUpHandler(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && selectedReportage) {
		isOpened = isOpened == false;
	}
}

//------------------------------------------------------------------------------
//	Cache Restore
//------------------------------------------------------------------------------
// FIXME Cache desativado
//private function verifyCacheState():void {
//	cache = localData.data as Reportage;
//	if (cache != null) {
//		displayRestoreForm = true;
//	}
//}

// FIXME Cache desativado
//private function restoreFromCache():void {
//	if (cache.id > 0) {
//		reportageService.loadById(cache.id, function(event:ResultEvent):void {
//			var reportage:Reportage = event.result as Reportage;
//			if (reportage) {
//				copyData(cache, reportage);
//				openForm(reportage);
//			} else {
//				ANews.showInfo(bundle.getString('Bundle' , "guideline.daily.msg.restoreFailed"), Info.WARNING);
//			}
//		});
//	}
//}

// FIXME Cache desativado
//protected function onRestoreHandler(event:MouseEvent):void {
//	restoreFromCache();
//	localData.clear();
//	displayRestoreForm = false;
//}

// FIXME Cache desativado
//protected function onIgnoreHandler(event:MouseEvent):void {
//	localData.clear();
//	displayRestoreForm = false;
//}

// FIXME Cache desativado

//private function copyData(from:Reportage, to:Reportage):void {
// Acredito que não é necessário copiar o objeto da pauta, as informações de exclusão e a data
//	to.sections = from.sections;

// Se for copiar mais campos faça assim:
//	var fieldsToCopy:Array = [ "date", "sections" ];
//	for each (var field:String in fieldsToCopy) {
//		to[field] = from[field] ;
//	}
//} 

// ---------------------------------
// Asynchronous Answers.
// ---------------------------------
private function onFault(event:FaultEvent):void {
	var exceptionMessage:String = event.fault.rootCause.message as String;
	ANews.showInfo(exceptionMessage, Info.ERROR);
}

private function onReportageListOfDay(event:ResultEvent):void {
    taskBoardReportages.removeAll();
    taskBoardReportages.addAll(event.result as ArrayCollection);
    selectedReportage = null;
}

private function onReportageList(event:ResultEvent):void {
    var result:PageResult = event.result as PageResult;

    reportages.removeAll();
    reportages.addAll(result.results as ArrayCollection);

    pageBar.load(reportages, result.pageSize, result.total, result.pageNumber);

	selectedReportage = null;
}

private function onGuidelineList(event:ResultEvent):void {
	guidelines = event.result as ArrayCollection;
}

private function onReportageMessageHandler(event:MessageEvent):void {
	var reportage:Reportage = event.message.body as Reportage;
	var action:String = event.message.headers.action;

    if (reportage == null)
        return;

	var index:*;

	switch (action) {
        case EntityAction.DELETED:
            if (buttonBarReportage.selectedIndex == 0) {
							index = getOldReportageIndex(reportage.id, reportages);
                if (index != null)
                    reportages.removeItemAt(index);
            } else if (buttonBarReportage.selectedIndex == 1) {
							index = getOldReportageIndex(reportage.id, taskBoardReportages);
                if (index != null)
                    taskBoardReportages.removeItemAt(index);
            }
            break;
        case EntityAction.FIELD_UPDATED:
        case EntityAction.LOCKED:
        case EntityAction.UNLOCKED:
        case EntityAction.UPDATED:
            var old:Reportage;
            if (buttonBarReportage.selectedIndex == 0) {
                old = getOldReportage(reportage.id, reportages);
            } else if (buttonBarReportage.selectedIndex == 1) {
                old = getOldReportage(reportage.id, taskBoardReportages);
            }

            if (old == null)
                return;

            switch (action) {
                case EntityAction.FIELD_UPDATED:
                    var field:String = event.message.headers["field"];
                    var value:Object = event.message.headers["value"];
                    old[field] = value;
                    break;
                case EntityAction.LOCKED:
                case EntityAction.UNLOCKED:
                    old.editingUser = action == EntityAction.LOCKED ? event.message.headers["user"] as User : null;
                    break;
                case EntityAction.UPDATED:
                    old.updateFields(reportage);
                    break;
            }
            break;
        case EntityAction.INSERTED:
        case EntityAction.RESTORED:
            if (buttonBarReportage.selectedIndex == 0) {
                reportages.addItemAt(reportage, 0);
            } else if (buttonBarReportage.selectedIndex == 1) {
                taskBoardReportages.addItemAt(reportage, 0);
            }
            break;
	}
}

private function getOldReportage(reportageId:Number, reportages:ArrayCollection):Reportage {
    var old:Reportage;
    for (var index:int = 0; index < reportages.length; index++) {
        old = reportages.getItemAt(index) as Reportage;
        if (old.id == reportageId) {
            return old;
        }
    }
    return null;
}

private function getOldReportageIndex(reportageId:Number, reportages:ArrayCollection):* {
    var old:Reportage;
    for (var index:int = 0; index < reportages.length; index++) {
        old = reportages.getItemAt(index) as Reportage;
        if (old.id == reportageId) {
            return index;
        }
    }
    return null;
}

private function onRemoveSuccess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', "reportage.msg.removeSuccess"), Info.INFO);
	selectedReportage = null;
}

private function onSelectionChangeKanBan(event:TaskBoardEvent):void {
	if (event.document is Reportage) {
		selectedReportage = event.document as Reportage;
	}
}

private function onSelectionChangeGrid(event:GridSelectionEvent):void {
	selectedReportage = reportageGrid.selectedItem as Reportage;
    isOpened = false;
}


/// ------------------
// Formatters
// ------------------
private function dateLabelFunction(item:Object, column:GridColumn):String {
	return item[column.dataField] ? dateUtil.dateToString(item[column.dataField]) : "";
}

private function initPopUpButtonMenu():void {
	var options:ArrayCollection = new ArrayCollection();

	if (ANews.USER.allow('010611')) {
		options.addItem(bundle.getString('Bundle', 'defaults.btn.copy'));
	}
	if (ANews.USER.allow("010605")) {
		options.addItem(bundle.getString("Bundle", "defaults.btn.print"));
	}
	
	if (ANews.USER.allow("010607")) {
		options.addItem(bundle.getString("Bundle", "rundown.story.sendToDrawer"));
	}

	menu.dataProvider = options;

	popUpButton.includeInLayout = popUpButton.visible = options.length != 0;
}

private function onMenuItemClick(event:MenuEvent):void {
	if (event.item == bundle.getString("Bundle", "defaults.btn.copy")) {
		copyTo();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.print")) {
		print();
	}
	if (event.item == bundle.getString("Bundle", "rundown.story.sendToDrawer")) {
		displaySendToDrawerOption();
	}
}

private function displaySendToDrawerOption():void {
	sendToDrawer.includeInLayout = true;
	sendToDrawer.visible = true;
    reportageGrid.enabled = false;
}

private function cancelSendToDrawer():void {
	sendToDrawer.includeInLayout = false;
	sendToDrawer.visible = false;
    reportageGrid.enabled = true;
}

private function sendReportageToDrawer():void {
	if (selectedReportage) {
		var programId:int = Program(drawerProgramCombo.selectedItem).id;
        documentService.archiveReportageToDrawer(selectedReportage.id, programId, true, onSendToDrawerSuccess);
		
	} else {
		ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
	}
		
}

private function onSendToDrawerSuccess(event:ResultEvent):void {
    if (event.result == null) {
        //LoaderMask.close();
        return;
    }
    switch (event.result as String) {
        case DrawerDocumentStatus.SUCCESS:
            var selectedProgram:Program = Program(drawerProgramCombo.selectedItem);
            ANews.showInfo(bundle.getString("Bundle", "quickview.msg.sentToDrawerSuccess", [selectedProgram.name]), Info.INFO);
            cancelSendToDrawer();
            break;
        case DrawerDocumentStatus.ALREADY_INCLUDED:
            ANews.showInfo(bundle.getString("Bundle", "quickview.msg.alreadyIncluded"), Info.WARNING);
            break;
    }
}

private function onGuidelineMessageHandler(event:MessageEvent):void {
	loadGuidelines(currentGuidelineStateIndex);	
}

private function loadGuidelines(stateIndex:int):void {
	// Se não estiver exibindo a gaveta, ignora.
	if(this.currentState != "drawer")
		return;
	
	currentGuidelineStateIndex = stateIndex;
	
	//Corrige os estados do scroller das laudas.
	switch(currentGuidelineStateIndex) {
		case 0: {
			completedBanner.currentState = "selected";
			coveringBanner.currentState = "normal";
			producingBanner.currentState = "normal";
			interruptedBanner.currentState = "normal";
			break;
		}
		case 1: {
			completedBanner.currentState = "normal";
			coveringBanner.currentState = "selected";
			producingBanner.currentState = "normal";
			interruptedBanner.currentState = "normal";
			break;
		}
		case 2: {
			completedBanner.currentState = "normal";
			coveringBanner.currentState = "normal";
			producingBanner.currentState = "selected";
			interruptedBanner.currentState = "normal";
			break;
		}
		case 3: {
			completedBanner.currentState = "normal";
			coveringBanner.currentState = "normal";
			producingBanner.currentState = "normal";
			interruptedBanner.currentState = "selected";
			break;
		}
		default: {
			break;
		}
	}
	
	// Faz a busca das novas pautas.
	guidelineService.listAllGuidelineOfDay(quickviewDate.selectedDate, (quickviewPrograms.selectedItem as Program).id, getStateByIndex(currentGuidelineStateIndex), onGuidelineList);
}

private function programLabelFunction(item:Object, column:GridColumn):String {
	var reportage:Reportage = item as Reportage;
	return reportage.program ? reportage.program.name : resourceManager.getString("Bundle", "defaults.drawerProgram");
}

private function stateLabelFunction(item:Object, column:GridColumn):String {
	var reportage:Reportage = item as Reportage;
	var stateLabel:String;
	
	switch (reportage.state) {
		case DocumentState.COMPLETED:
			return bundle.getString("Bundle", "document.states.complete")
		case DocumentState.COVERING:
			return bundle.getString("Bundle", "document.states.covering")
		case DocumentState.PRODUCING:
			return bundle.getString("Bundle", "document.states.producing")
		case DocumentState.INTERRUPTED:
			return bundle.getString("Bundle", "document.states.interrupt")
		case DocumentState.EDITING:
			return bundle.getString("Bundle", "document.states.editing")
			
	}
	return '?';
}

private function onSearchKeyDown(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        loadReportages();
    }
}

private function onChangeDate():void {
    loadReportages();
}

private function onChangeInitialDate(event:CalendarLayoutChangeEvent):void {
    if (periodStart.selectedDate && periodEnd.selectedDate) {
        var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
        if (val > 0) {
            periodEnd.selectedDate = periodStart.selectedDate;
        }
    }
}

private function onChangeFinalDate(event:CalendarLayoutChangeEvent):void {
    if (periodStart.selectedDate && periodEnd.selectedDate) {
        var val:int = dateTimeUtil.compareDates(periodStart.selectedDate, periodEnd.selectedDate);
        if (val > 0 ) {
            periodStart.selectedDate = periodEnd.selectedDate;
        }
    }
}

private function clearFilter():void {
    slugSearchInput.text = '';
    periodStart.selectedDate = null;
    periodEnd.selectedDate = null;
    pageBar.load(null, 0, 0, 0);
    reportages.removeAll();
}
