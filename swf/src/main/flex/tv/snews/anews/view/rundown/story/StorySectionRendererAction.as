import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.utils.ObjectUtil;

import spark.components.List;
import spark.events.TextOperationEvent;

import tv.snews.anews.domain.*;
import tv.snews.flexlib.utils.*;

[Bindable] private var obsLabel:String;
[Bindable] private var section:ReportageSection;

private const dateUtil:DateUtil = new DateUtil();

override public function set data(value:Object):void {
	super.data = value;
	section = value as ReportageSection;

	if (section) {
		updateObsLabel();
		
		// Deixa o checkbox de aprovação desativado para o repórter
//		if (ANews.USER.equals(section.reportage.guideline.reporter)) {
//			chkOk.enabled = false;
//		}
	}
}

//---------------------------
//	Actions
//---------------------------

private function onStartDrag(event:MouseEvent):void {
	List(this.owner).dragEnabled = true;
	List(this.owner).dropEnabled = true;
}

private function onStopDrag(event:MouseEvent):void {
	List(this.owner).dragEnabled = false;
	List(this.owner).dropEnabled = false;
}

private function onMoveByEnter(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER) {
		changeOrder();
	}
}

private function removeSection():void {
	var sectionList:List = List(this.owner);

	if (sectionList) {
		var sections:ArrayCollection = sectionList.dataProvider as ArrayCollection;
		
		var index:int = sections.getItemIndex(section);
		sections.removeItemAt(index);
		
		sections.refresh();
	}
}

private function hideOrShow():void {
	section.collapsed = !section.collapsed;
}

private function onContentChange(event:TextOperationEvent):void {
	section.reportage.calculateReadTime();
}

//---------------------------
//	Helpers
//---------------------------

private function changeOrder():void {
	var newIndex:int = parseInt(order.text) - 1;
	var sectionList:List = List(this.owner);

	if (sectionList) {
		var sections:ArrayCollection = sectionList.dataProvider as ArrayCollection;
		
		// Adjust the index variable against possible errors.
		if (newIndex > sections.length - 1)
			newIndex = sections.length - 1;
		if (newIndex < 0)
			newIndex = 0;
		
		var oldIndex:int = sections.getItemIndex(section);
		
		if (newIndex == oldIndex) {
			order.text = formatOrder("" + (oldIndex + 1));
			return;
		}
		
		var copy:ReportageSection = ObjectUtil.copy(section) as ReportageSection;
		copy.collapsed = section.collapsed;
		
		sections.removeItemAt(oldIndex); //remove from your position 
		sections.addItemAt(copy, newIndex); //adding the item on his new position
		
		sections.refresh();
	}
}

private function updateObsLabel():void {
	var key:String = section.observation != null ? "reportage.btn.deleteCitation" : "reportage.btn.citation";
	obsLabel = this.resourceManager.getString("Bundle", key);
}

private function toggleObservation():void {
	section.observation = section.observation != null ? null : "";
	updateObsLabel();
}

private function formatName(name:String):String {
	return name.charAt(0).toUpperCase() + name.substr(1, name.length);
}

private function formatOrder(value:String):String {
	while (value.length < 2) {
		value = "0" + value;
	}
	return value;
}
