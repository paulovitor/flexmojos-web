import flash.display.DisplayObject;
import flash.events.*;
import flash.ui.Keyboard;
import flash.utils.Timer;
import flash.utils.setTimeout;

import flashx.textLayout.operations.DeleteTextOperation;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.validators.PhoneNumberValidator;

import spark.components.ComboBox;
import spark.events.IndexChangeEvent;
import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.agenda.FormContactRenderer;
import tv.snews.anews.view.agenda.SelectContactWindow;
import tv.snews.flexlib.utils.DateUtil;

[Bindable] public var guide:Guide;
[Bindable] public var selectedCity:City = null;
[Bindable] public var selectedState:StateCountry = null;

[Bindable]
[ArrayElementType("tv.snews.anews.domain.City")]
private var cityList:ArrayCollection = new ArrayCollection();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.StateCountry")]
private var stateList:ArrayCollection = new ArrayCollection();

private const contactService:ContactService = ContactService.getInstance();
private const dateTimeUtil:DateUtil = new DateUtil();

[Bindable] private var timerContact:Timer = new Timer(1000, 1);

private function onContentCreationComplete(event:FlexEvent):void {
	this.addEventListener(CrudEvent.COPY, onContactSelected);
	listAllStates();
}

private function setCityAndState():void {
	selectedCity = cbcity.selectedItem as City;
	selectedState = cbstate.selectedItem as StateCountry;
}

private function changeState():void {
	if (cbstate.selectedIndex < 0) {
		Util.isValid(validators);
	} else {
		listAllCitys((cbstate.selectedItem as StateCountry).id);
		cbcity.selectedIndex = -1;
	}
}


//------------------------------------------------------------------------------
//	Contact Functions
//------------------------------------------------------------------------------
private var window:SelectContactWindow;

private function onAddContactOver(event:MouseEvent):void {
	addContact.setStyle("textDecoration", "underline");
}

private function onAddContactOut(event:MouseEvent):void {
	addContact.setStyle("textDecoration", "none");
}

private function onAddContactClick(event:MouseEvent):void {
	window = new SelectContactWindow();
	window.setStyle("windowColor", DomainCache.settings.colorGuideline);
	window.addEventListener(CrudEvent.COPY, onContactSelected);
	window.addEventListener(CloseEvent.CLOSE, onClose);
	
	PopUpManager.addPopUp(window, FlexGlobals.topLevelApplication as DisplayObject, true);
	PopUpManager.centerPopUp(window);
}

private function onContactSelected(event:CrudEvent):void {
	guide.addContact(event.entity as Contact);

    for each (var form:FormContactRenderer in getContactItemRenderers()){
        if(form.toRemove)
            guide.removeContact(form.simpleContact);
    }

}

private function onClose(event:CloseEvent):void {
	PopUpManager.removePopUp(window);
}


private function getContactItemRenderers():Vector.<FormContactRenderer> {
    var itemRenderers:Vector.<FormContactRenderer> = new Vector.<FormContactRenderer>();

    for (var index:int = 0; index < guideContactsList.numElements; index++) {
        var renderer:FormContactRenderer = guideContactsList.getElementAt(index) as FormContactRenderer;
        if (renderer) {
            itemRenderers.push(renderer);
        }
    }
    return itemRenderers;
}

// ------------------------
// Time Events
// ------------------------
private function onScheduleFocusOut():void {
	guide.schedule = dateTimeUtil.toDate(txtSchedule.text, "JJhNN");
	txtSchedule.text = formatHour(guide.schedule);
}

private function onScheduleClick():void {
	txtSchedule.selectRange(txtSchedule.text.length, txtSchedule.text.length);
	txtSchedule.setFocus();
}

private function onScheduleChange(event:TextOperationEvent):void {
	if (event.operation is DeleteTextOperation)
		return;

	if (txtSchedule.text.length > 2 && txtSchedule.text.indexOf('h') == -1) {
		txtSchedule.text = txtSchedule.text.substr(0, 2) + "h" + txtSchedule.text.substr(2);
	}

	onScheduleClick();
}

// ------------------------
// City and States Helpers.
// ------------------------
private function listAllCitys(idState:int):void {
	cbcity.selectedIndex = -1;
	CityService.getInstance().listAll(idState, onListCitys, onFault);
}

private function listAllStates():void {
	StateService.getInstance().listAll(onListStates, onFault);
}

private function setSelectState(value:Guide):void {
	if (value != null && value.city != null && value.city.state != null) {
		for (var i:int = 0; i < stateList.length; i++) {
			if (stateList.getItemAt(i).id == value.city.state.id) {
				cbstate.selectedIndex = i;
				cbstate.dispatchEvent(new IndexChangeEvent(IndexChangeEvent.CHANGE));
				selectedState = value.city.state;
				selectedCity = value.city;
				break;
			}
		}
	} else {
		selectedCity = null;
		selectedState = null;
		cityList = null;
		cbcity.selectedIndex = -1;
		cbstate.selectedIndex = -1;
	}
}

public function containsInvalidFields():Boolean {
	if (cbcity != null && cbstate != null) {
		updateCityAndStateCombo();
		if (!Util.isValid(validators)) {
			return true;
		}
	}
	return false;
}

// ------------------------
// Contacts Events
// ------------------------
private function onNewsContactsListCreated(event:FlexEvent):void {
	guideContactsList.addEventListener(CrudEvent.DELETE, onContactDeleteHandler);
}

private function onContactDeleteHandler(event:CrudEvent):void {
	guide.removeContact(event.entity as Contact);
}

// ------------------------
// Time Events
// ------------------------

protected function updateCityAndStateCombo():void {
	if (cbcity.selectedItem is City && cbstate.selectedItem is StateCountry) {
		guide.city = cbcity.selectedItem as City;
		guide.city.state = cbstate.selectedItem as StateCountry;
	} else {
		guide.city = null;
		if (Util.isValid(validators)) {
			return;
		}
	}
}

// ------------------------
// Formatters
// ------------------------
private function formatHour(date:Date):String {
	return dateTimeUtil.toString(date, "JJhNN");
}

// ------------------------
// Async. Answers
// ------------------------
private function onListContactFailed(event:FaultEvent, token:Object = null):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', "agenda.msg.failedToLoadContacts"), Info.ERROR);
}

private function onListCitys(event:ResultEvent):void {
	cityList = event.result as ArrayCollection;
	if (cityList.length > 0 && selectedCity != null && selectedCity.id > 0) {
		for (var i:int = 0; i < cityList.length; i++) {
			if (cityList.getItemAt(i).id == selectedCity.id) {
				cbcity.selectedIndex = i;
				break;
			}
		}

	} else {
		selectedCity = null;
		cbcity.selectedIndex = -1;
		return;
	}
}

private function onListStates(event:ResultEvent):void {
	stateList = event.result as ArrayCollection;

	if (stateList != null) {
		flash.utils.setTimeout(setSelectState, 100, guide);
	}

	if (stateList.length <= 0 || selectedState == null || selectedState.id < 0) {
		if (cbstate != null) {
			cbstate.selectedIndex = -1;
			return;
		}
	}
}

private function onFault(event:FaultEvent):void {
	var exceptionMessage:String = event.fault.rootCause.message as String;
	ANews.showInfo(exceptionMessage, Info.ERROR);
}
