import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.gridClasses.GridColumn;

import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Permission;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.domain.UserTeam;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.GroupService;
import tv.snews.anews.service.TeamService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.service.PermissionService;
import tv.snews.anews.utils.*;

[Bindable] private var team:UserTeam = new UserTeam();
[Bindable] private var user:User = new User();

[Bindable] private var usersOfTheTeam:ArrayCollection = new ArrayCollection();
[Bindable] private var notUsersOfTheTeam:ArrayCollection = new ArrayCollection();

private const bundle:IResourceManager = ResourceManager.getInstance();

private var teamService:TeamService = TeamService.getInstance();
//private var permissionService:PermissionService = PermissionService.getInstance();
private const userService:UserService = UserService.getInstance();

private function startUp():void {
//    permissionService.listAll(onListPermissions);

    gridTeam.addEventListener(CrudEvent.EDIT, onEditTeam);
    teamForm.addEventListener(CrudEvent.CREATE, onAddUser);
    teamForm.addEventListener(CrudEvent.DELETE, onRemoveUser);
}

private function onRemove():void{
    gridTeam.removeEventListener(CrudEvent.EDIT, onEditTeam);
    teamForm.removeEventListener(CrudEvent.CREATE, onAddUser);
    teamForm.removeEventListener(CrudEvent.DELETE, onRemoveUser);
}
//----------------------------
// Events
//----------------------------


private function onEditTeam(event:CrudEvent):void {
    cancel();
    team = ObjectUtil.copy(event.entity) as UserTeam;

    // Carrega os combos dos usuários que pertencem ao grupo e o dos que não pertencem.
    listByTeamName(true);
    listByTeamName(false);
    this.currentState = "edition";
    gridTeam.enabled = false;
}

//private function newTeam():void {
//    team = new UserTeam();
//    this.currentState = 'edition';
//    usersOfTheTeam = new ArrayCollection();
//    notUsersOfTheTeam = new ArrayCollection();
//}

private function save():void {
    if (teamName.text != '') {
        team.name = teamName.text;
        team.users = usersOfTheTeam;
        checkExistsGroup(team.name);
    } else {
        //TODO o nome do grupo esta em branco
    }
}

private function checkExistsGroup(name:String):void {
    teamService.listTeamByName(name, function(event:ResultEvent):void {
        var teamCheck:UserTeam = event.result as UserTeam;
        if (teamCheck != null && this.currentState == "copy") {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'guideline.team.alreadyExists'), Info.WARNING);
            teamName.errorString = ResourceManager.getInstance().getString('Bundle', 'guideline.team.alreadyExists');
        } else {
            if (teamCheck != null && team != null && teamCheck.id != team.id) {
                ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'guideline.team.alreadyExists'), Info.WARNING);
                teamName.errorString = ResourceManager.getInstance().getString('Bundle', 'guideline.team.alreadyExists');
            } else {
                teamService.save(team, onSaveSuccess);
            }
        }
    });
}

private function cancel():void {
    team = new UserTeam();
    this.currentState = "view";
    gridTeam.enabled = true;
    usersOfTheTeam.removeAll();
    notUsersOfTheTeam.removeAll();
}

//private function gridGroup_changeHandler(event:GridSelectionEvent):void {
//    team = gridTeam.selectedItem as UserTeam;
//}

private function listByTeamName(belongsToTheTeam:Boolean):void {
    // Quando o grupo for novo, só permite pesquisar se a flag belongsToTheGroup for falsa, ou seja, a pesquisa é por usuarios que não estejam no grupo
    // pois como o grupo ainda não exite no banco, não faz sentido buscar por usuarios desse grupo, já que ele ainda não existe

    if (!(team.id == 0 && belongsToTheTeam == true)){
        if(belongsToTheTeam){
            userService.listByTeamName(searchInputUsers.text, team.id, belongsToTheTeam,
                    function(event:ResultEvent):void { usersOfTheTeam = event.result as ArrayCollection;});
        } else {
            userService.listByTeamName(searchInputNotUsersOfTheTeam.text, team.id, belongsToTheTeam,
                    function(event:ResultEvent):void { notUsersOfTheTeam = event.result as ArrayCollection;});
        }
    }
}

private function onAddUser(event:CrudEvent):void {
    user = ObjectUtil.copy(event.entity) as User;
    usersOfTheTeam.addItem(user);

    var notUserOfTeam:User = new User();
    for (var i:int = 0; i < notUsersOfTheTeam.length; i++)
    {
        notUserOfTeam = notUsersOfTheTeam.getItemAt(i) as User;
        if(notUserOfTeam.id == user.id){
            notUsersOfTheTeam.removeItemAt(i);
            break;
        }
    }
}


private function onRemoveUser(event:CrudEvent):void {
    user = ObjectUtil.copy(event.entity) as User;
    notUsersOfTheTeam.addItem(user);

    var userOfTeam:User = new User();
    for (var i:int = 0; i < usersOfTheTeam.length; i++)
    {
        userOfTeam = usersOfTheTeam.getItemAt(i) as User;
        if(userOfTeam.id == user.id){
            usersOfTheTeam.removeItemAt(i);
            break;
        }
    }
}

// -----------------------------
// Asynchronous answers
// -----------------------------


private function onSuccess(event:ResultEvent):void {
    refresh();
}

private function onSaveSuccess(event:ResultEvent):void {
    ANews.showInfo(this.bundle.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
    refresh();
}

// --------------------------
// Helpers.
// --------------------------
private function refresh():void {
    //Restore default values.
    team = new UserTeam();
    usersOfTheTeam = new ArrayCollection();;
    notUsersOfTheTeam = new ArrayCollection();;
    this.currentState = "view";

    gridTeam.enabled = true;
}

private function userGroupsLabelFunction(user:User, column:GridColumn):String {
	return user.groupsToString();
}
