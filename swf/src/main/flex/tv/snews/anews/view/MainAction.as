import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.external.ExternalInterface;
import flash.geom.Point;
import flash.net.URLRequest;
import flash.net.navigateToURL;
import flash.utils.Timer;

import mx.controls.Alert;

import mx.core.FlexGlobals;

import mx.events.FlexEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.DeviceVendor;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.MenuEvent;
import tv.snews.anews.event.PluginActiveXEvent;
import tv.snews.anews.mos.OradPlugin;
import tv.snews.anews.service.LoginService;
import tv.snews.anews.service.SettingsService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.utils.IconUtils;
import tv.snews.anews.view.menu.AdministrationMenu;
import tv.snews.anews.view.menu.MainMenu;
import tv.snews.flexlib.components.LinkBox;

private var linkBoxes:Array;
private var floatMenu:Object;
private var floatConfigurationMenu:MainMenu;

private static const loginService:LoginService = LoginService.getInstance();
private static const bundle:IResourceManager = ResourceManager.getInstance();
private static const settingsService:SettingsService = SettingsService.getInstance();
private static const userService:UserService = UserService.getInstance();
private var app:Object = FlexGlobals.topLevelApplication;
private var tooltipIsFocused:Boolean;
private var isRunning:Boolean;
private var isRunning2:Boolean;
private var menuIsFocused:Boolean;
private var toolTipIsOpen:Boolean;
private var timer:Timer;
private var timer2:Timer;


private function startUp(event:FlexEvent):void {
    this.systemArea.addEventListener(PluginActiveXEvent.OPEN, onOpenActiveXPlugin);
    this.systemArea.addEventListener(PluginActiveXEvent.EDIT, onOpenActiveXPlugin);
    this.oradPlugin.addEventListener(PluginActiveXEvent.CLOSE, onCloseAllPluginActiveX);
    this.systemArea.addEventListener(ANewsPlayEvent.OPEN, onOpenANewsPlay);
    this.systemArea.addEventListener(ANewsPlayEvent.CLOSE, onCloseANewsPlay);
    this.aNewsPlay.addEventListener(ANewsPlayEvent.CLOSE, onCloseANewsPlay);

    this.addEventListener(MenuEvent.COLLAPSE, onMenuCollapseChange);

    linkBoxes = [systemLink, adminLink, previewLink, exitLink];

	if (systemLink.visible) {
		onSystemLinkClickHandler();
	} else if (adminLink.visible) {
		onAdminLinkClickHandler();
	} else if (previewLink.visible) {
		onPreviewLinkClickHandler();
	}
}

private function creationTabChatGroup():void {
	if (!ANews.USER.allow('01040101')) {
		chatTabBar.removeChildAt(chatTabBar.getChildIndex(tabNewsChatBar));
	}
}

/**
 * Gera a string contendo o email do usuário e seus grupos:
 * fulano@snews.tv (Editores, Repórteres)
 */
private function generateUserIdentification(user:User):String {
	var groupsNames:Array = new Array();
    for each (var group:UserGroup in user.groups) {
		groupsNames.push(group.name);
	}
	return user.email + " (" + groupsNames.join(", ") + ")";
}

private function setSelectedLink(currentLink:LinkBox):void {
	for (var i:int = 0, linkBox:LinkBox; i < linkBoxes.length; i++) {
		linkBox = linkBoxes[i] as LinkBox;
		if (linkBox.visible) {
		linkBox.selected = linkBox.id == currentLink.id;
	}
}
}

//--------------------------
//	Event Handlers
//--------------------------

private function onSystemLinkClickHandler():void {
	viewStack.selectedIndex = 0;
	setSelectedLink(systemLink);
}

private function onAdminLinkClickHandler():void {
	viewStack.selectedIndex = 1;
	setSelectedLink(adminLink);
}

private function onPreviewLinkClickHandler():void {
	viewStack.selectedIndex = 2;
	setSelectedLink(previewLink);
}

private function onExitLinkClickHandler():void {
	exitLink.selected = false;
	ANews.USER = null;
	loginService.leave(onLeaveSuccess);
}

//--------------------------
//	Asynchronous Answers
//--------------------------

private function onLeaveSuccess(event:ResultEvent):void {
	var ref:URLRequest = new URLRequest("javascript:location.reload(true)");
	navigateToURL(ref, "_self");
}

//------------------------------------------------------------------------------
// Comunicação com plugin ActiveX INÍCIO
//------------------------------------------------------------------------------
private function onOpenActiveXPlugin(event:PluginActiveXEvent):void {
    closeAllRightLayout();
    var ieIdentify:Boolean = new Boolean(ExternalInterface.call("function(){return !!window.MSStream;}")) || new Boolean(ExternalInterface.call("function(){ return window.navigator.userAgent.indexOf('MSIE') > 0}"));
    if (!ieIdentify) {
        ANews.showInfo(bundle.getString('Bundle', 'cg.plugin.notAvailableBrowser'), Info.WARNING);
        return;
    }
    switch (event.mosDevice.vendor) {
        case DeviceVendor.ORAD:
            oradPlugin.onStart(event);
            calcHDividedBoxWidth(oradPlugin);
            break;
        default:
            ANews.showInfo(bundle.getString('Bundle', 'cg.plugin.notFound'), Info.WARNING);
            break;
    }
}

private function onCloseAllPluginActiveX(event:PluginActiveXEvent):void {
    closeAllRightLayout();
}

private function closeAllRightLayout():void {
    oradPlugin.enableVisible = false;
    aNewsPlay.enableVisible = false;
    if(aNewsPlay.mediaPlayer || aNewsPlay.audioPlayer || aNewsPlay.img || aNewsPlay.anewsPlayIFrame) {
        aNewsPlay.stop();
    }
    calcHDividedBoxWidth(oradPlugin);
    calcHDividedBoxWidth(aNewsPlay);
}

//------------------------------------------------------------------------------
// ANewsPlay visualizador de videos.
//------------------------------------------------------------------------------
private function onOpenANewsPlay(event:ANewsPlayEvent):void {
    closeAllRightLayout();
    aNewsPlay.start(event);
    calcHDividedBoxWidth(aNewsPlay);
}

private function onCloseANewsPlay(event:ANewsPlayEvent):void {
    closeAllRightLayout();
}

//------------------------------------------------------------------------------
// Calculo de área HDividedBox
//------------------------------------------------------------------------------
private function calcHDividedBoxWidth(object:Object):void {
    if(object.enableVisible) {
        object.percentWidth = 40000 / this.width;
        anewsGroup.percentWidth = 100 - object.percentWidth;
        if(object is OradPlugin) {
            anewsGroup.enabled = false;
        } else {
            anewsGroup.enabled = true;
        }
    } else {
        object.percentWidth = 0;
        anewsGroup.percentWidth = 100;
        anewsGroup.enabled = true;
    }
}


//------------------------------------------------------------------------------
// Menu
//------------------------------------------------------------------------------

private function createToolTip():void {
    if(viewStack.selectedIndex == 0) {
        floatMenu = new MainMenu();
        floatMenu.navigator = systemArea.navigator;
    }
    else if(viewStack.selectedIndex == 1) {
        floatMenu = new AdministrationMenu();
        floatMenu.navigator = settingsArea.navigator;
    }
    app.main.addElement(floatMenu);

    var newPosition:Point = this.localToGlobal(new Point(menuIcon.x, menuIcon.y));
    floatMenu.x = newPosition.x;
    floatMenu.y = newPosition.y + menuIcon.height;
    floatMenu.addEventListener(MouseEvent.ROLL_OUT, onMouseOutTooltip);
    floatMenu.addEventListener(MouseEvent.ROLL_OVER, onMouseOverTooltip);
    menuIcon.addEventListener(MouseEvent.ROLL_OVER, onMouseOverMenuIcon);
    menuIcon.addEventListener(MouseEvent.ROLL_OUT, onMouseOutMenuIcon);
    toolTipIsOpen = true;
}

private function destroyToolTip():void {
    var indexs:int = app.main.getElementIndex(floatMenu);
    app.main.removeElementAt(indexs);
    floatMenu = null;
    toolTipIsOpen = false;
    menuIcon.addEventListener(MouseEvent.ROLL_OVER, onMouseOverMenuIcon);
    menuIcon.removeEventListener(MouseEvent.ROLL_OUT, onMouseOutMenuIcon);
    menuIcon.removeEventListener(MouseEvent.ROLL_OVER, onMouseOverMenuIcon);
}

function onMenuCollapseChange(event:MenuEvent):void {
    var collapse:Boolean = event.entity as Boolean;
    userService.collectMenu(ANews.USER.id, collapse);
}

private function onMenuClick(event:MouseEvent):void {
    if (viewStack.selectedIndex == 0) {
        if (floatMenu)
            destroyToolTip();
        else {
            systemArea.mainMenu.currentState = systemArea.mainMenu.currentState == 'expanded' ? 'indented' : 'expanded';
            userService.collectMenu(ANews.USER.id, systemArea.mainMenu.currentState == 'indented');
        }
    } else if (viewStack.selectedIndex ==  1) {
        if (floatConfigurationMenu)
            destroyToolTip();
        else {
            settingsArea.administrationMenu.currentState = settingsArea.administrationMenu.currentState == 'expanded' ? 'indented' : 'expanded';
            userService.collectMenu(ANews.USER.id, settingsArea.administrationMenu.currentState == 'indented');
        }
    }
}

private function onMouseOutTooltip(event:MouseEvent):void {
    tooltipIsFocused = false;
    isRunning2 = true;
    timer2 = new Timer(300);
    timer2.addEventListener(TimerEvent.TIMER, checkDestroyTooltip);
    timer2.start();
}

private function checkDestroyTooltip(event:TimerEvent):void {
    if (toolTipIsOpen && !tooltipIsFocused && !menuIsFocused) {
        destroyToolTip();
    }
    if(timer) timer.stop();
    if(timer2) timer2.stop();

    isRunning = false;
}

private function onMouseOverTooltip(event:MouseEvent):void {
    tooltipIsFocused = true;
}

private function onMouseOverMenuIcon(event:MouseEvent):void {
    menuIsFocused = true;
}

private function onMouseOutMenuIcon(event:MouseEvent):void {
    menuIsFocused = false;
    isRunning = true;
    timer = new Timer(300);
    timer.addEventListener(TimerEvent.TIMER, checkDestroyTooltip);
    timer.start();

}