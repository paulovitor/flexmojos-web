import flash.events.Event;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.domain.*;
import tv.snews.anews.domain.IIDevice;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.DeviceService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.view.cg.*;
import tv.snews.anews.view.rundown.story.*;

private const service:DeviceService = DeviceService.getInstance();

[Bindable] public var device:IIDevice;
[Bindable] private var templates:ArrayCollection = new ArrayCollection();

private function onInitialize(event:FlexEvent):void {
	this.setStyle("modalTransparency", 0.0);
	this.setStyle("modalTransparencyBlur", 2);
}

private function onCreationComplete(event:FlexEvent):void {
    templates = device.templates;

	// Remove a seleção após adicionar
//	this.addEventListener(CrudEvent.CREATE,
//			function(evt:CrudEvent):void {
//				templateList.selectedIndex = -1;
//			}
//	);

	// Pega o focus para que o ESC funcione de primeira
	focusManager.setFocus(templateList);
}

private function onRemove(event:Event):void {
}

private function onKeyDown(event:flash.events.KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER) {
		addCG();
	}
	if (event.keyCode == Keyboard.ESCAPE) {
		closeWindow();
	}
}

private function closeWindow():void {
	PopUpManager.removePopUp(this);
}

private function addCG():void {
    var index:int = templateList.selectedIndex;
	for each (var template:IITemplate in templateList.selectedItems) {
        if(template != null) {
            dispatchEvent(new CrudEvent(CrudEvent.CREATE, template));
            return;
        }
	}
    templateList.selectedIndex = index;
    templateList.getFocus();
}
