import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.ui.Keyboard;

import mx.collections.ArrayCollection;
import mx.containers.TabNavigator;
import mx.controls.Alert;
import mx.core.*;
import mx.events.*;
import mx.managers.DragManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.*;
import mx.utils.ObjectUtil;

import spark.components.Group;
import spark.components.gridClasses.*;
import spark.events.GridEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.RoundService;
import tv.snews.anews.utils.*;
import tv.snews.flexlib.utils.DateUtil;

private const service:RoundService = RoundService.getInstance();

private const dtUtil:DateUtil = new DateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();

private var _selectedRound:Round = null;

	[Bindable] private var roundStr:String;
[Bindable] private var rounds:ArrayCollection = new ArrayCollection();
[Bindable] private var bundle:IResourceManager = ResourceManager.getInstance();

private function onContentCreationComplete():void {
	// Força o carregamento dos segmentos para evitar problemas no form
	DomainCache.segments.length;
	
	service.subscribe(onRoundMessageHandler);

	loadPopUpButtonContent();
	listRoundByDate();
}

private function onFormCreationComplete(event:FlexEvent):void {
	roundForm.addEventListener(CrudEvent.CREATE, onSave);
	roundForm.addEventListener(CrudEvent.EDIT, onSave);
	roundForm.addEventListener(Event.CANCEL, onCancelEdition);
}

private function onSave(event:CrudEvent):void {
	event.stopPropagation();
	
	var round:Round = Round(event.entity);
	service.save(round, onSaveSucess, onFault);
}

private function onSaveSucess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString("Bundle", "defaults.msg.saveSuccess"), Info.INFO);
	
	if (selectedRound && selectedRound.id > 0) {
		// Remove o bloqueio de edição
		service.unlockEdition(selectedRound.id, function(event:ResultEvent):void {});
	}
	
	listRoundByDate();
}

private function onCancelEdition(event:Event):void {
	gridRound.selectedIndex = -1;
	
	if (selectedRound &&  selectedRound.id > 0) {
		// Remove o bloqueio de edição
		service.unlockEdition(selectedRound.id, function(event:ResultEvent):void {});
	}
	selectedRound = null;
	listRoundByDate();
	this.currentState = "view";
}

private function onRoundMessageHandler(event:MessageEvent):void{
	var round:Round = event.message.body as Round;
	var action:String = event.message.headers.action;
	
	var rounds:ArrayCollection =	gridRound.dataProvider as ArrayCollection;
	if (round == null)
		return;
	
	switch (action) {
		case EntityAction.LOCKED:
			for each (var current:Round in rounds) {
			if (current.equals(round)) {
				current.editingUser = event.message.headers["user"] as User;
				break;
			}
		}
			break;
		
		case EntityAction.UNLOCKED:
			for each (current in rounds) {
			if (current.equals(round)) {
				current.editingUser = null;
				break;
			}
		}
			break;
	}
	
	if (!(action == EntityAction.UNLOCKED || action == EntityAction.LOCKED))  {
		listRoundByDate();
	}
}

public function onRemove(event:Event):void {
	service.unsubscribe(onRoundMessageHandler);
	if (selectedRound && selectedRound.id > 0) {
		// Remove o bloqueio de edição
		service.unlockEdition(selectedRound.id, function(event:ResultEvent):void {});
	}
}

private function loadPopUpButtonContent():void {
	popUpButtonMenu.dataProvider = popUpButtonMenu.dataProvider || new ArrayCollection();
	
	var dataProvider:ArrayCollection = ArrayCollection(popUpButtonMenu.dataProvider);
	dataProvider.removeAll();
	
	// Verifica se deve exibir o botão de criar notícia
	if (selectedRound && ANews.USER.allow("01010306")) {
		dataProvider.addItem(bundle.getString("Bundle", "agency.newness.btn.createNews"));
	}
	
	// Verifica a permissão de imprimir
	if (ANews.USER.allow("01010305")) {
		dataProvider.addItem(bundle.getString("Bundle", "defaults.btn.printSelecteds"));
		dataProvider.addItem(bundle.getString("Bundle", "defaults.btn.printGrid"));
	}
	
	popUpButton.includeInLayout = popUpButton.visible = dataProvider.length != 0;
}

// -----------------------------
// Handler"s
// -----------------------------

private function onPopUpButtonItemClick(event:MenuEvent):void {
	if (event.item == bundle.getString("Bundle", "agency.newness.btn.createNews")) {
		publishRound();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.printSelecteds")) {
		print();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.printGrid")) {
		printGrid();
	}
}

private function listRoundByDate():void {
	service.listAll(roundDate.selectedDate, onListRounds, onFault);
}

private function deleteRound():void {
    if (selectedRound != null) {
        service.isLockedForEdition(selectedRound.id,
                function(event:ResultEvent):void {
                    var user:User = event.result as User;
                    if (user == null) {
                        Alert.show(bundle.getString("Bundle", "defaults.msg.confirmAllDelete"),
                                bundle.getString("Bundle", "defaults.msg.confirmDelete"), Alert.NO | Alert.YES, null, confirmationDelete);
                    } else {
                        ANews.showInfo(bundle.getString("Bundle", "round.msg.lockedMessage", [ user.nickname ]), Info.WARNING);
                    }
                }
        );
    }
}

private function confirmationDelete(event:CloseEvent):void {
	var round:Round = gridRound.selectedItem as Round;
	if (event.detail == Alert.YES) {
		service.remove(round.id, onDeleteSucess, onFault);
	}
}

private function startDragDrop(event:GridEvent):void {
	if (DragManager.isDragging || selectedRound == null)
		return;
	
	if (event.itemRenderer != null) {
		startDragDropRound(event);
	}
}

private function startDragDropRound(event:GridEvent):void {
	var sourceItens:Vector.<Object> = new Vector.<Object>();
	sourceItens.push(ObjectUtil.copy(gridRound.selectedItem) as Round)
	
	var dataSource:DragSource = new DragSource();
	dataSource.addData(sourceItens, "itemsByIndex");
	
	//Create the proxy (visual element that will be drag with cursor over the elements)
	var proxy:Group = new Group();
	proxy.width = gridRound.grid.width;
	
	for each (var columnIndex:int in gridRound.grid.getVisibleColumnIndices() as Vector.<int>) {
		var currentRenderer:IGridItemRenderer = gridRound.grid.getItemRendererAt(gridRound.selectedIndex, columnIndex);
		var factory:IFactory = gridRound.columns.getItemAt(columnIndex).itemRenderer;
		if (!factory)
			factory = gridRound.itemRenderer;
		var renderer:IGridItemRenderer = IGridItemRenderer(factory.newInstance());
		renderer.visible = true;
		renderer.column = currentRenderer.column;
		renderer.rowIndex = currentRenderer.rowIndex;
		renderer.label = currentRenderer.label;
		renderer.x = currentRenderer.x;
		renderer.y = currentRenderer.y;
		renderer.width = currentRenderer.width;
		renderer.height = currentRenderer.height;
		renderer.prepare(false);
		proxy.addElement(renderer);
		renderer.owner = gridRound;
	}
	proxy.height = renderer.height;
	
	//Start the drag.
	DragManager.doDrag(gridRound, dataSource, event, proxy as IFlexDisplayObject, 0, -gridRound.columnHeaderGroup.height);
}

protected function onKeyDown(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && gridRound.selectedItem != null && gridRound.selectedIndex != -1) {
		if (this.currentState == "view") {
			this.currentState = "preview";
		} else {
			this.currentState = "view";
		}
	}
}

// -----------------------------
// Utilities
// -----------------------------

private function displayContact():void {
	if (selectedRound && selectedRound.contact) {
		var strBuilder:Array = [];
		strBuilder.push(selectedRound.contact.name);
		for each(var contactNumber:ContactNumber in selectedRound.contact.numbers) {
			strBuilder.push(" | ");
			strBuilder.push(contactNumber.value);
		}
		roundStr = strBuilder.join("");
	} else {
		roundStr = "";
	}
}

private function formatTime(timestamp:Date):String {
	return dtUtil.toString(timestamp, bundle.getString("Bundle", "defaults.timeFormatHourMinuteSecond"));
}

private function formatDate(timestamp:Date):String {
	return dtUtil.toString(timestamp, bundle.getString("Bundle", "defaults.dateFormat"));
}

private function userLabelFunction(item:Object, column:GridColumn):String {
	var roundLabel:Round = Round(item);
	return roundLabel.user.nickname;
}

private function hourOfDay(item:Object, collumn:GridColumn):String {
	return formatTime(item["date"]);
}

//private function roundOccurrence(item:Object, collumn:GridColumn):String {
//	return item["occurrence"] == true ? ResourceManager.getInstance().getString("Bundle","agency.round.withOccurrence") : ResourceManager.getInstance().getString("Bundle","agency.round.noOccurrence");
//}

private function onSelectRound(event:Event):void {
	selectedRound = gridRound.selectedItem as Round;
	if (this.currentState == "preview") {
		viewRound();
	}
}

private function viewRound():void {
	if (selectedRound) {
		this.currentState = "preview";
	}
}

private function cancelRound():void {
	gridRound.selectedIndex = -1;
	this.currentState = "view";
	if (selectedRound && selectedRound.id > 0) {
		// Remove o bloqueio de edição
		service.unlockEdition(selectedRound.id, function(event:ResultEvent):void {});
	}
	selectedRound = null;
}

private function newRound():void {
	this.currentState = "edition";
	gridRound.selectedIndex = -1;
	roundForm.newRound();
}

private function editRound():void {
	
	if (selectedRound != null) {
		// Verifica se a notícia já está sendo editada por outro
		service.isLockedForEdition(selectedRound.id,
			function(event:ResultEvent):void {
				var user:User = event.result as User;
				if (user == null) {
					// Bloqueia a edição da notícia para os outros usuários
					service.lockEdition(selectedRound.id, ANews.USER.id,
						function(event:ResultEvent):void {
							currentState = "edition";
							roundForm.editRound(selectedRound);
							if (!selectedRound.institution)  {
								roundForm.cbSegment.selectedIndex = -1;
								roundForm.cbInstitution.errorString = "";
							}
						}
					);
				} else {
					ANews.showInfo(bundle.getString("Bundle", "round.msg.lockedMessage", [ user.nickname ]), Info.WARNING);
				}
			}
		);
	}
	
}

private function print():void {
	var params:Object = new Object();

	var ids:Array = [];
	for each (var current:Round in gridRound.selectedItems) {
		ids.push(current.id);
	}

	params.ids = ids.join("|");

	reportUtil.generate(ReportsUtil.ROUND, params);
}

private function printGrid():void {
	var params:Object = new Object();
	params.date = roundDate.text;
	reportUtil.generate(ReportsUtil.ROUND, params);
}

private function publishRound():void {
	selectedRound = this.gridRound.selectedItem as Round;

	var news:News = new News();
	news.slug = selectedRound.slug.substring(0, 49);
	news.information = selectedRound.information;
	news.address = (selectedRound.address || '').substring(0, 244);

	if (selectedRound.institution) {
		news.font = (selectedRound.institution.segment.name + " - " + selectedRound.institution.name).substring(0, 244);
	}

	if (selectedRound.contact) {
		news.contacts.addItem(selectedRound.contact);
	}

	var tabBar:TabNavigator = this.parent as TabNavigator;
	tabBar.dispatchEvent(new CrudEvent(CrudEvent.CREATE, news));
}

// -----------------------------
// Asynchronous answers
// -----------------------------

private function onFault(event:FaultEvent):void {
	var chandePasswordException:String = event.fault.faultString as String;
	var chandePasswordExceptionMessage:String = event.fault.rootCause.message as String;
	ANews.showInfo(chandePasswordExceptionMessage, Info.ERROR);
	
	if ( selectedRound.id > 0) {
		// Remove o bloqueio de edição
		service.unlockEdition(selectedRound.id, function(event:ResultEvent):void {});
	}
}

private function onDeleteSucess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString("Bundle", "defaults.msg.deleteSuccess"), Info.INFO);
	listRoundByDate();
}

// -----------------------------
// Methods result
// -----------------------------

private function onListRounds(event:ResultEvent):void {
	rounds = event.result as ArrayCollection;
	
	gridRound.selectedIndex = -1;
	selectedRound = null;
	
	this.currentState = "view";
}

// -----------------------------
// Get and Set
// -----------------------------

[Bindable]
public function set selectedRound(value:Round):void {
	_selectedRound = value;
	
	displayContact();
	loadPopUpButtonContent();
}

public function get selectedRound():Round {
	return _selectedRound;
}
