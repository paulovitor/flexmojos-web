import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.events.CloseEvent;
import mx.managers.PopUpManager;

import spark.components.DataGrid;

import tv.snews.anews.domain.WSDevice;

import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.utils.Util;

private function addVideo():void {
    mediaCenter.hideViewerSelectionOfMedia();
    var selectedMedias:ArrayCollection;
    if (mediaCenter.selectedDevice is WSDevice) {
        selectedMedias = getWSMediasSelecteds(mediaCenter.mediaCenterMediaList);
    } else {
        selectedMedias = Util.getSelecteds(mediaCenter.mediaCenterMediaList);
    }

    this.dispatchEvent(new CrudEvent(CrudEvent.CREATE, selectedMedias));
	closeWindow();
}

private function closeWindow():void {
    mediaCenter.hideViewerSelectionOfMedia();
	var closeEvent:CloseEvent = new CloseEvent(CloseEvent.CLOSE)
	this.dispatchEvent(closeEvent);
}

private function removePopUp(event:Event):void {
	PopUpManager.removePopUp(this);
}

private function getWSMediasSelecteds(grid:DataGrid):ArrayCollection {
    if (grid && grid.selectedItems && grid.selectedItems.length != 0) {
        return getWSMedias(grid.selectedItems);
    } else {
        return null;
    }
}

public static function getWSMedias(items:Vector.<Object>):ArrayCollection {
    var medias:ArrayCollection = new ArrayCollection();
    for each (var obj:Object in items) {
        medias.addItem(obj);
    }
    return medias;
}
