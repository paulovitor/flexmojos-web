package tv.snews.anews.view.navigator {

	import mx.collections.ArrayCollection;

	import spark.components.supportClasses.SkinnableComponent;

	[SkinState("normal")]
	[SkinState("preview")]
	[SkinState("edition")]
	[Event(name = "search", type = "tv.snews.anews.view.navigator.SelectSearchEvent")]
	[Event(name = "selectFilter", type = "tv.snews.anews.view.navigator.SelectSearchEvent")]

	public class SearchBar extends SkinnableComponent {

		[SkinPart]
		public var myTextInput:TextInputSearch;
		[Bindable] public var filterSearchList:ArrayCollection = new ArrayCollection();

		private var _currentArea:Object;

		public function SearchBar() {
			super();
			setStyle("skinClass", SearchBarSkin);
		}

		public function dispatchTopSelectEvent(selIndex:int, selItem:*):void {
			this.dispatchEvent(new SelectSearchEvent(SelectSearchEvent.SELECT, false, false, "", selIndex, selItem));
		}

		public function dispatchSearchEvent(searchText:String):void {
			this.dispatchEvent(new SelectSearchEvent(SelectSearchEvent.SEARCH, false, false, searchText));
		}

		[Bindable] public function get currentArea():Object {
			return _currentArea;
		}

		public function set currentArea(value:Object):void {
			_currentArea = value;
			if (myTextInput != null)
				myTextInput.currentArea = _currentArea;
		}

	}
}
