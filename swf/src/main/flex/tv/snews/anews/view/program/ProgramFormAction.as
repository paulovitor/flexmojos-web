import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.ui.Keyboard;

import flashx.textLayout.operations.DeleteTextOperation;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.ClassFactory;
import mx.events.CloseEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.components.TextInput;
import spark.components.gridClasses.GridColumn;
import spark.events.IndexChangeEvent;
import spark.events.TextOperationEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.program.ProgramForm;
import tv.snews.anews.view.program.ProgramHelper;
import tv.snews.flexlib.utils.DateUtil;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const programService:ProgramService = ProgramService.getInstance();
private const userService:UserService = UserService.getInstance();
private const dtUtil:DateUtil = Util.getDateUtil();
private const helper:ProgramHelper = new ProgramHelper(ProgramForm(this));

[Bindable]
public var program:Program;
[Bindable]
public var programToDelete:Program;

[ArrayElementType("tv.snews.anews.domain.Program")]
[Bindable]
private var programs:ArrayCollection = new ArrayCollection();

[ArrayElementType("tv.snews.anews.domain.User")]
[Bindable]
private var users:ArrayCollection = new ArrayCollection();

[ArrayElementType("tv.snews.anews.domain.IIDevicePath")]
[Bindable]
private var paths:ArrayCollection = null;

//------------------------------
//	Event Handlers & Callbacks
//------------------------------

private function creationCompleteHandler(event:Event):void {
    reset();
    dataGroupPrograms.addEventListener(CrudEvent.EDIT, crudEventEditHandler);
    dataGroupPrograms.addEventListener(CrudEvent.DELETE, crudEventDeleteHandler);
    presenterList.addEventListener(CrudEvent.DELETE, crudEventDeletePresenterHandler);
    editorList.addEventListener(CrudEvent.DELETE, crudEventDeleteEditorHandler);
    imageEditorList.addEventListener(CrudEvent.DELETE, crudEventDeleteImageEditorHandler);
}

private function removeHandler(event:Event):void {
    dataGroupPrograms.removeEventListener(CrudEvent.EDIT, crudEventEditHandler);
    dataGroupPrograms.removeEventListener(CrudEvent.DELETE, crudEventDeleteHandler);
    presenterList.removeEventListener(CrudEvent.DELETE, crudEventDeletePresenterHandler);
    editorList.removeEventListener(CrudEvent.DELETE, crudEventDeleteEditorHandler);
    imageEditorList.removeEventListener(CrudEvent.DELETE, crudEventDeleteImageEditorHandler);
}

private function crudEventEditHandler(event:CrudEvent):void {
    helper.resetErrors();

    program = event.entity as Program;
    currentState = "edit";
    dataGroupPrograms.enabled = false;

    if (program.cgDevice == null) {
        playoutDevicesCombo.selectedIndex = -1;
        playoutDevicesCombo.textInput.text = "";
        cgDevicesCombo.selectedIndex = -1;
        pathsCombo.selectedIndex = -1;
    } else {
        definePaths();
    }

    acPresenters.selectedItem = "";
    acEditors.selectedItem = "";
    acImageEditors.selectedItem = "";

    txtProgramName.setFocus();
}

private function crudEventDeleteHandler(event:CrudEvent):void {
    this.programToDelete = event.entity as Program;
    Alert.show(bundle.getString('Bundle', 'defaults.msg.confirmAllDelete'),
            bundle.getString('Bundle', 'defaults.msg.confirmation'), Alert.NO | Alert.YES, null, closeEventHandler);
}

private function closeEventHandler(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        programService.deleteProgram(this.programToDelete.id, resultDeleteProgramHandler);
    } else {
        this.programToDelete = null;
    }
}

private function resultDeleteProgramHandler(event:ResultEvent):void {
    ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.deleteSuccess'), Info.INFO);
    reset();
}

private function crudEventDeletePresenterHandler(event:CrudEvent):void {
    var presenter:User = event.entity as User;
    if (presenter != null) {
        program.presenters.removeItemAt(program.presenters.getItemIndex(presenter));
        if (defaultPresenterRadioButtonGroup.selectedValue == presenter) {
            defaultPresenterRadioButtonGroup.selectedValue = null;
        }
    }
}

private function crudEventDeleteEditorHandler(event:CrudEvent):void {
    var editor:User = event.entity as User;
    if (editor != null) {
        program.editors.removeItemAt(program.editors.getItemIndex(editor));
    }
}

private function crudEventDeleteImageEditorHandler(event:CrudEvent):void {
    var imageEditor:User = event.entity as User;
    if (imageEditor != null) {
        program.imageEditors.removeItemAt(program.imageEditors.getItemIndex(imageEditor));
    }
}

/*----------------Fields handlers----------------------*/

private function defaultPresenterRadioButtonGroup_changeHandler(event:Event):void {
    program.defaultPresenter = defaultPresenterRadioButtonGroup.selectedValue as User;
}

private function cancelButton_clickHandler(event:MouseEvent):void {
    reset();
}

private function saveButton_clickHandler(event:MouseEvent):void {
    if ((program.id == 0 && ANews.USER.allow("020402")) || (program.id != 0 && ANews.USER.allow("020403"))) {
        if (helper.validateRequiredFields()) {
            saveButton.enabled = false;
            programService.save(program, resultSaveHandler, faultSaveHandler);
        }
    }
}

private function txtProgramName_changeHandler(event:TextOperationEvent):void {
    if (txtProgramName.text != '' || txtScheduleStart.text != '' || txtScheduleEnd.text != '' || cgDevicesCombo.selectedIndex != -1) {
        this.currentState = "new";
    } else {
        this.currentState = "default";
    }
    txtProgramName.errorString = "";
}

private function txtSchedule_changeHandler(event:TextOperationEvent):void {
    if (event.operation is DeleteTextOperation)
        return;

    var input:TextInput = event.target as TextInput;
    helper.applyMaskTime(input);
    txtSchedule_clickHandler(input);
    txtProgramName_changeHandler(event);
}

private function txtSchedule_clickHandler(txtSchedule:TextInput):void {
    txtSchedule.selectRange(txtSchedule.text.length, txtSchedule.text.length);
    txtSchedule.setFocus();
}

private function txtScheduleStart_focusOutHandler():void {
    program.start = Util.getDateUtil().toDate(txtScheduleStart.text, "JJ:NN:SS");
    txtScheduleStart.text = helper.formatHour(program.start);
}

private function txtScheduleEnd_focusOutHandler():void {
    program.end = dtUtil.toDate(txtScheduleEnd.text, "JJ:NN:SS");
    txtScheduleEnd.text = helper.formatHour(program.end);
}

private function cgDevicesCombo_changeHandler(event:IndexChangeEvent):void {
    definePaths();
}

private function acPresenters_keyDownHandler(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        addPresenterButton_clickHandler(acPresenters.selectedItem as User);
    }
}

private function addPresenterButton_clickHandler(presenter:User):void {
    if (presenter != null && program != null && !program.hasPresenter(presenter.id)) {
        program.presenters.addItem(presenter);
        acPresenters.selectedItem = "";
    }
}

private function acEditors_keyDownHandler(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        addEditorButton_clickHandler(acEditors.selectedItem as User);
    }
}

private function addEditorButton_clickHandler(editor:User):void {
    if (editor != null && program != null && !program.hasEditor(editor.id)) {
        program.editors.addItem(editor);
        acEditors.selectedItem = "";
    }
}

private function acImageEditors_keyDownHandler(event:KeyboardEvent):void {
    if (event.keyCode == Keyboard.ENTER) {
        addImageEditorButton_clickHandler(acImageEditors.selectedItem as User);
    }
}

private function addImageEditorButton_clickHandler(imageEditor:User):void {
    if (imageEditor != null && program != null && !program.hasImageEditor(imageEditor.id)) {
        program.imageEditors.addItem(imageEditor);
        acImageEditors.selectedItem = "";
    }
}

/*-----------------Grid functions---------------------*/

private function defaultPresenterGridColumn_labelFunction(item:Object, column:GridColumn):String {
    var user:User = item as User;
    return program.defaultPresenter == user ? bundle.getString('Bundle', 'defaults.presenter') : "";
}

private function actionsGridColumn_itemRendererFunction(item:Object, column:GridColumn):ClassFactory {
    var factory:ClassFactory = new ClassFactory(PresentersActionsRenderer);
    factory.properties = { programForm: this, defaultPresenter: defaultPresenterRadioButtonGroup.selectedValue };
    return factory;
}

private function disabledGridColumn_labelFunction(item:Object, column:GridColumn):String {
    var program:Program = item as Program;
    return program.disabled == true ? bundle.getString('Bundle', 'defaults.disabled') : bundle.getString('Bundle', 'defaults.enabled');
}

//-----------------------------
//	Asynchronous answers
//-----------------------------

private function resultSaveHandler(event:ResultEvent):void {
    reset();

    ANews.showInfo(bundle.getString('Bundle', "defaults.msg.saveSuccess"), Info.INFO);

    saveButton.enabled = true;
    dataGroupPrograms.enabled = true;
}

private function faultSaveHandler(event:FaultEvent):void {
    var exception:String = event.fault.faultString as String;
    var exceptionMessage:String = event.fault.rootCause.message as String;

    if (exception.indexOf('ValidationException') != -1) {
        txtProgramName.errorString = bundle.getString('Bundle', "program.msg.areadyRegistredProgram");
    }
    saveButton.enabled = true;
}

private function resultListAllWithDisabledsHandler(event:ResultEvent):void {
    programs = event.result as ArrayCollection;
}

/*--------------------------------------*/

private function reset(event:CrudEvent = null):void {
    loadListPrograms();
    loadListUsers();

    program = new Program();
    program.cgDevice = null;
    cgDevicesCombo.selectedIndex = -1;
    playoutDevicesCombo.selectedIndex = -1;
    playoutDevicesCombo.textInput.text = "";
    defaultPresenterRadioButtonGroup.selectedValue = null;

    helper.resetErrors();

    dataGroupPrograms.enabled = true;
    this.currentState = "default";
}

private function loadListPrograms():void {
    programService.listAllWithDisableds([FetchType.CG, FetchType.EDITORS, FetchType.IMAGE_EDITORS, FetchType.PRESENTERS], resultListAllWithDisabledsHandler);
}

private function loadListUsers():void {
    userService.listAllOrderByNickname(function (event:ResultEvent):void {
        users = event.result as ArrayCollection;
    });
}

private function definePaths():void {
    // Somente é necessária a seleção do path para o protocolo Intelligent Interface, e para um dispositivo não-virtual
    var iiDevice:IIDevice = program.cgDevice as IIDevice;
    if (iiDevice && !iiDevice.virtual) {
        paths = iiDevice.paths;
    } else {
        paths = null;
    }
}