import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.DataGroup;

import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.StoryKind;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.StoryKindService;
import tv.snews.anews.utils.DomainCache;

[Bindable] private var storyKinds:ArrayCollection;

private const storykindService:StoryKindService = StoryKindService.getInstance();

// ---------------------
// Asynchronous Answer
// ---------------------
private function startUp():void {
	storykindService.subscribe(onStoryKindMessageReceived);
	storykindService.listAll(onListStoryKindsSuccess);
	this.addEventListener(CrudEvent.EDIT, edit);
}

private function onRemove(event:Event):void {
	storykindService.unsubscribe(onStoryKindMessageReceived);
	this.removeEventListener(CrudEvent.EDIT, edit);
}

private function onListStoryKindsSuccess(event:ResultEvent):void {
	storyKinds = event.result as ArrayCollection;
}

private function onStoryKindMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var storyKind:StoryKind = event.message.body as StoryKind;
	var localStoryKind:StoryKind;
	var index:int;

	switch (action) {
		case EntityAction.INSERTED:  {
			storyKinds.addItem(storyKind);
			break;
		}
		case EntityAction.UPDATED:  {
			localStoryKind = getKindById(storyKind.id);
			if (localStoryKind) {
				localStoryKind.updateFields(storyKind);
			}
			break;
		}
		case EntityAction.DELETED:  {
			localStoryKind = getKindById(storyKind.id);
			if (localStoryKind) {
				storyKinds.removeItemAt(storyKinds.getItemIndex(localStoryKind));
			}
			break;
		}
	}
}

// ---------------------
// Helpers
// ---------------------
public function edit(event:CrudEvent):void {
	var dataGroup:DataGroup = dataGroupStoryKinds.dataGroup;
	dataGroup.enabled = false;
	storyKindForm.txtStoryKindName.errorString = "";
	storyKindForm.txtStoryKindAcronym.errorString = "";
	storyKindForm.currentState = 'edit';
	storyKindForm.storyKind = 	ObjectUtil.copy(event.entity) as StoryKind;
}


private function getKindById(storykindId:int):StoryKind {	
	for each (var storyKind:StoryKind in storyKinds) {
		if (storyKind.id == storykindId) {
			return storyKind;
		}
	}
	return null;
}