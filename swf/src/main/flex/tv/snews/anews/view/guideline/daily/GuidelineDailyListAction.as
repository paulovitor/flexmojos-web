import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.controls.Menu;
import mx.core.DragSource;
import mx.core.IFactory;
import mx.core.IFlexDisplayObject;
import mx.events.CloseEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.messaging.events.MessageEvent;
import mx.messaging.messages.IMessage;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ComboBox;
import spark.components.Group;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.IGridItemRenderer;
import spark.events.GridEvent;
import spark.events.GridSelectionEvent;
import spark.events.IndexChangeEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.*;
import tv.snews.anews.view.guideline.GuidelineForm;
import tv.snews.anews.view.guideline.GuidelineHelper;
import tv.snews.anews.view.guideline.grid.GuidelineGroup;
import tv.snews.anews.component.TaskBoardEvent;
import tv.snews.flexlib.utils.DateUtil;

private const SELECTED_STATE_TOKEN:String = "AndSelected";
private const dtUtil:DateUtil = new DateUtil();
private const guidelineService:GuidelineService = GuidelineService.getInstance();
private const checklistService:ChecklistService = ChecklistService.getInstance();
private const programService:ProgramService = ProgramService.getInstance();
private const settingsService:SettingsService = SettingsService.getInstance();
private const documentService:DocumentService = DocumentService.getInstance();

private var fakeProgram:Program = null;
private var programDrawer:Program = null;
private var cache:Guideline;
private var guidelineToBeGlued:Guideline = null;
private var guidelineToBeChangeStatus:Guideline = null;

[Bindable] public var guidelines:ArrayCollection = new ArrayCollection();
[Bindable] public var programs:ArrayCollection;
[Bindable] public var programsWithProgramDrawer:ArrayCollection;
[Bindable] public var tabManager:TabManager = null;
[Bindable] public var showPreview:Boolean = false;
[Bindable] public var interrupted:Boolean = false;

[Bindable]
[ArrayElementType("String")]
private var popUpButtonOptions:ArrayCollection;
[Bindable] private var menu:Menu = new Menu();
[Bindable] private var helper:GuidelineHelper = new GuidelineHelper();
[Bindable] private var settings:Settings;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

private const sortOptions:ArrayCollection = new ArrayCollection(
		[
			{ id: GuidelineGroup.CLASSIFICATION, label: bundle.getString('Bundle', 'guideline.sortByClassification') },
			{ id: GuidelineGroup.PROGRAM,        label: bundle.getString('Bundle', 'guideline.sortByProgram') },
			{ id: GuidelineGroup.REPORTER,       label: bundle.getString('Bundle', 'guideline.sortByReporter') },
			{ id: GuidelineGroup.VEHICLE,        label: bundle.getString('Bundle', 'guideline.sortByVehicle') },
            { id: GuidelineGroup.TEAM,           label: bundle.getString('Bundle', 'guideline.sortByTeam') }
		]
);

// FIXME Cache desativado
//private const localData:LocalData = LocalData.getInstance(LocalData.GUIDELINE);

//------------------------------------------------------------------------------
//	Properties
//------------------------------------------------------------------------------

private var _selectedGuideline:Guideline;

public function get selectedGuideline():Guideline {
		return _selectedGuideline;
}

[Bindable]
public function set selectedGuideline(value:Guideline):void {
	// Se foi definido nulo, limpa as seleções
	if (value == null) {
		if (guidelineTaskBoard) {
			guidelineTaskBoard.clearSelection();
		}
		if (gridGuidelineDaily) {
			gridGuidelineDaily.selectedIndex = -1;
		}
        if(status && !guidelineToBeChangeStatus) {
            status.visible = false;
            status.includeInLayout = false;
        }
	}
	
	// Faz a troca entre normalStates e selectedStates
	var newState:String;
	if (!_selectedGuideline && value) {
		// Não havia nada selecionado e agora tem
		newState = this.currentState + SELECTED_STATE_TOKEN;
	} else if (_selectedGuideline && !value) {
		// Havia seleção e ela foi removida
		newState = this.currentState.replace(SELECTED_STATE_TOKEN, "");
	}

		_selectedGuideline = value;

	if (newState) {
		this.currentState = newState;
	}
}

//------------------------------------------------------------------------------
//	Methods
//------------------------------------------------------------------------------
private function startUp():void {
	/*
	 * NOTE Código atoa apenas para carregar as coleções lazy. Se não fizer 
	 * isso vai bugar a seleção dentro do formulário (já que vai ser carregado
	 * lá dentro após definir o selectedItem do combobox)
	 */
	DomainCache.reporters == DomainCache.producers;

    loadSettings();
	listPrograms();
	listGuidelines();

	// FIXME Cache desativado
	// verifyCacheState();

	programService.subscribe(onProgramMessageReceived);
	guidelineService.subscribe(onGuidelineMessageReceived);
	checklistService.subscribe(onChecklistMessageReceived);
	
	// Event Handler para o double-click da visualização de todos os programas e do taskBoard
	this.addEventListener(CrudEvent.UPDATE, function(event:CrudEvent):void {
		fillStatusGuideLine(event.entity as Guideline);
	});

	// Event Handler para o double-click da visualização de todos os programas e do taskBoard
	this.addEventListener(CrudEvent.EDIT, function(event:CrudEvent):void {
		edit();
	});
	
	// Event Handler para o drop de transformação de notícias em pautas
	this.addEventListener(CrudEvent.CREATE, function(event:CrudEvent):void {
		newsToGuideline(event.entity as Guideline);
	});
}

private function onGridCreationComple():void {
	// Oculta a coluna "equipe" caso for configurado para isso
	teamColumn.visible = DomainCache.settings.enableGuidelineTeam;
}

private function onViewClick(event:MouseEvent):void{
	showPreview = true;
}

private function onCancelClick(event:MouseEvent):void {
	showPreview = false;
}

private function onRemove(event:Event):void {
	programService.unsubscribe(onProgramMessageReceived);
	guidelineService.unsubscribe(onGuidelineMessageReceived);
	checklistService.unsubscribe(onChecklistMessageReceived);
}

protected function onChangeProgramCombo(event:IndexChangeEvent):void {
    interrupted = (cbGuidelineStatus.selectedItem && cbGuidelineStatus.selectedItem.id == DocumentState.INTERRUPTED);
    if (event.target is ComboBox) {
        var cb:ComboBox = event.target as ComboBox;
        if (cb.selectedItem is String) {
            cb.textInput.text = "";
            cb.selectedItem = null;
            cb.selectedIndex = -1;
        }
    }
}

public function fillStatusGuideLine(guideLine:Guideline):void {
    guidelineToBeChangeStatus = guideLine;
    cbGuidelineStatus.selectedItem = DocumentState.findById(guideLine.state);
	status.visible = true;
	status.includeInLayout = true;
	if (guidelineTaskBoard) {
		guidelineTaskBoard.isEditing = true;
	}
    interrupted = (cbGuidelineStatus.selectedItem && cbGuidelineStatus.selectedItem.id == DocumentState.INTERRUPTED);
    focusManager.setFocus(txtStatusReason);
    focusManager.showFocus();
}

private function findGuidelineById(id:Number):Guideline {
    for each (var guideline:Guideline in guidelines) {
        if (id == guideline.id) {
            return guideline;
        }
    }
    return null;
}

public function updateStatusGuideLine(yesOrNo:Boolean):void {
	if (yesOrNo) {
		if (!Util.isValid(validStatus)) {
			ANews.showInfo(bundle.getString("Bundle", "defaults.msg.requiredFields"), Info.WARNING);
			return;
		} else {
			var state:String = cbGuidelineStatus.selectedItem.id;

			var reason:String;
			if (state == DocumentState.INTERRUPTED) {
				reason = txtStatusReason.text;
			} else {
				reason = "";
			}

			guidelineService.updateState(guidelineToBeChangeStatus.id, state, reason,
					function(event:ResultEvent):void {
						selectedGuideline.state = state;
						selectedGuideline.statusReason = reason;
					}
			);
		}
	}
	txtStatusReason.errorString = "";
	txtStatusReason.text = "";
	interrupted = false;
	status.visible = false;
	status.includeInLayout = false;
	if (guidelineTaskBoard) {
		guidelineTaskBoard.isEditing = false;
	}
	guidelines.refresh();
}

private function stateLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	var stateLabel:String;

	switch (guideline.state) {
		case DocumentState.COMPLETED:
			return bundle.getString("Bundle", "document.states.complete")
		case DocumentState.COVERING:
			return bundle.getString("Bundle", "document.states.covering")
		case DocumentState.PRODUCING:
			return bundle.getString("Bundle", "document.states.producing")
		case DocumentState.INTERRUPTED:
			return bundle.getString("Bundle", "document.states.interrupt")
		case DocumentState.EDITING:
			return bundle.getString("Bundle", "document.states.editing")

	}
	return '?';
}

private function initPopUpButtonMenu():void {
	popUpButtonOptions = new ArrayCollection();

	// Opções disponíveis somente para quando tem uma pauta selecionada
	if (selectedGuideline) {
		if (ANews.USER.allow("01050108")) {
			popUpButtonOptions.addItem(bundle.getString("Bundle", "defaults.btn.copy"));
		}

        if (ANews.USER.allow("01050106")) {
            popUpButtonOptions.addItem(bundle.getString("Bundle", "defaults.btn.sendByEmail"));
        }

        if (ANews.USER.allow("01050111")) {
            popUpButtonOptions.addItem(bundle.getString("Bundle", "defaults.btn.sendToDrawer"));
        }

        popUpButtonOptions.addItem({type: "separator"});

        if (ANews.USER.allow("01050105")) {
            popUpButtonOptions.addItem(bundle.getString("Bundle", "defaults.btn.print"));
        }
	}

    if (ANews.USER.allow('01050105')) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'defaults.btn.printGrid'));
    }

    if (selectedGuideline) {

        popUpButtonOptions.addItem({type: "separator"});

        if (ANews.USER.allow('01050107')) {
            popUpButtonOptions.addItem(bundle.getString("Bundle", "defaults.btn.changeStatus"));
        }
    }

	menu.dataProvider = popUpButtonOptions;
    menu.addEventListener(MenuEvent.ITEM_CLICK, onMenuItemClick);
}

/**
 * Envia um email da pauta para o reporter da pauta.
 */
public function sendMailToReporter():void {
	popUpButton.enabled = false;

	if (selectedGuideline != null) {
		if (selectedGuideline.reporters.length > 0) {
			guidelineService.sendMailToReporter(selectedGuideline.id, onMailSuccess, onMailFail);
		} else {
			popUpButton.enabled = true;
			ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.noReporterGuidelineSelected"), Info.WARNING);
		}
	} else {
		popUpButton.enabled = true;
		ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.selectOneGuideline"), Info.WARNING);
	}
}

private function printGridGuideline():void {
	reportUtil.generate(ReportsUtil.GUIDELINE_GRID, {
		ids: "",
		date: guidelineDate.text,
		programId: cbPrograms.selectedIndex > -1 ? Program(cbPrograms.selectedItem).id : null
	});
}

private function displaySendToDrawerOption():void {
    sendToDrawer.includeInLayout = true;
    sendToDrawer.visible = true;
}

private function cancelSendToDrawer():void {
    sendToDrawer.includeInLayout = false;
    sendToDrawer.visible = false;
}

private function sendGuidelineToDrawer():void {
    if (selectedGuideline) {
        var programId:int = Program(drawerProgramCombo.selectedItem).id;
        documentService.archiveGuidelineToDrawer(selectedGuideline.id, programId, true, onSendToDrawerSuccess);

    } else {
        ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
    }
}

private function onSendToDrawerSuccess(event:ResultEvent):void {
    if (event.result == null) {
        //LoaderMask.close();
        return;
    }
    switch (event.result as String) {
        case DrawerDocumentStatus.SUCCESS:
            var selectedProgram:Program = Program(drawerProgramCombo.selectedItem);
            ANews.showInfo(bundle.getString("Bundle", "quickview.msg.sentToDrawerSuccess", [selectedProgram.name]), Info.INFO);
            cancelSendToDrawer();
            break;
        case DrawerDocumentStatus.ALREADY_INCLUDED:
            ANews.showInfo(bundle.getString("Bundle", "quickview.msg.alreadyIncluded"), Info.WARNING);
            break;
    }
}

// ---------------------------------
// Events.
// ---------------------------------

protected function onTaskBoardSelectionChange(event:TaskBoardEvent):void {
	if (event.document is Guideline) {
		selectedGuideline = event.document as Guideline;
	}
}

/**
 * Handler para o evento itemClick do menu do popUpButton.
 */
private function onMenuItemClick(event:MenuEvent):void {
	if (event.item == bundle.getString('Bundle', 'defaults.btn.printGrid')) {
		printGridGuideline();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.print")) {
		print();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.sendByEmail")) {
		sendMailToReporter();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.copy")) {
		copyTo();
	}
	if (event.item == bundle.getString("Bundle", "defaults.btn.changeStatus")) {
        changeStatus();
	}

    if (event.item == bundle.getString("Bundle", "defaults.btn.sendToDrawer")) {
        displaySendToDrawerOption();
    }
}

private function changeStatus():void {
    if(selectedGuideline) {
        var editEvent:CrudEvent = new CrudEvent(CrudEvent.UPDATE, (ObjectUtil.clone(selectedGuideline) as Guideline));
        this.dispatchEvent(editEvent);
    } else {
        ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.selectOneGuideline"), Info.WARNING);
    }
}

private function buttonBarChange(event:IndexChangeEvent):void {
	selectedGuideline = null;
	this.currentState = event.newIndex == 0 ? "listOneTaskBoard" : "listOneGrid";
}

private function onProgramMessageReceived(event:MessageEvent):void {
	listPrograms();
//	listProgramsOrderByStart();
}

/**
 * @private
 * Handler para as mensagens enviadas pelo servidor para notificar ações sobre
 * as pautas feitas pelos usuários, como operações de CRUD e bloqueio de edição.
 */
private function onGuidelineMessageReceived(event:MessageEvent):void {
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var guideline:Guideline = message.body as Guideline, current:Guideline;

	// Sugestão para a correção do problema de sincronia nas atualizações
	switch (action) {

		// Pauta foi inserida
		case EntityAction.INSERTED:
			if ((cbPrograms.selectedItem.id == -1 && guideline.program == null)	|| (cbPrograms.selectedIndex == 0 || (guideline.program && cbPrograms.selectedItem.id == guideline.program.id))) {
				if (dtUtil.compareDates(guideline.date, guidelineDate.selectedDate) == 0) {
					guidelines.addItem(guideline);
				}
			}
			break;

		// Pauta foi atualizada
		case EntityAction.UPDATED:
			for each (current in guidelines) {
				if (current.equals(guideline)) {
					current.updateFields(guideline);
					break;
				}
			}
			checkDateOrProgramChange(guideline);
			break;

		// Pauta foi enviada pra lixeira
		case EntityAction.DELETED:
			for (var i:int = 0; i < guidelines.length; i++) {
				current = guidelines.getItemAt(i) as Guideline;
				if (current.equals(guideline)) {
					guidelines.removeItemAt(i);
					if (guideline.equals(selectedGuideline)) {
						removeSelectedState();
					}
					break;
				}
			}
			break;

		// Pauta foi restaurada da lixeira
		case EntityAction.RESTORED:
			if (cbPrograms.selectedIndex == 0 || cbPrograms.selectedItem.id == guideline.program.id) {
				if (dtUtil.compareDates(guideline.date, guidelineDate.selectedDate) == 0) {
					guidelines.addItem(guideline);
				}
			}
			break;

		// Pauta foi bloqueada para edição
		case EntityAction.LOCKED:
			for each (current in guidelines) {
				if (current.equals(guideline)) {
					current.editingUser = message.headers["user"] as User;
					break;
				}
			}
			break;

		// Pauta foi desbloqueada para edição
		case EntityAction.UNLOCKED:
			for each (current in guidelines) {
				if (current.equals(guideline)) {
					current.editingUser = null;
					break;
				}
			}
			break;
	}
}

/**
 * @private
 * Handler para as mensagens enviadas pelo servidor para notificar ações sobre
 * as produções feitas pelos usuários, como operações de CRUD e bloqueio de edição.
 */
private function onChecklistMessageReceived(event:MessageEvent):void {
	var message:IMessage = event.message;
	var action:String = message.headers.action;
	var checklist:Checklist = message.body as Checklist, current:Guideline, aux:Checklist;

	switch (action) {
		case EntityAction.INSERTED:
		case EntityAction.RESTORED:
        case EntityAction.UPDATED:
        case EntityAction.DELETED:
                for each (current in guidelines) {
					if (current.equals(checklist.guideline)) {
                        action == EntityAction.DELETED ? current.checklist = null : current.checklist = checklist;
						break;
					}
				}
			break;
	}

    guidelines.refresh();
}

/**
 * Método que trata o caso de uma pauta tem sua data e/ou programa alterado(s). 
 */
private function checkDateOrProgramChange(guideline:Guideline):void {
	var programId:int = cbPrograms.selectedIndex > 0 ? Program(cbPrograms.selectedItem).id : 0;
	var sameProgram:Boolean = guideline.program ? (guideline.program.id == programId) : (programId == -1); // -1 = "Gaveta Geral"
	var sameDate:Boolean = dtUtil.compareDates(guideline.date, guidelineDate.selectedDate) == 0;

	var aux:Guideline = null;
	for each (aux in guidelines) {
		if (aux.equals(guideline)) {
			break; // com esse break o aux irá armazenar a pauta encontrada
		}
		aux = null;
	}

	var mustAdd:Boolean = false;
	var mustRemove:Boolean = false;

	if (programId == 0) {
		// Caso o combobox esteja em "todos", tem que verificar apenas a mudança da data
		mustAdd = (aux == null && sameDate);
		mustRemove = (aux != null && !sameDate);
	} else {
		// Se o combobox esta em algum programa específico, ou Gaveta Geral, tem que verificar mudanças na data ou programa
		mustAdd = (aux == null && sameDate && sameProgram);
		mustRemove = (aux != null && (!sameDate || !sameProgram));
	}

	if (mustAdd) {
		guidelines.addItem(guideline);
		guidelines.refresh();
	}

	if (mustRemove) {
		var index:int = guidelines.getItemIndex(aux);
		guidelines.removeItemAt(index);
		guidelines.refresh();

		if (guideline.equals(selectedGuideline)) {
			removeSelectedState();
		}
	}
}

/**
 * Gera impressão da pauta selecionada.
 */
private function print():void {
	if (selectedGuideline != null) {
		var params:Object = new Object();
		params.ids = selectedGuideline.id + "|";
		reportUtil.generate(ReportsUtil.GUIDELINE, params);
	}
}

private function newGuideline():void {
	var guideline:Guideline = new Guideline();
	if (cbPrograms.selectedIndex > 0) {
		guideline.program = cbPrograms.selectedItem as Program;
	}

	guideline.date = guidelineDate.selectedDate as Date;

	openForm(guideline);
}

private function newsToGuideline(guideline:Guideline):void {
	guideline.addProducer(ANews.USER);
	guideline.date = guidelineDate.selectedDate as Date;

	var program:Program = cbPrograms.selectedItem as Program;
	guideline.program = program && program.id > 0 ? program : null;

	guidelineService.save(guideline, onSaveSuccess);
}

private function edit():void {
	if (selectedGuideline != null && ANews.USER.allow("01050103")) {
		if (selectedGuideline.state == DocumentState.COMPLETED) {
			ANews.showInfo(bundle.getString("Bundle", "guideline.msg.editCompleteError"), Info.WARNING);
			return;
		}
		if (selectedGuideline.state == DocumentState.INTERRUPTED) {
			ANews.showInfo(bundle.getString("Bundle", "guideline.msg.editInterruptedError"), Info.WARNING);
			return;
		}

		// Verifica se a pauta já está sendo editada por outro
		guidelineService.isLockedForEdition(selectedGuideline.id, function(event:ResultEvent):void {
			var user:User = event.result as User;
			if (user != null) {
                helper.guidelineInEditing(user, tabManager, "guideline_" + selectedGuideline.id, bundle.getString("Bundle", "guideline.archive.title"));
			} else {
				openForm(selectedGuideline);
				guidelineService.lockEdition(selectedGuideline.id, ANews.USER.id, null);
			}
		});
	}
}

public function copyTo():void {
	guidelineToBeGlued = selectedGuideline;
	validGuidelineDateCopyTo.enabled = true;
	validCbProgramsCopyTo.enabled = true;
	sendTo.visible = true;
	sendTo.includeInLayout = true;
}


public function cancelCopyTo():void {
    guidelineToBeGlued = null;
	sendTo.visible = false;
	sendTo.includeInLayout = false;
	validGuidelineDateCopyTo.enabled = false;
	validCbProgramsCopyTo.enabled = false;
}

public function saveCopy():void {
	if (!Util.isValid(validCopyTo)) {
		ANews.showInfo(bundle.getString("Bundle", "defaults.msg.requiredFields"), Info.WARNING);
	} else {
		if (guidelineDateCopyTo && guidelineDateCopyTo.selectedDate && cbProgramsCopyTo && cbProgramsCopyTo.selectedIndex != -1) {		
			var targetProgram:Program = cbProgramsCopyTo.selectedItem as Program;
			guidelineService.copy(guidelineDateCopyTo.selectedDate, targetProgram.id, guidelineToBeGlued.id, onCopySuccess);
		}
	}
}

public function openForm(guideline:Guideline):void {
    var isNew:Boolean = guideline.id == 0 || isNaN(guideline.id);

    if (isNew) {
        tabManager.openTab(GuidelineForm, "NEW",
                {
                    guideline: Guideline(ObjectUtil.copy(guideline)),
                    tabManager: tabManager
                }
        );
    } else {
        // Na edição deve recarregar a pauta, por segurança
        guidelineService.loadById(guideline.id,
                function(event:ResultEvent):void {
                    guideline = Guideline(event.result);

					tabManager.openTab(GuidelineForm, ("guideline_" +	guideline.id),
                        {
                            guideline: Guideline(ObjectUtil.copy(guideline)),
                            tabManager: tabManager
                        }
                    );
                }
        );
    }
}

private function removeGuideline():void {
	if (selectedGuideline) {
		guidelineService.isLockedForEdition(selectedGuideline.id,
				function(event:ResultEvent):void {
					var user:User = event.result as User;
					if (user) {
						var name:String = user.equals(ANews.USER) ? bundle.getString("Bundle", "defaults.you") : ("'" + user.nickname + "'");
						ANews.showInfo(bundle.getString("Bundle", "guideline.msg.cannotDelete", [ name ]), Info.WARNING);
					} else {
						Alert.show(
							ResourceManager.getInstance().getString("Bundle", "guideline.daily.msg.confirmationDelete"),
							ResourceManager.getInstance().getString("Bundle", "defaults.msg.confirmation"),
							Alert.NO | Alert.YES, null, confirmation, null, Alert.NO
						);
					}
				
				}
		);
	} else {
		ANews.showInfo(bundle.getString("Bundle", "guideline.daily.msg.selectOneGuideline"), Info.WARNING);
	}
}

private function confirmation(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
		guidelineService.remove(selectedGuideline.id,
				function(event:ResultEvent):void {
					ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "guideline.daily.msg.removeSuccess"), Info.INFO);

					removeSelectedState();
				},
				function(event:FaultEvent):void {
					ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "guideline.msg.existGuideline"), Info.WARNING);
				}
		);
	}
}

private function removeSelectedState():void {
	selectedGuideline = null;
	this.currentState = this.currentState.replace(SELECTED_STATE_TOKEN, "");
}

private function onChangeCbPrograms(event:Event):void {
    if(sendTo.visible) cancelCopyTo();
    if(sendToDrawer.visible) cancelSendToDrawer();
	if (guidelineTaskBoard) {
		guidelineTaskBoard.clear();
	}

	var selectedProgram:Program = cbPrograms.selectedItem as Program;
	if (selectedProgram != null) {
		if (selectedProgram.id == 0) {
			this.currentState = "listAll";
		} else {
			// Somente precisa mudar o state se estivar listando todos
			if (this.currentState.indexOf("listAll") == 0) {
				this.currentState = "listOneTaskBoard";
			}
		}
		listGuidelines();
	}
}

private function startDragDrop(event:GridEvent):void {
	if (DragManager.isDragging || selectedGuideline == null)
		return;
	
	if (event.itemRenderer != null) {
		startDragDropGuideline(event);
	}
}

private function startDragDropGuideline(event:GridEvent):void {
	var sourceItens:Vector.<Object> = new Vector.<Object>();
	sourceItens.push(ObjectUtil.copy(gridGuidelineDaily.selectedItem) as Guideline)
	
	var dataSource:DragSource = new DragSource();
	dataSource.addData(sourceItens, "itemsByIndex");
	
	//Create the proxy (visual element that will be drag with cursor over the elements)
	var proxy:Group = new Group();
	proxy.width = gridGuidelineDaily.grid.width;
	
	for each (var columnIndex:int in gridGuidelineDaily.grid.getVisibleColumnIndices() as Vector.<int>) {
		var currentRenderer:IGridItemRenderer = gridGuidelineDaily.grid.getItemRendererAt(gridGuidelineDaily.selectedIndex, columnIndex);
		var factory:IFactory = gridGuidelineDaily.columns.getItemAt(columnIndex).itemRenderer;
		if (!factory)
			factory = gridGuidelineDaily.itemRenderer;
		var renderer:IGridItemRenderer = IGridItemRenderer(factory.newInstance());
		renderer.visible = true;
		renderer.column = currentRenderer.column;
		renderer.rowIndex = currentRenderer.rowIndex;
		renderer.label = currentRenderer.label;
		renderer.x = currentRenderer.x;
		renderer.y = currentRenderer.y;
		renderer.width = currentRenderer.width;
		renderer.height = currentRenderer.height;
		renderer.prepare(false);
		proxy.addElement(renderer);
		renderer.owner = gridGuidelineDaily;	
	}
	proxy.height = renderer.height;
	
	//Start the drag.
	DragManager.doDrag(gridGuidelineDaily, dataSource, event, proxy as IFlexDisplayObject, 0, -gridGuidelineDaily.columnHeaderGroup.height);
}

protected function onGridGuidelineDailySelectionChange(event:GridSelectionEvent):void {
	selectedGuideline = gridGuidelineDaily.selectedItem as Guideline;
}

private function onKeyUpHandler(event:KeyboardEvent):void {
	if (event.keyCode == Keyboard.ENTER && selectedGuideline) {
		showPreview = showPreview == false;
	}
}

// FIXME Cache desativado
//protected function onRestoreHandler(event:MouseEvent):void {
//	restoreFromCache();
//	displayRestoreForm = false;
//	localData.clear();
//}
//
//protected function onIgnoreHandler(event:MouseEvent):void {
//	displayRestoreForm = false;
//	localData.clear();
//}

//---------------------------------
//	Helpers
//---------------------------------

private function loadSettings():void {
    settingsService.loadSettings(onLoadSettings);
}

private function onLoadSettings(event:ResultEvent):void {
    settings = event.result as Settings;
    selectGuidelineGridSort();
}

private function selectGuidelineGridSort():void {
    for (var i:int = 0; i < sortOptions.length; i++) {
        if (sortOptions[i].id == settings.defaultGuidelineGridSort) {
            sortCombo.selectedIndex = i;
        }
    }
}

private function listPrograms():void {
	programs = ObjectUtil.copy(DomainCache.programs) as ArrayCollection;	
	if (programs) {
		programs.addItemAt(getFakeProgram(), 0);
		programs.addItemAt(getProgramDrawer(), 1);
		cbPrograms.selectedIndex = 0;
	}
	
	programsWithProgramDrawer = ObjectUtil.copy(DomainCache.programs) as ArrayCollection;
	
	if (programsWithProgramDrawer) {
		programsWithProgramDrawer.addItemAt(getProgramDrawer(), 0);
		cbProgramsCopyTo.selectedIndex = 0;
}
}

private function listGuidelines():void {
	var program:Program = cbPrograms.selectedItem as Program;
	var programId:int = program ? program.id : 0;
	
	guidelineService.listAllGuidelineOfDay(guidelineDate.selectedDate, programId, null, onListAllGuidelineOfDay);
}


private function getFakeProgram():Program {
	// Retorna um programa fake para ser tratado como "Todos" na listagem.
	if (fakeProgram == null) {
		fakeProgram = new Program();
		fakeProgram.id = 0;
		fakeProgram.name = bundle.getString("Bundle", "guideline.daily.allPrograms");
	}
	return fakeProgram;
}

private function getProgramDrawer():Program {
	// Retorna um programa fake para ser tratado como "GAVETA GERAL"
	if (programDrawer == null) {
		programDrawer = new Program();
		programDrawer.id = -1;
		programDrawer.name = bundle.getString("Bundle", "defaults.drawerProgram");
	}
	return programDrawer;
}

// FIXME Cache desativado
//private function verifyCacheState():void {
//	cache = localData.data as Guideline;
//	if (cache != null) {
//		displayRestoreForm = true;
//	}
//}

private function copyData(from:Guideline, to:Guideline):void {
	var fieldsToCopy:Array = ["date", "guides", "informations", "program", "proposal", "referral", "reporter", "producer", "slug"];
	for each (var field:String in fieldsToCopy) {
		to[field] = from[field];
	}
}

private function restoreFromCache():void {
	if (cache.id > 0) {
		guidelineService.loadById(cache.id, function(event:ResultEvent):void {
			var guideline:Guideline = event.result as Guideline;
			if (guideline) {
				copyData(cache, guideline);
				openForm(guideline);
			} else {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "guideline.daily.msg.restoreFailed"), Info.WARNING);
			}
		});
	} else {
		var guideline:Guideline = new Guideline();
		copyData(cache, guideline);
		openForm(guideline);
	}
}

//---------------------------------
//	Asynchronous Answers
//---------------------------------
private function onListAllGuidelineOfDay(event:ResultEvent):void {
    selectedGuideline = null;
    status.visible = false;
    status.includeInLayout = false;
    if (guidelineTaskBoard) {
        guidelineTaskBoard.isEditing = false;
    }
	guidelines.removeAll();
	guidelines.addAll(event.result as ArrayCollection);
	guidelines.refresh();
}

private function onCopySuccess(event:ResultEvent):void {
	sendTo.visible = false;
	sendTo.includeInLayout = false;
	validGuidelineDateCopyTo.enabled = true;
	validCbProgramsCopyTo.enabled = true;
	ANews.showInfo(bundle.getString("Bundle", "defaults.msg.copySuccess"), Info.INFO);
}

// ---------------------------------
// Formatters.
// ---------------------------------
private function timeLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	var i:int = 0;
	var firstDate:Date;

	for each (var guide:Guide in guideline.guides) {
		if (i == 0) {
			firstDate = guide.schedule;
		} else {
			if (ObjectUtil.dateCompare(guide.schedule, firstDate) > -1) {
				firstDate = guide.schedule;
			}
		}
		i++;
	}
//	updateBtnEmailVisible();
	return dtUtil.toString(firstDate, "JJhMM");
}

private function formatTime(timestamp:Date):String {
	return dtUtil.toString(timestamp, bundle.getString("Bundle", "defaults.timeFormatHourMinute"));
}

private function formatDate(timestamp:Date):String {
	return dtUtil.toString(timestamp, bundle.getString("Bundle", "defaults.dateFormat"));
}

private function onSaveSuccess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString("Bundle", "defaults.msg.saveSuccess"), Info.INFO);
}

private function onMailSuccess(event:ResultEvent):void {
//	updateBtnEmailVisible();
	popUpButton.enabled = true;
	ANews.showInfo(bundle.getString("Bundle", "guideline.msg.mailSuccess"), Info.INFO);
}

private function onMailFail(event:FaultEvent):void {
	popUpButton.enabled = true;
	ANews.showInfo(event.fault.faultString, Info.WARNING);
}

private function droppedDownLabelFunction(item:Object, column:GridColumn):String {
	var bundleField:String;
	if (item.droppedDown) {
		bundleField = "guideline.daily.droppedDownTrue";
	} else if (item.isEditing) {
		bundleField = "guideline.daily.inEditing";
	} else {
		bundleField = "guideline.daily.droppedDownFalse";
	}
	return bundle.getString("Bundle", bundleField);
}

private function scheduleLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	var guide:Guide = guideline ? guideline.firstGuide() : null;
	return guide ? formatTime(guide.schedule) : "";
}

private function programLabelFunction(item:Object, column:GridColumn):String {
	var guideline:Guideline = item as Guideline;
	return guideline.program ? guideline.program.name : this.resourceManager.getString("Bundle", "defaults.drawerProgram");
}

private function pendencyLabelFunction(item:Object, column:GridColumn):String {
    var guideline:Guideline = item as Guideline;
	return guideline && guideline.checklist && !guideline.checklist.excluded ? guideline.checklist.tasksAsStr : '';
}

private function teamLabelFunction(item:Object, column:GridColumn):String {
    var guideline:Guideline = item as Guideline;
    return guideline.team ? guideline.team.name : this.resourceManager.getString("Bundle", "guideline.grid.noTeamLowerCase");
}
