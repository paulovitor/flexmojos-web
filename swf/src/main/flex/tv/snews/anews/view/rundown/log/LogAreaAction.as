import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.collections.Sort;
import mx.collections.SortField;
import mx.controls.Alert;
import mx.core.IVisualElement;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.rpc.events.ResultEvent;

import spark.components.CheckBox;
import spark.components.Panel;
import spark.components.VGroup;
import spark.components.gridClasses.GridColumn;

import tv.snews.anews.domain.AbstractLog;
import tv.snews.anews.domain.BlockAction;
import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.RundownAction;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.StoryAction;
import tv.snews.anews.flex.PageResult;
import tv.snews.anews.service.LogService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.events.PageBarEvent;

private var actionsBlock:ArrayCollection = new ArrayCollection();
private var actionsRundown:ArrayCollection = new ArrayCollection();
private var actionsStory:ArrayCollection = new ArrayCollection();

[Bindable] private var totalSelectedStory:int = 0;
[Bindable] private var totalSelectedRundown:int = 0;
[Bindable] private var totalSelectedBlock:int = 0;

[Bindable] private var isRunning1:Boolean = false;
[Bindable] private var isRunning2:Boolean = false;
[Bindable] private var isRunning3:Boolean = false;

private const logService:LogService = LogService.getInstance();

[Bindable] public var rundown:Rundown;

//------------------------------------------------------------------
//	Event Handlers
//------------------------------------------------------------------
protected function onCheckboxChangeRundown(event:Event):void {
	var checkBox:CheckBox =	event.target as CheckBox;
	if ( checkBox.selected) { 
		totalSelectedRundown++;
	} else { 
		totalSelectedRundown--;
	}
	
	if(!checkBox.selected && totalSelectedRundown == 0) {
		groupActionsBlock.enabled = true;
		groupActionsStory.enabled = true;
		generalCbBlock.enabled = true;
		generalCbStory.enabled = true;
	} else {
		groupActionsBlock.enabled = false;
		groupActionsStory.enabled = false;
		generalCbBlock.enabled = false;
		generalCbStory.enabled = false;
	}
}

protected function onCheckboxChangeStory(event:Event):void {
	var checkBox:CheckBox =	event.target as CheckBox;
	if ( checkBox.selected) { 
		totalSelectedStory++;
	} else { 
		totalSelectedStory--;
	}
	
	if(!checkBox.selected && totalSelectedStory == 0) {
		groupActionsBlock.enabled = true;
		groupActionsRundown.enabled = true;
		generalCbBlock.enabled = true;
		generalCbRundown.enabled = true;
	} else {
		groupActionsBlock.enabled = false;
		groupActionsRundown.enabled = false;
		generalCbBlock.enabled = false;
		generalCbRundown.enabled = false;
	}
} 

protected function onCheckboxChangeBlock(event:Event):void {
	var checkBox:CheckBox =	event.target as CheckBox;
	if ( checkBox.selected) { 
		totalSelectedBlock++;
	} else { 
		totalSelectedBlock--;
	}
	
	if(!checkBox.selected && totalSelectedBlock == 0) {
		groupActionsStory.enabled = true;
		groupActionsRundown.enabled = true;
		generalCbStory.enabled = true;
		generalCbRundown.enabled = true;
	} else {
		groupActionsStory.enabled = false;
		groupActionsRundown.enabled = false;
		generalCbStory.enabled = false;
		generalCbRundown.enabled = false;
	}
	
}

protected function onCreationCompleteGroupActions(event:FlexEvent):void {
	var actionsStory:ArrayCollection =  StoryAction.getAllActions();
	var actionsBlock:ArrayCollection =  BlockAction.getAllActions();
	var actionsRundown:ArrayCollection =  RundownAction.getAllActions();
	var cb:CheckBox = null;
	
	for each (var action:Object in actionsStory) {
		cb =  new CheckBox();
		cb.id = action.id;
		cb.label = action.label;
		cb.addEventListener(Event.CHANGE, onCheckboxChangeStory);
		groupActionsStory.addElement(cb);
	}
	
	for each (var action2:Object in actionsBlock) {
		cb =  new CheckBox();
		cb.id = action2.id;
		cb.label = action2.label;
		cb.addEventListener(Event.CHANGE, onCheckboxChangeBlock);
		groupActionsBlock.addElement(cb);
	}
	
	for each (var action3:Object in actionsRundown) {
		cb =  new CheckBox();
		cb.id = action3.id;
		cb.label = action3.label;
		cb.addEventListener(Event.CHANGE, onCheckboxChangeRundown);
		groupActionsRundown.addElement(cb);
	}
	
}

protected function onContentCreationComplete(event:FlexEvent):void {
	logService.listAll(rundown.id, 1, onSearchSuccess);
}

 private function disableOthers(vgroup:VGroup):void {
	if (vgroup == groupActionsRundown) {
		groupActionsStory.enabled = false;
		groupActionsBlock.enabled = false;
	} else if (vgroup == groupActionsStory) {
		groupActionsBlock.enabled = false;
		groupActionsRundown.enabled = false;
	} else if (vgroup == groupActionsBlock) {
		groupActionsStory.enabled = false;
		groupActionsRundown.enabled = false;
	}
} 

protected function onSearch(event:PageBarEvent = null):void {
	if (totalSelectedBlock == 0 && totalSelectedRundown == 0 && totalSelectedStory == 0) {
		logService.listAll(rundown.id, 1, onSearchSuccess);
	} else {
		actionsBlock = new ArrayCollection();
		actionsRundown = new ArrayCollection();
		actionsStory = new ArrayCollection();
		var page:int = event == null || event.page < 1 ? 1 : event.page;
		var cb:CheckBox = null;
		
		for(var j:int = 0; j < groupActionsBlock.numElements; j++) {
			cb = groupActionsBlock.getElementAt(j) as CheckBox;
			if (cb.selected) {
				actionsBlock.addItem(cb.id);
			}
		}
		
		for(var i:int = 0; i < groupActionsRundown.numElements; i++) {
			cb = groupActionsRundown.getElementAt(i) as CheckBox;
			if (cb.selected) {
				actionsRundown.addItem(cb.id);
			}
		}
		
		for(var k:int = 0; k < groupActionsStory.numElements; k++) {
			cb  = groupActionsStory.getElementAt(k) as CheckBox;
			if (cb.selected) {
				actionsStory.addItem(cb.id);
			}
		}
		
		if ( actionsBlock.length > 0) {
			logService.listLogsByBlockAction(rundown.id, actionsBlock, page, onSearchSuccess)
		} else if (actionsRundown.length > 0) {
			logService.listLogsByRundownAction(rundown.id, actionsRundown, page, onSearchSuccess)
		} else if (actionsStory.length > 0) {
			logService.listLogsByStoryAction(rundown.id, actionsStory, page, onSearchSuccess)
		}
	}
}

private function onSearchSuccess(event:ResultEvent):void {
	var result:PageResult = event.result as PageResult;
	pgLog.load(result.results, result.pageSize, result.total, result.pageNumber);
}

private function dateLabelFunction(item:Object, column:GridColumn):String {
	return Util.getDateUtil().toString(item.date, "JJ:NN:SS");
}

protected function onClear(event:MouseEvent=null):void {
	pgLog.load(null, 0,0,0);
	var cb:CheckBox = null;
	for(var j:int = 0; j < groupActionsBlock.numElements; j++) {
		cb = groupActionsBlock.getElementAt(j) as CheckBox;
		if (cb.selected) {
			cb.selected = false;
		}
	}
	for(var i:int = 0; i < groupActionsRundown.numElements; i++) {
		cb = groupActionsRundown.getElementAt(i) as CheckBox;
		if (cb.selected) {
			cb.selected = false;
		}
	}
	for(var k:int = 0; k < groupActionsStory.numElements; k++) {
		cb  = groupActionsStory.getElementAt(k) as CheckBox;
		if (cb.selected) {
			cb.selected = false;
		}
	}
	groupActionsBlock.enabled = true;
	groupActionsStory.enabled = true;
	groupActionsRundown.enabled = true;
	generalCbBlock.selected = false;
	generalCbRundown.selected = false;
	generalCbStory.selected = false;
	generalCbBlock.enabled = true;
	generalCbRundown.enabled = true;
	generalCbStory.enabled = true;
	totalSelectedBlock = 0;
	totalSelectedStory = 0;
	totalSelectedRundown = 0;
}

private function generalSelectionStory(event:Event):void {
	if (!isRunning1) {
		isRunning1 = true;
		var cb:CheckBox = null;
		var target:CheckBox = event.target as CheckBox;
		if (target.selected) {
			for(var k:int = 0; k < groupActionsStory.numElements; k++) {
				cb  = groupActionsStory.getElementAt(k) as CheckBox;
				cb.selected = true;
			}
			totalSelectedStory = groupActionsStory.numElements;
			groupActionsBlock.enabled = false;
			groupActionsRundown.enabled = false;
			generalCbBlock.enabled = false;
			generalCbRundown.enabled = false;
		} else {
			for(var b:int = 0; b < groupActionsStory.numElements; b++) {
				cb  = groupActionsStory.getElementAt(b) as CheckBox;
				cb.selected = false;
			}
			totalSelectedStory = 0;
			groupActionsBlock.enabled = true;
			groupActionsRundown.enabled = true;
			generalCbBlock.enabled = true;
			generalCbRundown.enabled = true;
		}
		isRunning1 = false;
	}
}

private function generalSelectionRundown(event:Event):void {
	if (!isRunning2) {
		isRunning2 = true;
		var cb:CheckBox = null;
		if ((event.target as CheckBox).selected) {
			for(var i:int = 0; i < groupActionsRundown.numElements; i++) {
				cb = groupActionsRundown.getElementAt(i) as CheckBox;
				cb.selected = true;
			}
			totalSelectedRundown = groupActionsRundown.numElements;
			generalCbBlock.enabled = false;
			generalCbStory.enabled = false;
			groupActionsBlock.enabled = false;
			groupActionsStory.enabled = false;
		} else {
			for(var o:int = 0; o < groupActionsRundown.numElements; o++) {
				cb = groupActionsRundown.getElementAt(o) as CheckBox;
				cb.selected = false;
			}
			totalSelectedRundown = 0;
			generalCbBlock.enabled = true;
			generalCbStory.enabled = true;
			groupActionsBlock.enabled = true;
			groupActionsStory.enabled = true;
		}
		isRunning2 = false;
	}
}

private function generalSelectionBlock(event:Event):void {
	if (!isRunning3) {
		isRunning3 = true;
		var cb:CheckBox = null;
		if ((event.target as CheckBox).selected) {
			for(var j:int = 0; j < groupActionsBlock.numElements; j++) {
				cb = groupActionsBlock.getElementAt(j) as CheckBox;
				cb.selected = true;
			}
			totalSelectedBlock = groupActionsBlock.numElements;
			groupActionsRundown.enabled = false;
			groupActionsStory.enabled = false;
			generalCbRundown.enabled = false;
			generalCbStory.enabled = false;
		} else {
			for(var l:int = 0; l < groupActionsBlock.numElements; l++) {
				cb = groupActionsBlock.getElementAt(l) as CheckBox;
				cb.selected = false;
			}
			totalSelectedBlock = 0;
			groupActionsRundown.enabled = true;
			groupActionsStory.enabled = true;
			generalCbRundown.enabled = true;
			generalCbStory.enabled = true;
		}
		isRunning3 = false;
	}
}