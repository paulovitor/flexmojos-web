package tv.snews.anews.view.navigator {

	public class SearchFilter {

		private var _labelFilter:String = "";
		private var _nameField:String = "";

		public function SearchFilter() {

		}

		// --------------------------
		// SETTERS AND GETTERS
		// --------------------------

		[Bindable] public function get labelFilter():String {
			return _labelFilter;
		}

		public function set labelFilter(value:String):void {
			_labelFilter = value;
		}

		[Bindable] public function get nameField():String {
			return _nameField;
		}

		public function set nameField(value:String):void {
			_nameField = value;
		}
	}
}
