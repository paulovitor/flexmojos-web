import flash.events.MouseEvent;
import flash.utils.setTimeout;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.RssCategoryService;
import tv.snews.anews.service.RssService;
import tv.snews.anews.utils.Util;

private static const HTTP_PREFIX:String = "http://";

private static const STATES:Object = {
	NORMAL	: "normal",
	EDIT	: "edit"
};

private var feed:RssFeed;

[Bindable]
private var selectedCategory:RssCategory = new RssCategory();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.RssCategory")]
private var categories:ArrayCollection = new ArrayCollection();

private const categoryService:RssCategoryService = RssCategoryService.getInstance();
private const rssService:RssService = RssService.getInstance();

//------------------------------------------------------------------------------
//	Event Handlers
//------------------------------------------------------------------------------
private function startUp(event:FlexEvent):void {
	categoriesDataGrid.addEventListener(CrudEvent.EDIT, onCategoryEdit);
	categoriesDataGrid.addEventListener(CrudEvent.DELETE, onDeleteRssCategory);
	rssService.subscribe(onMessageReceivedRssHandler);
	loadCategoriesList();
}

private function onRemove(event:FlexEvent):void {
	categoriesDataGrid.removeEventListener(CrudEvent.EDIT, onCategoryEdit);
	categoriesDataGrid.removeEventListener(CrudEvent.DELETE, onDeleteRssCategory);
	rssService.unsubscribe(onMessageReceivedRssHandler);
}

private function onDataGridSelectionChange():void {
	selectedCategory = categoriesDataGrid.selectedItem as RssCategory;
	this.currentState = selectedCategory ? "selected" : "normal";
}



private function onMessageReceivedRssHandler(event:MessageEvent):void {
	var feed:RssFeed = event.message.body as RssFeed;
	var type:String = event.message.headers["type"] as String;
	
	if (type != RssService.FEED_UPDATED) {
		loadCategoriesList(500);
	}
}

/**
 * @private
 * Handler para o evento disparado pela lista de categorias quando uma delas 
 * será editada.
 */
private function onCategoryEdit(event:CrudEvent):void {
	selectedCategory = event.entity as RssCategory;
	putOnEditState();
}

private function onDeleteRss(event:CloseEvent):void{
	if (event.detail == Alert.YES) {
		feed.category.feeds.removeAll();
		rssService.deleteRssFeed(feed, 
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.deleteSuccess"), Info.INFO);
			}
		);
	}
}

private function onDeleteRssCategory(event:CloseEvent):void{
	if (event.detail == Alert.YES) {
		
		if (selectedCategory.feeds.length == 0) {
			setButtonsEnabled(false);
			
			categoryService.remove(selectedCategory, 
				function(event:ResultEvent):void {
					ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.deleteSuccess"), Info.INFO);
					putOnNormalState();
					
					setButtonsEnabled(true);
				},
				defaultFailHandler
			);
		} else {
			ANews.showInfo(this.resourceManager.getString("Bundle" , "rss.categoryDeleteNotAllowed"), Info.WARNING);
		}
	}	
}
//--------------------------------------
//	Category Events
//--------------------------------------

private function onCategorySave(event:MouseEvent):void {
	fillCategory();
	
	if (Util.isValid(categoryValidators)) {
		setButtonsEnabled(false);
		
		categoryService.insert(selectedCategory, 
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.saveSuccess"), Info.INFO);
				putOnNormalState();
				
				setButtonsEnabled(true);
			},
			defaultFailHandler
		);
	} else {
		ANews.showInfo(this.resourceManager.getString("Bundle" , "defaults.msg.saveFail"), Info.WARNING);
	}
}

private function onCategoryUpdate(event:MouseEvent):void {
	fillCategory();
	
	if (Util.isValid(categoryValidators)) {
		setButtonsEnabled(false);
		
		categoryService.update(selectedCategory, 
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.saveSuccess"), Info.INFO);
				putOnNormalState();
				
				setButtonsEnabled(true);
			},
			defaultFailHandler
		);
	} else {
		ANews.showInfo(this.resourceManager.getString("Bundle" , "rss.msg.categoryUpdateError"), Info.WARNING);
	}
}

private function onCategoryCancel(event:MouseEvent):void {
	putOnNormalState();
}

private function defaultFailHandler(event:FaultEvent):void {
	ANews.showInfo(event.fault.faultString, Info.ERROR);
	setButtonsEnabled(true);
}

//------------------------------------------------------------------------------
//	Helper Methods
//------------------------------------------------------------------------------

private function fillCategory():void {
	selectedCategory.name = catNameInput.text;
	selectedCategory.color = catColorPicker.selectedColor;
}

private function loadCategoriesList(delay:int = 0):void {
	if (delay > 0) {
		flash.utils.setTimeout(loadCategoriesList, delay);
	}
	
	categoryService.listAll(function(event:ResultEvent):void {
		categories = event.result as ArrayCollection;
	});
}

private function putOnNormalState():void {
	this.currentState = STATES.NORMAL;
	selectedCategory = new RssCategory();
}

private function putOnEditState():void {
	this.currentState = STATES.EDIT;
	setButtonsEnabled(true);
}

private function setButtonsEnabled(enabled:Boolean):void {
	if (this.currentState == STATES.NORMAL) {
		saveCatButton.enabled = enabled;
	} else {
		cancelCatButton.enabled = enabled;
		updateCatButton.enabled = enabled;
	}
}
