import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.charts.chartClasses.NumericAxis;
import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.DragSource;
import mx.core.mx_internal;
import mx.events.*;
import mx.managers.DragManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.components.DataGroup;
import spark.components.List;
import spark.layouts.supportClasses.DropLocation;

import tv.snews.anews.component.Info;
import tv.snews.anews.component.LabelWithFocus;
import tv.snews.anews.domain.*;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.*;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.rundown.BlockRenderer;
import tv.snews.anews.view.rundown.StoryRenderer2;
import tv.snews.flexlib.utils.StringUtils;

use namespace mx_internal;

private const rundownService:RundownService = RundownService.getInstance();
private const storyService:StoryService = StoryService.getInstance();
private const documentService:DocumentService = DocumentService.getInstance();
private const intelligentInterfaceService:IntelligentInterfaceService = IntelligentInterfaceService.getInstance();
//private const gcDeviceService:CGDeviceService = CGDeviceService.getInstance();

[Bindable] static public var _verifyDevice:Boolean = false;

[ArrayElementType("String")]
[Bindable] private var logLastCommandExecutionError:ArrayCollection = new ArrayCollection();

[Bindable] public var rundown:Rundown;
[Bindable] public var selectedStory:Story;

[Bindable]
private var shouldMove:Boolean = false;
private var isCleaning:Boolean = false;

protected function startUp(event:FlexEvent):void {
	storyService.subscribe(onStoryMessageReceived);
	documentService.subscribe(onDocumentMessageReceived);
    rundownService.subscribe(onRundownMessageReceived);
    intelligentInterfaceService.subscribe(onIntelligentInterfaceMessageReceived);
    //Handler de quando uma lauda é selecionada.
//    this.addEventListener(CrudEvent.SELECT_CHANGE, onSelectedStory);
//	gcDeviceService.subscribe(onGcDeviceMessageReceived);
    DomainCache.mamDevices.length;

	// Ajustes no drag & scroll da lista de blocos
	this.rundownListLayout.dragScrollInitialDelay = 0; // sem delay no início
//	this.rundownListLayout.dragScrollRegionSizeHorizontal *= 10;
//	this.rundownListLayout.dragScrollRegionSizeVertical *= 10;
	this.rundownListLayout.dragScrollSpeed *= 8; // aumenta a velocidade em 8 vezes
}

public function dispose():void {
	storyService.unsubscribe(onStoryMessageReceived);
	rundownService.unsubscribe(onRundownMessageReceived);
	intelligentInterfaceService.unsubscribe(onIntelligentInterfaceMessageReceived);
	documentService.unsubscribe(onDocumentMessageReceived);
    this.removeEventListener(CrudEvent.SELECT_CHANGE, onSelectedStory);
	if (timeControl) { timeControl.dispose(); }
//	gcDeviceService.unsubscribe(onGcDeviceMessageReceived);
}

//--------------------------------------
//	Drag & Drog Methods
//--------------------------------------

private function onDragOver(event:DragEvent):void {
	/*
	 * NOTE Chamar o método preventDefault() causa um bug no scroll da lista. Isso
	 * porque o método dragOverHandler() do componente List deixa de ser executado.
	 *
	 * Para evitar esse bug, copiei o código do método dragOverHandler() do
	 * componente List e fiz as modificações necessárias.
	 */

	// Não deixa executar o listener padrão para que possa exibir o feedback NONE
	event.preventDefault();

	// Trecho de código retirado de List.dragOverHandler()
	// Garante que o scroll continue funcionando
	var dropLocation:DropLocation = blockRendererList.layout.calculateDropLocation(event);
	if (dropLocation) {
        blockRendererList.drawFocusAnyway = true;
        blockRendererList.drawFocus(true);

        blockRendererList.layout.showDropIndicator(dropLocation);
	} else {
        blockRendererList.layout.hideDropIndicator();

        blockRendererList.drawFocus(false);
        blockRendererList.drawFocusAnyway = false;
	}

	// Notifica que não aceita drop
	DragManager.showFeedback(DragManager.NONE);
}

private function onDragDrop(event:DragEvent):void {
	// Não aceita nenhum tipo de drop na lista de blocos
	event.preventDefault();
}

/**
 * Reorganize page as ordered by other users.
 */
public function sortPages():void {
	var order:int = 0;
	for each (var block:Block in rundown.blocks) {
		for each (var story:Story in block.stories) {
			story.page = ++order < 10 ? "0" + order : "" + order;
		}
	}
}

/**
 * Reset display as ordered by other users.
 */
public function resetDisplay():void {
    for each (var blockRenderer:BlockRenderer in blockItemRenderers()) {
        for each (var storyRenderer:StoryRenderer2 in blockRenderer.storyItemRenderers()){
            storyRenderer.resetRundownDisplay();
        }
    }
}

/**
 * Complet display as ordered by other users.
 */
public function completeDisplay():void {
    for each (var blockRenderer:BlockRenderer in blockItemRenderers()) {
        for each (var storyRenderer:StoryRenderer2 in blockRenderer.storyItemRenderers()){
            storyRenderer.completeRundownDisplay();
        }
    }
}


/**
 * The story to be inserted on the rundown.
 * Each story MUST be in the correct order and MUST has his block.
 *
 * @param story
 */
public function addStory(story:Story):void {
	var localBlock:Block = story.block;

	var order:int = story.order;
	if (order < 0 || order > localBlock.stories.length) {
		throw new Error("ArrayOutBoundException: The story has a order out of bound of the block.");
	}

	localBlock.stories.addItemAt(story, order);
	localBlock.reorder(); //Organize indexes.
	localBlock.updateTimers();
	rundown.reorderGlobalIndexes(); //Organize global indexes
}

/**
 * Delete a story from the rundown
 *
 * @param story the story to be deleted
 */
public function removeStory(story:Story):void {
	//Get the local referente
	var block:Block = story.block;
	block.stories.removeItemAt(block.stories.getItemIndex(story));

	block.reorder(); //Organize indexes.
	block.updateTimers(); //Organize timers do block.
	rundown.updateTimers(); //Organize timers do rundown.
	rundown.reorderGlobalIndexes(); //Organize global indexes
}

/**
 * Move a list of story to a new location on the same block or in different block
 * @param story
 */
public function moveStory(newBlockId:Number, newIndex:int, oldStory:Story):void {
	//Get the current block used on the rundown. He has a differente UID,
	//so is prefered to work with the local copy of the block
	var localBlock:Block = getBlockById(newBlockId);

	if (localBlock == null) {
		throw new Error("IllegalArgumentException: The story has a undefined block so it couldn't be inserted.");
		return;
	}

	//If the user is moving itens on the same block, we should check if He's moving down on the list.
	//If the user is moving down, the dropIndex will decriased.
	//if (story.block.id == blockId) {
	//	if (story.order < newIndex) {
	//		newIndex--;
	//	}
	//}

	var oldBlock:Block = oldStory.block;

	//remove it from its old block
	oldBlock.stories.removeItemAt(oldBlock.stories.getItemIndex(oldStory));
	oldStory.block = null;

	//add it on its new block
	localBlock.stories.addItemAt(oldStory, newIndex);
	oldStory.block = localBlock;

	oldBlock.reorder(); // Organize indexes of the old block.
	oldBlock.updateTimers(); // Organize timers of the old block.

	if (oldBlock.id != localBlock.id) {
		localBlock.reorder(); // Organize indexes of the block.
		localBlock.updateTimers(); // Organize timers of block.
	}

	rundown.updateTimers(); //Organize timers of rundown.
	rundown.reorderGlobalIndexes(); //Organize global indexes
}

// -----------------------
// Actions
// -----------------------

public function showCopyTo(move:Boolean):void {
	if (verifySelectedStories()) {
		shouldMove = move;
		copyTo.visible = true;
		copyTo.includeInLayout = true;
	}
}

public function hideCopyTo():void {
	copyTo.visible = false;
	copyTo.includeInLayout = false;
}

/**
 * 	Efetua a cópia de uma lauda selecionada no espelho para
 *  um programa e data selecionados pelo usuário.
 */
private function onCopyTo():void {
	var date:Date = destinyDay.selectedDate;
	var programId:int = (destinyProgram.selectedItem as Program).id;

	if (date && programId) {
		rundownService.verifyRundown(date, programId,
				function(event:ResultEvent):void {
					var resultCode:String = event.result as String;
					var rm:IResourceManager = ResourceManager.getInstance();

					switch (resultCode) {
						case RundownStatus.EXIST:
							storyService.copyTo(selectedStoriesIds(), date, programId, shouldMove,
									function(event:ResultEvent):void {
										hideCopyTo();
										ANews.showInfo(rm.getString("Bundle", "story.successCopy"), Info.INFO);
									}
							);
							break;
						default:
							ANews.showInfo(rm.getString("Bundle", "story.noRundown"), Info.WARNING);
					}
				}
		);
	} else {
		ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "defaults.msg.requiredFields"), Info.WARNING);
	}
}

public function copyStories():void {
	if (verifySelectedStories()) {
		sendStoriesToClipboard(ClipboardAction.COPY);
	}
}

public function cutStories():void {
	if (verifySelectedStories()) {
		sendStoriesToClipboard(ClipboardAction.CUT);
	}
}

public function pastStories(block:Block=null):void {
	if (selectedStory == null && block == null) {
		for each (var blockTmp:Block in rundown.blocks) {
			if (blockTmp.standBy) {
				past(blockTmp.id, -1 /*significa que vai colar/recortar no final do bloco */);
				return;
			}
		}
	}

	if (block) {
        if(selectedStory && selectedStory.block.id == block.id) {
            past(selectedStory.block.id , (selectedStory.order+1));
        } else {
            past(block.id, -1 /*significa que vai colar/recortar no final do bloco */);
        }
	} else if (selectedStory) {
		past(selectedStory.block.id , (selectedStory.order+1));
	}
}

public function clearClipboard():void {
	if (!isCleaning) {
		isCleaning = true;
		storyService.clearClipboard(function(event:ResultEvent):void{
			removeMarkupsClipboard();
			isCleaning = false;
		},	function(event:FaultEvent):void{
			isCleaning = false;
		});
	}
}


private function removeMarkupsClipboard():void{
	for each (var block:Block in rundown.blocks) {
		for (var i:int = block.stories.length - 1; i >= 0; i--) {
			var _story:Story = block.stories.getItemAt(i) as Story;

			_story.cutted = false;
			_story.copied = false;
		}
	}

 	deselectAll();
	selectedStory = null;
}
public function verifySelectedStories():Boolean {
	if (selectedStoriesIds().length == 0) {
		ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "rundown.story.noSelectWarning"), Info.WARNING);
		return false;
	}
	return true;
}


private function sendStoriesToClipboard(action:String):void {

	storyService.sendStoriesToClipboard(action, selectedStoriesIds(), function(event:ResultEvent):void {
		var clipboardIds:ArrayCollection =  new ArrayCollection(event.result as Array);

		for each (var block:Block in rundown.blocks) {
			for (var i:int = block.stories.length - 1; i >= 0; i--) {
				var _story:Story = block.stories.getItemAt(i) as Story;
                _story.cutted = false;
                _story.copied = false;

                if (clipboardIds.contains(_story.id)) {
                    if(action == ClipboardAction.COPY)
                        _story.copied = true;
                    else{
                        if (_story.displaying || _story.displayed)
                            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'rundown.block.cantCutDisplayed', [ StringUtils.isNullOrEmpty(_story.slug) ?
                                    ResourceManager.getInstance().getString('Bundle', 'defaults.noSlug') : _story.slug ]), Info.WARNING);

                        // Mesmo que a lauda esteja em exibida ou em exibição, ela deve estar marcada para controle do usuário
                            _story.cutted = true;
                    }
                }
			}
		}
	});
}

private function past(blockId:Number, pastIndex:int):void {
	storyService.pastStoriesFromClipboard(blockId, pastIndex , function(event:ResultEvent):void {
		removeMarkupsClipboard();
	});
}

// -----------------------
// Asynchronous answer
// -----------------------
private function onRundownMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];

	var rundownUpdated:Rundown;

	switch (action) {
		case EntityAction.FIELD_UPDATED:
			var rundownId:Number = event.message.headers["rundownId"];
			var field:String = event.message.headers["field"];
			var value:Object = event.message.headers["value"];

			if (rundown.id == rundownId) {
				rundown[field] = value;
			}
			break;
		case RundownAction.SORTED:
			rundownUpdated = event.message.body as Rundown;
			if (rundown.id == rundownUpdated.id) {
				sortPages();
			}
			break;
        case RundownAction.DISPLAY_RESETS:
					rundownUpdated = event.message.body as Rundown;
            if (rundown.id == rundownUpdated.id) {
                rundown.displayed = false;
                resetDisplay();
            }
            break;
        case RundownAction.COMPLETE_DISPLAY:
					rundownUpdated = event.message.body as Rundown;
            if (rundown.id == rundownUpdated.id) {
                rundown.displayed = true;
                completeDisplay();
            }
            break;
		default:
			//INSERTED, DELETED AND UPDATE are not supported on the rundown.
			throw new Error("You receive an undefined message for a rundown, try to reload you rundown!");
	}
}

private function onStoryMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var multiple:Boolean = event.message.headers["multiple"] as Boolean;
	var story:Story = event.message.body as Story;
	var stories:ArrayCollection = event.message.body as ArrayCollection;
	var rundownId:Number = Number(event.message.headers["rundownId"]);
	var localStory:Story = null;

	if (rundown && rundown.id && rundownId) {
		switch (action) {
			case EntityAction.INSERTED:
				var localBlock:Block = getBlockById(story.block.id);
				if (localBlock) {
					story.block = localBlock;
					addStory(story);
				}
				break;
			case EntityAction.DELETED:
				localStory = getStoryById(story.id);
				if (localStory) {
					removeStory(localStory);
				}
				break;
			case RundownAction.DRAWER:
				//this message isn't for this user.
				localStory = getStoryById(story.id);
				if (localStory) {
					removeStory(localStory);
				}
				break;
			case EntityAction.FIELD_UPDATED:
				var storyId:Number = event.message.headers["storyId"];
				var field:String = event.message.headers["field"];
				var value:Object = event.message.headers["value"];

				//this message isn't for this user.
				localStory = getStoryById(storyId);
				if (localStory) {
					localStory[(field.toLowerCase() != "image_editor" ? field.toLowerCase() : "imageEditor")] = value;
					if (field.toLowerCase() == 'displayed')
						localStory.displaying = false;
				}
				break;
			case EntityAction.MOVED:
				if (multiple) {
					// Deve remover todas as laudas primeiro para depois inserir. Isso evita que sejam inseridas em posições erradas.

					for each (var currRemove:Story in stories) {
						rundown.removeStory(currRemove);
					}
					for each (var currAdd:Story in stories) {
						rundown.addStory(currAdd);
					}

					for each (var block:Block in rundown.blocks) {
						block.reorder();
						block.updateTimers();
					}
					rundown.updateTimers();
					rundown.reorderGlobalIndexes();

				} else {
					localStory = getStoryById(story.id);
					if (localStory) {
						var blockId:Number = story.block.id;
						var newIndex:int = story.order;

						moveStory(blockId, newIndex, localStory);
					}
				}

				deselectOthers(null);
				break;
			case EntityAction.UPDATED:
				localStory = getStoryById(story.id);
				if (localStory) {
					localStory.updateFields(story);
				}
				break;
			case EntityAction.UNLOCKED:
				localStory = getStoryById(story.id);
				if (localStory) {
					localStory.editingUser = null;
				}
				break;
			case EntityAction.LOCKED:
				localStory = getStoryById(story.id);
				if (localStory) {
					localStory.editingUser = event.message.headers["user"] as User;
				}
				break;
//			case RundownAction.DRAG_UNLOCKED:  {
//				if (story != null) {
//					localStory = getStoryById(story.id);
//					if (localStory) {
//						localStory.isDragging = false;
//					}
//				}
//				break;
//			}
//			case RundownAction.DRAG_LOCKED:  {
//				if (story != null) {
//					localStory = getStoryById(story.id);
//					if (localStory) {
//						localStory.isDragging = true;
//					}
//				}
//				break;
//			}
			case EntityAction.RESTORED:
				var aux:Block = getBlockById(story.block.id);
				if (aux) {
					story.block = aux;
					addStory(story);
				}
				break;
			case EntityAction.MULTIPLE_EXCLUSION:
				var affectedStories:Array = event.message.body as Array;
				rundownId = event.message.headers["rundownId"];
				if (rundownId == rundown.id) {
					rundown.removeStories(affectedStories);
				}
				break;
			default:
				throw new Error("You receive a undefined message for story, try to reload you rundown!");
		}
	}
}

private function onDocumentMessageReceived(event:MessageEvent):void {
    var rundownId:Number = event.message.headers["rundownId"];
    if(rundownId) {
        var action:String = event.message.headers["action"];
        var affectedStories:Array = event.message.body as Array;

        switch (action) {
            case DocumentAction.SENT_TO_DRAWER: {

                if(rundownId == rundown.id){
                    rundown.removeStories(affectedStories);
                }
                break;
            }
        }
    }
}

private function onIntelligentInterfaceMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];

	switch (action) {
		case IntelligentInterfaceResult.ERROR: {
			var intelligentInterfaceError:String = event.message.body as String;
			var currentUser:User = event.message.headers['currentUser'] as User;
			if (ANews.USER.equals(currentUser)) {
				ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "cg.msg.intelligentInterfaceError") + "\n\r" + intelligentInterfaceError, Info.ERROR);
			}
			break;
		}
	}
}

protected function onKeyDown(event:KeyboardEvent):void {
	if ((event.target is List || event.target is LabelWithFocus) && (event.ctrlKey || event.shiftKey)) {
		if (event.keyCode == Keyboard.X) {
			cutStories();
		} else if (event.keyCode == Keyboard.V) {
			pastStories();
		} else if (event.keyCode == Keyboard.C) {
			copyStories();
		}
	} else if (event.keyCode == Keyboard.ESCAPE) {
		clearClipboard();
	}
}

// -----------------------
// Helpers
// -----------------------

private function getBlockById(blockId:int):Block {
	if (rundown) {
		for each (var block:Block in rundown.blocks) {
			if (block.id == blockId) {
				return block;
			}
		}
	}
	return null;
}

private function getStoryById(storyId:int):Story {
	if (rundown == null)
		return null;

	for each (var block:Block in rundown.blocks) {
		for each (var story:Story in block.stories) {
			if (story.id == storyId) {
				return story;
			}
		}
	}
	return null;
}

//------------------------------
//
//------------------------------

public function deselectAll():void {
	for each (var renderer:BlockRenderer in blockItemRenderers()) {
		renderer.deselect();
	}
}

public function deselectOthers(current:BlockRenderer):void {
	for each (var renderer:BlockRenderer in blockItemRenderers()) {
		if (renderer != current) {
			renderer.deselect();
		}
	}
}

private function blockItemRenderers():Vector.<BlockRenderer> {
	var result:Vector.<BlockRenderer> = new Vector.<BlockRenderer>();

	var dataGroup:DataGroup = blockRendererList.dataGroup;
	for (var index:int = 0; index < dataGroup.numElements; index++) {
		var renderer:BlockRenderer = dataGroup.getElementAt(index) as BlockRenderer;
		if (renderer) {
			result.push(renderer);
		}
	}
	return result;
}

public function addDragSourceHandler(dragSource:DragSource):void {
	dragSource.addHandler(copySelectedItemsForDragDrop, "itemsByIndex");
}

private function copySelectedItemsForDragDrop():Vector.<Object> {
	var length:int = 0;

	for each (var renderer:BlockRenderer in blockItemRenderers()) {
		length += renderer.selectedIndices.length;
	}

	var result:Vector.<Object> = new Vector.<Object>(length);
	var filled:int = 0;

	for each (renderer in blockItemRenderers()) {
		// Cria uma cópia para não modificar o original
		var draggedIndices:Vector.<int> = renderer.selectedIndices.slice(0, renderer.selectedIndices.length);
		draggedIndices.sort(compareValues);

		var count:int = draggedIndices.length;
		for (var i:int = 0; i < count; i++) {
			result[filled + i] = renderer.getItemAt(draggedIndices[i]);
		}
		filled += count;
	}

	return result;
}

private function compareValues(a:int, b:int):int {
	return a - b;
}

public function selectedStories():Array {
	var stories:Array = new Array();

	for each (var renderer:BlockRenderer in blockItemRenderers()) {
        var selectedItems:Vector.<Object> = renderer.selectedItems;
		if (selectedItems) {
			for each (var story:Story in selectedItems) {
				stories.push(story);
			}
		}
	}

	return stories;
}

public function selectedStoriesIds():Array {
	var stories:Array = new Array();

	for each (var renderer:BlockRenderer in blockItemRenderers()) {
		var selectedItems:Vector.<Object> = renderer.selectedItems;
		if (selectedItems) {
			for each (var story:Story in selectedItems) {
				stories.push(story.id);
			}
		}
	}

	return stories;
}

private function onSelectedStory(event:CrudEvent):void {
    event.stopPropagation();
    var currentStory:Story = event.entity as Story;
    var indexNextBlock:int = blockRendererList.dataProvider.getItemIndex(currentStory.block);
    indexNextBlock += currentStory.globalIndex < currentStory.globalIndex ? -1 : +1;
    var nextBlockRenderer:BlockRenderer =  blockRendererList.dataGroup.getElementAt(indexNextBlock) as BlockRenderer;
    var currentBlockRenderer:BlockRenderer = blockRendererList.dataGroup.getElementAt(blockRendererList.dataProvider.getItemIndex(currentStory.block)) as BlockRenderer;
    if(currentStory.nextGlobalIndex != -1 && currentStory.nextGlobalIndex <= currentStory.block.rundown.maxGlobalIndex) {
        var nextStory: Story = currentStory.block.rundown.getStoryByGlobalIndex(currentStory.nextGlobalIndex);
        if(nextStory) {
//            var nextBlockRenderer:BlockRenderer = blockRendererList.dataGroup.getElementAt(blockRendererList.dataProvider.getItemIndex(nextStory.block)) as BlockRenderer;
            if (!(currentStory.block.equals(nextStory.block))) {
                currentBlockRenderer.storiesList.selectedIndex = -1;
                currentBlockRenderer.selected = false;
                dispatchEvent(new CrudEvent(CrudEvent.SELECT, null));
                if(!(nextStory.block.standBy || currentStory.block.standBy) && event.target is StoryRenderer2) {
                    if(currentStory.nextGlobalIndex >= currentStory.globalIndex) {
                        nextBlockRenderer.commercial.editor.focusManager.setFocus(nextBlockRenderer.commercial.editor);
//                        nextBlockRenderer.commercial.editor.focusManager.showFocus();
                        nextBlockRenderer.selected = false;
                    } else {
                        currentBlockRenderer.commercial.editor.focusManager.setFocus(currentBlockRenderer.commercial.editor);
//                        currentBlockRenderer.commercial.editor.focusManager.showFocus();
                        currentBlockRenderer.selected = false;
                    }
                    return;
                }
            }
            if (nextBlockRenderer.block.equals(nextStory.block)) {
                nextBlockRenderer.storiesList.selectedIndex = nextBlockRenderer.storiesList.dataProvider.getItemIndex(nextStory);
                nextBlockRenderer.selected = true;
                var storyRenderer2:StoryRenderer2 = nextBlockRenderer.storiesList.dataGroup.getElementAt(nextBlockRenderer.storiesList.selectedIndex) as StoryRenderer2;
                storyRenderer2.selected = true;
                dispatchEvent(new CrudEvent(CrudEvent.SELECT, nextStory));
                if(!nextStory.displayed && !nextStory.displaying && !nextStory.block.rundown.displayed && currentStory.nextGlobalIndex >= currentStory.globalIndex) {
                    storyRenderer2.setFocusInFirstElementOfItemRenderer();
                } else {
                    storyRenderer2.setFocusInLastElementOfItemRenderer();
                }
            }
        } else {
            dispatchEvent(new CrudEvent(CrudEvent.SELECT, null));
        }
    } else {
        dispatchEvent(new CrudEvent(CrudEvent.SELECT, null));
        currentBlockRenderer.storiesList.selectedIndex = -1;
        currentBlockRenderer.selected = false;
        return;
    }
}
