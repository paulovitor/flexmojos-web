import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import mx.controls.Alert;
import mx.core.DragSource;
import mx.core.IFlexDisplayObject;
import mx.core.UIComponent;
import mx.core.mx_internal;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.managers.DragManager;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.components.DataGroup;
import spark.components.Label;
import spark.components.TextInput;
import spark.events.IndexChangeEvent;
import spark.layouts.supportClasses.DropLocation;

import tv.snews.anews.component.Info;
import tv.snews.anews.component.LabelWithFocus;
import tv.snews.anews.component.TimeEditable;
import tv.snews.anews.domain.Block;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.News;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.BlockService;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.view.rundown.RundownList;
import tv.snews.anews.view.rundown.StoryRenderer2;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

use namespace mx_internal;

private const dateUtil:DateUtil = new DateUtil();
private const bundle:IResourceManager = ResourceManager.getInstance();
private const reportUtil:ReportsUtil = new ReportsUtil();

[Bindable] public var block:Block;
private var blockService:BlockService = BlockService.getInstance();
private var storyService:StoryService = StoryService.getInstance();
private var oldValue:String;
private var rundownList:RundownList;

// --------------------
// Overrides
// --------------------
override public function set data(value:Object):void {
	super.data = value;
	if (value) {
		if (value is Block) {
			block = value as Block;
		} else {
			throw new Error("You're doing it wrong!");
		}
	}
}

// ------------------
// Actions
// ------------------

private function onCreationComplete(event:FlexEvent):void {
	var parent:Object = UIComponent(owner).parentDocument;

	while (!(parent is RundownList) && parent is UIComponent) {
		parent = UIComponent(parent).parentDocument;
		}

	rundownList = RundownList(parent);
    configureSpecificTimeEditableEvents(commercial);
}

private function onAddStory(event:Event):void {
	btnAddStory.enabled = false;
	storyService.createStory(block.id, -1/*last position*/, function(event:ResultEvent):void {
		btnAddStory.enabled = true;
	});
}

private function onPrint(event:Event):void {
	var paramsStory:Object = new Object();
	var laudas:String = "";

	for each (var story:Story in block.stories) {
		laudas = laudas + story.id + "|";
	}

	paramsStory.ids = laudas;
	reportUtil.generate(ReportsUtil.STORY, paramsStory);
}

public function deleteBlock():void {
	if (validBlockToDelete()) {
		Alert.show(bundle.getString('Bundle', 'rundown.block.msg.confirmDelete'), bundle.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onDeleteConfirm);
	}
}

public function onDeleteConfirm(event:CloseEvent):void {
    btnDeleteBlock.enabled = false;
	if (event.detail == Alert.YES) {
		blockService.removeBlock(block.id, onDeleteSuccess);
	}
}

private function focusFieldIn(event:FocusEvent):void {
	oldValue = TimeEditable(event.currentTarget).editor.text;
}

private function dateOut(event:FocusEvent):void {
	var input:TextInput = TimeEditable(event.currentTarget).editor;
	var format:String = input.text.length == 5 ? DateUtil.MINUTE_SECOND : input.text.length == 8 ? DateUtil.TIME : null;

	if (format) {
		var date:Date = dateUtil.toDate(input.text, format);
		input.text = date ? dateUtil.toString(date, format) : oldValue as String;
	} else {
		input.text = oldValue as String;
	}

	if (input.text != oldValue) {
		blockService.updateBlockTime(block.id, date, null, function(event:FaultEvent):void {
            input.text = oldValue;
            blockService.faultHandler(event);
        });
	}
}

// ------------------
// Events
// ------------------

protected function onClickPast(event:MouseEvent):void {
	rundownList.pastStories(block);
}

/**
 * Executado nos eventos "click" e "change" da lista de laudas. Faz o controle
 * da lauda selecionada.
 *
 * É necessário executar no "click" também para tratar o caso de troca de
 * seleção entre blocos.
 */
protected function changeStory(event:IndexChangeEvent):void {
    event.stopPropagation();
//    var story:Story = storiesList.selectedItem as Story;
//    story.nextGlobalIndex =  story.globalIndex;
//    this.dispatchEvent(new CrudEvent(CrudEvent.SELECT_CHANGE, story));
    // Ao desselecionar todos os itens selecionados, o indice enviado para esse metodo é -1
    if(event.newIndex != -1){
        var story:Story = storiesList.selectedItem as Story;
        var child:StoryRenderer2 = storiesList.dataGroup.getElementAt(event.newIndex) as StoryRenderer2;

        if(!story.displayed && !story.displaying && !story.block.rundown.displayed){
            child.setFocusInFirstElementOfItemRenderer();
        }
        if (story) {
            dispatchEvent(new CrudEvent(CrudEvent.SELECT, story));
        }
    } else {
        dispatchEvent(new CrudEvent(CrudEvent.SELECT, null));
    }

}

private function onMoveFromDrawerSuccess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString("Bundle", "rundown.trash.restoreSuccess"), Info.INFO);
}

private function onDeleteSuccess(event:ResultEvent):void {
	ANews.showInfo(bundle.getString('Bundle', 'rundown.block.msg.deleteSucess'), Info.INFO);
    btnDeleteBlock.enabled = true;
}

private function onStoryMouseDown(event:MouseEvent):void {
	if (!event.ctrlKey && !event.shiftKey) {
		rundownList.deselectOthers(this);
	}
}

protected function onDoubleClick(event:MouseEvent):void {
	var story:Story = storiesList.selectedItem as Story;
	if (ANews.USER.allow('01071103') && (story.approved ? ANews.USER.allow('01071110') : true)) {
		if (story) {
			var evt:CrudEvent = new CrudEvent(CrudEvent.EDIT);
			evt.entity = story;
			this.dispatchEvent(evt);
		}
	}
}

// ------------------
// Helpers
// ------------------
private function formatTimeAsString(time:Date):String {
	return dateUtil.toString(time, bundle.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}

private function validBlockToDelete():Boolean {
	for each (var story:Story in block.stories) {
		if (story.editingUser) {
			ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownEdition"), Info.WARNING);
			return false;
		}
		if (story.isDragging) {
			ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownDragging"), Info.WARNING);
			return false;
		}
        if (story.displayed) {
            ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownDisplayed"), Info.WARNING);
            return false;
        }
        if (story.displaying) {
            ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownDisplaying"), Info.WARNING);
            return false;
        }
	}
	return true;
}

//------------------------------
//
//------------------------------

public function deselect():void {
	storiesList.setSelectedIndices(new Vector.<int>(), false);
	storiesList.validateProperties();
}

public function get selectedIndices():Vector.<int> {
	return storiesList.selectedIndices;
}

public function get selectedItems():Vector.<Object> {
	return storiesList.selectedItems;
}

public function getItemAt(index:int):Object {
	return block.stories.getItemAt(index);
}

private function calculateDropLocation(event:DragEvent):DropLocation {
	if (!storiesList.enabled || !event.dragSource.hasFormat("itemsByIndex")) {
		return null;
	}

	return storiesList.layout.calculateDropLocation(event);
}

private function hideVisualIndication():void {
	storiesList.layout.hideDropIndicator();
	storiesList.destroyDropIndicator();

	storiesList.drawFocus(false);
	storiesList.drawFocusAnyway = false;
}

private function scrollToCaretIndex(caretIndex:int, dropIndex:int):void {
	var loopCount:int = 0;

	while (loopCount++ < 10) {
		validateNow();

		var delta:* = storiesList.layout.getScrollPositionDeltaToElement(dropIndex + caretIndex);
		if (!delta || (delta.x == 0 && delta.y == 0)) {
			break;
		}
		layout.horizontalScrollPosition += delta.x;
		layout.verticalScrollPosition += delta.y;
	}
}

private function onDragStart(event:DragEvent):void {
	event.preventDefault();

	var dragSource:DragSource = new DragSource();
	storiesList.addDragData(dragSource);
	rundownList.addDragSourceHandler(dragSource);

	var dragIndicator:IFlexDisplayObject = new Label();

	DragManager.doDrag(storiesList, dragSource, event, dragIndicator, 0, 0, 0.5, true);

//	var items:Vector.<Object> = dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
//	for each (var item:Object in items) {
//		var story:Story = item as Story;
//		if (story) {
//			storyService.lockDragging(story.id, ANews.USER.id);
//		}
//	}
}

private function onDragEnter(event:DragEvent):void {
	event.preventDefault();

	var dropLocation:DropLocation = calculateDropLocation(event);
	if (dropLocation) {
		DragManager.acceptDragDrop(storiesList);

		storiesList.createDropIndicator();

		storiesList.drawFocusAnyway = true;
		storiesList.drawFocus(true);

		DragManager.showFeedback(DragManager.COPY);

		storiesList.layout.showDropIndicator(dropLocation);
	} else {
		DragManager.showFeedback(DragManager.NONE);
	}
}

protected function onDragOver(event:DragEvent):void {
	event.preventDefault();

	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
	var dragItem:Object = items && items.length != 0 ? items[0] : null;
	var valid:Boolean = !block.rundown.displayed;

	var dropLocation:DropLocation = calculateDropLocation(event);

	if (dropLocation && valid) {
		storiesList.drawFocusAnyway = true;
		storiesList.drawFocus(true);

		DragManager.showFeedback(DragManager.COPY);

		storiesList.layout.showDropIndicator(dropLocation);
	} else {
		storiesList.layout.hideDropIndicator();

		storiesList.drawFocus(false);
		storiesList.drawFocusAnyway = false;

		DragManager.showFeedback(DragManager.NONE);
	}
}

protected function onDragDrop(event:DragEvent):void {
	event.preventDefault();

	var dropLocation:DropLocation = calculateDropLocation(event);
	if (!dropLocation) {
		return;
	}
	var dropIndex:int = dropLocation.dropIndex;
	var dropIndexBackup:int = dropIndex;

	var dragSource:DragSource = event.dragSource;
	var items:Vector.<Object> = dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;

	
	hideVisualIndication();

	DragManager.showFeedback(DragManager.MOVE);

	var caretIndex:int = -1;
	if (dragSource.hasFormat("caretIndex")) {
		caretIndex = event.dragSource.dataForFormat("caretIndex") as int;
	}
	
	var dragItem:Object = items[0];
	
	

	// Se quem iniciou o drag não for uma lista, significa que veio da gaveta
	if (event.dragInitiator is List && !(dragItem && dragItem is News)) {

		var storiesIds:Array = [];

		//--------------------------------------------------------
		//	Lógica customizada do dragDropHandler padrão do List
		//--------------------------------------------------------

//		dropIndex = removeSelectedItems(dropIndex);
//		scriptArea.removeOthersSelectedItems(this);

		for (var i:int = 0; i < items.length; i++) {
			var item:Object = items[i];
//			block.scripts.addItemAt(item, dropIndex + i);

            if ((item as Story).displaying)
                ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'story.isDisplayingWithSlug', [ StringUtils.isNullOrEmpty((item as Story).slug) ? ResourceManager.getInstance().getString('Bundle', 'defaults.noSlug') : (item as Story).slug ]), Info.WARNING);
            else if ((item as Story).displayed) ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'rundown.block.displayedWithSlug', [ StringUtils.isNullOrEmpty((item as Story).slug) ? ResourceManager.getInstance().getString('Bundle', 'defaults.noSlug') : (item as Story).slug ]), Info.WARNING);
            else
                storiesIds.push(item.id);
		}

		//--------------------------------------------------------
		//	Lógica para a persistência das mundanças no servidor
		//--------------------------------------------------------

		// OLD: storyService.moveStory(block.id, index, story.id);
        if (storiesIds.length > 0)
		    this.callLater(storyService.moveStories, [ storiesIds, block.id, dropIndexBackup ]);

	} else {
		if (items.length == 1) {
			dragItem = items[0];

			// Restaurando um script da gaveta
			if (dragItem is Story) {
				var story:Story = Story(dragItem);
				storyService.copyDrawerStoryToBlock(story.id, block.id, dropIndexBackup, onMoveFromDrawerSuccess);
			}

			// Criando script à partir de uma pauta
			if (dragItem is Guideline) {
				var guideline:Guideline = Guideline(dragItem);
				storyService.createStoryFromGuideline(block.id, dropIndexBackup, guideline.id);
			}

			// Criando script à partir de uma reportagem
			if (dragItem is Reportage) {
				var reportage:Reportage = Reportage(dragItem);
				storyService.createStoryFromReportage(block.id, dropIndexBackup, reportage.id);
			}
			
			// Criando script à partir de uma reportagem
			if (dragItem is News) {
				var news:News = News(dragItem);
				storyService.createStoryFromNews(block.id, dropIndexBackup, news.id);
			}
		}
	}

	if (caretIndex != -1) {
		scrollToCaretIndex(caretIndex, dropIndex);
	}
}

private function onDragComplete(event:DragEvent):void {
	event.preventDefault();

//	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
//	for each (var item:Object in items) {
//		var story:Story = item as Story;
//		if (story) {
//			storyService.unlockDragging(story.id);
//		}
//	}

	storiesList.dragEnabled = false;
}

public function storyItemRenderers():Vector.<StoryRenderer2> {
    var result:Vector.<StoryRenderer2> = new Vector.<StoryRenderer2>();

    var dataGroup:DataGroup = storiesList.dataGroup;
    for (var index:int = 0; index < dataGroup.numElements; index++) {
        var renderer:StoryRenderer2 = dataGroup.getElementAt(index) as StoryRenderer2;
        if (renderer) {
            result.push(renderer);
        }
    }
    return result;
}

private function configureSpecificTimeEditableEvents(timeEditable:UIComponent):void {
    timeEditable.addEventListener(KeyboardEvent.KEY_DOWN, function (event:KeyboardEvent):void {
        if (event.keyCode == Keyboard.TAB && timeEditable.id == "commercial" || event.keyCode == Keyboard.ENTER && timeEditable.id == "commercial") {
            if(event.shiftKey) {
                nextSelectStory(true);
            } else {
                nextSelectStory();
            }
        }
    });
}

private function nextSelectStory(carriageReturn:Boolean = false):void {
    var story:Story = new Story();
    story.globalIndex = 0;
    story.block = block;
    if(block.stories.length > 0) {
        story = storiesList.dataProvider.getItemAt(0) as Story;
        story.nextGlobalIndex = story.globalIndex;
    }
    story.nextGlobalIndex +=  carriageReturn ? -1 : 0;
    this.dispatchEvent(new CrudEvent(CrudEvent.SELECT_CHANGE, story));
}

public function getStoryRenderer(index:int):ItemRenderer {
	return storiesList.dataGroup.getElementAt(index) as ItemRenderer;
}
