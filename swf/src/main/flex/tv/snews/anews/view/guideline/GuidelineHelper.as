package tv.snews.anews.view.guideline {

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.User;
import tv.snews.anews.utils.TabManager;

public class GuidelineHelper {

    private const bundle:IResourceManager = ResourceManager.getInstance();

    public function guidelineInEditing(user:User, tabManager:TabManager, key:String, area:String):void {
        if (user.equals(ANews.USER)) {
            if (tabManager.isTabOpened(key))
                tabManager.openTab(GuidelineForm, key);
            else
                ANews.showInfo(bundle.getString("Bundle", "guideline.msg.areaLockedMessage", [ bundle.getString("Bundle", "defaults.you"), area ]), Info.WARNING);
        } else {
            ANews.showInfo(bundle.getString("Bundle", "guideline.msg.lockedMessage", [ user.nickname ]), Info.WARNING);
        }
    }

}
}
