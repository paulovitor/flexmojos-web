package tv.snews.anews.view.program {

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import spark.components.ComboBox;
import spark.components.DataGrid;
import spark.components.TextInput;

import tv.snews.anews.component.Info;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.DateUtil;

public class ProgramHelper {

    private const _bundle:IResourceManager = ResourceManager.getInstance();
    private const _dateUtil:DateUtil = Util.getDateUtil();

    private var _view:ProgramForm;

    public function ProgramHelper(view:ProgramForm) {
        _view = view;
    }

    public function validateRequiredFields():Boolean {
        var fieldsOk:Boolean = Util.isValid(view.validators);
        var presentersOk:Boolean = validate(view.presenterList, view.acPresenters);
        var editorsOk:Boolean = validate(view.editorList, view.acEditors);
        var imageEditorsOk:Boolean = validate(view.imageEditorList, view.acImageEditors);
        var defaultPresenterOk:Boolean = presentersOk ? validateDefaultPresenter() : false;
        if (!fieldsOk || !presentersOk || !editorsOk || !imageEditorsOk || !defaultPresenterOk) {
            view.txtProgramName.setFocus();
            var message:String = presentersOk && editorsOk && imageEditorsOk && !defaultPresenterOk ? view.presenterList.errorString : _bundle.getString('Bundle', "defaults.msg.requiredFields");
            ANews.showInfo(message, Info.WARNING);
            return false;
        }
        return true;
    }

    private function validateDefaultPresenter():Boolean {
        if (view.presenterList.dataProvider.length > 0 && view.defaultPresenterRadioButtonGroup.selectedValue == null) {
            view.presenterList.errorString = _bundle.getString('Bundle', 'program.msg.defaultPresenterUnselected');
            return false;
        }
        view.presenterList.errorString = "";
        return true;
    }

    private function validate(dataGrid:DataGrid, comboBox:ComboBox):Boolean {
        if (dataGrid.dataProvider.length == 0) {
            comboBox.errorString = _bundle.getString('Bundle', 'defaults.msg.requiredField');
            return false;
        }
        comboBox.errorString = "";
        return true;
    }

    public function applyMaskTime(txtSchedule:TextInput):void {
        if (txtSchedule.text.length > 2 && txtSchedule.text.length < 5 && txtSchedule.text.indexOf(':') == -1) {
            txtSchedule.text = txtSchedule.text.substr(0, 2) + ":" + txtSchedule.text.substr(2);
        } else {
            if (txtSchedule.text.length > 5 && txtSchedule.text.length < 7 && txtSchedule.text.substr(4).indexOf(':') == -1) {
                txtSchedule.text = txtSchedule.text.substr(0, 5) + ":" + txtSchedule.text.substr(5);
            }
        }
    }

    public function formatHour(date:Date):String {
        return _dateUtil.timeToString(date);
    }

    public function resetErrors():void {
        view.playoutDevicesCombo.textInput.text = "";
        view.playoutDevicesCombo.errorString = "";
        view.pathsCombo.errorString = "";
        view.cgDevicesCombo.errorString = "";
        view.txtProgramName.errorString = "";
        view.txtScheduleStart.errorString = "";
        view.txtScheduleEnd.errorString = "";
        view.acPresenters.errorString = "";
        view.presenterList.errorString = "";
        view.acEditors.errorString = "";
        view.editorList.errorString = "";
        view.acImageEditors.errorString = "";
        view.imageEditorList.errorString = "";
    }

    public function get view():ProgramForm {
        return _view;
    }

    public function set view(value:ProgramForm):void {
        _view = value;
    }
}
}
