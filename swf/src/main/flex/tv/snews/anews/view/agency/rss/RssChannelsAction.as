
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.MouseEvent;
import flash.utils.setTimeout;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.RssCategory;
import tv.snews.anews.domain.RssFeed;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.RssCategoryService;
import tv.snews.anews.service.RssService;
import tv.snews.anews.utils.*;
import tv.snews.flexlib.utils.StringUtils;

private static const HTTP_PREFIX:String = "http://";

private static const STATES:Object = {
	NORMAL	: "normal",
	EDIT	: "edit"
};

private var feed:RssFeed;

[Bindable]
private var category:RssCategory = new RssCategory();
private var categoryDel:RssCategory = new RssCategory();

[Bindable]
[ArrayElementType("tv.snews.anews.domain.RssCategory")]
private var categories:ArrayCollection = new ArrayCollection();

private const categoryService:RssCategoryService = RssCategoryService.getInstance();
private const rssService:RssService = RssService.getInstance();

//------------------------------------------------------------------------------
//	Event Handlers
//------------------------------------------------------------------------------

private function contentCreationCompleteHandler(event:FlexEvent):void {
	configureEventListeners();
	loadCategoriesList();
	
	rssService.subscribe(onMessageReceivedRssHandler);
}

private function onMessageReceivedRssHandler(event:MessageEvent):void {
	var feed:RssFeed = event.message.body as RssFeed;
	var type:String = event.message.headers["type"] as String;
	
	if (type != RssService.FEED_UPDATED) {
		loadCategoriesList(500);
	}
}

private function onRemove(event:Event):void {
	rssService.unsubscribe(onMessageReceivedRssHandler);
}

/**
 * @private
 * Handler para o evento disparado pela lista de categorias quando uma delas 
 * será editada.
 */
private function onCategoryEdit(event:CrudEvent):void {
	categoriesList.enabled = false;
	rssItem.enabled = false;
	category = event.entity as RssCategory;
	putOnEditState();
}

private function onDeleteRss(event:CloseEvent):void{
	if (event.detail == Alert.YES) {
		feed.category.feeds.removeAll();
		rssService.deleteRssFeed(feed, 
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.deleteSuccess"), Info.INFO);
			}
		);
	}
}

private function onDeleteRssCategory(event:CloseEvent):void{
	if (event.detail == Alert.YES) {
		
		if (categoryDel.feeds.length == 0) {
			setButtonsEnabled(false);
			
			categoryService.remove(categoryDel, 
				function(event:ResultEvent):void {
					ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.deleteSuccess"), Info.INFO);
					putOnNormalState();
					
					setButtonsEnabled(true);
				},
				defaultFailHandler
			);
		} else {
			ANews.showInfo(this.resourceManager.getString("Bundle" , "rss.categoryDeleteNotAllowed"), Info.WARNING);
		}
	}	
}
private function onFeedDelete(event:CrudEvent):void {
	
	//Deletando um Feed
	if (event.entity is RssFeed) {
		feed = event.entity as RssFeed;
		
		Alert.show(this.resourceManager.getString("Bundle" , "defaults.msg.doYouWantDeleteItem"), this.resourceManager.getString("Bundle" , "defaults.msg.confirmDelete"), Alert.YES | Alert.NO, null, onDeleteRss, null, Alert.YES);
	}
	else{ //Deletando uma categoria
		categoryDel = event.entity as RssCategory
		Alert.show(this.resourceManager.getString("Bundle" , "defaults.msg.doYouWantDeleteItem"), this.resourceManager.getString("Bundle" , "defaults.msg.confirmDelete"), Alert.YES | Alert.NO, null, onDeleteRssCategory, null, Alert.YES);

	}
}

//--------------------------------------
//	Rss Feed Events
//--------------------------------------

private function onUrlFocusOut(event:FocusEvent):void {
	var aux:String = rssUrlInput.text.replace(/(http|feed):\/\//g, "");
	rssUrlInput.text = HTTP_PREFIX + aux;
}

private function onSaveRss(event:MouseEvent):void {
	feed = new RssFeed();
	feed.name = StringUtils.trim(rssNameInput.text);
	feed.url = StringUtils.trim(rssUrlInput.text);
	feed.category = rssCategoryCombo.selectedItem as RssCategory;
	
	if (Util.isValid(feedValidators)) {
		setButtonsEnabled(false);
		
		rssService.saveRssFeed(feed,
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.saveSuccess"), Info.INFO);
				clearRssField();
				setButtonsEnabled(true);
			}, 
			defaultFailHandler
		);
	} else {
		ANews.showInfo(this.resourceManager.getString("Bundle" , "defaults.msg.saveFail"), Info.WARNING);
	}
}

//--------------------------------------
//	Category Events
//--------------------------------------

private function onCategorySave(event:MouseEvent):void {
	fillCategory();
	
	if (Util.isValid(categoryValidators)) {
		setButtonsEnabled(false);
		
		categoryService.insert(category, 
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.saveSuccess"), Info.INFO);
				putOnNormalState();
				
				setButtonsEnabled(true);
				categoriesList.enabled = true;
				rssItem.enabled = true;
			},
			defaultFailHandler
		);
	} else {
		ANews.showInfo(this.resourceManager.getString("Bundle" , "defaults.msg.saveFail"), Info.WARNING);
	}
}

private function onCategoryUpdate(event:MouseEvent):void {
	fillCategory();
	
	if (Util.isValid(categoryValidators)) {
		setButtonsEnabled(false);
		
		
		categoryService.update(category, 
			function(event:ResultEvent):void {
				ANews.showInfo(ResourceManager.getInstance().getString("Bundle" , "defaults.msg.saveSuccess"), Info.INFO);
				putOnNormalState();
				
				setButtonsEnabled(true);
				categoriesList.enabled = true;
				rssItem.enabled = true;
			},
			defaultFailHandler
		);
	} else {
		ANews.showInfo(this.resourceManager.getString("Bundle" , "rss.msg.categoryUpdateError"), Info.WARNING);
	}
}

private function onCategoryCancel(event:MouseEvent):void {
	categoriesList.enabled = true;
	rssItem.enabled = true;
	putOnNormalState();
}

private function defaultFailHandler(event:FaultEvent):void {
	setButtonsEnabled(true);
}

//------------------------------------------------------------------------------
//	Helper Methods
//------------------------------------------------------------------------------

private function fillCategory():void {
	category.name = catNameInput.text;
	category.color = catColorPicker.selectedColor;
}

private function loadCategoriesList(delay:int = 0):void {
	if (delay > 0) {
		flash.utils.setTimeout(loadCategoriesList, delay);
	}
	
	categoryService.listAll(function(event:ResultEvent):void {
		categories = event.result as ArrayCollection;
	});
}

private function configureEventListeners():void {
	categoriesList.addEventListener(CrudEvent.EDIT, onCategoryEdit);
	categoriesList.addEventListener(CrudEvent.DELETE, onFeedDelete);
}

private function putOnNormalState():void {
	this.currentState = STATES.NORMAL;
	category = new RssCategory();
}

private function putOnEditState():void {
	this.currentState = STATES.EDIT;
	setButtonsEnabled(true);
}

private function clearRssField():void {
	feed = null;
	rssNameInput.text = "";
	rssUrlInput.text = "";
	rssCategoryCombo.selectedIndex = -1;
}

private function setButtonsEnabled(enabled:Boolean):void {
	if (this.currentState == STATES.NORMAL) {
		saveCatButton.enabled = enabled;
	} else {
		cancelCatButton.enabled = enabled;
		updateCatButton.enabled = enabled;
	}
}
