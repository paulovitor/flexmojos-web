import mx.rpc.events.ResultEvent;

import tv.snews.anews.domain.Story;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.DateUtil;

private var dateUtil:DateUtil = Util.getDateUtil();
private var _story:Story;
[Bindable] public var storyFontSize:int;

[Bindable] 
public function set story(value:Story):void {
	_story = value;
	
	if (_story) {
		//StoryService.getInstance().loadById(_story.id, onLoadStory);
	}
}

public function get story():Story {
	return _story;
}

private function onLoadStory(event:ResultEvent):void {
	var fullStory:Story = Story(event.result);
	
	if (story) {
		story.headSection = fullStory.headSection;
		//story.credits = fullStory.credits;
	}
}