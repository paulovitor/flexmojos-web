package tv.snews.anews.view.navigator {

	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	
	import mx.controls.Alert;
	
	import spark.components.TextInput;
	import spark.components.supportClasses.SkinnableComponent;
	
	import tv.snews.anews.view.AdministrationArea;


	public class TextInputSearch extends SkinnableComponent {
		[SkinPart]
		public var textDisplay:TextInput;

		public var dropdown:SearchDropDown;

		/** Instância da área onde o usuário está interagindo no sistema **/
		private var _currentArea:Object;

		public function TextInputSearch() {
			super();
			setStyle("skinClass", TextInputSearchSkin)
		}

		override protected function getCurrentSkinState():String {
			return super.getCurrentSkinState();
		}

		override protected function partAdded(partName:String, instance:Object):void {
			super.partAdded(partName, instance);
		}

		override protected function partRemoved(partName:String, instance:Object):void {
			super.partRemoved(partName, instance);
		}

		//EVENTS
		public function rollOver(event:MouseEvent):void {

			//Abre a dropdown

		/*			dropdown.closeDropDown(false);
					dropdown.stayOpen = true;
					dropdown.selectedIndex = 0;
					dropdown.addEventListener(ListEvent.CHANGE, itemSelectHandler);
					dropdown.x = globalToLocal(parent.localToGlobal(new Point(this.x))).x
					callLater(dropdown.openDropDown);*/
		}

		public function keyDown(event:KeyboardEvent):void {
			if (event.keyCode == Keyboard.ENTER) {
				dispatchSearchEvent(event);
				textDisplay.text = "";
			}
		}

		private function dispatchSearchEvent(event:Event):void {
			if (currentArea != null) {

				if (currentArea is AdministrationArea) {
					var administrationArea:AdministrationArea = currentArea as AdministrationArea;

					var menuSelected:Object = administrationArea.menuList.selectedItem;

					if (menuSelected != null) {
						switch (menuSelected.id) {
							case "settingsItem":
								break;
							case "groupsItem":
								break;
							case "usersItem":
								(dropdown.parent as SearchBarSkin).dispatchSearchEvent(textDisplay.text);
								break;
							default:
								Alert.show("BUG. Não existe mapeamento de ID para o menu selecionado");
						}
					} else {
						Alert.show(" BUG. Menu selecionado da área AdministrationArea esta NULL. ");
					}
				}
			}
		}

		private function itemSelectHandler(event:Event):void {

		}

		public function rollOut(event:MouseEvent):void {
			Mouse.cursor = MouseCursor.AUTO;
		}

		public function click(event:MouseEvent):void {
			dispatchSearchEvent(event);
		}

		[Bindable] public function get currentArea():Object {
			return _currentArea;
		}

		/**
		 * @private
		 */
		public function set currentArea(value:Object):void {
			_currentArea = value;
		}

	}
}