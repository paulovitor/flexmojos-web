package tv.snews.anews.view.rundown {

import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Story;
import tv.snews.anews.domain.User;
import tv.snews.anews.service.StoryService;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.view.rundown.story.section.form.StoryForm;

public class StoryHelper {

    private const bundle:IResourceManager = ResourceManager.getInstance();
    private const storyService:StoryService = StoryService.getInstance();

    public function storyInEditing(user:User, tabManager:TabManager, key:String, story:Story, area:String):void {
        if (user.equals(ANews.USER)) {
            if (tabManager.isTabOpened(key))
                tabManager.openTab(StoryForm, key);
            else
                ANews.showInfo(bundle.getString("Bundle", "rundown.story.areaLockedMessage", [ bundle.getString("Bundle", "defaults.you"), area ]), Info.WARNING);
        } else if (ANews.USER.allow("01071116")){
            Alert.show(ResourceManager.getInstance().getString('Bundle', 'story.msg.confirmAssumeEdition', [ user.name ]), ResourceManager.getInstance().getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null,
                function (event:CloseEvent):void {
                    if (event.detail == Alert.YES) {
                        storyService.assumeEdition(story.id, ANews.USER.id, user.id, function (event:ResultEvent):void {
                            storyService.loadById(story.id, function (event:ResultEvent):void {
                                var result:Story = Story(event.result);
                                var params:Object = {story: result, tabManager: tabManager};
                                tabManager.openTab(StoryForm, "story_" + result.id, params);
                            });
                        });
                    }
                }, null, Alert.NO);
        }
        else
            ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessage", [ user.nickname ]), Info.WARNING);
    }
}
}
