import flash.events.Event;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.events.FlexEvent;
import mx.messaging.events.MessageEvent;
import mx.rpc.events.ResultEvent;

import tv.snews.anews.domain.News;
import tv.snews.anews.service.NewsService;
import tv.snews.anews.view.agency.newsviewer.NewsViewerTooltip;

[Bindable]
private var news:ArrayCollection = new ArrayCollection();

[Bindable]
private var tooltipsOpened:ArrayCollection = new ArrayCollection();
private var app:Object = FlexGlobals.topLevelApplication;

[Bindable] public var isDraggin:Boolean = false;

protected function onCreationComplete(event:FlexEvent):void {
	NewsService.getInstance().subscribe(onMessage);
}

public function listFirstTwentyNews():void {
	NewsService.getInstance().listFirstTwenty(onListSuccess);
}

public function destroyOtherOpenedTooltips(currentTooltip:NewsViewerTooltip = null):void {
	for each (var tooltip:NewsViewerTooltip in tooltipsOpened) {
		app.main.removeElementAt(app.main.getElementIndex(tooltip));
	}
}

public function addTooltipOpened(tooltip:NewsViewerTooltip) :void {
	tooltipsOpened.addItem(tooltip);
} 

public function removeTooltipOpened(tooltip:NewsViewerTooltip) :void {
	tooltipsOpened.removeItemAt(tooltipsOpened.getItemIndex(tooltip));
}

private function onRemove(event:Event):void {
	NewsService.getInstance().unsubscribe(onMessage);
}
//----------------------------
// Assynchronous call.
//----------------------------
private function onListSuccess(event:ResultEvent):void {
	news = event.result as ArrayCollection;
}

private function onMessage(event:MessageEvent):void {
	var headers:Object = event.message.headers;
	var entity:News = News(event.message.body);
	var index:int = 0;
	
	switch(headers.type) {
		case "INSERTED": {
            if(news.length == 20)
                news.removeItemAt(news.length - 1);
			news.addItemAt(entity,0);
			break;
		}
		case "DELETED": {
            listFirstTwentyNews();
			break;
		}
		case "UPDATED": {
			index = findNewsIndex(entity);
			if(index < 0 && news.length == 20) {
                news.removeItemAt(news.length - 1);
                news.addItemAt(entity,0);
                break;
            } else {
                var atual:News = news.getItemAt(index) as News;
                atual.address = entity.address;
                atual.contacts = entity.contacts;
                atual.date = entity.date;
                atual.font = entity.font;
                atual.information = entity.information;
                atual.lead = entity.lead;
                atual.slug = entity.slug;
                atual.user = entity.user;
                break;
            }
		}
		default: {
			break;
		}
	}
}
//----------------------------
// Helpers.
//----------------------------
private function findNewsIndex(entity:News):int {
	for each(var _news:News in news) {
		if(_news.id == entity.id){
			return news.getItemIndex(_news);
		}
	}
	return -1;
}