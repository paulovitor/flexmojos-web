import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.events.CloseEvent;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.events.GridSelectionEvent;

import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Permission;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserGroup;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.GroupService;
import tv.snews.anews.service.UserService;
import tv.snews.anews.service.PermissionService;
import tv.snews.anews.utils.*;

[Bindable] public var permissions:Permission;
[Bindable] private var group:UserGroup = new UserGroup();
[Bindable] private var user:User = new User();

[Bindable] private var usersOfTheGroup:ArrayCollection = new ArrayCollection();
[Bindable] private var nonUsersOfTheGroup:ArrayCollection = new ArrayCollection();

[ArrayElementType("int")]
private var ids:ArrayCollection = new ArrayCollection();
private var permUtil:PermissionUtil = new PermissionUtil();

private var groupService:GroupService = GroupService.getInstance();
private var permissionService:PermissionService = PermissionService.getInstance();
private const userService:UserService = UserService.getInstance();

private function startUp():void {
	permissionService.listAll(onListPermissions);

	gridGroup.addEventListener(CrudEvent.COPY, onCopyGroup);
	gridGroup.addEventListener(CrudEvent.EDIT, onEditGroup);
    groupform.addEventListener(CrudEvent.CREATE, onAddUser);
    groupform.addEventListener(CrudEvent.DELETE, onRemoveUser);
}

//----------------------------
// Events
//----------------------------

private function onCopyGroup(event:CrudEvent):void {
	cancel();
	group = ObjectUtil.copy(event.entity) as UserGroup;

	group.id = 0;
	updatePermission();
	this.currentState = "copy";
	gridGroup.enabled = false;
}

private function onEditGroup(event:CrudEvent):void {
	cancel();
	group = ObjectUtil.copy(event.entity) as UserGroup;

    // Carrega os combos dos usuários que pertencem ao grupo e o dos que não pertencem.
    listByGroupName(true);
    listByGroupName(false);
	updatePermission();
	this.currentState = "edition";
	gridGroup.enabled = false;
}

private function newGroup():void {
	group = new UserGroup();
	this.currentState = 'edition';
    usersOfTheGroup = new ArrayCollection();
    nonUsersOfTheGroup = new ArrayCollection();
}

private function save():void {
	if (groupName.text != '') {
		group.name = groupName.text
		group.showAsReporters = checkGuidelineReporters.selected;
		group.showAsProducers = checkGuidelineProducers.selected;
        group.showAsChecklistProducers = checkChecklistProducers.selected;
		group.permissions = new ArrayCollection();
        group.users = usersOfTheGroup;
		permUtil.addCheckedPermissionsToGroup(group, permissions);
		checkExistsGroup(group.name);
	} else {
		//TODO o nome do grupo esta em branco
	}
}

private function checkExistsGroup(name:String):void {
	groupService.listGroupByName(name, function(event:ResultEvent):void {
		var groupCheck:UserGroup = event.result as UserGroup;
		if (groupCheck != null && this.currentState == "copy") {
			ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'group.alreadyExists'), Info.WARNING);
			groupName.errorString = ResourceManager.getInstance().getString('Bundle', 'group.alreadyExists');
		} else {
			if (groupCheck != null && group != null && groupCheck.id != group.id) {
				ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'group.alreadyExists'), Info.WARNING);
				groupName.errorString = ResourceManager.getInstance().getString('Bundle', 'group.alreadyExists');
			} else {
				groupService.save(group, onSaveSuccess);
			}
		}
	});
}

private function cancel():void {
	group = new UserGroup();
	this.currentState = "view";
	gridGroup.enabled = true;
    usersOfTheGroup.removeAll();
    nonUsersOfTheGroup.removeAll();
}

private function gridGroup_changeHandler(event:GridSelectionEvent):void {
	group = gridGroup.selectedItem as UserGroup
	updatePermission();
}

private function updatePermission():void {
	permUtil.clearPermissions(permissions);

	for each (var permission:Permission in group.permissions) {
		//Marca na lista geral de permissoes, a permissao do grupo.
		permUtil.checkPermissions(permission, permissions);
	}

}

private function listByGroupName(belongsToTheGroup:Boolean):void {
    // Quando o grupo for novo, só permite pesquisar se a flag belongsToTheGroup for falsa, ou seja, a pesquisa é por usuarios que não estejam no grupo
    // pois como o grupo ainda não exite no banco, não faz sentido buscar por usuarios desse grupo, já que ele ainda não existe

    if (!(group.id == 0 && belongsToTheGroup == true)){
        if(belongsToTheGroup){
            userService.listByGroupName(searchInputUsers.text, group.id, belongsToTheGroup,
                    function(event:ResultEvent):void { usersOfTheGroup = event.result as ArrayCollection;});
        } else {
            userService.listByGroupName(searchInputNonUsersOfTheGroup.text, group.id, belongsToTheGroup,
                    function(event:ResultEvent):void { nonUsersOfTheGroup = event.result as ArrayCollection;});
        }
    }
}

private function onAddUser(event:CrudEvent):void {
    user = ObjectUtil.copy(event.entity) as User;
    usersOfTheGroup.addItem(user);

    var nonUserOfGroup:User = new User();
    for (var i:int = 0; i < nonUsersOfTheGroup.length; i++)
    {
        nonUserOfGroup = nonUsersOfTheGroup.getItemAt(i) as User;
        if(nonUserOfGroup.id == user.id){
            nonUsersOfTheGroup.removeItemAt(i);
            break;
        }
    }
}


private function onRemoveUser(event:CrudEvent):void {
    user = ObjectUtil.copy(event.entity) as User;
    nonUsersOfTheGroup.addItem(user);

    var userOfGroup:User = new User();
    for (var i:int = 0; i < usersOfTheGroup.length; i++)
    {
        userOfGroup = usersOfTheGroup.getItemAt(i) as User;
        if(userOfGroup.id == user.id){
            usersOfTheGroup.removeItemAt(i);
            break;
        }
    }
}

// -----------------------------
// Asynchronous answers
// -----------------------------

private function onListPermissions(event:ResultEvent):void {
	permissions = event.result as Permission;
}

private function onSuccess(event:ResultEvent):void {
	refresh();
}

private function onSaveSuccess(event:ResultEvent):void {
	ANews.showInfo(this.resourceManager.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
	refresh();
}

// --------------------------
// Helpers.
// --------------------------
private function refresh():void {
	//Restore default values.
	group = new UserGroup();
	this.currentState = "view";

	gridGroup.enabled = true;
}
