import flash.display.DisplayObject;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.controls.Menu;
import mx.core.FlexGlobals;
import mx.core.mx_internal;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.utils.ObjectUtil;

import spark.components.ButtonBarButton;
import spark.layouts.supportClasses.DropLocation;

import tv.snews.anews.component.ButtonAddVideo;

import tv.snews.anews.component.ChangesHistoryWindow;
import tv.snews.anews.component.FormScrollManager;
import tv.snews.anews.component.Info;
import tv.snews.anews.domain.Device;
import tv.snews.anews.domain.AbstractScriptItem;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.IIDevice;
import tv.snews.anews.domain.IITemplate;
import tv.snews.anews.domain.Media;
import tv.snews.anews.domain.MosDevice;
import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptCG;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.domain.ScriptPlanAction;
import tv.snews.anews.domain.ScriptVideo;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.WSMedia;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.event.PluginActiveXEvent;
import tv.snews.anews.service.DeviceService;
import tv.snews.anews.service.GuidelineService;
import tv.snews.anews.service.MediaCenterService;
import tv.snews.anews.service.ReportageService;
import tv.snews.anews.service.ScriptPlanService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.cg.ii.CGTemplatesWindow;
import tv.snews.anews.view.guideline.GuidelineViewer;
import tv.snews.anews.view.reportage.ReportageViewer;
import tv.snews.anews.view.script.ScriptRevisionRenderer;
import tv.snews.anews.view.script.viewer.ScriptViewer;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

// Necessário para acessar as propriedades "dragScrollInitialDelay" e "dragScrollSpeed" do rundownListLayout
use namespace mx_internal;

private const bundle:IResourceManager = ResourceManager.getInstance();

private static const PRINT:String = ResourceManager.getInstance().getString('Bundle', 'defaults.btn.print');
private static const SEE_GUIDELINE_LABEL:String = ResourceManager.getInstance().getString('Bundle', 'script.btn.seeGuideline');
private static const SEE_REPORTAGE_LABEL:String = ResourceManager.getInstance().getString('Bundle', 'script.btn.seeReportage');
private static const SEE_ORIGINAL:String        = ResourceManager.getInstance().getString('Bundle', 'script.btn.seeSourceScript');
private static const OK_LABEL:String			= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.ok');
private static const APPROVE_LABEL:String		= ResourceManager.getInstance().getString('Bundle', 'defaults.btn.approve');
private const scriptPlanService:ScriptPlanService = ScriptPlanService.getInstance();

private const scriptService:ScriptService = ScriptService.getInstance();
private const mediaCenterService:MediaCenterService = MediaCenterService.getInstance();
private const deviceService:DeviceService = DeviceService.getInstance();
private const dateUtil:DateUtil = new DateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();

[Bindable] private var modify:String;

[Bindable]
[ArrayElementType("String")]
private var popUpButtonOptions:ArrayCollection;

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Media")]
private var selectedMedias:ArrayCollection = new ArrayCollection();

[Bindable] private var menu:Menu = new Menu();
[Bindable] private var scriptModify:Boolean;
[Bindable] public var tabManager:TabManager;
[Bindable] public var script:Script;
[Bindable] public var playout:MosDevice;
[Bindable] public var playoutChannels:ArrayCollection = new ArrayCollection();

private var timer:Timer = new Timer(5000);
private var scriptOld:Script;
private var btnAddVideo:ButtonAddVideo = new ButtonAddVideo();
private var scrollManager:FormScrollManager;
private var assumeEdition:Boolean = false;

// -----------------------
// Events handler
// -----------------------

private function onContentCreationComplete(event:FlexEvent):void {
    scriptOld = ObjectUtil.clone(script) as Script;

    activeControlOfChanges();

    scriptService.subscribe(onScriptMessageReceived);
    scriptPlanService.subscribe(onScriptPlanMessageReceived);
    deviceService.subscribe(onDeviceChange);
    this.listScriptCgs.addEventListener(CrudEvent.DELETE, onDeleteCG);

    if (script.program.playoutDevice is MosDevice) {
        playout = MosDevice(script.program.playoutDevice);
        playoutChannels.addAll(playout.channels);
    }
    enableDisableVideoButton(DomainCache.mamDevices.length != 0);
    script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
    btnAddVideo.crudEventCreateHandler = crudEventCreateHandler;

    listScriptVideos.addEventListener(CrudEvent.DELETE, crudEventDeleteHandler);

    scrollManager = new FormScrollManager(formScroller);
}

private function onRemove(event:FlexEvent):void {
    scrollManager.deactivate();

    scriptService.unsubscribe(onScriptMessageReceived);
    deviceService.unsubscribe(onDeviceChange);
    scriptPlanService.unsubscribe(onScriptPlanMessageReceived);

    this.listScriptCgs.removeEventListener(CrudEvent.DELETE, onDeleteCG);
    listScriptVideos.addEventListener(CrudEvent.DELETE, crudEventDeleteHandler);
}

private function onScriptMessageReceived(event:MessageEvent):void {
    var scriptServer:Script = event.message.body as Script;
    var action:String = event.message.headers["action"];
    var scriptId:int = (scriptServer != null) ? scriptServer.id : event.message.headers["scriptId"];

    if (script == null || script.id != scriptId)
        return;

    switch (action) {
        case EntityAction.FIELD_UPDATED:  {
            var field:String = event.message.headers["field"];
            var value:Object = event.message.headers["value"];
            if(field.toLowerCase() == 'image_editor')
                script.imageEditor = value as User;
            else
                script[field.toLowerCase()] = value;

            // Se mudou o "ok" ou a aprovação, atualiza o PopUpButton
            if (field == "ok" || field == "approved") {
                initPopUpButtonMenu();
            } else if(field.toLowerCase() == 'displayed') {
                script.displaying = false;
                setSaveEnabled(false);
            } else if(field.toLowerCase() == 'displaying') {
                setSaveEnabled(false);
                ANews.showInfo(bundle.getString("Bundle", "script.msg.displaying", [StringUtils.isNullOrEmpty(script.slug) ? bundle.getString('Bundle', 'defaults.noSlug') : script.slug]), Info.WARNING);
            }
            break;
        }
        case EntityAction.UPDATED:
        {
            script.updateFields(scriptServer);
            if (script.videos) script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
            scriptOld = ObjectUtil.clone(script) as Script;
            scriptModify = false;
            break;
        }
        case EntityAction.UNLOCKED:
        {
            script.editingUser = null;
            break;
        }
        case EntityAction.LOCKED:
        {
            script.editingUser = event.message.headers["user"] as User;
            if (script.editingUser.equals(ANews.USER)) {
                setSaveEnabled(true);
            } else {
                Alert.show(bundle.getString("Bundle", "script.isEditingWithSlug", [ StringUtils.isNullOrEmpty(script.slug) ? '(' + script.page + ') ' + bundle.getString('Bundle', 'defaults.noSlug') : '(' + script.page +') ' + script.slug , script.editingUser.name ]), null);
                setSaveEnabled(false);
                assumeEdition = true;
            }
            break;
        }
    }
}

public function onDeviceChange(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var aux:Device = event.message.body as Device;

    if(aux is MosDevice){
        var device:Device = event.message.body as MosDevice;
        switch (action) {
            case EntityAction.UPDATED:
                if (playout.id == device.id) {
                    playout.update(device);
                    playoutChannels.removeAll();
                    playoutChannels.addAll(playout.channels);
                    break;
                }
            break;
            case EntityAction.DELETED:
                if (playout.id == device.id) {
                    playout = null;
                    playoutChannels.removeAll();
                    for each (var video:ScriptVideo in script.videos){
                        video.channel = null;
                    }
                }
            break;
        }
    }
}


private function onScriptPlanMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var scriptPlanUpdated:ScriptPlan = event.message.body as ScriptPlan;

    if (script.block.scriptPlan.id == scriptPlanUpdated.id) {
        //Caso tenha sido feita uma solicitação de mudança de data, o notificador notificará
        //para que a data do roteiro seja trocada
        switch (action) {
            case EntityAction.FIELD_UPDATED:

                break;
            case ScriptPlanAction.SORTED:

                break;
            case EntityAction.LOCKED:

                break;
            case EntityAction.UNLOCKED:

                break;
            case ScriptPlanAction.DISPLAY_RESETS:
                setSaveEnabled(true);
                script.displayed = false;
                script.displaying = false;
                script.block.scriptPlan.displayed = false;
                break;
            case ScriptPlanAction.COMPLETE_DISPLAY:
                setSaveEnabled(false);
                script.block.scriptPlan.displayed = true;
                script.displaying = false;
                ANews.showInfo(bundle.getString("Bundle", "scriptarea.msg.displaying", [StringUtils.isNullOrEmpty(script.slug) ? bundle.getString('Bundle', 'defaults.noSlug') : script.slug]), Info.WARNING);
                break;
            default:
                //INSERTED, DELETED AND UPDATE are not supported on the scriptPlan.
                throw new Error("You receive an undefined message for a script, try to reload you script!");
        }
    }
}

private function onGetVersions():void {
    scriptService.loadChangesHistory(script.id, onLoadChangesHistorySuccess);
}

private function onDeleteCG(event:CrudEvent):void {
    event.stopPropagation();
    event.preventDefault();
    script.removeCG(event.entity as AbstractScriptItem);
    script.reorderCGs();
}

private function onSave(closeable:Boolean, scriptReport:String=null):void {
    timer.stop();

    setDataFromCombos();

    if (!isValid()) {
        ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.requiredFields'), Info.WARNING);
        timer.start();
        return;
    }

    scriptModify = script.isDifferent(scriptOld) && wasNotDisplayed();

    if (scriptModify) {
        this.btnSave.enabled = this.btnClose.enabled = this.btnSaveAndClose.enabled = false;
        scriptService.merge(script, function (event:ResultEvent):void {
            ANews.showInfo(bundle.getString('Bundle', 'defaults.msg.saveSuccess'), Info.INFO);
            scriptOld = ObjectUtil.clone(script) as Script;
            scriptModify = false;
            availableButtons();
            if (scriptReport) {
                print(scriptReport);
            }
        }, onMergeFault);
    }

    if (closeable) {
        unlockAndRemoveTab(true);
        return;
    }

    timer.start();
}

public function onClose():void {
    if (assumeEdition) {
        unlockAndRemoveTab(false);
        return;
    }

	scriptModify = script.isDifferent(scriptOld) && wasNotDisplayed();

	if (script.uploading) {
		ANews.showInfo(bundle.getString('Bundle', 'scriptarea.msg.ScriptFormSendingImage'), Info.WARNING);
	} else {
		if (scriptModify) {
			Alert.show(
					bundle.getString("Bundle", "defaults.msg.dataAreNotSaved"),
					bundle.getString("Bundle", "defaults.msg.confirmation"),
							Alert.CANCEL | Alert.NO | Alert.YES, null, onChooseOption, null, Alert.YES
			);
			return;
		}

		unlockAndRemoveTab(true);
	}
}

private function onChooseOption(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        onSave(true);
    } else if (event.detail == Alert.NO) {
        unlockAndRemoveTab(true);
    }
}

private function initPopUpButtonMenu():void {
    popUpButtonOptions = new ArrayCollection();

    if (ANews.USER.allow("01170904")) {
        popUpButtonOptions.addItem(PRINT);
    }

    // botão "ver script original"
    if(script.sourceScript) popUpButtonOptions.addItem(SEE_ORIGINAL);

    // botão "ver reportagem"
    if(script.reportage) popUpButtonOptions.addItem(SEE_REPORTAGE_LABEL);

    // botão "ver pauta"
    if(script.guideline) popUpButtonOptions.addItem(SEE_GUIDELINE_LABEL);

    // botão "ok"
    if(ANews.USER.equals(script.editor) && ANews.USER.allow('01170906') && !script.ok) popUpButtonOptions.addItem(OK_LABEL);

    // botão "aprovar"
    if(ANews.USER.allow('01170903') && !script.approved) popUpButtonOptions.addItem(APPROVE_LABEL);

    menu.dataProvider = popUpButtonOptions;
    menu.addEventListener(MenuEvent.ITEM_CLICK, onPopUpButtonItemClick);

}

private function crudEventCreateHandler(crudEvent:CrudEvent):void {
    var medias:ArrayCollection = crudEvent.entity as ArrayCollection;
    if (medias[0] is WSMedia) {
        selectedMedias = medias;
        addScriptVideo();
    } else {
        mediaCenterService.loadById(medias, onVerifyMosMediaAdded);
    }
}

private function onVerifyMosMediaAdded(event:ResultEvent):void {
    selectedMedias = event.result as ArrayCollection;
    addScriptVideo();
}

private function addScriptVideo():void {
	var mediasAdded:int = script.videos.length;

	for each (var media:Media in selectedMedias) {
		if (script.existMediaInScriptVideos(media)) {
			ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAlreadyAdded', [media.slug]), Info.WARNING);
			continue;
		}
		var video:ScriptVideo = new ScriptVideo();
		video.media = media;
		script.addVideo(video);
	}

	script.updateVTTime();
	script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));

	if (script.videos.length - mediasAdded > 0) {
		ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAddedSucess', [script.videos.length - mediasAdded]), Info.INFO);
	}
}

public function crudEventDeleteHandler(event:CrudEvent):void {
    var video:ScriptVideo = event.entity as ScriptVideo;
    script.removeVideo(video);
    script.updateVTTime();
    script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
}

private function onAddCG():void {
    if (script.program.cgDevice == null) {
        ANews.showInfo(bundle.getString('Bundle', 'cg.notFound'), Info.WARNING);
        return;
    }

    if (script.program.cgDevice is IIDevice) {
        openPopUpCG();
    } else if (script.program.cgDevice is MosDevice) {
        if ((script.program.cgDevice as MosDevice).activeX) {
            openActiveX(script.program.cgDevice as MosDevice);
        } else {
            ANews.showInfo(bundle.getString('Bundle', 'cg.msg.verifyConfigDevice'), Info.WARNING);
        }
    }
}

private function onAddSection():void {
    switch (this.sectionsToolBar.selectedIndex) {
        case 0:
            this.imageBoard.uploadFiles();
            break;
        case 1:
            onAddCG();
            break;
        case 2:
            btnAddVideo.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
            break;
        default :
            break;
    }
}

//---------------------------------
//	Drag n' drop events
//---------------------------------

public function onDragCompleteVideos(event:DragEvent):void {
    this.callLater(function():void {
        script.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
    });
}

public function onDragCompleteCG(event:DragEvent):void {
    this.callLater(script.reorderCGs);
}

private function onDragVideoOver(event:DragEvent):void {
    /*
     * NOTE Chamar o método preventDefault() causa um bug no scroll da lista. Isso
     * porque o método dragOverHandler() do componente List deixa de ser executado.
     *
     * Para evitar esse bug, copiei o código do método dragOverHandler() do
     * componente List e fiz as modificações necessárias.
     */
    var items:* = event.dragSource.dataForFormat("itemsByIndex");
    if (items is Vector.<Object> && Vector.<Object>(items).length > 0) {
        var dragItem:Object = items[0];
        if (dragItem is ScriptVideo) {
            // Não deixa executar o listener padrão para que possa exibir o feedback NONE
            event.preventDefault();

            // Trecho de código retirado de List.dragOverHandler()
            // Garante que o scroll continue funcionando

            var dropLocation:DropLocation = this.listScriptVideos.layout.calculateDropLocation(event);
            if (dropLocation) {
                this.listScriptVideos.drawFocusAnyway = true;
                this.listScriptVideos.drawFocus(true);

                this.listScriptVideos.layout.showDropIndicator(dropLocation);
            } else {
                this.listScriptVideos.layout.hideDropIndicator();

                this.listScriptVideos.drawFocus(false);
                this.listScriptVideos.drawFocusAnyway = false;
            }
        } else {
            event.preventDefault();
            DragManager.showFeedback(DragManager.NONE);
        }
    }
}

private function onDragCGOver(event:DragEvent):void {
    /*
     * NOTE Chamar o método preventDefault() causa um bug no scroll da lista. Isso
     * porque o método dragOverHandler() do componente List deixa de ser executado.
     *
     * Para evitar esse bug, copiei o código do método dragOverHandler() do
     * componente List e fiz as modificações necessárias.
     */

    var items:* = event.dragSource.dataForFormat("itemsByIndex");
    if (items is Vector.<Object> && Vector.<Object>(items).length > 0) {
        var dragItem:Object = items[0];
        if (dragItem is AbstractScriptItem) {
            // Não deixa executar o listener padrão para que possa exibir o feedback NONE
            event.preventDefault();

            // Trecho de código retirado de List.dragOverHandler()
            // Garante que o scroll continue funcionando

            var dropLocation:DropLocation = this.listScriptCgs.layout.calculateDropLocation(event);
            if (dropLocation) {
                this.listScriptCgs.drawFocusAnyway = true;
                this.listScriptCgs.drawFocus(true);

                this.listScriptCgs.layout.showDropIndicator(dropLocation);
            } else {
                this.listScriptCgs.layout.hideDropIndicator();

                this.listScriptCgs.drawFocus(false);
                this.listScriptCgs.drawFocusAnyway = false;
            }
        } else {
            event.preventDefault();
            DragManager.showFeedback(DragManager.NONE);
        }
    }
}

//---------------------------------
//	Asynchronous Answers
//---------------------------------

private function onLoadChangesHistorySuccess(event:ResultEvent):void {
    var versions:ArrayCollection = event.result as ArrayCollection;

    if (versions == null || versions.length == 0) {
        ANews.showInfo(bundle.getString('Bundle', 'script.msg.noVersions'), Info.WARNING);
        return;
    }

    var changesWindow:ChangesHistoryWindow = new ChangesHistoryWindow();
    changesWindow.addEventListener(CloseEvent.CLOSE,
            function (event:CloseEvent):void {
                PopUpManager.removePopUp(changesWindow);
            }
    );

    changesWindow.dataProvider = versions;
    changesWindow.itemRenderer = ScriptRevisionRenderer;

    PopUpManager.addPopUp(changesWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
    PopUpManager.centerPopUp(changesWindow);
}

private function onMergeFault(event:FaultEvent):void {
    ANews.showInfo(event.fault.faultString, Info.ERROR);
    availableButtons();
}

private function onAddScriptCG(event:CrudEvent):void {
    var template:IITemplate = event.entity as IITemplate;
    var scriptCG:ScriptCG = new ScriptCG(template);
    scriptCG.order = script.cgs.length;
    scriptCG.script = script;
    script.cgs.addItem(scriptCG);
}

// -----------------------
// Helpers
// -----------------------

private function availableButtons():void {
    this.btnSave.enabled = this.btnClose.enabled = this.btnSaveAndClose.enabled = true;
}

private function activeControlOfChanges():void {
    timer.addEventListener(TimerEvent.TIMER, function (event:TimerEvent):void {
        setDataFromCombos();
        scriptModify = script.isDifferent(scriptOld) && wasNotDisplayed();
    });
    timer.start();
}

private function setDataFromCombos():void {
    script.presenter = this.cbPresenter.selectedItem as User;
    script.reporter = this.cbReporter.selectedItem as User;
}

private function isValid():Boolean {
    return Util.isValid(this.validators);
}

private function unlockAndRemoveTab(unlock:Boolean = true):void {
    timer.stop();

    if (script.id > 0 && unlock)
        scriptService.unlockEdition(script.id, null);

    onRemove(null);

//    this.imageBoard.timer.stop();
    this.dispatchEvent(new ANewsPlayEvent(ANewsPlayEvent.CLOSE));
    tabManager.removeTab(this);

}

private function onPopUpButtonItemClick(event:MenuEvent):void {
    switch (event.item as String) {
        case PRINT:
            saveAndPrint(ReportsUtil.SCRIPT);
            break;
        case SEE_ORIGINAL:
            openSourceScript();
            break;
        case SEE_REPORTAGE_LABEL:
            openReportage();
            break;
        case SEE_GUIDELINE_LABEL:
            openGuideline();
            break;
        case OK_LABEL:
            if(script.displayed || script.block.scriptPlan.displayed) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "script.block.displayed"), Info.WARNING);
            else if(script.displaying) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "script.isDisplaying"), Info.WARNING);
            else if(!script.displayed && !script.displaying && !script.block.scriptPlan.displayed)
                finalize();
            break;
        case APPROVE_LABEL:
            if(script.displayed || script.block.scriptPlan.displayed) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "script.block.displayed"), Info.WARNING);
            else if(script.displaying) ANews.showInfo(ResourceManager.getInstance().getString("Bundle", "script.isDisplaying"), Info.WARNING);
            else if(!script.displayed && !script.displaying && !script.block.scriptPlan.displayed)
            approved();
            break;
    }
}

private function openReportage():void {
    if (script.reportage) {
        ReportageService.getInstance().loadById(script.reportage.id, function(event:ResultEvent):void {
            var result:Reportage = Reportage(event.result);
            tabManager.openTab(ReportageViewer, "reportage_" + result.id, { reportage: result, tabManager: tabManager });
        });
    }
}

private function openGuideline():void {
    var guideline:Guideline = null;

    if (script.guideline) {
        guideline = script.guideline;
    } else if (script.reportage && script.reportage.guideline) {
        guideline = script.reportage.guideline;
    }

    GuidelineService.getInstance().loadById(guideline.id,
            function(event:ResultEvent):void {
                var result:Guideline = Guideline(event.result);
                tabManager.openTab(GuidelineViewer, "guideline_" + result.id, { guideline: result, tabManager: tabManager });
            }
    );
}

private function openSourceScript():void {
    scriptService.loadById(script.sourceScript.id,
            function(event:ResultEvent):void {
                var result:Script = Script(event.result);
                tabManager.openTab(ScriptViewer, "script_" + result.id, { script: result, tabManager: tabManager });
            }
    );
}

private function approved():void {
    scriptService.updateField(script.id, "approved", true);
    initPopUpButtonMenu();
}

private function finalize():void {
    scriptService.updateField(script.id, "ok", true);
    initPopUpButtonMenu();
}

private function controlButton():void {
    // TODO Implementar a listagem de GCs
}

private function saveAndPrint(scriptReport:String):void {
	scriptModify = scriptOld.isDifferent(script) && wasNotDisplayed();
	if (scriptModify) {
		Alert.show(
				bundle.getString("Bundle", "defaults.msg.saveBeforePrint"),
				bundle.getString("Bundle", "defaults.msg.confirmation"),
						Alert.NO | Alert.YES, null,

				function (event:CloseEvent):void {
					if (event.detail == Alert.YES) {
						onSave(false, scriptReport);
					}
				}
		);
	} else {
		print(scriptReport);
	}
}

private function print(scriptReport:String):void {
    reportUtil.generate(scriptReport, { ids: script.id });
}

private function enableDisableImageButton(value:Boolean):void {
    var imageButton:ButtonBarButton = this.sectionsToolBar.dataGroup.getElementAt(0) as ButtonBarButton;
    imageButton.enabled = value;
}

private function enableDisableVideoButton(value:Boolean):void {
    var videoButton:ButtonBarButton = this.sectionsToolBar.dataGroup.getElementAt(2) as ButtonBarButton;
    videoButton.enabled = value;
}

private function openPopUpCG():void {
    var iiWindow:CGTemplatesWindow = new CGTemplatesWindow();
    iiWindow.device = script.program.cgDevice as IIDevice;
    iiWindow.addEventListener(CrudEvent.CREATE, onAddScriptCG);

    PopUpManager.addPopUp(iiWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
    PopUpManager.centerPopUp(iiWindow);
}

private function openActiveX(device:MosDevice):void {
    this.dispatchEvent(new PluginActiveXEvent(PluginActiveXEvent.OPEN, device, script));
}

private function wasNotDisplayed():Boolean {
    return !script.displayed && !script.displaying && !script.block.scriptPlan.displayed;
}

private function setSaveEnabled(enable:Boolean):void {
    btnSaveAndClose.enabled = enable;
    btnSave.enabled = enable;
}
