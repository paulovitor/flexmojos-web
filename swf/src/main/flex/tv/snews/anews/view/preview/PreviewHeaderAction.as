import flash.events.Event;

import mx.binding.utils.ChangeWatcher;
import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;
import mx.events.PropertyChangeEvent;

import tv.snews.anews.domain.Rundown;
import tv.snews.anews.domain.Story;
import tv.snews.flexlib.utils.DateUtil;

private var _rundown:Rundown;
private var _stories:ArrayCollection;

//Utils
private var dateUtil:DateUtil = new DateUtil();

//Calcs
private var _produced:Date = dateUtil.timeZero();
private var _available:Date = dateUtil.timeZero();
private var _total:Date = dateUtil.timeZero();

private var _availablePositive:Boolean = true;

//Watchers
public var watcherLimit:ChangeWatcher;
public var watcherBreaks:ChangeWatcher;

//----------------------------------------------------------------------
//	Helpers
//----------------------------------------------------------------------
private function formatTime(time:Date):String {
	return dateUtil.timeToString(time);
}

private function setDateValue(oldValue:Date, newValue:Date, property:String):void {
	var ms:Number = dateUtil.getTimeGap(oldValue, newValue);
	var diff:Date = dateUtil.getTimeFromMilliseconds(ms);
	
	var result:Date = null;
	if (oldValue.getTime() > newValue.getTime()) {
		result = dateUtil.subtractTimes(this[property], diff);
	} else {
		result = dateUtil.sumTimes(this[property], diff);
	}
	this[property] = result;
	
	updateTotal();
}

/**
 * Faz o cálculo do tempo produzido, tempo comercial e tempo previsto.
 */
public function updateTimers():void {
	produced = dateUtil.timeZero();
	
	for each (var story:Story in stories) {
		addStory(story);
	}
	
	updateTotal();
}

private function addStory(story:Story):void {
	if(story == null || story.block.standBy)
		return;
	produced = dateUtil.sumTimes(produced, story.total);
}

private function removeStory(story:Story):void {
	if(story == null)
		return;
	produced = dateUtil.subtractTimes(produced, story.total);
}


private function updateTotal():void {
	total = dateUtil.sumTimes(produced, rundown.commercial);
	updateAvailable();
}

private function updateAvailable():void {
	if (rundown.limit.time >= total.time) {
		available = dateUtil.subtractTimes(rundown.limit, total);
		availablePositive = true;
	} else {
		available = dateUtil.subtractTimes(total, rundown.limit);
		availablePositive = false;
	}
}
//----------------------------------------------------------------------
//	Setters & Getters
//----------------------------------------------------------------------
public function get rundown():Rundown {
	return _rundown;
}

[Bindable]
public function set rundown(value:Rundown):void {
	if (watcherLimit != null) 
		watcherLimit.unwatch();
	
	if(watcherBreaks != null)
		watcherBreaks.unwatch();
	
	_rundown = value;
	
	watcherLimit = ChangeWatcher.watch(rundown,["limit"], onChangeTotal);
	watcherBreaks = ChangeWatcher.watch(rundown,["commercial"], onChangeTotal);
}

public function get stories():ArrayCollection {
	return _stories;
}

[Bindable]
[ArrayElementType("tv.snews.anews.domain.Story")]
public function set stories(value:ArrayCollection):void {
	if (stories != null) {
		stories.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onStoriesChange);
	}
	
	_stories = value;
	updateTimers();
	
	if (stories != null) {
		stories.addEventListener(CollectionEvent.COLLECTION_CHANGE, onStoriesChange);
	}
}

public function get produced():Date {
	return _produced;
}

[Bindable]
public function set produced(value:Date):void {
	if(value == null)
		value = dateUtil.timeZero();
	
	_produced = value;
}

public function get available():Date {
	return _available;
}

[Bindable]
public function set available(value:Date):void {
	if(value == null)
		value = dateUtil.timeZero();
	
	_available = value;
}

public function get total():Date {
	return _total;
}

[Bindable]
public function set total(value:Date):void {
	if(value == null)
		value = dateUtil.timeZero();
	
	_total = value;
}

public function get availablePositive():Boolean {
	return _availablePositive;
}

[Bindable]
public function set availablePositive(value:Boolean):void {
	_availablePositive = value;
}

//----------------------------------------------------------------------
//	Events
//----------------------------------------------------------------------
private function onStoriesChange(event:CollectionEvent):void {
	switch (event.kind) {
		case CollectionEventKind.ADD:
			updateTimers();
			break;
		case CollectionEventKind.REMOVE:
			updateTimers();
			break;
		case CollectionEventKind.UPDATE:
			for each (var change:PropertyChangeEvent in event.items) {
			switch (change.property) {
				case "total":
					setDateValue(change.oldValue as Date, change.newValue as Date, "produced");
					break;
			}
		}
			break;
	}
}


private function onChangeTotal(event:Event):void {
	updateTotal();
}