import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;

import mx.collections.IList;
import mx.controls.Alert;

import mx.core.UIComponent;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.events.StateChangeEvent;
import mx.managers.IFocusManagerComponent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.components.ComboBox;

import spark.components.Group;
import spark.components.Label;
import spark.components.List;
import spark.components.TextInput;

import tv.snews.anews.component.Info;

import tv.snews.anews.component.LabelWithFocus;
import tv.snews.anews.component.TimeInput;

import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.User;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.view.script.ScriptExpand;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

private static const service:ScriptService = ScriptService.getInstance();
private static const dateUtil:DateUtil = new DateUtil();
private static const timeFormat:String = "NN:SS";

private var created:Boolean = false;
private var changePending:Boolean = false;
private var canEdit:Boolean;
private var canEditApproved:Boolean;
private var script:Script;
private var expander:ScriptExpand;

//------------------------------
//	Event Handlers
//------------------------------

private function creationCompleteHandler(event:FlexEvent):void {
    created = true;

    if (changePending) {
        populate();
    }

    configureEventHandlers();
}

private function dataChangeHandler(event:FlexEvent):void {
    if (created) {
        populate();
    } else {
        changePending = true;
    }
}

//------------------------------
//	Methods
//------------------------------

private function configureEventHandlers():void {
    setTimeout(function ():void {
        dragLabel.addEventListener(MouseEvent.MOUSE_DOWN, dragImage_mouseDownHandler);
        dragLabel.addEventListener(MouseEvent.MOUSE_UP, dragImage_mouseUpHandler);
        dragLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
        dragLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

        pageLabel.configureLabelEvents(pageLabelEditHandler);
        slugLabel.configureLabelEvents(slugLabelEditHandler);

        reporterLabel.configureLabelEvents(reporterLabelEditHandler);
        editorLabel.configureLabelEvents(editorLabelEditHandler);
        imageEditorLabel.configureLabelEvents(imageEditorLabelEditHandler);

        tapeLabel.configureLabelEvents(tapeLabelEditHandler);
        okLabel.addEventListener(MouseEvent.CLICK, okLabel_clickHandler);
        okLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
        okLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);
        vtLabel.configureLabelEvents(vtLabelEditHandler);
        approvedLabel.addEventListener(MouseEvent.CLICK, approvedLabel_clickHandler);
        approvedLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
        approvedLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

        indexLabel.configureLabelEvents(indexLabelEditHandler);

        collapseLabel.addEventListener(MouseEvent.CLICK, collapseLabel_clickHandler);
        collapseLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
        collapseLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

        displayingLabel.addEventListener(MouseEvent.CLICK, displayingLabel_clickHandler);
        displayingLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
        displayingLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);

        deleteLabel.addEventListener(MouseEvent.CLICK, deleteLabel_clickHandler);
        deleteLabel.addEventListener(MouseEvent.MOUSE_OVER, iconLabel_mouseOverHandler);
        deleteLabel.addEventListener(MouseEvent.MOUSE_OUT, iconLabel_mouseOutHandler);
    }, 500);
}

private function populate():void {
    changePending = false;

    script = data as Script;
    if (script != null) {
        canEditApproved = script.approved ? ANews.USER.allow('01170902') : true;
        canEdit = script.editingUser == null && ANews.USER.allow('01170906');

        defineVisibility(script);
        defineVisualStyle(script);
        defineEnabledState(script);
        defineToolTip(script);
        displayProperties(script);
    }
}

private function defineVisibility(script:Script):void {
    collapseLabel.visible = DomainCache.mamDevices.length > 0;
    displayingLabel.visible = !script.block.standBy;
    authorLabel.visible = authorLabel.includeInLayout = DomainCache.settings.enableScriptPlanAuthor;
    imageEditorGroup.visible = imageEditorGroup.includeInLayout = DomainCache.settings.enableScriptPlanImageEditor;
}

private function defineVisualStyle(script:Script):void {
    backgroundColor.color = script.getColor();
}

private function defineEnabledState(script:Script):void {
    valuesGroup.enabled = !script.displayed && !script.displaying && !script.block.scriptPlan.displayed;

    setEnabledState(dragLabel, script.editingUser == null && ANews.USER.allow('011707') && canEditApproved);
    setEnabledState(pageLabel, canEditApproved && canEdit);
    setEnabledState(slugLabel, canEditApproved && canEdit);
    setEnabledState(reporterLabel, !(script.hasGuideline || script.hasReportage) && canEditApproved && canEdit);
    setEnabledState(editorLabel, canEditApproved && canEdit);
    setEnabledState(imageEditorLabel, canEditApproved && canEdit);
    setEnabledState(tapeLabel, canEditApproved && canEdit);
    setEnabledState(okLabel, canEdit && canEditApproved);
    setEnabledState(vtLabel, canEditApproved && canEdit);
    setEnabledState(approvedLabel, ANews.USER.allow('01170903') && canEditApproved);

    setEnabledState(indexLabel, ANews.USER.allow('01071107') && canEditApproved);
    setEnabledState(collapseLabel, script.editingUser == null && ANews.USER.allow('011201'));

    if (displayingLabel.visible) {
        setEnabledState(displayingLabel, ANews.USER.allow('01170305'));
    }

    setEnabledState(deleteLabel, script.editingUser == null && ANews.USER.allow('01170905') && canEdit && !script.isDragging);
}

private function setEnabledState(label:Label, enabled:Boolean):void {
    label.mouseEnabled = label.enabled = enabled;
}

private function defineToolTip(script:Script):void {
    const rm:IResourceManager = ResourceManager.getInstance();

    toolTip = function():String {
        if (script.displayed) {
            return rm.getString('Bundle', 'rundown.block.displayed');
        }
        if (script.displaying) {
            return rm.getString('Bundle', 'story.isDisplaying');
        }
        if (script.editingUser) {
            return rm.getString('Bundle', 'story.isEditing', [ script.editingUser.nickname ]);
        }
        return "";
    }();

    var lastChange:String = rm.getString('Bundle', 'rundown.block.author');

    if (toolTip != "") {
        toolTip += "\n\n";
    }

    toolTip += lastChange + ": " + script.author.nickname;
}

private function displayProperties(script:Script):void {
    pageLabel.text = script.page;
    slugLabel.text = script.slug;

    authorLabel.text = script.author.nickname;

    reporterLabel.text = reporterLabel.toolTip = script.reporter ? script.reporter.nickname : script.reportage ? script.reportage.reporter.nickname : script.guideline ? script.guideline.reportersAsStr : "";
    editorLabel.text = editorLabel.toolTip = userNickname(script.editor);
    imageEditorLabel.text = imageEditorLabel.toolTip = userNickname(script.imageEditor);

    tapeLabel.text = script.tape;
    okLabel.text = script.ok ? CHECK_SELECTED_ICON : CHECK_ICON;
    vtLabel.text = formatTime(script.vt);
    approvedLabel.text = script.approved ? CHECK_SELECTED_ICON : CHECK_ICON;

    indexLabel.text = script.globalIndex + "";
    displayingLabel.text = script.displaying ? RADIO_SELECTED_ICON : RADIO_ICON;
}

//------------------------------
//	Fields Event Handlers
//------------------------------

private function dragImage_mouseDownHandler(event:MouseEvent):void {
    List(owner).dragEnabled = true;
}

private function dragImage_mouseUpHandler(event:MouseEvent):void {
    List(owner).dragEnabled = true;
}

private function pageLabelEditHandler():void {
    var oldPage:String = script.page;
    var pageInput:TextInput = createTextInput("page", oldPage, pageGroup.width, 3);
    configureTextInput(pageGroup, pageInput, 2, pageLabel, null, slugLabel, function ():void {
        var newPage:String = fixPage(pageInput.text);
        if (isValidPage(newPage) && newPage != oldPage) {
            pageLabel.text = newPage;
            service.updateField(script.id, "page", newPage, null, function(event:FaultEvent):void {
                faultFunction(pageLabel, oldPage, event);
            });
        }

        escapeFunction(pageInput, pageGroup);
    });
}

private function slugLabelEditHandler():void {
    const uppercase:Boolean = DomainCache.settings.editorUpperCase;

    var oldSlug:String = script.slug;
    var slugInput:TextInput = createTextInput("slug", oldSlug, slugGroup.width, 40, uppercase);

    configureTextInput(slugGroup, slugInput, 3, slugLabel, pageLabel, DomainCache.settings.enableScriptPlanImageEditor ? imageEditorLabel : editorLabel, function ():void {
        var newSlug:String = uppercase ? slugInput.text.toUpperCase() : slugInput.text;
        if (newSlug != oldSlug) {
            slugLabel.text = newSlug;
            service.updateField(script.id, "slug", newSlug, null, function(event:FaultEvent):void {
                faultFunction(slugLabel, oldSlug, event);
            });
        }

        escapeFunction(slugInput, slugGroup);
    });
}

private function reporterLabelEditHandler():void {
    var oldReporter:User = script.reporter;
    var dataProvider:IList = DomainCache.reporters;
    var reporterCombo:ComboBox = createComboBox(dataProvider, oldReporter, reporterGroup.width);

    configureComboBox(reporterGroup, reporterCombo, 6, reporterLabel, editorLabel, tapeLabel, oldReporter, "reporter");
}

private function editorLabelEditHandler():void {
    var oldEditor:User = script.editor;
    var dataProvider:IList = DomainCache.programCache.editors(script.program);
    var editorCombo:ComboBox = createComboBox(dataProvider, oldEditor, editorGroup.width);

    configureComboBox(editorGroup, editorCombo, 5, editorLabel, DomainCache.settings.enableScriptPlanImageEditor ? imageEditorLabel : slugLabel, reporterLabel, oldEditor, "editor");
}

private function imageEditorLabelEditHandler():void {
    var oldImageEditor:User = script.imageEditor;
    var dataProvider:IList = DomainCache.programCache.imageEditors(script.program);
    var imageEditorCombo:ComboBox = createComboBox(dataProvider, oldImageEditor, imageEditorGroup.width);

    configureComboBox(imageEditorGroup, imageEditorCombo, 4, imageEditorLabel, slugLabel, editorLabel, oldImageEditor, "image_editor");
}

private function tapeLabelEditHandler():void {
    var oldTape:String = script.tape;
    var tapeInput:TextInput = createTextInput("tape", oldTape, tapeGroup.width, 5);

    configureTextInput(tapeGroup, tapeInput, 8, tapeLabel, reporterLabel, vtLabel, function ():void {
        var newTape:String = tapeInput.text;
        if (newTape != oldTape) {
            tapeLabel.text = newTape;
            service.updateField(script.id, "tape", newTape, null, function(event:FaultEvent):void {
                faultFunction(tapeLabel, oldTape, event);
            });
        }

        escapeFunction(tapeInput, tapeGroup);
    });
}

private function okLabel_clickHandler(event:MouseEvent):void {
    setEnabledState(okLabel, false);

    service.updateField(script.id, "ok", !script.ok,
            function (event:ResultEvent):void {
                setEnabledState(okLabel, true);
            }, function (event:FaultEvent):void {
                faultEnabledFunction(okLabel, event);
            }
    );
}

private function vtLabelEditHandler():void {
    var oldVt:Date = script.vt;
    var vtInput:TimeInput = createTimeInput(script.vt, vtGroup.width);

    configureTimeInput(vtGroup, vtInput, 9, vtLabel, tapeLabel, indexLabel, oldVt, "vt");
}

private function approvedLabel_clickHandler(event:MouseEvent):void {
    setEnabledState(approvedLabel, false);

    service.updateField(script.id, "approved", !script.approved,
            function (event:ResultEvent):void {
                setEnabledState(approvedLabel, true);
            }, function (event:FaultEvent):void {
                faultEnabledFunction(approvedLabel, event);
            }
    );
}

private function indexLabelEditHandler():void {
    var oldIndex:String = indexLabel.text;

    var indexInput:TextInput = createTextInput("index", oldIndex, indexGroup.width, 3);
    indexInput.restrict = "0-9";
    configureTextInput(indexGroup, indexInput, 11, indexLabel, vtLabel, null, function ():void {
        var newIndex:String = indexInput.text;

        if (newIndex != oldIndex && !StringUtils.isNullOrEmpty(newIndex)) {
            var index:int = parseInt(newIndex);
            if (!isNaN(index) && index < script.block.scriptPlan.maxGlobalIndex) {
                var target:Script = script.block.scriptPlan.getScriptByGlobalIndex(index);

                if (!script.equals(target)) {
                    var dropIndex:int = target.order;
                    if (script.block.equals(target.block) && script.order < target.order) {
                        dropIndex++;
                    }
                    service.updatePosition(script.id, target.block.id, dropIndex, false);
                }
            }
        }
        escapeFunction(indexInput, indexGroup);
    });
}

private function collapseLabel_clickHandler(event:MouseEvent):void {
    if (expander) {
        contentGroup.removeElement(expander);
        expander = null;

        collapseLabel.text = EXPAND_ICON;
    } else {
        expander = new ScriptExpand();
        expander.script = script;
        expander.enabled = script.editingUser == null && !script.displayed && !script.displaying;
        expander.dispatchEvent(new StateChangeEvent(StateChangeEvent.CURRENT_STATE_CHANGE));

        contentGroup.addElementAt(expander, 1);

        collapseLabel.text = COLLAPSE_ICON;
    }
}

private function displayingLabel_clickHandler(event:MouseEvent):void {
    setEnabledState(displayingLabel, false);

    service.updateField(script.id, "displaying", !script.displaying,
            function (event:ResultEvent):void {
                setEnabledState(displayingLabel, true);
            }, function (event:FaultEvent):void {
                faultEnabledFunction(displayingLabel, event);
            }
    );
}

private function deleteLabel_clickHandler(event:MouseEvent):void {
    deleteLabel.enabled = false;

    var bundle:IResourceManager = ResourceManager.getInstance();

    Alert.show(
            bundle.getString('Bundle', 'scriptarea.script.confirmDelete'),
            bundle.getString('Bundle', 'defaults.msg.confirmation'),
                    Alert.YES | Alert.NO, null,

            function (event:CloseEvent):void {
                if (event.detail == Alert.YES) {
                    if (script.displayed || script.displaying || script.block.scriptPlan.displayed) {
                        ANews.showInfo(bundle.getString('Bundle', 'script.block.displayed'), Info.WARNING);
                        deleteLabel.enabled = true;
                    } else {
                        service.removeScript(script.id,
                                function (event:ResultEvent):void {
                                    ANews.showInfo(bundle.getString('Bundle', 'script.msg.deleteSuccess'), Info.INFO);
                                }
                        );
                    }
                }
            }
    );
}

private function iconLabel_mouseOverHandler(event:MouseEvent):void {
    event.target.setStyle("color", OVER_COLOR);
}

private function iconLabel_mouseOutHandler(event:MouseEvent):void {
    event.target.setStyle("color", NORMAL_COLOR);
}

//------------------------------
//	Helpers
//------------------------------

private function createTextInput(id:String, value:String, width:int, maxChars:int, spelling:Boolean=false):TextInput {
    var input:TextInput = new TextInput();
    input.setStyle("typographicCase", spelling ? "uppercase" : "default");
    input.setStyle("fontSize", 12);
    input.setStyle("borderVisible", false);
    input.setStyle("paddingTop", 2);
    input.setStyle("paddingBottom", 2);
    input.setStyle("paddingLeft", 2);
    input.setStyle("paddingRight", 2);
    input.tabEnabled = false;
    input.tabFocusEnabled = false;
    input.width = width;
    input.maxChars = maxChars;
    input.text = value;
    input.id = id;
    return input;
}

private function createComboBox(dataProvider:IList, selectedItem:User, width:int):ComboBox {
    var comboBox:ComboBox = new ComboBox();
    comboBox.setStyle("fontSize", 12);
    comboBox.setStyle("borderVisible", false);
    comboBox.setStyle("paddingTop", 2);
    comboBox.setStyle("paddingBottom", 2);
    comboBox.setStyle("paddingLeft", 2);
    comboBox.setStyle("paddingRight", 2);
    comboBox.tabEnabled = false;
    comboBox.tabFocusEnabled = false;
    comboBox.width = width;
    comboBox.labelField = "nickname";
    comboBox.dataProvider = dataProvider;
    comboBox.selectedItem = findItem(dataProvider, selectedItem);
    return comboBox;
}

private function createTimeInput(date:Date, width:int):TimeInput {
    var input:TimeInput = new TimeInput();
    input.setStyle("fontSize", 12);
    input.setStyle("borderVisible", false);
    input.setStyle("paddingTop", 2);
    input.setStyle("paddingBottom", 2);
    input.setStyle("paddingLeft", 2);
    input.setStyle("paddingRight", 2);
    input.tabEnabled = false;
    input.tabFocusEnabled = false;
    input.format = "NN:SS";
    input.date = date;
    input.width = width;
    return input;
}

private function findItem(dataProvider:IList, user:User):User {
    if (user == null) {
        return null;
    }
    for each (var current:User in dataProvider) {
        if (current.equals(user)) {
            return current;
        }
    }
    return user;
}

private function configureTimeInput(timeGroup:Group, timeInput:TimeInput, index:int, timeLabel:LabelWithFocus, beforeField:UIComponent, nextField:UIComponent, oldDate:Date, field:String):void {
    configureTextInput(timeGroup, timeInput, index, timeLabel, beforeField, nextField, function ():void {
        var newDate:Date = timeInput.date;
        var newStr:String = newDate ? dateUtil.minuteSecondToString(newDate) : "00:00";
        var oldStr:String = oldDate ? dateUtil.minuteSecondToString(oldDate) : "00:00";

        if (newStr != oldStr) {
            timeLabel.text = newStr;
            service.updateField(script.id, field, newDate, null, function(event:FaultEvent):void {
                faultFunction(timeLabel, oldStr, event);
            });
        }

        escapeFunction(timeInput, timeGroup);
    });
}

private function configureTextInput(textInputGroup:Group, textInput:TextInput, index:int, label:UIComponent, beforeField:UIComponent, nextField:UIComponent, callback:Function):void {
    textInputGroup.visible = textInputGroup.includeInLayout = false;
    valuesGroup.addElementAt(textInput, index);

    setFocusOn(textInput);

    configureInputEvents(textInput, beforeField, nextField, callback, function ():void {
        escapeFunction(textInput, textInputGroup, label);
    });
}

private function configureInputEvents(input:TextInput, beforeField:UIComponent, nextField:UIComponent, callback:Function, escape:Function):void {
    const focusOutHandler:Function = function (event:FocusEvent):void {
        callback();
        input.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
        input.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
        event.stopPropagation();
    };

    const keyDownHandler:Function = function (event:KeyboardEvent):void {
        if (event.keyCode == Keyboard.ESCAPE) {
            escape();
            input.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
            input.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
            event.stopPropagation();
        }
        if (event.keyCode == Keyboard.ENTER || event.keyCode == Keyboard.TAB) {
            callback();
            if (event.keyCode == Keyboard.TAB && event.shiftKey) {
                if (beforeField) {
                    setFocusOn(beforeField);
                } else {
                    escape();
                }
            } else {
                if (nextField) {
                    setFocusOn(nextField);
                } else {
                    escape();
                }
            }
            input.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
            input.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
            event.stopPropagation();
        }
    };

    input.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandler);
    input.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);
}

private function configureComboBox(comboBoxGroup:Group, comboBox:ComboBox, index:int, comboBoxLabel:LabelWithFocus, beforeField:UIComponent, nextField:UIComponent, oldUser:User, field:String):void {
    comboBoxGroup.visible = comboBoxGroup.includeInLayout = false;
    valuesGroup.addElementAt(comboBox, index);

    setFocusOn(comboBox.textInput);

    configureComboBoxEvents(comboBox, beforeField, nextField, function ():void {
                var newUser:User = comboBox.selectedItem as User;
                var comboBoxText:String = comboBox.textInput.text;
                var verifyComboBoxLabel:Boolean = comboBoxText == "" || !(comboBoxText == comboBoxLabel.text);
                if ((newUser && !newUser.equals(oldUser)) || (!newUser && oldUser && verifyComboBoxLabel)) {
                    comboBoxLabel.text = newUser ? newUser.nickname : "";
                    service.updateField(script.id, field, newUser, null, function(event:FaultEvent):void {
                        faultFunction(comboBoxLabel, oldUser ? oldUser.nickname : "", event);
                    });
                } else {
                    if(oldUser)
                        comboBoxLabel.text = oldUser.nickname;
                }
                escapeFunction(comboBox, comboBoxGroup);
            },
            function ():void {
                escapeFunction(comboBox, comboBoxGroup, comboBoxLabel);
            }
    );
}

private function configureComboBoxEvents(comboBox:ComboBox, beforeField:UIComponent, nextField:UIComponent, callback:Function, escape:Function):void {
    const focusOutHandlerComboBox:Function = function (event:FocusEvent):void {
        callback();
        comboBox.textInput.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
        comboBox.textInput.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
        event.stopPropagation();
    };

    const keyDownHandlerComboBox:Function = function (event:KeyboardEvent):void {
        if (event.keyCode == Keyboard.ESCAPE) {
            escape();
            comboBox.textInput.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
            comboBox.textInput.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
            event.stopPropagation();
        }
        if (event.keyCode == Keyboard.ENTER || event.keyCode == Keyboard.TAB) {
            callback();
            if (event.keyCode == Keyboard.TAB && event.shiftKey) {
                setFocusOn(beforeField);
            } else {
                setFocusOn(nextField);
            }
            comboBox.textInput.removeEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
            comboBox.textInput.removeEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
            event.stopPropagation();
        }
    };

    comboBox.textInput.addEventListener(FocusEvent.FOCUS_OUT, focusOutHandlerComboBox);
    comboBox.textInput.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandlerComboBox);
}

private function fixPage(value:String):String {
    value = value.toUpperCase();
    if ("0123456789".indexOf(value.charAt(0)) != -1) {
        if (value.length >= 2) {
            if ("0123456789".indexOf(value.charAt(1)) == -1) {
                value = "0" + value.substring(0, 2);
            }
        } else {
            value = "0" + value;
        }
    }
    return value;
}

private function isValidPage(value:String):Boolean {
    return /^\d{2,3}[A-Z]?$/.test(value);
}

private static function userNickname(user:User):String {
    return user ? user.nickname : "";
}

private static function formatTime(value:Date):String {
    return value ? dateUtil.toString(value, timeFormat) : "00:00";
}

private function faultFunction(label:LabelWithFocus, oldValue:String, event:FaultEvent):void {
    label.text = oldValue;
    service.faultHandler(event);
}

private function faultEnabledFunction(label:Label, event:FaultEvent):void {
    setEnabledState(label, true);
    service.faultHandler(event);
}

private function escapeFunction(component:UIComponent, group:Group, label:UIComponent = null):void {
    if (valuesGroup.containsElement(component)) {
        valuesGroup.removeElement(component);
    }
    group.visible = group.includeInLayout = true;
    if (label)
        setFocusOn(label);
}

private function setFocusOn(component:UIComponent):void {
    callLater(function ():void {
        focusManager.setFocus(component as IFocusManagerComponent);
    });
}
