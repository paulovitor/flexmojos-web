package tv.snews.anews.view.navigator {

	import flash.events.Event;

	public class SelectSearchEvent extends Event {
		public static const SEARCH:String = "search";
		public static const SELECT:String = "selectFilter";

		public var searchText:String;
		public var selectedIndex:int;
		public var selectedItem:*;

		public function SelectSearchEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, searchText:String = "", selectedIndex:int = -1, selectedItem:* = null) {
			this.searchText = searchText;
			this.selectedIndex = selectedIndex;
			this.selectedItem = selectedItem;
			super(type, bubbles, cancelable);

		}

		override public function clone():Event {
			return new SelectSearchEvent(type, bubbles, cancelable, searchText, selectedIndex, selectedItem);
		}
	}
}
