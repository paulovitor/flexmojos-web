import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.PerspectiveProjection;
import flash.ui.Keyboard;

import mx.controls.Alert;
import mx.core.DragSource;
import mx.core.IFactory;
import mx.core.IFlexDisplayObject;
import mx.core.IVisualElement;
import mx.core.UIComponent;
import mx.core.mx_internal;
import mx.effects.Move;
import mx.effects.Sequence;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.managers.DragManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;

import spark.components.CheckBox;
import spark.components.ComboBox;
import spark.components.IItemRenderer;
import spark.components.Label;
import spark.components.List;
import spark.components.TextInput;
import spark.components.supportClasses.SkinnableTextBase;
import spark.events.IndexChangeEvent;
import spark.layouts.supportClasses.DropLocation;

import tv.snews.anews.component.Info;
import tv.snews.anews.component.SpellTextArea;
import tv.snews.anews.component.SpellTextInput;
import tv.snews.anews.component.TimeEditable;
import tv.snews.anews.domain.Guideline;
import tv.snews.anews.domain.Reportage;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.ScriptBlockStatus;
import tv.snews.anews.domain.ScriptPlanStatus;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.ScriptBlockService;
import tv.snews.anews.service.ScriptPlanService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.view.script.ScriptArea;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.anews.utils.Util;

use namespace mx_internal;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const dtUtil:DateUtil = new DateUtil();
private const reportUtil:ReportsUtil = new ReportsUtil();

private var oldValue:Object;

[Bindable] private var quickViewVisible:Boolean = false;
[Bindable] private var block:ScriptBlock;

[Bindable]
[Embed(source = "/assets/section-dragdrop.png")]
public var imgDragDrop:Class;

private var scriptArea:ScriptArea;

private const scriptPlanService:ScriptPlanService = ScriptPlanService.getInstance();
private const scriptBlockService:ScriptBlockService = ScriptBlockService.getInstance();
private const scriptService:ScriptService = ScriptService.getInstance();
private const planService:ScriptPlanService = ScriptPlanService.getInstance();

// --------------------
// Overrides
// --------------------
override public function set data(value:Object):void {
	super.data = value;
	if (value) {
		if (value is ScriptBlock) {
			block = value as ScriptBlock;
		} else {
			throw new Error("You're doing it wrong!");
		}
	}
}

// -----------------------
// Events handler
// -----------------------
protected function onClickPast(event:MouseEvent):void {
    scriptArea.pastScripts(block);
}

/**
 * Handler de onRemove do NavigatorContent
 * externo da Área
 **/
private function onRemove(event:FlexEvent):void {
	// TODO Auto-generated method stub
}

private function onAddScript(event:Event):void {
    btnAddScript.enabled = false;
	scriptService.createScript(block.id, function(event:ResultEvent):void {
		btnAddScript.enabled = true;
	});
}

private function onSynchronize(event:Event):void {
    scriptPlanService.verifyScriptPlan(block.scriptPlan.date, block.scriptPlan.program.id, function (event:ResultEvent):void {
        if (event.result == null)
            return;

        switch (event.result as String) {
            case ScriptPlanStatus.EXIST:
                synchronizeStanByBlock(event);
                break;
            case ScriptPlanStatus.NOT_EXIST:
            case ScriptPlanStatus.CANT_CREATE:
                ANews.showInfo(bundle.getString("Bundle", "script.msg.noScriptPlan"), Info.WARNING);
                break;
            default:
                throw new Error("There's something wrong ");
        }
    });
}

private function onPrint(event:Event):void {
	var paramsScriptPlan:Object = new Object();
    paramsScriptPlan.blockId = block.id;
    paramsScriptPlan.id = block.scriptPlan.id;
    paramsScriptPlan.onlyApproved = false;
	reportUtil.generate(ReportsUtil.SCRIPT_PLAN_FUll, paramsScriptPlan);
}

public function deleteBlock():void {
	if (validBlockToDelete()) {
		Alert.show(bundle.getString('Bundle', 'scriptblock.msg.confirmMessageDelete'), bundle.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onDeleteConfirm);
	}
}

public function onDeleteConfirm(event:CloseEvent):void {
	if (event.detail == Alert.YES) {
        btnDeleteBlock.enabled = false;
		scriptBlockService.remove(block.id, function(event:ResultEvent):void {
			ANews.showInfo(bundle.getString("Bundle", "scriptblock.msg.successDeleted"), Info.INFO);
            btnDeleteBlock.enabled = true;
		});
	}
}
/**
 * Executado nos eventos "click" e "change" da lista de laudas. Faz o controle
 * da lauda selecionada.
 *
 * É necessário executar no "click" também para tratar o caso de troca de
 * seleção entre blocos.
 */
private function onChangeScriptList(event:IndexChangeEvent):void {
	var script:Script = scriptList.selectedItem as Script;
	if (script) {
		this.dispatchEvent(new CrudEvent(CrudEvent.SELECT, script));
	}
}

private function onKeyDown(event:KeyboardEvent):void {
	//Evento é capturado pelo ScriptArea.mxml
}

private function onDoubleClick(event:MouseEvent):void {
	var script:Script = scriptList.selectedItem as Script;
	if (ANews.USER.allow('01170906') && (script.approved ? ANews.USER.allow('01170902') : true)) {
		if (script) {
			this.dispatchEvent(new CrudEvent(CrudEvent.EDIT, script));
		}
	}
}

private function onMoveFromDrawerSuccess(event:ResultEvent):void {
    var resultCode:String = event.result as String;

    ANews.showInfo(bundle.getString("Bundle", "script.trash.restoreSuccess"), Info.INFO);

//    if(resultCode == "CREDITS_REMOVED")
//        ANews.showInfo(bundle.getString('Bundle', 'rundown.block.msg.creditsDeleted'), Info.WARNING);
}

// -----------------------
// Drag n' Drop events
// -----------------------

private function onBlockMouseDown(event:MouseEvent):void {
	if (this.itemIndex > 0) { //0 == bloco de apoio 
		List(this.owner).dragEnabled = true;
		List(this.owner).dragMoveEnabled = true;

		planService.lockScriptPlan(block.scriptPlan.id);
	}
}

// -----------------------
// Helpers
// -----------------------
private function onKeyDownInputs(event:KeyboardEvent):void {
	event.stopPropagation();
	if (event.keyCode == Keyboard.ENTER) {
		textOut(event);
	}
}

private function focusFieldIn(event:FocusEvent):void {
	if (event.currentTarget is TextInput) {
		oldValue = TextInput(event.currentTarget).text;
	}
	
	if (event.currentTarget is ComboBox) {
		oldValue = ComboBox(event.currentTarget).selectedItem;
	}
	
	if (event.currentTarget is CheckBox) {
		oldValue = CheckBox(event.currentTarget).selected;
	}
	
	if (event.currentTarget is SpellTextArea) {
		oldValue = SpellTextArea(event.currentTarget).text;
	}
	
	if (event.currentTarget is SpellTextInput) {
		oldValue = SpellTextInput(event.currentTarget).text;
	}
	
	if (event.currentTarget is TimeEditable) {
		oldValue = TimeEditable(event.currentTarget).editor.text;
	}
}

private function textOut(event:Event):void {
	var input:SkinnableTextBase;
	if (event.currentTarget is SpellTextArea) {
		input = SpellTextArea(event.currentTarget);
	} else if (event.currentTarget is SpellTextInput) {
		input = SpellTextInput(event.currentTarget);
	} else {
		input = TextInput(event.currentTarget);
	}
	
	var field:String = event.currentTarget.id;
	var text:String = input.text;
	
	if (text != null && oldValue != null && text.toUpperCase() == oldValue.toUpperCase()) {
		return;
	}
	
	scriptBlockService.updateField(block.id, field.toUpperCase(), text);
}

private function dateOut(event:FocusEvent):void {
	var input:TextInput;
	if (event.currentTarget is TimeEditable) {
		input = TimeEditable(event.currentTarget).editor;
	} else {
		input = TextInput(event.currentTarget);
	}
	
	var field:String = event.currentTarget.id;
	var time:Date = dtUtil.stringToMinuteSecond(input.text);
		
	if (time == null) {
		input.text = oldValue as String;
		return;
	} else {
		input.text = dtUtil.minuteSecondToString(time);
	}
	
	if (input.text == oldValue) {
		return;
	}
	
	scriptBlockService.updateField(block.id, field.toUpperCase(), time);
}

private function formatTimeAsString(time:Date):String {
	return dtUtil.toString(time, bundle.getString('Bundle', "defaults.timeFormatHourMinuteSecond"));
}

private function validBlockToDelete():Boolean {
	if (block.locked) {
		ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownDragging"), Info.WARNING);
		return false;
	}

	for each (var script:Script in block.scripts) {
		if (script.editingUser) {
			ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownEdition"), Info.WARNING);
			return false;
		}
//		if (script.locked) {
//			ANews.showInfo(bundle.getString("Bundle", "rundown.story.lockedMessageFromRundownDragging"), Info.WARNING);
//			return false;
//		}
	}

	return true;
}

private function synchronizeStanByBlock(event:Event):void {
    scriptBlockService.synchronizeStandByBlock(block.scriptPlan.date, synchronizeTo.selectedDate, block.scriptPlan.program.id, function(event:ResultEvent):void {
        var resultCode:String = event.result as String;
        var resourceManager:IResourceManager = ResourceManager.getInstance();

        switch (resultCode) {
            case ScriptBlockStatus.SUCCESSFUL_SYNCHRONIZATION:
                ANews.showInfo(resourceManager.getString("Bundle", "script.msg.synchronizeSuccess"), Info.INFO);
                hideSyncTo();
                break;
            case ScriptBlockStatus.SCRIPT_PLAN_NOT_FOUND:
                ANews.showInfo(resourceManager.getString("Bundle", "script.msg.noScriptPlan"), Info.WARNING);
                break;
        }
    });
}

public function showSyncTo():void {
    syncTo.visible = true;
    syncTo.includeInLayout = true;
    showHideSyncToEffect(true);
}

public function hideSyncTo():void {
    showHideSyncToEffect(false);

}

private function showHideSyncToEffect(show:Boolean): void {
    var
        _rSeq: Sequence = new Sequence(),
        _rMv: Move = new Move();

    // Configurando os efeitos
    _rMv.duration = 500;
    _rMv.yTo = show ? 0 : -30;
    _rMv.yFrom = show ? -30 : 0;

    // Agrupando os efeitos no parallel
    _rSeq.addChild(_rMv);

    _rSeq.target = syncTo;
    _rSeq.play();
}

//------------------------------
//
//------------------------------

private function onCreationComplete(event:FlexEvent):void {
	var parent:Object = UIComponent(owner).parentDocument;

	while (!(parent is ScriptArea) && parent is UIComponent) {
		parent = UIComponent(parent).parentDocument;
	}

	scriptArea = ScriptArea(parent);
}

private function onScriptMouseDown(event:MouseEvent):void {
	if (!event.ctrlKey && !event.shiftKey) {
		scriptArea.deselectOthers(this);
	}
}

private function onScriptDragStart(event:DragEvent):void {
	planService.lockScriptPlan(block.scriptPlan.id);

	event.preventDefault();

	var dragSource:DragSource = new DragSource();
	scriptList.addDragData(dragSource); // apenas para definir o caretIndex, o handler será substituido
	scriptArea.addDragSourceHandler(dragSource);

	var dragIndicator:IFlexDisplayObject = new Label();

	DragManager.doDrag(scriptList, dragSource, event, dragIndicator, 0, 0, 0.5, true);
}

private function onScriptDragEnter(event:DragEvent):void {
	event.preventDefault();

	var dropLocation:DropLocation = calculateDropLocation(event);
	if (dropLocation) {
		DragManager.acceptDragDrop(scriptList);

		scriptList.createDropIndicator();

		scriptList.drawFocusAnyway = true;
		scriptList.drawFocus(true);

		DragManager.showFeedback(DragManager.COPY);

		scriptList.layout.showDropIndicator(dropLocation);
	} else {
		DragManager.showFeedback(DragManager.NONE);
	}
}

private function onScriptDragOver(event:DragEvent):void {
	event.preventDefault();

	var items:Vector.<Object> = event.dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;
	var dragItem:Object = items && items.length != 0 ? items[0] : null;
	var valid:Boolean = dragItem is Script || dragItem is Guideline || dragItem is Reportage;

	var dropLocation:DropLocation = calculateDropLocation(event);

	if (dropLocation && valid) {
		scriptList.drawFocusAnyway = true;
		scriptList.drawFocus(true);

		DragManager.showFeedback(DragManager.COPY);

		scriptList.layout.showDropIndicator(dropLocation);
	} else {
		scriptList.layout.hideDropIndicator();

		scriptList.drawFocus(false);
		scriptList.drawFocusAnyway = false;

		DragManager.showFeedback(DragManager.NONE);
	}
}

private function onScriptDragDrop(event:DragEvent):void {
	event.preventDefault();

	var dropLocation:DropLocation = calculateDropLocation(event);
	if (!dropLocation) {
		return;
	}
	var dropIndex:int = dropLocation.dropIndex;
	var dropIndexBackup:int = dropIndex;

	var dragSource:DragSource = event.dragSource;
	var items:Vector.<Object> = dragSource.dataForFormat("itemsByIndex") as Vector.<Object>;

	hideVisualIndication();

	DragManager.showFeedback(DragManager.MOVE);

	var caretIndex:int = -1;
	if (dragSource.hasFormat("caretIndex")) {
		caretIndex = event.dragSource.dataForFormat("caretIndex") as int;
	}

	// Se quem iniciou o drag não for uma lista, significa que veio da gaveta
	if (event.dragInitiator is List) {

		var scriptsIds:Array = [];

		//--------------------------------------------------------
		//	Lógica customizada do dragDropHandler padrão do List
		//--------------------------------------------------------

		dropIndex = removeSelectedItems(dropIndex);
		scriptArea.removeOthersSelectedItems(this);

		for (var i:int = 0; i < items.length; i++) {
			var item:Object = items[i];
			block.scripts.addItemAt(item, dropIndex + i);

			scriptsIds.push(item.id);
		}

		//--------------------------------------------------------
		//	Lógica para a persistência das mundanças no servidor
		//--------------------------------------------------------

		this.callLater(scriptService.updatePositions, [ scriptsIds, block.id, dropIndexBackup ]);

	} else {
		if (items.length == 1) {
			var dragItem:Object = items[0];

			// Restaurando um script da gaveta
			if (dragItem is Script) {
				var script:Script = Script(dragItem);
                scriptService.copyDrawerScriptToBlock(script.id, block.id, dropIndex);
			}

			// Criando script à partir de uma pauta
			if (dragItem is Guideline) {
				var guideline:Guideline = Guideline(dragItem);
				scriptService.createScriptFromGuideline(block.id, dropIndex, guideline.id);
			}

			// Criando script à partir de uma reportagem
			if (dragItem is Reportage) {
				var reportage:Reportage = Reportage(dragItem);
				scriptService.createScriptFromReportage(block.id, dropIndex, reportage.id);
			}
		}
	}

	if (caretIndex != -1) {
		scrollToCaretIndex(caretIndex, dropIndex);
	}
}

private function onScriptDragComplete(event:DragEvent):void {
    event.preventDefault();

    planService.unlockScriptPlan(block.scriptPlan.id);

    scriptList.dragEnabled = false;
    scriptList.dragMoveEnabled = false;
}

public function deselect():void {
	scriptList.setSelectedIndices(new Vector.<int>(), false);
	scriptList.validateProperties();
}

public function get selectedIndices():Vector.<int> {
	return scriptList.selectedIndices;
}

public function get selectedItems():Vector.<Object> {
    return scriptList.selectedItems;
}

public function getItemAt(index:int):Object {
	return block.scripts.getItemAt(index);
}

private function hideVisualIndication():void {
	scriptList.layout.hideDropIndicator();
	scriptList.destroyDropIndicator();

	scriptList.drawFocus(false);
	scriptList.drawFocusAnyway = false;
}

private function calculateDropLocation(event:DragEvent):DropLocation {
	if (!scriptList.enabled || !event.dragSource.hasFormat("itemsByIndex")) {
		return null;
	}

	return scriptList.layout.calculateDropLocation(event);
}

public function removeSelectedItems(dropIndex:int = 0):int {
	var indices:Vector.<int> = scriptList.selectedIndices;
	deselect();

	indices.sort(function(a:int, b:int):int {
		return a - b;
	});

	for (var i:int = indices.length - 1; i >= 0; i--) {
		if (indices[i] < dropIndex) {
			dropIndex--;
		}

		block.scripts.removeItemAt(indices[i]);
	}

	return dropIndex;
}

private function scrollToCaretIndex(caretIndex:int, dropIndex:int):void {
	var loopCount:int = 0;

	while (loopCount++ < 10) {
		validateNow();

		var delta:* = scriptList.layout.getScrollPositionDeltaToElement(dropIndex + caretIndex);
		if (!delta || (delta.x == 0 && delta.y == 0)) {
			break;
		}
		layout.horizontalScrollPosition += delta.x;
		layout.verticalScrollPosition += delta.y;
	}
}
