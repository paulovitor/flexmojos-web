import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;

import flexlib.controls.tabBarClasses.SuperTab;
import flexlib.events.SuperTabEvent;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.controls.Menu;
import mx.core.DragSource;
import mx.core.FlexGlobals;
import mx.core.IUIComponent;
import mx.core.IVisualElement;
import mx.core.mx_internal;
import mx.events.CloseEvent;
import mx.events.DragEvent;
import mx.events.FlexEvent;
import mx.events.MenuEvent;
import mx.managers.DragManager;
import mx.managers.PopUpManager;
import mx.messaging.events.MessageEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
import mx.rpc.events.*;

import spark.components.DataGroup;
import spark.components.IItemRenderer;

import spark.components.TextInput;
import spark.layouts.supportClasses.DropLocation;

import tv.snews.anews.component.*;
import tv.snews.anews.domain.ClipboardAction;
import tv.snews.anews.domain.DocumentAction;
import tv.snews.anews.domain.EntityAction;
import tv.snews.anews.domain.IIDevice;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.Script;
import tv.snews.anews.domain.ScriptBlock;
import tv.snews.anews.domain.ScriptPlan;
import tv.snews.anews.domain.ScriptPlanAction;
import tv.snews.anews.domain.ScriptPlanStatus;
import tv.snews.anews.domain.User;
import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.service.DocumentService;
import tv.snews.anews.service.ScriptBlockService;
import tv.snews.anews.service.ScriptPlanService;
import tv.snews.anews.service.ScriptService;
import tv.snews.anews.utils.DomainCache;
import tv.snews.anews.utils.ReportsUtil;
import tv.snews.anews.utils.TabManager;
import tv.snews.anews.utils.Util;
import tv.snews.anews.view.script.ScriptBlockRenderer;
import tv.snews.anews.view.script.ScriptForm;
import tv.snews.anews.view.script.ScriptPrintOptionsWindow;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

use namespace mx_internal;

private const bundle:IResourceManager = ResourceManager.getInstance();
private const dtUtil:DateUtil = new DateUtil();
private const scriptPlanService:ScriptPlanService = ScriptPlanService.getInstance();
private const scriptBlockService:ScriptBlockService = ScriptBlockService.getInstance();
private const scriptService:ScriptService = ScriptService.getInstance();
private const documentService:DocumentService = DocumentService.getInstance();
private const planService:ScriptPlanService = ScriptPlanService.getInstance();

private const STATE_NORMAL:String = "normal";
private const STATE_SCRIPT_PLAN_NOT_EXIST:String = "script_plan_not_exist";
private const STATE_SCRIPT_PLAN_CANT_CREATE:String = "script_plan_cant_create";
private const STATE_SCRIPT_PLAN_EXIST:String = "script_plan_exist";

public static var date:Date;
private var program:Program;

[Bindable]
private var tabManager:TabManager = new TabManager();
[Bindable]
private var quickViewVisible:Boolean = false;
[Bindable]
private var selectedScript:Script = null;
[ArrayElementType("String")] var idsOfScripts:Array = new Array();
[ArrayElementType("tv.snews.anews.domain.Script")] var scripts:Array = new Array();
[Bindable]
private var menu:Menu = new Menu();
[Bindable]
private var showPreview:Boolean;
[Bindable]
private var textOld:String;
[Bindable]
[ArrayElementType("String")]
private var popUpButtonOptions:ArrayCollection;
[Bindable]
public var currentProgram:Program;
[Bindable]
private var shouldMove:Boolean = false;

[Bindable]
private var indexOfDroppedItem : int = 0;

[Bindable]
public var scriptPlan:ScriptPlan;
private var isCleaning:Boolean = false;

private const reportUtil:ReportsUtil = new ReportsUtil();

// -----------------------
// Events handler
// -----------------------

private function onContentCreationComplete(event:FlexEvent):void {
    // Pré-carregamento de lazies
    DomainCache.playoutDevices.length;
    DomainCache.mamDevices.length;
    DomainCache.reporters.length;

    if (DomainCache.settings.enableScriptPlanLoadButton && !program) {
        btnLoadScript.visible = btnLoadScript.includeInLayout = true;
    } else {
        btnLoadScript.visible = btnLoadScript.includeInLayout = false;

        cbPrograms.addEventListener("change", onScriptPlanListChange);
        dateScriptPlan.addEventListener("change", onScriptPlanListChange);
    }

    scriptPlanService.subscribe(onScriptPlanMessageReceived);
    scriptBlockService.subscribe(onScriptBlockMessageReceived);
    scriptService.subscribe(onScriptMessageReceived);
    documentService.subscribe(onDocumentMessageReceived);

    // Ajustes no drag & scroll da lista de blocos
//	this.scriptBlockListLayout.dragScrollInitialDelay = 0; // sem delay no início
    //	this.rundownListLayout.dragScrollRegionSizeHorizontal *= 10;
    //	this.rundownListLayout.dragScrollRegionSizeVertical *= 10;
//	this.scriptBlockListLayout.dragScrollSpeed *= 10; // aumenta a velocidade em 8 vezes

    this.addEventListener(CrudEvent.SELECT, onSelectedScript);
    this.addEventListener(CrudEvent.EDIT, onEditScript);
    this.addEventListener(CrudEvent.UPDATE, onUpdateEditEvent);

    tabManager.tabNavigator = mainNavigator;
    callLater(function ():void {
        tabManager.setClosePolicyForTab(0, SuperTab.CLOSE_NEVER)
    });
    mainNavigator.addEventListener(SuperTabEvent.TAB_CLOSE, onClosedTab, false, 0, true);
}

private function onScriptPlanListChange(event:Event):void {
    program = cbPrograms.selectedItem as Program;
    date = dateScriptPlan.selectedDate;

    if (!DomainCache.settings.enableScriptPlanLoadButton && (!date || !program)) {
        return;
    }

    if (DomainCache.settings.enableScriptPlanLoadButton && (!date || !program)) {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'document.msg.noProgramSelected'), Info.WARNING);
        return;
    }

    DomainCache.programCache.editors(program);
    DomainCache.programCache.imageEditors(program);


    selectedScript = null;
    currentProgram = null;

    closeChangeScriptPlanDate();
    onVerifyScriptPlan();

    if (scriptPlan) {
        hideCopyTo();
        showPreview = false;
    }

    // Evita problema com navegação com o teclado
    this.callLater(function ():void {
        stage.focus = null;
    });
}

private function onUpdateEditEvent(event:CrudEvent):void {
    if (event.entity is Program) {
        controlButton(); //recarrega menu por causa da mudança de GC.
    }
}

private function onRemove(event:FlexEvent):void {
    for each (var tab:Object in mainNavigator.getChildren()) {
        if (tab is ScriptForm) {
            var form:ScriptForm = tab as ScriptForm;
            form.onClose();
        }
    }

    scriptPlanService.unsubscribe(onScriptPlanMessageReceived);
    scriptBlockService.unsubscribe(onScriptBlockMessageReceived);
    scriptService.unsubscribe(onScriptMessageReceived);
    documentService.unsubscribe(onDocumentMessageReceived);
    this.removeEventListener(CrudEvent.UPDATE, onUpdateEditEvent);
}

private function onLoadScriptPlan(event:Event):void {
    loadScriptPlan(date, program);
}

private function onAddBlock(event:Event):void {
    btnBlock.enabled = false;

    scriptBlockService.create(scriptPlan.id,
            function (event:ResultEvent):void {
                btnBlock.enabled = true;
            },
            function (event:FaultEvent):void {
                btnBlock.enabled = true;
            }
    );
}

private function onShowPrintOptions():void {
    var window:ScriptPrintOptionsWindow = new ScriptPrintOptionsWindow();
    window.scriptPlan = scriptPlan;
    window.script = selectedScript;

    PopUpManager.addPopUp(window, DisplayObject(FlexGlobals.topLevelApplication), true);
    PopUpManager.centerPopUp(window);
}

private function onDeleteScripts(event:MouseEvent):void {
    Alert.show(bundle.getString('Bundle', 'scriptarea.msg.confirmMultipleDelete'), bundle.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onDeleteScriptsConfirm);
}

private function onDeleteScriptsConfirm(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        if (!verifySelectedBeforeChangeScripts()) {
            return;
        }

        btnDelete.enabled = false;
        scriptService.removeScripts(idsOfScripts, scriptPlan.id, function (event:ResultEvent):void {
            btnDelete.enabled = true;
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "scriptarea.msg.multipleDeleteSuccess"), Info.INFO);
        }, function (event:FaultEvent):void {
            btnDelete.enabled = true;
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "client.sendError"), Info.ERROR);
        });
    }
}

private function onSelectedScript(event:CrudEvent):void {
	selectedScript = event.entity as Script;
    showPreview = false;
}

private function onEditScript(event:CrudEvent):void {
    scriptService.isLockedForEdition(selectedScript.id, onCheckLockedSuccess);
}

private function focusFieldIn(event:FocusEvent):void {
    if (event.currentTarget is SpellTextInput) {
        textOld = SpellTextInput(event.currentTarget).text
    } else if (event.currentTarget is TextInput) {
        textOld = TextInput(event.currentTarget).text
    }
}

private function focusFieldOut(event:FocusEvent):void {
    var currentText:String = null;
    if (event.currentTarget is SpellTextInput) {
        currentText = SpellTextInput(event.currentTarget).text
    } else if (event.currentTarget is TextInput) {
        currentText = TextInput(event.currentTarget).text
    }
    if (currentText == null) {
        currentText = textOld as String;
        return;
    }

    if (currentText == textOld) {
        return;
    }

    var field:String = event.currentTarget.id;
    scriptPlanService.updateField(scriptPlan.id, field, currentText);
}

private function onReorder(event:Event = null):void {
    if (selectedScript && selectedScript.locked) {
        ANews.showInfo(bundle.getString('Bundle', "scriptarea.script.saveToDrawerDragging"), Info.WARNING);
    } else {
        scriptPlanService.sortScripts(scriptPlan.id);
    }
}

private function showScriptPreviwer(event:Event):void {
    scriptService.loadById(selectedScript.id, function (event2:ResultEvent):void {
        scriptPlanViewerScript.script = event2.result as Script;
        showPreview = true;
    });
}

//---------------------------------
//	Asynchronous Answers
//---------------------------------
private function onClosedTab(event:SuperTabEvent):void {
    this.dispatchEvent(new ANewsPlayEvent(ANewsPlayEvent.CLOSE));
    var tab:Object = mainNavigator.getChildAt(event.tabIndex);
    if (tab is ScriptForm) {
        event.preventDefault();
        var form:ScriptForm = tab as ScriptForm;
        form.onClose();
    }
}

private function onCheckLockedSuccess(event:ResultEvent):void {
    var user:User = event.result as User;

    if (selectedScript.displayed || scriptPlan.displayed) ANews.showInfo(bundle.getString("Bundle", "script.block.displayed"), Info.WARNING);
    else if (selectedScript.displaying) ANews.showInfo(bundle.getString("Bundle", "script.isDisplaying"), Info.WARNING);

    else if (user) {
        if (user.equals(ANews.USER)) {
            tabManager.openTab(ScriptForm, "script_" + selectedScript.id);
        } else if (ANews.USER.allow("01170910")) {
            Alert.show(bundle.getString('Bundle', 'script.msg.confirmAssumeEdition', [ user.name ]), bundle.getString('Bundle', 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null,
                    function (event:CloseEvent):void {
                        if (event.detail == Alert.YES) {
                            scriptService.assumeEdition(selectedScript.id, ANews.USER.id, function (event:ResultEvent):void {
                                scriptService.loadById(selectedScript.id, onLoadByIdSuccess);
                            });
                        }
                    }, null, Alert.NO);
        } else {
            ANews.showInfo(bundle.getString("Bundle", "scriptarea.script.lockedMessage", [user.nickname]), Info.WARNING);
        }
        return;
    } else {
        scriptService.loadById(selectedScript.id, onLoadByIdSuccess);
        scriptService.lockEdition(selectedScript.id, ANews.USER.id, null);
    }
}

private function onLoadByIdSuccess(event:ResultEvent):void {
    var script:Script = Script(event.result);
    var params:Object = { script: script, tabManager: tabManager };

    this.tabManager.openTab(ScriptForm, "script_" + script.id, params);
}

private function onDocumentMessageReceived(event:MessageEvent):void {
    var scriptPlanId:Number = event.message.headers["scriptPlanId"];
    if(scriptPlanId) {
        var action:String = event.message.headers["action"];
        var affectedScripts:Array = event.message.body as Array;

        switch (action) {
            case DocumentAction.SENT_TO_DRAWER: {

                if(scriptPlanId == scriptPlan.id){
                    scriptPlan.removeScripts(affectedScripts);
                }
                break;
            }
        }
    }
}

private function onScriptMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var script:Script = event.message.body as Script;
    var scripts:ArrayCollection = event.message.body as ArrayCollection;
    var localScript:Script = null;

    if (scriptPlan && (!script || !script.block || script.block.scriptPlan.equals(scriptPlan))) {
        switch (action) {
            case EntityAction.INSERTED:
                scriptPlan.addScript(script);

							// Deixa as coleções vazias para não afetar a performance
							script.clearCollections();
							break;
            case EntityAction.UPDATED:
                localScript = getScriptById(script.id);
                if (localScript) {
                    localScript.updateFields(script);

									// Deixa as coleções vazias para não afetar a performance
									localScript.clearCollections();
//                    localScript.reorderAndRenameVideos(bundle.getString("Bundle", "story.mosObjTitle"));
                }
                break;
            case EntityAction.FIELD_UPDATED:
                var scriptId:Number = event.message.headers["scriptId"];
                var field:String = event.message.headers["field"];
                var value:Object = event.message.headers["value"];

                localScript = getScriptById(scriptId);
                if (localScript) {
                    localScript[(field.toLowerCase() != "image_editor" ? field.toLowerCase() : "imageEditor")] = value;
                    if(field.toLowerCase() == 'displayed')
                        localScript.displaying = false;
                }
                break;
            case EntityAction.DELETED:
                scriptPlan.removeScript(script);
                if (script.equals(selectedScript)) {
                    showPreview = false;
                    selectedScript = null;
                }
                break;
			case EntityAction.MOVED:
//				var user:User = event.message.headers["user"] as User;
				var dragNDrop:Boolean = event.message.headers["dragNDrop"] as Boolean;
				
				if (/*user.equals(ANews.USER) &&*/ dragNDrop) {
					// O script já esta na posição correta para quem fez o drag n' drop, basta apenas reordernar os índices
					scriptPlan.reorder();
				} else {
					if (script) {
						scriptPlan.removeScript(script);
						scriptPlan.addScript(script);
						
						// Deixa as coleções vazias para não afetar a performance
						script.clearCollections();
					}
					if (scripts) {
						// Deve remover todos os scripts primeiro para depois inserir. Isso evita que sejam inseridos em posições erradas.
						
						for each (var currRemove:Script in scripts) {
							scriptPlan.removeScript(currRemove);
						}
						for each (var currAdd:Script in scripts) {
							scriptPlan.addScript(currAdd);
                            currAdd.clearCollections();
						}
                        for each (var block:ScriptBlock in scriptPlan.blocks) {
                            block.reorder();
                            block.calculateTimes();
                        }
                        scriptPlan.reorder();
                        scriptPlan.calculateTimes();
					}
				}
				
				deselectOthers(null);
				break;
            case EntityAction.UNLOCKED:
                localScript = getScriptById(script.id);
                if (localScript) {
                    localScript.editingUser = null;
                }
                break;
            case EntityAction.LOCKED:
                localScript = getScriptById(script.id);
                if (localScript) {
                    localScript.editingUser = event.message.headers["user"] as User;
                }
                break;
            case EntityAction.RESTORED:
                scriptPlan.addScript(script);

							// Deixa as coleções vazias para não afetar a performance
							script.clearCollections();
                break;
            case ScriptPlanAction.DRAWER:
                   // foi enviada para a gaveta
                scriptPlan.removeScript(script);
                if (script.equals(selectedScript)) {
                    showPreview = false;
                    selectedScript = null;
                }
                break;
            case EntityAction.MULTIPLE_EXCLUSION:
                var affectedScripts:Array = event.message.body as Array;
                var scriptPlanId:Number = Number(event.message.headers["scriptPlanId"]);
                if (scriptPlanId == scriptPlan.id) {
                    scriptPlan.removeScripts(affectedScripts);
                }
                break;
            default:
                throw new Error("You receive a undefined message for script, try to reload you scriptPlan!");
        }
    }
}

private function onVerifyScriptPlanSuccess(event:ResultEvent):void {
    if (event.result == null) {
        LoaderMask.close();
        return;
    }
    switch (event.result as String) {
        case ScriptPlanStatus.EXIST:
            loadScriptPlan(date, program);
            this.currentState = STATE_SCRIPT_PLAN_EXIST;
            break;
        case ScriptPlanStatus.NOT_EXIST:
            this.currentState = STATE_SCRIPT_PLAN_NOT_EXIST;
            LoaderMask.close();
            break;
        case ScriptPlanStatus.CANT_CREATE:
            this.currentState = STATE_SCRIPT_PLAN_CANT_CREATE;
            LoaderMask.close();
            break;
        default:
            throw new Error("There's something wrong ");
    }
}

private function onScriptPlanMessageReceived(event:MessageEvent):void {
	var action:String = event.message.headers["action"];
	var updatedPlan:ScriptPlan = event.message.body as ScriptPlan;
	var scriptPlanUpdated:ScriptPlan;

	//Caso tenha sido feita uma solicitação de mudança de data, o notificador notificará
	//para que a data do roteiro seja trocada
	switch (action) {
		case EntityAction.FIELD_UPDATED:
			var field:String = event.message.headers["field"];
			var value:Object = event.message.headers["value"];
			var planId:Number = event.message.headers["planId"] as Number;

			if (scriptPlan.id == planId) {
				if (field == "date") {
					dateScriptPlan.selectedDate = value as Date;
				} else if (field == "start" || field == "end" || field == "production") {
					this.timeControl[field].date = value as Date;
				} else {
					scriptPlan[field] = value;
				}
			}
			break;
		case ScriptPlanAction.SORTED:
			if (scriptPlan.equals(updatedPlan)) {
				sortPages();
			}
			break;
		case EntityAction.LOCKED:
			if (scriptPlan.equals(updatedPlan)) {
				scriptPlan.locked = true;
			}
			break;
		case EntityAction.UNLOCKED:
			if (scriptPlan.equals(updatedPlan)) {
				scriptPlan.locked = false;
			}
			break;
		case ScriptPlanAction.DISPLAY_RESETS:
			scriptPlanUpdated = event.message.body as ScriptPlan;
			if (scriptPlan.id == scriptPlanUpdated.id) {
				scriptPlan.displayed = false;
				resetDisplay();
			}
			break;
		case ScriptPlanAction.COMPLETE_DISPLAY:
			scriptPlanUpdated = event.message.body as ScriptPlan;
			if (scriptPlan.id == scriptPlanUpdated.id) {
				scriptPlan.displayed = true;
				completeDisplay();
			}
			break;
		default:
			//INSERTED, DELETED AND UPDATE are not supported on the scriptPlan.
			throw new Error("You receive an undefined message for a script, try to reload you script!");
	}
}

/**
 * Handler para o evento itemClick do menu do popUpButton.
 */
private function onMenuItemClick(event:MenuEvent):void {
    if (event.item == bundle.getString('Bundle', 'scriptarea.script.copy')) {
        copyScripts();
    }
    if (event.item == bundle.getString('Bundle', 'scriptarea.script.cut')) {
        cutScripts();
    }
    if (event.item == bundle.getString('Bundle', 'scriptarea.script.past')) {
        pastScripts(selectedScript.block);
    }
    if (event.item == bundle.getString('Bundle', 'scriptarea.script.copyTo')) {
        showCopyTo(false);
    }
    if (event.item == bundle.getString('Bundle', 'scriptarea.script.moveTo')) {
        showCopyTo(true);
    }
    else if (event.item == bundle.getString('Bundle', 'scriptarea.script.sendToDrawer') && !scriptPlan.displayed) {
        sendToDrawer();
    }
    else if (event.item == bundle.getString('Bundle', 'scriptarea.btn.printScript')) {
        print();
    }
    else if (event.item == bundle.getString('Bundle', 'scriptarea.btn.completeDisplay') && !scriptPlan.displayed) {
        onCompleteDisplay();
    }
    else if (event.item == bundle.getString('Bundle', 'scriptarea.btn.resetDisplay')) {
        onResetDisplay();
    }
    else if (event.item == bundle.getString('Bundle', 'scriptarea.btn.synchronizeStatus')) {
        onSynchronizeStatus();
    }
    else if (event.item == bundle.getString('Bundle', 'script.btn.alterDate') && !scriptPlan.displayed) {
        showChangeDateArea();
    }
    else if (event.item == bundle.getString('Bundle', 'scriptarea.btn.sendCredits') && !scriptPlan.displayed) {
        onSendCredit();
    }
    else if(selectedScript && selectedScript.displaying) ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.isDisplaying'), Info.WARNING);
    else if( scriptPlan.displayed || selectedScript && selectedScript.displayed) ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.block.displayed'), Info.WARNING);

}

private function onScriptBlockMessageReceived(event:MessageEvent):void {
    var action:String = event.message.headers["action"];
    var user:User = event.message.headers["user"] as User;
    var scriptBlock:ScriptBlock = event.message.body as ScriptBlock;
    var localBlock:ScriptBlock = null;

    if (scriptPlan && scriptPlan.equals(scriptBlock.scriptPlan)) {
        switch (action) {
            case EntityAction.INSERTED:
                scriptPlan.addBlock(scriptBlock);
                break;
            case EntityAction.DELETED:
                scriptPlan.removeBlock(scriptBlock);
                break;
            case EntityAction.UPDATED:
                scriptPlan.updateBlock(scriptBlock);
                break;
            case EntityAction.MOVED:
                if(user.id == ANews.USER.id) {
                    scriptPlan.updateBlock(scriptBlock);
                } else {
                    scriptPlan.removeBlock(scriptBlock);
                    scriptPlan.addBlock(scriptBlock);
                }
                break;
            default:
                throw new Error("You receive a undefined message for block, try to reload you script!");
        }
    }
}

private function onCreateScriptPlan(event:Event):void {
    scriptPlanService.verifyScriptPlan(date, program.id, function (event:ResultEvent):void {
        if (event.result == null)
            return;

        switch (event.result as String) {
            case ScriptPlanStatus.EXIST:
                onLoadScriptPlan(event);
                break;
            case ScriptPlanStatus.NOT_EXIST:
                createScriptPlan(date, program);
                break;
            case ScriptPlanStatus.CANT_CREATE:
                this.currentState = STATE_SCRIPT_PLAN_CANT_CREATE;
                break;
            default:
                throw new Error("There's something wrong ");
        }
    });
}

private function onSendCreditsSucess(event:ResultEvent):void {
    switch (event.result as String) {
        case "OK":
            ANews.showInfo(bundle.getString('Bundle', "defaults.msg.sentSuccessfully"), Info.INFO);
            break;
        case "NOT_SENT":
            ANews.showInfo(bundle.getString('Bundle', "defaults.msg.scriptPlanHasNoApprovedScripts"), Info.WARNING);
            break;
        case "CONTAINS_NON_APPROVED":
            ANews.showInfo(bundle.getString('Bundle', "defaults.msg.containsNonApproved"), Info.INFO);
            break;
        default:
            throw new Error("There's something wrong ");
    }
}

private function onSendCredit(event:Event = null):void {
    scriptPlanService.sendCredits(scriptPlan.id, onSendCreditsSucess);
}

private function onResetDisplay():void {
    if (scriptPlan != null) {
        Alert.show(bundle.getString('Bundle' , 'scriptarea.msg.storyDisplayReset'), bundle.getString('Bundle' , 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onResetDisplayConfirm);
    }
}

private function onResetDisplayConfirm(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        scriptPlanService.resetScriptPlanExhibition(scriptPlan.id, function(event:Event):void{
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "scriptarea.msg.displayResetsSuccess"), Info.INFO);
        });
    }
}

private function onCompleteDisplay():void {
    if (scriptPlan != null) {
        Alert.show(bundle.getString('Bundle' , 'scriptarea.msg.storyCompleteDisplay'), bundle.getString('Bundle' , 'defaults.msg.confirmation'), Alert.YES | Alert.NO, null, onCompleteDisplayConfirm);
    }
}

private function onCompleteDisplayConfirm(event:CloseEvent):void {
    if (event.detail == Alert.YES) {
        scriptPlanService.completeScriptPlanExhibition(scriptPlan.id, function(event:Event):void {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "scriptarea.msg.completeDisplaySuccess"), Info.INFO);
        });
    }
}

/**
 *    Efetua a cópia de um script selecionado no roteiro para
 *  um programa e data selecionados pelo usuário.
 */
private function onCopy(event:MouseEvent):void {
    selectedScriptsIds();
    var date:Date = destinyDay.selectedDate;
    var programId:int = (destinyProgram.selectedItem as Program).id;
    scriptPlanDividedBox.enabled = false;
    if (date && programId) {
        scriptPlanService.verifyScriptPlan(date, programId, function(event:ResultEvent):void {
            var resultCode:String = event.result as String;
            var rm:IResourceManager = ResourceManager.getInstance();
            switch (resultCode) {
                case ScriptPlanStatus.EXIST:
                    scriptService.copyTo(idsOfScripts, date, programId, shouldMove,
                            function(event:ResultEvent):void {
                                hideCopyTo();
                                ANews.showInfo(rm.getString("Bundle", "scriptarea.script.successCopy", [ shouldMove ? rm.getString("Bundle", "scriptarea.script.moved") : rm.getString("Bundle", "scriptarea.script.copied")]), Info.INFO);
                            }
                    );
                    break;
                default:
                    ANews.showInfo(rm.getString("Bundle", "script.msg.noScriptPlan"), Info.WARNING);
            }
        });
    } else {
        ANews.showInfo(bundle.getString("Bundle", "defaults.msg.requiredFields"), Info.WARNING);
    }
}

private function onCreationScriptPreviewer(event:FlexEvent):void {
    this.scriptPlanViewerScript.addEventListener(CrudEvent.EDIT, onEditScript);
    this.scriptPlanViewerScript.addEventListener(Event.CLOSE, hideScriptPreviwer);
}

public function controlButton():void {
    if (ANews.USER.allow("011711") && scriptPlan != null) {
        var cg:IIDevice = program.cgDevice && program.cgDevice.isII() ? program.cgDevice as IIDevice : null;
        if (cg != null && !cg.virtual) {
            if (!popUpButtonOptions.contains(bundle.getString('Bundle', 'scriptarea.btn.sendCredits'))) {
                popUpButtonOptions.addItemAt(bundle.getString('Bundle', 'scriptarea.btn.sendCredits'), 3);
            }
        } else if (popUpButtonOptions.contains(bundle.getString('Bundle', 'scriptarea.btn.sendCredits'))) {
            popUpButtonOptions.removeItemAt(popUpButtonOptions.getItemIndex(bundle.getString('Bundle', 'scriptarea.btn.sendCredits')));
        }
    }
}

private function initPopUpButtonMenu():void {
    popUpButtonOptions = new ArrayCollection();

    if (ANews.USER.allow("01170904")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.script.copy'));
    }
    if (ANews.USER.allow("01170904")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.script.cut'));
    }
    if (ANews.USER.allow("01170904")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.script.past'));
    }

    popUpButtonOptions.addItem({type: "separator"});

    if (ANews.USER.allow("01170904")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.script.copyTo'));
    }
    if (ANews.USER.allow("01170904")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.script.moveTo'));
    }

    if (ANews.USER.allow("011705")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.script.sendToDrawer'));
    }

    popUpButtonOptions.addItem({type: "separator"});

    if (ANews.USER.allow("01170907")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.btn.printScript'));
    }

    popUpButtonOptions.addItem({type: "separator"});

    if (ANews.USER.allow("01170307")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.btn.completeDisplay'));
    }

    if (ANews.USER.allow("01170306")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.btn.resetDisplay'));
    }

    popUpButtonOptions.addItem({type: "separator"});

    if (ANews.USER.allow("011712")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'scriptarea.btn.synchronizeStatus'));
    }

    if (ANews.USER.allow("011702")) {
        popUpButtonOptions.addItem(bundle.getString('Bundle', 'script.btn.alterDate'));
    }

    menu.dataProvider = popUpButtonOptions;
    menu.addEventListener(MenuEvent.ITEM_CLICK, onMenuItemClick);
}

private function onReceiveScriptPlanSuccess(event:ResultEvent):void {
    //cleanning  atual data on screen.
    if (scriptPlan != null && scriptPlan.blocks != null) {
        for each (var block:ScriptBlock in scriptPlan.blocks) {
            if (block.scripts) {
                block.scripts.removeAll();
                block.scripts = null;
            }
        }
        scriptPlan.blocks.removeAll();
        scriptPlan.blocks = null;
        scriptPlan = null;
    }

    this.currentState = STATE_SCRIPT_PLAN_EXIST;
    scriptPlan = event.result as ScriptPlan;
    scriptPlan.refreshGlobalIndexes();
    LoaderMask.close();
}

private function onReceiveScriptPlanFault(event:FaultEvent):void {
    LoaderMask.close();
}

// -----------------------
// Drag n' Drop events
// -----------------------

/*
 * No componente List do Flex, quando faço um drag n' drop com o Ctrl/Command
 * pressionado, ele copia o item, e não move. Esse método previne isso.
 */
private function avoidCopy(event:DragEvent):void {
    event.ctrlKey = false;
    event.action = DragManager.MOVE;
}

private function onDragOver(event:DragEvent):void {
    /*
     * NOTE Chamar o método preventDefault() causa um bug no scroll da lista. Isso
     * porque o método dragOverHandler() do componente List deixa de ser executado.
     *
     * Para evitar esse bug, copiei o código do método dragOverHandler() do
     * componente List e fiz as modificações necessárias.
     */

    // Não deixa executar o listener padrão para que possa exibir o feedback NONE

    var items:* = event.dragSource.dataForFormat("itemsByIndex");
    if (items is Vector.<Object> && Vector.<Object>(items).length > 0) {
        var draggedBlock:Object = items[0];
        if (draggedBlock is ScriptBlock) {
            indexOfDroppedItem = event.currentTarget.layout.calculateDropLocation(event).dropIndex;
            if (indexOfDroppedItem == 0 || draggedBlock.standBy || verifyDisplayingOrDisplayed((draggedBlock as ScriptBlock).order, indexOfDroppedItem)) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                DragManager.showFeedback(DragManager.NONE);
                return;
            }
        } else {
            event.preventDefault();

            // Trecho de código retirado de List.dragOverHandler()
            // Garante que o scroll continue funcionando
            var dropLocation:DropLocation = scriptBlockList.layout.calculateDropLocation(event);
            if (dropLocation) {
                scriptBlockList.drawFocusAnyway = true;
                scriptBlockList.drawFocus(true);

                scriptBlockList.layout.showDropIndicator(dropLocation);
            } else {
                scriptBlockList.layout.hideDropIndicator();

                scriptBlockList.drawFocus(false);
                scriptBlockList.drawFocusAnyway = false;
            }

            // Notifica que não aceita drop
            event.stopPropagation();
            event.stopImmediatePropagation();
            DragManager.showFeedback(DragManager.NONE);
            return;
        }
    }
}

private function onDragDrop(event:DragEvent):void {
    avoidCopy(event);
    var items:* = event.dragSource.dataForFormat("itemsByIndex");
    if (items is Vector.<Object> && Vector.<Object>(items).length > 0) {
        var dragItem:Object = items[0];
        if (!(dragItem is ScriptBlock)) {
            event.preventDefault();
            event.preventDefault();
            scriptBlockList.layout.hideDropIndicator();
            scriptBlockList.drawFocus(false);
            scriptBlockList.drawFocusAnyway = false;
            // Notifica que não aceita drop
            DragManager.showFeedback(DragManager.NONE);
        }
    }
}

private function onDragStart(event:DragEvent):void {
    planService.lockScriptPlan(scriptPlan.id);
}

private function onDragComplete(event:DragEvent):void {
    planService.unlockScriptPlan(scriptPlan.id);
    scriptBlockList.dragEnabled = false;
    scriptBlockList.dragMoveEnabled = false;
    event.preventDefault();
    event.stopPropagation();
    var items:* = event.dragSource.dataForFormat("itemsByIndex");
    var draggedBlock:ScriptBlock = items[0] as ScriptBlock;
    if(verifyDisplayingOrDisplayed(draggedBlock.order, indexOfDroppedItem)){
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.isDisplayingOrDisplayed'), Info.WARNING);
        return;
    }
    if (items is Vector.<Object> && Vector.<Object>(items).length > 0) {
        if (draggedBlock && indexOfDroppedItem > 0) {
            for each (var curr:ScriptBlock in scriptPlan.blocks) {
                if (curr.id == draggedBlock.id && curr.order != indexOfDroppedItem) {
                    scriptBlockService.updatePosition(draggedBlock.id, indexOfDroppedItem, true);
                    break;
                }
            }
        }
    }
}

private function verifyDisplayingOrDisplayed(oldOrder:int, newOrder:int):Boolean {

    /* Drag para baixo do bloco */
    if (oldOrder < newOrder) {
        /*
         * Fez um drag n' drop para baixo bloco. Neste caso os valores das ordens maiores que 'oldOrder'
         * e menores ou iguais a 'newOrder' devem ser decrementados.
         */
        for (var i:int = oldOrder + 1; i < newOrder; i++) {
            var curr:ScriptBlock = scriptPlan.blocks.getItemAt(i) as ScriptBlock;
            for each(var script:Script in curr.scripts) {
                if(script.displayed || script.displayed) {
                    return true;
                }
            }
        }
    }
    /* Drag para cima do bloco */
    if (oldOrder > newOrder) {
        /*
         * Fez um drag n' drop para cima do bloco. Neste caso os valores das ordens maiores ou iguais
         * a 'newOrder' e menores que 'oldOrder' devem ser incrementados.
         */
        for (var i:int = newOrder; i <= oldOrder - 1; i++) {
            var curr:ScriptBlock = scriptPlan.blocks.getItemAt(i) as ScriptBlock;
            for each(var script:Script in curr.scripts) {
                if(script.displaying || script.displayed) {
                    return true;
                }
            }
        }
    }
    return false;
}

// -----------------------
// Helpers
// -----------------------


public function allowedTabToClose():Boolean {
    for each (var tab:Object in mainNavigator.getChildren()) {
        if (tab is ScriptForm) {
            var form:ScriptForm = tab as ScriptForm;
            if (form.script.uploading) {
                ANews.showInfo(bundle.getString('Bundle', 'scriptarea.msg.ScriptAreaSendingImage'), Info.WARNING);
                return false;
            }
        }
    }
    return true;
}

private function showOrHideQuickview(toShow:Boolean):void {
    quickViewVisible = toShow;
    if(toShow){
        fadeShowParallel.end();
        fadeShowParallel.play();
    } else {
        fadeHideParallel.end();
        fadeHideParallel.play();
    }
    this.setFocus();
}

private function verifyScriptPlan(date:Date, program:Program):void {
    if (program != null && date != null) {
        LoaderMask.show(bundle.getString('Bundle', 'script.loading'));
        scriptPlanService.verifyScriptPlan(date, program.id, onVerifyScriptPlanSuccess);
    }
}

private function getScriptById(scriptId:int):Script {
    for each (var block:ScriptBlock in scriptPlan.blocks) {
        for each (var script:Script in block.scripts) {
            if (script.id == scriptId) {
                return script;
            }
        }
    }
    return null;
}

private function hideScriptPreviwer(event:Event):void {
    showPreview = false;
}

private function sendToDrawer():void {
    if (!verifySelectedBeforeChangeScripts()) {
        return;
    }

    documentService.archiveScriptsToDrawer(idsOfScripts, scriptPlan.id, true, function (event:ResultEvent):void {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "scriptarea.script.saveToDrawer"), Info.INFO);
    });
}

public function verifySelectedScripts():Boolean {
    selectedScripts();
    if (scripts.length == 0) {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "rundown.story.noSelectWarning"), Info.WARNING);
        return false;
    }
    return true;
}

public function verifySelectedBeforeChangeScripts():Boolean {
    selectedScripts();
    if (scripts.length == 0) {
        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "rundown.story.noSelectWarning"), Info.WARNING);
        return false;
    }
    idsOfScripts = new Array();
    var index:int = 0;
    for each(var script:Script in scripts) {
        if (script.locked) {
            ANews.showInfo(bundle.getString('Bundle', "scriptarea.script.theScriptLocked", [script.slug, script.editingUser.nickname]), Info.WARNING);
            return false;
        }
        if (script.displaying) {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.isDisplaying'), Info.WARNING);
            return false;
        }
        if (script.displayed) {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.block.displayed'), Info.WARNING);
            return false;
        }
        if (script.editingUser != null) {
            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.isEditing', [script.editingUser.nickname]), Info.WARNING);
            return false;
        }
        idsOfScripts[index] = script.id.toString();
        index++;
    }
    return true;
}

public function print():void {
	if (verifySelectedScripts()) {
        var scriptsIdsList:String = "";
        for each(var script:Script in scripts) {
            scriptsIdsList = scriptsIdsList + script.id + "|";
        }
		reportUtil.generate(ReportsUtil.SCRIPT, { ids: scriptsIdsList });
	}
}

private function showCopyTo(move:Boolean):void {
    if (verifySelectedScripts()) {
        shouldMove = move;
        sendTo.visible = true;
        sendTo.includeInLayout = true;
        scriptPlanDividedBox.enabled = false;
    }
}

public function selectedScripts():void {
    scripts = new Array();
    for each (var renderer:ScriptBlockRenderer in blockItemRenderers()) {
        var selectedItems:Vector.<Object> = renderer.selectedItems;
        if (selectedItems) {
            for each (var script:Script in selectedItems) {
                scripts.push(script);
            }
        }
    }
}

private function hideCopyTo():void {
    sendTo.visible = false;
    sendTo.includeInLayout = false;
    scriptPlanDividedBox.enabled = true;
}

// -----------------------
// Actions on ScriptPlan
// -----------------------

private function onVerifyScriptPlan(event:Event = null):void {
    verifyScriptPlan(date, program);
}

private function createScriptPlan(date:Date, program:Program):void {
    if (program != null && date != null) {
        scriptPlanService.create(date, program.id, onReceiveScriptPlanSuccess);
    }
}

private function onCancelScriptPlan(event:Event):void {
    cbPrograms.selectedIndex = -1;
    this.currentState = STATE_NORMAL;
}

private function loadScriptPlan(date:Date, program:Program):void {
    if (program != null && date != null) {
        scriptPlanService.load(date, program.id, onReceiveScriptPlanSuccess, onReceiveScriptPlanFault);
    }
}

private function getBlockById(blockId:int):ScriptBlock {
    if (scriptPlan) {
        for each (var block:ScriptBlock in scriptPlan.blocks) {
            if (block.id == blockId) {
                return block;
            }
        }
    }
    return null;
}

private function showChangeDateArea():void {
    groupScriptPlanDate.includeInLayout = groupScriptPlanDate.visible = true;
}

private function confirmChangeScriptPlanDate():void {
    scriptPlanService.verifyScriptPlan(dtScriptPlanAlterDate.selectedDate, (cbPrograms.selectedItem as Program).id, function (event:ResultEvent):void {
        switch (event.result as String) {
            case ScriptPlanStatus.EXIST:
                ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "script.alreadyExist"), Info.WARNING);
                break;
            case ScriptPlanStatus.NOT_EXIST:
                scriptPlanService.updateField(scriptPlan.id, "date", dtScriptPlanAlterDate.selectedDate as Date, function (event:ResultEvent):void {
                    dateScriptPlan.selectedDate = dtScriptPlanAlterDate.selectedDate as Date;
                    ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "script.changeDateSuccess"), Info.INFO);
                    closeChangeScriptPlanDate();
                    onScriptPlanListChange(event);
                });
                break;
            case ScriptPlanStatus.CANT_CREATE:
                ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "script.cantCreate"), Info.WARNING);
                break;
            default:
                throw new Error("There's something wrong ");
        }
    });
}

private function closeChangeScriptPlanDate():void {
    groupScriptPlanDate.visible = groupScriptPlanDate.includeInLayout = false;
}

/**
 * Reorganize page as ordered by other users.
 */
private function sortPages():void {
    var order:int = 0;
    for each (var block:ScriptBlock in scriptPlan.blocks) {
        for each (var script:Script in block.scripts) {
            script.page = ++order < 10 ? "0" + order : "" + order;
        }
    }
}

/**
 * Reset display as ordered by other users.
 */
public function resetDisplay():void {
    for each (var block:ScriptBlock in scriptPlan.blocks) {
        for each (var script:Script in block.scripts) {
            script.displayed = false;
            script.displaying = false;
        }
    }
}

/**
 * Complet display as ordered by other users.
 */
public function completeDisplay():void {
    for each (var block:ScriptBlock in scriptPlan.blocks) {
        for each (var script:Script in block.scripts) {
            if(script.displaying){
                script.displayed = true;
                script.displaying = false;
            }
            script.editingUser = null;
        }
    }
}

private function onSynchronizeStatus():void {
    if (scriptPlan != null) {
        scriptPlanService.synchronizeStatusVideosWS(scriptPlan.id, onSynchronizeVideosWsSuccess);
    }
}

private function onSynchronizeVideosWsSuccess(event:ResultEvent):void {
    ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "scriptarea.msg.synchronizedStatus"), Info.INFO);
}

//------------------------------
//
//------------------------------

public function deselectOthers(current:ScriptBlockRenderer):void {
	for each (var renderer:ScriptBlockRenderer in blockItemRenderers()) {
		if (renderer != current) {
			renderer.deselect();
		}
	}
}

private function blockItemRenderers():Vector.<ScriptBlockRenderer> {
	var result:Vector.<ScriptBlockRenderer> = new Vector.<ScriptBlockRenderer>();

	var dataGroup:DataGroup = scriptBlockList.dataGroup;
	for (var index:int = 0; index < dataGroup.numElements; index++) {
		var renderer:ScriptBlockRenderer = dataGroup.getElementAt(index) as ScriptBlockRenderer;
		if (renderer) {
			result.push(renderer);
		}
	}
	return result;
}

public function addDragSourceHandler(dragSource:DragSource):void {
	dragSource.addHandler(copySelectedItemsForDragDrop, "itemsByIndex");
}

protected function onKeyDown(event:KeyboardEvent):void {
    if ((event.target is List || event.target is LabelWithFocus) && (event.ctrlKey || event.shiftKey)) {
        if (event.keyCode == Keyboard.X) {
            cutScripts();
        } else if (event.keyCode == Keyboard.V) {
            pastScripts();
        } else if (event.keyCode == Keyboard.C) {
            copyScripts();
        }
    } else if (event.keyCode == Keyboard.ESCAPE) {
        clearClipboard();
    }
}

public function selectedScriptsIds():void {
    idsOfScripts = new Array();
    var index:int = 0;
    for each (var renderer:ScriptBlockRenderer in blockItemRenderers()) {
        var selectedItems:Vector.<Object> = renderer.selectedItems;
        if (selectedItems) {
            for each (var script:Script in selectedItems) {
                idsOfScripts[index] = script.id.toString();
                index++;
            }
        }
    }
}

public function copyScripts():void {
    if (verifySelectedScripts()) {
        sendScriptsToClipboard(ClipboardAction.COPY);
    }
}

public function cutScripts():void {
    if (verifySelectedScripts()) {
        sendScriptsToClipboard(ClipboardAction.CUT);
    }
}

private function sendScriptsToClipboard(action:String):void {
    selectedScriptsIds();
    scriptService.sendScriptsToClipboard(action, idsOfScripts, function(event:ResultEvent):void {
        var clipboardIds:ArrayCollection =  new ArrayCollection(event.result as Array);

        for each (var block:ScriptBlock in scriptPlan.blocks) {
            for (var i:int = block.scripts.length - 1; i >= 0; i--) {
                var _script:Script = block.scripts.getItemAt(i) as Script;
                _script.cutted = false;
                _script.copied = false;

                if (clipboardIds.contains(_script.id)) {
                    if(action == ClipboardAction.COPY)
                        _script.copied = true;
                    else{
                        if (_script.displaying || _script.displayed)
                            ANews.showInfo(ResourceManager.getInstance().getString('Bundle', 'script.block.cantCutDisplayed', [ StringUtils.isNullOrEmpty(_script.slug) ?
                                    ResourceManager.getInstance().getString('Bundle', 'defaults.noSlug') : _script.slug ]), Info.WARNING);

                        // Mesmo que o script esteja em exibido ou em exibição, ele deve estar marcado para controle do usuário
                        _script.cutted = true;
                    }
                }
            }
        }
    });
}

public function pastScripts(block:ScriptBlock=null):void {
    if (selectedScript == null && block == null) {
        for each (var blockTmp:ScriptBlock in scriptPlan.blocks) {
            if (blockTmp.standBy) {
                past(blockTmp.id, -1 /*significa que vai colar/recortar no final do bloco */);
                return;
            }
        }
    }

    if (block) {
        if (selectedScript && selectedScript.block.id == block.id) {
            past(selectedScript.block.id , (selectedScript.order+1));
        } else {
            past(block.id, -1 /*significa que vai colar/recortar no final do bloco */);
        }
    } else if (selectedScript) {
        past(selectedScript.block.id , (selectedScript.order+1));
    }
}

private function past(blockId:Number, pastIndex:int):void {
    scriptService.pastScriptsFromClipboard(blockId, pastIndex , function(event:ResultEvent):void {
        removeMarkupsClipboard();
    });
}

public function clearClipboard():void {
    if (!isCleaning) {
        isCleaning = true;
        scriptService.clearClipboard(function(event:ResultEvent):void{
            removeMarkupsClipboard();
            isCleaning = false;
        },	function(event:FaultEvent):void{
            isCleaning = false;
        });
    }
}

private function removeMarkupsClipboard():void{
    for each (var block:ScriptBlock in scriptPlan.blocks) {
        for (var i:int = block.scripts.length - 1; i >= 0; i--) {
            var _script:Script = block.scripts.getItemAt(i) as Script;

            _script.cutted = false;
            _script.copied = false;
        }
    }

    deselectAll();
    selectedScript = null;
}

public function deselectAll():void {
    for each (var renderer:ScriptBlockRenderer in blockItemRenderers()) {
        renderer.deselect();
    }
}

private function copySelectedItemsForDragDrop():Vector.<Object> {
	var length:int = 0;

	for each (var renderer:ScriptBlockRenderer in blockItemRenderers()) {
		length += renderer.selectedIndices.length;
	}

	var result:Vector.<Object> = new Vector.<Object>(length);
	var filled:int = 0;

	for each (renderer in blockItemRenderers()) {
		// Cria uma cópia para não modificar o original
		var draggedIndices:Vector.<int> = renderer.selectedIndices.slice(0, renderer.selectedIndices.length);
		draggedIndices.sort(compareValues);

		var count:int = draggedIndices.length;
		for (var i:int = 0; i < count; i++) {
			result[filled + i] = renderer.getItemAt(draggedIndices[i]);
		}
		filled += count;
	}

	return result;
}

private function compareValues(a:int, b:int):int {
	return a - b;
}

public function removeOthersSelectedItems(current:ScriptBlockRenderer):void {
	for each (var renderer:ScriptBlockRenderer in blockItemRenderers()) {
		if (renderer != current) {
			renderer.removeSelectedItems();
		}
	}
}
