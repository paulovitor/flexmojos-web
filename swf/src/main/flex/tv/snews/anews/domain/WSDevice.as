package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.WSDevice")]
public class WSDevice extends Device {

	public var address:String;
	public var username:String;
	public var password:String;

	public function WSDevice() {
		super();
		super.protocol = DeviceProtocol.WS;
	}

	//------------------------------
	//	Operations
	//------------------------------

	override public function update(device:Device):void {
		super.update(device);

		if (device is WSDevice) {
			var other:WSDevice = WSDevice(device);
			this.address = other.address;
			this.username = other.username;
			this.password = other.password;
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	override public function set protocol(value:String):void {
		super.protocol = DeviceProtocol.WS;
	}
}
}
