package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Entidade que representa os grupos de acesso ao ANews.
	 * 
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.Group")]
	public class UserGroup extends AbstractEntity_Int {

		private var _name:String;
		private var _showAsReporters:Boolean;
		private var _showAsProducers:Boolean;
		private var _showAsEditors:Boolean;
        private var _showAsChecklistProducers:Boolean;

		[ArrayElementType("tv.snews.anews.domain.Permission")]
		private var _permissions:ArrayCollection = new ArrayCollection();

		[Transient]
		private var _users:ArrayCollection = new ArrayCollection();
		
		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function UserGroup() {
			super(this); // Just to keep the abstraction.
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		public function get showAsEditors():Boolean
		{
			return _showAsEditors;
		}

		public function set showAsEditors(value:Boolean):void
		{
			_showAsEditors = value;
		}

		/**
		 * Copia todos os valores do grupo informado pelo parâmetro para este
		 * grupo.
		 * 
		 * @param other Grupo do qual serão copiados os atributos.
		 */
		public function copy(other:UserGroup):void {
			this.id = other.id;
			this.name = other.name;
			this.showAsReporters  = other.showAsReporters;
			this.showAsProducers  = other.showAsProducers;
			this.showAsEditors  = other.showAsEditors;
            this.showAsChecklistProducers = other.showAsChecklistProducers;
			this.permissions = other.permissions;
			//this.users = other.users; Quando vem do java, esta lista vem fazia.
		}
		
		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function set name(value:String):void {
			_name = value;
		}

		public function get name():String {
			return _name;
		}

		public function set permissions(value:ArrayCollection):void {
			_permissions = value;
		}
		
		public function get permissions():ArrayCollection {
			return _permissions;
		}
		
		public function get users():ArrayCollection {
			return _users;
		}
		
		public function set users(value:ArrayCollection):void {
			_users = value;
		}

		public function get showAsReporters():Boolean {
			return _showAsReporters;
		}

		public function set showAsReporters(value:Boolean):void {
			_showAsReporters = value;
		}
		
		public function get showAsProducers():Boolean {
			return _showAsProducers;
		}
		
		public function set showAsProducers(value:Boolean):void	{
			_showAsProducers = value;
		}

        public function get showAsChecklistProducers():Boolean {
            return _showAsChecklistProducers;
        }

        public function set showAsChecklistProducers(value:Boolean):void {
            _showAsChecklistProducers = value;
        }
    }
}