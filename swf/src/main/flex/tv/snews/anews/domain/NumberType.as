package tv.snews.anews.domain {

	/**
	 * @author Samuel Guedes
	 * @author Paulo Felipe
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.NumberType")]
	public class NumberType extends AbstractEntity_Int {

		private var _description:String;

		public function NumberType() {
			super(this); //Just to keep the abstraction
		}

		public function get description():String {
			return _description;
		}

		public function set description(value:String):void {
			_description = value;
		}

	}
}
