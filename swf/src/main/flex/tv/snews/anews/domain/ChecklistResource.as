package tv.snews.anews.domain {

	/**
	 * @author Samuel Guedes de Melo.
	 * @since 1.5
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.ChecklistResource")]
	public class ChecklistResource extends AbstractEntity_Int {
		
		private var _name:String = ""; // não tire essa iniciação
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function ChecklistResource() {
			super(this);
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		public function copy(checklistResource:ChecklistResource):void {
			this.name = checklistResource.name;
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
	}
}