package tv.snews.anews.domain {

/**
 * Classe enum que define as coleções possíveis para a lauda.
 *
 * @author Paulo Felipe
 * @author Samuel Guedes de Melo
 * @since 1.3.0
 */
public class FetchType {

    public static const CG:String = "CG";
    public static const EDITORS:String = "EDITORS";
    public static const IMAGE_EDITORS:String = "IMAGE_EDITORS";
    public static const PRESENTERS:String = "PRESENTERS";

}
}