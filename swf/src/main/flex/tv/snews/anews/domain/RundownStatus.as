package tv.snews.anews.domain {

	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe enum que define os possíveis status do Espelho.
	 *
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class RundownStatus {
		/**
		 * Marca que o espelho ja foi criado anteriormente no sistema. 
		 */ 
		public static const EXIST:String = "EXIST";
		
		/**
		 * Marca que o espelho não existe no sistema.
		 */ 
		public static const NOT_EXIST:String = "NOT_EXIST";
		
		/**
		 * Marca que o espelho não pode ser criado.
		 */ 
		public static const CANT_CREATE:String = "CANT_CREATE";
		
	}
}
