package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class DeviceProtocol {

	private static const bundle:IResourceManager = ResourceManager.getInstance();

	public static const II:String = "II";
	public static const MOS:String = "MOS";
	public static const WS:String = "WS";
	public static const NONE:String = "NONE";

	public static const values:ArrayCollection = new ArrayCollection([
		{ id: II, label: "Intelligent Interface" },
		{ id: MOS, label: "MOS Protocol" },
		{ id: WS, label: "Web Service" },
		{ id: NONE, label: bundle.getString("Bundle", "device.protocol.none") }
	]);

	public static function i18n(value:String):String {
		return entry(value).label;
	}

	public static function entry(value:String):Object {
		for each (var entry:Object in values) {
			if (entry.id == value) {
				return entry;
			}
		}
		throw new Error("Entry not found for " + value);
	}
}
}
