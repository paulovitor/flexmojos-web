package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * @author Samuel Guedes de Melo.
 * @author Felipe Zap de Mello
 * @since 1.2.6
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.StoryCG")]
public class StoryCG extends StorySubSection {

	public var code:int;
	public var templateCode:int;
	public var comment:Boolean = false;
	public var disabled:Boolean = false;

	[ArrayElementType("tv.snews.anews.domain.StoryCGField")]
	public var fields:ArrayCollection = new ArrayCollection();

	public function StoryCG(template:IITemplate = null) {
		if (template) {
			this.templateCode = template.templateCode;
			this.comment = template.comment;

			for each (var templateField:IITemplateField in template.fields) {
				fields.addItem(new StoryCGField(this, templateField));
			}
		}
	}

	/**
	 * Verifica se o objeto foi alterado.
	 *
	 * @return retorna true se os objetos forem diferentes.
	 */
	override public function isDifferent(other:StorySubSection):Boolean {
		if (super.isDifferent(other)) return true;

		const otherCg:StoryCG = StoryCG(other);
		if (templateCode != otherCg.templateCode ||
                code != otherCg.code ||
                comment != otherCg.comment ||
                fields.length != otherCg.fields.length) {
			return true;
		}

        // Verifica mudança nos fields
        if (fields.length != otherCg.fields.length) {
            return true;
        }

		// Mesmo template indica que possuem a mesma quantidade de fields. Tem que verificar apenas os valores.
		for (var i:int = 0; i < fields.length; i++) {
			var thisField:StoryCGField = StoryCGField(fields.getItemAt(i));
			var otherField:StoryCGField = StoryCGField(otherCg.fields.getItemAt(i));
			if (thisField.isDifferent(otherField)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Limpa o ID desse crédito e de seus fields para que sejam cadastrados novamente.
	 */
	public function resetIDs():void {
		this.id = undefined;
		for each (var field:StoryCGField in fields) {
			field.id = undefined;
		}
	}
}
}
