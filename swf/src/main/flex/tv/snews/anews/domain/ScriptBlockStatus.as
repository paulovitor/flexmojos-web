package tv.snews.anews.domain {

	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe enum que define os possíveis status do Roteiro.
	 *
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	public class ScriptBlockStatus {
		/**
		 * Marca que o roteiro, juntamente com o bloco de apoio existem
		 */ 
		public static const SUCCESSFUL_SYNCHRONIZATION:String = "SUCCESSFUL_SYNCHRONIZATION";
		
		/**
		 * Marca que o roteiro não existe no sistema.
		 */ 
		public static const SCRIPT_PLAN_NOT_FOUND:String = "SCRIPT_PLAN_NOT_FOUND";

		
	}
}
