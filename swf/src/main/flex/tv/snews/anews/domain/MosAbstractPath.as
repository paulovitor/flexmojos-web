package tv.snews.anews.domain {

	/**
	 * Contem os atributos e comportamentos comuns a todos os tipos de objetos 
	 * de path de um objeto vindo do MOS.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.3
	 */
	[Bindable]
	public class MosAbstractPath extends AbstractEntity_Number {
		
		protected var _techDescription:String;
		protected var _url:String;
		
		//------------------------------
		//	Constructors
		//------------------------------
		
		public function MosAbstractPath() {
			super(this); // Just to keep the abstraction
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		public function copy(path:MosAbstractPath):void {
			this.id = path.id; // necessário para o insert
			this.techDescription = path.techDescription;
			this.url = path.url;
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get techDescription():String {
			return _techDescription;
		}
		
		public function set techDescription(value:String):void {
			_techDescription = value;
		}
		
		public function get url():String {
			return _url;
		}
		
		public function set url(value:String):void {
			_url = value;
		}
	}
}