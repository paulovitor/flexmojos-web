package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.6
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ReportageCG")]
public class ReportageCG extends AbstractEntity_Number {

	public var order:int = 0;
	public var code:int;
	public var templateCode:int;
	public var comment:Boolean = false;

	[ArrayElementType("tv.snews.anews.domain.ReportageCGField")]
	public var fields:ArrayCollection = new ArrayCollection();

	public var reportage:Reportage;

	//------------------------------
	//	Constructor
	//------------------------------

	public function ReportageCG(template:IITemplate=null) {
		super(this);

		if (template) {
			this.templateCode = template.templateCode;
			this.comment = template.comment;

			for each (var templateField:IITemplateField in template.fields) {
				fields.addItem(new ReportageCGField(this, templateField));
			}
		}
	}

	/**
	 * Verifica se o objeto foi alterado.
	 *
	 * @return retorna true se os objetos forem diferentes.
	 */
	public function isDifferent(other:ReportageCG):Boolean {
		if (templateCode != other.templateCode || id != other.id || code != other.code || comment != other.comment) {
			return true;
		}

        // Verifica mudança nos fields
        if (fields.length != other.fields.length) {
            return true;
        }

		// Mesmo template indica que possuem a mesma quantidade de fields. Tem que verificar apenas os valores.
		for (var i:int = 0; i < fields.length; i++) {
			var thisField:ReportageCGField = ReportageCGField(fields.getItemAt(i));
			var otherField:ReportageCGField = ReportageCGField(other.fields.getItemAt(i));

			if (thisField.number != otherField.number
			   || thisField.name != otherField.name
				|| thisField.value != otherField.value) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Limpa o ID desse crédito e de seus fields para que sejam cadastrados novamente.
	 */
	public function resetIDs():void {
		this.id = undefined;
		for each (var field:ReportageCGField in fields) {
			field.id = undefined;
		}
	}
}
}
