package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import tv.snews.anews.event.UploadEvent;
import tv.snews.anews.utils.Util;

import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Script")]
public class Script extends Document {

	public static const IMAGES_LIMIT:int = 10;

    private static const dateUtil:DateUtil = new DateUtil();

    //------------------------------
    //	Persistent Properties
    //------------------------------

    public var order:int;

    public var page:String;
    public var tape:String;
    public var synopsis:String;
    public var mosStatus:String;

    public var vt:Date;

    public var ok:Boolean;
    public var approved:Boolean;
    public var blank:Boolean;
	public var hasVideos:Boolean;

    public var displaying:Boolean;
    public var displayed:Boolean;

    public var editor:User;
    public var imageEditor:User;
    public var reporter:User;
    public var presenter:User;

    private var _block:ScriptBlock;
    private var _guideline:Guideline;
    private var _reportage:Reportage;
    public var sourceScript:Script;

    [ArrayElementType("tv.snews.anews.domain.ScriptImage")]
    public var images:ArrayCollection = new ArrayCollection();

    [ArrayElementType("tv.snews.anews.domain.ScriptVideo")]
    public var videos:ArrayCollection = new ArrayCollection();

    [ArrayElementType("tv.snews.anews.domain.AbstractScriptItem")]
    public var cgs:ArrayCollection = new ArrayCollection();

    //------------------------------
    //	Transient Properties
    //------------------------------

    [Transient] public var total:Date;
    [Transient] public var locked:Boolean = false;
    [Transient] public var editingUser:User;
    [Transient] public var hasGuideline:Boolean = false;
    [Transient] public var hasReportage:Boolean = false;
    [Transient] public var globalIndex:int;
	[Transient] public var uploading:Boolean = false;
    [Transient] public var expanderCollapsed:Boolean = true;
	[Transient] private var _uploadCount:int = 0;
    [Transient] public var isDragging:Boolean = false;
    [Transient] public var cutted:Boolean = false;
    [Transient] public var copied:Boolean = false;

    public function Script() {
        super();
    }

    public function isDifferent(other:Script):Boolean {
        return hasDiff(other) ||
                (this.tape != other.tape) ||
                hasDifferencesInPresenter(other.presenter) ||
                hasDifferencesInReporter(other.reporter) ||
                (dateUtil.timeToString(this.vt) != dateUtil.timeToString(other.vt)) ||
                (this.synopsis != other.synopsis) ||
                hasDifferencesInImages(other.images) ||
                hasDifferencesInVideos(other.videos) ||
                hasDifferencesInCGs(other.cgs);
    }

    private function hasDifferencesInPresenter(otherPresenter:User):Boolean {
        return this.presenter == null && otherPresenter != null ||
                this.presenter != null && otherPresenter == null ||
                this.presenter != null && otherPresenter != null && !this.presenter.equals(otherPresenter);
    }

    private function hasDifferencesInReporter(otherReporter:User):Boolean {
        return this.reporter == null && otherReporter != null ||
                this.reporter != null && otherReporter == null ||
                this.reporter != null && otherReporter != null && !this.reporter.equals(otherReporter);
    }

    private function hasDifferencesInImages(otherImages:ArrayCollection):Boolean {
        if (Util.hasDifferencesInCollections(this.images, otherImages)) {
            return true;
        }

        for (var i:int = 0; i < this.images.length; i++) {
            var thisScriptImage:ScriptImage = ScriptImage(this.images.getItemAt(i));
            var otherScriptImage:ScriptImage = ScriptImage(otherImages.getItemAt(i));
            if (thisScriptImage.isDifferent(otherScriptImage)) {
                return true;
            }
        }
        return false;
    }

    private function hasDifferencesInVideos(otherVideos:ArrayCollection):Boolean {
        if (Util.hasDifferencesInCollections(this.videos, otherVideos)) {
            return true;
        }

        for (var index:int = 0; index < this.videos.length; index++) {
            var video:ScriptVideo = ScriptVideo(this.videos.getItemAt(index));
            var otherVideo:ScriptVideo = ScriptVideo(otherVideos.getItemAt(index));
            if (video.isDifferent(otherVideo)) {
                return true;
            }
        }
        return false;
    }

    private function hasDifferencesInCGs(otherCGs:ArrayCollection):Boolean {
        if (Util.hasDifferencesInCollections(this.cgs, otherCGs)) {
            return true;
        }

        for (var index:int = 0; index < this.cgs.length; index++) {
            if (this.cgs.getItemAt(index) is ScriptCG) {
                var cg:ScriptCG = ScriptCG(this.cgs.getItemAt(index));
                var otherCG:ScriptCG = ScriptCG(otherCGs.getItemAt(index));
                if (cg.isDifferent(otherCG)) {
                    return true;
                }
            } else if (this.cgs.getItemAt(index) is ScriptMosCredit) {
                var cgMos:ScriptMosCredit = ScriptMosCredit(this.cgs.getItemAt(index));
                var otherCGMos:ScriptMosCredit = ScriptMosCredit(otherCGs.getItemAt(index));
                if (cgMos.isDifferent(otherCGMos)) {
                    return true;
                }
            }
        }
        return false;
    }

    override public function updateFields(other:AbstractVersionedEntity):void {
        if (other is Script) {
            var otherScript:Script = other as Script;

            this.copyData(otherScript);

            this.order = otherScript.order;
            this.page = otherScript.page;
            this.tape = otherScript.tape;
            this.synopsis = otherScript.synopsis;
            this.mosStatus = otherScript.mosStatus;

            this.vt = otherScript.vt;

            this.ok = otherScript.ok;
            this.approved = otherScript.approved;
            this.blank = otherScript.blank;
			this.hasVideos = otherScript.hasVideos;
			
            this.editor = otherScript.editor;
            this.imageEditor = otherScript.imageEditor;
            this.reporter = otherScript.reporter;
            this.presenter = otherScript.presenter;

            this.guideline = otherScript.guideline;
            this.reportage = otherScript.reportage;

            this.images = otherScript.images;
            this.videos = otherScript.videos;
            this.cgs = otherScript.cgs;
        }
    }

    public function updateVTTime():Date {
        var duration:Date = dateUtil.timeZero();

        for each (var video:ScriptVideo in this.videos) {
            var dateWithoutMillis:Date = dateUtil.stringToTime(dateUtil.timeToString(video.durationTime));
            duration = dateUtil.sumTimes(duration, dateWithoutMillis);
        }

        this.vt = duration;

        return duration;
    }

    public function existMediaInScriptVideos(media:Media):Boolean {
        for each (var video:ScriptVideo in this.videos) {
            if (video.media != null && !media.isDifferent(video.media))
                return true;
        }
        return false;
    }

    // ------------------
    // Helpers
    // ------------------

	public function clearCollections():void {
		this.cgs = null;
		this.images = null;
		if(this.expanderCollapsed) this.videos = null;
	}

    public function reorderAndRenameVideos(title:String = ""):void {
        var index:int = 0;
        var mosCount:int = 0;
        for each (var video:ScriptVideo in videos) {
            video.order = index;
            index++;
            video.label = Util.formatName(title, ++mosCount);
        }
    }

    public function reorderCGs():void {
        for (var index:int = 0; index < this.cgs.length ; index++) {
            this.cgs.getItemAt(index).order = index;
        }
    }

	public function addVideo(video:ScriptVideo):void {
		video.script = this;
		video.order = videos.length;

		if (program.playoutDevice is MosDevice) {
			var playout:MosDevice = MosDevice(program.playoutDevice);
			video.channel = playout.defaultChannel;
		}

		videos.addItem(video);
	}

    public function addCG(cg:AbstractScriptItem):void {
        cg.script = this;
        cg.order = this.cgs.length;
        this.cgs.addItem(cg);
    }

	public function addImage(image:ScriptImage):void {
		image.script = this;
		image.order = images.length;
		images.addItem(image);

		image.addEventListener(UploadEvent.INIT, onUploadInit);
		image.addEventListener(UploadEvent.CANCEL, onUploadCancel);
		image.addEventListener(UploadEvent.COMPLETE, onUploadComplete);
	}

	public function removeImage(image:ScriptImage):void {
		images.removeItemAt(images.getItemIndex(image));

		image.removeEventListener(UploadEvent.INIT, onUploadInit);
		image.removeEventListener(UploadEvent.CANCEL, onUploadCancel);
		image.removeEventListener(UploadEvent.COMPLETE, onUploadComplete);
	}

    public function removeVideo(video:ScriptVideo):void {
        this.videos.removeItemAt(this.videos.getItemIndex(video));
    }

    public function removeCG(deletedCG:AbstractScriptItem):void {
        for each (var cg:AbstractScriptItem in this.cgs) {
            if (cg == deletedCG) {
                this.cgs.removeItemAt(deletedCG.order);
                break;
            }
        }
    }

	private function onUploadInit(event:UploadEvent):void {
		uploadCount++;
	}

	private function onUploadCancel(event:UploadEvent):void {
		uploadCount--;
	}

	private function onUploadComplete(event:UploadEvent):void {
		uploadCount--;
	}

    public function getColor():uint {
        return this.copied ? 0x33AAAA :
                this.cutted ? 0x628CBC :
                    this.displaying ? 0x9600B8 :
                        this.displayed ? 0x646464 :
                            this.editingUser ? 0xE2666A :
                                    this.approved ? 0xD4EBD9 :
                                            this.ok ? 0xD2EEF4 :
                                                    !this.blank ? 0xFFF7B4 : 0xFFFFFF
    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public function get block():ScriptBlock {
        return _block;
    }

    public function set block(value:ScriptBlock):void {
        if (_block) {
            this.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, _block.onScriptChange);
        }

        _block = value;
    }

    public function get guideline():Guideline {
        return _guideline;
    }

    public function set guideline(value:Guideline):void {
        _guideline = value;
        hasGuideline = value is Guideline;
    }

    public function get reportage():Reportage {
        return _reportage;
    }

    public function set reportage(value:Reportage):void {
        _reportage = value;
        hasReportage = value is Reportage;
    }

	public function get uploadCount():int {
		return _uploadCount;
	}

	public function set uploadCount(value:int):void {
		_uploadCount = value;
		uploading = _uploadCount != 0;
	}

}
}
