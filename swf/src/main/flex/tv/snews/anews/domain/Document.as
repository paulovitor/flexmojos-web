package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Document")]
public class Document extends AbstractVersionedEntity {

	private static const bundle:IResourceManager = ResourceManager.getInstance();
	private static const dateUtil:DateUtil = new DateUtil();

	public var date:Date;
	public var changeDate:Date = new Date();

	protected var _slug:String;

	public var excluded:Boolean;

	protected var _program:Program;
	
	protected var _state:String;
	public var statusReason:String;

	[ArrayElementType("tv.snews.anews.domain.Program")]
	public var programs:ArrayCollection = new ArrayCollection();
	
	[ArrayElementType("tv.snews.anews.domain.Program")]
	public var drawerPrograms:ArrayCollection = new ArrayCollection();
	
	[Transient]
	public var stateI18n:String = "";

	public function Document() {
		super(this);
		state = DocumentState.PRODUCING;
	}

	//------------------------------
	//	Operations
	//------------------------------

	protected function copyData(other:Document):void {
		super.updateFields(other);

		this.date = other.date;
		this.changeDate = other.changeDate;
		this.slug = other.slug;
		this.excluded = other.excluded;
		this.program = other.program;
		this.state = other.state;
		this.statusReason = other.statusReason;

		this.programs.removeAll();
		this.programs.addAll(other.programs);
	}

	protected function hasDiff(other:Document):Boolean {
		if (slug != other.slug || excluded != other.excluded) {
			return true;
		}

		if ((author && !author.equals(other.author)) || (author == null && other.author != null)) {
			return true;
		}

		if ((program && !program.equals(other.program)) || (program == null && other.program != null)) {
			return true;
		}

		if (!dateUtil.equals(date, other.date)) {
			return true;
		}

		if (!dateUtil.equals(changeDate, other.changeDate)) {
			return true;
		}

		if (program != null && other.programs != null) {
			if (programs.length != other.programs.length) {
				return true;
			}

			var found:Boolean;

			for each (var curr:Program in programs) {
				found = false;
				for each (var otherProgram:Program in other.programs) {
					if (curr.equals(otherProgram))
						found = true;
				}
				if (!found) return true;
			}
		}

		return false;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public function get slug():String {
		return _slug;
	}

	public function set slug(value:String):void {
		_slug = value;
	}

	public function get program():Program {
		return _program;
	}

	public function set program(value:Program):void {
		_program = value;
	}
	
	public function get state():String {
		return _state;
	}
	
	public function set state(value:String):void {
		_state = value;
		
		switch (value) {
			case DocumentState.COMPLETED:
				stateI18n = bundle.getString('Bundle', 'document.states.complete');
				break;
			case DocumentState.COVERING:
				stateI18n = bundle.getString('Bundle', 'document.states.covering');
				break;
			case DocumentState.PRODUCING:
				stateI18n = bundle.getString('Bundle', 'document.states.producing');
				break;
			case DocumentState.INTERRUPTED:
				stateI18n = bundle.getString('Bundle', 'document.states.interrupt');
				break;
			default:
				stateI18n = " ";
		}
	}
}
}
