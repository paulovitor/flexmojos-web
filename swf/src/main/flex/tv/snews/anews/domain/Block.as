package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.events.PropertyChangeEvent;
	import mx.resources.ResourceBundle;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.DateUtil;

	/**
	 * Representação da classe remota "tv.snews.anews.domain.Block". Contém 
	 * as mesma propriedades da classe remota e algumas propriedades transient 
	 * que auxiliam o cálculo de tempos do espelho.
	 * 
	 * @author Eliezer Reis
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.Block")]
	public class Block extends AbstractEntity_Number {
		
		private const dateUtil:DateUtil = new DateUtil();

		//------------------------------
		//	Persisted Properties
		//------------------------------
		
		private var _order:int = 0;
		private var _commercial:Date = dateUtil.timeZero();
		private var _standBy:Boolean = false;
		private var _rundown:Rundown;
		private var _stories:ArrayCollection = new ArrayCollection();

		//------------------------------
		//	Transient Properties
		//------------------------------

		[Transient]
		public var globalIndex:int = 0;

		[Transient]
		public var expanded:Boolean = true;

		private var _produced:Date = dateUtil.timeZero();
		private var _expected:Date = dateUtil.timeZero();

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function Block() {
			super(this); // Just to keep the abstraction
		}

		//----------------------------------------------------------------------
		//	Helpers
		//----------------------------------------------------------------------
		
		[Transient]
		public function get name():String {
			if (standBy) {
				return ResourceManager.getInstance().getString('Bundle', 'preview.standBy');
			} else {
				return ResourceManager.getInstance().getString('Bundle', 'preview.block') + " " + (order + 1);
			}
		}

		public function addStory(story:Story):void {
			if (story.order < 0 || story.order > stories.length) {
				throw new Error("ArrayOutBoundException: The story has a order out of bound of the block.");
			}

			story.block = this;
			stories.addItemAt(story, story.order);

//			reorder();
//			updateTimers();
		}

		public function removeStory(story:Story):void {
			var localInstance:Story;

			for each (localInstance in stories) {
				if (localInstance.equals(story)) {
					break;
				}
				localInstance = null;
			}

			if (localInstance) {
				stories.removeItemAt(stories.getItemIndex(localInstance));
//				reorder();
//				updateTimers();
			}
		}

		public function reorder():void {
			var order:int = 0;
			for each(var story:Story in stories) {
				story.order = order++;
			}
		}
		
		public function updateTimers():void {
			produced = dateUtil.timeZero();
			expected = dateUtil.timeZero();
			for each (var story:Story in stories) {
				produced = dateUtil.sumTimes(produced, story.total);
				expected = dateUtil.sumTimes(expected, story.expected);
			}
		}


        public function storyById(id:Number):Story {
            for each (var story:Story in stories) {
                if(story.id == id) return story;
            }
            return null;
        }

		/**
		 * Handler para o evento CHANGE da coleção de stories. Quando a 
		 * propriedade é um valor de tempo, atualiza as contagens de tempo onde 
		 * aquele valor é envolvido, como o total de produção do bloco.
		 */
		private function onStoriesChange(event:CollectionEvent):void {
			switch (event.kind) {
				case CollectionEventKind.ADD:
					break;
				case CollectionEventKind.REMOVE:
					break;
				case CollectionEventKind.UPDATE:
					for each (var change:PropertyChangeEvent in event.items) {
						switch (change.property) {
							case "total":
								setDateValue(change.oldValue as Date, change.newValue as Date, "produced");
								break;
							case "expected":
								if (change.newValue != null) 
									setDateValue(change.oldValue as Date, change.newValue as Date, "expected");
								break;
						}
					}
					break;
			}
		}
		
		private function setDateValue(oldValue:Date, newValue:Date, property:String):void {
			var ms:Number = dateUtil.getTimeGap(oldValue, newValue);
			var diff:Date = dateUtil.getTimeFromMilliseconds(ms);
			
			var result:Date = null;
			if (oldValue.getTime() > newValue.getTime()) {
				result = dateUtil.subtractTimes(this[property], diff);
			} else {
				result = dateUtil.sumTimes(this[property], diff);
			}
			this[property] = result;
		}
		
		//----------------------------------------------------------------------
		//	Setters & Getters
		//----------------------------------------------------------------------
		
		public function get order():int {
			return _order;
		}

		public function set order(value:int):void {
			_order = value;
		}

		public function get commercial():Date {
			return _commercial;
		}

		public function set commercial(value:Date):void {
			_commercial = value;
		}

		public function get standBy():Boolean {
			return _standBy;
		}

		public function set standBy(value:Boolean):void {
			_standBy = value;
		}

		public function get rundown():Rundown {
			return _rundown;
		}

		public function set rundown(value:Rundown):void {
			_rundown = value;
		}

		[ArrayElementType("tv.snews.anews.domain.Story")]
		public function set stories(value:ArrayCollection):void {
			stories.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onStoriesChange);

			_stories = value || new ArrayCollection(); // não deixa atribuir um valor nulo
			updateTimers();

			stories.addEventListener(CollectionEvent.COLLECTION_CHANGE, onStoriesChange);
		}

		public function get stories():ArrayCollection {
			return _stories;
		}

		//----------------------------------------------------------------------
		//	Transient
		//----------------------------------------------------------------------
		
		[Transient]
		public function get produced():Date {
			return _produced;
		}

		public function set produced(value:Date):void {
			if(value == null)
				value = dateUtil.timeZero();
			
			_produced = value;
		}

		[Transient]
		public function get expected():Date {
			return _expected;
		}

		public function set expected(value:Date):void {
			if(value == null)
				value = dateUtil.timeZero();
			
			_expected = value;
		}
	}
}
