package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.utils.ObjectUtil;

import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

/**
 * Representação da classe remota "tv.snews.anews.domain.Story". Contém
 * as mesma propriedades da classe remota e algumas propriedades transient
 * que auxiliam o cálculo de tempos do espelho.
 *
 * @author Eliezer Reis
 * @author Felipe Pinheiro
 * @author Felipe Zap de Mello
 * @author Samuel Guedes de Melo
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Story")]
public class Story extends Document implements IStory {

	private static const dateUtil:DateUtil = new DateUtil();

	//------------------------------
	//	Persisted Properties
	//------------------------------

	private var _order:int = 0;
	public var ok:Boolean;
	public var approved:Boolean;
	private var _videosAdded:Boolean;
	public var mosStatus:String;
	public var tapes:String;
    public var displaying:Boolean;
    public var displayed:Boolean;

	public var block:Block;
	public var editor:User;
    public var imageEditor:User;
	public var storyKind:StoryKind;
    public var sourceStory:Story;

    private var _reporter:User;
    private var _guideline:Guideline;
    private var _reportage:Reportage;
	private var _news:News;

	[ArrayElementType("tv.snews.anews.domain.StoryCopyLog")]
	public var copiesHistory:ArrayCollection;

	//------------------------------
	//	Dynamic Properties
	//------------------------------

	private var _page:String = "01";
	private var _expected:Date = dateUtil.timeZero();
	private var _head:Date = dateUtil.timeZero();
	private var _vt:Date = dateUtil.timeZero();
    private var _vtManually:Boolean = true;
	private var _off:Date = dateUtil.timeZero();
	private var _total:Date = dateUtil.timeZero();
	private var _information:String;

	private var _headSection:StorySection;
	private var _ncSection:StoryNCSection;
	private var _vtSection:StoryVTSection;
	private var _footerSection:StorySection;
	private var _offSection:StorySection;

	//------------------------------
	//	Transient Properties
	//------------------------------

	[Transient]
	public var globalIndex:int;

	[Transient]
	public var nextGlobalIndex:int = -1;

	[Transient]
	public var totalDiffs:int;

	public var blank:Boolean = false;

	[Transient]
	public var isDragging:Boolean = false; // transient

	[Transient]
	public var editingUser:User;

	[Transient]
	public var selected:Boolean = false;

	[Transient]
	public var noVtMedia:Boolean = true;

	[Transient]
	public var hasInformation:Boolean = false;

	[Transient]
	public var hasGuideline:Boolean = false;

	[Transient]
	public var hasReportage:Boolean = false;

    [Transient]
	public var reporterNickname:String = null;

	[Transient]
	public var cutted:Boolean = false;
	
	[Transient]
	public var copied:Boolean = false;
	//----------------------------------------------------------------------
	//	Constructor
	//----------------------------------------------------------------------

	public function Story() {
		super();

		this.slug = "";
	}

	//----------------------------------------------------------------------
	//	Operations
	//----------------------------------------------------------------------

	/**
	 * Verifica se o objeto foi alterado.
	 *
	 * @return retorna true se os objetos forem diferentes.
	 **/
	public function isDifferent(other:Story):Boolean {
		if (hasDiff(other)) {
			return true;
		}

		if (information != other.information ||
				tapes != other.tapes ||
				approved != other.approved ||
				ok != other.ok ||
                vtManually != other.vtManually) {
			return true;
		}

		if ((storyKind && !storyKind.equals(other.storyKind)) || (storyKind == null && other.storyKind != null)) {
			return true;
		}

		if (dateUtil.timeToString(expected) != dateUtil.timeToString(other.expected)) {
			return true;
		}

		if ((vtSection != null || ncSection != null) && dateUtil.timeToString(vt) != dateUtil.timeToString(other.vt)) {
			return true;
		}

		// Comparações do head section
		if ((headSection == null && other.headSection != null) || (headSection != null && other.headSection == null)) {
			return true;
		}

		if (headSection && other.headSection && headSection.isDifferent(other.headSection)) {
			return true;
		}

		// Comparações do vt section
		if ((vtSection == null && other.vtSection != null) || (vtSection != null && other.vtSection == null)) {
			return true;
		}

		if (vtSection && other.vtSection && this.vtSection.isDifferent(other.vtSection)) {
			return true;
		}

		// Comparações do nc section
		if ((ncSection == null && other.ncSection != null) || (ncSection != null && other.ncSection == null)) {
			return true;
		}

		if (ncSection && other.ncSection && this.ncSection.isDifferent(other.ncSection)) {
			return true;
		}

		// Comparações do footer section
		if ((footerSection == null && other.footerSection != null) || (footerSection != null && other.footerSection == null)) {
			return true;
		}
		if (footerSection && other.footerSection && this.footerSection.isDifferent(other.footerSection)) {
			return true;
		}

		// Comparações do off section
		if ((offSection == null && other.offSection != null) || (offSection != null && other.offSection == null)) {
			return true;
		}

		if (offSection && other.offSection && offSection.isDifferent(other.offSection)) {
			return true;
		}

		return false;
	}

	override public function updateFields(other:AbstractVersionedEntity):void {
		if (other is Story) {
			var otherStory:Story = other as Story;

			this.copyData(otherStory);

			this.storyKind = otherStory.storyKind;
			this.vt = otherStory.vt;
            this.vtManually = otherStory.vtManually;
			this.off = otherStory.off;
			this.total = otherStory.total;
			this.head = otherStory.head;
			this.expected = otherStory.expected;
			this.videosAdded = otherStory.videosAdded;
			this.headSection = otherStory.headSection;
			this.ncSection = otherStory.ncSection;
			this.vtSection = otherStory.vtSection;
			this.footerSection = otherStory.footerSection;
			this.offSection = otherStory.offSection;
			this.information = otherStory.information;
			this.reporter = otherStory.reporter;
            this.editor = otherStory.editor;
            this.imageEditor = otherStory.imageEditor;
			this.tapes = otherStory.tapes;
			this.copiesHistory = otherStory.copiesHistory;
		}
	}

	private function updateReporterNickname():void {
		reporterNickname = null;

		if (guideline) {
			reporterNickname = guideline.reportersAsStr;
		}
		if (reportage && reportage.reporter) {
			reporterNickname = reportage.reporter.nickname;
		}
		if (reporter) {
			reporterNickname = reporter.nickname;
		}
	}

	//----------------------------------------------------------------------
	//	Action Methods
	//----------------------------------------------------------------------

	public function addHead():void {
		headSection = headSection || new StorySection();
		headSection.addSubSection(new StoryCameraText(defaultPresenter));
	}

	public function addNC():void {
		vtSection = null; // NCSection elimina a VT Section
		ncSection = ncSection || new StoryNCSection();
	}

	public function addVT():void {
		ncSection = null; // VTSection elimina a NCSection
		vtSection = vtSection || new StoryVTSection();
	}

	public function addFooterNote():void {
		footerSection = footerSection || new StorySection();
		footerSection.addSubSection(new StoryCameraText(defaultPresenter));
	}

	public function addOff():void {
		offSection = offSection || new StorySection();
		offSection.addSubSection(new StoryText(defaultPresenter));
	}

	public function removeHeadSection():void {
		this.headSection = null;
		updateTimes();
	}

	public function removeNCSection():void {
		this.ncSection = null;
		vt = dateUtil.timeZero();
		updateTimes();
        this.videosAdded = false;
    }

	public function removeVTSection():void {
		this.vtSection = null;
		vt = dateUtil.timeZero();
		updateTimes();
        this.videosAdded = false;
    }

	public function removeFooterSection():void {
		footerSection = null;
		updateTimes();
	}

	public function removeOffSection():void {
		this.offSection = null;
		updateTimes();
	}

	public function updateTimes():void {
		var zero:Date = dateUtil.timeZero();

		total = zero;
		head = zero;
		off = zero;
		vt = vt || zero;

		if (headSection != null) {
			head = headSection.calculateDuration();
		}
		if (vtSection != null) {
            var vtDuration:Date = vtSection.calculateDuration();
            if (!vtManually) {
                vt = vtDuration;
            } else {
                vtSection.duration = vt;
            }
		}
		if (ncSection != null) {
            var ncDuration:Date = ncSection.calculateDuration();
            if (!vtManually) {
                vt = ncDuration;
            } else {
                ncSection.duration = vt;
            }
		}
		if (footerSection != null) {
			head = dateUtil.sumTimes(head, footerSection.calculateDuration());
		}
		if (offSection != null) {
			off = offSection.calculateDuration();
		}
        total = dateUtil.sumTimes(head, vt);
    }

	/*
	 * Método chamando nos setters de todas as propriedades envolvidas no seu cálculo
	 */
	private function updateBlankState():void {
		blank = headSection || vtSection || ncSection || footerSection || offSection || !StringUtils.isNullOrEmpty(information);
	}

	public function updateGlobalIndex():void {
		globalIndex = block.globalIndex + _order;
	}

    public function getColor():uint {
        return this.copied ? 0x33AAAA :
                    this.cutted ? 0x628CBC :
                        this.displaying ? 0x9D58AC :
                                this.displayed ? 0x928F92 :
                                        this.editingUser ? 0xE2666A :
                                                this.approved ? 0xD4EBD9 :
                                                        this.ok ? 0xD2EEF4 :
                                                                this.blank ? 0xFFF7B4 : 0xFFFFFF;
    }

	//----------------------------------------------------------------------
	//	Setters & Getters
	//----------------------------------------------------------------------

	public function get page():String {
		return _page;
	}

	public function set page(value:String):void {
		_page = (value || '').toUpperCase();
	}

	public function get expected():Date {
		return _expected;
	}

	public function set expected(value:Date):void {
		_expected = value || dateUtil.timeZero();
	}

	public function get head():Date {
		return _head == null ? dateUtil.timeZero() : _head;
	}

	public function set head(value:Date):void {
		_head = value || dateUtil.timeZero();
	}

	public function get vt():Date {
		return _vt == null ? dateUtil.timeZero() : _vt;
	}

	public function set vt(value:Date):void {
		_vt = value || dateUtil.timeZero();
	}

	public function get off():Date {
		return _off == null ? dateUtil.timeZero() : _off;
	}

	public function set off(value:Date):void {
		_off = value || dateUtil.timeZero();
	}

	public function get total():Date {
		return _total;
	}

	public function set total(value:Date):void {
		_total = value || dateUtil.timeZero();
	}

	public function get information():String {
		return _information;
	}

	public function set information(value:String):void {
		_information = value;
		hasInformation = value != null;
		updateBlankState();
	}

	public function get headSection():StorySection {
		return _headSection;
	}

	public function set headSection(value:StorySection):void {
		_headSection = value;
		if (_headSection) {
			_headSection.story = this;
			_headSection.renameSubSections();
		}
		updateBlankState();
	}

	public function get ncSection():StoryNCSection {
		return _ncSection;
	}

	public function set ncSection(value:StoryNCSection):void {
		_ncSection = value;
		if (_ncSection) {
			_ncSection.story = this;
			_ncSection.renameSubSections();
		}
		updateBlankState();
	}

	public function get vtSection():StoryVTSection {
		return _vtSection;
	}

	public function set vtSection(value:StoryVTSection):void {
		_vtSection = value;
		if (_vtSection) {
			_vtSection.story = this;
			_vtSection.renameSubSections();
		}
		updateBlankState();
	}

	public function get footerSection():StorySection {
		return _footerSection;
	}

	public function set footerSection(value:StorySection):void {
		_footerSection = value;
		if (_footerSection) {
			_footerSection.story = this;
			_footerSection.renameSubSections();
		}
		updateBlankState();
	}

	public function get offSection():StorySection {
		return _offSection;
	}

	public function set offSection(value:StorySection):void {
		_offSection = value;
		if (_offSection) {
			_offSection.story = this;
			_offSection.renameSubSections();
		}
		updateBlankState();
	}

	[ArrayElementType("tv.snews.anews.domain.StoryRevision")]
	override public function get revisions():ArrayCollection {
		return super.revisions;
	}

	override public function set revisions(value:ArrayCollection):void {
		super.revisions = value;
	}
	
	public function get news():News {
		return _news;
	}
	
	public function set news(value:News):void {
		_news = value;
	}

	public function get guideline():Guideline {
		return _guideline;
	}

	public function set guideline(value:Guideline):void {
		_guideline = value;
		hasGuideline = value is Guideline;
		updateReporterNickname();
	}

	public function get reportage():Reportage {
		return _reportage;
	}

	public function set reportage(value:Reportage):void {
		_reportage = value;
		hasReportage = value is Reportage;
		updateReporterNickname();
	}

	public function get reporter():User {
		return _reporter;
	}

	public function set reporter(value:User):void {
		_reporter = value;
		updateReporterNickname();
	}

	//----------------------------------------------------------------------
	//	Transient
	//----------------------------------------------------------------------

//	[Transient]
//	public function get program():Program {
//		return block.rundown.program;
//	}

	[Transient]
	public function get defaultPresenter():User {
		return block.rundown.program.defaultPresenter;
	}

	[Transient]
	public function get sections():Array {
		return [ headSection, vtSection, ncSection, offSection, footerSection ];
	}

    public function get videosAdded():Boolean {
        return _videosAdded;
    }

    public function set videosAdded(value:Boolean):void {
        _videosAdded = value;
    }

	public function get order():int {
		return _order;
	}

	public function set order(value:int):void {
		_order = value;
		updateGlobalIndex();
	}

    public function get vtManually():Boolean {
        return _vtManually;
    }

    public function set vtManually(value:Boolean):void {
        _vtManually = value;
    }
}
}
