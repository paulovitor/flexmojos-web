package tv.snews.anews.domain{
	import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;
	
	/**
	 * Representa as ações exclusivas do espelho.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.2.0
	 */
	public class RundownAction {
		
		public static const SORTED:String = "SORTED"; 
		public static const DRAWER:String = "DRAWER"; 
//		public static const DRAG_LOCKED:String = "DRAG_LOCKED";
//		public static const DRAG_UNLOCKED:String = "DRAG_UNLOCKED";
		public static const CREATED:String = "CREATED";
		public static const START_CHANGED:String = "START_CHANGED";
		public static const END_CHANGED:String = "END_CHANGED";
		public static const PRODUCTION_CHANGED:String = "PRODUCTION_CHANGED";
        public static const DISPLAY_RESETS:String = "DISPLAY_RESETS";
        public static const COMPLETE_DISPLAY:String = "COMPLETE_DISPLAY";
        public static const FIELD_UPDATED:String = "FIELD_UPDATED";
		
		public function RundownAction() {}
	
		public static function getAllActions():ArrayCollection {
			var actions:ArrayCollection = new ArrayCollection();
			for each (var key:String in [ CREATED, START_CHANGED, END_CHANGED, PRODUCTION_CHANGED ]) {
				var obj:Object = new Object();
				obj.id = key;
				obj.label = i18nOf(key);
				actions.addItem(obj);
			}
			return actions;
		}
		
		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String):String {
			var bundle:IResourceManager = ResourceManager.getInstance();

			switch (type) {
				case CREATED:			 return bundle.getString("Bundle", "rundown.log.lbCreated");
				case START_CHANGED:		 return bundle.getString("Bundle", "rundown.log.lbStartChanged");
				case END_CHANGED:		 return bundle.getString("Bundle", "rundown.log.lbEndChanged");
				case PRODUCTION_CHANGED: return bundle.getString("Bundle", "rundown.log.lbProductionChanged");
				default:				 return "MISSING BUNDLE VALUE";
			}
		}
	}
}
