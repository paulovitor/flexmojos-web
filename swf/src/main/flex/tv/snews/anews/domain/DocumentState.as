package tv.snews.anews.domain {
	import mx.collections.ArrayCollection;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;

	/**
	 * Classe enum que define os possíveis estados que uma pauta pode passar 
	 * durante seu ciclo de produção.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class DocumentState {
		
		/**
		 * Estado inicial da pauta, onde o produtor ainda está elaborando e 
		 * coletando o mínimo de informações necessárias para que o repórter possa 
		 * cobrir a matéria. 
		 */
		public static const PRODUCING:String = "PRODUCING";
		
		/**
		 * Segundo estado da pauta, que é definido a partir do momento em que o 
		 * repórter inicia a cobertura da matéria, mesmo que o produtor ainda não 
		 * tenha encerrado seu trabalho.
		 */
		public static const COVERING:String = "COVERING";
		
		/**
		 * Um dos estados finais da pauta que indica que o repórter encerrou a 
		 * cobertura da pauta com sucesso.
		 */
		public static const COMPLETED:String = "COMPLETED";
		
		/**
		 * Um dos estados finais da pauta, que indica que a mesma foi cancelada 
		 * por um motívo qualquer. A pauta pode voltar a um estado ativo 
		 * posteriormente.
		 */
		public static const INTERRUPTED:String = "INTERRUPTED";
		
		/**
		 * Sinaliza que o documento está no estado "em edição"
		 */
		public static const EDITING:String = "EDITING";
		
		private static var _values:ArrayCollection;

		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(state:String):String {
			switch (state) {
				case PRODUCING:
					return ResourceManager.getInstance().getString("Bundle", "document.states.producing");
				case COVERING:
					return ResourceManager.getInstance().getString("Bundle", "document.states.covering");
				case INTERRUPTED:
					return ResourceManager.getInstance().getString("Bundle", "document.states.interrupted");
				case COMPLETED:
					return ResourceManager.getInstance().getString("Bundle", "document.states.completed");
				case EDITING:
					return ResourceManager.getInstance().getString("Bundle", "document.states.completed");
				default:
					throw new Error("Unknow document state: " + state);
			}
		}
		
		public static function values():ArrayCollection {
			if (_values == null) {
				const bundle:IResourceManager = ResourceManager.getInstance();
				
				_values = new ArrayCollection(
					[
						{
							"id": DocumentState.PRODUCING,
							"name": bundle.getString("Bundle", "document.states.producing")
						},
						{
							"id": DocumentState.COVERING,
							"name": bundle.getString("Bundle", "document.states.covering")
						},
						{
							"id": DocumentState.COMPLETED,
							"name": bundle.getString("Bundle", "document.states.completed")
						},
						{
							"id": DocumentState.INTERRUPTED,
							"name": bundle.getString("Bundle", "document.states.interrupted")
						},
						{
							"id": DocumentState.EDITING,
							"name": bundle.getString("Bundle", "document.states.editing")
						}
					]
				);
			}
			
			return _values;
		}

        public static function findById(id:String):Object {
            for each (var obj:Object in values()) {
                if (id == obj.id) {
                    return obj;
                }
            }
            return null;
        }

		public function DocumentState() {
			throw new Error("This is a enum like class. Cannot be instantiated.");
		}
	}
}
