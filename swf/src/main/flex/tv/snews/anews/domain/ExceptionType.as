package tv.snews.anews.domain {

	/**
	 * Representa as entidades do sistema.
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.0.0
	 */
	public class ExceptionType {
		
		/**
		 * Exceção de validação.
		 */
		public static const VALIDATION_EXCEPTION:String = "tv.snews.anews.exception.ValidationException";
		
		/**
		 * Exceção de operação inválida.
		 */
		public static const ILLEGAL_OPERATION_EXCEPTION:String  = "tv.snews.anews.exception.IllegalOperationException";
	
		
		/**
		 * Exceção lançada pelo nosso firewall CustomMessageInterceptor
		 */
		public static const UNAUTHORIZED_EXCEPTION:String  = "tv.snews.anews.flex.UnauthorizedException";
	}
}
