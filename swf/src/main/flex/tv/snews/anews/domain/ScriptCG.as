package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ScriptCG")]
public class ScriptCG extends AbstractScriptItem {

    public var code:int;
    public var templateCode:int;
    public var comment:Boolean = false;

    [ArrayElementType("tv.snews.anews.domain.ScriptCGField")]
    public var fields:ArrayCollection = new ArrayCollection();

    public function ScriptCG(template:IITemplate = null) {
        if (template) {
            this.templateCode = template.templateCode;
            this.comment = template.comment;

            for each (var templateField:IITemplateField in template.fields) {
                fields.addItem(new ScriptCGField(this, templateField));
            }
        }
    }

    override public function isDifferent(other:AbstractScriptItem):Boolean {
        if (super.isDifferent(other)) return true;

        var otherScriptCG:ScriptCG = ScriptCG(other);
        if (templateCode != otherScriptCG.templateCode ||
                code != otherScriptCG.code ||
                comment != otherScriptCG.comment ||
                fields.length != otherScriptCG.fields.length) {
            return true;
        }

        for (var index:int = 0; index < fields.length; index++) {
            var scriptCGField:ScriptCGField = ScriptCGField(fields.getItemAt(index));
            var otherScriptCGField:ScriptCGField = ScriptCGField(otherScriptCG.fields.getItemAt(index));
            if (scriptCGField.isDifferent(otherScriptCGField)) {
                return true;
            }
        }

        return false;
    }

    public function resetIDs():void {
        this.id = undefined;
        for each (var field:ScriptCGField in fields) {
            field.id = undefined;
        }
    }
}
}
