package tv.snews.anews.domain {

import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.anews.domain.MosMedia;

import tv.snews.flexlib.utils.ObjectUtils;

/**
 * @author Felipe Pinheiro
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ScriptVideo")]
public class ScriptVideo extends AbstractEntity_Number implements IMediaWrapper {

	[Embed(source = "/assets/video.png")]
	private static var VIDEO_IMAGE:Class;

	[Embed(source = "/assets/audio.png")]
	private static var AUDIO_IMAGE:Class;

	[Embed(source = "/assets/still.png")]
	private static var STILL_IMAGE:Class;

    public var channel:MosDeviceChannel;
    private var _media:Media;
    public var mosStatus:String;
    private var _slug:String = "";
    private var _durationTime:Date;
    private var _ready:Boolean;
    public var disabled:Boolean;
    public var order:int;
    public var script:Script;

    [Transient]
    public var label:String;

    public function ScriptVideo() {
        super(this);
    }

    public function isDifferent(other:ScriptVideo):Boolean {
        if (other == null || channel && !other.channel || !channel && other.channel ||
                order != other.order ||
                (!(this is ObjectUtils.getClass(other)))) {
            return true;
        }

        if (channel && other.channel && channel.name != other.channel.name) return true;
        if (_media && !other._media || !_media && other._media) return true;
        if (_media && other._media && _media.isDifferent(other._media)) return true;

        return false;
    }

    //----------------------------------------------------------------------
    //	Getter's and Setter's
    //----------------------------------------------------------------------

    public function get media():Media {
        return _media;
    }

    public function set media(value:Media):void {
        if(value != null) {
            _ready = value is MosMedia ? MosMedia(value).ready : null;
            _durationTime = value.durationTime;
            _slug = value.slug;
        }
        disabled = (value == null);
        _media = value;
    }

    public function get mosMedia():MosMedia {
        return _media as MosMedia;
    }

    public function set mosMedia(value:MosMedia):void {
        _media = value;
    }

    public function get slug():String {
        return _slug;
    }

    public function set slug(value:String):void {
        _slug = value;
    }

    public function get durationTime():Date {
        return _durationTime;
    }

    public function set durationTime(value:Date):void {
        _durationTime = value;
    }

    public function get ready():Boolean {
        return _ready;
    }

    public function set ready(value:Boolean):void {
        _ready = value;
    }

	//------------------------------
	//	IVisualMedia Methods
	//------------------------------

	public function get channelName():String {
		return channel ? channel.name : null;
	}

	public function get time():Date {
		return _durationTime;
	}

	public function get name():String {
		return _slug;
	}

	public function get proxyPath():String {
		if (_media is MosMedia) {
			var mosMedia:MosMedia = _media as MosMedia;
			return mosMedia.proxyPaths.length > 0 ? MosProxyPath(mosMedia.proxyPaths.getItemAt(0)).url : null;
		}

		if (_media is WSMedia) {
			return WSMedia(_media).streamingURL;
		}

		return null;
	}

	public function get representationImage():Class {
		if (_media is MosMedia) {
			var mosMedia:MosMedia = _media as MosMedia;

			switch (mosMedia.type) {
				case MosObjectType.AUDIO: return AUDIO_IMAGE;
				case MosObjectType.STILL: return STILL_IMAGE;
				case MosObjectType.VIDEO: return VIDEO_IMAGE;
				default: return null;
			}
		}

		if (_media is WSMedia) {
			return VIDEO_IMAGE;
		}

		return null;
	}

	public function get status():String {
		if (_media is MosMedia) {
			var mosMedia:MosMedia = _media as MosMedia;

			if (mosStatus) {
				return mosStatus;
			}

			const rm:IResourceManager = ResourceManager.getInstance();

			return mosMedia.ready ? rm.getString('Bundle', 'mos.search.ready') : rm.getString('Bundle', 'mos.search.notReady');
		}

		if (_media is WSMedia) {
            return WSMediaStatus.statusDescription(WSMedia(_media).status);
		}

		return null;
	}
}
}
