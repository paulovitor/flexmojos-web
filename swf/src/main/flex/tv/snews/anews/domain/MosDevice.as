package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

import tv.snews.anews.domain.MosDeviceChannel;
import tv.snews.anews.utils.DomainCache;

/**
 * Classe utilizada para persistir as informações de conexão com um
 * dispositivo MOS, como seu endereço IP, ID, número das portas e profiles
 * suportados.
 *
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.MosDevice")]
public class MosDevice extends Device {

	public var mosId:String;
	public var hostname:String;

	public var lowerPort:int = 10540;
	public var upperPort:int = 10541;
	public var queryPort:int = 10542;
	public var currentMessageId:int;

	public var activeX:Boolean = false;
	public var tpMode:Boolean = false;

	public var profileOneSupported:Boolean = false;
	public var profileTwoSupported:Boolean = false;
	public var profileThreeSupported:Boolean = false;
	public var profileFourSupported:Boolean = false;
	public var profileFiveSupported:Boolean = false;
	public var profileSixSupported:Boolean = false;
	public var profileSevenSupported:Boolean = false;

	public var revision:String = MosRevision.V_284;
	public var defaultChannel:MosDeviceChannel;

	[ArrayElementType("tv.snews.anews.domain.MosDeviceChannel")]
	public var channels:ArrayCollection = new ArrayCollection();

	//------------------------------
	//	Constructor
	//------------------------------

	public function MosDevice() {
		super();
		super.protocol = DeviceProtocol.MOS;
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function addChannel(channel:MosDeviceChannel):void {
		channel.device = this;
		channels.addItem(channel);
	}

	public function removeChannel(channel:MosDeviceChannel):void {
		var index:int = channels.getItemIndex(channel);
		channels.removeItemAt(index);
	}

	override public function update(device:Device):void {
		super.update(device);

		if (device is MosDevice) {
			var other:MosDevice = MosDevice(device);
			this.mosId = other.mosId;
			this.hostname = other.hostname;
			this.lowerPort = other.lowerPort;
			this.upperPort = other.upperPort;
			this.queryPort = other.queryPort;
			this.currentMessageId = other.currentMessageId;
			this.activeX = other.activeX;
			this.tpMode = other.tpMode;
			this.profileOneSupported = other.profileOneSupported;
			this.profileTwoSupported = other.profileTwoSupported;
			this.profileThreeSupported = other.profileThreeSupported;
			this.profileFourSupported = other.profileFourSupported;
			this.profileFiveSupported = other.profileFiveSupported;
			this.profileSixSupported = other.profileSixSupported;
			this.profileSevenSupported = other.profileSevenSupported;
			this.revision = other.revision;
			this.defaultChannel = other.defaultChannel;
			this.channels = other.channels;
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	override public function set protocol(value:String):void {
		super.protocol = DeviceProtocol.MOS;
	}

	[Transient]
	public function get revisionObject():Object {
		return revision ? MosRevision.entry(revision) : null;
	}

	public function set revisionObject(value:Object):void {
		revision = value && value.id ? value.id : null;
	}

	[Transient]
	public function get activeXObject():Object {
		return DomainCache.YES_OR_NO.getItemAt(activeX ? 0 : 1);
	}

	public function set activeXObject(value:Object):void {
		activeX = value ? value.id : false;
	}
}
}
