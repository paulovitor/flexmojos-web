package tv.snews.anews.domain {
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.StatusEvent;
	import flash.events.TimerEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
	import flash.net.URLStream;
	import flash.utils.Timer;

import mx.resources.ResourceManager;

import mx.rpc.events.ResultEvent;
	
	import tv.snews.anews.component.Info;
	import tv.snews.anews.service.LoginService;
	import tv.snews.anews.service.UserService;
	
	

	/**
	 * Classe utilizada para persistir as informações de conexão com outras praças.
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.3
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.Company")]
	public class Company extends AbstractEntity_Int {

		private const timer:Timer = new Timer(5000);
		private var loginService:LoginService;
		private var userService:UserService;
		
		private var _name:String;
		private var _host:String;
		private var _port:int;
		private var _email:String;
		private var _password:String;
		
		//Transient
		private var monitor:URLLoader = new URLLoader();
		private var _online:Boolean = false;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function Company() {
			super(this); // Just to keep the abstraction
			
			// Valores default
			this._port = 8080;
		}

		//------------------------------
		//	Operations
		//------------------------------
		public function connect(callback:Function):void {
			callback();
            /* TODO Desativado até corrigir o Spring Security */
			/*loginService = LoginService.getInstance(host, port);
			userService = UserService.getInstance(host, port);

        userService.loadByCredentials(email, password,
                function(event:ResultEvent):void {
                    var user:User = event.result as User;
                    if (user == null) {
                        ANews.showInfo(ResourceManager.getInstance().getString('Bundle', "company.msg.loginFailed", [name, email]), Info.WARNING);
                    } else {
                        loginService.login(user,
                            function(event:ResultEvent):void {
//                                    online = true;
                                callback();
                            }
                        );
                    }
                }
            );*/
		}
		
		//Inicia o monitoramento do host
		public function startMonitoringTheHost():void {
            monitor.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHTTPStatus);
            monitor.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
            monitor.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			verifyHost();

			// Em intervalos de 5 segundos verifica se o endereço está online
			timer.addEventListener(TimerEvent.TIMER,
                    function(event:TimerEvent):void {
						verifyHost();
			        }
            );
			timer.start();
		}
		
		private function verifyHost():void {
			if (host != null && host.length > 0) {
				try {
					var urlReq:URLRequest = new URLRequest('http://'+ host + ":" + port + "/flexmojos-web/?rnd=" + Math.random());
					monitor.load(urlReq);
				} catch(error:Error) {
					trace("Error catch: " + error);
				}
			}
		}
	
		public function stopMonitoringTheHost():void {
            monitor.removeEventListener(HTTPStatusEvent.HTTP_STATUS, onHTTPStatus);
            monitor.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
            monitor.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);

            timer.stop();
		}
		
		//Handler de monitoramento 
		private function onHTTPStatus(event:HTTPStatusEvent):void {
			online = event.status > 199 && event.status < 300;
		}

        private function onIOError(event:IOErrorEvent):void {
            trace("Erro de IO no URLLoader.", event.text, event.toString());
            online = false;
        }

        private function onSecurityError(event:SecurityErrorEvent):void {
            trace("Erro de Security no URLLoader.", event.text, event.toString());
            online = false;
        }
		
		public function updateFields(company:Company):void {
			this.id = company.id; // necessário para o insert
			this.name = company.name;
			this.host = company.host;
			this.port = company.port;
			this.email = company.email;
			this.password = company.password;
		}

		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
		}

		public function get host():String {
			return _host;
		}

		public function set host(value:String):void {
			_host = value;
		}

		public function get port():int {
			return _port;
		}
		
		public function set port(value:int):void {
			_port = value;
		}
		
		public function get email():String {
			return _email;
		}
		
		public function set email(value:String):void {
			_email = value;
		}
		
		public function get password():String {
			return _password;
		}
		
		public function set password(value:String):void	{
			_password = value;
		}
		
		public function get online():Boolean {
			return _online;
		}
		
		public function set online(value:Boolean):void {
			_online = value;
		}
	}
}
