package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import spark.collections.Sort;

import spark.collections.SortField;

import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

/**
 * Representa uma pauta no sistema.
 *
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Guideline")]
public class Guideline extends Document {

	private static const bundle:IResourceManager = ResourceManager.getInstance();
	private static const dateUtil:DateUtil = new DateUtil();

	//
	// NOTE: Mudança no nome de qualquer variável deve refletir no copy()
	//

	public var proposal:String;
	public var referral:String;
	public var informations:String;

	public var classification:GuidelineClassification; // se mudar o nome, mude a lógica em GuidelineCollection#sourceChangeHandler()

    public var team:UserTeam;

	public var news:News;

	private var _checklist:Checklist;

	public var editingUser:User;

	[ArrayElementType("tv.snews.anews.domain.Guide")]
	public var guides:ArrayCollection = new ArrayCollection();

	private var _vehicles:ArrayCollection = new ArrayCollection(); // se mudar o nome, mude a lógica em GuidelineCollection#sourceChangeHandler()

	[ArrayElementType("tv.snews.anews.domain.Reportage")]
	public var reportages:ArrayCollection = new ArrayCollection();

	private var _producers:ArrayCollection = new ArrayCollection(); // nunca deve ser nulo!
	private var _reporters:ArrayCollection = new ArrayCollection();
	private var _editors:ArrayCollection = new ArrayCollection();

	[ArrayElementType("tv.snews.anews.domain.GuidelineCopyLog")]
	public var copiesHistory:ArrayCollection;

	[Transient]
	public var totalDiffs:int;

	// Armazena os nicknames na forma de string
	[Transient]
	public var producersAsStr:String = "";

	[Transient]
	public var reportersAsStr:String = "";

	[Transient]
	public var vehiclesAsStr:String = "";

	[Transient]
	public var editorsAsStr:String = "";

	//----------------------------------------------------------------------
	//	Constructor
	//----------------------------------------------------------------------

	public function Guideline() {
		super(); // Just to keep the abstraction
		this.excluded = false;

		_producers.addEventListener(CollectionEvent.COLLECTION_CHANGE, onProducersChange);
		_reporters.addEventListener(CollectionEvent.COLLECTION_CHANGE, onReportersChange);
		_vehicles.addEventListener(CollectionEvent.COLLECTION_CHANGE, onVehiclesChange);
		_editors.addEventListener(CollectionEvent.COLLECTION_CHANGE, onEditorsChange);
	}

	//----------------------------------------------------------------------
	//	Operations
	//----------------------------------------------------------------------

	override public function updateFields(other:AbstractVersionedEntity):void {
		super.updateFields(other);

		if (other is Guideline) {
			var otherGuideline:Guideline = other as Guideline;

			this.copyData(otherGuideline);

			this.proposal = otherGuideline.proposal;
			this.referral = otherGuideline.referral;
			this.informations = otherGuideline.informations;
			this.classification = otherGuideline.classification;
            this.team = otherGuideline.team;
	
			this.news = otherGuideline.news;

			// Atualiza as coleções evitando valores null provenientes de lazy
			this.copiesHistory = otherGuideline.copiesHistory || copiesHistory;
			this.editors = otherGuideline.editors || editors;
			this.guides = otherGuideline.guides || guides;
			this.producers = otherGuideline.producers || producers;
			this.reporters = otherGuideline.reporters || reporters;
			this.vehicles = otherGuideline.vehicles || vehicles;

			if (this.checklist == null) {
				this.checklist = otherGuideline.checklist;
			} else {
				if (otherGuideline.checklist) {
					this.checklist.updateFields(otherGuideline.checklist);
				}
			}
		}
	}

	/**
	 * Verifica se o objeto foi alterado.
	 *
	 * @return retorna true se os objetos forem diferentes.
	 */
	public function isDifferent(other:Guideline):Boolean {
		if (hasDiff(other)) {
			return true;
		}
		
		if ((classification && !classification.equals(other.classification)) || (classification == null && other.classification != null)) {
			return true;
		}

        if( (team && !team.equals(other.team)) || team == null && other.team != null ){
            return true;
        }

		if (informations != other.informations ||
				proposal != other.proposal ||
				referral != other.referral) {
			return true;
		}

		if (guides.length != other.guides.length) {
			return true;
		}

		for (var i:int = 0; i < guides.length; i++) {
			var thisGuide:Guide = Guide(guides.getItemAt(i));
			var otherGuide:Guide = Guide(other.guides.getItemAt(i));

			if (thisGuide.isDifferent(otherGuide)) return true;
		}

		var found:Boolean;

		// Verifica mudança nos vehicles
		if (vehicles.length != other.vehicles.length) {
			return true;
		}
		for each (var vehicle:String in vehicles) {
			found = false;
			for each (var otherVehicle:String in other.vehicles) {
				if (vehicle == otherVehicle)
					found = true;
			}
			if (!found) return true;
		}

		// Verificação dos produtores
		if (producers.length != other.producers.length) {
			return true;
		}
		for each (var producer:User in producers) {
			found = false;
			for each (var otherProducer:User in other.producers) {
				if (producer.equals(otherProducer))
					found = true;
			}
			if (!found) return true;
		}

		// Verificação dos reporters
		if (reporters.length != other.reporters.length) {
			return true;
		}
		for each (var reporter:User in reporters) {
			found = false;
			for each (var otherReporter:User in other.reporters) {
				if (reporter.equals(otherReporter))
					found = true;
			}
			if (!found) return true;
		}

		// Verifica mudança nos editors
		if (editors.length != other.editors.length) {
			return true;
		}
		for each (var editor:User in editors) {
			found = false;
			for each (var otherEditor:User in other.editors) {
				if (editor.equals(otherEditor)) {
					found = true;
				}
			}
			if (!found) {
				return false;
				}
			}
		
		if ((checklist && !checklist.isTheSame(other.checklist)) || (checklist == null && other.checklist != null)) {
				return true;
		}

		return false;
	}

	/**
	 * Retorna o primeiro roteiro que será executado, baseando-se no valor
	 * de schedule.
	 *
	 * @return Primeiro roteiro ou <code>null</code> caso não existam
	 *    roteiros para esta pauta.
	 */
	public function firstGuide():Guide {
		var result:Guide = null;
		for each (var guide:Guide in guides) {
			if (result == null) {
				result = guide;
			} else if (guide.orderGuide < result.orderGuide) {
				result = guide;
			}
		}
		return result;
	}

	[Transient]
	public function get firstGuideSchedule():Number {
		var first:Guide = firstGuide();

		// NOTE Não se pode basear no "schedule.getTime()"
		return first && first.schedule? (first.schedule.hours * 60 + first.schedule.minutes) : Number.MAX_VALUE;
	}

	public function addProducer(producer:User):void {
		if (producer != null) {
			for each (var current:User in producers) {
				if (current.nickname == producer.nickname) {
					return; // sem repetição
				}
			}
			producers.addItem(producer);
		}
	}

	public function addReporter(reporter:User):void {
		if (reporter != null) {
			for each (var current:User in reporters) {
				if (current.nickname == reporter.nickname) {
					return; // sem repetição
				}
			}
			reporters.addItem(reporter);
		}
	}

	// [TV CÂMARA]
//	public function addChecklist(checklist:Checklist):void {
//		if (checklist.id) {
//			for each (var aux:Checklist in checklists) {
//				if (checklist.equals(aux)) {
//					return;
//				}
//			}
//		}
//
//		checklist.guideline = this;
//		checklists.addItem(checklist);
//		updatePendencies()
//	}

	// [TV CÂMARA]
//	public function updateChecklist(current:Checklist):void {
//		// Atualiza a checklist SE pertencer a essa pauta
//		for each (var aux:Checklist in checklists) {
//			if (aux.equals(current)) {
//				aux.updateFields(current);
//				updatePendencies();
//			}
//		}
//	}

//	public function removeChecklist(checklist:Checklist):void {
//		checklists.removeItemAt(checklists.getItemIndex(checklist));
//		updatePendencies()
//	}

	private function onProducersChange(event:CollectionEvent=null):void {
		producersAsStr = joinNicknames(producers);
	}

	private function onReportersChange(event:CollectionEvent=null):void {
		reportersAsStr = joinNicknames(reporters);
	}

	private function onVehiclesChange(event:CollectionEvent=null):void {
		var joinResult:String = joinVehicles(vehicles);
		if (joinResult != vehiclesAsStr) {
			vehiclesAsStr = joinResult;
		}
	}

	private function onEditorsChange(event:CollectionEvent=null):void {
		var joinResult:String = joinNicknames(editors);
		if (joinResult != editorsAsStr) {
			editorsAsStr = joinResult;
		}
	}


	private function joinNicknames(users:ArrayCollection):String {
		var nicknames:Array = [];
		for each (var user:User in users) {
			nicknames.push(user.nickname);
		}
		return concatNames(nicknames);
	}

	private function joinVehicles(vehicles:ArrayCollection):String {
		var names:Array = [];
		for each (var vehicle:String in vehicles) {
			names.push(bundle.getString('Bundle', 'vehicle.' + vehicle.toLowerCase()));
		}
		return concatNames(names);
	}

	

	private function concatNames(names:Array):String {
		const AND:String = bundle.getString('Bundle', 'defaults.and');
		const NONE:String = bundle.getString('Bundle', 'defaults.none');

		// Junta os nomes separando com vírgula
		var aux:String = names.join(", ");

		// Troca a última vírgula por 'e'
		var index:int = aux.lastIndexOf(",");
		if (index != -1) {
			return aux.substr(0, index) + ' ' + AND + aux.substr(index + 1, aux.length);
		} else {
			return StringUtils.trim(aux).length == 0 ? NONE : aux;
		}
	}


	//----------------------------------------------------------------------
	//	Getters & Setters
	//----------------------------------------------------------------------

	[ArrayElementType("tv.snews.anews.domain.User")]
	public function get producers():ArrayCollection {
		return _producers;
	}

	public function set producers(value:ArrayCollection):void {
		if (_producers.length == 0 && value.length == 0) {
			onProducersChange();
		}

		// Nunca deve setar null em _producers
		_producers.removeAll();
		if (value) {
			_producers.addAll(value);
		}
	}

	[ArrayElementType("tv.snews.anews.domain.User")]
	public function get reporters():ArrayCollection {
		return _reporters;
	}

	public function set reporters(value:ArrayCollection):void {
		if (_reporters.length == 0 && value.length == 0) {
			onReportersChange();
		}

		// Nunca deve setar null em _reporters
		_reporters.removeAll();
		if (value) {
			_reporters.addAll(value);
		}
	}

	public function get vehicles():ArrayCollection {
		return _vehicles;
	}

	public function set vehicles(value:ArrayCollection):void {
		if (_vehicles.length == 0 && value.length == 0) {
			onVehiclesChange();
		}

		//não deve setar null em _vehicles
		_vehicles.removeAll();
		if (value) {
			_vehicles.addAll(value);
		}
	}



	// [TV CÂMARA]
//	[ArrayElementType("tv.snews.anews.domain.Checklist")]
//	public function get checklists():ArrayCollection {
//		return _checklists;
//	}
//
//	public function set checklists(value:ArrayCollection):void {
//		if (value) {
//			// Define o sort e organiza
//			value.sort = new Sort();
//			value.sort.fields = [ new SortField("date"), new SortField("slug") ];
//			value.refresh();
//		}
//
//		// Corrige referência errada quando a pauta é atualizada
//		for each (var checklist:Checklist in value) {
//			checklist.guideline = this;
//		}
//
//		_checklists.removeAll();
//		_checklists.addAll(value);
//
//		updatePendencies();
//	}

	[ArrayElementType("tv.snews.anews.domain.GuidelineRevision")]
	override public function get revisions():ArrayCollection {
		return super.revisions;
	}

	override public function set revisions(value:ArrayCollection):void {
		super.revisions = value;
	}

	override public function set slug(value:String):void {
		_slug = value;
		if (_checklist) {
			_checklist.slug = value == null ? "" : value;
		}
	}

	public function get checklist():Checklist {
		return _checklist;
		}

	public function set checklist(value:Checklist):void {
		_checklist = value;
		}

    public function get editors():ArrayCollection {
		return _editors;
	}

	public function set editors(value:ArrayCollection):void {
		if (_editors.length == 0 && value.length == 0) {
			onEditorsChange();
	}

		// Nunca deve setar null em _producers
		_editors.removeAll();
		if (value) {
			_editors.addAll(value);
	}

	}
}
}
