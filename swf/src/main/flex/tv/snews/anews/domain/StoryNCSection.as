package tv.snews.anews.domain {
	
	import mx.utils.ObjectUtil;

import tv.snews.anews.domain.StoryWSVideo;

import tv.snews.flexlib.utils.DateUtil;

	/**
	 * @author Samuel Guedes de Melo.
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StoryNCSection")]
	public class StoryNCSection extends StorySection {

		private const dateUtil:DateUtil = new DateUtil();
		private var _timeImage:Date = dateUtil.timeZero();

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StoryNCSection() {
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		override public function calculateDuration():Date {
			duration = dateUtil.timeZero();
			timeImage = dateUtil.timeZero();
			// Isso pode falhar no caso em que acabou de remover a última 
			// subSection do tipo de vídeo.
			if (subSectionsByType(StoryText).length > 0 || subSectionsByVideoType().length > 0) {
				for each (var subSection:StorySubSection in subSections) {
					/*
					* Por enquanto as subSections das subSections não precisam
					* calcular
					* tempo, então elas não são tratadas nesse código.
					*/
					if (subSection is StoryText) {
						var storyText:StoryText = subSection as StoryText;
                        timeImage = dateUtil.sumTimes(timeImage, storyText.calculateReadTime());
					}
					if (subSection is StoryMosVideo) {
						var storyMosObj:StoryMosVideo = subSection as StoryMosVideo;
                        duration = dateUtil.sumTimes(duration, dateUtil.stringToTime(dateUtil.timeToString(storyMosObj.mosMedia.durationTime)));
					}
                    if (subSection is StoryWSVideo) {
                        var storyWSVideo:StoryWSVideo = subSection as StoryWSVideo;
                        duration = dateUtil.sumTimes(duration, dateUtil.stringToTime(dateUtil.timeToString(storyWSVideo.media.durationTime)));
                    }
				}
			}
			return duration;
		}
		
		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function get timeImage():Date {
			return _timeImage;
		}
		
		public function set timeImage(value:Date):void {
			_timeImage = value == null ? dateUtil.timeZero() : value;
		}
	}
}
