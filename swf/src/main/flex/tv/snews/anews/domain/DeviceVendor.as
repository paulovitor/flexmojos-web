package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class DeviceVendor {

	private static const bundle:IResourceManager = ResourceManager.getInstance();
	private static const OTHER_LABEL:String = bundle.getString('Bundle', 'device.form.other');

	public static const AVID:String = "AVID";
	public static const CHYRON:String = "CHYRON";
//	public static const EITV:String = "EITV";
	public static const HARRIS:String = "HARRIS";
	public static const MEDIA_PORTAL:String = "MEDIA_PORTAL";
	public static const ORAD:String = "ORAD";
	public static const OTHER:String = "OTHER";
	public static const SNEWS:String = "SNEWS";
	public static const VIZRT:String = "VIZRT";

	public static const values:ArrayCollection = new ArrayCollection([
		{ id: AVID,         label: "Avid",         protocols: [ DeviceProtocol.MOS, DeviceProtocol.NONE ] },
		{ id: CHYRON,       label: "Chyron",       protocols: [ DeviceProtocol.II, DeviceProtocol.MOS ] },
		{ id: HARRIS,       label: "Harris",       protocols: [ DeviceProtocol.MOS ] },
		{ id: MEDIA_PORTAL, label: "Media Portal", protocols: [ DeviceProtocol.WS ] },
		{ id: ORAD,         label: "Orad",         protocols: [ DeviceProtocol.MOS ] },
		{ id: OTHER,        label: OTHER_LABEL,    protocols: [ DeviceProtocol.II, DeviceProtocol.MOS, DeviceProtocol.WS ] },
		{ id: SNEWS,        label: "SNEWS",        protocols: [ DeviceProtocol.MOS ] },
		{ id: VIZRT,        label: "Vizrt",        protocols: [ DeviceProtocol.II, DeviceProtocol.MOS ] }
//		{ id: EITV, label: "EiTV" }
	]);

	public static function i18n(value:String):String {
		return entry(value).label;
	}

	public static function entry(value:String):Object {
		for each (var entry:Object in values) {
			if (entry.id == value) {
				return entry;
			}
		}
		throw new Error("Entry not found for " + value);
	}

	public static function supportedProtocols(value:String):ArrayCollection {
		var protocols:ArrayCollection = new ArrayCollection();
		var obj:Object = entry(value);

		for each (var protocol:String in obj.protocols) {
			protocols.addItem(DeviceProtocol.entry(protocol));
		}

		return protocols;
	}
}
}
