package tv.snews.anews.domain {
	
	public class ClipboardAction {
		
		public static const COPY:String = "COPY";
		public static const CUT:String  = "CUT";
		public static const PAST:String  = "PAST";
		
	}
}