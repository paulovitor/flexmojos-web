package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * 
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.Permission")]
	public class Permission extends AbstractEntity_String{

		private var _name:String;
		private var _state:String = 'unchecked';
		
		[ArrayElementType("tv.snews.anews.domain.Permission")]
		private var _children:ArrayCollection = new ArrayCollection();
		
		public function Permission() {
			super(this); //Just to keep the abstraction
		}

		// --------------------------
		// SETTERS AND GETTERS
		// --------------------------
		public function set name(value:String):void {
			_name = value;
		}

		public function get name():String {
			return _name;
		}

		public function set state(value:String):void {
			_state = value;
		}

		public function get state():String {
			return _state;
		}
		
		public function set children(value:ArrayCollection):void {
			_children = value;
		}
		
		public function get children():ArrayCollection {
			return _children;
		}
	}
}
