package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 */
[Bindable]
public interface IMediaWrapper {

	function get proxyPath():String;

	function get representationImage():Class;

	function get label():String;

	function get name():String;

	function get time():Date;

	function get status():String;

	function get channelName():String;

	function get channel():MosDeviceChannel;

	function set channel(value:MosDeviceChannel):void;

	function get disabled():Boolean;

}
}
