package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import tv.snews.anews.component.Versionable;
	import tv.snews.flexlib.utils.DateUtil;
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Representação da classe remota "tv.snews.anews.domain.StoryKind". Representa
	 * os tipos da lauda.
	 * 
	 * @author Samuel Guedes
	 * @since 1.2.5
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StoryKind")]
	public class StoryKind extends AbstractEntity_Number {

		//------------------------------
		//	Persisted Properties
		//------------------------------
		private var _name:String;
		private var _acronym:String;
		private var _defaultKind:Boolean;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StoryKind() {
			super(this); // Just to keep the abstraction
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		public function updateFields(kind:StoryKind):void {
			this.name = kind.name;
			this.acronym = kind.acronym;
			this.defaultKind = kind.defaultKind;
		}

		//----------------------------------------------------------------------
		//	Setters & Getters
		//----------------------------------------------------------------------
		public function get name():String {
			return _name;
		}
		
		public function set name(value:String):void {
			_name = value;
		}
		
		public function get acronym():String {
			return _acronym;
		}
		
		public function set acronym(value:String):void {
			_acronym = value;
		}
		
		public function get defaultKind():Boolean {
			return _defaultKind;
		}
		
		public function set defaultKind(value:Boolean):void {
			_defaultKind = value;
		}
		
	}
}