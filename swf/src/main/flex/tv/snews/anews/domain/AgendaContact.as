package tv.snews.anews.domain {

	/**
	 * @author Felipe Pinheiro
	 * @author Paulo Felipe
	 * @author Samuel Guedes
	 * @since 4.2.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.AgendaContact")]
	public class AgendaContact extends Contact {

		public var info:String;
		public var address:String;
		public var company:String;
		private var _email:String;
		public var excluded:Boolean;

		public var city:City;
		public var contactGroup:ContactGroup;

		//------------------------------
		//	Constructor
		//------------------------------
	
		public function AgendaContact(contact:Contact=null) {
			super(this); // Just to keep the abstraction.
			
			if (contact) {
				this.name = contact.name;
				this.numbers = contact.numbers;
			}
		}

		//------------------------------
		//	Operations
		//------------------------------

		override public function updateProperties(newData:Contact):void {
			super.updateProperties(newData);

			if (newData is AgendaContact) {
				var aux:AgendaContact = newData as AgendaContact;

				address = aux.address;
				company = aux.company;
				contactGroup = aux.contactGroup;
				city = aux.city;
				email = aux.email;
				excluded = aux.excluded;
				info = aux.info;
			}
		}

		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get email():String {
			return _email;
		}

		public function set email(value:String):void {
			_email = value ? value.replace(" ", "") : value;
		}
	}
}
