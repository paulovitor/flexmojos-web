package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.2.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ReportageRevision")]
public class ReportageRevision extends AbstractRevision {
	
	public var date:String;
	public var slug:String;
	public var program:String;
	public var reporter:String;
	public var cameraMan:String;
	public var sections:String;
	public var vehicles:String;
	public var information:String;
	public var tipOfHead:String;
	
	public function ReportageRevision() {}
	
}

}