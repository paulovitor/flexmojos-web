package tv.snews.anews.domain {

[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ScriptCGField")]
public class ScriptCGField extends AbstractEntity_Number {

    public var name:String;
    public var value:String;
    public var number:int;
    public var size:int;

    public var scriptCG:ScriptCG;

    public function ScriptCGField(scriptCG:ScriptCG = null, templateField:IITemplateField = null) {
        super(this);

        if (templateField) {
            this.number = templateField.number;
            this.name = templateField.name;
            this.size = templateField.size;
        }
        this.scriptCG = scriptCG;
    }

    public function isDifferent(other:ScriptCGField):Boolean {
        return this.number != other.number ||
                this.name != other.name ||
                this.value != other.value;
    }
}
}
