/**
 * Copyright © SNEWS 2013
 * http://www.snews.tv
 */
package tv.snews.anews.domain {

	/**
	 * Representa as ações possíveis de ocorrer no Media Center.
	 * 
	 * @author Samuel Guedes de Melo.
	 * @since 1.2.5
	 */
	public class MediaCenter {
		public static const SEARCH_SCHEMA:String = "SEARCH_SCHEMA";
		
		public function MediaCenter() {
			throw new Error("This class cannot be instantiated.");
		}
	}
}
