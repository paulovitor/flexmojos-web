package tv.snews.anews.domain {
	import mx.collections.ArrayCollection;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Representa os tipos de geradores de caracter do sistema.
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.6
	 */
	public class CGProviderEnum {
		
		/**
		 * Entidade que representa um Gerador de caracter da Chyron modelo Lyric
     	 * que trabalha com charset UTF-16BE.
		 */
		public static const CHYRON:String = "CHYRON";
		
		/**
     	 * Entidade que representa um Gerador de caracter da Vizrt modelo Viz
     	 * Trio que trabalha com charset ISO-8859-1.
		 */
		public static const VIZRT:String  = "VIZRT";
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		private static var _values:ArrayCollection;
		
		public static function values():ArrayCollection {
			if (_values == null) {
				const bundle:IResourceManager = ResourceManager.getInstance();
				
				_values = new ArrayCollection(
					[
						{
							"id": CGProviderEnum.CHYRON,
							"name": CGProviderEnum.CHYRON
						},
						{
							"id": CGProviderEnum.VIZRT,
							"name": CGProviderEnum.VIZRT
						}
					]
				);
			}
			
			return _values;
		}
		
		public static function findById(id:String):Object {
			for each (var obj:Object in values()) {
				if (id == obj.id) {
					return obj;
				}
			}
			return undefined;
		}
	}
}