package tv.snews.anews.domain {
import tv.snews.flexlib.utils.ObjectUtils;

/**
 * @author Felipe Pinheiro
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.StoryWSVideo")]
public class StoryWSVideo extends StorySubSection implements IMediaWrapper {

	[Embed(source = "/assets/video.png")]
	private static var VIDEO_IMAGE:Class;

	private var _media:WSMedia;
	public var channel:MosDeviceChannel;
    private var _slug:String = "";
    private var _durationTime:Date;
    public var disabled:Boolean;

    override public function isDifferent(other:StorySubSection):Boolean {
        if (super.isDifferent(other)) return true;

        const otherWs:StoryWSVideo = StoryWSVideo(other);

        if (media != null && !media.equals(otherWs.media)) {
            return true;
        }

        if (other == null || channel && !otherWs.channel || !channel && otherWs.channel ||
                order != other.order ||
                (!(this is ObjectUtils.getClass(other)))) {
            return true;
        }

        if (channel && otherWs.channel && channel.name != otherWs.channel.name) return true;
        return false;
    }

	//------------------------------
	//	IVisualMedia Methods
	//------------------------------

	public function get channelName():String {
        return channel ? channel.name : null;
	}

	public function get time():Date {
		return _durationTime;
	}

	public function get label():String {
		return _slug;
	}

	public function get proxyPath():String {
		return media != null ? media.streamingURL : null;
	}

	public function get representationImage():Class {
		return media != null ? VIDEO_IMAGE : null;
	}

	public function get status():String {
		return media != null ? WSMediaStatus.statusDescription(media.status) : null;
	}

    //----------------------------------------------------------------------
    //	Getter's and Setter's
    //----------------------------------------------------------------------

    public function set media(value:WSMedia):void {
        if(value != null) {
            _durationTime = value.durationTime;
            _slug = value.slug;
        }
        disabled = (value == null);
        _media = value;
    }

    public function get media():WSMedia {
        return _media;
    }

    public function get slug():String {
        return _slug;
    }

    public function set slug(value:String):void {
        _slug = value;
    }

    public function get durationTime():Date {
        return _durationTime;
    }

    public function set durationTime(value:Date):void {
        _durationTime = value;
    }
}
}
