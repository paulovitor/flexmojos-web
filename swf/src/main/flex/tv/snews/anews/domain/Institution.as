package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * Entidade que representa as instituições da ronda.
 *
 * @author Paulo Felipe
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Institution")]
public class Institution extends AbstractEntity_Int {

	public var name:String;
	public var segment:Segment;
    public var defaultContact:Contact;

	[ArrayElementType("tv.snews.anews.domain.Contact")]
	public var contacts:ArrayCollection = new ArrayCollection();

	public function Institution() {
		super(this); // Just to keep the abstraction.
	}

	public function containsContact(contact:Contact):Contact {
		// Verifica se é um contato novo
		if (contact.id <= 0) {
			return null;
		}

		for each (var current:Contact in contacts) {
			if (current.id && current.id == contact.id) {
				return current;
			}
		}
		return null;
	}

	public function addContact(contact:Contact):void {
		var found:Contact = containsContact(contact);

		if (!found) {
			contacts.addItem(contact);
		} else { // atualiza
			found.updateProperties(contact);
		}
	}

	public function removeContact(contact:Contact):void {
		for (var i:int = 0; i < contacts.length; i++) {
			if (contacts.getItemAt(i).id == contact.id) {
				contacts.removeItemAt(i);
				break;
			}
		}
	}

	public function updateFields(newData:Institution):void {
		this.name = newData.name;
		this.segment = newData.segment;
		this.contacts = newData.contacts;
        this.defaultContact = newData.defaultContact;
	}
}

}
