package tv.snews.anews.domain {
	import flash.media.Camera;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	import mx.utils.ObjectUtil;
	
	import tv.snews.flexlib.utils.DateUtil;
	import tv.snews.flexlib.utils.ObjectUtils;
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Representa uma uma sub sessão da lauda
	 * A sub sessão pode conter outras sub sessões dentro dela
	 * mesma.
	 * 
	 * Exemplo
	 * - Cabeça1
	 * 		- GC1
	 *  	- GC2 
	 *
	 * @author Felipe Zap de Mello.
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StorySubSection")]
	public class StorySubSection extends AbstractEntity_Number {
		
		private var _order:int;
		private var _storySection:StorySection;
		private var _parent:StorySubSection;
		private var _subSections:ArrayCollection = new ArrayCollection();
		
		// Transients
		private var _collapsed:Boolean;

		[Transient]
		public var name:String;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StorySubSection() {
			super(this); // Just to keep the abstraction.
		}

		/**
		 * Verifica se o objeto foi alterado.
		 *  
		 * @return retorna true se os objetos forem diferentes.
		 * 
		 **/
		public function isDifferent(other:StorySubSection):Boolean {
			if (other == null) return true;
			if (order != other.order) {
				return true;
			}
			
			// Verifica se são da mesma classe
			if (!(this is ObjectUtils.getClass(other))) {
				return true;
			}
			
			// Transforma coleções nulas em coleções vazias
			const thisSubSections:ArrayCollection = subSections || new ArrayCollection();
			const otherSubSections:ArrayCollection = other.subSections || new ArrayCollection();
			
			if (thisSubSections.length != otherSubSections.length) {
				return true;
			}
			
			for (var i:int = 0; i < thisSubSections.length; i++) {
				var thisSubSection:StorySubSection = StorySubSection(thisSubSections.getItemAt(i));
				var otherSubSection:StorySubSection = StorySubSection(otherSubSections.getItemAt(i));
				
				if (thisSubSection.isDifferent(otherSubSection)) {
					return true;
				}
			}
			
			return false;
		}
		
		//----------------------------------------------------------------------
		//	Helpers
		//----------------------------------------------------------------------
		public function addSubSection(subSection:StorySubSection):void {
			subSection.storySection = null;
			subSection.parent = this;
			subSection.order = subSections.length;
			subSections.addItem(subSection);
		}
		
		public function updateTimes():void {
			if(storySection) { //se for pai
				storySection.story.updateTimes();
			} else if(parent) { // se for filho
				parent.storySection.story.updateTimes();
			}
		}
		
		public function removeSubSection(subSection:StorySubSection):Boolean {
			if (!subSections)
				return false;
			
			var removed:Object =  subSections.removeItemAt(subSections.getItemIndex(subSection));
			if (removed) {
				updateTimes();
				return true;
			}
			return false;
		}

		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------
		
		public function get order():int {
			return _order;
		}

		public function set order(value:int):void {
			_order = value;
		}
		
		public function get parent():StorySubSection{
			return _parent;
		}
		
		public function set parent(value:StorySubSection):void {
			_parent = value;
		}
		
		public function get storySection():StorySection	{
			return _storySection;
		}
		
		public function set storySection(value:StorySection):void {
			_storySection = value;
		}
		
		[ArrayElementType("tv.snews.anews.domain.StorySubSection")]
		public function get subSections():ArrayCollection {
			return _subSections;
		}
		
		public function set subSections(value:ArrayCollection):void {
			_subSections = value;
		}
		
		//----------------------------------------------------------------------
		//	Transients
		//----------------------------------------------------------------------
		
		[Transient]
		public function get collapsed():Boolean {
			return _collapsed;
		}
		
		public function set collapsed(value:Boolean):void {
			_collapsed = value;
		}
	}
}
