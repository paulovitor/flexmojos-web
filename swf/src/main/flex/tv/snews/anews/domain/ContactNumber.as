package tv.snews.anews.domain {

	/**
	 * @autor Samuel / Paulo
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.ContactNumber")]
	public class ContactNumber extends AbstractEntity_Int {
		
		private var _numberType:NumberType;
		private var _value:String;

		public function ContactNumber() {
			super(this);
		}
		
		public function get numberType():NumberType {
			return _numberType;
		}

		public function set numberType(value:NumberType):void {
			_numberType = value;
		}

		public function get value():String {
			return _value;
		}

		public function set value(value:String):void {
			_value = value;
		}
	}
}
