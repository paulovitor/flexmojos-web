package tv.snews.anews.domain {
	import flash.geom.Utils3D;
	
	import tv.snews.anews.utils.Util;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.Report")]
	public class Report extends AbstractEntity_Number {
		
		private var _slug:String = "";
		private var _content:String = "";
		
		private var _date:Date;
		private var _lastChange:Date;
		private var _author:User;
		
		private var _event:ReportEvent;
		private var _nature:ReportNature;
		
		private var _editingUser:User;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function Report() {
			super(this);
		}

		//------------------------------
		//	Operations
		//------------------------------
		
		/**
		 * Verifica se este objeto e o objeto informado possuem os mesmo valores.
		 * 
		 * @param other outro objeto ao qual este será comparado
		 * @return true se ambos possuirem os mesmo dados
		 */
		public function isTheSame(other:Report):Boolean {
			return slug == other.slug 
				&& content == other.content 
				&& Util.getDateUtil().dateToString(date) == Util.getDateUtil().dateToString(other.date)
				&& ((event == other.event) || (event && event.equals(other.event)))   
				&& ((nature == other.nature) || (nature && nature.equals(other.nature)));
		}
		
		/**
		 * Copia os dados de um outro relatório para este objeto.
		 * 
		 * @param report objeto com os dados que serão copiados
		 */
		public function copy(other:Report):void {
			slug = other.slug;
			content = other.content;
			date = other.date;
			lastChange = other.lastChange;
			author = other.author;
			event = other.event;
			nature = other.nature;
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get slug():String {
			return _slug;
		}

		public function set slug(value:String):void {
			_slug = value;
		}

		public function get content():String {
			return _content;
		}

		public function set content(value:String):void {
			_content = value;
		}

		public function get date():Date {
			return _date;
		}
		
		public function set date(value:Date):void {
			_date = value;
		}
		
		public function get lastChange():Date {
			return _lastChange;
		}

		public function set lastChange(value:Date):void {
			_lastChange = value;
		}

		public function get author():User {
			return _author;
		}

		public function set author(value:User):void {
			_author = value;
		}

		public function get event():ReportEvent {
			return _event;
		}

		public function set event(value:ReportEvent):void {
			_event = value;
		}

		public function get nature():ReportNature {
			return _nature;
		}

		public function set nature(value:ReportNature):void {
			_nature = value;
		}
		
		public function get editingUser():User {
			return _editingUser;
		}
		
		public function set editingUser(value:User):void {
			_editingUser = value;
		}
	}
}
