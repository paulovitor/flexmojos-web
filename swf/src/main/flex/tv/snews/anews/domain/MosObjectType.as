package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	/**
	 * @author Samuel Guedes de Melo
	 */
	public class MosObjectType {
		public static const VIDEO:String = "VIDEO";
		public static const AUDIO:String = "AUDIO";
		public static const STILL:String = "STILL";
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		private static var _values:ArrayCollection;
		
		public static function values():ArrayCollection {
			if (_values == null) {
				const bundle:IResourceManager = ResourceManager.getInstance();
				
				_values = new ArrayCollection(
					[
						{
							"id": MosObjectType.VIDEO,
							"name": bundle.getString("Bundle", "story.mosObj.type.video")
						},
						{
							"id": MosObjectType.AUDIO,
							"name": bundle.getString("Bundle", "story.mosObj.type.audio")
						},
						{
							"id": MosObjectType.STILL,
							"name": bundle.getString("Bundle", "story.mosObj.type.still")
						}
					]
				);
			}
			
			return _values;
		}
		
		public static function findById(id:String):Object {
			for each (var obj:Object in values()) {
				if (id == obj.id) {
					return obj;
				}
			}
			return null;
		}
	}
}