package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.ReportageCopyLog")]
public class ReportageCopyLog extends GenericCopyLog {

	private var _reportage:Reportage;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public function ReportageCopyLog() {
		super();
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public function get reportage():Reportage {
		return _reportage;
	}

	public function set reportage(value:Reportage):void {
		_reportage = value;
	}
}
}
