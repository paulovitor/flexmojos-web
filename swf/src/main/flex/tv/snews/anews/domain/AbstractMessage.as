package tv.snews.anews.domain {

	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;

	/**
	 * Define a estrutura básica de todas as mensagens do sistema de comunicação
 	 * instantânea, envio de arquivos e mensagens offline.
  	 *
   	 * @author Felipe Pinheiro
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.AbstractMessage")]
	public class AbstractMessage extends AbstractEntity_Number {

		// Identificadores dos tipos de mensagens
		public static const FILE_MESSAGE:String = "file";
		public static const TEXT_MESSAGE:String = "text";
		
		protected var _date:Date;
		protected var _sender:User;
		protected var _chat:Chat;
		
		[ArrayElementType("tv.snews.anews.domain.MessageDestination")]
		protected var _destinations:ArrayCollection = new ArrayCollection();
		
		protected var _sent:Boolean; //Transient

		public function AbstractMessage(self:AbstractMessage) {
			super(this); //Just to keep the abstraction
			
			if (self != this) {
				//only a subclass can pass a valid reference to self
				throw new IllegalOperationError("Abstract class did not receive reference to self. AbstractMessage cannot be instantiated directly.");
			}
		}

		public function getDestinationOf(user:User):MessageDestination {
			for each (var destination:MessageDestination in _destinations) {
				if (destination.receiver.id == user.id) {
					return destination;
				}
			}
			return null;
		}
		
		// --------------------------
		// SETTERS AND GETTERS
		// --------------------------
		public function set date(value:Date):void {
			_date = value;
		}

		public function get date():Date {
			return _date;
		}

		public function set sender(value:User):void {
			_sender = value;
		}

		public function get sender():User {
			return _sender;
		}
		
		public function set chat(value:Chat):void {
			_chat = value;
		}
		
		public function get chat():Chat {
			return _chat;
		}
		
		public function set destinations(value:ArrayCollection):void {
			_destinations = value;
		}
		
		public function get destinations():ArrayCollection {
			return _destinations;
		}
		
		public function set sent(value:Boolean):void {
			_sent = value;
		}
		
		public function get sent():Boolean {
			return _sent;
		}

	}
}
