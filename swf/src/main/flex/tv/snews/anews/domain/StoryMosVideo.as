package tv.snews.anews.domain {
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.flexlib.utils.ObjectUtils;

/**
 * @author Samuel Guedes de Melo.
 * @author Felipe Zap de Mello
 * @since 1.2.6
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.StoryMosVideo")]
public class StoryMosVideo extends StoryMosObject implements IMediaWrapper {

	[Embed(source = "/assets/video.png")]
	private static var VIDEO_IMAGE:Class;

	[Embed(source = "/assets/audio.png")]
	private static var AUDIO_IMAGE:Class;

	[Embed(source = "/assets/still.png")]
	private static var STILL_IMAGE:Class;

	public var channel:MosDeviceChannel;
    private var _slug:String = "";
    private var _durationTime:Date;
    private var _ready:Boolean;
    public var disabled:Boolean;

	//----------------------------------------------------------------------
	//	Constructors
	//----------------------------------------------------------------------

	public function StoryMosVideo() {
	}

	//----------------------------------------------------------------------
	//	Operations
	//----------------------------------------------------------------------

	/**
	 * Verifica se o objeto foi alterado.
	 *
	 * @return retorna true se os objetos forem diferentes.
	 */
	override public function isDifferent(other:StorySubSection):Boolean {
		if (super.isDifferent(other)) return true;

		const otherMos:StoryMosVideo = StoryMosVideo(other);

		if(mosMedia != null && !mosMedia.equals(otherMos.mosMedia)) {
			return true;
		}

        if (other == null || channel && !otherMos.channel || !channel && otherMos.channel ||
                order != other.order ||
                (!(this is ObjectUtils.getClass(other)))) {
            return true;
        }

        if (channel && otherMos.channel && channel.name != otherMos.channel.name) return true;

		return false;
	}

	//------------------------------
	//	IVisualMedia Methods
	//------------------------------

	public function get channelName():String {
		return channel ? channel.name : null;
	}

	public function get time():Date {
		return _durationTime;
	}

	public function get proxyPath():String {
		return mosMedia != null && mosMedia.proxyPaths.length > 0 ? MosProxyPath(mosMedia.proxyPaths.getItemAt(0)).url : null;
	}

	public function get representationImage():Class {
		switch (mosMedia.type) {
			case MosObjectType.AUDIO: return AUDIO_IMAGE;
			case MosObjectType.STILL: return STILL_IMAGE;
			case MosObjectType.VIDEO: return VIDEO_IMAGE;
			default: return null;
		}
	}

	public function get label():String {
		return _slug;
	}

	public function get status():String {
		if (mosStatus) {
			return mosStatus;
		}

		const rm:IResourceManager = ResourceManager.getInstance();

		return mosMedia != null ? mosMedia.ready ? rm.getString('Bundle', 'mos.search.ready') : rm.getString('Bundle', 'mos.search.notReady') : null;
	}

    //----------------------------------------------------------------------
    //	Getter's and Setter's
    //----------------------------------------------------------------------

    override public function set mosMedia(value:MosMedia):void {
        if(value != null) {
            _ready = value.ready;
            _durationTime = value.durationTime;
            _slug = value.slug;
        }
        disabled = (value == null);
        super.mosMedia = value;
    }

    public function get slug():String {
        return _slug;
    }

    public function set slug(value:String):void {
        _slug = value;
    }

    public function get durationTime():Date {
        return _durationTime;
    }

    public function set durationTime(value:Date):void {
        _durationTime = value;
    }

    public function get ready():Boolean {
        return _ready;
    }

    public function set ready(value:Boolean):void {
        _ready = value;
    }
}
}
