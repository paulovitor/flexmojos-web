package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;
	
	import tv.snews.flexlib.utils.DateUtil;

	/**
	 * Representa um roteiro usado na pauta do sistema.
	 *
	 * @author Felipe Zap de Mello
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.Guide")]
	public class Guide extends AbstractEntity_Int {

		private static const dateUtil:DateUtil = new DateUtil();
		
		private var _schedule:Date;
		private var _address:String;
		private var _reference:String;
		private var _observation:String;
		private var _orderGuide:int;
		private var _city:City;

		[ArrayElementType("tv.snews.anews.domain.Contact")]
		private var _contacts:ArrayCollection = new ArrayCollection();

		private var _guideline:Guideline;

		public function get firstContact():Contact {
			if (contacts && contacts.length != 0) {
				return contacts.getItemAt(0) as Contact;
			} else {
				return null;
			}
		}

		public function containsContact(contact:Contact):Contact {
			// Verifica se é um contato novo
			if (contact.id <= 0) {
				return null;
			}
			
			for each (var current:Contact in contacts) {
				if (current.id && current.id == contact.id) {
					return current;
				}
			}
			return null;
		}

		public function addContact(contact:Contact):void {
			var found:Contact = containsContact(contact);
			
			if (!found) {
				contacts.addItem(contact);
			} else { //atualiza
				found.updateProperties(contact);
			}
		}

		public function removeContact(contact:Contact):void {
			for (var i:int = 0; i < contacts.length; i++) {
				if (contacts.getItemAt(i).id == contact.id) {
					contacts.removeItemAt(i);
					break;
				}
			}
		}

		//------------------------------
		//	Constructor
		//------------------------------

		public function Guide() {
			super(this); // Just to keep the abstraction
		}

		public function isDifferent(other:Guide):Boolean {
			if (observation != other.observation ||
				   address != other.address ||
				 reference != other.reference ||
				orderGuide != other.orderGuide) {
				return true;
			}
			
			if (!dateUtil.equals(schedule, other.schedule)) {
				return true;
			}

			if ((city && !city.equals(other.city)) || (city == null && other.city != null)) {
				return true;
			}
			
			// Troca coleções nulas por coleções vazias
			var thisContacts:ArrayCollection = contacts || new ArrayCollection();
			var otherContacts:ArrayCollection = other.contacts || new ArrayCollection();

			if (thisContacts.length != otherContacts.length) {
				return true;
			}
			
			for (var i:int = 0; i < thisContacts.length; i++) {
				var thisContact:Contact = Contact(thisContacts.getItemAt(i));
				var otherContact:Contact = Contact(otherContacts.getItemAt(i));
				
				if (!thisContact.equals(otherContact)) {
					return true;
				}
			}
			return false;
		}
		
		public function get contacts():ArrayCollection {
			return _contacts;
		}

		public function set contacts(value:ArrayCollection):void {
			_contacts = value;
		}

		public function get observation():String {
			return _observation;
		}

		public function set observation(value:String):void {
			_observation = value;
		}
		
		public function get orderGuide():int
		{
			return _orderGuide;
		}
		
		public function set orderGuide(value:int):void
		{
			_orderGuide = value;
		}
		
		public function get reference():String {
			return _reference;
		}

		public function set reference(value:String):void {
			_reference = value;
		}

		public function get address():String {
			return _address;
		}

		public function set address(value:String):void {
			_address = value;
		}

		public function get schedule():Date {
			return _schedule;
		}

		public function set schedule(value:Date):void {
			_schedule = value;
		}

		public function get guideline():Guideline {
			return _guideline;
		}

		public function set guideline(value:Guideline):void {
			_guideline = value;
		}

		public function get city():City {
			return _city;
		}

		public function set city(value:City):void {
			_city = value;
		}
	}
}
