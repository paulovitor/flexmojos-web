package tv.snews.anews.domain {

	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe enum que define os possíveis status dos documentos na gaveta.
	 *
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	public class DrawerDocumentStatus {

		public static const SUCCESS:String = "SUCCESS";

        public static const ALREADY_INCLUDED:String = "ALREADY_INCLUDED";
		
	}
}
