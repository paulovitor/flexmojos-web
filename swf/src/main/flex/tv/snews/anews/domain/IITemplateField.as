package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.IITemplateField")]
public class IITemplateField extends AbstractEntity_Int {

	public var name:String;
	public var number:int;
	public var size:int;

	public var template:IITemplate;

	public function IITemplateField() {
		super(this);
	}
}
}
