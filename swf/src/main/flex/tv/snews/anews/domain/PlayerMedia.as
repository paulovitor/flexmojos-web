package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

import tv.snews.flexlib.utils.DateUtil;

import tv.snews.flexlib.utils.StringUtils;

/**
 * Classe utilizada para DTO do playe de video.
 *
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
[Bindable]
public class PlayerMedia {

    private static const dateUtil:DateUtil = new DateUtil();

    public var abstractText:String;
    public var group:String;
    public var type:String;
    public var url:String;
    public var revision:int;
    public var slug:String;
    public var durationTime:Date = dateUtil.timeZero();
    public var description:String;
    public var status:Boolean;
    public var createdBy:String;
    public var created:Date;
    public var changedBy:String;
    public var changed:Date;

    //------------------------------
    //	Constructor
    //------------------------------

    public function PlayerMedia() {
        super(); // Just to keep the abstraction
    }

    public function parseToWSMedia(other:WSMedia):void {
        this.abstractText = other.observation;
        this.group = other.classification;
        this.type = MosObjectType.VIDEO;
        this.url = other.streamingURL;
//        this.revision = other.revision;
        this.slug = other.slug;
        this.durationTime = other.durationTime;
        this.description = other.description;
//        this.status = other.status;
//        this.createdBy = other.createdBy;
        this.created = other.eventDate;
//        this.changedBy = other.changedBy;
        this.changed = other.changed;
    }

    public function parseToMosMedia(other:MosMedia):void {
        this.abstractText = other.abstractText;
        this.group = other.group;
        this.type = other.type;
        this.url = other.proxyPaths.getItemAt(0).url;
        this.revision = other.revision;
        this.slug = other.slug;
        this.durationTime = other.durationTime;
        this.description = other.description;
        this.status = other.ready;
        this.createdBy = other.createdBy;
        this.created = other.created;
        this.changedBy = other.changedBy;
        this.changed = other.changed;
    }
}
}
