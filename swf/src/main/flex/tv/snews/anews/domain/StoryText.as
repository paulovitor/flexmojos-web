package tv.snews.anews.domain {
	
	import mx.utils.ObjectUtil;
	
	import tv.snews.flexlib.utils.DateUtil;

	/**
	 * @author Felipe Zap de Mello
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StoryText")]
	public class StoryText extends StorySubSection {

		private static const dateUtil:DateUtil = new DateUtil();
		
		private var _presenter:User;
		private var _readTime:Date;
		private var _text:String = "";

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StoryText(presenter:User=null) {
			super();

            this.presenter = presenter;
            if (presenter) {
                readTime = dateUtil.timeZero();
            }
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		/**
		 * Verifica se o objeto foi alterado.
		 *  
		 * @return retorna true se os objetos forem diferentes.
		 * 
		 */
		override public function isDifferent(other:StorySubSection):Boolean {
			if (super.isDifferent(other)) return true;
			
			const otherText:StoryText = StoryText(other);
			
			// Se o presenter não for null, compara os IDs. Se for null, verifica se o outro é null também.
			if ((presenter && !presenter.equals(otherText.presenter)) || (presenter == null && otherText.presenter != null)) {
				return true;
			}
			
			if (text != otherText.text) {
				return true;
			}
			
			return false;
		}
		
		public function calculateReadTime():Date {
			readTime = presenter == null ? dateUtil.timeZero() : presenter.calculateReadTime(text);
			return readTime;
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function get text():String {
			return _text;
		}

		public function set text(value:String):void {
			_text = value;
		}

		public function get readTime():Date {
			return _readTime;
		}

		public function set readTime(value:Date):void {
			_readTime = value;
		}

		public function get presenter():User {
			return _presenter;
		}

		public function set presenter(value:User):void {
			_presenter = value;
		}
	}
}
