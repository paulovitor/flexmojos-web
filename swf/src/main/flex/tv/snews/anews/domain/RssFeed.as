package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Representa um feed RSS cadastrado pelo usuário.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.RssFeed")]
	public class RssFeed extends AbstractEntity_Int {

		private var _name:String;
		private var _url:String;
		private var _lastBuild:Date;
		private var _category:RssCategory;
		
		[ArrayElementType("tv.snews.anews.domain.RssItem")]
		private var _items:ArrayCollection = new ArrayCollection();
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function RssFeed() {
			super(this); // Just to keep the abstraction
		}

		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get category():RssCategory {
			return _category;
		}

		public function set category(value:RssCategory):void {
			_category = value;
		}

		public function get items():ArrayCollection {
			return _items;
		}

		public function set items(value:ArrayCollection):void {
			_items = value;
		}

		public function get lastBuild():Date {
			return _lastBuild;
		}

		public function set lastBuild(value:Date):void {
			_lastBuild = value;
		}

		public function get url():String {
			return _url;
		}

		public function set url(value:String):void {
			_url = value;
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
	}
}
