package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Classe utilizada para persistir as informações de um 
	 * dispositivo MosObjectPath
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.MosPath")]
	public class MosPath extends MosAbstractPath {

	}
}
