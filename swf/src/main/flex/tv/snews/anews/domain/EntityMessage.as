package tv.snews.anews.domain {

	/**
	 * Classe que representa as mensagens de entidades enviadas dentro do sistema, tanto as
 	 * mensagens da comunicação instantânea quanto as mensagens offline.
	 * 
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.EntityMessage")]
	public class EntityMessage extends AbstractMessage {
		
		private var _idEntity:int;
		private var _type:String;
		private var _description:String;
			
		public function EntityMessage() {
			super(this); // Just to keep the abstraction
		}

		public function get idEntity():Number {
			return _idEntity;
		}
		
		public function set idEntity(value:Number):void	{
			_idEntity = value;
		}
		
		public function get type():String	{
			return _type;
		}

		public function set type(value:String):void {
			_type = value;
		}
		
		public function get description():String {
			return _description;
		}
		
		public function set description(value:String):void {
			_description = value;
		}
	}
}
