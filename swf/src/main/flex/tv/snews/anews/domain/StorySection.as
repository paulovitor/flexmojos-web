package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;

import tv.snews.anews.component.Info;

import tv.snews.anews.utils.Util;

import tv.snews.flexlib.utils.DateUtil;
	import tv.snews.flexlib.utils.ObjectUtils;

	/**
	 * Representa uma uma sessão da lauda
	 * que engloba suas sub sessões 
	 *
	 * @author Paulo Felipe.
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StorySection")]
	public class StorySection extends AbstractEntity_Number {
		
		private static const dateUtil:DateUtil = new DateUtil();
        private static const bundle:IResourceManager = ResourceManager.getInstance();
		
		private var _duration:Date = dateUtil.timeZero();
		private	var _subSections:ArrayCollection = new ArrayCollection();

		[Transient] public var name:String;
        [Transient] public var collapsed:Boolean;
        [Transient] public var story:IStory;
		
		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StorySection() {
			super(this); // Just to keep the abstraction.
		}
		
		//----------------------------------------------------------------------
		//	Helpers
		//----------------------------------------------------------------------
		
		public function removeSubSection(subSection:StorySubSection):Boolean {
			var removed:Boolean = subSections.removeItemAt(subSections.getItemIndex(subSection));
			if (removed) {
				story.updateTimes();
				renameSubSections();
			}
			return removed;
		}
		
		public function addSubSection(subSection:StorySubSection):void {
			subSection.storySection = this;
			subSection.order = subSections.length;
			subSections.addItem(subSection);
			renameSubSections();
		}

        public function addVideoSubSections(selectedMedias:ArrayCollection, playoutDevice:Device):void {
            for each (var media:Media in selectedMedias) {
                if (existMediaInSubSections(media)) {
                    ANews.showInfo(bundle.getString('Bundle', 'mediaCenter.mediaSearchWindow.msg.mediaAlreadyAdded', [media.slug]), Info.WARNING);
                    continue;
                }
                var playout:MosDevice;
                if (media is MosMedia) {
                    var storyMosObj:StoryMosVideo = new StoryMosVideo();
                    storyMosObj.mosMedia = media as MosMedia;
                    if (playoutDevice is MosDevice) {
                        playout = MosDevice(playoutDevice);
                        storyMosObj.channel = playout.defaultChannel;
                    }
                    addSubSection(storyMosObj);
                } else if (media is WSMedia) {
                    var storyWSVideo:StoryWSVideo = new StoryWSVideo();
                    storyWSVideo.media = media as WSMedia;
                    if (playoutDevice is MosDevice) {
                        playout = MosDevice(playoutDevice);
                        storyWSVideo.channel = playout.defaultChannel;
                    }
                    addSubSection(storyWSVideo);
                }
            }
        }

        private function existMediaInSubSections(media:Media):Boolean {
            for each (var storySubSection:StorySubSection in subSections) {
                if (storySubSection is StoryMosVideo && media is MosMedia) {
                    var storyMosVideo:StoryMosVideo = storySubSection as StoryMosVideo;
                    if (storyMosVideo.mosMedia != null && !storyMosVideo.mosMedia.isDifferent(media))
                        return true;
                } else if (storySubSection is StoryWSVideo && media is WSMedia) {
                    var storyWSVideo:StoryWSVideo = storySubSection as StoryWSVideo;
                    if (storyWSVideo.media != null && !storyWSVideo.media.isDifferent(media))
                        return true;
                }
            }
            return false;
        }
		
		public function renameSubSections():void {
            if (subSections) {
                const isFooter:Boolean = this == story.footerSection;
                const isOff:Boolean = this == story.offSection;

                var camCount:int = 0;
                var ncTextCount:int = 0;
                var offTextCount:int = 0;
                var mosCount:int = 0;

                for (var i:int = 0; i < subSections.length; i++) {
                    var sub:Object = subSections.getItemAt(i);

                    if (sub is StoryCameraText) {
                        var camText:StoryCameraText = StoryCameraText(sub);
                        if (isFooter) {
                            camText.name = bundle.getString("Bundle", "story.footerNote");
                        } else {
                            camText.name = Util.formatName(bundle.getString("Bundle", "story.headTitle"), ++camCount);
                        }
                    } else if (sub is StoryText) {
                        if (isOff) {
                            var offStoryText:StoryText = sub as StoryText;
                            offStoryText.name = Util.formatName(bundle.getString("Bundle", "story.offTitle"), ++offTextCount);
                        } else {
                            var ncStoryText:StoryText = sub as StoryText;
                            ncStoryText.name = Util.formatName(bundle.getString("Bundle", "story.ncTitle"), ++ncTextCount);
                        }
                    } else if (sub is StoryMosVideo) {
                        var storyMosObj:StoryMosVideo = sub as StoryMosVideo;
                        storyMosObj.name = Util.formatName(bundle.getString("Bundle", "story.mosObjTitle"), ++mosCount);
                    } else if (sub is StoryWSVideo) {
                        var storyWsVideo:StoryWSVideo = sub as StoryWSVideo;
                        storyWsVideo.name = Util.formatName(bundle.getString("Bundle", "story.mosObjTitle"), ++mosCount);
                    }
                }
            }
		}
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		public function subSectionsByType(type:Class):ArrayList {
			var results:ArrayList = new ArrayList();
			
			for each (var subSection:StorySubSection in subSections) {
				if (subSection is type) {
					results.addItem(subSection);
				}
			}
			
			return results;
		}

		public function subSectionsByVideoType():ArrayList {
			var results:ArrayList = new ArrayList();

			for each (var subSection:StorySubSection in subSections) {
				if (subSection is StoryWSVideo || subSection is StoryMosVideo) {
					results.addItem(subSection);
				}
			}

			return results;
		}

		/**
		 * Verifica se o objeto foi alterado.
		 *  
		 * @return retorna true se os objetos forem diferentes.
		 * 
		 **/
		public function isDifferent(other:StorySection):Boolean {
			if (other == null) return true;
			
			// Verifica se são da mesma classe
			if (!(this is ObjectUtils.getClass(other))) {
				return true;
			}
			
			if (subSections.length != other.subSections.length) {
				return true;
			}
			
			for (var i:int = 0; i < subSections.length; i++) {
				var thisSubSection:StorySubSection = StorySubSection(subSections.getItemAt(i));
				var otherSubSection:StorySubSection = StorySubSection(other.subSections.getItemAt(i));
				
				if (thisSubSection.isDifferent(otherSubSection)) {
					return true;
				}
			}
			
			return false;
		}

		public function calculateDuration():Date {
			duration = dateUtil.timeZero();

			for each (var subSection:StorySubSection in subSections) {
				/*
				 * Por enquanto as subSections das subSections não precisam
				 * calcular tempo, então elas não são tratadas nesse código.
				 */

				var gatoDoJenkinsParaTirarMillis:Date;
				if (subSection is StoryMosVideo) {
					var storyMosObj:StoryMosVideo = StoryMosVideo(subSection);
					gatoDoJenkinsParaTirarMillis = dateUtil.stringToTime(dateUtil.timeToString(storyMosObj.durationTime));
					duration = dateUtil.sumTimes(duration, gatoDoJenkinsParaTirarMillis);
				}
				if (subSection is StoryWSVideo) {
					var storyWSObj:StoryWSVideo = StoryWSVideo(subSection);
					gatoDoJenkinsParaTirarMillis = dateUtil.stringToTime(dateUtil.timeToString(storyWSObj.durationTime));
					duration = dateUtil.sumTimes(duration, gatoDoJenkinsParaTirarMillis);
				}
				if (subSection is StoryText) {
					var storyText:StoryText = StoryText(subSection);
					duration = dateUtil.sumTimes(duration, storyText.calculateReadTime());
				}
			}

			return duration;
		}
		
		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function get duration():Date {
			return _duration;
		}
		
		public function set duration(value:Date):void {
			_duration = value || dateUtil.timeZero();
		}
		
		
		[ArrayElementType("tv.snews.anews.domain.StorySubSection")]
		public function get subSections():ArrayCollection {
			return _subSections;
		}

		public function set subSections(value:ArrayCollection):void {
			_subSections = value;
			renameSubSections();
		}
	}
}
