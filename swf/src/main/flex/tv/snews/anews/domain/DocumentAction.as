package tv.snews.anews.domain{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;
	
	/**
	 * Representa as ações dos documentos
	 * 
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	public class DocumentAction {
		
		public static const SENT_TO_DRAWER:String = "SENT_TO_DRAWER";
		
		public function DocumentAction() {}
		
		public static function getAllActions():ArrayCollection {
			
			var actions:ArrayCollection = new ArrayCollection();
			for each (var key:String in [SENT_TO_DRAWER]) {
				var obj:Object = new Object();
				obj.id = key;
				obj.label = i18nOf(key);
				actions.addItem(obj);
			}
			return actions;
		}
		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String):String {
			var LABELS:Object = new Object();
			if (StringUtils.isNullOrEmpty(LABELS[type])) {
				switch (type) {
					case DocumentAction.SENT_TO_DRAWER.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "document.log.lbSentToDrawer");  break;
					default:
						break;
				}
			}
			return LABELS[type];
		}
	}
}