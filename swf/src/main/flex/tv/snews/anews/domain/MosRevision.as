package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class MosRevision {

	public static const V_26:String = "V_26";
	public static const V_28:String = "V_28";
	public static const V_281:String = "V_281";
	public static const V_282:String = "V_282";
	public static const V_283:String = "V_283";
	public static const V_284:String = "V_284";

	public static const values:ArrayCollection = new ArrayCollection([
		{ id: V_26, label: "2.6" },
		{ id: V_28, label: "2.8" },
		{ id: V_281, label: "2.8.1" },
		{ id: V_282, label: "2.8.2" },
		{ id: V_283, label: "2.8.3" },
		{ id: V_284, label: "2.8.4" }
	]);

	public static function entry(value:String):Object {
		for each (var entry:Object in values) {
			if (entry.id == value) {
				return entry;
			}
		}
		throw new Error("Entry not found for " + value);
	}
}
}
