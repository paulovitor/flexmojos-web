package tv.snews.anews.domain {

	/**
	 * Classe que representa as mensagens enviadas dentro do sistema, tanto as
 	 * mensagens da comunicação instantânea quanto as mensagens offline.
	 * 
	 * @author Felipe Pinheiro
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.TextMessage")]
	public class TextMessage extends AbstractMessage {

		public static const LENGTH_LIMIT:int = 140;
		
		private var _content:String;
		
		public function TextMessage() {
			super(this); // Just to keep the abstraction
		}

		//----------------------------------------------------------------------
		//	Helpers
		//----------------------------------------------------------------------
		
		private function limitMessage(value:String):String {
			if (value) {
				return value.length > LENGTH_LIMIT ? value.substr(0, LENGTH_LIMIT) : value;
			}
			return value;
		}
		
		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function set content(value:String):void {
			// Por segurança, limita a quantidade de caracteres manualmente
			_content = limitMessage(value);
		}
		
		public function get content():String {
			return _content;
		}
	}
}
