package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.MosDeviceChannel")]
public class MosDeviceChannel extends AbstractEntity_Int {

	public var name:String;
	public var device:MosDevice;

	public function MosDeviceChannel(name:String = null) {
		super(this);
		this.name = name;
	}

	override public function equals(obj:Object):Boolean {
		if (this == obj) {
			return true;
		}
		var other:MosDeviceChannel = obj as MosDeviceChannel;
		return other != null && name == other.name;
	}
}
}
