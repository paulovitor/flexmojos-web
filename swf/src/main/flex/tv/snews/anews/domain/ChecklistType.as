package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.ChecklistType")]
public class ChecklistType extends AbstractEntity_Int {

    private var _name:String = ""; // não tire essa iniciação

    //----------------------------------
    //  Constructor
    //----------------------------------

    public function ChecklistType() {
        super(this);
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    public function copy(other:ChecklistType):void {
        this.name = other.name;
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }
}
}
