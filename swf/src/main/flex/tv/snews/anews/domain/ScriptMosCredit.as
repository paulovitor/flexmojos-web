package tv.snews.anews.domain {
import mx.collections.ArrayCollection;

import tv.snews.anews.mos.NcsItemCredit;

/**
 * @author Samuel Guedes de Melo.
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ScriptMosCredit")]
public class ScriptMosCredit extends AbstractScriptItem {

    public var mosStatus:String;
    public var mosMedia:MosMedia;
    public var slug:String;
    public var channel:String;

    override public function isDifferent(other:AbstractScriptItem):Boolean {
        if (super.isDifferent(other)) return true;

        const otherScriptMosCredit:ScriptMosCredit = ScriptMosCredit(other);
        if ((slug != otherScriptMosCredit.slug) ||
                (channel != otherScriptMosCredit.channel) ||
                (!this.mosMedia.equals(otherScriptMosCredit.mosMedia))) {
            return true;
        }

        return false;
    }

    public function updateFields(other:ScriptMosCredit):void {
        this.slug = other.slug;
        this.channel = other.channel;
        this.mosMedia = other.mosMedia;
        this.mosStatus = other.mosStatus;
		this.disabled = other.disabled;
    }

    /**
     * Limpa o ID desse crédito e de seus fields para que sejam cadastrados novamente.
     */
    public function resetIDs():void {
        this.id = undefined;
        this.mosMedia.id = undefined;
    }

    public function parseScriptMosCredit(ncsItemCredit:NcsItemCredit):void {
        this.id = ncsItemCredit.id;
        this.slug = ncsItemCredit.slug;
        this.channel = ncsItemCredit.channel;
        this.mosMedia = ncsItemCredit.mosMedia;
    }
}
}
