package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;
	import mx.collections.IViewCursor;

	/**
	 * Representa uma conversa, ou seja, um grupo de mensagens que envolvem os
	 * mesmos membros.
	 * 
	 * @author Felipe Pinheiro
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.Chat")]
	public class Chat extends AbstractEntity_Number {

		[ArrayElementType("tv.snews.anews.domain.User")]
		private var _members:ArrayCollection = new ArrayCollection();
		
		[ArrayElementType("tv.snews.anews.domain.User")]
		private var _leftMembers:ArrayCollection = new ArrayCollection();
		
		[ArrayElementType("tv.snews.anews.domain.AbstractMessage")]
		private var _messages:ArrayCollection = new ArrayCollection();
		private var _lastInteraction:Date;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function Chat() {
			super(this); // Just to keep the abstraction
		}

		//------------------------------
		//	Operations
		//------------------------------
		
		public function addMessage(message:AbstractMessage):void {
			message.chat = this;
			_messages.addItem(message);
		}
		
		/**
		 * Retorna a lista de usuários membros do chat que não sairam da conversa.
		 */
		public function activeMembers():ArrayCollection {
			var activeMembers:ArrayCollection = new ArrayCollection();
			for each (var member:User in _members) {
				if (!userLeftChat(member)) {
					activeMembers.addItem(member);
				}
			}
			return activeMembers;
		}
		
		/**
		 * Verifica se o usuário informado está dentro da lista de participantes da
	 	 * conversa.
	 	 */
		public function containsMember(user:User):Boolean {
			for each (var member:User in _members) {
				if (member.id == user.id) {
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Verifica se o usuário informado está dentro da lista de usuários que
		 * sairam da conversa.
		 */
		public function userLeftChat(user:User):Boolean {
			for each (var member:User in _leftMembers) {
				if (member.id == user.id) {
					return true;
				}
			}
			return false;
		}
		
		/**
		 * Adiciona um ou mais usuários à lista de participantes do chat. Será
		 * retornada uma lista de usuários que foram adicionados na conversa por
		 * esse método, já que aqueles que já participavam não serão adicionados
		 * novamente.
		 */
		public function addMembers(newMembers:ArrayCollection):ArrayCollection {
			var addedMembers:ArrayCollection = new ArrayCollection();
			for each (var newMember:User in newMembers) {
				if (!containsMember(newMember)) {
					_members.addItem(newMember);
					addedMembers.addItem(newMember);
				} else if (userLeftChat(newMember)) {
					reJoinMember(newMember);
					addedMembers.addItem(newMember);
				}
			}
			return addedMembers;
		}
		
		/**
		 * Registra a saída de um usuário da conversa. Ele ainda estara na lista de
		 * membros do chat, mas novas mensagens não deverão adicionar ele como
		 * destinatário.
		 * 
		 * Esse método irá retornar um boleano indicando o resultado dessa operação;
		 * isso para evitar falhas de lógica onde o usuário já teve sua saída
		 * registrada ou até no caso em que ele não consta como um membro do chat.
		 */
		public function registerLeave(member:User):Boolean {
			if (containsMember(member) && !userLeftChat(member)) {
				_leftMembers.addItem(member);
				return true;
			}
			return false;
		}
		
		/**
		 * Remove o usuário especificado da lista dos que abandonaram a conversa, ou
		 * seja, faz com que o usuário volte a conversa.
		 */
		public function reJoinMember(user:User):void {
			var cursor:IViewCursor = _leftMembers.createCursor();
			while (!cursor.afterLast) {
				var leftMember:User = cursor.current as User;
				if (leftMember.id == user.id) {
					cursor.remove();
				}
				cursor.moveNext();
			}
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function set members(value:ArrayCollection):void {
			_members = value;
		}
		
		public function get members():ArrayCollection {
			return _members;
		}

		public function set leftMembers(value:ArrayCollection):void {
			_leftMembers = value;
		}
		
		public function get leftMembers():ArrayCollection {
			return _leftMembers;
		}
		
		public function set messages(value:ArrayCollection):void {
			_messages = value != null ? value : new ArrayCollection();
		}

		public function get messages():ArrayCollection {
			return _messages;
		}

		public function set lastInteraction(value:Date):void {
			_lastInteraction = value;
		}

		public function get lastInteraction():Date {
			return _lastInteraction;
		}
	}
}
