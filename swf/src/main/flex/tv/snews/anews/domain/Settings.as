package tv.snews.anews.domain {

/**
 * Entidade que representa as configurações do sistema.
 *
 * @author Felipe Pinheiro
 * @author Eliezer Reis
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Settings")]
public class Settings extends AbstractEntity_Int {

	private static const DEFAULT_FONT_FAMILY:String = "Arial";
	private static const PROGRAM:String = "program";

	public var name:String;
	public var locale:String;

	public var storageLocation:String;
	public var storageLocationBannerLogin:String;
	public var storageLocationSystemLogo:String;
	public var storageLocationReportLogo:String;
	public var defaultDomain:String;

	public var sessionExpiration:int;
	public var passwordExpiration:int;

	public var cleaningRound:int;
	public var cleaningRss:int;
	public var cleaningTweets:int;
	public var cleaningReportageVersions:int;
	public var cleaningGuidelineVersions:int;
	public var cleaningGuidelinesTrash:int;
	public var cleaningStoriesTrash:int;
	public var cleaningStoryVersions:int;
	public var cleaningChecklistsTrash:int;

	public var editorSpellCheck:Boolean;
	public var editorUpperCase:Boolean;
	public var editorFontSize:int;
	private var _defaultGuidelineGridSort:String;

	//Ativa o streaming do anews para visualização dos videos.
	public var serverStreaming:Boolean;
	public var otherHostActive:Boolean;
	public var host:String;
	public var rootDirectory:String;
	public var protocol:String;

	private var _editorFontFamily:String;
	private var _reportFontFamily:String;

	public var reportFontSize:int;
	public var colorAgency:uint;
	public var colorAgenda:uint;
	public var colorChecklist:uint;
	public var colorGuideline:uint;
	public var colorReporter:uint;
	public var colorRundown:uint;
	public var colorScript:uint;
	public var colorPersonal:uint;
	public var colorSettings:uint;
	public var colorEdition:uint;
	public var colorMediaCenter:uint;
	public var colorReport:uint;
	public var defaultColor:uint;

	public var displayIndexAlert:Boolean;

    public var enableGuidelineTeam:Boolean;
    public var enableRundownLoadButton:Boolean;
    public var enableRundownImageEditor:Boolean;
    public var enableRundownAuthor:Boolean;
    public var enableScriptPlanImageEditor:Boolean;
    public var enableScriptPlanAuthor:Boolean;
    public var enableScriptPlanLoadButton:Boolean;

	// Stand by será incluído, por padrão, na impressão (sim/não)
	public var includeStandBy:Boolean;

	[Transient]
	public var currentLocale:String;

	//------------------------------
	//	Constructor
	//------------------------------

	public function Settings() {
		super(this);
	}

	public function updateFields(settings:Settings):void {
		id = settings.id;

		name = settings.name;
		locale = settings.locale;

		storageLocation = settings.storageLocation;
		storageLocationBannerLogin = settings.storageLocationBannerLogin;
		storageLocationSystemLogo = settings.storageLocationSystemLogo;
		storageLocationReportLogo = settings.storageLocationReportLogo;

		sessionExpiration = settings.sessionExpiration;
		passwordExpiration = settings.passwordExpiration;

		cleaningRound = settings.cleaningRound;
		cleaningRss = settings.cleaningRss;
		cleaningTweets = settings.cleaningTweets;
		cleaningReportageVersions = settings.cleaningReportageVersions;
		cleaningGuidelineVersions = settings.cleaningGuidelineVersions;
		cleaningGuidelinesTrash = settings.cleaningGuidelinesTrash;
		cleaningStoriesTrash = settings.cleaningStoriesTrash;
		cleaningStoryVersions = settings.cleaningStoryVersions;
		cleaningChecklistsTrash = settings.cleaningChecklistsTrash;

		editorSpellCheck = settings.editorSpellCheck;
		editorUpperCase = settings.editorUpperCase;
		editorFontSize = settings.editorFontSize;
		editorFontFamily = settings.editorFontFamily;

		serverStreaming = settings.serverStreaming;
		otherHostActive = settings.otherHostActive;
		host = settings.host;
		rootDirectory = settings.rootDirectory;
		protocol = settings.protocol;

		defaultGuidelineGridSort = settings.defaultGuidelineGridSort;

		reportFontFamily = settings.reportFontFamily;
		reportFontSize = settings.reportFontSize;

		colorAgency = settings.colorAgency;
		colorAgenda = settings.colorAgenda;
		colorChecklist = settings.colorChecklist;
		colorGuideline = settings.colorGuideline;
		colorReporter = settings.colorReporter;
		colorRundown = settings.colorRundown;
		colorScript = settings.colorScript;
		colorPersonal = settings.colorPersonal;
		colorSettings = settings.colorSettings;
		colorEdition = settings.colorEdition;
		colorMediaCenter = settings.colorMediaCenter;
		colorReport = settings.colorReport;
		defaultColor = settings.defaultColor;

		displayIndexAlert = settings.displayIndexAlert;

        enableGuidelineTeam = settings.enableGuidelineTeam;
		enableRundownLoadButton = settings.enableRundownLoadButton;
		enableRundownImageEditor = settings.enableRundownImageEditor;
		enableRundownAuthor = settings.enableRundownAuthor;
        enableScriptPlanAuthor = settings.enableScriptPlanAuthor;
        enableScriptPlanImageEditor = settings.enableScriptPlanImageEditor;
        enableScriptPlanLoadButton = settings.enableScriptPlanLoadButton;

		includeStandBy = settings.includeStandBy;

        defaultDomain = settings.defaultDomain;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public function get editorFontFamily():String {
		return _editorFontFamily;
	}

	public function set editorFontFamily(value:String):void {
		_editorFontFamily = value || DEFAULT_FONT_FAMILY;
	}

	public function get reportFontFamily():String {
		return _reportFontFamily;
	}

	public function set reportFontFamily(value:String):void {
		_reportFontFamily = value || DEFAULT_FONT_FAMILY;
	}

	public function get defaultGuidelineGridSort():String {
		return _defaultGuidelineGridSort;
	}

	public function set defaultGuidelineGridSort(value:String):void {
		_defaultGuidelineGridSort = value || PROGRAM;
	}
}
}
