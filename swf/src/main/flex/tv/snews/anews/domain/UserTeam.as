package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Entidade que representa as equipes da pauta.
	 * 
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.Team")]
	public class UserTeam extends AbstractEntity_Int {

		private var _name:String;

		[Transient]
        private var _users:ArrayCollection = new ArrayCollection();
		
		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function UserTeam() {
			super(this); // Just to keep the abstraction.
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		/**
		 * Copia todos os valores do grupo informado pelo parâmetro para este
		 * grupo.
		 *
		 * @param other Equipe do qual serão copiados os atributos.
		 */
		public function copy(other:UserTeam):void {
			this.id = other.id;
			this.name = other.name;
			//this.users = other.users; Quando vem do java, esta lista vem fazia.
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function set name(value:String):void {
			_name = value;
		}

		public function get name():String {
			return _name;
		}


        public function get users():ArrayCollection {
            return _users;
        }

        public function set users(value:ArrayCollection):void {
            _users = value;
        }
    }
}