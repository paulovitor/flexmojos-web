package tv.snews.anews.domain {

/**
 * Representa um tweet dentro do sistema.
 *
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */

[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.TweetInfo")]
public class TweetInfo extends AbstractEntity_Number {

    private var _tweetId:String;
    private var _text:String;
    private var _createdAt:Date;
    private var _twitterUser:TwitterUser;

    public function TweetInfo() {
        super(this); // Just to keep the abstraction
    }

    public function get tweetId():String {
        return _tweetId;
    }

    public function set tweetId(value:String):void {
        _tweetId = value;
    }

    public function get text():String {
        return _text;
    }

    public function set text(value:String):void {
        _text = value;
    }

    public function get createdAt():Date {
        return _createdAt;
    }

    public function set createdAt(value:Date):void {
        _createdAt = value;
    }

    public function get twitterUser():TwitterUser {
        return _twitterUser;
    }

    public function set twitterUser(value:TwitterUser):void {
        _twitterUser = value;
    }
}
}