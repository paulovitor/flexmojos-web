package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.IIDevicePath")]
public class IIDevicePath extends AbstractEntity_Int {

	public var path:String;
	public var device:IIDevice;

	public function IIDevicePath(path:String = null) {
		super(this);
		this.path = path;
	}
}
}
