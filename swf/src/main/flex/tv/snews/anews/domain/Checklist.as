package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Checklist")]
public class Checklist extends AbstractVersionedEntity {

	private static const dateUtil:DateUtil = new DateUtil();

	private var _date:Date;
	public var schedule:Date;
	public var departure:Date;
	public var arrival:Date;

	public var modified:Date = new Date();

	private var _slug:String;
	public var local:String;
	public var info:String;
	public var tapes:String;

	public var done:Boolean = false;
	public var excluded:Boolean = false;

	public var producer:User;

	private var _guideline:Guideline;
	public var program:Program;
	public var type:ChecklistType;

	private var _resources:ArrayCollection = new ArrayCollection();
	private var _vehicles:ArrayCollection = new ArrayCollection();
	private var _editors:ArrayCollection = new ArrayCollection();

	[ArrayElementType("tv.snews.anews.domain.ChecklistTask")]
	private var _tasks:ArrayCollection = new ArrayCollection();

	// Não persistido
	public var editingUser:User = null;
	[Transient] public var vehiclesAsStr:String = "";
	[Transient] public var resourceAsStr:String = "";
	[Transient] public var tasksAsStr:String = "";
	[Transient] public var editorsAsStr:String = "";

	//----------------------------------
	//	Constructor
	//----------------------------------

	public function Checklist(guideline:Guideline = null) {
		super(this);
		this.guideline = guideline;
		vehicles.addEventListener(CollectionEvent.COLLECTION_CHANGE, onVehiclesChange);
		resources.addEventListener(CollectionEvent.COLLECTION_CHANGE, onResourcesChange);
		tasks.addEventListener(CollectionEvent.COLLECTION_CHANGE, onTasksChange);
		_editors.addEventListener(CollectionEvent.COLLECTION_CHANGE, onEditorsChange);
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function get editors():ArrayCollection {
		return _editors;
	}
	
	public function set editors(value:ArrayCollection):void {
		if (_editors.length == 0 && value.length == 0) {
			onEditorsChange();
		}
		
		// Nunca deve setar null em _producers
		_editors.removeAll();
		if (value) {
			_editors.addAll(value);
		}
	}
	
	public function get date():Date {
		return _date;
	}

	public function set date(value:Date):void {
		_date = value;
	}

	public function get slug():String {
		return _slug;
	}

	public function set slug(value:String):void {
		_slug = value;
	}

	override public function updateFields(other:AbstractVersionedEntity):void {
		super.updateFields(other);

		if (other is Checklist) {
			var otherChecklist:Checklist = other as Checklist;

			this.date = otherChecklist.date;
			this.schedule = otherChecklist.schedule;
			this.departure = otherChecklist.departure;
			this.arrival = otherChecklist.arrival;
			this.modified = otherChecklist.modified;
			this.slug = otherChecklist.slug;
			this.local = otherChecklist.local;
			this.info = otherChecklist.info;
			this.tapes = otherChecklist.tapes;
			this.done = otherChecklist.done;
			this.excluded = otherChecklist.excluded;
			this.producer = otherChecklist.producer;
			this.guideline = otherChecklist.guideline;
			this.type = otherChecklist.type;

			// Atualiza as coleções evitando valores null provenientes de lazy
			this.editors = otherChecklist.editors || editors;
			this.resources = otherChecklist.resources || resources;
			this.tasks = otherChecklist.tasks || tasks;
			this.vehicles = otherChecklist.vehicles || vehicles;
		}
	}

	public function updateFieldsFromGuideline(value:Guideline):void {
		// Não há nada para sincronizar
		if (value == null)
			return;
		
		this.slug = value.slug == null ? "" : value.slug;
		this.date = value.date || new Date();
		this.vehicles = value.vehicles;
		this.editors = value.editors;
		
		//No momento da criação, sincronizo alguns dados extras
		if (this.id <= 0 || isNaN(this.id)) {
			if (value.firstGuide()) {
				this.schedule = this.schedule || value.firstGuide().schedule;
				this.local =  this.local || value.firstGuide().address;
			}
		}
	}
	
	public function isTheSame(other:Checklist):Boolean {
		if (!other) {
			return false;
		}

		// Verifica as propriedades de tipo primitivo
		var keys:Array = [ 'slug', 'local', 'info', 'tapes' ];
		for each (var key:String in keys) {
			
			//quando possui pauta esse dado vem da pauta.
			if (key == 'slug' && this.guideline) {
				continue;
			}
			
			if (this[key] != other[key]) {
				return false;
			}
		}

		// Verifica as entidades
		if (type != other.type && !(type && type.equals(other.type))) {
			return false;
		}
		if (producer != other.producer && !(producer && producer.equals(other.producer))) {
			return false;
		}

		// Verifica as propriedades Date
		if (dateUtil.dateToString(date) != dateUtil.dateToString(other.date) && !this.guideline) {
			return false;
		}

		// Verifica as propriedades Time
		keys = [ 'schedule', 'departure', 'arrival' ];
		for each (key in keys) {
			if (dateUtil.timeToString(this[key]) != dateUtil.timeToString(other[key])) {
				return false;
			}
		}

		var found:Boolean;

		// Verifica mudança nos vehicles
		if (vehicles.length != other.vehicles.length && !this.guideline) {
			return false;
		}
		
		if (!this.guideline) {
			for each (var vehicle:String in vehicles) {
				found = false;
				for each (var otherVehicle:String in other.vehicles) {
					if (vehicle == otherVehicle) {
						found = true;
					}
				}
				if (!found) {
					return false;
				}
			}
		}

		
		// Verifica mudança nos editors
		if (editors.length != other.editors.length && !this.guideline ) {
			return false;
		}
		
		if (!this.guideline) {
			for each (var editor:User in editors) {
				found = false;
				for each (var otherEditor:User in other.editors) {
					if (editor.equals(otherEditor)) {
						found = true;
					}
				}
				if (!found) {
					return false;
				}
			}
		}
		
		// Verifica mudança nos recursos
		if (resources) {
			if (resources.length != other.resources.length) {
				return false;
			}
			for each (var resourceA:ChecklistResource in resources) {
				found = false;
				for each (var resourceB:ChecklistResource in other.resources) {
					if (resourceA.equals(resourceB)) {
						found = true;
						break;
					}
				}
				if (!found) {
					return false;
				}
			}
		}

		// Verifica mudança da aprovação total das tarefas.
		if (done != other.done) {
				return false;
		}

		// Verifica mudança nas tarefas
		if (tasks) {
			if (tasks.length != other.tasks.length) {
				return false;
			}
			for (var i:int = 0; i < tasks.length; i++) {
				var taskA:ChecklistTask = ChecklistTask(tasks.getItemAt(i));
				var taskB:ChecklistTask = ChecklistTask(other.tasks.getItemAt(i));

				if (taskA.done != taskB.done || taskA.label != taskB.label || taskA.info != taskB.info
						|| dateUtil.dateToString(taskA.date) != dateUtil.dateToString(taskB.date)
						|| dateUtil.timeToString(taskA.time) != dateUtil.timeToString(taskB.time)) {
					return false;
				}
			}
		}

		return true;
	}

	public function addTask(task:ChecklistTask):void {
		task.checklist = this;
		tasks.addItem(task);

		// Reordena as tasks
		for (var i:int = 0; i < tasks.length; i++) {
			ChecklistTask(tasks.getItemAt(i)).index = i;
		}
	}

	public function removeTask(task:ChecklistTask):void {
		task.checklist = null;
		tasks.removeItemAt(tasks.getItemIndex(task));

		// Reordena as tasks
		for (var i:int = 0; i < tasks.length; i++) {
			ChecklistTask(tasks.getItemAt(i)).index = i;
		}
	}

	private function onVehiclesChange(event:CollectionEvent):void {
		var joinedVehicles:String = joinVehicles();
		if (vehiclesAsStr != joinedVehicles) {
			vehiclesAsStr = joinedVehicles;
		}
	}
	
	private function onEditorsChange(event:CollectionEvent=null):void {
		var joinResult:String = joinNicknames(editors);
		if (joinResult != editorsAsStr) {
			editorsAsStr = joinResult;
		}
	}

	private function onResourcesChange(event:CollectionEvent):void {
		var joinedResource:String = joinResources();
		if (resourceAsStr != joinedResource) {
			resourceAsStr = joinedResource;
		}
	}
	
	private function onTasksChange(event:CollectionEvent):void {
		var joinedTasks:String = joinTasks();
		if (tasksAsStr != joinedTasks) {
			tasksAsStr = joinedTasks;
		}
	}

	private function joinVehicles():String {
		var names:Array = [];
		for each (var vehicle:String in vehicles) {
			names.push(ResourceManager.getInstance().getString('Bundle', 'vehicle.' + vehicle.toLowerCase()));
		}
		return concatNames(names);
	}

	private function joinResources():String {
		var names:Array = [];
		for each (var resource:ChecklistResource in resources) {
			names.push(resource.name);
		}
		return concatNames(names);
	}

	private function joinTasks():String {
		return pendecies();
	}
	
	private function joinNicknames(users:ArrayCollection):String {
		var nicknames:Array = [];
		for each (var user:User in users) {
			nicknames.push(user.nickname);
		}
		return concatNames(nicknames);
	}
	
	private function concatNames(names:Array):String {
		const bundle:IResourceManager = ResourceManager.getInstance();
		const AND:String = bundle.getString('Bundle', 'defaults.and');
		const NONE:String = bundle.getString('Bundle', 'defaults.none');

		// Junta os nomes separando com vírgula
		var aux:String = names.join(", ");

		// Troca a última vírgula por 'e'
		var index:int = aux.lastIndexOf(",");
		if (index != -1) {
			return aux.substr(0, index) + ' ' + AND + aux.substr(index + 1, aux.length);
		} else {
			return StringUtils.trim(aux).length == 0 ? NONE : aux;
		}
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	[ArrayElementType("tv.snews.anews.domain.ChecklistResource")]
	public function get resources():ArrayCollection {
		return _resources;
	}

	public function set resources(value:ArrayCollection):void {
		_resources.removeAll();
		if (value) {
			_resources.addAll(value);
		}
	}

	public function get vehicles():ArrayCollection {
		return _vehicles;
	}

	public function set vehicles(value:ArrayCollection):void {
		// Nunca deve setar null em _vehicles
		_vehicles.removeAll();
		if (value) {
			_vehicles.addAll(value);
		}
	}

	[Transient]
	private function pendecies():String {
		var pending:int = 0;
		var totalPendency:int = 0;
		if (tasks) {
			totalPendency = tasks.length;
			for each (var checklistTask:ChecklistTask in tasks) {
				if (checklistTask.done) {
					pending++;
				}
			}
		}
		return totalPendency == 0 ? "" : pending + " / " + totalPendency;
	}

	[ArrayElementType("tv.snews.anews.domain.ChecklistRevision")]
	override public function get revisions():ArrayCollection {
		return super.revisions;
	}

	override public function set revisions(value:ArrayCollection):void {
		super.revisions = value;
	}

	public function get guideline():Guideline {
		return _guideline;
	}

	public function set guideline(value:Guideline):void {
		_guideline = value;
		updateFieldsFromGuideline(value);
	}

	public function get tasks():ArrayCollection {

		return _tasks;
	}

	public function set tasks(value:ArrayCollection):void {
		_tasks.removeAll();
		if (value) {
			_tasks.addAll(value);
		}
	}
}
}
