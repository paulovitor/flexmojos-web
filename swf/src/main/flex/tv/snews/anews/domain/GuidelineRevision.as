package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.2.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.GuidelineRevision")]
public class GuidelineRevision extends AbstractRevision {

	public var date:String;
	public var slug:String;

	public var proposal:String;
	public var referral:String;
	public var information:String;
	public var vehicles:String;

	public var program:String;
	public var classification:String;
	public var team:String;
	public var producer:String;
	public var reporter:String;
	public var editor:String;

	public var guides:String;
}
}
