package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

import tv.snews.anews.utils.DomainCache;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.IIDevice")]
public class IIDevice extends Device {

	public var hostname:String;
	public var port:int = 23;
	public var increment:int = 0;
	public var virtual:Boolean = true;

	[ArrayElementType("tv.snews.anews.domain.IIDevicePath")]
	public var paths:ArrayCollection = new ArrayCollection();

	[ArrayElementType("tv.snews.anews.domain.IITemplate")]
	public var templates:ArrayCollection = new ArrayCollection();

	//------------------------------
	//	Constructor
	//------------------------------

	public function IIDevice() {
		super();
		super.protocol = DeviceProtocol.II;
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function addPath(path:IIDevicePath):void {
		path.device = this;
		paths.addItem(path);
	}

	public function removePath(path:IIDevicePath):void {
		var index:int = paths.getItemIndex(path);
		paths.removeItemAt(index);
	}

	public function addTemplate(template:IITemplate):void {
		template.device = this;
		templates.addItem(template);
	}

	public function removeTemplate(template:IITemplate):void {
		var index:int = templates.getItemIndex(template);
		templates.removeItemAt(index);
	}

	override public function update(device:Device):void {
		super.update(device);

		if (device is IIDevice) {
			var other:IIDevice = IIDevice(device);
			this.hostname = other.hostname;
			this.port = other.port;
			this.increment = other.increment;
			this.virtual = other.virtual;
			this.paths = other.paths;
			this.templates = other.templates;
		}
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	override public function set protocol(value:String):void {
		super.protocol = DeviceProtocol.II;
	}

	[Transient]
	public function get virtualObject():Object {
		return DomainCache.YES_OR_NO.getItemAt(virtual ? 0 : 1);
	}

	public function set virtualObject(value:Object):void {
	this.virtual = value ? value.id : false;
	}
}
}
