package tv.snews.anews.domain {

/**
 * Representa uma ronda cadastrada pelo usuário.
 *
 * @author Samuel Guedes de Melo.
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Round")]
public class Round extends AbstractEntity_Int {

	public var slug:String;
	public var address:String;
	public var information:String;
	public var date:Date;
	public var institution:Institution = null;
	public var user:User;
	public var contact:Contact;
	public var editingUser:User;

	public function Round() {
		super(this);
	}
}
}
