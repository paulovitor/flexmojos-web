package tv.snews.anews.domain {

	/**
	 * Representa uma cidade.
	 *
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.City")]
	public class City extends AbstractEntity_Int {

		private var _name:String;
		private var _state:StateCountry;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function City() {
			super(this); // Just to keep the abstraction.
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}

		public function get state():StateCountry {
			return _state;
		}

		public function set state(value:StateCountry):void {
			_state = value;
		}
	}
}
