package tv.snews.anews.domain {

	import flash.errors.IllegalOperationError;

	/**
	 * Classe abstrata para log de ações do sistema.
	 * 
	 * @author Felipe Zap de Mello
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.AbstractLog")]
	public class AbstractLog extends AbstractEntity_Number {

		protected var _date:Date;
		protected var _author:User;
		protected var _action:String;
		
		public function get date():Date
		{
			return _date;
		}

		public function set date(value:Date):void
		{
			_date = value;
		}

		public function AbstractLog(self:AbstractLog) {
			super(this); //Just to keep the abstraction
			
			if (self != this) {
				//only a subclass can pass a valid reference to self
				throw new IllegalOperationError("Abstract class did not receive reference to self. AbstractLog cannot be instantiated directly.");
			}
		}

		public function get author():User
		{
			return _author;
		}

		public function set author(value:User):void
		{
			_author = value;
		}

		public function get action():String
		{
			return _action;
		}

		public function set action(value:String):void
		{
			_action = value;
		}

	}
}
