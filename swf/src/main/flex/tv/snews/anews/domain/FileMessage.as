package tv.snews.anews.domain {

	/**
	 * Mensagem utilizada para enviar um arquivo para um ou mais usuários pelo
	 * sistema de mensagens.
	 * 
	 * @author Felipe Pinheiro
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.FileMessage")]
	public class FileMessage extends AbstractMessage {

		private var _file:UserFile;
		
		public function FileMessage() {
			super(this); // Just to keep the abstraction
		}

		// --------------------------
		// SETTERS AND GETTERS
		// --------------------------
		public function set file(value:UserFile):void {
			_file = value;
		}
		
		public function get file():UserFile {
			return _file;
		}
	}
}
