package tv.snews.anews.domain {

	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe enum que define os possíveis status do Roteiro.
	 *
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
	public class ScriptPlanStatus {
		/**
		 * Marca que o roteiro ja foi criado anteriormente no sistema.
		 */ 
		public static const EXIST:String = "EXIST";
		
		/**
		 * Marca que o roteiro não existe no sistema.
		 */ 
		public static const NOT_EXIST:String = "NOT_EXIST";
		
		/**
		 * Marca que o roteiro não pode ser criado.
		 */ 
		public static const CANT_CREATE:String = "CANT_CREATE";
		
	}
}
