package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @author Samuel Guedes de Melo
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.GenericCopyLog")]
public class GenericCopyLog extends AbstractEntity_Int {

	private var _nickname:String;
	private var _company:String;
	private var _date:Date;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public function GenericCopyLog() {
		super(this);
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------
	public function get nickname():String {
		return _nickname;
	}

	public function set nickname(value:String):void {
		_nickname = value;
	}

	public function get company():String {
		return _company;
	}

	public function set company(value:String):void {
		_company = value;
	}

	public function get date():Date {
		return _date;
	}

	public function set date(value:Date):void {
		_date = value;
	}
}
}
