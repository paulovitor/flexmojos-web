package tv.snews.anews.domain {

	import flash.errors.IllegalOperationError;

	/**
	 * @author Paulo Felipe
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.AbstractEntity")]
	public class AbstractEntity_String {

		private var _id:String;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function AbstractEntity_String(self:AbstractEntity_String) {
			if (self != this) {
				//only a subclass can pass a valid reference to self
				throw new IllegalOperationError("Abstract class did not receive reference to self. AbstractIdentityStringObject cannot be instantiated directly.");
			}
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		/**
		 * Faz a comparação entre dois objetos de forma semelhante à do Java,
		 * baseando-se no "id".
		 */
		public function equals(obj:Object):Boolean {
			if (this == obj) {
				return true;
			}
			var other:AbstractEntity_String = obj as AbstractEntity_String;
			return other != null && id == other.id;
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------

		public function set id(value:String):void {
			_id = value;
		}

		public function get id():String {
			return _id;
		}

	}
}
