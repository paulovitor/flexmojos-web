package tv.snews.anews.domain {

import mx.collections.ArrayList;

import tv.snews.flexlib.utils.DateUtil;

/**
	 * @author Felipe Zap de Mello
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StoryVTSection")]
	public class StoryVTSection extends StorySection {

		private const dateUtil:DateUtil = new DateUtil();
		private var _cue:String;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StoryVTSection() {
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		override public function calculateDuration():Date {
            duration = dateUtil.timeZero();
			if (subSectionsByVideoType().length != 0) {
				return super.calculateDuration();
			}
			return duration;
		}
		
		override public function removeSubSection(subSection:StorySubSection):Boolean {
			var removed:Boolean = subSections.removeItemAt(subSections.getItemIndex(subSection));
			var mediasVt:ArrayList = subSectionsByVideoType();
			
			if (removed && mediasVt.length == 0 && subSection is StoryMosVideo || subSection is StoryWSVideo) {
				duration = dateUtil.timeZero();
				story.vt = dateUtil.timeZero();
			}
			if (removed) {
				story.updateTimes();
				renameSubSections();
			}
			return removed;
		}
		
		/**
		 * Verifica se o objeto foi alterado.
		 *  
		 * @return retorna true se os objetos forem diferentes.
		 * 
		 **/
		override public function isDifferent(other:StorySection):Boolean {
			if (super.isDifferent(other)) return true;
			if (cue != StoryVTSection(other).cue) return true;
			
			return false;
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function get cue():String {
			return _cue;
		}

		public function set cue(value:String):void {
			_cue = value;
		}

    }
}
