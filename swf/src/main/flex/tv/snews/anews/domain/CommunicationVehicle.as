package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
public class CommunicationVehicle {

	public var id:String;
	public var name:String;

	public function CommunicationVehicle(id:String, bundleKey:String) {
		this.id = id;
		this.name = ResourceManager.getInstance().getString('Bundle', bundleKey);
	}

	public function equals(obj:Object):Boolean {
		return obj is CommunicationVehicle && CommunicationVehicle(obj).id == id;
	}

	//------------------------------
	//	Static
	//------------------------------

	public static const AGENCY:String = "AGENCY";
	public static const JOURNAL:String = "JOURNAL";
	public static const RADIO:String = "RADIO";
	public static const TV:String = "TV";

	private static var _values:ArrayCollection;

	public static function values():ArrayCollection {
		return (_values || (_values = new ArrayCollection(
				[
					new CommunicationVehicle(AGENCY, 'vehicle.agency'),
					new CommunicationVehicle(JOURNAL, 'vehicle.journal'),
					new CommunicationVehicle(RADIO, 'vehicle.radio'),
					new CommunicationVehicle(TV, 'vehicle.tv')
				]
		)));
	}

	public static function findById(id:String):CommunicationVehicle {
		for each (var obj:CommunicationVehicle in values()) {
			if (id == obj.id) {
				return obj;
			}
		}
		throw new Error('[CommunicationVehicle] Could not find a match for ' + id);
	}

	public static function equals(objA:Object, objB:Object):Boolean {
		return objA is CommunicationVehicle && CommunicationVehicle(objA).equals(objB);
	}
}

}
