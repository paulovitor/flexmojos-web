package tv.snews.anews.domain {

/**
 * @author Samuel Guedes de Melo
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.GuidelineCopyLog")]
public class GuidelineCopyLog extends GenericCopyLog {

	private var _guideline:Guideline;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public function GuidelineCopyLog() {
		super();
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public function get guideline():Guideline {
		return _guideline;
	}
	
	public function set guideline(value:Guideline):void	{
		_guideline = value;
	}
}
}
