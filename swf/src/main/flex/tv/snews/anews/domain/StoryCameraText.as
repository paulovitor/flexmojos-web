package tv.snews.anews.domain {

	/**
	 * @author Felipe Zap de Mello
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StoryCameraText")]
	public class StoryCameraText extends StoryText {
		
		private var _camera:int;
		
		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StoryCameraText(presenter:User=null) {
			super(presenter);
		}
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		/**
		 * Verifica se o objeto foi alterado.
		 *  
		 * @return retorna true se os objetos forem diferentes.
		 * 
		 **/
		override public function isDifferent(other:StorySubSection):Boolean {
			if (super.isDifferent(other)) return true;
			if (camera != StoryCameraText(other).camera) {
				return true;
			}
			
			return false;
		}
		
		public function get camera():int {
			return _camera;
		}

		public function set camera(value:int):void {
			_camera = value;
		}
	}
}
