package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;

import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.ScriptPlan")]
public class ScriptPlan extends AbstractEntity_Number {

	private static const dateUtil:DateUtil = new DateUtil();

	//------------------------------
	//	Persistent Properties
	//------------------------------

	public var slug:String;
	public var channel:String;
	public var mosStatus:String;

	public var date:Date;
	public var start:Date;           // horário de inicio do programa
	public var end:Date;             // horário de término do programa
	public var productionLimit:Date; // tempo máximo de produção
    private var _displayed:Boolean;

	public var program:Program;

	private var _blocks:ArrayCollection = new ArrayCollection();

	//------------------------------
	//	Transient Properties
	//------------------------------

	[Transient] public var limit:Date;          // diferença entre o 'end' e o 'start'
	[Transient] public var commercial:Date;     // somatório dos tempos dos breaks comerciais
	[Transient] public var produced:Date;       // somatório dos tempos dos scripts
	[Transient] public var total:Date;          // somatório do 'commercial' com o 'produced'
	[Transient] public var diff:Date;           // diferença entre o 'limit' e o 'total'
	[Transient] public var productionDiff:Date; // diferença entre o 'productionLimit' e o 'produced'

	[Transient] public var diffPositive:Boolean;
	[Transient] public var diffProductionPositive:Boolean;

	[Transient] private var _locked:Boolean = false;
	[Transient] public var maxGlobalIndex:int;
	
	//------------------------------
	//	Constructor
	//------------------------------

	public function ScriptPlan() {
		super(this);

		this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onChange);
	}
	
	//------------------------------
	//	Public Operations
	//------------------------------
	
	public function addBlock(block:ScriptBlock):void {
		if (block.order < 0 || block.order > blocks.length) {
			throw new Error("ArrayOutBoundException: The block has a order out of bound of the script plan.");
		}

		block.scriptPlan = this;
		blocks.addItemAt(block, block.order);

		block.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onBlockChange);

		reorder();
		calculateTimes();
	}

    public function updateBlock(block:ScriptBlock):void {
        for each (var scriptBlock:ScriptBlock in blocks) {
            if (scriptBlock.equals(block)) {
                scriptBlock.updateFields(block);
                break;
            }
        }
        refreshGlobalIndexes();
    }

	public function removeBlock(block:ScriptBlock):void {
		var localInstance:ScriptBlock;

		for each (localInstance in blocks) {
			if (localInstance.equals(block)) {
				break;
			}
			localInstance = null;
		}

		if (localInstance) {
			localInstance.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onBlockChange);
			blocks.removeItemAt(blocks.getItemIndex(localInstance));

			reorder();
			calculateTimes();
		}
	}

	public function addScript(script:Script):void {
		for each (var block:ScriptBlock in blocks) {
			if (block.id == script.block.id) {
				block.addScript(script);
		    }
	    }
	}
	
	public function removeScript(script:Script):void {
		for each (var block:ScriptBlock in blocks) {
			// script vem sem bloco, então não verifica se é o bloco correto
			block.removeScript(script);
		}
	}

    public function removeScripts(ids:Array):void {
        for each (var block:ScriptBlock in blocks) {
            for (var i:int = 0; i < ids.length; i++) {
                var script:Script = block.scriptById(ids[i]);
                if (script) {
                    block.scripts.removeItemAt(block.scripts.getItemIndex(script));
                }
            }
            block.reorder(); //Organize indexes.
            block.calculateTimes(); //Organize timers do block.
        }
        calculateTimes();//Organize timers do scriptPlan.
        reorder(); //Organize global indexes
    }

	public function getScriptByGlobalIndex(index:int):Script {
		if (index > maxGlobalIndex) {
			index = maxGlobalIndex;
		}

		for each (var block:ScriptBlock in blocks) {
			if (index <= block.maxGlobalIndex) {
				for each (var script:Script in block.scripts) {
					if (script.globalIndex == index) {
						return script;
					}
				}
			}
		}

		throw new Error("Index out of bounds.");
	}

	public function reorder():void {
		var order:int = 0;
		for each (var block:ScriptBlock in blocks) {
			block.order = order++;
			block.reorder();
		}

		refreshGlobalIndexes();
	}

	public function refreshGlobalIndexes():void {
		var count:int = 0;
		for each (var block:ScriptBlock in blocks) {
			for each (var script:Script in block.scripts) {
				script.globalIndex = ++count;
			}
			block.maxGlobalIndex = count;
		}
		maxGlobalIndex = count;
	}

	//------------------------------
	//	Private Operations
	//------------------------------

	public function calculateTimes():void {
		if (start && end && productionLimit) {
			commercial = produced = dateUtil.timeZero();

			for each (var block:ScriptBlock in blocks) {
				commercial = dateUtil.sumTimes(block.commercial, commercial);
				produced = dateUtil.sumTimes(block.produced, produced);
			}

			total = dateUtil.sumTimes(commercial, produced);
			limit = dateUtil.subtractTimes(end, start);

			if (limit.time >= total.time) {
				diff = dateUtil.subtractTimes(limit, total);
				diffPositive = true;
			} else {
				diff = dateUtil.subtractTimes(total, limit);
				diffPositive = false;
			}

			if (productionLimit.time >= produced.time) {
				productionDiff = dateUtil.subtractTimes(productionLimit, produced);
				diffProductionPositive = true;
			} else {
				productionDiff = dateUtil.subtractTimes(produced, productionLimit);
				diffProductionPositive = false;
			}
		}
	}

	private function onChange(event:PropertyChangeEvent):void {
		switch (event.property) {
			case "start":
			case "end":
			case "productionLimit":
				if (changed(event.oldValue, event.newValue)) {
					calculateTimes();
				}
				break;
		}
	}

	function onBlockChange(event:PropertyChangeEvent):void {
		if (event.property == "total" && changed(event.oldValue, event.newValue)) {
			calculateTimes();
		}
	}

	private function changed(oldValue:Object, newValue:Object):Boolean {
		var oldDate:Date = oldValue as Date;
		var newDate:Date = newValue as Date;

		if (oldDate && newDate) {
			return oldDate.time != newDate.time;
		}

		return (oldDate && !newDate) || (!oldDate && newDate);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	[ArrayElementType("tv.snews.anews.domain.ScriptBlock")]
	public function get blocks():ArrayCollection {
		return _blocks;
	}

	public function set blocks(value:ArrayCollection):void {
		if (_blocks) {
			for each (var oldBlock:ScriptBlock in _blocks) {
				oldBlock.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onBlockChange);
			}
		}

		value = value || new ArrayCollection();

		for each (var newBlock:ScriptBlock in value) {
			newBlock.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onBlockChange);
		}

		_blocks.removeAll();
		_blocks.addAll(value);

		calculateTimes();
		refreshGlobalIndexes();
	}

	public function get locked():Boolean {
		return _locked;
	}

	public function set locked(value:Boolean):void {
		_locked = value;

		for each (var block:ScriptBlock in blocks) {
			block.locked = value;
		}
	}

    public function get displayed():Boolean {
        return _displayed;
    }

    public function set displayed(value:Boolean):void {
        _displayed = value;
    }
}
}
