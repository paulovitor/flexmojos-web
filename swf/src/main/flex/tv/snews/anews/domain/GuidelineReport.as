package tv.snews.anews.domain {
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.GuidelineReport")]
	public class GuidelineReport extends Report {
		
		private var _guideline:Guideline;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function GuidelineReport(guideline:Guideline = null) {
			super();
			this.guideline = guideline;
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		override public function copy(other:Report):void {
			super.copy(other);
			
			if (other is GuidelineReport) {
				guideline = GuidelineReport(other).guideline;
			}
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get guideline():Guideline {
			return _guideline;
		}

		public function set guideline(value:Guideline):void {
			_guideline = value;
		}
	}
}