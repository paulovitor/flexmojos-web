package tv.snews.anews.domain{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;
	
	/**
	 * Representa as ações exclusivas da lauda.
	 * 
	 * @author Felipe Zap de Mello
	 * @since 1.2.6.1
	 */
	public class StoryAction {
		
		public static const COPIED:String = "COPIED"; 
		public static const INSERTED:String = "INSERTED"; 
		public static const EXCLUDED:String = "EXCLUDED"; 
		public static const BLOCK_CHANGED:String = "BLOCK_CHANGED";
		public static const ORDER_CHANGED:String = "ORDER_CHANGED";
		public static const APPROVED:String = "APPROVED";
		public static const DISAPPROVED:String = "DISAPPROVED";
		public static const VT_CHANGED:String = "VT_CHANGED";
		public static const EDITOR_CHANGED:String = "EDITOR_CHANGED";
		public static const ARCHIVED:String = "ARCHIVED";
		public static const PAGE_CHANGED:String = "PAGE_CHANGED";
		public static const SLUG_CHANGED:String = "SLUG_CHANGED";
		public static const OK:String = "OK";
		public static const NOT_OK:String = "NOT_OK";
		public static const EDITING_USER_CHANGED:String = "EDITING_USER_CHANGED";

		public function StoryAction() {}
		
		public static function getAllActions():ArrayCollection {
			
			var actions:ArrayCollection = new ArrayCollection();
			for each (var key:String in [COPIED, INSERTED, EXCLUDED, BLOCK_CHANGED, ORDER_CHANGED, APPROVED, DISAPPROVED,
										VT_CHANGED, EDITOR_CHANGED, ARCHIVED, PAGE_CHANGED, SLUG_CHANGED, OK, NOT_OK, EDITING_USER_CHANGED]) {
				var obj:Object = new Object();
				obj.id = key;
				obj.label = i18nOf(key);
				actions.addItem(obj);
			}
			return actions;
		}
		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String):String {
			var LABELS:Object = new Object();
			if (StringUtils.isNullOrEmpty(LABELS[type])) {
				switch (type) {
					case StoryAction.COPIED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbCopied");  break;
					case StoryAction.APPROVED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbApproved");  break;
					case StoryAction.BLOCK_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbBlockChanged"); break;
					case StoryAction.DISAPPROVED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbDisapproved");  break;
					case StoryAction.EDITOR_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbEditorChanged");  break;
					case StoryAction.EXCLUDED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbExcluded");  break;
					case StoryAction.INSERTED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbInserted");  break;
					case StoryAction.NOT_OK.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbNotOK");  break;
					case StoryAction.OK.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbOK");  break;
					case StoryAction.ORDER_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbOrderChanged");  break;
					case StoryAction.ARCHIVED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbOrderChanged");  break;
					case StoryAction.PAGE_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbPageChanged");  break;
					case StoryAction.SLUG_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbSlugChanged");  break;
					case StoryAction.VT_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbVtChanged");  break;
					case StoryAction.EDITING_USER_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "story.log.lbEditionChanged");  break;
					default:
						break;
				}
			}
			return LABELS[type];
		}
	}
}