package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

import tv.snews.anews.mos.NcsItemCredit;
import tv.snews.flexlib.utils.StringUtils;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.StoryMosCredit")]
public class StoryMosCredit extends StoryMosObject {

    public var slug:String;
    public var channel:String;
	public var disabled:Boolean = false;
	
	public var edited:Boolean = false;

    [ArrayElementType("tv.snews.anews.domain.StoryMosCreditField")]
    public var fields:ArrayCollection = new ArrayCollection();

    override public function isDifferent(subSection:StorySubSection):Boolean {
        if (super.isDifferent(subSection)) return true;

        const other:StoryMosCredit = StoryMosCredit(subSection);

        if (slug != other.slug) return true;
        if (channel != other.channel) return true;
        if (!mosMedia.equals(other.mosMedia)) return true;

        return false;
    }

    public function updateFields(other:StoryMosCredit):void {
        this.slug = other.slug;
        this.channel = other.channel;
        this.mosMedia = other.mosMedia;
        this.mosStatus = other.mosStatus;
    }

    /**
     * Limpa o ID desse crédito e de seus fields para que sejam cadastrados novamente.
     */
    public function resetIDs():void {
        this.id = undefined;
        this.mosMedia.id = undefined;
    }

    public function parseStoryMosCredit(ncsItemCredit:NcsItemCredit):void {
        this.id = ncsItemCredit.id;
        this.slug = ncsItemCredit.slug.substring(0, 127);
        this.channel = ncsItemCredit.channel.substring(0, 49);
        this.mosMedia = ncsItemCredit.mosMedia;
    }

}
}
