package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.anews.utils.Util;
import tv.snews.flexlib.utils.DateUtil;
import tv.snews.flexlib.utils.StringUtils;

/**
 * Representa uma reportagem
 *
 * @author Eliezer Reis
 * @author Felipe Lucas
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Reportage")]
public class Reportage extends Document {

	private static const dateUtil:DateUtil = new DateUtil();

	public var ok:Boolean;
	public var cameraman:String;
	public var information:String;
	public var tipOfHead:String;
	public var guideline:Guideline;

	private var _reporter:User;
	private var _vehicles:ArrayCollection = new ArrayCollection();

    [ArrayElementType("tv.snews.anews.domain.ReportageSection")]
	private var _sections:ArrayCollection;

	[ArrayElementType("tv.snews.anews.domain.ReportageCG")]
	public var cgs:ArrayCollection = new ArrayCollection();

	[ArrayElementType("tv.snews.anews.domain.ReportageCopyLog")]
	public var copiesHistory:ArrayCollection;

	[Transient] private var _readTime:Date;
	[Transient] public var readTimeStr:String;
	[Transient] public var blank:Boolean;
	[Transient] public var editingUser:User;
	[Transient] public var totalDiffs:int;
	[Transient] public var vehiclesAsStr:String = "";

	//-----------------------------
	//	Constructor
	//-----------------------------

	public function Reportage(value:Guideline = null) {
		super();

		this.id = 0;
		this.sections = new ArrayCollection();
		this.excluded = false;

		if (value) {
			guideline = value;
			slug = value.slug;
			program = value.program;
			reporter = value.reporters.length == 0 ? null : User(value.reporters.getItemAt(0));
			vehicles = value.vehicles;
		}

		_vehicles.addEventListener(CollectionEvent.COLLECTION_CHANGE, onVehiclesChange);
	}

	//-----------------------------
	//	Helpers
	//-----------------------------

	public function isDifferent(other:Reportage):Boolean {
		return hasDiff(other) ||
                (cameraman != other.cameraman || ok != other.ok || slug != other.slug) ||
                ((reporter && !reporter.equals(other.reporter)) || (reporter == null && other.reporter != null)) ||
                (information != other.information || tipOfHead != other.tipOfHead) ||
                hasDifferencesInVehicles(other.vehicles) ||
                hasDifferencesInCgs(other.cgs) ||
                hasDifferencesInSections(other.sections);
	}

    private function hasDifferencesInVehicles(otherVehicles:ArrayCollection):Boolean {
        if (Util.hasDifferencesInCollections(vehicles, otherVehicles)) {
            return true;
        }

        for each (var vehicle:String in vehicles) {
            var foundVehicle:Boolean = false;
            for each (var auxVehicle:String in otherVehicles) {
                if (vehicle == auxVehicle) {
                    foundVehicle = true;
                }
            }
            if (!foundVehicle) {
                return true;
            }
        }
        return false;
    }

    private function hasDifferencesInCgs(otherCgs:ArrayCollection):Boolean {
        if (Util.hasDifferencesInCollections(cgs, otherCgs)) {
            return true;
        }

        for (var i:int = 0; i < cgs.length; i++) {
            var thisField:ReportageCG = ReportageCG(cgs.getItemAt(i));
            var otherField:ReportageCG = ReportageCG(otherCgs.getItemAt(i));

            if (thisField.isDifferent(otherField)) {
                return true;
            }
        }
        return false;
    }

    private function hasDifferencesInSections(otherSections:ArrayCollection):Boolean {
        if (Util.hasDifferencesInCollections(sections, otherSections)) {
            return true;
        }

        for (var i:int = 0; i < sections.length; i++) {
            var thisSection:ReportageSection = ReportageSection(sections.getItemAt(i));
            var otherSection:ReportageSection = ReportageSection(otherSections.getItemAt(i));

            if (thisSection.isDifferent(otherSection)) {
                return true;
            }
        }
        return false;
    }

	public function calculateReadTime():Date {
		if (!reporter) {
			return dateUtil.timeZero();
		}

		var aux:Date = dateUtil.timeZero();
		for each (var section:ReportageSection in sections) {
			aux = dateUtil.sumTimes(aux, section.calculateReadTime());
		}
		readTime = aux;
		return readTime;
	}

	private function onSectionsChange(event:CollectionEvent):void {
		if (event.kind == CollectionEventKind.ADD || event.kind == CollectionEventKind.REMOVE) {
			renameSections();
			calculateReadTime();
		}
	}

	private function renameSections():void {
		if (sections == null) {
			return;
		}

		var countOff:int = 0;
		var countInterview:int = 0;
		var countAppearance:int = 0;
		var countArt:int = 0;
		var countSoundUp:int = 0;

		for (var i:int = 0; i < sections.length; i++) {
			var section:ReportageSection = sections.getItemAt(i) as ReportageSection;
			section.position = i;

			if (section == null)
				continue;

			//If on first position and is Appearance change to Opening;
			if (i == 0 && (section.type == SectionType.APPEARANCE || section.type == SectionType.CLOSURE)) {
				section.type = SectionType.OPENING;
			}

			//If on last position and is Appearance change to Closure
			if (i == sections.length - 1 && (section.type == SectionType.APPEARANCE || section.type == SectionType.OPENING)) {
				section.type = SectionType.CLOSURE;
			}

			//If isn't on last but Closure or if isn't on first but Opening change to Appearance
			if ((i != sections.length - 1 && section.type == SectionType.CLOSURE) || (i != 0 && section.type == SectionType.OPENING)) {
				section.type = SectionType.APPEARANCE;
			}

			section.name = SectionType.i18nOf(section.type);

			switch (section.type) {
				case SectionType.APPEARANCE:
					section.name = Util.formatName(section.name, ++countAppearance);
					break;
				case SectionType.ART:
					section.name = Util.formatName(section.name, ++countArt);
					break;
				case SectionType.INTERVIEW:
					section.name = Util.formatName(section.name, ++countInterview);
					break;
				case SectionType.OFF:
					section.name = Util.formatName(section.name, ++countOff);
					break;
				case SectionType.SOUNDUP:
					section.name = Util.formatName(section.name, ++countSoundUp);
					break;
			}
		}
	}

	override public function updateFields(other:AbstractVersionedEntity):void {
		super.updateFields(other);

		if (other is Reportage) {
			var otherReportage:Reportage = other as Reportage;

			this.copyData(otherReportage);

			this.ok = otherReportage.ok;
			this.cameraman = otherReportage.cameraman;
			this.guideline = otherReportage.guideline;
			this.reporter = otherReportage.reporter;
			this.sections = otherReportage.sections;
			this.cgs = otherReportage.cgs;
			this.copiesHistory = otherReportage.copiesHistory;
			this.information = otherReportage.information;
			this.tipOfHead = otherReportage.tipOfHead;
			this.vehicles = otherReportage.vehicles;
		}
	}

	public function approve():void {
		if (!ok) {
			if (sections) { // lazy-proof
				for each (var section:ReportageSection in sections) {
					if (section.approvable) {
						section.ok = true;
					}
				}
			}
			ok = true;
		}
	}

	public function disapprove():void {
		if (ok) {
			ok = false;
		}
	}

	public function removeInformation():void {
		this.information = null;
	}

	public function removeTipOfHead():void {
		this.tipOfHead = null;
	}

	private function onVehiclesChange(event:CollectionEvent):void {
		var joinResult:String = joinVehicles(vehicles);
		if (joinResult != vehiclesAsStr) {
			vehiclesAsStr = joinResult;
		}
	}

	private function joinVehicles(vehicles:ArrayCollection):String {
		var names:Array = [];
		for each (var vehicle:String in vehicles) {
			names.push(ResourceManager.getInstance().getString('Bundle', 'vehicle.' + vehicle.toLowerCase()));
		}
		return concatNames(names);
	}

	private function concatNames(names:Array):String {
		const bundle:IResourceManager = ResourceManager.getInstance();
		const AND:String = bundle.getString('Bundle', 'defaults.and');
		const NONE:String = bundle.getString('Bundle', 'defaults.none');

		// Junta os nomes separando com vírgula
		var aux:String = names.join(", ");

		// Troca a última vírgula por 'e'
		var index:int = aux.lastIndexOf(",");
		if (index != -1) {
			return aux.substr(0, index) + ' ' + AND + aux.substr(index + 1, aux.length);
		} else {
			return StringUtils.trim(aux).length == 0 ? NONE : aux;
		}
	}

	public function addCG(reportageCG:ReportageCG):void {
		reportageCG.order = cgs.length;
		reportageCG.reportage = this;
		cgs.addItem(reportageCG);
	}

	public function removeCG(reportageCG:ReportageCG):void {
		cgs.removeItemAt(cgs.getItemIndex(reportageCG));
        reorderCGs();
	}

	public function reorderCGs():void {
        var index:int = 0;
        for each (var reportageCG:ReportageCG in cgs) {
            if (reportageCG.order != index) {
                reportageCG.order = index;
            }
            index++;
        }
	}

	//-----------------------------
	//	Setters & Getters
	//-----------------------------

	public function get reporter():User {
		return _reporter;
	}

	public function set reporter(value:User):void {
		_reporter = value;
		calculateReadTime();
	}

	public function get vehicles():ArrayCollection {
		return _vehicles;
	}

	public function set vehicles(value:ArrayCollection):void {
		// Nunca deve setar null em _vehicles
		_vehicles.removeAll();
		if (value) {
			_vehicles.addAll(value);
		}
	}

	[ArrayElementType("tv.snews.anews.domain.ReportageSection")]
	public function get sections():ArrayCollection {
		return _sections
	}

	public function set sections(value:ArrayCollection):void {
		if (_sections != null) {
			_sections.removeEventListener(CollectionEvent.COLLECTION_CHANGE, onSectionsChange);
		}

		_sections = value;

		if (_sections != null) {
			_sections.addEventListener(CollectionEvent.COLLECTION_CHANGE, onSectionsChange);
		}

		renameSections();
		calculateReadTime();
	}

	[ArrayElementType("tv.snews.anews.domain.ReportageRevision")]
	override public function get revisions():ArrayCollection {
		return super.revisions;
	}

	override public function set revisions(value:ArrayCollection):void {
		super.revisions = value;
	}

    public function get readTime():Date {
        return _readTime;
    }

    public function set readTime(value:Date):void {
        _readTime = value;
        if(_readTime) {
            readTimeStr = dateUtil.minuteSecondToString(_readTime);
        }
    }
}
}
