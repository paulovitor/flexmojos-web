package tv.snews.anews.domain {

	import flash.errors.IllegalOperationError;

	/**
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.AbstractEntity")]
	public class AbstractEntity_Int {

		private var _id:int;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function AbstractEntity_Int(self:AbstractEntity_Int) {
			if (self != this) {
				//only a subclass can pass a valid reference to self
				throw new IllegalOperationError("Abstract class did not receive reference to self. AbstractIdentityIntObject cannot be instantiated directly.");
			}
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		/**
		 * Faz a comparação entre dois objetos de forma semelhante à do Java,
		 * baseando-se no "id".
		 */
		public function equals(obj:Object):Boolean {
			if (this == obj) {
				return true;
			}
			var other:AbstractEntity_Int = obj as AbstractEntity_Int;
			return other != null && id == other.id;
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------

		public function set id(value:int):void {
			_id = value;
		}

		public function get id():int {
			return _id;
		}
	}
}
