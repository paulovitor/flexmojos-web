package tv.snews.anews.domain {

/**
 * @author Samuel Guedes de Melo
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.StoryCopyLog")]
public class StoryCopyLog extends GenericCopyLog {

	private var _story:Story;

	//----------------------------------
	//  Constructor
	//----------------------------------

	public function StoryCopyLog() {
		super();
	}

	//----------------------------------
	//  Getters & Setters
	//----------------------------------

	public function get story():Story {
		return _story;
	}

	public function set story(value:Story):void	{
		_story = value;
	}
}
}
