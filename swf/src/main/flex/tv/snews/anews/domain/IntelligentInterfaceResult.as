package tv.snews.anews.domain {
	
	
	/**
	 * Classe enum que define os possíveis status do IntelligenceInterface.
	 *
	 * @author Felipe Zap de Mello
	 * @since 1.0.0
	 */
	public class IntelligentInterfaceResult {
		/**
		 * Marca que o houve possível erro com o IntelligenceInterface. 
		 */ 
		public static const ERROR:String = "ERROR";
		
		/**
		 * Marca que foram enviados créditos ao IntelligenceInterface.
		 */ 
		public static const SUCCESS:String = "SUCCESS";
		
	}
}
