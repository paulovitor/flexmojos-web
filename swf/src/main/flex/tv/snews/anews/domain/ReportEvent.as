package tv.snews.anews.domain {

	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.ReportEvent")]
	public class ReportEvent extends AbstractEntity_Int {
		
		private var _name:String = ""; // não tire essa iniciação
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function ReportEvent() {
			super(this);
		}

		//------------------------------
		//	Operations
		//------------------------------
		
		public function copy(event:ReportEvent):void {
			this.name = event.name;
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
	}
}