package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class DeviceType {

	public static const CG:String = "CG";
	public static const MAM:String = "MAM";
	public static const PLAYOUT:String = "PLAYOUT";
	public static const TP:String = "TP";

	public static const values:ArrayCollection = new ArrayCollection([
			CG, MAM, PLAYOUT, TP
	]);
}
}
