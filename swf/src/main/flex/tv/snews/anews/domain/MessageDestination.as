package tv.snews.anews.domain {

	/**
	 * Representa um destino de uma mensagem, contendo o destinatário e o status da
 	 * mensagem.
	 * 
	 * @author Felipe Pinheiro
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.MessageDestination")]
	public class MessageDestination extends AbstractEntity_Number {

//		private var _message:AbstractMessage;
		private var _receiver:User;
		private var _readed:Boolean;
		
		public function MessageDestination() {
			super(this); // Just to keep the abstraction
		}

		// --------------------------
		// SETTERS AND GETTERS
		// --------------------------
//		public function set message(value:AbstractMessage):void {
//			_message = value;
//		}
//		
//		public function get message():AbstractMessage {
//			return _message;
//		}
		
		public function set receiver(value:User):void {
			_receiver = value;
		}
		
		public function get receiver():User {
			return _receiver;
		}
		
		public function set readed(value:Boolean):void {
			_readed = value;
		}
		
		public function get readed():Boolean {
			return _readed;
		}
	}
}
