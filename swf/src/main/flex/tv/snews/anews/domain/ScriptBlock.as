package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;
import mx.events.PropertyChangeEvent;

import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.ScriptBlock")]
public class ScriptBlock extends AbstractEntity_Number {

	private static const dateUtil:DateUtil = new DateUtil();

	//------------------------------
	//	Persistent Properties
	//------------------------------

	public var order:int;
	public var slug:String;
	public var standBy:Boolean;
	public var commercial:Date; // duração do break commercial

	private var _scriptPlan:ScriptPlan;

	private var _scripts:ArrayCollection = new ArrayCollection();

	//------------------------------
	//	Transient Properties
	//------------------------------

	[Transient] public var produced:Date; // somatório das durações dos scripts deste bloco
	[Transient] public var total:Date;    // somatório do 'produced' com o 'commercial'
	[Transient] public var expanded:Boolean = true;
	[Transient] public var maxGlobalIndex:int;

	[Transient] private var _locked:Boolean = false;

	//------------------------------
	//	Constructor
	//------------------------------

	public function ScriptBlock() {
		super(this);
		this.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onChange);
		scripts.addEventListener(CollectionEvent.COLLECTION_CHANGE, onScriptsChange);
	}

	//------------------------------
	//	Public Operations
	//------------------------------

    public function updateFields(other:ScriptBlock):void {
        this.order = other.order;
        this.slug = other.slug;
        this.commercial = other.commercial;
        this.scripts = other.scripts;
    }

	public function reorder():void {
		var order:int = 0;
		for each(var script:Script in _scripts) {
			script.order = order++;
		}
	}

	public function addScript(script:Script):void {
		if (script.order < 0 || script.order > scripts.length) {
			throw new Error("ArrayOutBoundException: The script has a order out of bound of the block.");
		}

		script.block = this;
		scripts.addItemAt(script, script.order);

		script.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onScriptChange);

		scriptPlan.refreshGlobalIndexes();
	}

	public function removeScript(script:Script):void {
		var localInstance:Script;

		for each (localInstance in scripts) {
			if (localInstance.equals(script)) {
				break;
			}
			localInstance = null;
		}

		if (localInstance) {
			localInstance.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onScriptChange);
			scripts.removeItemAt(scripts.getItemIndex(localInstance));

//			reorder();
//			calculateTimes(); // Não precisa por causa do changeHandler dos scripts
		}

		scriptPlan.refreshGlobalIndexes();
	}

	//------------------------------
	//	Private Operations
	//------------------------------

    public function calculateTimes():void {
		if (commercial) {
			var sum:Date = dateUtil.timeZero();
			for each (var script:Script in scripts) {
				sum = dateUtil.sumTimes(sum, script.vt);
			}

			produced = sum;
			total = dateUtil.sumTimes(produced, commercial);
		}
	}

    public function scriptById(id:Number):Script {
        for each (var script:Script in scripts) {
            if(script.id == id) return script;
        }
        return null;
    }

	/*
	 * Verifica mudanças feitas nos tempos deste bloco.
	 */
	private function onChange(event:PropertyChangeEvent):void {
		if (event.property == "commercial" && changed(event.oldValue, event.newValue)) {
			calculateTimes();
		}
	}

	private function onScriptsChange(event:CollectionEvent):void {
		switch (event.kind) {
			case CollectionEventKind.ADD:
			case CollectionEventKind.MOVE:
			case CollectionEventKind.REMOVE:
			case CollectionEventKind.REPLACE:
			case CollectionEventKind.RESET:
				calculateTimes();
				reorder();
				break;
		}
	}

	/*
	 * Verifica mudanças feitas nos tempos dos scripts deste bloco.
	 */
	function onScriptChange(event:PropertyChangeEvent):void {
		if (event.property == "vt" && changed(event.oldValue, event.newValue)) {
			calculateTimes();
		}
	}

	private function changed(oldValue:Object, newValue:Object):Boolean {
		var oldDate:Date = oldValue as Date;
		var newDate:Date = newValue as Date;

		if (oldDate && newDate) {
			return oldDate.time != newDate.time;
		}

		return (oldDate && !newDate) || (!oldDate && newDate);
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	public function get scriptPlan():ScriptPlan {
		return _scriptPlan;
	}

	public function set scriptPlan(value:ScriptPlan):void {
		if (_scriptPlan) {
			this.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, _scriptPlan.onBlockChange);
		}

		_scriptPlan = value;
	}

	[ArrayElementType("tv.snews.anews.domain.Script")]
	public function get scripts():ArrayCollection {
		return _scripts;
	}

	public function set scripts(value:ArrayCollection):void {
		if (_scripts) {
			for each (var oldScript:Script in _scripts) {
				oldScript.removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onScriptChange);
			}
		}

		value = value || new ArrayCollection();

		for each (var newScript:Script in value) {
			newScript.addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, onScriptChange);
		}

		_scripts.removeAll();
		_scripts.addAll(value)

//		calculateTimes(); // Com o addAll() e o changeHandler da coleção acima não precisa disso
		scriptPlan.refreshGlobalIndexes();
	}

	public function get locked():Boolean {
		return _locked;
	}

	public function set locked(value:Boolean):void {
		_locked = value;

		for each (var script:Script in scripts) {
			script.locked = value;
		}
	}
}
}
