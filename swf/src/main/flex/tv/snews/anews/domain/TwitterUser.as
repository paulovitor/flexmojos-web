package tv.snews.anews.domain {
	import mx.collections.ArrayCollection;
	
	/**
	 * Representa um usuário do twitter dentro do sistema.
	 * 
	 * @author Felipe Zap de Mello
	 * @since 1.0.0
	 */
	
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.TwitterUser")]
	public class TwitterUser extends AbstractEntity_Number {
		
		private var _screenName:String;
		private var _profileImageUrl:String;
		private var _name:String;
		
		[ArrayElementType("tv.snews.anews.domain.TweetInfo")]
		private	var _tweetsInfo:ArrayCollection;
		
		public function TwitterUser() {
			super(this); // Just to keep the abstraction	
		}
		
		
		public function get tweetsInfo():ArrayCollection {
			return _tweetsInfo;
		}

		public function set tweetsInfo(value:ArrayCollection):void {
			_tweetsInfo = value;
		}

		public function get screenName():String {
			return _screenName;
		}
		
		public function set screenName(value:String):void {
			_screenName = value;
		}

		public function get profileImageUrl():String {
			return _profileImageUrl;
		}

		public function set profileImageUrl(value:String):void {
			_profileImageUrl = value;
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
		
	}
}