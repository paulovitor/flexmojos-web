package tv.snews.anews.domain{
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;
	
	/**
	 * Representa as ações exclusivas do block.
	 * 
	 * @author Felipe Zap de Mello
	 * @since 1.2.6.1
	 */
	public class BlockAction {
		
		public static const INSERTED:String = "INSERTED"; 
		public static const EXCLUDED:String = "EXCLUDED"; 
		public static const BREAK_CHANGED:String = "BREAK_CHANGED"; 
		
		public function BlockAction() {}
		
		public static function getAllActions():ArrayCollection {
			
			var actions:ArrayCollection = new ArrayCollection();
			for each (var key:String in [INSERTED, EXCLUDED, BREAK_CHANGED]) {
					var obj:Object = new Object();
					obj.id = key;
					obj.label = i18nOf(key);
					actions.addItem(obj);
				}
			return actions;
		}
		
		
		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String):String {
			var LABELS:Object = new Object();
			if (StringUtils.isNullOrEmpty(LABELS[type])) {
				switch (type) {
					case BlockAction.INSERTED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "block.log.lbInserted");  break;
					case BlockAction.EXCLUDED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "block.log.lbExcluded");  break;
					case BlockAction.BREAK_CHANGED.valueOf().toUpperCase():
						LABELS[type] =  ResourceManager.getInstance().getString("Bundle", "block.log.lbBreakChanged"); break;
					default:
						break;
				}
			}
			return LABELS[type];
		}
	}
}