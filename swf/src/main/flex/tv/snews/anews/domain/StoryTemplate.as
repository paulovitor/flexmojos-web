package tv.snews.anews.domain {

import mx.utils.ObjectUtil;

import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.StoryTemplate")]
public class StoryTemplate extends AbstractEntity_Number implements IStory {

	private static const dateUtil:DateUtil = new DateUtil();
	
    public var page:String;
    public var slug:String;
    private var _information:String;
    public var tapes:String;
    private var _videosAdded:Boolean;

    private var _vt:Date;
    private var _vtManually:Boolean = false;

    public var order:int;

    public var block:BlockTemplate;
    public var kind:StoryKind;
    public var editor:User;

    private var _headSection:StorySection;
    private var _offSection:StorySection;
    private var _footerSection:StorySection;
    private var _ncSection:StoryNCSection;
    private var _vtSection:StoryVTSection;

	[Transient]
	public var selected:Boolean = false; // transient

    [Transient]
    public var totalHead:Date = dateUtil.timeZero();

    [Transient]
    public var totalOff:Date = dateUtil.timeZero();

    [Transient]
    public var total:Date = dateUtil.timeZero();

    [Transient]
    public var hasInformation:Boolean = false;

    //----------------------------------
    //  Constructor
    //----------------------------------

    public function StoryTemplate() {
        super(this);
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    public function isDifferent(other:StoryTemplate):Boolean {
		if (!other || information != other.information || tapes != other.tapes || slug != other.slug) {
            return true;
        }

        if ((kind && !kind.equals(other.kind)) || (kind == null && other.kind != null)) {
            return true;
        }

        if ((vtSection != null || ncSection != null) && dateUtil.timeToString(vt) != dateUtil.timeToString(other.vt)) {
            return true;
        }

        // Comparações do head section
        if ((headSection == null && other.headSection != null) || (headSection != null && other.headSection == null)) {
            return true;
        }
        if (headSection && other.headSection && headSection.isDifferent(other.headSection)) {
            return true;
        }

        // Comparações do vt section
        if ((vtSection == null && other.vtSection != null) || (vtSection != null && other.vtSection == null)) {
            return true;
        }
        if (vtSection && other.vtSection && vtSection.isDifferent(other.vtSection)) {
            return true;
        }

        // Comparações do nc section
        if ((ncSection == null && other.ncSection != null) || (ncSection != null && other.ncSection == null)) {
            return true;
        }
        if (ncSection && other.ncSection && ncSection.isDifferent(other.ncSection)) {
            return true;
        }

        // Comparações do footer section
        if ((footerSection == null && other.footerSection != null) || (footerSection != null && other.footerSection == null)) {
            return true;
        }
        if (footerSection && other.footerSection && footerSection.isDifferent(other.footerSection)) {
            return true;
        }

        // Comparações do off section
        if ((offSection == null && other.offSection != null) || (offSection != null && other.offSection == null)) {
            return true;
        }
        if (offSection && other.offSection && offSection.isDifferent(other.offSection)) {
            return true;
        }

        return false;
	}

	public function addHeadText():void {
        headSection = headSection || new StorySection();
		headSection.addSubSection(new StoryCameraText(defaultPresenter));
	}

    public function addFooterText():void {
        footerSection = footerSection || new StorySection();
        footerSection.addSubSection(new StoryCameraText(defaultPresenter));
    }

    public function addOffText():void {
        offSection = offSection || new StorySection();
        offSection.addSubSection(new StoryText(defaultPresenter));
    }

	public function addNCSection():void {
		vtSection = null; // NCSection elimina a VT Section
		ncSection = ncSection || new StoryNCSection();
	}

	public function addVTSection():void {
		ncSection = null; // VTSection elimina a NCSection
		vtSection = vtSection || new StoryVTSection();
	}

    public function updateFields(other:StoryTemplate):void {
        this.id = other.id;
        this.page = other.page;
        this.slug = other.slug;
        this.information = other.information;
        this.tapes = other.tapes;
        this.videosAdded = other.videosAdded;
        this.vt = other.vt;
//        this.order = other.order;
//        this.block = other.block;
        this.kind = other.kind;
        this.editor = other.editor;
        this.headSection = other.headSection;
        this.offSection = other.offSection;
        this.footerSection = other.footerSection;
        this.ncSection = other.ncSection;
        this.vtSection = other.vtSection;
    }

    public  function updateTimes():void {
        var zero:Date = dateUtil.timeZero();

        total = zero;
        totalHead = zero;
        totalOff = zero;
        vt = vt || zero;

        if (headSection != null) {
            totalHead = headSection.calculateDuration();
        }
        if (vtSection != null) {
            var vtDuration:Date = vtSection.calculateDuration();
            if (vtSection.subSectionsByVideoType().length > 0) {
                vt = vtDuration;
            } else {
                vtSection.duration = vt;
            }
        }
        if (ncSection != null) {
            var ncDuration:Date = ncSection.calculateDuration();
            if (ncSection.subSectionsByType(StoryText).length > 0) {
                vt = ncDuration;
            } else {
                ncSection.duration = vt;
            }
        }
        if (footerSection != null) {
            totalHead = dateUtil.sumTimes(totalHead, footerSection.calculateDuration());
        }
        if (offSection != null) {
            totalOff = offSection.calculateDuration();
        }

        recalculateTotal();
    }

    private function recalculateOffTotal():void {
        totalOff = offSection ? offSection.duration : dateUtil.timeZero();
    }

    private function recalculateHeadTotal():void {
        var head:Date = headSection ? headSection.duration : dateUtil.timeZero();
        var footer:Date = footerSection ? footerSection.duration : dateUtil.timeZero();
        totalHead = dateUtil.sumTimes(head, footer);
    }

    private function recalculateTotal():void {
        total = dateUtil.sumTimes(vt, totalHead);
    }

    public function removeHeadSection():void {
        headSection = null;
        updateTimes();
    }

    public function removeNCSection():void {
        ncSection = null;
        vt = dateUtil.timeZero();
        updateTimes();
        this.videosAdded = false;
    }

    public function removeVTSection():void {
        vtSection = null;
        vt = dateUtil.timeZero();
        updateTimes();
        this.videosAdded = false;
    }

    public function removeOffSection():void {
        offSection = null;
        updateTimes();
    }

    public function removeFooterSection():void {
        footerSection = null;
        updateTimes();
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    [Transient]
    public function get program():Program {
        return block.rundown.program;
    }

    [Transient]
    public function get defaultPresenter():User {
        return block.rundown.program.defaultPresenter;
    }

    public function get vt():Date {
        return _vt;
    }

    public function set vt(value:Date):void {
        if (ObjectUtil.dateCompare(_vt, value) != 0) {
            _vt = value;
            recalculateTotal();
        }
    }

    public function get vtManually():Boolean {
        return _vtManually;
    }

    public function set vtManually(value:Boolean):void {
        _vtManually = value;
    }

    public function get headSection():StorySection {
        return _headSection;
    }

    public function set headSection(value:StorySection):void {
        if (ObjectUtil.compare(_headSection, value) != 0) {
            _headSection = value;
            recalculateHeadTotal();

            if (value) {
                value.story = this;
                value.renameSubSections();
            }
        }
    }

    public function get offSection():StorySection {
        return _offSection;
    }

    public function set offSection(value:StorySection):void {
        if (ObjectUtil.compare(_offSection, value) != 0) {
            _offSection = value;
            recalculateOffTotal();

            if (value) {
                value.story = this;
                value.renameSubSections();
            }
        }
    }

    public function get footerSection():StorySection {
        return _footerSection;
    }

    public function set footerSection(value:StorySection):void {
        if (ObjectUtil.compare(_footerSection, value) != 0) {
            _footerSection = value;
            recalculateHeadTotal();

            if (value) {
                value.story = this;
                value.renameSubSections();
            }
        }
    }

    public function get ncSection():StoryNCSection {
        return _ncSection;
    }

    public function set ncSection(value:StoryNCSection):void {
        _ncSection = value;

        if (value) {
            value.story = this;
            value.renameSubSections();
        }
    }

    public function get vtSection():StoryVTSection {
        return _vtSection;
    }

    public function set vtSection(value:StoryVTSection):void {
        _vtSection = value;

        if (value) {
            value.story = this;
            value.renameSubSections();
        }
    }

    public function get information():String {
        return _information;
    }

    public function set information(value:String):void {
        _information = value;
        hasInformation = value != null;
    }

    public function get videosAdded():Boolean {
        return _videosAdded;
    }

    public function set videosAdded(value:Boolean):void {
        _videosAdded = value;
    }

    public function get ncTextAdded():Boolean {
        return false;
    }

    public function set ncTextAdded(value:Boolean):void {
    }
}

}
