package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe enum que define os possíveis status de uma midia de um dispositivo MAM Web Service.
	 *
	 * @author Maxuel Ramos
	 * @since 1.7
	 */
    [Bindable]
    [RemoteClass(alias = "tv.snews.anews.domain.WSMediaStatus")]
	public class WSMediaStatus {

		public static const UNDEFINED:String = "UNDEFINED";
        public static const REGISTER_OK:String = "REGISTER_OK";
        public static const EDITION_PREPARATION:String = "EDITION_PREPARATION";
        public static const EDITION_OK:String = "EDITION_OK";
        public static const LOW_RESOLUTION_PREPARATION:String = "LOW_RESOLUTION_PREPARATION";
        public static const LOW_RESOLUTION_OK:String = "LOW_RESOLUTION_OK";
        public static const LOW_RESOLUTION_FAILED:String = "LOW_RESOLUTION_FAILED";
        public static const DECOUPAGE_STARTED:String = "DECOUPAGE_STARTED";
        public static const DECOUPAGE_OK:String = "DECOUPAGE_OK";
        public static const NEW_VERSION_REQUESTED:String = "NEW_VERSION_REQUESTED";
        public static const NEW_VERSION_PREPARATION:String = "NEW_VERSION_PREPARATION";
        public static const NEW_VERSION_OK:String = "NEW_VERSION_OK";
        public static const APPROVED:String = "APPROVED";
        public static const CANCELED:String = "CANCELED";
        public static const REVISION_PREPARATION:String = "REVISION_PREPARATION";
        public static const ARCHIVED_VIDEOS:String = "ARCHIVED_VIDEOS";


        public static function statusDescription(value:String):String {
            switch (value){
                case UNDEFINED: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.undefined"); break;;
                case REGISTER_OK: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.registerOk"); break;;
                case EDITION_PREPARATION: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.editionPreparation"); break;
                case EDITION_OK: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.editionOk"); break;
                case LOW_RESOLUTION_PREPARATION: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.lowResolutionPreparation"); break;
                case LOW_RESOLUTION_OK: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.lowResolutionOk"); break;
                case LOW_RESOLUTION_FAILED: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.lowResolutionFailed"); break;
                case DECOUPAGE_STARTED: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.decoupageStarted"); break;
                case DECOUPAGE_OK: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.decoupageOk"); break;
                case NEW_VERSION_REQUESTED: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.newVersionRequested"); break;
                case NEW_VERSION_PREPARATION: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.newVersionPreparation"); break;
                case NEW_VERSION_OK: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.newVersionOk"); break;
                case APPROVED: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.approved"); break;
                case CANCELED: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.canceled"); break;
                case REVISION_PREPARATION: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.revisionPreparation"); break;
                case ARCHIVED_VIDEOS: return ResourceManager.getInstance().getString("Bundle", "media.wsMedia.type.archivedVideos"); break;
                default: return null;
            }
        }

        public function WSMediaStatus ():void {}
	}
}
