package tv.snews.anews.domain {
	
	import flash.errors.IllegalOperationError;
	
	import mx.collections.ArrayCollection;

	/**
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */ 
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.AbstractVersionedEntity")]
	public class AbstractVersionedEntity {
		
		private var _id:Number = 0;
		private var _version:int;
		private var _author:User;
		private var _revisions:ArrayCollection = new ArrayCollection();

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function AbstractVersionedEntity(self:AbstractVersionedEntity) {
			if (self != this) {
				//only a subclass can pass a valid reference to self
				throw new IllegalOperationError("Abstract class did not receive reference to self. AbstractVersionedEntity cannot be instantiated directly.");
			}
		}
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		public function updateFields(other:AbstractVersionedEntity):void {
			this.id = other.id;
			this.version = other.version;
			this.author = other.author;
			this.revisions = other.revisions;
		}
		
		/**
		 * Faz a comparação entre dois objetos de forma semelhante à do Java, 
		 * baseando-se no "id".
		 */
		public function equals(obj:Object):Boolean {
			if (this == obj) {
				return true;
			}
			var other:AbstractVersionedEntity = obj as AbstractVersionedEntity;
			
			return other != null && id == other.id;
		}
		
		public function getRevisionByNumber(revisionNumber:int):AbstractRevision {
			for each (var revision:AbstractRevision in revisions) {
				if (revision.revisionNumber == revisionNumber) {
					return revision;
				}
			}
			return null;
		}
		
		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function set id(value:Number):void {
			_id = value;
		}
		
		public function get id():Number {
			return _id;
		}
		
		public function set version(value:int):void {
			_version = value;
		}
		
		public function get version():int {
			return _version;
		}
		
		public function get author():User {
			return _author;
		}
		
		public function set author(value:User):void {
			_author = value;
		}
		
		[ArrayElementType("tv.snews.anews.domain.AbstractRevision")]
		public function get revisions():ArrayCollection {
			if (_revisions == null) {
				_revisions = new ArrayCollection();
			}
			return _revisions;
		}
		
		public function set revisions(value:ArrayCollection):void {
			_revisions = value;
		}
	}
}
