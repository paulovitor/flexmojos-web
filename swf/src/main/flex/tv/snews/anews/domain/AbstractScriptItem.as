package tv.snews.anews.domain {

import tv.snews.flexlib.utils.ObjectUtils;

[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.AbstractScriptItem")]
public class AbstractScriptItem extends AbstractEntity_Number {

	public var order:int;
	public var disabled:Boolean = false;
	public var script:Script;

	public function AbstractScriptItem() {
		super(this);
	}

	public function isDifferent(other:AbstractScriptItem):Boolean {
		if (other == null ||
				order != other.order ||
				(!(this is ObjectUtils.getClass(other)))) {
			return true;
		}

		return false;
	}
}
}
