package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ChecklistRevision")]
public class ChecklistRevision extends AbstractRevision {

    public var date:String;
    public var schedule:String;
    public var departure:String;
    public var arrival:String;

    public var slug:String;
    public var local:String;
    public var tapes:String;
    public var info:String;
	public var vehicles:String;
	public var editors:String;

    public var producer:String;
    public var type:String;

    public var resources:String;
    public var tasks:String;

    public function ChecklistRevision() {
    }
}
}
