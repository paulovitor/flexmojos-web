package tv.snews.anews.domain {
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;
	
	/**
	 * Representa tipos de consulta do sistema.
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.0.0
	 */
	public class SearchType {
		
		
		/** Deve conter algum dos termos */
		public static const SHOULD:String = "SHOULD"; 
		
		/** Deve conter todos os termos */
		public static const MUST:String = "MUST";
		
		/** Deve conter a frase */
		public static const PHRASE:String = "PHRASE";
		
		/** Aproximação */
		public static const FUZZY:String = "FUZZY";
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		public static function values():ArrayCollection {
			var finders:ArrayCollection = new ArrayCollection();
			finders.addItem({"id": SearchType.SHOULD, "name": i18nOf(SearchType.SHOULD)});
			finders.addItem({"id": SearchType.MUST, "name": i18nOf(SearchType.MUST)});
			finders.addItem({"id": SearchType.PHRASE, "name": i18nOf(SearchType.PHRASE)});
			finders.addItem({"id": SearchType.FUZZY, "name": i18nOf(SearchType.FUZZY)});
           return finders;
       }

		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String):String {
			var LABELS:Object = new Object();
			if (StringUtils.isNullOrEmpty(LABELS[type])) {
				switch (type) {
					case SearchType.SHOULD.valueOf().toUpperCase():
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "defaults.searchShould");
						break;
					case SearchType.MUST.valueOf().toUpperCase():
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "defaults.searchMuch");
						break;
					case SearchType.PHRASE.valueOf().toUpperCase():
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "defaults.searchPrase");
						break;
					case SearchType.FUZZY.valueOf().toUpperCase():
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "defaults.searchFuzzy");
						break;
					default:
						break;
				}
			}
			return LABELS[type];
		}
		
		
	}
}