package tv.snews.anews.domain {
import tv.snews.flexlib.utils.ObjectUtils;

[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.WSMedia")]
public class WSMedia extends Media {

    public var assetId:int;
    public var classification:String;
    public var city:String;
    public var eventDate:Date;
    public var observation:String;
    public var reporter:String;
    public var cameraman1:String;
    public var cameraman2:String;
    public var cameraman3:String;
    public var assistant:String;
    public var editor:String;
	public var fileName:String;
    public var videographer:String;
    public var streamingURL:String;
    public var status:String;

    //------------------------------
    //	Constructor
    //------------------------------

    public function WSMedia() {
        super(); // Just to keep the abstraction
    }

    public function copy(obj:WSMedia):void {
        super.copyMedia(obj);
        this.assetId = obj.assetId;
        this.classification = obj.classification;
        this.city = obj.city;
        this.eventDate = obj.eventDate;
        this.observation = obj.observation;
        this.reporter = obj.reporter;
        this.cameraman1 = obj.cameraman1;
        this.cameraman2 = obj.cameraman2;
        this.cameraman3 = obj.cameraman3;
        this.assistant = obj.assistant;
        this.editor = obj.editor;
        this.videographer = obj.videographer;
			this.fileName = obj.fileName;
    }

    public function updateFields(other:WSMedia):void {
        super.updateMediaFields(other);
        this.assetId = other.assetId;
        this.classification = other.classification;
        this.city = other.city;
        this.eventDate = other.eventDate;
        this.observation = other.observation;
        this.reporter = other.reporter;
        this.cameraman1 = other.cameraman1;
        this.cameraman2 = other.cameraman2;
        this.cameraman3 = other.cameraman3;
        this.assistant = other.assistant;
        this.editor = other.editor;
        this.videographer = other.videographer;
			this.fileName = other.fileName;
    }

    override public function isDifferent(other:Media):Boolean {
        if (!(this is ObjectUtils.getClass(other)))
            return true;

        var wsMedia:WSMedia = other as WSMedia;
        return wsMedia.assetId != this.assetId;
    }

}
}
