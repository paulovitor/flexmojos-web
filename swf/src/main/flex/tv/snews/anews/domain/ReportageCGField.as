package tv.snews.anews.domain {

/**
 * @author Samuel Guedes de Melo
 * @since 1.6
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ReportageCGField")]
public class ReportageCGField extends AbstractEntity_Number {

	public var name:String;
	public var value:String;
	public var number:int;
	public var size:int;

	public var reportageCG:ReportageCG;

	public function ReportageCGField(reportageCG:ReportageCG = null, templateField:IITemplateField = null) {
		super(this);

		if (templateField) {
			this.number = templateField.number;
			this.name = templateField.name;
			this.size = templateField.size;
		}

		this.reportageCG = reportageCG;
	}
}
}
