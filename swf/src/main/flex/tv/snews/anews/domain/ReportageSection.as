package tv.snews.anews.domain {

import tv.snews.flexlib.utils.DateUtil;

/**
 * Representa partes da reportagem
 *
 * @author Eliezer Reis
 * @author Felipe Lucas
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ReportageSection")]
public class ReportageSection extends AbstractEntity_Number {

	private static const dateUtil:DateUtil = new DateUtil();

	public var content:String;
	public var observation:String;
	public var position:int;
	public var readTime:Date;
	public var ok:Boolean;
	public var reportage:Reportage;
	private var _type:String;

	//------------------------------
	//	Transient
	//------------------------------

	[Transient] public var name:String;                // armazena o nome da seção (ex.: Off 1, Off 2, Sonora 1, etc)
	[Transient] public var collapsed:Boolean;          // marca se o item está no estado collapsed
	[Transient] public var countTime:Boolean = false;  // indica se deve ser feita a contagem do tempo de leitura
	[Transient] public var approvable:Boolean = false; // indica se deve exibir informação de aprovação
	[Transient] public var hasTwoFields:Boolean = false;

	//----------------------------------------------------------------------
	//	Constructor
	//----------------------------------------------------------------------

	public function ReportageSection() {
		super(this); // Just to keep the abstraction.
	}

	//----------------------------------------------------------------------
	//	Operations & Helpers
	//----------------------------------------------------------------------

	/**
	 * Verifica se o objeto foi alterado.
	 *
	 * @return retorna true se os objetos forem diferentes.
	 */
	public function isDifferent(other:ReportageSection):Boolean {
		return position != other.position ||
                type != other.type ||
                ok != other.ok ||
                content != other.content ||
                observation != other.observation;
	}

	public function calculateReadTime():Date {
		if (countTime && reportage.reporter) {
            var textForCalculation:String = ReportageSectionType.INTERVIEW == type ? observation : content;
			readTime = reportage.reporter.calculateReadTime(textForCalculation);
		} else {
			readTime = dateUtil.timeZero();
		}
		return readTime;
	}

	//----------------------------------------------------------------------
	//	Setters & Getters
	//----------------------------------------------------------------------

	public function get type():String {
		return _type;
	}

	public function set type(value:String):void {
		_type = value;

		// Muda a flag que indica se deve contar tempo
		switch (value) {
			case SectionType.OPENING:
			case SectionType.APPEARANCE:
			case SectionType.CLOSURE:
			case SectionType.OFF:
			case SectionType.INTERVIEW:
				countTime = true;
				approvable = true;
				break;
			case SectionType.ART:
				approvable = true;
			// no break
			default:
				countTime = false;
				break;
		}

		// Muda a flag que indica se essa seção possui dois campos de texto
		hasTwoFields = value != SectionType.OFF && value != SectionType.SOUNDUP;
	}

}
}
