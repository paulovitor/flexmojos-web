package tv.snews.anews.domain {

	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Classe enum que define as partes possíveis de uma reportagem.
	 *
	 * @author Felipe Lucas
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class ReportageSectionType {

		private static const LABELS:Object = new Object();
		
		/**
		 * Representa a sessão OFF.
		 */
		public static const OFF:String = "OFF";
		
		/**
		 * Representa a sessão sonora.
		 */
		public static const INTERVIEW:String = "INTERVIEW";
		
		/**
		 * Representa a sessão passagem.
		 */
		public static const APPEARANCE:String = "APPEARANCE";
		
		/**
		 * Representa a sessão Arte.
		 */
		public static const ART:String = "ART";
		
		/**
	 	 * Representa a sessão Abertura
		 */
		public static const OPENING:String = "OPENING";
		
		/**
		 * Representa a sessão encerramento.
		 */
		public static const CLOSURE:String = "CLOSURE";
		
		/**
		 *Representa a sessão sobe o som. 
		 */
		public static const SOUNDUP:String = "SOUNDUP";
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String):String {
			if (StringUtils.isNullOrEmpty(LABELS[type])) {
				switch (type) {
					case APPEARANCE:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.appearance"); break;
					case ART:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.art"); break;
					case CLOSURE:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.closure"); break;
					case INTERVIEW:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.interview"); break;
					case OFF:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.off"); break;
					case OPENING:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.opening"); break;
					case SOUNDUP:
						LABELS[type] = ResourceManager.getInstance().getString("Bundle", "reportage.soundUp"); break;
					default:
						throw new Error("Unknow reportage section type: " + type);
				}
			}
			return LABELS[type];
		}
	}
}
