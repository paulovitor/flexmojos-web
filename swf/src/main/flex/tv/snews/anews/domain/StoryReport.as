package tv.snews.anews.domain {

	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.StoryReport")]
	public class StoryReport extends Report {
		
		private var _story:Story;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function StoryReport(story:Story = null) {
			super();
			this.story = story;
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		override public function copy(other:Report):void {
			super.copy(other);
			
			if (other is StoryReport) {
				story = StoryReport(other).story;
			}
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get story():Story {
			return _story;
		}

		public function set story(value:Story):void {
			_story = value;
		}
	}
}