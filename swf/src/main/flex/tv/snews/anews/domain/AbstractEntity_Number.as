package tv.snews.anews.domain {

	import flash.errors.IllegalOperationError;

	/**
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.AbstractEntity")]
	public class AbstractEntity_Number {

		protected var _id:Number = 0; 

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function AbstractEntity_Number(self:AbstractEntity_Number) {
			if (self != this) {
				//only a subclass can pass a valid reference to self
				throw new IllegalOperationError("Abstract class did not receive reference to self. AbstractIdentityNumberObject cannot be instantiated directly.");
			}
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		/**
		 * Faz a comparação entre dois objetos de forma semelhante à do Java,
		 * baseando-se no "id".
		 */
		public function equals(obj:Object):Boolean {
			if (this == obj) {
				return true;
			}
			var other:AbstractEntity_Number = obj as AbstractEntity_Number;
			return other != null && id == other.id;
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------

		public function set id(value:Number):void {
			_id = value;
		}

		public function get id():Number {
			return _id;
		}

	}
}
