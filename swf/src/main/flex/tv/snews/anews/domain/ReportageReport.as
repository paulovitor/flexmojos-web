package tv.snews.anews.domain {
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.ReportageReport")]
	public class ReportageReport extends Report {
		
		private var _reportage:Reportage;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function ReportageReport(reportage:Reportage = null) {
			super();
			this.reportage = reportage;
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		override public function copy(other:Report):void {
			super.copy(other);
			
			if (other is ReportageReport) {
				reportage = ReportageReport(other).reportage;
			}
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get reportage():Reportage {
			return _reportage;
		}

		public function set reportage(value:Reportage):void {
			_reportage = value;
		}
	}
}