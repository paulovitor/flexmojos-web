package tv.snews.anews.domain {

	/**
	 * Representa algumas ações possíveis de acontecer na sessão.
	 * 
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	public class SessionActions {

		public static const USER_IN:String = "USER_IN";
		public static const USER_OUT:String = "USER_OUT";

		/**
		 * Ctor
		 * @privado
		 */
		public function SessionActions() {
			throw Error("This is a Final class. You can't instantiate this class.");
		}
	}
}
