package tv.snews.anews.domain {
	
	/**
	 * Contém as informações de uma notícia obtida a partir de um feed RSS. É
	 * importante lembrar que teoricamente nenhum dos campos é obrigatório.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.RssItem")]
	public class RssItem extends AbstractEntity_Number {

		private var _title:String;
		private var _description:String;
		private var _content:String;
		private var _link:String;
		private var _published:Date;
		private var _authors:String;
		private var _categories:String;
		private var _rssFeed:RssFeed;
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function RssItem() {
			super(this); // Just to keep the abstraction
		}

		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get title():String {
			return _title;
		}

		public function set title(value:String):void {
			_title = value;
		}

		public function get description():String {
			return _description;
		}

		public function set description(value:String):void {
			_description = value;
		}

		public function get content():String {
			return _content;
		}

		public function set content(value:String):void {
			_content = value;
		}

		public function get link():String {
			return _link;
		}

		public function set link(value:String):void {
			_link = value;
		}

		public function get published():Date {
			return _published;
		}

		public function set published(value:Date):void {
			_published = value;
		}

		public function get authors():String {
			return _authors;
		}

		public function set authors(value:String):void {
			_authors = value;
		}

		public function get categories():String {
			return _categories;
		}

		public function set categories(value:String):void {
			_categories = value;
		}

		public function get rssFeed():RssFeed {
			return _rssFeed;
		}

		public function set rssFeed(value:RssFeed):void {
			_rssFeed = value;
		}
	}
}
