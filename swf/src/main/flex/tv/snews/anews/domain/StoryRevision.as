package tv.snews.anews.domain {

	/**
	 * @author Felipe Pinheiro
	 * @since 1.2.7
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.StoryRevision")]
	public class StoryRevision extends AbstractRevision {
		
		public var slug:String;
		public var storyKind:String;
		public var information:String;
		public var headSection:String;
		public var ncSection:String;
		public var vtSection:String;
		public var footerSection:String;
		public var offSection:String;
		
		public function StoryRevision() {}
		
	}
}