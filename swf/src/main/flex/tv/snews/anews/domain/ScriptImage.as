package tv.snews.anews.domain {

import flash.events.DataEvent;
import flash.events.Event;
import flash.events.ProgressEvent;
import flash.net.FileReference;
import flash.net.URLRequest;
import flash.net.URLVariables;
import flash.utils.ByteArray;
import flash.utils.setTimeout;

import tv.snews.anews.event.UploadEvent;

import tv.snews.anews.utils.Util;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ScriptImage")]
public class ScriptImage extends AbstractEntity_Number {

	// Baseado em UserFile.as

	//------------------------------
	//	Persistent Properties
	//------------------------------

	public var order:int;

	public var name:String;
	public var extension:String;
	public var path:String;
	public var size:Number;
	public var thumbnailPath:String;

	public var script:Script;

	//------------------------------
	//	Transient Properties
	//------------------------------

	[Transient] public var fileRef:FileReference;
	[Transient] public var loaded:Number = 0;
	[Transient] public var total:Number = 0;
	[Transient] public var uploading:Boolean = false;
	[Transient] public var uploaded:Boolean = false;

	public var thumbnailBytes:ByteArray;

	//------------------------------
	//	Constructor
	//------------------------------

	public function ScriptImage(fileRef:FileReference = null) {
		super(this);
		this.fileRef = fileRef;

		if (fileRef) {
			this.thumbnailBytes = fileRef.data;
			this.extension = Util.getExtension(fileRef.name);
			this.name = fileRef.name.replace(extension, "");
			this.size = fileRef.size;

			fileRef.addEventListener(Event.CANCEL, onCancel);
			fileRef.addEventListener(ProgressEvent.PROGRESS, onProgress);
			fileRef.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onUploadCompleteData);
//			fileRef.addEventListener(HTTPStatusEvent.HTTP_STATUS, onHttpStatus);
//			fileRef.addEventListener(IOErrorEvent.IO_ERROR, onIoError);
//			fileRef.addEventListener(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
		} else {
			// Veio do servidor
			uploaded = true;
		}
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function isDifferent(other:ScriptImage):Boolean {
		return this.order != other.order || this.name != other.name ||
				this.extension != other.extension || this.path != other.path ||
				this.size != other.size || this.thumbnailPath != other.thumbnailPath;
	}

	public function uploadFile():void {
		if (!uploading) {
			loaded = 0;
			total = size;

			setTimeout(function():void {
				// Notifica o início do upload
				dispatchEvent(new UploadEvent(UploadEvent.INIT));
			}, 0);

			// prepara algumas váriaveis que devem ser enviadas para o servidor.
			var vars:URLVariables = new URLVariables();
			vars.extension = extension;
			vars.fileName = name;
			vars.size = size;
			vars.source = "scriptImage";

			// faz o upload.
			var url:URLRequest = new URLRequest(Util.urlUpload());
			url.data = vars;
			fileRef.upload(url);

			uploading = true;
		}
	}

	public function cancelUpload():void {
		if (fileRef != null) {
			fileRef.cancel();
			loaded = total = 0;
			uploading = uploaded = false;
		}

		// Notifica o cancelamento do upload
		dispatchEvent(new UploadEvent(UploadEvent.CANCEL));
	}

	private function releaseFileRef():void {
		fileRef.removeEventListener(Event.CANCEL, onCancel);
		fileRef.removeEventListener(ProgressEvent.PROGRESS, onProgress);
		fileRef.removeEventListener(DataEvent.UPLOAD_COMPLETE_DATA, onUploadCompleteData);
		fileRef = null;
	}

	//------------------------------
	//	Event Handlers
	//------------------------------

	private function onCancel(event:Event):void {
		loaded = total = 0;
		uploading = uploaded = false;

		// Notifica o início do upload
		dispatchEvent(new UploadEvent(UploadEvent.CANCEL));
	}

	private function onUploadCompleteData(event:DataEvent):void {
		var data:XML = new XML(event.data);

		this.name = data.fileName.text();
		this.extension = data.extension.text();
		this.size = new Number(data.size.text());
		this.total = size;
		this.path = data.path.text();
		this.thumbnailPath = data.thumbnailPath.text();
		this.loaded = size;

		uploaded = !(uploading = false);

		releaseFileRef();

		// Notifica o término do upload
		dispatchEvent(new UploadEvent(UploadEvent.COMPLETE));
	}

	private function onProgress(event:ProgressEvent):void {
		loaded = event.bytesLoaded;
	}
}
}
