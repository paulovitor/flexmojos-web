package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;

	/**
	 * @author Felipe Pinheiro
	 * @author Paulo Felipe
	 * @author Samuel Guedes
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.Contact")]
	public class Contact extends AbstractEntity_Int {

		public var name:String;
		public var profession:String;
		
		[ArrayElementType("tv.snews.anews.domain.ContactNumber")]
		private var _numbers:ArrayCollection = new ArrayCollection();

		// FIXME Precisa dessa coleção?
		[ArrayElementType("tv.snews.anews.domain.Guide")]
		public var guides:ArrayCollection = new ArrayCollection();

		// FIXME Precisa dessa coleção?
		[ArrayElementType("tv.snews.anews.domain.News")]
		public var news:ArrayCollection = new ArrayCollection();
		
		public var firstContactNumber:ContactNumber;
		
		//------------------------------
		//	Constructor
		//------------------------------

		public function Contact(self:Contact = null) {
			super(self || this); // Just to keep the abstraction.
		}

		//------------------------------
		//	Operations
		//------------------------------

		public function updateProperties(newData:Contact):void {
			name = newData.name;
			profession = newData.profession;
			guides = newData.guides;
			news = newData.news;
			numbers = newData.numbers;
		}

		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get numbers():ArrayCollection {
			return _numbers;
		}

		public function set numbers(value:ArrayCollection):void {
			_numbers = value;
			if (numbers && numbers.length != 0) {
				firstContactNumber = numbers.getItemAt(0) as ContactNumber;
			} else {
				firstContactNumber = null;
			}
		}
	}
}
