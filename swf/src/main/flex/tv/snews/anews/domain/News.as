package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;

	/**
	 * @author Samuel Guedes
	 * @author Paulo Felipe
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.News")]
	public class News extends AbstractEntity_Number {

		private var _font:String;
		private var _slug:String;
		private var _lead:String;
		private var _information:String;
		private var _address:String;
		private var _date:Date;
		private var _user:User;

		public var editingUser:User;
		
		[ArrayElementType("tv.snews.anews.domain.Contact")]
		private var _contacts:ArrayCollection = new ArrayCollection();

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function News() {
			super(this); // Just to keep the abstraction.
		}

		//----------------------------------------------------------------------
		//	Helpers
		//----------------------------------------------------------------------
		
		public function get firstContact():Contact {
			if (contacts && contacts.length != 0) {
				return contacts.getItemAt(0) as Contact;
			} else {
				return null;
			}
		}
		
		public function containsContact(contact:Contact):Contact {
			// Verifica se é um contato novo
			if (contact.id <= 0) {
				return null;
			}
			
			for each (var current:Contact in contacts) {
				if (current.id && current.id == contact.id) {
					return current;
				}
			}
			return null;
		}
		
		public function addContact(contact:Contact):void {
			var found:Contact = containsContact(contact);
			
			if (!found) {
				contacts.addItem(contact);
			} else { //atualiza
				found.updateProperties(contact);
			}
		}
		
		public function removeContact(contact:Contact):void {
			for (var i:int = 0; i < contacts.length; i++) {
				if (contacts.getItemAt(i).id == contact.id) {
					contacts.removeItemAt(i);
					break;
				}
			}
		}
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		private const fieldsToCopy:Array = [ "id", "font", "slug", "lead", "information", "address", "date", "user", "contacts"];

		public function copy(other:News):void {
			for each (var field:String in fieldsToCopy) {
				this[field] = other[field];
			}
		}
		
		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------
		
		public function get font():String {
			return _font;
		}

		public function set font(value:String):void {
			_font = value;
		}

		public function get slug():String {
			return _slug;
		}

		public function set slug(value:String):void {
			_slug = value;
		}

		public function get lead():String {
			return _lead;
		}

		public function set lead(value:String):void {
			_lead = value;
		}

		public function get information():String {
			return _information;
		}

		public function set information(value:String):void {
			_information = value;
		}

		public function get address():String {
			return _address;
		}

		public function set address(value:String):void {
			_address = value;
		}

		public function get date():Date {
			return _date;
		}

		public function set date(value:Date):void {
			_date = value;
		}

		public function get user():User {
			return _user;
		}

		public function set user(value:User):void {
			_user = value;
		}

		public function get contacts():ArrayCollection {
			return _contacts;
		}

		public function set contacts(value:ArrayCollection):void {
			_contacts = value;
		}
	}
}
