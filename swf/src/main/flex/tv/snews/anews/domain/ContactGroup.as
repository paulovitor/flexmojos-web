package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Representa o grupo de contatos.
	 *
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.ContactGroup")]
	public class ContactGroup extends AbstractEntity_Int {

		private var _name:String;
		
		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function ContactGroup() {
			super(this); // Just to keep the abstraction.
		}

		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		public function copy(other:ContactGroup):void {
			this.id = other.id;
			this.name = other.name;
		}

		public function toString():String {
			return "ContactGroup[id=" + id + ", name=" + name + "]";
		}

		//----------------------------------------------------------------------
		//	Getters & Setters
		//----------------------------------------------------------------------

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
	}
}
