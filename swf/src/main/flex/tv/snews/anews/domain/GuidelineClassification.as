package tv.snews.anews.domain {

	/**
	 * @author Samuel Guedes de Melo.
	 * @since 1.5
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.GuidelineClassification")]
	public class GuidelineClassification extends AbstractEntity_Int {
		
		private var _name:String = ""; // não tire essa iniciação
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function GuidelineClassification() {
			super(this);
		}
		
		//------------------------------
		//	Operations
		//------------------------------
		
		public function copy(classification:GuidelineClassification):void {
			this.name = classification.name;
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
	}
}