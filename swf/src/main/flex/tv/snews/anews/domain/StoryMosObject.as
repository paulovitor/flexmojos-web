package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.StoryMosObject")]
public class StoryMosObject extends StorySubSection {

	private var _mosMedia:MosMedia;
	public var mosStatus:String;

    public function get mosMedia():MosMedia {
        return _mosMedia;
    }

    public function set mosMedia(value:MosMedia):void {
        _mosMedia = value;
    }
}
}
