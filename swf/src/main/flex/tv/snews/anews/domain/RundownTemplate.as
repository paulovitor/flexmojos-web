package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.RundownTemplate")]
public class RundownTemplate extends AbstractEntity_Number {

    public var name:String;
    public var program:Program;

    [ArrayElementType("tv.snews.anews.domain.BlockTemplate")]
    public var blocks:ArrayCollection = new ArrayCollection();

    [Transient]
    public var totalCommercial:Date; // transient

    [Transient]
    public var totalProduction:Date; // transient

    [Transient]
    public var total:Date; // transient

    [Transient]
    public var editable:Boolean = true;

    //----------------------------------
    //  Constructor
    //----------------------------------

    public function RundownTemplate(name:String=null) {
        super(this);
        this.name = name;
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    public function addBlock(block:BlockTemplate):void {
        block.rundown = this;
        blocks.addItemAt(block, block.order);

        // Corrige a ordem do Stand By, se necessário
        if (!block.standBy) {
            var standBy:BlockTemplate = BlockTemplate(blocks.getItemAt(blocks.length - 1));
            standBy.order++;
        }
    }

    public function removeBlock(block:BlockTemplate):void {
        for (var i:int = block.order + 1; i < blocks.length; i++) {
            var aux:BlockTemplate = BlockTemplate(blocks.getItemAt(i));
            aux.order--;
        }

        blocks.removeItemAt(block.order);
    }

    public function updateFields(other:RundownTemplate):void {
        this.id = id;
        this.name = name;
        this.program = program;
        this.blocks = blocks;
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    // ... (crie somente se for necessário implementar alguma lógica)
}
}
