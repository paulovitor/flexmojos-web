package tv.snews.anews.domain {

	/**
	 * @author Felipe Pinheiro
	 * @since 1.2.7
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.AbstractRevision")]
	public class AbstractRevision extends AbstractEntity_Number {
		
		public var revisionNumber:int;
		public var revisionDate:Date;
		
		public var revisor:User;
		public var entity:AbstractVersionedEntity;
		
		public function AbstractRevision() {
			super(this);
		}
		
		public static function correctMarkupTags(value:String):String {
			var builder:Array = new Array();
			
			builder.push("<TextFlow xmlns='http://ns.adobe.com/textLayout/2008'>");
			builder.push(value
				.replace(/<ins>/g, "<span backgroundColor='0xF0F2DB' color='#5FA800' fontWeight='normal'>").replace(/<\/ins>/g, "</span>")
				.replace(/<del>/g, "<span color='#FF0000' fontWeight='normal' lineThrough='true'>").replace(/<\/del>/g, "</span>")
				.replace(/<none>/g, "<span>").replace(/<\/none>/g, "</span>")
                .replace(/\n/g, "<br />")
			);
			builder.push("</TextFlow>");
			
			return builder.join("");
		}
	}

}