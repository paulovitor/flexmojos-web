package tv.snews.anews.domain {

import mx.collections.ArrayCollection;
import mx.resources.IResourceManager;
import mx.resources.ResourceManager;

import tv.snews.flexlib.utils.StringUtils;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.Device")]
public class Device extends AbstractEntity_Int {

	private static const bundle:IResourceManager = ResourceManager.getInstance();

	public var name:String;
	protected var _protocol:String;
	private var _vendor:String;

	[ArrayElementType("String")]
	private var _types:ArrayCollection = new ArrayCollection();

	[Transient]
	public var i18nProtocol:String;

	[Transient]
	public var i18nVendor:String;

	[Transient]
	public var typesAsStr:String;

	//------------------------------
	//	Constructor
	//------------------------------

	public function Device() {
		super(this);
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function update(other:Device):void {
		this.name = other.name;
		this.protocol = other.protocol;
		this.vendor = other.vendor;
		this.types = other.types;
	}

	public function noProtocol():Boolean {
		return _protocol == DeviceProtocol.NONE;
	}

	public function isII():Boolean { // sem data bind
		return _protocol == DeviceProtocol.II;
	}

	public function isMos():Boolean { // sem data bind
		return _protocol == DeviceProtocol.MOS;
	}

	public function isWebService():Boolean { // sem data bind
		return _protocol == DeviceProtocol.WS;
	}

	public function isCG():Boolean { // sem data bind
		return containsType(DeviceType.CG);
	}

	public function isMAM():Boolean { // sem data bind
		return containsType(DeviceType.MAM);
	}

	public function isPlayout():Boolean { // sem data bind
		return containsType(DeviceType.PLAYOUT);
	}

	public function isTP():Boolean { // sem data bind
		return containsType(DeviceType.TP);
	}

	//------------------------------
	//	Helpers
	//------------------------------

	public function containsType(type:String):Boolean {
		for each (var current:String in _types) {
			if (type == current) return true;
		}
		return false;
	}

	public function get protocol():String {
		return _protocol;
	}

	public function set protocol(value:String):void {
		_protocol = value;
		i18nProtocol = DeviceProtocol.i18n(value);
	}

	public function get vendor():String {
		return _vendor;
	}

	public function set vendor(value:String):void {
		_vendor = value;
		i18nVendor = DeviceVendor.i18n(value);
	}

	public function get types():mx.collections.ArrayCollection {
		return _types;
	}

	public function set types(value:ArrayCollection):void {
		_types = value;

		if (value) {
			const AND:String = bundle.getString('Bundle', 'defaults.and');
			const NONE:String = bundle.getString('Bundle', 'defaults.none');

			var aux:String = value.toArray().join(", ");

			var index:int = aux.lastIndexOf(",");
			if (index != -1) {
				typesAsStr = aux.substr(0, index) + ' ' + AND + aux.substr(index + 1, aux.length);
			} else {
				typesAsStr = StringUtils.trim(aux).length == 0 ? NONE : aux;
			}
		}
	}

	[Transient]
	public function get vendorObject():Object {
		return vendor ? DeviceVendor.entry(vendor) : null;
	}

	public function set vendorObject(value:Object):void {
		vendor = value && value.id ? value.id : null;
	}

	[Transient]
	public function get protocolObject():Object {
		return protocol ? DeviceProtocol.entry(protocol) : null;
	}

	public function set protocolObject(value:Object):void {
		protocol = value && value.id ? value.id : null;
	}
}
}
