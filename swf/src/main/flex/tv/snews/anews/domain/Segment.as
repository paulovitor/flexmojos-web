package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * Entidade que representa os segmentos da ronda.
 *
 * @author Paulo Felipe
 * @since 1.0.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Segment")]
public class Segment extends AbstractEntity_Int {

	public var name:String;

	[ArrayElementType("tv.snews.anews.domain.Institution")]
	public var institutions:ArrayCollection = new ArrayCollection();

	public function Segment() {
		super(this); // Just to keep the abstraction.
	}

	//----------------------------------------------------------------------
	//	Operations
	//----------------------------------------------------------------------

	public function addInstitution(institution:Institution):void {
		if (this.equals(institution.segment)) {
			institution.segment = this; // mantém a mesma referência
			institutions.addItem(institution);
			institutions.refresh();
		}
	}

	public function removeInstitution(institution:Institution):void {
		for (var index:int = 0; index < institutions.length; index++) {
			var current:Institution = institutions.getItemAt(index) as Institution;
			if (current.equals(institution)) {
				institutions.removeItemAt(index);
			}
		}
	}

	/**
	 * Copia todos os valores do segmento informado pelo parâmetro para este
	 * segmento.
	 *
	 * @param other Segmento do qual serão copiados os atributos.
	 */
	public function copy(other:Segment):void {
		this.id = other.id;
		this.name = other.name;
	}
}
}
