package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

import tv.snews.anews.domain.IITemplateField;

import tv.snews.anews.utils.DomainCache;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.IITemplate")]
public class IITemplate extends AbstractEntity_Int {

	public var name:String;
	public var templateCode:int;
	public var comment:Boolean;
	public var device:IIDevice;

	[ArrayElementType("tv.snews.anews.domain.IITemplateField")]
	public var fields:ArrayCollection = new ArrayCollection();

	public function IITemplate() {
		super(this);
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function addField(field:IITemplateField = null):void {
		field = field || new IITemplateField();
		field.number = fields.length + 1;
		field.template = this;

		fields.addItem(field);
	}

	public function removeField(field:IITemplateField):void {
		var index:int = fields.getItemIndex(field);
		fields.removeItemAt(index);

		for (var i:int = 0; i < fields.length; i++) {
			var curr:IITemplateField = IITemplateField(fields.getItemAt(i));
			curr.number = i + 1;
		}
	}

	public function update(other:IITemplate):void {
		this.name = other.name;
		this.templateCode = other.templateCode;
		this.comment = other.comment;
		this.fields = other.fields;
	}

	//------------------------------
	//	Getters & Setters
	//------------------------------

	[Transient]
	public function get commentObject():Object {
		return DomainCache.YES_OR_NO.getItemAt(comment ? 0 : 1);
	}

	public function set commentObject(value:Object):void {
		comment = value ? value.id : false;
	}
}
}
