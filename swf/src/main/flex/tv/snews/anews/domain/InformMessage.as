package tv.snews.anews.domain {

	/**
	 * Classe que representa as mensagens enviadas após
	 * a adição de últimas mensagens.
	 *
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	public class InformMessage extends AbstractMessage {

		public static const LAST_MESSAGES:String = "lastMessages";
		public static const JOIN_MESSAGE:String = "joinMessage";
		public static const LEFT_MESSAGE:String = "leftMessage";

		private var _type:String;
		private var _user:User;

		public function InformMessage(type:String) {
			super(this);
			_type = type;
		}

		public function get user():User {
			return _user;
		}

		public function set user(value:User):void {
			_user = value;
		}

		public function get type():String {
			return _type;
		}

		public function set type(value:String):void {
			_type = value;
		}
	}
}
