package tv.snews.anews.domain {
	import flash.events.*;
	import flash.net.*;

	/**
	 * Classe que referência um arquivo enviado por um usuário para o servidor.
	 *
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.UserFile")]
	public class UserFile extends AbstractEntity_Int {
		private var _name:String = "";
		private var _extension:String;
		private var _path:String;
		private var _size:Number;
		private var _owner:User;

		[Transient] private var _loaded:Number = 0;
		[Transient] private var _total:Number = 0;

		public function UserFile() {
			super(this);
		}

		// --------------------------
		// SETTERS AND GETTERS
		// --------------------------
		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}

		public function get extension():String {
			return _extension;
		}

		public function set extension(value:String):void {
			_extension = value;
		}

		public function get path():String {
			return _path;
		}

		public function set path(value:String):void {
			_path = value;
		}

		public function get size():Number {
			return _size;
		}

		public function set size(value:Number):void {
			_size = value;
		}

		public function get owner():User {
			return _owner;
		}

		public function set owner(value:User):void {
			_owner = value;
		}

		public function get loaded():Number {
			return _loaded;
		}

		public function set loaded(value:Number):void {
			_loaded = value;
		}

		public function get total():Number {
			return _total;
		}

		public function set total(value:Number):void {
			_total = value;
		}
	}
}
