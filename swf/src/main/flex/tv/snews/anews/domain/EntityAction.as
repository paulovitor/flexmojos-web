package tv.snews.anews.domain {

	/**
	 * Representa as ações possíveis de acontecer sobre as entidades.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class EntityAction {
		public static const INSERTED:String = "INSERTED";
		public static const DELETED:String  = "DELETED";
		public static const UPDATED:String  = "UPDATED";
		public static const LOCKED:String   = "LOCKED";
		public static const UNLOCKED:String = "UNLOCKED";
		public static const RESTORED:String = "RESTORED";
		public static const FIELD_UPDATED:String = "FIELD_UPDATED";
		public static const MOVED:String = "MOVED";
		public static const MULTIPLE_EXCLUSION:String = "MULTIPLE_EXCLUSION";
		
		public function EntityAction() {
			throw new Error("This class cannot be instantiated.");
		}
	}
}
