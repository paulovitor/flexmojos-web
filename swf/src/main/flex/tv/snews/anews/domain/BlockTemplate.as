package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.BlockTemplate")]
public class BlockTemplate extends AbstractEntity_Number {

    public var order:int;
    public var standBy:Boolean;
    public var commercial:Date;

    public var rundown:RundownTemplate;

	public var expanded:Boolean = true;

    [ArrayElementType("tv.snews.anews.domain.StoryTemplate")]
    public var stories:ArrayCollection = new ArrayCollection();

    [Transient]
    public var totalStories:Date; // transient
	
    //----------------------------------
    //  Constructor
    //----------------------------------

    public function BlockTemplate() {
        super(this);
    }

    //----------------------------------
    //  Operations
    //----------------------------------

    public function addStory(story:StoryTemplate):void {
        story.block = this;
        stories.addItem(story);
    }

    public function removeStory(story:StoryTemplate):void {
        for (var i:int = story.order + 1; i < stories.length; i++) {
            var aux:StoryTemplate = StoryTemplate(stories.getItemAt(i));
            aux.order--;
        }

        stories.removeItemAt(story.order);
    }

    public function updateFields(other:BlockTemplate):void {
        this.id = other.id;
        this.order = other.order;
        this.standBy = other.standBy;
        this.commercial = other.commercial;
        this.rundown = other.rundown;
        this.stories = other.stories;
		this.expanded = other.expanded;
    }

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    // ... (crie somente se for necessário implementar alguma lógica)
}
}
