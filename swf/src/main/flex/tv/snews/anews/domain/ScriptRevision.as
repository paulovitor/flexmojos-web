package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.domain.ScriptRevision")]
public class ScriptRevision extends AbstractRevision {

	public var slug:String;
	public var tape:String;
	public var synopsis:String;
	public var presenter:String;
	public var reporter:String;
	public var vt:String;
	public var images:String;
	public var videos:String;
}
}
