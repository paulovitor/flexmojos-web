package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Representa uma categoria de feeds RSS.
	 * 
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.RssCategory")]
	public class RssCategory extends AbstractEntity_Int {

		private var _name:String;
		private var _color:uint;
		
		[ArrayElementType("tv.snews.anews.domain.RssFeed")]
		private var _feeds:ArrayCollection = new ArrayCollection();
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function RssCategory(name:String = null) {
			super(this); // Just to keep the abstraction
			this.name = name;
		}

		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get feeds():ArrayCollection {
			return _feeds;
		}

		public function set feeds(value:ArrayCollection):void {
			_feeds = value;
		}

		public function get color():uint {
			return _color;
		}

		public function set color(value:uint):void {
			_color = value;
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}
	}
}
