package tv.snews.anews.domain {
	
	import mx.collections.ArrayCollection;

	/**
	 * Classe utilizada para persistir as informações de um 
	 * dispositivo MosObjProxyPath
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.2.6
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.domain.MosProxyPath")]
	public class MosProxyPath extends MosAbstractPath {

	}
}
