package tv.snews.anews.domain {

/**
 * Interface que define as propriedades e operações que as classes Story e
 * StoryTemplate devem possuir para atender as necessidades de StorySection,
 * StorySubSection e sub-classes.
 *
 * Isso é necessário porque os formulários das duas utilizam componentes em comum.
 *
 * @author Felipe Pinheiro
 * @since 1.6
 */
[Bindable]
public interface IStory {

    function updateTimes():void;

    function removeHeadSection():void;

    function removeNCSection():void;

    function removeVTSection():void;

    function removeOffSection():void;

    function removeFooterSection():void;

    //----------------------------------
    //  Getters & Setters
    //----------------------------------

    [Transient]
    function get program():Program;

    function get vt():Date;
    function set vt(value:Date):void;

    function get vtManually():Boolean;
    function set vtManually(value:Boolean):void;

    function get headSection():StorySection;
    function set headSection(value:StorySection):void;

    function get vtSection():StoryVTSection;
    function set vtSection(value:StoryVTSection):void;

    function get ncSection():StoryNCSection;
    function set ncSection(value:StoryNCSection):void;

    function get offSection():StorySection;
    function set offSection(value:StorySection):void;

    function get footerSection():StorySection;
    function set footerSection(value:StorySection):void;

    function get videosAdded():Boolean;
    function set videosAdded(value:Boolean):void;
}
}
