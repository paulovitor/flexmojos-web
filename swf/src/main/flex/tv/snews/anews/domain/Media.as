package tv.snews.anews.domain {

import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.Media")]
public class Media extends AbstractEntity_Int {

    private static const dateUtil:DateUtil = new DateUtil();

    public var slug:String;
    private var _durationTime:Date = dateUtil.timeZero();
    public var description:String;
    public var changed:Date;
    public var device:Device;

    //------------------------------
    //	Constructor
    //------------------------------

    public function Media() {
        super(this); // Just to keep the abstraction
    }

    public function copyMedia(other:Media):void {
        this.id = other.id; // necessário para o insert
        this.slug = other.slug;
        this.durationTime = other.durationTime;
        this.changed = other.changed;
        this.device = other.device;
        this.description = other.description;
    }

    public function updateMediaFields(other:Media):void {
        this.slug = other.slug;
        this.durationTime = other.durationTime;
        this.changed = other.changed;
        this.device = other.device;
        this.description = other.description;
    }

    public function isDifferent(other:Media):Boolean {
        if( this.id != other.id ||
             this.slug != other.slug ||
             this._durationTime != other._durationTime ||
             this.description != other.description ||
             this.changed != other.changed ||
             this.device != other.device )
                return true;
        return false;

    }

    //------------------------------
    //	Getters & Setters
    //------------------------------

    public function get durationTime():Date {
        return this._durationTime == null ? dateUtil.timeZero() : this._durationTime;
    }

    public function set durationTime(value:Date):void {
        this._durationTime = value || dateUtil.timeZero();
    }

}
}
