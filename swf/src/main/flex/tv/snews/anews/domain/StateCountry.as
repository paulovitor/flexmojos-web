package tv.snews.anews.domain {

	/**
	 * Representa um estado.
	 *
	 * @author Samuel Guedes de Melo.
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.State")]
	public class StateCountry extends AbstractEntity_Int {
		
		private var _code:String;
		private var _name:String;

		//------------------------------s----------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function StateCountry() {
			super(this); // Just to keep the abstraction.
		}
		
		public function get code():String
		{
			return _code;
		}

		public function set code(value:String):void
		{
			_code = value;
		}

		public function get name():String {
			return _name;
		}

		public function set name(value:String):void {
			_name = value;
		}

	}
}
