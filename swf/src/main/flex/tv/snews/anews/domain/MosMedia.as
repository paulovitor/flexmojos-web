package tv.snews.anews.domain {

import mx.collections.ArrayCollection;

import tv.snews.flexlib.utils.ObjectUtils;

import tv.snews.flexlib.utils.StringUtils;

/**
 * Classe utilizada para persistir as informações de um
 * dispositivo MosObj
 *
 * @author Samuel Guedes de Melo
 * @since 1.2.6
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.MosMedia")]
public class MosMedia extends Media {

    public var objectId:String;
    public var abstractText:String;
    public var group:String;
    public var type:String;
    public var revision:int;

    public var ready:Boolean;
    public var createdBy:String;
    public var created:Date;
    public var changedBy:String;

    public var metadataPath:MosMetadataPath;

    public var duration:Number = 0;
    public var timeBase:Number;

    public var mosExternalMetadata:String;

    [ArrayElementType("tv.snews.anews.domain.MosPath")]
    public var paths:ArrayCollection = new ArrayCollection();

    [ArrayElementType("tv.snews.anews.domain.MosProxyPath")]
    public var proxyPaths:ArrayCollection = new ArrayCollection();

    //------------------------------
    //	Constructor
    //------------------------------

    public function MosMedia() {
        super(); // Just to keep the abstraction
    }

    public function copy(mosObj:MosMedia):void {
        super.copyMedia(mosObj);
        this.objectId = mosObj.objectId;
        this.abstractText = mosObj.abstractText;
        this.group = mosObj.group;
        this.type = mosObj.type;
        this.revision = mosObj.revision;
        this.timeBase = mosObj.timeBase;
        this.ready = mosObj.ready;
        this.createdBy = mosObj.createdBy;
        this.created = mosObj.created;
        this.changedBy = mosObj.changedBy;
        this.paths = mosObj.paths;
        this.metadataPath = mosObj.metadataPath;
        this.proxyPaths = mosObj.proxyPaths;
    }

    public function updateFields(other:MosMedia):void {
        super.updateMediaFields(other);
        this.objectId = other.objectId;
        this.abstractText = other.abstractText;
        this.group = other.group;
        this.type = other.type;
        this.revision = other.revision;
        this.duration = other.duration;
        this.timeBase = other.timeBase;
        this.ready = other.ready;
        this.createdBy = other.createdBy;
        this.created = other.created;
        this.changedBy = other.changedBy;
        this.paths = other.paths;
        this.metadataPath = other.metadataPath;
        this.proxyPaths = other.proxyPaths;
        this.mosExternalMetadata = other.mosExternalMetadata;
    }

    public function validateProxyPaths():Boolean {
        return this.proxyPaths && this.proxyPaths.length > 0
                && !StringUtils.isNullOrEmpty((this.proxyPaths.getItemAt(0) as MosProxyPath).url);
    }

    override public function isDifferent(other:Media):Boolean {
        if (!(this is ObjectUtils.getClass(other)))
            return true;

        var mosMedia:MosMedia = other as MosMedia;
        return mosMedia.objectId != this.objectId;
    }

}
}
