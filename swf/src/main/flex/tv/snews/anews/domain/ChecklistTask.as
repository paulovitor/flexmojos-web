package tv.snews.anews.domain {

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.ChecklistTask")]
public class ChecklistTask extends AbstractEntity_Number {

	public var date:Date;
	public var time:Date;
	public var label:String;
	public var info:String;
	public var done:Boolean;
	public var index:int;

	public var checklist:Checklist;

	//----------------------------------
	//	Constructor
	//----------------------------------

	public function ChecklistTask() {
		super(this);
	}

	//----------------------------------
	//	Getters & Setters
	//----------------------------------

	// adicionar se necessário...
}
}
