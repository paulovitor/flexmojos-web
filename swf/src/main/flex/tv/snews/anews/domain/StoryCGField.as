package tv.snews.anews.domain {

/**
 * @author Felipe Zap de Mello
 * @since 1.2.6
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.domain.StoryCGField")]
public class StoryCGField extends AbstractEntity_Number {

	public var name:String;
	public var value:String;
	public var number:int;
	public var size:int;

	public var storyCG:StoryCG;

	public function StoryCGField(storyCG:StoryCG = null, templateField:IITemplateField = null) {
		super(this);

		if (templateField) {
			this.number = templateField.number;
			this.name = templateField.name;
            this.size = templateField.size;
		}
		this.storyCG = storyCG;
	}

    public function isDifferent(other:StoryCGField):Boolean {
        return this.number != other.number ||
                this.name != other.name ||
                this.value != other.value;
    }
}
}
