package tv.snews.anews.domain {

	import mx.collections.ArrayCollection;
	import mx.utils.ObjectUtil;

import spark.components.Group;

import tv.snews.anews.utils.Util;

import tv.snews.flexlib.utils.DateUtil;
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * @author Felipe Pinheiro
	 * @author Eliezer Reis
	 * @since 1.0.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.domain.User")]
	public class User extends AbstractEntity_Int {

		private static const dateUtil:DateUtil = new DateUtil();
		
		public var name:String;
		public var nickname:String;
		public var password:String;
		public var email:String;
		public var birthdate:Date;
		public var phoneNumber:String;
		public var deleted:Boolean;
		public var online:Boolean;
		public var sessionId:String; //Hold the current user session id;
		public var agreementDate:Date;
		public var passwordExpiration:Date;
		public var newPassword:String;
		public var cellNumber:String;
		public var nextelNumber:String;
		public var readTime:Number = 0;
        public var collapsedMenu:Boolean;

		[ArrayElementType("tv.snews.anews.domain.UserGroup")]
		public var groups:ArrayCollection = new ArrayCollection();

		public function User() {
			super(this); // Just to keep the abstraction.
		}
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------

		public static function equals(objA:Object, objB:Object):Boolean {
			return objA is User && User(objA).equals(objB);
		}

		/**
		 * Copia o nome e o apelido do usário pelo parametro para este usuário
		 * 
		 * @param other User do qual serão copiados os atributos.
		 */
		public function copy(other:User):void {
			this.id = other.id;
			this.name = other.name;
			this.nickname = other.nickname;
		}

		public function isSupportUser():Boolean {
			return "suporte@snews.tv" == email;
		}
		
		public function isAccountNonExpired():Boolean {
			return passwordExpiration != null && ObjectUtil.dateCompare(passwordExpiration, new Date()) > 0;
		}
		
		public function isAccountNonLocked():Boolean {
			return agreementDate != null;
		}

		/**
		 * Verifica se o usuário tem a permissão informada.
		 *
		 * @param code O codigo da permissão a ser verificada.
		 * @return o resultado da operação.
		 */
		public function allow(codes:Object, reverse:Boolean = false, matchAll:Boolean = true):Boolean {
			if (isSupportUser()) return true;

			if (codes is String) {
				return allowHelper(String(codes), reverse);
			}

			if (codes is Array) {
				var aux:Array = codes as Array;
				var allowedCount:int = 0;

				for each (var code:String in aux) {
					if (allowHelper(code, reverse)) {
						allowedCount++;
					}
				}
				if (matchAll && aux.length == allowedCount) {
					return true;
				}

				return !!(!matchAll && allowedCount != 0);
			}

			throw new Error("Invalid [codes] type. It must be a String or Array.");
		}

		private function allowHelper(code:String, reverse:Boolean = false):Boolean {
			if (ANews.PERMISSION.getItemIndex("0") != -1) return true;
			if (ANews.PERMISSION.getItemIndex(code) != -1) return true;
			for (var i:int = 2; i < code.length; i += 2) {
				if (ANews.PERMISSION.getItemIndex(code.substr(0,i)) != -1) {
					return true;
				}
			}
			if (reverse) {
				return allowMenu(code);
			}
			return false;
		}

		/**
		 * Verifica se o usuário tem a permissão informada.
		 *
		 * @param code O codigo da permissão a ser verificada.
		 * @return o resultado da operação.
		 */
		private function allowMenu(code:String):Boolean {
			if (isSupportUser()) return true;
			
			for (var j:int = 0; j < ANews.PERMISSION.length; j++) {
				var permissionStr:String = ANews.PERMISSION.getItemAt(j) as String;
				if (permissionStr.substr(0, code.length) == code) {
					return true;
				}
			}
			return false;
		}
		
		public function getFirstName():String {
			var index:int = nickname.indexOf(" ");
			return index == -1 ? nickname : nickname.substr(0, index);
		}

		public function calculateReadTime(text:String):Date {
			if (text == null || isNaN(readTime) || readTime <= 0) {
				return dateUtil.timeZero();
			} else {
				text = StringUtils.trim(text).replace(/\[[^\]]*\]\n?/g, "");
				
				var millis:Number = text.length * readTime * 1000;
				var result:Date = dateUtil.getTimeFromMilliseconds(millis);

				return result;
			}
		}

		public function groupsToString():String {
			var groupsList:Array = [];
			for each (var group:UserGroup in groups) {
				groupsList.push(group.name);
			}
			return Util.concatNames(groupsList);
		}
	}
}
