package tv.snews.anews.domain {
	import mx.collections.ArrayCollection;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	import tv.snews.flexlib.utils.StringUtils;

	/**
	 * Representa as entidades do sistema.
	 * 
	 * @author Samuel Guedes de Melo
	 * @since 1.0.0
	 */
	public class EntityType {
		
		/**
		 * Entidade que representa um tweet carregado da API do Twitter.
		 */
		public static const TWEETINFO:String = "TweetInfo";
		
		/**
		 * Entidade que representa as informações de uma notícia obtida a partir de um feed RSS.
		 */
		public static const RSSITEM:String  = "RssItem";

		/**
		 * Entidade que representa a notícia apurada.
		 */
		public static const NEWS:String  = "News";
		
		/**
		 * Entidade que representa a ronda registrada.
		 */
		public static const ROUND:String   = "Round";
		
		/**
		 * Entidade que representa o contado da agenda.
		 */
		public static const AGENDACONTACT:String = "AgendaContact";
		
		/**
		 * Entidade que representa a pauta.
		 */
		public static const GUIDELINE:String = "Guideline";
		
		/**
		 * Entidade que representa a reportagem.
		 */
		public static const REPORTAGE:String = "Reportage";
		
		/**
		 * Entidade que representa a lauda.
		 */
		public static const STORY:String = "Story";

		/**
		 * Entidade que representa o relatório.
		 */
		public static const REPORT:String = "Report";
		
		
		/**
		 * Entidade que representa o relatório.
		 */
		public static const DOCUMENT:String = "Document";
		
		/**
		 * Entidade que representa o relatório.
		 */
		public static const SCRIPT:String = "Script";
		
		//----------------------------------------------------------------------
		//	Operations
		//----------------------------------------------------------------------
		
		/**
		 * Ao passar uma das constantes desta classe para este método, será 
		 * retornada a string do bundle que representa essa constante no atual 
		 * idioma ativo.
		 * 
		 * @param state Constante a ser traduzida
		 * @return Mensagem internacionalizada da constante.
		 */
		public static function i18nOf(type:String, sending:Boolean):String {
			var LABELS:Object = new Object();
			if (StringUtils.isNullOrEmpty(LABELS[type])) {
				switch (type) {
					case EntityType.TWEETINFO.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.tweetinfo") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingTweetinfo"); break;
					case EntityType.RSSITEM.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.rssitem") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingRssitem"); break;
					case EntityType.NEWS.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.news") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingNews"); break;
					case EntityType.ROUND.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.round") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingRound"); break;
					case EntityType.AGENDACONTACT.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.agendacontact") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingAgendacontact"); break;
					case EntityType.GUIDELINE.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.guideline") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingGuideline"); break;
					case EntityType.REPORTAGE.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.reportage") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingReportage"); break;
					case EntityType.STORY.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.story") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingStory"); break;
					case EntityType.REPORT.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.report") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingReport"); break;
					case EntityType.SCRIPT.valueOf().toUpperCase():
						LABELS[type] = sending ? ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.script") : ResourceManager.getInstance().getString("Bundle", "chat.entitymessage.sendingScript"); break;
					default:
						break;
				}
			}
			return LABELS[type];
		}
		
		
		private static var _values:ArrayCollection;
		
		public static function values(entities:Array=null):ArrayCollection {
			var values:ArrayCollection =  new ArrayCollection();
			const bundle:IResourceManager = ResourceManager.getInstance();
			if (entities) {
				for (var i:int = 0; i < entities.length ;i++) {
					var obj:Object = new Object();
					var key:String =	entities[i].toString(); 
					obj.id 	=  key;
					obj.name =  bundle.getString("Bundle", "entitytype."+key.toLowerCase()) ? bundle.getString("Bundle", "entitytype."+key.toLowerCase()) : "";
					values.addItem(obj);
				}
			}
			
			return values;				
		}
	}
}