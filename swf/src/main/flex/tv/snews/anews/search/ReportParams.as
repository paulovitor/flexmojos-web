package tv.snews.anews.search {
	
import tv.snews.anews.domain.ReportEvent;
import tv.snews.anews.domain.ReportNature;
import tv.snews.anews.domain.SearchType;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.search.ReportParams")]
public class ReportParams {

    public var initialDate:Date;
    public var finalDate:Date;

    public var slug:String;
    public var text:String;
    public var ignoreText:String;

    public var origin:String;
    public var searchType:String = SearchType.values()[0];

    public var event:ReportEvent;
    public var nature:ReportNature;

    public var page:int = 1;

    //------------------------------
    //	Operations
    //------------------------------

    /**
     * Limpa os valores de busca armazenados neste objeto.
     */
    public function clear():void {
        initialDate = null;
        finalDate = null;
        slug = null;
        text = null;
        ignoreText = null;
        origin = "NONE";
        searchType = SearchType.values()[0];
        event = null;
        nature = null;
        page = 1;
    }
}
}