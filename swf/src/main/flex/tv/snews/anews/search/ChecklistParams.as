package tv.snews.anews.search {

import tv.snews.anews.domain.ChecklistType;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.User;

/**
 * @author Felipe Pinheiro
 * @since 1.5
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.search.ChecklistParams")]
public class ChecklistParams {

    public var initialDate:Date;
    public var finalDate:Date;

    public var slug:String;
    public var text:String;
    public var ignoreText:String;

    public var searchType:String = SearchType.values()[0];

    public var type:ChecklistType;
    public var producer:User;

    public var page:int = 1;

    //------------------------------
    //	Operations
    //------------------------------

    /**
     * Limpa os valores de busca armazenados neste objeto.
     */
    public function clear():void {
        initialDate = null;
        finalDate = null;
        slug = null;
        text = null;
        ignoreText = null;
        searchType = SearchType.values()[0];
        type = null;
        producer = null;
        page = 1;
    }
}
}
