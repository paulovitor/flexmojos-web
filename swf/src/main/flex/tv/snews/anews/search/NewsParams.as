package tv.snews.anews.search {

[Bindable]
[RemoteClass(alias="tv.snews.anews.search.NewsParams")]
public class NewsParams {

    public var initialDate:Date;
    private var _finalDate:Date;
    public var slug:String;
    public var page:int = 1;

    public function get finalDate():Date {
        return _finalDate;
    }

    public function set finalDate(value:Date):void {
        _finalDate = value != null ? new Date(value.fullYear, value.month, value.date, 23, 59, 59) : value;
    }
}
}
