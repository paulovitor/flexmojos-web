package tv.snews.anews.search {
	
	import tv.snews.anews.domain.SearchType;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.3.0
	 */
	[Bindable]
	[RemoteClass(alias = "tv.snews.anews.search.MosMediaParams")]
	public class MosMediaParams {
		
		public var initialDate:Date;
		public var finalDate:Date;
		
		public var text:String;
		public var ignoreText:String;
		
		public var searchType:String = SearchType.values()[0];
		
		public var page:int = 1;
		
		//------------------------------
		//	Operations
		//------------------------------
		
		/**
		 * Limpa os valores de busca armazenados neste objeto.
		 */
		public function clear():void {
			initialDate = null;
			finalDate = null;
			text = null;
			ignoreText = null;
			searchType = SearchType.values()[0];
			page = 1;
		}
	}
}