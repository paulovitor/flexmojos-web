package tv.snews.anews.search {

import tv.snews.anews.domain.SearchType;

/**
 * @author Felipe Pinheiro
 * @since 1.3.0
 */
[Bindable]
[RemoteClass(alias="tv.snews.anews.search.DocumentParams")]
public class DocumentParams {

	public var initialDate:Date;
	public var finalDate:Date;

	public var slug:String;

	public var searchType:String = SearchType.values()[0];

	public var programId:Number;
	public var programUsedId:Number;
    public var programNotUsedId:Number;
	public var origin:String;
	public var page:int = 1;
	public var drawer:Boolean;
    public var source:String;
	
	public var hideUsed:Boolean;

	public function DocumentParams(drawer:Boolean = true) {
		this.drawer = drawer;
	}

	//------------------------------
	//	Operations
	//------------------------------

	/**
	 * Limpa os valores de busca armazenados neste objeto.
	 */
	public function clear():void {
		initialDate = null;
		finalDate = null;
		slug = null;
		searchType = SearchType.values()[0];
		programId = 0;
		programUsedId = 0;
        programNotUsedId = 0;
		origin = "DOCUMENT";
		page = 1;
		hideUsed = false;
	}
}
}
