package tv.snews.anews.search {

import tv.snews.anews.domain.SearchType;

/**
 * @author Maxuel Ramos
 * @since 1.7
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.search.WSMediaParams")]
public class WSMediaParams {

    public var titleText:String;
    public var classificationText:String;
    public var observationText:String;
    public var descriptionText:String;
    public var reporterText:String;
    public var cameramanText:String;
//    public var assistanceText:String;
    public var editorText:String;
    public var videographerText:String;
    public var cityText:String;


    public var eventDateStart:Date;
    public var eventDateEnd:Date;
    public var changedDateStart:Date;
    public var changedDateEnd:Date;


    public var page:int = 1;

    //------------------------------
    //	Operations
    //------------------------------

    /**
     * Limpa os valores de busca armazenados neste objeto.
     */
    public function clear():void {
        titleText = null;
        classificationText = null;
        observationText = null;
        descriptionText = null;
        reporterText = null;
        cameramanText = null;
//        assistanceText = null;
        editorText = null;
        videographerText = null;

        eventDateStart = null;
        eventDateEnd = null;
        changedDateStart = null;
        changedDateEnd = null;
        page = 1;
    }
}
}