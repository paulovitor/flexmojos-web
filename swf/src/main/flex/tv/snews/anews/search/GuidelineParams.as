package tv.snews.anews.search {

import tv.snews.anews.domain.GuidelineClassification;
import tv.snews.anews.domain.Program;
import tv.snews.anews.domain.SearchType;
import tv.snews.anews.domain.User;
import tv.snews.anews.domain.UserTeam;

/**
 * @author Felipe Pinheiro
 * @since 1.6
 */
[Bindable]
[RemoteClass(alias = "tv.snews.anews.search.GuidelineParams")]
public class GuidelineParams {

    public var initialDate:Date;
    public var finalDate:Date;

    public var slug:String;
    public var text:String;
    public var ignoreText:String;

    public var searchType:String = SearchType.values()[0];

    public var program:Program;
    public var producer:User;
    public var reporter:User;

    public var page:int = 1;
	
	public var classification:GuidelineClassification;
    public var team:UserTeam;
	public var state:String;
	public var vehicle:String;

    //------------------------------
    //	Operations
    //------------------------------

    /**
     * Limpa os valores de busca armazenados neste objeto.
     */
    public function clear():void {
        initialDate = null;
        finalDate = null;
        slug = null;
        text = null;
        ignoreText = null;
        searchType = SearchType.values()[0];
        program = null;
        producer = null;
        reporter = null;
        page = 1;
		vehicle = null;
		state = null;
		classification = null;
    }
}
}
