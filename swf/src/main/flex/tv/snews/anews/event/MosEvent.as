package tv.snews.anews.event {
import tv.snews.anews.mos.*;

import flash.events.Event;

import tv.snews.anews.mos.MosMessage;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class MosEvent extends Event {

	public static const HANDSHAKE_COMPLETE:String = "handshakeComplete";
	public static const HANDSHAKE_FAILED:String = "handshakeFailed";
	public static const MESSAGE_FROM_PLUGIN:String = "messageFromPlugin";
	
	public var message:MosMessage;

	public function MosEvent(type:String, message:MosMessage = null) {
		super(type, true, false);
		this.message = message;
	}
}
}
