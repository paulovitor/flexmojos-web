package tv.snews.anews.event {

import flash.events.Event;

/**
 * @author Felipe Pinheiro
 * @since 1.7
 */
public class UserActivityEvent extends Event {

	public static const ACTIVATED:String = "activated";
	public static const DEACTIVATED:String = "deactivated";

	public function UserActivityEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false) {
		super(type, bubbles, cancelable);
	}
}
}
