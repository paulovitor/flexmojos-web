package tv.snews.anews.event {

	import flash.events.Event;

import mx.controls.Alert;

/**
	 * Evento para ações relacionadas a operações de CRUD. O <code>type</code>
	 * do evento determina a ação e o <code>entity</code> irá conter os dados da
	 * entidade alvo da ação.
	 *
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MenuEvent extends Event {

		public static const ITEM_CLICK:String = "item_click";
		public static const COLLAPSE:String = "collapse";

		private var _entity:Object;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function MenuEvent(type:String, entity:Object = null, bubbles:Boolean = true, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			_entity = entity;
		}

		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------

		public function get entity():Object {
			return _entity;
		}

		public function set entity(value:Object):void {
			_entity = value;
		}
	}
}
