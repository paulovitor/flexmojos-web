package tv.snews.anews.event {

import flash.events.Event;

import tv.snews.anews.domain.MosDevice;

/**
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public class PluginActiveXEvent extends Event {

	public static const OPEN:String = "OPEN_ORAD_PLUGIN";
	public static const EDIT:String = "EDIT_ORAD_PLUGIN";
	public static const CLOSE:String = "CLOSE_ORAD_PLUGIN";

	public var mosDevice:MosDevice;
    public var entity:Object;


	public function PluginActiveXEvent(type:String, mosDevice:MosDevice, entity:Object = null) {
		super(type, true, false);
		this.entity = entity;
		this.mosDevice = mosDevice;
	}
}
}
