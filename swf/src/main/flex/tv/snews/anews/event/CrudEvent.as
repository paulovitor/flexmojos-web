package tv.snews.anews.event {

	import flash.events.Event;

	/**
	 * Evento para ações relacionadas a operações de CRUD. O <code>type</code>
	 * do evento determina a ação e o <code>entity</code> irá conter os dados da
	 * entidade alvo da ação.
	 *
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class CrudEvent extends Event {

		public static const READ:String = "read";
		public static const CANCEL:String = "cancel";
		public static const CREATE:String = "create";
		public static const EDIT:String = "edit";
		public static const UPDATE:String = "update";
		public static const DELETE:String = "delete";
		public static const COPY:String = "copy_";
		public static const SELECT:String = "select";
		public static const SELECT_CHANGE:String = "select_change";

		private var _entity:Object;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function CrudEvent(type:String, entity:Object = null, bubbles:Boolean = true, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			_entity = entity;
		}

		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------

		public function get entity():Object {
			return _entity;
		}

		public function set entity(value:Object):void {
			_entity = value;
		}
	}
}
