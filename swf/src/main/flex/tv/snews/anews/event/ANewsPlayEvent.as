package tv.snews.anews.event {

import flash.events.Event;

import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.PlayerMedia;

/**
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public class ANewsPlayEvent extends Event {

	public static const OPEN:String = "ANEWS_OPEN_PLAY";
	public static const CLOSE:String = "ANEWS_CLOSE_PLAY";
    public var playerMedia:PlayerMedia;

	public function ANewsPlayEvent(type:String, playerMedia:PlayerMedia = null) {
		super(type, true, false);
		this.playerMedia = playerMedia;
	}
}
}
