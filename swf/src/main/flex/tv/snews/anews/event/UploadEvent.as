package tv.snews.anews.event {

import flash.events.Event;

/**
 * @author Felipe Pinheiro
 */
public class UploadEvent extends Event {

	public static const INIT:String = "uploadInit";
	public static const CANCEL:String = "uploadCancel";
	public static const COMPLETE:String = "uploadComplete";

	public function UploadEvent(type:String) {
		super(type, true, true);
	}
}
}
