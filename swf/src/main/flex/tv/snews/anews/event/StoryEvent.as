package tv.snews.anews.event {

import flash.events.Event;

import tv.snews.anews.domain.MosMedia;
import tv.snews.anews.domain.PlayerMedia;
import tv.snews.anews.domain.Story;

/**
 * @author Maxuel Ramos
 * @since 1.8
 */
public class StoryEvent extends Event {

	public static const DISPLAYING:String = "DISPLAYING";
    public var story:Story;

	public function StoryEvent(type:String, story:Story = null) {
		super(type, true, false);
		this.story = story;
	}
}
}
