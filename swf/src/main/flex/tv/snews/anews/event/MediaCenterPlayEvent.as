package tv.snews.anews.event {

import flash.events.Event;

import tv.snews.anews.domain.PlayerMedia;

/**
 * @author Samuel Guedes de Melo
 * @since 1.7
 */
public class MediaCenterPlayEvent extends Event {

	public static const OPEN:String = "MEDIA_CENTER_OPEN_PLAY";
	public static const CLOSE:String = "MEDIA_CENTER_CLOSE_PLAY";
    public var playerMedia:PlayerMedia;

	public function MediaCenterPlayEvent(type:String, playerMedia:PlayerMedia = null) {
		super(type, true, false);
		this.playerMedia = playerMedia;
	}
}
}
