package tv.snews.anews.flex {
	
	import mx.collections.ArrayCollection;
	
	/**
	 * Classe que agrupa todos os dados do resultado paginado de uma busca
	 * (número da página, tamanho da página, total de registros e os registros
	 * da página).
	 *
	 * @author Felipe Pinheiro
	 * @since 0.0.1
	 */
	[Bindable]
	[RemoteClass(alias="tv.snews.anews.flex.PageResult")]
	public class PageResult {
		
		private var _pageNumber:int;
		private var _pageSize:int;
		private var _total:int;
		private var _results:ArrayCollection = new ArrayCollection();
		
		//------------------------------
		//	Constructor
		//------------------------------
		
		public function PageResult() {
			
		}
		
		//------------------------------
		//	Getters & Setters
		//------------------------------
		
		public function get pageNumber():int {
			return _pageNumber;
		}
		
		public function set pageNumber(value:int):void {
			_pageNumber = value;
		}
		
		public function get pageSize():int {
			return _pageSize;
		}
		
		public function set pageSize(value:int):void {
			_pageSize = value;
		}
		
		public function get total():int {
			return _total;
		}
		
		public function set total(value:int):void {
			_total = value;
		}
		
		public function get results():ArrayCollection {
			return _results;
		}
		
		public function set results(value:ArrayCollection):void {
			_results = value;
		}
	}
}
