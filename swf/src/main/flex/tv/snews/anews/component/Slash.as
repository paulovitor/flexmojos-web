package tv.snews.anews.component {
	import spark.components.Button;
	import spark.components.SkinnableContainer;

import tv.snews.anews.skin.SlashSkin;

[Style(name="position", type="String", enumeration="left,right", inherit="no")]
	[Style(name="lineColor", type="uint", format="Color", inherit="no", theme="spark")]
	public class Slash extends SkinnableContainer {
		
		public function Slash() {
			super();
			setStyle("skinClass", SlashSkin);
		}
	}
}
