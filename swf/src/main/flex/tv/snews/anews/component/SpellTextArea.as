package tv.snews.anews.component {

import flash.events.Event;
import flash.events.MouseEvent;

import flashx.textLayout.elements.FlowElement;
import flashx.textLayout.elements.FlowGroupElement;

import flashx.textLayout.operations.PasteOperation;

import mx.events.FlexEvent;
import mx.utils.StringUtil;

import spark.components.Button;

import spark.components.RichEditableText;

import spark.components.TextArea;
import spark.events.TextOperationEvent;

import tv.snews.anews.domain.Settings;
import tv.snews.anews.utils.DomainCache;

import mx.core.mx_internal;

use namespace mx_internal;

[DefaultProperty("content")]

[DefaultTriggerEvent("change")]

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class SpellTextArea extends TextArea {

	private static const STYLES_TO_CLEAR:Array = [ 'color', 'fontWeight', 'backgroundColor', 'fontWeight', 'lineThrough'];
	private static const settings:Settings = DomainCache.settings;

	[SkinPart(required="false")]
	public var upperButton:Button;

	[SkinPart(required="false")]
	public var lowerButton:Button;

	[SkinPart(required="false")]
	public var sentenceButton:Button;

	// FIXME Corrigir bugs do corretor ortográfico
//	private var spellCheckEnabled:Boolean = false;

	//----------------------------------------------------------------------
	//	Constructor
	//----------------------------------------------------------------------

	public function SpellTextArea() {
		super();

		this.horizontalCenter = 0;
		this.verticalCenter = 0;
//		this.heightInLines = 1;

//		this.setStyle("focusThickness", 0);
//		this.setStyle("horizontalScrollPolicy", "off");
//		this.setStyle("verticalScrollPolicy", "off");

		this.addEventListener(Event.RENDER, onRenderHandler);
		this.addEventListener(Event.CHANGE, onChangeHandler);
		this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
		this.addEventListener(FlexEvent.REMOVE, onRemoveHandler);
		this.addEventListener(TextOperationEvent.CHANGING, onChangingHandler);
	}

	//----------------------------------------------------------------------
	//	Overrides
	//----------------------------------------------------------------------

	override protected function partAdded(partName:String, instance:Object):void {
		super.partAdded(partName, instance);

		if (instance == upperButton) {
			upperButton.addEventListener(MouseEvent.CLICK, upperButton_clickHandler);
		}
		if (instance == lowerButton) {
			lowerButton.addEventListener(MouseEvent.CLICK, lowerButton_clickHandler);
		}
		if (instance == sentenceButton) {
			sentenceButton.addEventListener(MouseEvent.CLICK, sentenceButton_clickHandler);
		}
	}

	override protected function partRemoved(partName:String, instance:Object):void {
		super.partRemoved(partName, instance);

		if (instance == upperButton) {
			upperButton.removeEventListener(MouseEvent.CLICK, upperButton_clickHandler);
		}
		if (instance == lowerButton) {
			lowerButton.removeEventListener(MouseEvent.CLICK, lowerButton_clickHandler);
		}
		if (instance == sentenceButton) {
			sentenceButton.removeEventListener(MouseEvent.CLICK, sentenceButton_clickHandler);
		}
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function dispose():void {
		// FIXME Corrigir bugs do corretor ortográfico
//		if (spellCheckEnabled) {
//			SpellingManager.disableSpelling(this);
//		}

		this.removeEventListener(Event.RENDER, onRenderHandler);
		this.removeEventListener(Event.CHANGE, onChangeHandler);
		this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
		this.removeEventListener(FlexEvent.REMOVE, onRemoveHandler);
		this.removeEventListener(TextOperationEvent.CHANGING, onChangingHandler);
	}

	//----------------------------------------------------------------------
	//	Event Handlers
	//----------------------------------------------------------------------

	private function onCreationCompleteHandler(event:FlexEvent):void {
		setStyle("fontFamily", settings.editorFontFamily);
		setStyle("fontSize", settings.editorFontSize);

		// Corrige a mudança da cor da seleção que ocorre quando altera o interactionManager
		setStyle("focusedTextSelectionColor", 0x98B7EE);
		setStyle("inactiveTextSelectionColor", 0xCBDBE7);
//		setStyle("unfocusedTextSelectionColor", 0xCBDBE7); // não sei qual era a cor original

		// Altera a flag interna (mx_internal) que faz com que o histórico de alterações seja zerado ao perder o focus
		RichEditableText(this.textDisplay).clearUndoOnFocusOut = false;

		// FIXME Corrigir bugs do corretor ortográfico
//		if (settings.editorSpellCheck) {
//			SpellingManager.enableSpelling(this);
//			spellCheckEnabled = true;
//		}
	}

	private function onRenderHandler(event:Event):void {
		limitHeight();
	}

	private function onChangeHandler(event:Event):void {
		limitHeight();
	}

	private function limitHeight():void {
		this.heightInLines = RichEditableText(this.textDisplay).mx_internal::textContainerManager.numLines;
	}

	private static function onChangingHandler(event:TextOperationEvent):void {
		if (event.operation is PasteOperation) {
			var pasteOperation:PasteOperation = event.operation as PasteOperation;
			if (pasteOperation.textScrap) {
				clearStyles(pasteOperation.textScrap.textFlow);
			}
		}
	}

	private static function clearStyles(flowElement:FlowElement):void {
		// Limpa os estilos definidos por "STYLES_TO_CLEAR"
		for each (var style:String in STYLES_TO_CLEAR) {
			flowElement.clearStyle(style);
		}

		// Elementos do tipo FlowGroupElement, como o <div> e o <p>, podem possuir filhos.
		// Esses filhos também devem ter seus estilos limpados.
		if (flowElement is FlowGroupElement) {
			var groupElement:FlowGroupElement = flowElement as FlowGroupElement;
			for (var i:int = 0; i < groupElement.numChildren; i++) {
				clearStyles(groupElement.getChildAt(i));
			}
		}
	}

	private function onRemoveHandler(event:FlexEvent):void {
		dispose();
	}

	private function upperButton_clickHandler(event:MouseEvent):void {
		if (text) {
			text = text.toUpperCase();
		}
	}

	private function lowerButton_clickHandler(event:MouseEvent):void {
		if (text) {
			text = text.toLowerCase();
		}
	}

	private function sentenceButton_clickHandler(event:MouseEvent):void {
		if (text) {
			var aux:String = text.toLowerCase();

			/*
				Lógica da regex:
			 		[\n\.\?\!] -> procura por um \n, ponto final, ponto de interrogação ou ponto de exclamação
			 		[^a-zA-Z\u00C0-\u017F]* -> procura por zero ou mais caracteres que não sejam uma letras, acentuadas ou não
			 		[a-zA-Z\u00C0-\u017F] -> procura por uma letra, acentuada ou não
			 */
			aux = aux.replace(/([\n\.\?\!][^a-zA-Z\u00C0-\u017F]*)([a-zA-Z\u00C0-\u017F])/g,
					function():String {
						return arguments[1] + arguments[2].toUpperCase();
					}
			);

			// A regex não pega o primeiro caractere
			if (aux.length > 1) {
				text = aux.substr(0, 1).toUpperCase() + aux.substr(1, aux.length);
			} else {
				text = aux.toUpperCase();
			}
		}
	}

}
}
