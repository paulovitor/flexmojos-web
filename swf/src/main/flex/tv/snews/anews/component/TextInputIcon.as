package tv.snews.anews.component
{
	import flash.events.FocusEvent;
	
	import spark.components.TextInput;
	
	public class TextInputIcon extends spark.components.TextInput
	{
		[Bindable]
		public var iconFont:String;
		[Bindable]
		public var showDefaultDomain:Boolean = false;

		
		public function TextInputIcon()
		{
			super();
		}
		
	}
}