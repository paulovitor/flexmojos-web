package tv.snews.anews.component {

import flash.events.FocusEvent;
import flash.events.MouseEvent;

import flashx.textLayout.edit.EditManager;
import flashx.undo.UndoManager;

import mx.events.FlexEvent;
import mx.resources.ResourceManager;
import mx.utils.StringUtil;

import spark.components.RichEditableText;
import spark.components.supportClasses.SkinnableComponent;

import tv.snews.anews.domain.Settings;
import tv.snews.anews.skin.EditableTextSkin;
import tv.snews.anews.utils.DomainCache;

/**
 * @author Felipe Pinheiro
 */
[SkinState("normal")]
[SkinState("hovered")]
[SkinState("focused")]
public class EditableText extends SkinnableComponent {

	public static const DEFAULT_PROMPT:String = ResourceManager.getInstance().getString("Bundle", "editor.defaultPromptText");

	public const undoManager:UndoManager = new UndoManager();
	public const editManager:EditManager = new EditManager(undoManager);

	private static const settings:Settings = DomainCache.settings;

	[Bindable]
	[SkinPart(required=true)]
	public var textDisplay:RichEditableText;

//	[Bindable]
//	[SkinPart(required=false)]
//	public var textBorder:Rect;

//	[Bindable]
//	[SkinPart(required=false)]
//	public var eyeCandyBorder:Rect;

	private var hovered:Boolean = false;
	private var focused:Boolean = false;

	//------------------------------
	//	Properties
	//------------------------------

	private var _text:String = "";

	[Bindable]
	public function get text():String {
		return StringUtil.trim(changeCase() ? _text.toUpperCase() : _text);
	}

	[Bindable("change")]
	[Bindable("textChanged")]
	[CollapseWhiteSpace]
	public function set text(value:String):void {
		_text = value ? (StringUtil.trim(changeCase() ? value.toUpperCase() : value)) : "";
	}

	[Bindable]
	public var promptText:String = DEFAULT_PROMPT;

	/**
	 * Se true ignora o UpperCase caso esteja ativo no settings.
	 * Define o valor do ignore UpperCase caso ele esteja ativo.
	 */
	public var ignoreUpperCase:Boolean = false;

	//------------------------------
	//	Constructor
	//------------------------------

	public function EditableText() {
		super();

		addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);

		addEventListener(FocusEvent.FOCUS_IN, onFocusIn);
		addEventListener(FocusEvent.FOCUS_OUT, onFocusOut);

		addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
		addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);

		setStyle("skinClass", EditableTextSkin);
	}

	//------------------------------
	//	Overrides
	//------------------------------

	override protected function getCurrentSkinState():String {
		if (focused) {
			return "focused";
		}
		if (hovered) {
			return "hovered"
		}
		return "normal";
	}

	//------------------------------
	//	Helpers
	//------------------------------

	private function changeCase():Boolean {
		return settings.editorUpperCase && !ignoreUpperCase;
	}

	//------------------------------
	//	Event Handlers
	//------------------------------

	protected function onCreationComplete(event:FlexEvent):void {
		textDisplay.setStyle("fontFamily", settings.editorFontFamily);
		textDisplay.setStyle("fontSize", settings.editorFontSize);
		textDisplay.setStyle("typographicCase", settings.editorUpperCase && !ignoreUpperCase ? "uppercase" : "default");

		textDisplay.setStyle("focusedTextSelectionColor", 0x98B7EE);
		textDisplay.setStyle("inactiveTextSelectionColor", 0xCBDBE7);

		// Altera o interactionManager para corrigir problema com a perca do histórico do Ctrl+Z ao perder o foco
		textDisplay.textFlow.interactionManager = editManager;
	}

	protected function onFocusIn(event:FocusEvent):void {
		focused = true;
		invalidateSkinState();
	}

	protected function onFocusOut(event:FocusEvent):void {
		focused = false;
		invalidateSkinState();
	}

	protected function onMouseOver(event:MouseEvent):void {
		hovered = true;
		invalidateSkinState();
	}

	protected function onMouseOut(event:MouseEvent):void {
		hovered = false;
		invalidateSkinState();
	}
}
}
