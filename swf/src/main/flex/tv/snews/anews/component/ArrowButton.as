package tv.snews.anews.component {
	import spark.components.Button;

import tv.snews.anews.skin.ArrowSkin;

[Style(name="position", type="String", enumeration="down,up,left,right", inherit="no")]
	[Style(name="arrowColor", type="uint", format="Color", inherit="no", theme="spark")]
	[Style(name="backgroundColor", type="uint", format="Color", inherit="no", theme="spark")]
	[Style(name="borderBg", type="uint", format="Color", inherit="no", theme="spark")]

	public class ArrowButton extends Button {
		
		public function ArrowButton() {
			super();
			setStyle("skinClass", ArrowSkin);
		}
	}
}
