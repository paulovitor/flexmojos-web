package tv.snews.anews.component {
	
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	import spark.primitives.Path;
	
	import tv.snews.anews.skin.MenuItemSkin;

	[Style(name="headerColor", inherit="no", type="uint", format="Color")]
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MenuItem extends MenuGroup {
		
		public function MenuItem() {
			super();
			setStyle("skinClass", MenuItemSkin);
		}
		
		//----------------------------------------------------------------------
		//	Event Handlers
		//----------------------------------------------------------------------
		
		/**
		 * Assim que o componente é criado define os event listeners para suas 
		 * partes internas.
		 */
		override protected function onCreationComplete(event:FlexEvent):void {
			headerLabel.addEventListener(MouseEvent.CLICK, onArrowGroupClick);
			arrowGroup.addEventListener(MouseEvent.CLICK, onArrowGroupClick);
			headerGroup.addEventListener(MouseEvent.MOUSE_OUT, onHeaderGroupMouseAction);
			headerGroup.addEventListener(MouseEvent.MOUSE_OVER, onHeaderGroupMouseAction);
			currentState = "minimized";
		}
		
		override protected function onArrowGroupClick(event:MouseEvent):void {
			if (event.target.id == "arrowGroup") {
				super.onArrowGroupClick(event);
			}
		}
		
		protected function onHeaderGroupClick(event:MouseEvent):void {
			currentState = currentState == "hover" ? "hoverAndMinimized" : "hover";
		}
	}
}
