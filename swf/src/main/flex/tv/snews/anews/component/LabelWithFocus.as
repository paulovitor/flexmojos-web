package tv.snews.anews.component {

import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.ui.Keyboard;
import flash.utils.setTimeout;

import mx.managers.IFocusManagerComponent;

import spark.components.Label;

/**
 * @author Paulo Vitor
 */
public class LabelWithFocus extends Label implements IFocusManagerComponent {

	public function configureLabelEvents(callback:Function):void {
		this.addEventListener(MouseEvent.CLICK, function (event:MouseEvent):void {
			setTimeout(callback, 300);
            event.stopPropagation();
		});

		this.addEventListener(KeyboardEvent.KEY_DOWN, function (event:KeyboardEvent):void {
			if (event.keyCode == Keyboard.ENTER || event.keyCode == Keyboard.SPACE) {
				callback();
                event.stopPropagation();
            }
		});
	}
}

}
