package tv.snews.anews.component {
	
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	import spark.components.Group;
	import spark.components.HGroup;
	import spark.components.Label;
	
	import tv.snews.anews.skin.MenuSubItemSkin;

	[Style(name="headerColor", inherit="no", type="uint", format="Color")]
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MenuSubItem extends MenuItem {
		
		public function MenuSubItem() {
			super();
			setStyle("skinClass", MenuSubItemSkin);
		}
		
		/**
		 * Assim que o componente é criado define os event listeners para suas 
		 * partes internas.
		 */
		override protected function onCreationComplete(event:FlexEvent):void {
			headerGroup.addEventListener(MouseEvent.MOUSE_OUT, onHeaderGroupMouseAction);
			headerGroup.addEventListener(MouseEvent.MOUSE_OVER, onHeaderGroupMouseAction);
			currentState = "minimized";
		}
	}
}
