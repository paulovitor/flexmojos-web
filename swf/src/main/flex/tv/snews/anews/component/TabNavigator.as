package tv.snews.anews.component {
	
	import flexlib.containers.SuperTabNavigator;
	
	import mx.collections.ArrayCollection;
	
	import spark.components.NavigatorContent;
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class TabNavigator extends SuperTabNavigator {
		
		public function TabNavigator() {
			super();
		}
		
		public function displayArea(AreaClass:Class):void {
			var tab:NavigatorContent = null;
			for each (var area:NavigatorContent in this.getChildren()) {
				if (classesAreEquals(AreaClass, area.className)) {
					tab = area;
					break;
				}
			}
			if (tab == null) {
				tab = new AreaClass();
				addChild(tab);
			}
			this.selectedChild = tab;
		}
		
		private function classesAreEquals(classObj:Class, className:String):Boolean {
			var classAsStr:String = classObj.toString();
			var nameToken:String = classAsStr.substr(classAsStr.length - className.length - 1, className.length);
			return className == nameToken;
		}
	}
}