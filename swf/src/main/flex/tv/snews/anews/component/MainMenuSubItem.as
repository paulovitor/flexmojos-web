package tv.snews.anews.component {
	
	import flash.events.MouseEvent;
	
	import mx.events.FlexEvent;
	
	import spark.components.Group;
	import spark.components.HGroup;
	import spark.components.Label;

import tv.snews.anews.skin.MainMenuSubItemSkin;

import tv.snews.anews.skin.MenuSubItemSkin;

	[Style(name="headerColor", inherit="no", type="uint", format="Color")]
	[Style(name="backgroundColor", inherit="no", type="uint", format="Color")]
	[Style(name="headerDarkenColor", inherit="no", type="uint", format="Color")]

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MainMenuSubItem extends MainMenuItem {

        [Bindable]
        public var subMenuIcon:String;

		public function MainMenuSubItem() {
			super();
			setStyle("skinClass", MainMenuSubItemSkin);
		}
		
		/**
		 * Assim que o componente é criado define os event listeners para suas 
		 * partes internas.
		 */
		override protected function onCreationComplete(event:FlexEvent):void {
			headerGroup.addEventListener(MouseEvent.MOUSE_OUT, onHeaderGroupMouseAction);
			headerGroup.addEventListener(MouseEvent.MOUSE_OVER, onHeaderGroupMouseAction);
			currentState = "minimized";
		}
	}
}
