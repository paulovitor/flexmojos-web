package tv.snews.anews.component {

import flashx.textLayout.elements.FlowElement;
import flashx.textLayout.elements.FlowGroupElement;

import flashx.textLayout.operations.PasteOperation;

import mx.events.FlexEvent;
import mx.utils.StringUtil;

import spark.components.RichEditableText;
import spark.components.TextInput;
import spark.events.TextOperationEvent;

import tv.snews.anews.domain.Settings;
import tv.snews.anews.utils.DomainCache;

import mx.core.mx_internal;

use namespace mx_internal;

[DefaultProperty("text")]

[DefaultTriggerEvent("change")]

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class SpellTextInput extends TextInput {

	private static const STYLES_TO_CLEAR:Array = [ 'color', 'fontWeight', 'backgroundColor', 'fontWeight', 'lineThrough'];
	private static const settings:Settings = DomainCache.settings;

	/**
	 * Se true ignora o UpperCase caso esteja ativo no settings.
	 * Define o valor do ignore UpperCase caso ele esteja ativo.
	 */
	public var ignoreUpperCase:Boolean = false;

	//----------------------------------------------------------------------
	//	Constructor
	//----------------------------------------------------------------------

	public function SpellTextInput() {
		super();

		this.horizontalCenter = 0;
		this.verticalCenter = 0;

		this.setStyle("focusThickness", 0);

		this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
		this.addEventListener(FlexEvent.REMOVE, onRemoveHandler);
		this.addEventListener(TextOperationEvent.CHANGING, onChangingHandler);
	}

	//------------------------------
	//	Operations
	//------------------------------

	public function dispose():void {
		this.removeEventListener(FlexEvent.CREATION_COMPLETE, onCreationCompleteHandler);
		this.removeEventListener(FlexEvent.REMOVE, onRemoveHandler);
		this.removeEventListener(TextOperationEvent.CHANGING, onChangingHandler);
	}

	//----------------------------------------------------------------------
	//	Overrides
	//----------------------------------------------------------------------

	override public function get text():String {
		return StringUtil.trim(changeCase() ? super.text.toUpperCase() : super.text);
	}

	[Bindable("change")]
	[Bindable("textChanged")]
	[CollapseWhiteSpace]
	override public function set text(value:String):void {
		super.text = StringUtil.trim(changeCase() ? (value || '').toUpperCase() : value);
	}

	//----------------------------------------------------------------------
	//	Helpers
	//----------------------------------------------------------------------

	private function changeCase():Boolean {
		return settings.editorUpperCase && !ignoreUpperCase;
	}

	//----------------------------------------------------------------------
	//	Event Handlers
	//----------------------------------------------------------------------

	private function onCreationCompleteHandler(event:FlexEvent):void {
		setStyle("fontFamily", settings.editorFontFamily);
		setStyle("typographicCase", settings.editorUpperCase && !ignoreUpperCase ? "uppercase" : "default");

		// Corrige a mudança da cor da seleção que ocorre quando altera o interactionManager
		setStyle("focusedTextSelectionColor", 0x98B7EE);
		setStyle("inactiveTextSelectionColor", 0xCBDBE7);
//		setStyle("unfocusedTextSelectionColor", 0xCBDBE7); // não sei qual era a cor original

		// Altera a flag interna (mx_internal) que faz com que o histórico de alterações seja zerado ao perder o focus
		RichEditableText(this.textDisplay).clearUndoOnFocusOut = false;
	}

	private static function onChangingHandler(event:TextOperationEvent):void {
		if (event.operation is PasteOperation) {
			var pasteOperation:PasteOperation = event.operation as PasteOperation;
			if (pasteOperation.textScrap) {
				clearStyles(pasteOperation.textScrap.textFlow);
			}
		}
	}

	private static function clearStyles(flowElement:FlowElement):void {
		// Limpa os estilos definidos por "STYLES_TO_CLEAR"
		for each (var style:String in STYLES_TO_CLEAR) {
			flowElement.clearStyle(style);
		}

		// Elementos do tipo FlowGroupElement, como o <div> e o <p>, podem possuir filhos.
		// Esses filhos também devem ter seus estilos limpados.
		if (flowElement is FlowGroupElement) {
			var groupElement:FlowGroupElement = flowElement as FlowGroupElement;
			for (var i:int = 0; i < groupElement.numChildren; i++) {
				clearStyles(groupElement.getChildAt(i));
			}
		}
	}

	private function onRemoveHandler(event:FlexEvent):void {
		dispose();
	}
}
}
