package tv.snews.anews.component {
	
	import flash.events.MouseEvent;

import mx.controls.Alert;

import mx.events.FlexEvent;
import mx.utils.ObjectUtil;


import tv.snews.anews.skin.MainMenuItemSkin;


	[Style(name="headerColor", inherit="no", type="uint", format="Color")]
	[Style(name="headerDarkenColor", inherit="no", type="uint", format="Color")]
	[Style(name="backgroundColor", inherit="no", type="uint", format="Color")]

	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MainMenuItem extends MenuGroup {

        [Bindable] public var paddingBottom:int = 5;
        [Bindable] public var contentLeft:int = 0; //35

		public function MainMenuItem() {
			super();
			setStyle("skinClass", MainMenuItemSkin);
		}
		
		//----------------------------------------------------------------------
		//	Event Handlers
		//----------------------------------------------------------------------
		
		/**
		 * Assim que o componente é criado define os event listeners para suas 
		 * partes internas.
		 */
		override protected function onCreationComplete(event:FlexEvent):void {
			headerLabel.addEventListener(MouseEvent.CLICK, onArrowGroupClick);
			arrowGroup.addEventListener(MouseEvent.CLICK, onArrowGroupClick);
			headerGroup.addEventListener(MouseEvent.MOUSE_OUT, onHeaderGroupMouseAction);
			headerGroup.addEventListener(MouseEvent.MOUSE_OVER, onHeaderGroupMouseAction);

            currentState = "minimized";
		}
		
		override protected function onArrowGroupClick(event:MouseEvent):void {
			if (event.target.id == "arrowGroup") {
				super.onArrowGroupClick(event);
			}
		}
		
		protected function onHeaderGroupClick(event:MouseEvent):void {
			currentState = currentState == "hover" ? "hoverAndMinimized" : "hover";
		}

        public function createCopy():MainMenuItem {
            return MainMenuItem(ObjectUtil.copy(this)) as MainMenuItem;
        }
	}
}
