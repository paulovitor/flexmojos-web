//------------------------------------------------------------------------------
//
//  SoftwareNews Soluções para Jornalismo http://www.snews.com.br 
//  Copyright (C) 2012 SoftwareNews Comércio e Serviços de Informática Ltda. 
//
//------------------------------------------------------------------------------

package tv.snews.anews.component {
	import mx.controls.Alert;
	import mx.utils.StringUtil;
	import mx.validators.StringValidator;
	import mx.validators.ValidationResult;
	
	import tv.snews.flexlib.utils.StringUtils;
	
	public class TextSearchValidator extends StringValidator{
		
		private const regexp:RegExp = new RegExp("[\\[\\]\\+\\\\\\-&|!\\(\\)\\{\\}^~\\*?:\\/]");
		//------------------------------
		//	Construtor
		//------------------------------

		public function TextSearchValidator(){
			super();
		}
		
		//------------------------------
		//	Propriedades
		//------------------------------
		private var _formatError:String = "O formato do texto para busca é inválido. Caracteres inválidos +-&|!(){}[]^/~*?:\\";

		public function get formatError():String {
			return _formatError;
		}
		
		public function set formatError(value:String):void {
			_formatError = value;
		}
		
		override protected function doValidation(value:Object):Array {
			var text:String = value as String;
			var validatorResults:Array = super.doValidation(text);
			if (text != "" || required) {
				if (regexp.test(text))
					validatorResults.push(new ValidationResult(true, "", "formatError", formatError));
			} 
			
			return validatorResults;
		}
	}
}