package tv.snews.anews.component {

	import flash.events.MouseEvent;

import mx.controls.Alert;

import mx.events.FlexEvent;
	
	import spark.components.Group;
	import spark.components.HGroup;
	import spark.components.Label;
	import spark.components.SkinnableContainer;
	import spark.layouts.supportClasses.LayoutBase;
	import spark.primitives.Path;
	
	import tv.snews.anews.skin.MenuGroupSkin;

	[SkinState("normal")]
	[SkinState("minimized")]
	[SkinState("disabled")]
	[SkinState("hover")]
	[SkinState("hoverAndMinimized")]
	
	[Style(name="headerHeight", inherit="no", type="uint")]
	
	/**
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class MenuGroup extends SkinnableContainer {
		
		[SkinPart] public var headerGroup:HGroup;
		[SkinPart] public var arrowGroup:Group;
		[SkinPart] public var headerLabel:Label;
		[SkinPart] public var arrow:Path;

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------
		
		public function MenuGroup() {
			super();
			setStyle("skinClass", MenuGroupSkin);
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------
		
		//----------------------------------
		//	label
		//----------------------------------
		
		private var _label:String;
		
		[Bindable]
		public function get label():String {
			return _label;
		}
		
		public function set label(value:String):void {
			_label = value;
		}

		//----------------------------------
		//	label
		//----------------------------------

		private var _handCursor:Boolean = true;

		[Bindable]
		public function get handCursor():Boolean {
			return _handCursor;
		}

		public function set handCursor(value:Boolean):void {
            _handCursor = value;
		}

		//----------------------------------
		//	layout
		//----------------------------------
		
		private var _layout:LayoutBase;
		
		[Bindable]
		override public function get layout():LayoutBase {
			return _layout;
		}
		
		override public function set layout(value:LayoutBase):void {
			_layout = value;
		}
		
		//----------------------------------
		//	layout
		//----------------------------------
		
		override public function get currentState():String {
			return skin.currentState;
		}
		
		override public function set currentState(value:String):void {
			skin.currentState = value;
		}
		
		//----------------------------------------------------------------------
		//	Event Handlers
		//----------------------------------------------------------------------
		
		/**
		 * Assim que o componente é criado define os event listeners para suas 
		 * partes internas.
		 */
		protected function onCreationComplete(event:FlexEvent):void {
			headerLabel.addEventListener(MouseEvent.CLICK, onArrowGroupClick);
			arrowGroup.addEventListener(MouseEvent.CLICK, onArrowGroupClick);
			arrowGroup.addEventListener(MouseEvent.MOUSE_OUT, onHeaderGroupMouseAction);
			arrowGroup.addEventListener(MouseEvent.MOUSE_OVER, onHeaderGroupMouseAction);
		}
		
		protected function onArrowGroupClick(event:MouseEvent):void {
			currentState = currentState == "hover" ? "hoverAndMinimized" : "hover";
			event.preventDefault();
			event.stopPropagation();
			event.stopImmediatePropagation();
		}

        protected function onHeaderGroupMouseAction(event:MouseEvent):void {
            if (event.type == MouseEvent.MOUSE_OVER) {
                currentState = currentState == "normal" ? "hover" : currentState == "minimized" ? "hoverAndMinimized" : currentState;
            } else {
                currentState = currentState == "hover" ? "normal" : currentState == "hoverAndMinimized" ? "minimized" : currentState;
            }
        }
	}
}
