package tv.snews.anews.component {

import flash.events.FocusEvent;

import flashx.textLayout.operations.CompositeOperation;
import flashx.textLayout.operations.DeleteTextOperation;

import flashx.textLayout.operations.InsertTextOperation;

import spark.components.TextInput;
import spark.events.TextOperationEvent;

import tv.snews.flexlib.utils.DateUtil;

/**
 * @author Felipe Pinheiro
 */
public class TimeInput extends TextInput {

	private static const dateUtil:DateUtil = new DateUtil();
	private static const delimiter:String = ":";

	private var previousValue:Date;

	//------------------------------
	//	Properties
	//------------------------------

	private var _date:Date;

	public function get date():Date {
		return _date;
	}

	public function set date(value:Date):void {
		_date = value || dateUtil.timeZero();

		displayTime();
	}

	private var _format:String;

	public function get format():String {
		return _format;
	}

	public function set format(value:String):void {
		_format = value;
		maxChars = value.length;

		displayTime();
	}

	//------------------------------
	//	Constructor
	//------------------------------

	public function TimeInput() {
		format = "JJ:NN:SS";
		restrict = "0-9";

		addEventListener(FocusEvent.FOCUS_IN, this_focusInHandler);
		addEventListener(TextOperationEvent.CHANGING, this_changingHandler);
		addEventListener(TextOperationEvent.CHANGE, this_changeHandler);
	}

	//------------------------------
	//	Methods
	//------------------------------

	private function displayTime():void {
		text = dateUtil.toString(date, format);
	}

	/*
	 * Coloca o cursor no final do texto.
	 */
	private function placeCursor():void {
		selectRange(text.length, text.length);
	}

	/*
	 * Seleciona o texto para substituição.
	 */
	private function selectAllText():void {
		selectRange(0, text.length);
	}

	/*
	 * Converte o texto para um Date, se for preenchido por completo. Caso
	 * contrário o "date" será null.
	 */
	private function parseDate():void {
		if (text.length == format.length) {
			var value:Date = dateUtil.toDate(text, format);
			if (value) {
				_date = value;
				return;
			}
		}
		_date = dateUtil.timeZero();
	}

	//------------------------------
	//	Event Handlers
	//------------------------------

	private function this_focusInHandler(event:FocusEvent):void {
        selectAllText();
	}

	private function this_changingHandler(event:TextOperationEvent):void {
		if (event.operation is InsertTextOperation) {
			var tokens:Array = format.split(delimiter);
			var actualSize:int = 0;

			for (var i:int = 0; i < tokens.length - 1; i++) {
				actualSize += tokens[i].length + i;
				if (actualSize == text.length) {
					var insert:InsertTextOperation = event.operation as InsertTextOperation;
					insert.text = delimiter + insert.text;
				}
			}

			placeCursor();
		}
	}

	private function this_changeHandler(event:TextOperationEvent):void {
		if (!(event.operation is CompositeOperation) && !(event.operation is DeleteTextOperation)) {
			var tokens:Array = format.split(delimiter);
			var actualSize:int = 0;

			for (var i:int = 0; i < tokens.length - 1; i++) {
				actualSize += tokens[i].length + i;
				if (actualSize == text.length) {
					text += ":";
				}
			}

			placeCursor();
		}
		parseDate();
	}

}
}
