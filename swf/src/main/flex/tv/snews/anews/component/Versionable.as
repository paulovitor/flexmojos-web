package tv.snews.anews.component {
	public interface Versionable {
		function getVersionDate():Date; 
	}
}