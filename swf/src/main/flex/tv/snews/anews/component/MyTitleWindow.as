/**
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
package tv.snews.anews.component {
	
	import spark.components.TitleWindow;

import tv.snews.anews.skin.MyTitleWindowSkin;

public class MyTitleWindow extends TitleWindow {
		
		public function MyTitleWindow() {
			super();
			setStyle("skinClass", MyTitleWindowSkin);
		}
	}
}
