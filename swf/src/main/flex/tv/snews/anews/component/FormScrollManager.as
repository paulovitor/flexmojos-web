package tv.snews.anews.component {

import flash.display.DisplayObject;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.geom.Rectangle;

import mx.core.FlexGlobals;
import mx.core.IVisualElement;
import mx.core.UIComponent;
import mx.effects.AnimateProperty;

import spark.components.RichEditableText;

import spark.components.Scroller;

import spark.components.TextArea;

/**
 * @author Felipe Pinheiro
 */
public class FormScrollManager {

	private const visualIncrement:Number = 10;

	private var scroller:Scroller;

	public function FormScrollManager(scroller:Scroller) {
		this.scroller = scroller;

		scroller.addEventListener(FocusEvent.FOCUS_IN, scroller_focusInHandler);
		scroller.addEventListener(KeyboardEvent.KEY_DOWN, scroller_keyDownHandler);
	}

	public function deactivate():void {
		if (scroller) {
			scroller.removeEventListener(FocusEvent.FOCUS_IN, scroller_focusInHandler);
			scroller.removeEventListener(KeyboardEvent.KEY_DOWN, scroller_keyDownHandler);
			scroller = null;
		}
	}

	private function ensureVisibility(target:DisplayObject):void {
		/*
			TextInput, TextArea e ComboBox, internamente, o focus é feito em um RichEditableText
			dentro do Skin. Sendo assim, o "if" abaixo limita a lógica desse método a esses tipos
			de componente visual.
		 */
		if (target is RichEditableText) {
			// Se o target for uma parte de um TextArea, redireciona para o TextArea
			var backup:DisplayObject = target;
			while (target != null && !(target is TextArea)) {
				target = target.parent;
			}
			target = target || backup;

			var parent:DisplayObject = FlexGlobals.topLevelApplication as DisplayObject;

			var scrollerRect:Rectangle = scroller.getVisibleRect();
			var targetRect:Rectangle = target.getRect(parent);

			// Limita o tamanho do TextArea à altura do scroll - (visualIncrement * 2) pixels
			if (targetRect.height + (visualIncrement * 2) > scrollerRect.height) {
				if (target is TextArea) {
					TextArea(target).maxHeight = scrollerRect.height - (visualIncrement * 2);
				}
			}

			var scrollerLimit:Number = scrollerRect.y + scrollerRect.height;
			var targetLimit:Number = targetRect.y + targetRect.height;

			var verticalScrollPosition:Number = scroller.verticalScrollBar.value,
					diff:Number;

			if (targetLimit > scrollerLimit) {
				diff = targetLimit - scrollerLimit + visualIncrement;
				scrollToPosition(verticalScrollPosition + diff);
			} else if (targetRect.y < scrollerRect.y) {
				diff = scrollerRect.y - targetRect.y + visualIncrement;
				scrollToPosition(verticalScrollPosition - diff);
			}
		}
	}

	private function scrollToPosition(pos:Number):void {
		if (scrollEffect.isPlaying) {
			scrollEffect.stop();
		}
		scrollEffect.toValue = pos;
		scrollEffect.play();
	}

	private var _scrollEffect:AnimateProperty;

	private function get scrollEffect():AnimateProperty {
		if (!_scrollEffect) {
			_scrollEffect = new AnimateProperty(scroller.verticalScrollBar);
			_scrollEffect.property = "value";
			_scrollEffect.duration = 200;
		}
		return _scrollEffect;
	}

	private function scroller_focusInHandler(event:FocusEvent):void {
		ensureVisibility(event.target as DisplayObject);
	}

	private function scroller_keyDownHandler(event:KeyboardEvent):void {
		ensureVisibility(event.target as DisplayObject);
	}
}
}
