package tv.snews.anews.component {

import com.adobe.linguistics.spelling.SpellUI;

import mx.core.UIComponent;
import mx.resources.ResourceManager;

/**
 * @author Felipe Pinheiro
 * @since 1.0.0
 */
public class SpellingManager {

	private static var _instance:SpellingManager = null;

	//----------------------------------
	//	Singleton Constructor
	//----------------------------------

	public function SpellingManager() {
		if (_instance) {
			throw new Error("This class cannot be instantiated. You can only use it's static methods.");
		}

		SpellUI.spellingConfigUrl = "dictionaries/AdobeSpellingConfig.xml";
	}

	//----------------------------------
	//	Instance Methods
	//----------------------------------

	public function enableSpelling(comp:UIComponent):void {
		SpellUI.enableSpelling(comp, getCurrentLocale());
	}

	public function disableSpelling(comp:UIComponent):void {
		SpellUI.disableSpelling(comp);
	}

	/**
	 * @private
	 * Apenas retorna o locale ativo no ResourceManager.
	 */
	private function getCurrentLocale():String {
		return ResourceManager.getInstance().localeChain[0];
	}

	//----------------------------------
	//	Static Methods
	//----------------------------------

	public static function get instance():SpellingManager {
		if (!_instance) {
			_instance = new SpellingManager();
		}
		return _instance;
	}

	public static function enableSpelling(comp:UIComponent):void {
		instance.enableSpelling(comp);
	}

	public static function disableSpelling(comp:UIComponent):void {
		instance.disableSpelling(comp);
	}
}
}
