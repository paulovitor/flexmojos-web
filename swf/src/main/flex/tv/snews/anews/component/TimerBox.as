package tv.snews.anews.component {

	import mx.graphics.SolidColorStroke;

	import spark.components.Group;
	import spark.components.SkinnableContainer;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.primitives.Rect;

import tv.snews.anews.skin.TimerBoxSkin;

[Style(name = "topColor", type = "uint", format = "Color")]
	[Style(name = "bottomColor", type = "uint", format = "Color")]
	public class TimerBox extends SkinnableContainer {

		public function TimerBox() {
			super();
			setStyle("skinClass", TimerBoxSkin);
		}

	}
}
