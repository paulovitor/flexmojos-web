package tv.snews.anews.component {

import flash.display.DisplayObject;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.core.FlexGlobals;
import mx.events.CloseEvent;
import mx.events.FlexEvent;
import mx.managers.PopUpManager;
import mx.resources.ResourceManager;

import spark.components.Button;

import tv.snews.anews.event.ANewsPlayEvent;
import tv.snews.anews.event.CrudEvent;
import tv.snews.anews.view.media.MediaSearchWindow;

public class ButtonAddVideo extends Button {

    [Bindable]
    public var mamDevices:ArrayCollection;
    [Bindable]
    public var crudEventCreateHandler:Function;
    [Bindable]
    public var closeEventCloseHandler:Function;
    [Bindable]
    public var after_clickHandler:Function;

    //------------------------------
    //	Constructor
    //------------------------------

    public function ButtonAddVideo() {
        super();
        addEventListener(FlexEvent.CREATION_COMPLETE, creationCompleteHandler);
        addEventListener(MouseEvent.CLICK, this_clickHandler);
        addEventListener(FlexEvent.REMOVE, removeHandler)
    }

    //------------------------------
    //	Event Handlers
    //------------------------------

    private function creationCompleteHandler(event:FlexEvent):void {
        label = ResourceManager.getInstance().getString('Bundle', 'defaults.btn.video');
        includeInLayout = visible = mamDevices != null && mamDevices.length != 0;
        buttonMode = true;
        useHandCursor = true;
        focusEnabled = false;
    }

    private function this_clickHandler(event:MouseEvent):void {
        this.dispatchEvent(new ANewsPlayEvent(ANewsPlayEvent.CLOSE));
        var mediaSearchWindow:MediaSearchWindow = new MediaSearchWindow();
        if (crudEventCreateHandler != null)
            mediaSearchWindow.addEventListener(CrudEvent.CREATE, crudEventCreateHandler);
        if (closeEventCloseHandler != null)
            mediaSearchWindow.addEventListener(CloseEvent.CLOSE, closeEventCloseHandler);
        PopUpManager.addPopUp(mediaSearchWindow, DisplayObject(FlexGlobals.topLevelApplication), true);
        PopUpManager.centerPopUp(mediaSearchWindow);
        if (after_clickHandler != null)
            after_clickHandler();
    }

    private function removeHandler(event:FlexEvent):void {
        removeEventListener(MouseEvent.CLICK, this_clickHandler);
    }

}
}
