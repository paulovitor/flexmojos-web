<?xml version="1.0" encoding="utf-8"?>

<!--
	Componente que apresenta e implementa uma das colunas do quadro de tarefas.

	@author Felipe Pinheiro
	@since 4.0.0
-->
<s:Group xmlns:fx="http://ns.adobe.com/mxml/2009"
		 xmlns:s="library://ns.adobe.com/flex/spark"
		 xmlns:tn="tv.snews.anews.component.*"
		 minWidth="{TaskBoardItemRendererGuideline.WIDTH + 12}"
		 creationComplete="onCreationComplete(event)">

	<fx:Metadata>
		[Event(name="change", type="spark.events.IndexChangeEvent")]
		[Style(name="columnColor", type="uint", format="Color")]
	</fx:Metadata>

	<fx:Script>
		<![CDATA[
			import mx.collections.IList;
			import mx.events.DragEvent;
			import mx.events.FlexEvent;
			import mx.rpc.events.ResultEvent;
			import mx.utils.ObjectUtil;
			
			import spark.events.IndexChangeEvent;
			
			import tv.snews.anews.domain.Document;
			import tv.snews.anews.domain.DocumentState;
			import tv.snews.anews.domain.EntityType;
			import tv.snews.anews.domain.Guide;
			import tv.snews.anews.domain.Guideline;
			import tv.snews.anews.domain.News;
			import tv.snews.anews.domain.Reportage;
			import tv.snews.anews.event.CrudEvent;
			import tv.snews.anews.service.GuidelineService;
			import tv.snews.anews.service.ReportageService;
			import tv.snews.flexlib.utils.ObjectUtils;

            private static const guidelineService:GuidelineService = GuidelineService.getInstance();

			[Bindable] public var isDraggin:Boolean = false;

			[Bindable] public var entityType:String;
			
			private var _taskBoard:TaskBoard;

			//------------------------------------------------------------------
			//	Properties
			//------------------------------------------------------------------

			//------------------------------
			//	columnName
			//------------------------------

			private var _columnName:String;

			/**
			 * Retorna o nome de identificação desta coluna.
			 *
			 * @return Nome da coluna.
			 */
			[Bindable] public function get columnName():String {
				return _columnName;
			}

			/**
			 * Define o nome de identificação da coluna.
			 *
			 * @param value Nome para a coluna.
			 */
			public function set columnName(value:String):void {
				_columnName = value;
			}

			//------------------------------
			//	updateState
			//------------------------------

			private var _updateState:String;

			/**
			 * Retorna o valor de 'GuidelineState' que foi definido para esta
			 * coluna utilizar quando uma pauta é arrastada para ela.
			 *
			 * @return Valor de 'GuidelineState'.
			 */
			[Bindable] public function get updateState():String {
				return _updateState;
			}

			/**
			 * Define o valor de 'GuidelineState' que será utilizado na atualização
			 * da pauta quando ela é arrastada para esta coluna.
			 *
			 * @param value Valor de 'GuidelineState'.
			 */
			public function set updateState(value:String):void {
				_updateState = value;
			}

			//------------------------------
			//	dataProvider
			//------------------------------

			[Bindable] public function get dataProvider():IList {
				return list.dataProvider;
			}

			public function set dataProvider(value:IList):void {
				list.dataProvider = value;
			}

			//------------------------------
			//	selectedItem
			//------------------------------

			[Bindable] public function get selectedItem():Document {
				return 	list.selectedItem as Document;
			}

			public function set selectedItem(value:Document):void {
				list.selectedItem = value;
			}

			//------------------------------
			//	selectedIndex
			//------------------------------

			[Bindable] public function get selectedIndex():int {
				return list.selectedIndex;
			}

			public function set selectedIndex(value:int):void {
				list.selectedIndex = value;
			}

			//------------------------------------------------------------------
			//	Event Handlers
			//------------------------------------------------------------------

			protected function onCreationComplete(event:FlexEvent):void {
				if (!columnName) {
					throw new Error("You didn't defined a name for the column.");
				}
				if (!updateState) {
					throw new Error("You didn't defined the 'updateState' for the dragDrop event handler.");
				}
			}

			protected function onListChange(event:IndexChangeEvent):void {
				this.dispatchEvent(event);
			}

			protected function onDragOver(event:DragEvent):void {
				var list:List = event.dragInitiator as List;

				if (taskBoard) {
					var producingColumn:TaskBoardColumn = taskBoard.producingColumn as TaskBoardColumn;
					var coveringColumn:TaskBoardColumn = taskBoard.coveringColumn as TaskBoardColumn;
					var completedColumn:TaskBoardColumn = taskBoard.completedColumn as TaskBoardColumn;
					var interruptedColumn:TaskBoardColumn = taskBoard.interruptedColumn as TaskBoardColumn;
					var editingColumn:TaskBoardColumn = taskBoard.editingColumn as TaskBoardColumn;

					
					if (list.selectedItem is News && entityType == EntityType.REPORTAGE) {
						producingColumn.list.dropEnabled = false;
						coveringColumn.list.dropEnabled = false;
						completedColumn.list.dropEnabled = false;
						interruptedColumn.list.dropEnabled = false;
						editingColumn.list.dropEnabled = false;
						return;
					} else {
						producingColumn.list.dropEnabled = true;
						coveringColumn.list.dropEnabled = true;
						completedColumn.list.dropEnabled = true;
						interruptedColumn.list.dropEnabled = true;
						editingColumn.list.dropEnabled = true;
					}
					
					if (list.selectedItem is News && entityType == EntityType.GUIDELINE ||
						list.selectedItem is Guideline && entityType == EntityType.REPORTAGE
					) {
						coveringColumn.list.dropEnabled = false;
						completedColumn.list.dropEnabled = false;
						interruptedColumn.list.dropEnabled = false;
						editingColumn.list.dropEnabled = false;
					}  else {
						coveringColumn.list.dropEnabled = true;
						completedColumn.list.dropEnabled = true;
						interruptedColumn.list.dropEnabled = true;
						editingColumn.list.dropEnabled = true;
					}
				
					
				}
			}

			protected function onDropComplete(event:DragEvent):void {
				if (taskBoard) {
					var coveringColumn:TaskBoardColumn = taskBoard.coveringColumn as TaskBoardColumn;
					var completedColumn:TaskBoardColumn = taskBoard.completedColumn as TaskBoardColumn;
					var interruptedColumn:TaskBoardColumn = taskBoard.interruptedColumn as TaskBoardColumn;
					var editingColumn:TaskBoardColumn = taskBoard.editingColumn as TaskBoardColumn;


					coveringColumn.list.dropEnabled = true;
					completedColumn.list.dropEnabled = true;
					interruptedColumn.list.dropEnabled = true;
					editingColumn.list.dropEnabled = true;
				}
			}

			protected function onListDragDrop(event:DragEvent):void {
				var list:List = event.dragInitiator as List;
				var guideline:Guideline = null;

				if (list.selectedItem is Guideline && entityType == EntityType.GUIDELINE) {
					guideline = list.selectedItem as Guideline;


					if (updateState == DocumentState.INTERRUPTED) {
						var guidelineChangeState:Guideline = ObjectUtil.clone(guideline) as Guideline;
						guidelineChangeState.state = DocumentState.INTERRUPTED;
						var editEvent:CrudEvent = new CrudEvent(CrudEvent.UPDATE, guidelineChangeState);
						this.dispatchEvent(editEvent);
					} else {

						/*
						 * Precisa do delay de zero para corrigir o bug de quando faz o
						 * drag para uma coluna vazia.
						 */
						setTimeout(function (guideline:Guideline):void {
							if (guideline) {
								guidelineService.updateState(guideline.id, updateState, null,
										function (event:ResultEvent):void {
											guideline.state = updateState;
										}
								);
							} else {
								throw new Error("Could not obtain the dragged guideline.");
							}
						}, 0, guideline);

					}
				}
				if (list.selectedItem is News && entityType == EntityType.GUIDELINE) {
					var news:News = list.selectedItem as News;
					guideline = new Guideline();
					guideline.slug = news.slug;
					guideline.proposal = news.lead;
					guideline.state = DocumentState.PRODUCING;
					guideline.informations = news.information;
					guideline.news = news;

					if (news.contacts.length > 0) {
						var guide:Guide = new Guide();
						guide.contacts.addAll(news.contacts);
						guide.guideline = guideline;
						guide.orderGuide = 0;
						guideline.guides.addItem(guide);
					}
					this.dispatchEvent(new CrudEvent(CrudEvent.CREATE, guideline));
				}

				if (list.selectedItem is Guideline && entityType == EntityType.REPORTAGE) {
					this.dispatchEvent(new CrudEvent(CrudEvent.CREATE, list.selectedItem as Guideline));
					event.preventDefault();
					return;
				}

				if (list.selectedItem is Reportage) {
					var reportage:Reportage = list.selectedItem as Reportage;
					/*
					 * Precisa do delay de zero para corrigir o bug de quando faz o
					 * drag para uma coluna vazia.
					 */
					setTimeout(function (reportage:Reportage):void {
						if (reportage) {
							ReportageService.getInstance().updateState(reportage.id, updateState, null,
									function (event:ResultEvent):void {
										reportage.state = updateState;
									}
							);
						} else {
							throw new Error("Could not obtain the dragged guideline.");
						}
					}, 0, reportage);
				}
			}

			protected function onDoubleClick(event:MouseEvent):void {
				var editEvent:CrudEvent = new CrudEvent(CrudEvent.EDIT, selectedItem);
				this.dispatchEvent(editEvent);
			}

			private function get taskBoard():TaskBoard {
				if (!_taskBoard) {
					var container:DisplayObjectContainer = this.parent;
					while (container != null && !(container is TaskBoard)) {
						container = container.parent;
					}
					_taskBoard = container as TaskBoard;
				}
				return _taskBoard;
			}
			
			protected function onCreationCompleteList(event:FlexEvent):void {
				if (entityType == EntityType.GUIDELINE) {
					list.itemRenderer =	new ClassFactory(TaskBoardItemRendererGuideline);
				}
				
				if (entityType == EntityType.REPORTAGE) {
					list.itemRenderer =	new ClassFactory(TaskBoardItemRendererReportage);
				}
			}
			
		]]>
	</fx:Script>

	<s:Rect left="0" right="0" top="0" bottom="0" topLeftRadiusX="2" topLeftRadiusY="2" topRightRadiusX="2"
			topRightRadiusY="2" bottomLeftRadiusX="2" bottomLeftRadiusY="2" bottomRightRadiusX="2"
			bottomRightRadiusY="2">
		<s:stroke>
			<s:SolidColorStroke color="#E7E7E8" weight="2" joints="bevel"/>
		</s:stroke>
	</s:Rect>

	<s:Rect left="2" right="2" top="0">
		<s:stroke>
			<s:SolidColorStroke color="{getStyle('columnColor')}" weight="2"/>
		</s:stroke>
	</s:Rect>

	<s:Rect left="1" right="1" top="1">
		<s:stroke>
			<s:SolidColorStroke color="{getStyle('columnColor')}" weight="1" joints="bevel"/>
		</s:stroke>
	</s:Rect>

	<s:Rect left="0" right="0" top="2">
		<s:stroke>
			<s:SolidColorStroke color="{getStyle('columnColor')}" weight="1" joints="bevel"/>
		</s:stroke>
	</s:Rect>

	<!--<s:Rect left="1" right="1" top="2" height="4">
		<s:fill>
			<s:LinearGradient rotation="90">
				<s:GradientEntry color="{getStyle('columnColor')}"/>
				<s:GradientEntry color="0xFFFFFF"/>
			</s:LinearGradient>
		</s:fill>
	</s:Rect>-->

	<s:VGroup left="2" right="2" bottom="2" width="100%" height="100%" top="3" gap="0" paddingTop="6">
		<s:Label text="{columnName}" width="100%"  paddingLeft="10" paddingBottom="6" fontSize="14" fontWeight="bold"/>

		<tn:HorizontalLine color="0xE2E2E2"/>

		<s:List id="list" width="100%" height="100%" creationComplete="onCreationCompleteList(event)"
				dataProvider="{dataProvider}" change="onListChange(event)" horizontalScrollPolicy="off"
				borderVisible="false" dragEnabled="{ANews.USER.allow('01050107') || ANews.USER.allow('01050110')}"
				dropEnabled="{ANews.USER.allow('01050107')  || ANews.USER.allow('01050110') &amp;&amp; ANews.USER.allow('01050102')}"
				dragOver="onDragOver(event)" dragComplete="onDropComplete(event)" dragMoveEnabled="true"
				dragDrop="onListDragDrop(event)" doubleClickEnabled="true" doubleClick="onDoubleClick(event)">
			<s:layout>
				<s:TileLayout horizontalGap="6" verticalGap="6" paddingTop="6" paddingBottom="6" paddingLeft="6"
							  paddingRight="6"/>
			</s:layout>
		</s:List>
	</s:VGroup>

</s:Group>
