/**
 * Notificador mostrado sobre a aplicação para informar
 * alguma ação no sistema em tempo real.
 * 
 * @author Felipe Zap de Mello
 * @since 1.0.0
 */
package tv.snews.anews.component {
	
	import mx.collections.ArrayCollection;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	import spark.components.Button;
	import spark.components.DataGroup;
	import spark.components.Label;
	import spark.components.SkinnableContainer;
	import spark.components.supportClasses.SkinnableComponent;
	
	import tv.snews.anews.skin.NotifierSkin;
	
	
	public class Notifier extends SkinnableComponent {
	
		/** Botão do notificador caso necessário disparar uma função ao clicar. **/
		[SkinPart] public var buttonNotifier:Button;
		
		/** Label da mensagem do notificador. **/
		[SkinPart] public var labelNotifier:Label;
		
		/** Hora do notificador. **/
		[Bindable] public var timeNotifier:Date;
		
		/** Cor do HeaderBox**/
		[Bindable] public var color:uint;
		
		public function Notifier(colorBox:uint) {
			super();
			this.color = colorBox;
			setStyle("skinClass", NotifierSkin);
		}
	}
}