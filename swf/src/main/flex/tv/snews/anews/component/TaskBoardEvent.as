package tv.snews.anews.component {

	import flash.events.Event;
	
	import tv.snews.anews.domain.Document;
	import tv.snews.anews.domain.Guideline;

	/**
	 * Evento para notificiar as ações no TaskBoard.
	 *
	 * @author Felipe Pinheiro
	 * @since 1.0.0
	 */
	public class TaskBoardEvent extends Event {

		public static const SELECTION_CHANGE:String = "selectionChange";

		//----------------------------------------------------------------------
		//	Properties
		//----------------------------------------------------------------------

		private var _document:Document;
		
		public function get document():Document {
			return _document;
		}
		
		public function set document(value:Document):void {
			_document = value;
		}

		//----------------------------------------------------------------------
		//	Constructor
		//----------------------------------------------------------------------

		public function TaskBoardEvent(type:String, document:Document, bubbles:Boolean = false, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
			this.document = document;
		}

	}
}
