package tv.snews.anews.component {

	import mx.graphics.SolidColorStroke;

	import spark.components.Group;
	import spark.components.SkinnableContainer;
	import spark.components.supportClasses.SkinnableComponent;
	import spark.primitives.Rect;

import tv.snews.anews.skin.ColorBoxSkin;

[Style(name = "topBorderColor", type = "uint", format = "Color")]
	public class ColorBox extends SkinnableContainer {

		public function ColorBox() {
			super();
			setStyle("skinClass", ColorBoxSkin);
		}

		//----------------------------------------------------------------------
		//
		//	Properties
		//
		//----------------------------------------------------------------------

		//------------------------------
		//	topRadius
		//------------------------------

		private var _topRadius:int = 4;

		[Bindable]
		public function get topRadius():int {
			return _topRadius;
		}

		public function set topRadius(value:int):void {
			_topRadius = value;
		}
		
		//------------------------------
		//	bottomRadius
		//------------------------------
		
		private var _bottomRadius:int = 4;
		
		[Bindable]
		public function get bottomRadius():int {
			return _bottomRadius;
		}
		
		public function set bottomRadius(value:int):void {
			_bottomRadius = value;
		}
	}
}
