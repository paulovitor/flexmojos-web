package tv.snews.anews.component {

import flash.events.Event;

import mx.collections.IList;
import mx.events.CollectionEvent;
import mx.events.CollectionEventKind;

import spark.components.DropDownList;

/**
 * Componente que implementa a mesma função do ComboBoxBindable, mas nesse
 * caso é baseado no componente DropDownList.
 *
 * @author Felipe Pinheiro
 * @since 1.2.6.3
 */
public class DropDownListBindable extends DropDownList {

	[Bindable]
	public var valueField:String = "";

	private var _selected:*;
	private var _lastSelected:*;

	public function DropDownListBindable() {
		super();
	}

	override public function set selectedItem(value:*):void {
		if (value == undefined || value == null) {
			selectedIndex = -1;
			super.selectedItem = value;
		}
		_selected = value;

		try {
			for (var i:uint = 0; i < dataProvider.length; i++) {
				if (dataProvider[i][valueField] == value[valueField]) {
					selectedIndex = i;
					break;
				} else {
					selectedIndex = -1;
				}
			}
		} catch (e:Error) {
			trace(e);
		}
	}

	override public function set dataProvider(value:IList):void {
		super.dataProvider = value;
		selectedItem = _selected;
	}

	override protected function dataProvider_collectionChangeHandler(event:Event):void {
		if (_selected) {
			_lastSelected = _selected;
		}

		var collectionEvent:CollectionEvent = CollectionEvent(event);
		if (collectionEvent.kind != CollectionEventKind.REFRESH) {
			super.dataProvider_collectionChangeHandler(event);

			if (_lastSelected) {
				selectedItem = _lastSelected;
			}
		}
	}
}
}
