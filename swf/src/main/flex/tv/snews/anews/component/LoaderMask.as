package tv.snews.anews.component {
	import flash.display.Sprite;
	
	import mx.containers.Box;
	import mx.controls.ProgressBar;
	import mx.core.Application;
	import mx.core.FlexGlobals;
	import mx.managers.PopUpManager;
	
	/**
	* @author Felipe Zap de Mello
	* @since 4.0.0
	*/
	public class LoaderMask extends Box {
		
		private static var _mask:LoaderMask;
		
		private var _message:String;
		
		public function LoaderMask() {
			super();
		}
		
		public static function show(message:String, parent:Sprite=null):void{
			_mask = new LoaderMask();
			_mask._message = message;
			PopUpManager.addPopUp(_mask, parent||Sprite(FlexGlobals.topLevelApplication), true);
			PopUpManager.centerPopUp(_mask);
		}
		
		
		public static function close():void {
			PopUpManager.removePopUp(_mask);
			if (_mask) {
				_mask = null;
			}
		}
		
		override protected function createChildren():void {
			super.createChildren();
			
			var pb:ProgressBar = new ProgressBar();
			pb.label = _message||"Carregando...";
			pb.indeterminate = true;
			pb.labelPlacement= 'top';
			
			//pb.setStyle("barColor", "haloBlue");
			//pb.setStyle("trackColors", ["white", "haloSilver"]);
			//pb.setStyle("trackHeight", 15);
				
			addChild(pb);				
		}
	}
}